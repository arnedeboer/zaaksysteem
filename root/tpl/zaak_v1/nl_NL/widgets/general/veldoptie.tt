[% USE Dumper %]

[%

    bag = undef;

# Opties
# veldoptie_type    => type
# veldoptie_name    => type
# veldoptie_value    => type
# veldoptie_opties    => type
    RETURN UNLESS (veldoptie_type && veldoptie_name);
%]

[% USE JSON %]

[% BLOCK veldoptie_value_row %]
    <li>
        [% INCLUDE veldoptie_single veldoptie_value=vokey %]
        <div class="row-actions">
            <a href="#" class="del">
                <i class="icon-font-awesome icon-remove"></i>
            </a>
            <span class="del-inactive">
                <i class="icon-font-awesome icon-remove icon-remove-disabled"></i>
            </span>
        </div>
    </li>
[% END %]

[% BLOCK veldoptie_multiple %]
<div class="veldoptie_multiple">
    <ul class="veldoptie_multiple_list">
        [% multiple_shown = 0 %]
        [% FOREACH vokey IN veldoptie_value %]
            [% NEXT UNLESS vokey.length %]
            [% multiple_shown = 1 %]
            [% INCLUDE veldoptie_value_row %]
        [% END %]

        [% UNLESS multiple_shown %]
            [% vokey = veldoptie_value %]
            [% INCLUDE veldoptie_value_row %]
        [% END %]
    </ul>
    [% IF veldoptie_label_multiple %]
        [% title = veldoptie_label_multiple | html_entity %]
    [% ELSE %]
        [% title = "Veld toevoegen" %]
    [% END %]
    <a href="#" class="add-veldoptie add button button-secondary
    button-small"><i class="icon icon-font-awesome icon-plus"></i> [% title %]</a>
</div>
[% END %]

[% BLOCK veldoptie_single %]

    [% IF veldoptie_id %]
        [% my_veldoptie_id = ' id="' _ veldoptie_id _ '"' %]
    [% ELSE %]
        [% my_veldoptie_id = '' # freaking TT globals %]
    [% END %]

    [% IF veldoptie_type == 'textarea' %]
        <textarea
            name="[% veldoptie_name %]"
            cols="60"
            rows="4"
            [% my_veldoptie_id %]
            class="veldoptie_textarea"
        >[% veldoptie_value.0 || veldoptie_value | html_entity %]</textarea>
    [% ELSIF
        veldoptie_type == 'text' ||
        veldoptie_type == 'image_from_url' ||
        veldoptie_type == 'text_uc'
    %]
        [% IF veldoptie_type == 'image_from_url' && veldoptie_value %]
            <img src="[% veldoptie_value | html_entity %]" /><br />
        [% END %]
        <input
            type="text"
            [% my_veldoptie_id %]
            name="[% veldoptie_name %]"
            value="[% veldoptie_value.0 || veldoptie_value | html_entity %]"
            class="veldoptie_[% veldoptie_type %]"
            />
    [% ELSIF veldoptie_type == 'numeric' %]
        <input
            type="number"
            [% my_veldoptie_id %]
            name="[% veldoptie_name %]"
            value="[% veldoptie_value.0 || veldoptie_value | html_entity %]"
            class="veldoptie_[% veldoptie_type %]"
            ng-model="[% veldoptie_name %]"
            zs-scope
            />
        <div class="kenmerk-veld-validation-message validator invalid rounded">
            <div class="validate-content"><i class="icon-remove-sign icon-font-awesome"></i>Vul enkel cijfers in</div>
        </div>
    
    [% ELSIF veldoptie_type == 'richtext' %]
        <textarea [% my_veldoptie_id %] name="[% veldoptie_name %]" class="ezra_dagobert_editor ezra_dagobert_editor_enlarge hide simple" data-zs-case-webform-rich-text-field>[% veldoptie_value | html_entity %]</textarea>

    [% ELSIF veldoptie_type == 'url' %]

        <div class="ezra_url_input_type veldoptie_webadres">
            <input
                type="text"
                [% my_veldoptie_id %]
                name="[% veldoptie_name %]"
                value="[% veldoptie_value | html_entity %]"
                class="veldoptie_[% veldoptie_type %]"
                />

            <button class="ezra_url_field_goto button button-secondary button-small [% UNLESS veldoptie_value %]disabled="disabled"[% END %]" rel="[% veldoptie_value | html_entity %]" title="Ga naar de url">
                <i class="icon-font-awesome icon icon-external-link"></i>
            </button>
        </div>

    [% ELSIF veldoptie_type == 'bankaccount' %]
        [% PROCESS widgets/general/veldoptie/bankaccount.tt %]
    [% ELSIF veldoptie_type == 'email' %]
        [% PROCESS widgets/general/veldoptie/email.tt %]
    [% ELSIF veldoptie_type == 'date' %]
        <input
            type="text"
            placeholder="Vul een datum in (dd-mm-jjjj)"
            name="[% veldoptie_name %]"
            value="[% veldoptie_value.0 || veldoptie_value | html_entity %]"
            [% IF !veldoptie_zaaktype_kenmerk.date_fromcurrentdate || zaak.id != '' %]
            class="veldoptie_datepicker normal100"
            [% ELSE %]
            class="veldoptie_datepicker_begindate normal100"
            [% END %]
            [% IF my_veldoptie_id %]
                [% my_veldoptie_id %]
            [% ELSE %]
                id="dp_[% veldoptie_name %]"
            [% END %]
        />
    [% ELSIF veldoptie_type.match('^bag_') %]
        [% multiple_addresses = 
            veldoptie_type == 'bag_adressen' ||
            veldoptie_type == 'bag_straat_adressen' ||
            veldoptie_type == 'bag_openbareruimtes'
        %]
        [% select_street = veldoptie_type.match('openbareruimte')  %]
        <div class="veldoptie-bag veldoptie_bag_angular_wrapper[% IF multiple_addresses %] multiple-addresses[% END %][% IF select_street %] select-street[% END %]" data-zs-case-webform-object-field data-zs-case-webform-object-field-limit="[% IF !multiple_addresses %]1[% END %]" data-zs-case-webform-object-field-value="value" data-zs-case-webform-object-field-data-converter>
            <script type="text/zs-scope-data" data-zs-scope-data-load="caseWebformObjectFieldDataConverter.setObjects($data.value)">
                [% temp = [] %]

                [% FOREACH key IN veldoptie_value %]
                    [% json_entry = c
                                    .model('Gegevens::Bag')
                                    .retrieve_bag_entry(key)
                    %]
                    [% temp.push(json_entry) IF json_entry %]
                [% END %]

                [% JSON.encode({
                    value => temp
                }) %]
            </script>
            <div class="bag-selected-addresses ng-cloak">
                <label data-ng-repeat="address in caseWebformObjectField.objects">
                    <input class="ezra-disable_fieldUpdate ezra-ezra_disable_webform_update" type="checkbox" checked="checked" name="[% veldoptie_name %]" value="<[address.bag_id]>" data-ng-click="caseWebformObjectField.removeObject(address)"/>
                    <[address.human_identifier]>
                </label>
            </div>
            <div class="spot-enlighter-wrapper" data-ng-show="caseWebformObjectField.limit>caseWebformObjectField.objects.length">
                <input class="ezra-disable_fieldUpdate ezra-ezra_disable_webform_update" type="text" name="bogus_[% veldoptie_name %]" data-ng-model="newObject" data-zs-spot-enlighter-select="caseWebformObjectField.handleObjectSelect($object)" data-zs-placeholder="'[% IF !select_street %]Vul een adres in[% ELSE %]Vul een straatnaam in[% END %]'" data-zs-spot-enlighter data-zs-spot-enlighter-restrict="'[% select_street ? 'bag-street' : 'bag' %]'" data-zs-spot-enlighter-label="human_identifier" data-zs-object-required="false">
            </div>
            <div class="bag-address-decoy" data-ng-switch="caseWebformObjectField.objects.length">
                <div data-ng-switch-when="0">
                    <input type="hidden" name="[% veldoptie_name %]"/>
                </div>
            </div>
        </div>
    [% ELSIF veldoptie_type == 'valuta' %]
    [% veldoptie_value = veldoptie_value.0 || veldoptie_value %]
    [% veldoptie_value = veldoptie_value.replace(',','.') %]
    [% veldoptie_value = veldoptie_value | format('%01.2f') %]
        <input
            class="veldoptie_[% veldoptie_type %]_simpel"
            type="text"
            [% my_veldoptie_id %]
            name="[% veldoptie_name %]"
            value="[% veldoptie_value.replace('\.', ',') | html_entity %]"
         /> <span class="veld-uitleg">Voorbeeld: 500,00</span>
    [% ELSIF veldoptie_type.match('valutain') || veldoptie_type.match('valutaex') %]
    [% veldoptie_value = veldoptie_value.0 || veldoptie_value %]
    [% valutasplitted = veldoptie_value.split('\.'); %]
        <div class="veldoptie_valuta">
            <input
                type="hidden"
                name="[% veldoptie_name %]"
                value="[% veldoptie_value | html_entity %]"
                class="[% veldoptie_type %] eur"
             />
            [% c.loc('&euro;') %] <input
                size="10"
                type="text"
                [% my_veldoptie_id %]
                name="eur_[% veldoptie_name %]"
                value="[% valutasplitted.0 %]"
                class="[% veldoptie_type %] eur ezra-disable_fieldUpdate"
             />
            <input
                size="2"
                type="text"
                name="cnt_[% veldoptie_name %]"
                value="[% valutasplitted.1 %]"
                class="[% veldoptie_type %] cnt ezra-disable_fieldUpdate"
             />
             <span class="veldoptie_valuta_calculation nojshide veld-uitleg">
                [% (veldoptie_type.match('valutain') ? 'excl.' : 'incl.') %]
                [% c.loc('VAT &euro;') %]
                <span class="value [% (
                    veldoptie_type.match('valutain') ? 'inclbtw' : 'exclbtw')
                %] btw[% (
                    veldoptie_type.match('valuta[inex]+(\d+)')
                        ? veldoptie_type.match('valuta[inex]+(\d+)').0
                        : '19'
                ) %]"></span>
            </span>
        </div>
    [% ELSIF veldoptie_type == 'select' || veldoptie_type == 'checkbox' || veldoptie_type == 'option' %]
        [% PROCESS widgets/general/veldoptie/multioption.tt %]
    [% ELSIF veldoptie_type == 'file' %]
        [% PROCESS widgets/general/veldoptie/file.tt %]

    [% ELSIF veldoptie_type == 'calendar' %]
        [% PROCESS widgets/general/veldoptie/qmatic.tt
            qmatic_can_edit = 1
        %]
    [% ELSIF veldoptie_type == 'googlemaps' %]
        [% PROCESS widgets/general/map.tt
            MAP_INPUT       = veldoptie_name
            MAP_TEMPLATE    = 'address'
            MAP_CSSPROFILE  = 'smallfixed'
            MAP_FIXEDWIDTH  = 1
            MAP_LOCATION    = veldoptie_value.0 || veldoptie_value
            MAP_CENTER      = c.config.gemeente.latitude _ ' ' _ c.config.gemeente.longitude
            MAP_FORM        = 1
            my_veldoptie_id = my_veldoptie_id
        %]
    [% ELSIF veldoptie_type == 'objecttype' %]
        [% PROCESS widgets/general/veldoptie/objecttype.tt %]
    [% END %]

[% END %]


[% IF veldoptie_multiple %] 
    [% PROCESS veldoptie_multiple %]
[% ELSE %]
    [% PROCESS veldoptie_single %]
[% END %]

