[% USE Scalar %]

<!-- Coördinator wijzigen -->
[% INCLUDE widgets/betrokkene/select.tt 
    instance_id = 'bulk_update_coordinator'
    input_name = 'coordinator_id'
    allow_internal = 1
    betrokkene_type = 'medewerker'
    label = 'Coordinator'
%]
<!-- einde Coordinator wijzigen -->

<!-- Aanvrager wijzigen -->
[% INCLUDE widgets/betrokkene/select.tt 
    instance_id = 'bulk_update_aanvrager'
    input_name = 'aanvrager_id'
    allow_internal = 1
    allow_external = 1
    label = 'Aanvrager'
%]
<!-- einde Aanvrager wijzigen -->

<tr>
    <td>Registratiedatum:</td>
    <td><input type="text" name="registratiedatum" class="veldoptie_datepicker" id="dp_bulk_registratiedatum" /></td>
    <td>[% PROCESS widgets/general/validator.tt %]</td>
</tr>
<tr>
    <td>Streefafhandeldatum:</td>
    <td><input type="text" name="streefafhandeldatum" class="veldoptie_datepicker" id="dp_bulk_streefafhandeldatum" /></td>
    <td>[% PROCESS widgets/general/validator.tt %]</td>
</tr>

<tr>
    <td>Specifieke kenmerken:</td>    
    <td>
        [% PROCESS widgets/general/simple_table.tt %]
    </td>
	<td>&nbsp;</td>
</tr>

<!-- Afdeling wijzigen -->
<tr>
    <td class="td-label">
        Wijzig afdeling
    </td>
    <td class="opties_blok_actie opties_blok_afdeling">
        <input type="hidden" name="change_only_route_fields" value="1" />
        <input type="hidden" name="wijzig_route_force" value="1" />
        [% PROCESS widgets/general/auth_select.tt
            AUTH_SELECT_COMPLETE_AFDELING = 1
            AUTH_SELECT_NO_GLOBAL = 0
            AUTH_SELECT_NO_ROLE = 0
            AUTH_SELECT_OU_NAME   = 'ou_id'
            #AUTH_SELECT_OU_VALUE  = zaak.route_ou
            AUTH_SELECT_ROLE_NAME = 'role_id'
            #AUTH_SELECT_ROLE_VALUE  = zaak.route_role
        %]
    </td>

    <td>
        [% PROCESS widgets/general/validator.tt %]
        [% PROCESS widgets/general/tooltip.tt
            message = 'Wijzig hier de huidige afdeling en rol'
        %]
    </td>
    
</tr>
<!-- einde Afdeling wijzigen -->


<!-- Zaaktype wijzigen -->
[% PROCESS zaaktype/select.tt
    trigger = ''
    hide_remember_zaaktype = 1
%]
<!-- Einde Zaaktype wijzigen -->


<!-- Vernietigingsdatum wijzigen -->
<tr>
    <td class="td-label">
        Vernietigingsdatum wijzigen
    </td>
   <td class="opties_blok_actie opties_blok_vernietigingsdatum">
        <div class="huidige-vernietigingsdatum">Huidige vernietigingsdatum: [% (zaak.vernietigingsdatum.dmy || 'Onbekend') %]</div>
        <div class="nieuwe-vernietigingsdatum">
            <div>
                <input type="radio"
                name="vernietigingsdatum_type"
                value=""
                id="vernietigingsdatum_geen"
                checked="checked"
                />
                <label
                for="vernietigingsdatum_geen">
                Ongewijzigd
                </label>
            </div>
            <div>
                <input type="radio" name="vernietigingsdatum_type" value="termijn" id="vernietigingsdatum_termijn" />
                <label
                for="vernietigingsdatum_termijn">
                    Nieuwe vernietigingsdatum:
                    [% PROCESS widgets/general/veldoptie.tt
                        veldoptie_name      = 'vernietigingsdatum'
                        veldoptie_type      = 'date'
                    %]
                </label>
            </div>
            [% IF zaak.is_afgehandeld %]
            <div>
                <input type="radio" name="vernietigingsdatum_type" value="bewaren" id="vernietigingsdatum_bewaren" />
                <label
                for="vernietigingsdatum_bewaren">Bewaren
                </label>
            </div>
            [% END %]
        </div>
        <div>
            <select name="vernietigingsdatum_reden">
                <option value="In belang van de aanvrager">In belang van de aanvrager</option>
                <option value="Uniek of bijzonder karakter voor de organisatie">Uniek of bijzonder karakter voor de organisatie</option>
                <option value="Bijzondere tijdsomstandigheid of gebeurtenis">Bijzondere tijdsomstandigheid of gebeurtenis</option>
                <option value="Beeldbepalend karakter">Beeldbepalend karakter</option>
                <option value="Samenvatting van gegevens">Samenvatting van gegevens</option>
                <option value="Betrokkene(n) is van bijzondere betekenis geweest">Betrokkene(n) is van bijzondere betekenis geweest</option>
                <option value="Vervanging van stukken bij calamiteit">Vervanging van stukken bij calamiteit</option>
                <option value="Aanleiding van algemene regelgeving">Aanleiding van algemene regelgeving</option>
                <option value="Verstoring van logische samenhang">Verstoring van logische samenhang</option>
            </select>
        </div>
    </td>
    <td>
        [% PROCESS widgets/general/validator.tt %]
        [% PROCESS widgets/general/tooltip.tt
            message = 'Wijzig de vernietigingstermijn van de zaak'
        %]
    </td>
</tr>

<!-- Einde Vernietigingsdatum wijzigen -->

<!-- Fase Wijzigen -->
<tr>
    <td class="td-label">
        Fase wijzigen
    </td>
   <td class="opties_blok_actie">
        <select name="milestone">
            [% fasen = zaak.scalar.fasen.scalar.search %]
            [% WHILE (fase = fasen.next) %]
                [% NEXT IF fase.status == 1 %]
                [% NEXT IF fase.status > (zaak.milestone + 1) %]
                <option
                [% (fase.status == (zaak.milestone + 1)
                    ? 'selected="selected"'
                    : ''
                ) %]
                value="[% (fase.status - 1) %]">[% fase.fase %]</option>
            [% END %]
            [% IF zaak.is_afgehandeld %]
                <option value=""
                selected="selected">[Afgehandeld]</option>
            [% END %]
        </select> <small>(Registratiefase is niet beschikbaar)</small>
    </td>
    <td>
        [% PROCESS widgets/general/validator.tt %]
        [% PROCESS widgets/general/tooltip.tt
            message = 'Zet zaak terug naar gewenste fase'
        %]
    </td>
</tr>
<!-- Einde fase wijzigen -->

<!-- status wijzigen -->
<tr>
    <td class="td-label">
        Status wijzigen
    </td>
    <td class="opties_blok_actie">
        <select name="status">
            <option value="" selected="selected">Niet wijzigen</option>
            [% FOREACH status IN ZCONSTANTS.zaken_statussen %]
                [% NEXT IF status == 'deleted' %]
                <option value="[% status %]">[% status %]</option>
            [% END %]
        </select>
    </td>
    <td>                
        [% PROCESS widgets/general/validator.tt %]
        [% PROCESS widgets/general/tooltip.tt
            message = 'Wijzig de interne status van deze zaak'
        %]
    </td>
</tr>
<!-- einde status wijzigen -->
