[% BLOCK regel_uitleg %]

[%- SET first_condition = 1 %]
[% FOREACH vw IN regel.voorwaarden %]
    [%- IF first_condition %]
        Als
        [% SET first_condition = 0 %]
    [%- ELSE %]
        [% IF regel.condition_type == 'and' %]en[% ELSE %]of[% END %]
    [%- END %]

    [%- km = 'voorwaarde_' _ vw _ '_kenmerk' %]
    [%- IF regel.$km == 'aanvrager_postcode' %]
        [% kv1 = 'voorwaarde_' _ vw _ '_value_postcode_from' %]
        [% kv2 = 'voorwaarde_' _ vw _ '_value_postcode_to' %]
        postcode van aanvrager tussen <strong>[% regel.$kv1 %]</strong> en <strong>[% regel.$kv2 %]</strong> ligt
    [%- ELSE %]
        [%- kv = 'voorwaarde_' _ vw _ '_value' %]
        kenmerk 
        <strong>[% PROCESS kenmerk_naam kenmerk_id=regel.$km %]</strong>
        gelijk is aan &quot;<strong>[% regel.$kv.list.join("</strong>&quot; of &quot;<strong>") %]</strong>&quot;
    [%- END %]
[%- END %]
dan:
[%- IF regel.acties.size %]
    [%- FOREACH act IN regel.acties %]
        [%- INCLUDE regel_actie regel=regel prefix='actie_' _ act %]
        [% IF loop.size - loop.count == 1 %] en [% ELSIF !loop.last() %], [% END %]
    [%- END %]
[%- ELSE %]
    <strong>doe niks</strong>
[%- END %]
[%- IF regel.anders.size %]
    ; anders
    [% FOREACH act IN regel.anders %]
        [% INCLUDE regel_actie regel=regel, prefix='ander_' _ act %]
        [% IF loop.size - loop.count == 1 %] en [% ELSIF !loop.last() %], [% END %]
    [%- END %]
[%- END %]

[%# USE Dumper; GET '<pre>'; Dumper.dump(regel); GET prefix; GET '</pre>'; %]

[% END %]

[% BLOCK kenmerk_naam %]
    [%-
     SET regel_kenmerk_naam = '(onbekend ' _ kenmerk_id _ ')';

        IF kenmerk_id == 'contactchannel'; SET regel_kenmerk_naam='contactkanaal';
     ELSIF kenmerk_id == 'payment_status'; SET regel_kenmerk_naam='betaalstatus';
     ELSIF kenmerk_id == 'confidentiality'; SET regel_kenmerk_naam='vertrouwlijkheid';
     ELSIF kenmerk_id == 'vertrouwelijkheidsaanduiding'; SET regel_kenmerk_naam='vertrouwelijkheidsaanduiding';
     ELSIF kenmerk_id == 'beroep_mogelijk'; SET regel_kenmerk_naam='bezwaar en beroep mogelijk';
     ELSIF kenmerk_id == 'publicatie'; SET regel_kenmerk_naam='publicatie';
     ELSIF kenmerk_id == 'bag'; SET regel_kenmerk_naam='BAG';
     ELSIF kenmerk_id == 'lex_silencio_positivo'; SET regel_kenmerk_naam='lex silencio positivo';
     ELSIF kenmerk_id == 'opschorten_mogelijk'; SET regel_kenmerk_naam='opschorten mogelijk';
     ELSIF kenmerk_id == 'verlenging_mogelijk'; SET regel_kenmerk_naam='verlenging mogelijk';
     ELSIF kenmerk_id == 'wet_dwangsom'; SET regel_kenmerk_naam='wet dwangsom';
     ELSIF kenmerk_id == 'wkpb'; SET regel_kenmerk_naam='WKPB';
     ELSIF kenmerk_id.match('[0-9]+');
        FOREACH mkenmerki IN milestone.elementen.kenmerken.keys.nsort;
            mkenmerk = milestone.elementen.kenmerken.$mkenmerki;
            IF mkenmerk.bibliotheek_kenmerken_id != kenmerk_id;
                NEXT;
            END;

            SET regel_kenmerk_naam = mkenmerk.naam || mkenmerk.label;
        END;
     ELSE; SET regel_kenmerk_naam = regel.$km;
     END;

     GET regel_kenmerk_naam;
    %]
[% END %]

[% BLOCK regel_actie %]
    [%- SET actie_km = prefix _ '_kenmerk' %]
    [%- SET actie_val = prefix _ '_value' %]
    [%- IF regel.$prefix == 'show_text_block' %]
        <strong>toon</strong> tekstblok &quot;<strong>[% regel.$actie_km %]</strong>&quot;
    [%- ELSIF regel.$prefix == 'hide_text_block' %]
        <strong>verberg</strong> tekstblok &quot;<strong>[% regel.$actie_km %]</strong>&quot;
    [%- ELSIF regel.$prefix == 'show_group' %]
        <strong>toon</strong> groep &quot;<strong>[% regel.$actie_km %]</strong>&quot;
    [%- ELSIF regel.$prefix == 'hide_group' %]
        <strong>verberg</strong> groep &quot;<strong>[% regel.$actie_km %]</strong>&quot;
    [%- ELSIF regel.$prefix == 'toon_kenmerk' %]
        <strong>toon</strong> kenmerk &quot;<strong>[% PROCESS kenmerk_naam kenmerk_id=regel.$actie_km %]</strong>&quot;
    [%- ELSIF regel.$prefix == 'verberg_kenmerk' %]
        <strong>verberg</strong> kenmerk &quot;<strong>[%  PROCESS kenmerk_naam kenmerk_id=regel.$actie_km %]</strong>&quot;
    [%- ELSIF regel.$prefix == 'vul_waarde_in' %]
        <strong>vul</strong> kenmerk <strong>[% PROCESS kenmerk_naam kenmerk_id=regel.$actie_km %]</strong> met waarde &quot;<strong>[% regel.$actie_val | truncate(15) | html %]</strong>&quot;
    [%- ELSIF regel.$prefix == 'vul_waarde_in_met_formule' %]
        <strong>vul</strong> kenmerk <strong>[% PROCESS kenmerk_naam kenmerk_id=regel.$actie_km %]</strong> met formule &quot;<strong>[% regel.$actie_val | truncate(15) | html %]</strong>&quot;
    [%- ELSIF regel.$prefix == 'pauzeer_aanvraag' %]
        <strong>pauzeer aanvraag</strong>
    [%- ELSIF regel.$prefix == 'toewijzing' %]
        <strong>wijzig toewijzing</strong> naar 
        [%- SET role = c.model('DB::Roles').find(regel.role_id) %]
        [%- SET ou = c.model('DB::Group').find(regel.ou_id) %]
        [%- IF ou && role %]
            <strong>[% ou.name | html %] / [% role.name | html %]</strong>
        [%- ELSE %]
            <strong>onbekende groep/rol</strong>
        [%- END %]
    [%- ELSIF regel.$prefix == 'case_result' %]
        <strong>zet resultaat</strong>
    [%- ELSIF regel.$prefix == 'wijzig_registratiedatum' %]
        [%- SET regel_source = prefix _ '_datum_bibliotheek_kenmerken_id' %]
        <strong>wijzig registratiedatum</strong>
        naar de waarde van
        <strong>[% PROCESS kenmerk_naam kenmerk_id=regel.$regel_source %]</strong>
    [%- ELSIF regel.$prefix == 'wijzig_afhandeltermijn' %]
        [% SET termijn = prefix _ '_afhandeltermijn' %]
        [% SET termijn_type = prefix _ '_afhandeltermijntype' %]
        <strong>wijzig afhandeltermijn</strong>
        naar
        <strong>[% regel.$termijn %] [% regel.$termijn_type %]</strong>
    [%- ELSIF regel.$prefix == 'sjabloon_genereren' %]
        <strong>genereer sjabloon</strong>
        <strong>
        [%
            SET sjabloon_index = prefix _ '_sjabloon';
            FOREACH sjabloon IN milestone.elementen.sjablonen;
                NEXT IF sjabloon.key != regel.$sjabloon_index;

                IF sjabloon.value.naam;
                    GET sjabloon.value.naam;
                ELSE;
                    GET '(onbekend)';
                END;
            END;
        %]
        </strong>
    [%- ELSIF regel.$prefix == 'schedule_mail' %]
        <strong>plan email in</strong>
        van type
        <strong>
        [%
            SET notificatie_index = prefix _ '_notificatie_index';
            FOREACH notificatie IN milestone.elementen.notificaties;
                NEXT IF notificatie.key != regel.$notificatie_index;

                IF notificatie.value.bibliotheek_notificaties_id;
                    GET c.model('DB::BibliotheekNotificaties').find(notificatie.value.bibliotheek_notificaties_id).label;
                ELSE;
                    GET '(onbekend)';
                END;
            END;
        %]
        </strong>
    [%- ELSIF regel.$prefix == 'add_subject' %]
        <strong>voeg betrokkene toe</strong>
        met rol
        <strong>
        [%
            SET betrokkene_uuid = prefix _ '_rule_uuid';
            FOREACH relatie IN milestone.elementen.standaard_betrokkenen;
                NEXT IF relatie.value.uuid != regel.$betrokkene_uuid;

                GET relatie.value.rol _ "</strong> en naam <strong>" _ relatie.value.naam;
            END;
        %]
        </strong>
    [%- ELSIF regel.$prefix == 'start_case' %]
        <strong>start zaak</strong>
        van type
        <strong>
        [%
            SET case_index = prefix _ '_case_index';
            FOREACH relatie IN milestone.elementen.relaties;
                NEXT IF relatie.key != regel.$case_index;

                IF relatie.value.relatie_naam;
                    GET relatie.value.relatie_naam;
                ELSE;
                    GET '(onbekend)';
                END;
            END;
        %]
        </strong>
    [%- ELSIF regel.$prefix == 'send_external_system_message' %]
        [%- SET regel_type = prefix _ '_type' %]
        <strong>stuur extern systeembericht</strong>
        naar
        [% SET interface = c.model('DB::Interface').find(regel.$regel_type) %]
        <strong>[% interface ? interface.name : '(onbekende koppeling)' %]</strong>
    [%- ELSIF regel.$prefix == 'set_online_payment' %]
        [%- SET regel_status = prefix _ '_status' %]
        schakel internetkassa <strong>[% regel.$regel_status ? 'in' : 'uit' %]</strong>
    [%- ELSE %]
        <strong>[% regel.$prefix %]</strong>
    [%- END %]
[% END %]

[% BLOCK milestone_table_row %]
<tr
    class="mijlpaal_element_rij ezra_table_row[% (!mregel ? '_template' : '') %][% mregel.is_group ? ' ezra_regels_grouping_group mijlpaal_element_rij_groep' : ''%]"
    id="ezra_table_regel_row_number[% (mregel ? '_' _ mregeli : '') %]">
    <td class="td15 ver_center no-border drag"></td>
    <td class="td_14px tdvar">

        <span class="regel-actief-checkbox">
            <input type="hidden" name="ezra_checkboxes" value="params.status.regels.active[% (mregel ? '.' _ mregeli : '') %]" />
            [% UNLESS mregel.is_group %]
            <input type="checkbox" name="params.status.regels.active[% (mregel ? '.' _ mregeli : '') %]" value="1"[% (!mregel || mregel.active) ? ' checked="checked"' : '' %] />
            [% END %]
        </span>

        <input type="hidden" class="roworder" name="params.status.regels.mijlsort[% (mregel ? '.' _ mregeli : '') %]" value="[% mregeli %]" />

        <span class="rownaam" />
        [% FOREACH problem IN integrity.rules.problems %]
            [% IF problem.rule_number == mregeli %]
                <i
                class="icon-warning-sign icon-font-awesome regel-error"
                data-zs-title="[% problem.message %]"></i>
            [% END %]
        [% END %]
        [% mregel.naam %][%# Dumper.dump(mregel) %]
        </span>

        [% IF mregel.is_group && mregel.help %]
            <div class="rowdescription">[% mregel.help %]</div></span>
        [% END %]
        [% UNLESS mregel.is_group %]
        <br>
        <span class="regel-beschrijving">[% INCLUDE regel_uitleg regel=mregel %]</span>
        [% END %]
    </td>
    <td class="row-actions td100">
        <a
            href="[% formaction _ '/regel' _ (mregel.is_group ? 'group' : '') %]/bewerken"
            class="edit ezra_table_row_edit row-action"
            title="Regel bewerken"
            rel="ezra_dialog_layout: fullscreen;[% mregel.is_group ? ' callback: ezra_regels_grouping_update_row;' : '' %]">
            <i class="icon-font-awesome icon-pencil"></i>
        </a>
        <a href="#" class="ezra_table_row_del row-action">
            <i class="icon-font-awesome icon-remove"></i>
        </a>
    </td>
</tr>
[% END %]



<h3 class="ui-accordion-header ui-state-default ui-helper-reset" id="milestone_acc_regel">
    <a href="#">
        [% IF integrity.rules.failed %]
            <i
            class="icon-warning-sign icon-font-awesome regel-error"
            data-zs-title="Zaaksysteem heeft enkele problemen gevonden binnen uw regels"></i>
        [% END %]
        Regels
        (<span class="number">[% milestone.elementen.regels.size || 0 %]</span>)
    </a>
</h3>
<div class="ezra_table element_tabel_regel mijlpaal_element">
    <div class="ui-accordion-inner">
    <table cellspacing="0" class="ezra_table_table sortable-element table-generic">
        <tbody>
            [% INCLUDE milestone_table_row %]
            [% FOREACH mregeli IN milestone.elementen.regels.keys.nsort %]
                [% mregel = milestone.elementen.regels.$mregeli %]
                [% INCLUDE milestone_table_row %]
            [% END %]
        </tbody>
    </table>
    <a  class="ezra_table_row_add"
        href="#"
        style="display: none;"
        title="Regel bewerken"
        rel="callback: ezra_zaaktype_regel_edit; newfddcallback:regel"
        ></a>
        <div class="mijlpaal_element_add_group">
        <a
            id="voegtoe"
            title="Voeg groep toe"
            class="ezra_regels_grouping_add button button-primary"
            href="[% formaction _ '/regelgroup/bewerken' %]"
            rel="callback: ezra_regels_grouping_add_dialog"
            >Voeg groep toe</a>
        </div>
    </div>
</div>
