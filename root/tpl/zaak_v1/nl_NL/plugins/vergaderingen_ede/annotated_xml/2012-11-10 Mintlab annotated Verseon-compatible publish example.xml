<?xml version="1.0" encoding="utf-8" ?>
<!--

  Note:

  - Each XML file only contains one single Vergadering. Depending on the XML document's root node either being
    <publish /> or <unpublish />, the items referenced or described in the XML document are added to the database 
    or removed from it.
  
  - When a change occurs to a Vergadering or one or more of its children, then:
    * The entire Vergadering including all children needs to be unpublished.
    * The entire updated Vergadering including all updated children need to be republished.
    * Unpublishing and republishing is done in separate XML files. The current file is a (re)publishing file.
    * Files are processed in alphabetical order of file names.
    * In alphabetical sorting of file names, the <unpublish /> XML file appears before the <publish /> file.
    
      (Means: In numeric filenames, as used in Gemeente Heemstede, where XML file names are composed of 24 digits,
      a <publish /> file's corresponding <unpublish /> file has a *pre*ceding number as its file name.)
      
      This way, it is ensured that the record is first unpublished before the updated version is published.

  - Portefeuillehouder is misspelled as <Portefeulliehouder />. This is intended. It needs to be kept this way. :)

  -->

<!-- 

  Items within the 'publish' root node inside a publishing XML file are created or updated. 
  
  -->
<publish>
  
  <!--
  
    Per XML file, the 'publish' or 'unpublish' root node contains one single 'Vergaderingen_Bestuurlijk' node.
    The 'Vergaderingen_Bestuurlijk' node represents - despite its plural name - a single Vergadering.
    
    Attributes:
      id: The Vergadering is identified by this attribute's value. The value needs to be unique within the Gemeente's Vergaderingen. (GUID, required)
      access: An access group can be assigned to the item. As this feature is not yet fully implemented, possible group names
              can still be defined in collaboration with Gemeente Ede. Basic access groups could be "public" and "internal" or "restricted".
              Finer grained access, e.g. for members of different Commissies is possible as well.
              Decision depends on precise feature wishes of Gemeente Ede. (string, required)
      
    -->
  <Vergaderingen_Bestuurlijk id="948B1EEE-5567-4C30-9808-4A4D319FC0CC" access="public">
    
    <!-- 
    
      The 'Vergaderingen_Bestuurlijk' node contains one single 'datum' node. (empty [attribute-only], required)
      
      Attributes:
        displayValue: The date at which the Vergadering took place or will take place, in Dutch date format DD-MM-YYYY. (date [DD-MM-YYYY], required)
        
      -->
    <datum displayValue="23-10-2012" />
    
    <!-- 
    
      The 'Vergaderingen_Bestuurlijk' node contains one single 'tijdstrip' node.
      As node value, it contains the time at which the Vergadering took palce or will take place, in 24h format HH:MM. (time [HH:MM], required)
      
      -->
    <tijdstip>09:00</tijdstip>
    
    <!-- 
    
      The 'Vergaderingen_Bestuurlijk' node contains one single 'lokatie' node.
      As node value, it contains the place at which the Vergadering took place or will take place. (string, required)
      
      -->
    <lokatie>Raadhuis</lokatie>
    
    <!--
    
      The 'Vergaderingen_Bestuurlijk' node contains one single 'vergaderklasse' node.
      As node value, it contains the name of the Vergadering's Vergaderklasse (e.g. Commissie). (string, required)
    
      Attributes:
        access: An access group can be assigned to the item. As this feature is not yet fully implemented, possible group names
                can still be defined in collaboration with Gemeente Ede. Basic access groups could be "public" and "internal" or "restricted".
                Finer grained access, e.g. for members of different Commissies is possible as well.
                Decision depends on precise feature wishes of Gemeente Ede. (string, required)
      -->
    <vergaderklasse access="public">Vergadering BW</vergaderklasse>
    
    <!--
    
      The 'Vergaderingen_Bestuurlijk' node may contain one single 'Behandelvoorstel_en_Agendapunten' container node. (optional)
      This node is the container for all Agendapunten of the Vergadering, which are represented by child nodes of the same name.
    
      -->
    <Behandelvoorstel_en_Agendapunten>
      
      <!--
      
        The 'Behandelvoorstel_en_Agendapunten' container node may contain one or multiple 'Behandelvoorstel_en_Agendapunten' item nodes. (optional)
        
        Attributes:
          orderNr: The Volgnummer of the Agendapunt within the Vergadering's Agendapunten. The Volgnummer is mostly numeric. It can contain letters, through, as e.g. '9a'. (string, maxlength 255, required)
          access: An access group can be assigned to the item. As this feature is not yet fully implemented, possible group names
                  can still be defined in collaboration with Gemeente Ede. Basic access groups could be "public" and "internal" or "restricted".
                  Finer grained access, e.g. for members of different Commissies is possible as well.
                  Decision depends on precise feature wishes of Gemeente Ede. (string, required)
              
        -->
      <Behandelvoorstel_en_Agendapunten orderNr="1" access="public">
        
        <!--
        
          A 'Behandelvoorstel_en_Agendapunten' item node contains a single 'regdatum' node. (empty [attribute-only], required)
          
          Attributes:
            displayValue: The 'displayValue' attribute contains the registration date of the Agendapunt in the Dutch date format DD-MM-YYYY. (date [DD-MM-YYYY], required)
        
          -->
        <regdatum displayValue="2-10-2012" />
        
        <!--
        
          A 'Behandelvoorstel_en_Agendapunten' item node may contain a single 'registratiekenmerk' node. (string, maxlength 255, optional)
          The node value is used for internal purposes of the gemeente's administration.
        
          -->
        <registratiekenmerk>589026</registratiekenmerk>
        
        <!--
        
          A 'Behandelvoorstel_en_Agendapunten' item node contains a single 'onderwerp' node. (string, maxlength 65535, required)
          As node value, it contains the Agendapunt's subject.
        
          -->
        <onderwerp>Subsidieverzoek project NH-POP Live 2013-2014, Stichting NH-POP</onderwerp>
        
        <!--
        
          A 'Behandelvoorstel_en_Agendapunten' item node contains a single 'EDMSdocmanid' node. (GUID, required)
          The Agendapunt is identified by node's value. The value needs to be unique within the Gemeente's Agendapunten.
        
          -->
        <EDMSdocmanid>C6F94CBF-2A0B-4B76-86BC-806A50162B12</EDMSdocmanid>
        
        <!--
        
          A 'Behandelvoorstel_en_Agendapunten' item node may contain a single 'document' node. (empty [attribute-only], optional)
          
          Attributes:
            file: The 'file' attribute contains the name of a file describing the Agendapunt's contents. (string, maxlength 255, required)
        
          -->
        <document file="6a2347b3-5349-4c7a-babd-5a54ccbdaef6.doc" />
        
        <!--
        
          A 'Behandelvoorstel_en_Agendapunten' item node may contain any of the following nodes:
          
          - A 'BiInleiding' node. (Introduction)
          - A 'BiSamenvatting' node. (Summary)
          - A 'BiBeslBW' node. (Decision of Commissie B&W)
          - A 'BiResultaatCom' node. (Result of Commissie)
          - A 'BiConcBeslRaad' node. (Concept Decision Raad)
          - A 'BiBeslRaad' node. (Decision Raad)
          - A 'BiMotivering' node. (Rationale)
          - A 'BiFinGevolg' node. (Financial Effect)
          - A 'BiJurGevolg' node. (Juridical Effect)
          - A 'BiPlanning' node. (Execution Planning)
          
          The text is stored in text/rtf or text/plain format. (string, maxlength 65535, optional)
        
          -->
        <BiInleiding>{\rtf1\ansi\ansicpg1252\deff0\deflang1043{\fonttbl{\f0\fdecor\fcharset0 Arial;}{\f1\fnil\fcharset0 Arial;}}{\colortbl ;\red0\green0\blue0;}\viewkind4\uc1\pard\cf1\f0\fs20 Inleiding... \par \cf0\f1 \par }</BiInleiding>
        <BiSamenvatting>{\rtf1\ansi\ansicpg1252\deff0\deflang1043{\fonttbl{\f0\fdecor\fcharset0 Arial;}{\f1\fnil\fcharset0 Arial;}}{\colortbl ;\red0\green0\blue0;}\viewkind4\uc1\pard\cf1\f0\fs20 Recentelijk is een subsidieverzoek voor in totaal \'80 2.000 ontvangen van de Stichting NH-POP ten behoeve van het project NH-POP Live 2013 (\'80 1.000) en NH-POP Live 2014 (\'80 1.000). Onderhavig verzoek voldoet niet aan het gestelde in artikel 15, lid 1, sub a van de Algemene subsidieverordening Heemstede 2009. Gelet hierop wordt voorgesteld het verzoek af te wijzen. \par \cf0\f1 \par }</BiSamenvatting>
        <BiBeslBW>{\rtf1\ansi\ansicpg1252\deff0\deflang1043{\fonttbl{\f0\fdecor\fcharset0 Arial;}{\f1\fnil\fcharset0 Arial;}}{\colortbl ;\red0\green0\blue0;}\viewkind4\uc1\pard\cf1\f0\fs20 Het subsidieverzoek van de Stichting NH-POP ten behoeve van het project NH-POP Live 2013 en 2014 op grond van artikel 15, lid 1, sub a van de Algemene subsidieverordening Heemstede 2009 af te wijzen.\par \cf0\f1 \par }</BiBeslBW>
        
        <!--
        
          Optional nodes can be added as empty strings or left out in their entirely.
        
          -->
        <BiResultaatCom />
        <BiConcBeslRaad />
        <BiBeslRaad />
        <!--
          <BiMotivering />
          <BiFinGevolg />
          <BiJurGevolg />
          <BiPlanning />
          -->
          
        <!--
        
          A 'Behandelvoorstel_en_Agendapunten' item node may contain a single 'SoortAgendapunt' node. (empty [attribute-only], optional)
          
          Attributes:
            displayValue: The 'displayValue' attribute contains the kind of the Agendapunt. (string, maxlength 255, required)
        
          -->        
        <SoortAgendapunt displayValue="Behandelvoorstel" />
        
        <!--
        
          A 'Behandelvoorstel_en_Agendapunten' item node may contain a single 'SoortAgendapunt' node. (string, maxlength 255, optional)
          As node value, it contains a keyword. ('besluitvorming', 'inspraak', '')
        
          -->
        <trefwoord>besluitvorming</trefwoord>
        
        <!--
    
          A 'Behandelvoorstel_en_Agendapunten' item node may contain one single 'Portefeulliehouder' container node. (optional)
          This node is the container for all (usually one) Portefeuillehouders of the Vergadering, which are represented by child nodes of the same name.
          
          Please note that 'Portefeuillehouder' is intentionally misspelled as 'Portefeulliehouder'.
        
          -->
        <Portefeulliehouder>
          <!--
        
          A 'Portefeulliehouder' container node may contain a single or multiple 'Portefeulliehouder' item nodes. (optional)
          
          Attributes:
            oId: The Portefeuillehouder is identified by this attribute. It needs to be unique within the Gemeente's Portefeuillehouders. (GUID, required)
                 The database is searched for this GUID and the item attached if found. 
                 If no Portefeuillehouder is found with this GUID, a new item is created using the child node data (see below). 
                 Existing Portefeuillehouders are not updated using the child node data.
                 This means that to update a Portefeuillehouder item, it needs to be unpublished first.
            access: An access group can be assigned to the item. As this feature is not yet fully implemented, possible group names
                    can still be defined in collaboration with Gemeente Ede. Basic access groups could be "public" and "internal" or "restricted".
                    Finer grained access, e.g. for members of different Commissies is possible as well.
                    Decision depends on precise feature wishes of Gemeente Ede. (string, required)
        
            -->
          <Portefeulliehouder oId="601B0456-A5F0-409B-B1A6-82116E73DE89" access="public">
            
            <!--
            
               A 'Portefeulliehouder' item node contains a single 'OOCode' node. (required)
               As node value, it contains the Portefeuillehouders's personal code. (string, maxlength 255, required)
            
              -->
            <OOCode prompt="Code" type="string">BOTTER</OOCode>
            
            <!--
            
               A 'Portefeulliehouder' item node contains a single 'OONaam' node. (required)
               As node value, it contains the Portefeuillehouders's name. (string, maxlength 255, required)
            
              -->
            <OONaam prompt="Naam" type="string">Botter, drs. J.</OONaam>
            
          </Portefeulliehouder>
          
        </Portefeulliehouder>
        
        <!--
    
          A 'Behandelvoorstel_en_Agendapunten' item node may contain one single 'Bijlagen' container node. (optional)
          This node is the container for all attachments of the Vergadering, which are represented by child nodes of four different kinds.
        
          -->
        <Bijlagen>
          
          <!--
          
            A 'Bijlagen' node may contain one or multiple 'Documenten' nodes. (optional)
            The 'Documenten' node represents a document attached to the Agendapunt.
            
            Attributes:
              oId: The document is identified by this attribute. It needs to be unique within the Gemeente's documents. (GUID, required)
                   The database is searched for this GUID and the item attached if found. 
                   If no document is found with this GUID, a new item is created using the child node data (see below). 
                   Existing documents are not updated using the child node data.
                   This means that to update a document, it needs to be unpublished first.
              access: An access group can be assigned to the item. As this feature is not yet fully implemented, possible group names
                      can still be defined in collaboration with Gemeente Ede. Basic access groups could be "public" and "internal" or "restricted".
                      Finer grained access, e.g. for members of different Commissies is possible as well.
                      Decision depends on precise feature wishes of Gemeente Ede. (string, required)
          
            -->
          <Documenten oId="1457A1E0-9E87-4B63-8801-71D100BF6EDA" access="internal">
            
            <!--
            
              A 'Documenten' node contains one single 'document' node. (empty [attribute-only], required)
              
              Attributes:
                file: The 'file' attribute contains the document's file name. (string, maxlength 255, required)
            
              -->
            <document file="a8a70bb0-1e0f-40fe-97e1-84c7940aab3d.pdf" />
            
            <!--
            
              A 'Documenten' node contains one single 'EDMSdocmanid' node. (GUID, required)
              As node value, it contains the same value as its parent's 'oId' attribute.
            
              -->
            <EDMSdocmanid>1457A1E0-9E87-4B63-8801-71D100BF6EDA</EDMSdocmanid>
            
            <!--
            
              A 'Documenten' node contains one single 'onderwerp' node. (string, maxlength 255, required)
            
              -->
            <onderwerp>Inspreektekst A. Konincks inzake zienswijze op de uitbreiding van Lombokstraat 3</onderwerp>
            
            <!--
            
              A 'Documenten' item node contains a single 'registratiekenmerk' node. (string, maxlength 255, required)
              The node value is used for internal purposes of the gemeente's administration.
            
              -->
            <registratiekenmerk>587685</registratiekenmerk>
            
          </Documenten>
          
          <!--
          
            A 'Bijlagen' node may contain one or multiple 'vergaderdocument' nodes. (optional)
            The 'vergaderdocument' node represents a document attached to the Agendapunt, in reference to the Vergadering.
            
            All attributes, child nodes and their attributes are identical to these of 'Documenten' nodes. (see above)
          
            -->
          <vergaderdocument oId="4583E788-8DA3-4C7E-8DBF-E7F42A2F1934" access="public">
            
            <document file="99e02762-5e52-4ffa-9c3d-90aad22d6183.pdf" />
            <EDMSdocmanid>4583E788-8DA3-4C7E-8DBF-E7F42A2F1934</EDMSdocmanid>
            <onderwerp>Amendement Verlichting Tennispark Groenendaal 28 mei 2009 VVd CDA D66 HBB NH (AANGENOMEN)</onderwerp>
            <registratiekenmerk>417826</registratiekenmerk>
            
          </vergaderdocument>
          
          <!--
          
            A 'Bijlagen' node may contain one or multiple 'Producten' item nodes. (optional)
            The 'Producten' node represents a product document attached to the Agendapunt.
            
            All attributes, child nodes and their attributes described for 'Documenten' nodes above also apply here.
          
            -->
          <Producten oId="66BB1B7E-C48E-474D-B176-9CD91FEF86C6" access="public">
            
            <document file="68c3b280-9ede-4ab6-bec7-160932a6e715.pdf" />
            <EDMSdocmanid>66BB1B7E-C48E-474D-B176-9CD91FEF86C6</EDMSdocmanid>
            <onderwerp>Reactie op Nota van beantwoording van de zienswijze op bestemmingsplan Woonwijken Zuidoost namens G.W.J.M. van den Putten en C.A. van den Putten-Van der Peet</onderwerp>
            <registratiekenmerk>587150</registratiekenmerk>
            
            <!--
            
              Other than 'Documenten' and 'vergaderdocument' nodes, 'Producten' nodes may contain one or multiple further 'Documenten'.
              
              All attributes, child nodes and their attributes described for 'Documenten' nodes above also apply here.
            
              -->
            <Documenten oId="10085742-77FD-44EC-8303-B29FE1397178" access="internal">
              
              <document file="11e48ee6-2df7-4712-8910-dcbbc7d13e10.pdf" />
              <EDMSdocmanid>10085742-77FD-44EC-8303-B29FE1397178</EDMSdocmanid>
              <onderwerp>Begroting 2013 Bijlagenboek</onderwerp>
              <registratiekenmerk>587956</registratiekenmerk>
              
            </Documenten>
            
          </Producten>
          
          <!--
          
            A 'Bijlagen' node may contain one or multiple 'Behandelvoorstel_en_Agendapunten' attachment item nodes. (optional)
            The 'Behandelvoorstel_en_Agendapunten' attachment item node represents an Agendapunt related to the parent Agendapunt.
            
            All attributes, child nodes and their attributes are equal to these of 'Documenten' nodes. (see above)
            
            Despite sharing their name and representing Agendapunten as well, 'Behandelvoorstel_en_Agendapunten' attachment item nodes
            are not comparable to 'Behandelvoorstel_en_Agendapunten' container nodes or 'Behandelvoorstel_en_Agendapunten' item nodes 
            described above.
          
            -->
          <Behandelvoorstel_en_Agendapunten oId="8C3F3A9B-71F4-47A5-BB92-BDB4556A1B38" access="public">
            
            <document file="6e44d181-d812-4700-9dff-4ae11e4af6d1.doc" />
            <EDMSdocmanid>8C3F3A9B-71F4-47A5-BB92-BDB4556A1B38</EDMSdocmanid>
            <onderwerp>10AR-Vaststelling bestemmingsplan "Woonwijken zuidoost"</onderwerp>
            <registratiekenmerk>584335</registratiekenmerk>
            
          </Behandelvoorstel_en_Agendapunten>
          
        </Bijlagen>
        
        <!--
    
          A 'Behandelvoorstel_en_Agendapunten' item node may contain one single 'Producten' container node. (optional)
          This node is the container for all product attachments of the Vergadering, which are represented by child nodes of four different kinds.
        
          -->
        <Producten>
          
          <!--
          
            As 'Bijlagen' container nodes described above, 'Producten' container node can contain one or multiple child nodes 
            of the following kinds:
            
            - 'Documenten' nodes.
            - 'vergaderdocument' nodes.
            - 'Producten' item nodes.
            - 'Behandelvoorstel_en_Agendapunten' attachment item nodes.
            
            For reference about these four kinds, please refer to the section about 'Bijlagen' container nodes. (see above)
          
            -->
            
        </Producten>
        
      </Behandelvoorstel_en_Agendapunten>
      
    </Behandelvoorstel_en_Agendapunten>
    
  </Vergaderingen_Bestuurlijk>
  
</publish>
