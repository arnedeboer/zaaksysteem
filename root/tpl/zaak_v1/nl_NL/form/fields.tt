[% USE Scalar %]
[% USE Dumper %]


[% BLOCK show_help %]
    [% IF kenmerk_help.length && !view_only %]
        <div class="field-help">
            [% PROCESS widgets/general/dropdown.tt
                icon = 'icon-question-sign',
                icon_type = 'icon-font-awesome',
                content_overflow = 'dropdown-content-overflow',
                dropdown_content = kenmerk_help,
                allow_click = 1,
                alt_text = 'Toelichting',
                aria_label = 'Hier volgt de toelichting van het invoerveld "' _ (kenmerk.label || kenmerk.naam) _ '": ' _ kenmerk_help
            %] </div>
    [% END %]
[% END %]


[% BLOCK field_title %]

    [% kenmerk_help = c.user_exists ? kenmerk.help : kenmerk.help_extern %]

    <div class="field-label [% IF kenmerk_help && !view_only %] field-has-help[% END %]">
        [% PROCESS show_help %]
        <label class="titel"
        [% UNLESS kenmerk.type == 'checkbox' ||
            kenmerk.type == 'file' ||
            kenmerk.type == 'option' ||
            view_only
        %]for="[% veldoptie_id %]"[% END %]>

        [% kenmerk.label || kenmerk.naam %]

        [% kenmerkvalue_present = vorig_value || (vorig_value.size > 0 && vorig_value.first) %]

        [% IF kenmerk.type == 'date' %]
             <span class="visuallyhidden">(dag-maand-jaar)</span>
        [% END %]

        [% IF !pip && kenmerk.value_mandatory && !view_only %]
            <i class="ezra_required_field kenmerk-label-verplicht" title="Dit veld is verplicht"> * </i>
        [% END %]
        </label>
    </div>
[% END %]


[% # mintloaders in this scope will upload their files here %]
[% mintloader_upload_destination = '/form' %]

[% values           = form.kenmerken %]
[% groupname        = groupname || process.current_stap %]
[% shortgroupname   = groupname.replace('[^a-zA-Z0-9]', '_'); %]
[% kenmerkgroup     = kenmerken_groups_only.$groupname %]

[% IF kenmerkgroup && !view_only  %]
<div class="stap-content" id="[% shortgroupname %]">
<div class="stap-tekst [% IF process.total_step_count == 1 %]stap-tekst-enkel[% END %]">
    <h2 class="stap-titel">[% kenmerkgroup.label %]</h2>

    [% IF kenmerkgroup.help.length %]
    <div class="stap-beschrijving">[% kenmerkgroup.help | html_para %]</div>
    [% END %]
    [% UNLESS process.current_stap == 'verify' %]
        <div class="form-required-fields-explanation">
            * = Verplicht veld
        </div>
    [% END %]
</div>

<div class="form vragen_form table_window no_margin">
[% END %]

[% count = 1; %]
[% kenmerkkeys = [] %]


[% seen_kenmerken = {} %]
[% FOREACH kenmerk IN kenmerken_groups.$groupname %]
    [% vorig_value = '' %]
    [% NEXT UNLESS kenmerk.zaak_status_id && kenmerk.zaak_status_id.status == 1 %]
    [% kenmerkkey = kenmerk.rtkey %]

    [% bibliotheek_id = kenmerk.bibliotheek_kenmerken_id.id %]

    [% # VALUE HORROR
       # 1) Check if value exists, could be an arrayref
       # 2) Check when no value exists, if the value is not a 0
    %]
    [% IF values.$bibliotheek_id || values.$bibliotheek_id.length %]
        [% vorig_value = values.$bibliotheek_id %]
    [% END %]

        [% prevgroupname = shortgroupname %]
        [% kenmerkkeys.push(kenmerkkey) %]

        [% # some types require a 'fieldset' tag %]
        [% fieldset_fieldtypes = {
            option => 1,
            checkbox => 1
        } %]
        [% is_fieldset_field = fieldset_fieldtypes.${kenmerk.type} %]
        [% row_tag = is_fieldset_field ? 'fieldset' : 'div' %]
        [% inner_row_tag = is_fieldset_field ? 'legend' : 'div' %]

        [%# IF !(regels_result.verberg_kenmerk.$bibliotheek_id) %]

        [% IF (kenmerk.type != 'text_block' && view_only) || !view_only %]
        <[% row_tag %]
            class="[% IF regels_result.active_fields.$bibliotheek_id %]regels_enabled_kenmerk[% END %] kenmerk-veld ezra_field_wrapper row ng-cloak"
            data-ng-show="[% kenmerk.is_systeemkenmerk ? 'false' : 'caseWebformField.isVisible()' %]"
            data-zs-case-webform-field
            data-zs-case-webform-field-type="[% kenmerk.type %]"
            data-field-group-id="[% shortgroupname %]"

            [% SWITCH kenmerk.type %]

            [%   CASE 'objecttype' %]
            data-zs-case-webform-field-name="object.[% kenmerk.object_id.get_object_attribute('prefix').value %]"
            data-zs-case-webform-field-id="[% kenmerk.get_column('object_id') %]"

            [%   CASE 'text_block' %]
            data-zs-case-webform-field-name="text_block_[% kenmerk.id %]"
            data-zs-case-webform-field-id="text_block_[% kenmerk.id %]"

            [%   CASE DEFAULT %]
            data-zs-case-webform-field-name="attribute.[% kenmerk.bibliotheek_kenmerken_id.magic_string %]"
            data-zs-case-webform-field-id="kenmerk_id_[% bibliotheek_id %]"

            [% END %]
        >

            [% veldoptie_id = 'field_' _ bibliotheek_id _ '_' _ count %]

            [% IF kenmerk.type == 'text_block' && !view_only %]
                <div class="field text_block">
                    [% kenmerk.properties.text_content %]
                </div>
            [% END %]

            [% UNLESS kenmerk.type == 'text_block' %]

            <[% inner_row_tag %] class="column large-4">[% PROCESS field_title %]</[% inner_row_tag %]>

            <div class="column large-8">
                
                <div class="field regels_fixed_value" data-ng-show="caseWebformField.isFixedValue()" data-ng-bind-html="caseWebformField.getFixedValueHtml()">
                    
                </div>
                    
                <div class="field" data-ng-show="!caseWebformField.isFixedValue()">
                    [% IF kenmerk.type == 'file' %]
                        [% vorig_value = form_uploaded_files.$bibliotheek_id %]
                    [% END %]

                    [% IF (kenmerk.type == 'option' || kenmerk.type == 'checkbox' || kenmerk.type == 'file') && !view_only %]
                        <input type="hidden" name="defined_kenmerk" value="[% kenmerk.rtkey %]"/>
                    [% END %]

                    [% # TODO - create a hook structure for this %]                       
                    [% IF kenmerk.magic_string == 'woz_objectnummer' %]
                        <div class="ezra_woz_in_zaaktype woz-in-zaaktype">
                            <input type="hidden" name="[% kenmerk.rtkey %]" value="[% vorig_value %]"/>
                            [%  woz_object = c.model('DB::WozObjects').find({
                                    object_id => vorig_value
                                });
                            %]
                            [% woz_object_data = woz_object.object_data %]
                            [% PROCESS betrokkene/woz_object_view.tt %]
                        </div>
                    [% ELSE %]
                        [% field_template = 'widgets/general/' _ (view_only ? 'veldoptie_view.tt' : 'veldoptie.tt') %]
                        [% PROCESS $field_template
                            veldoptie_id        = veldoptie_id
                            veldoptie_name      = kenmerk.rtkey
                            veldoptie_hidden    = kenmerk.is_systeemkenmerk
                            veldoptie_type      = kenmerk.type
                            veldoptie_opties    = kenmerk.options
                            veldoptie_value     = vorig_value
                            veldoptie_bibliotheek = kenmerk.bibliotheek_kenmerken_id
                            veldoptie_zaaktype_kenmerk = kenmerk
                            veldoptie_multiple       = kenmerk.bibliotheek_kenmerken_id.type_multiple
                            veldoptie_label_multiple = kenmerk.label_multiple
                            veldoptie_properties = kenmerk.properties
                        %]
                    [% END %]
                </div>

                <div class="validator rounded">
                    <div class="validate-tip"></div>
                    <div class="validate-content rounded">
                        <span></span> Dit veld is verplicht
                    </div>
                </div>
            </div>
        [% END %]
        [% count = count+1 %]
        </[% row_tag %]>
        [% END %]



        [%  # we only want to pause the application ater all the influencing 
            # fields have been shown. so keep a hash with the seen
            # fields (kenmerken) and check every time if all the required fields
            # are visible.
        %]
        [% seen_kenmerken.$bibliotheek_id = 1 %]
        [% condition_fields = regels_result.pause_application.condition_fields %]
        [% condition_fields_complete = 1 %]
        [% FOREACH condition_field = condition_fields.keys %]
            [% UNLESS seen_kenmerken.$condition_field %]
                [% condition_fields_complete = 0 %]
            [% END %]
        [% END %]
        
    [% END %]

    [% IF c.user_exists %]
        [% IF kenmerken_groups_keep_sort.last == groupname && process.current_stap != 'contactgegevens' %]
            [% PROCESS form/behandel_acties.tt %]
        [% END %]
    [% END %]

[% IF kenmerkgroup && !view_only %]
    </div>
    <input type="hidden" name="stepfields-[% prevgroupname %]"
    value="[% kenmerkkeys.join(',') %]" />
</div>
[% END %]

[% IF process.current_stap == 'contactgegevens' %]
    [% PROCESS form/contact.tt %]
[% END %]

