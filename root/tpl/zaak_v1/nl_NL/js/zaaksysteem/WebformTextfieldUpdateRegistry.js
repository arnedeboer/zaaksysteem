
function WebformTextfieldUpdateRegistry () {

	var AUTOSAVE_TIMEOUT = 1000;
	var phases = [];

	this.scheduleUpdate = function (element) {

		var form_id = element.closest('form.webform').attr('id');
		var fieldname = element.attr('name');

		if(!phases.hasOwnProperty(form_id)) {
			phases[form_id] = {};
		}

		if(phases[form_id].hasOwnProperty(fieldname)) {
			this.clearField(element);
		}

        var timeout_handle = setTimeout( function() {
            updateField(element);
        }, AUTOSAVE_TIMEOUT);

		phases[form_id][fieldname] = timeout_handle;
	};
	
	this.clearField = function (element) {

		var form_id = element.closest('form.webform').attr('id');
		var fieldname = element.attr('name');
		
		// clear this specific timeout
		if(phases.hasOwnProperty(form_id) && phases[form_id].hasOwnProperty(fieldname)) {

	        var timeout_handle = phases[form_id][fieldname];
	        clearTimeout(timeout_handle);

	        delete phases[form_id][fieldname];
	    }
	};

	this.clearPhase = function (form_id) {


		if(phases.hasOwnProperty(form_id)) {

			// clear all scheduled update for the phase
			for(var fieldname in phases[form_id]) {
				var timeout_handle = phases[form_id][fieldname];
				clearTimeout(timeout_handle);
			}

			// and get rid of the shit
	        delete phases[form_id];
		}

	};
}

