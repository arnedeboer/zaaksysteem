package Zaaksysteem::Metarole::AccessProfileTrait;

use Moose::Role;

use Data::Dumper;
use Zaaksysteem::Exception;

has '_security_profiles' => ( is => 'rw', isa => 'HashRef' );
has '_default_security_profile' => ( is => 'rw', isa => 'Str' );
has '_initialized' => ( is => 'rw', isa => 'Bool', default => 0 );

sub _init_class_modifiers {
    my $self = shift;

    if($self->_initialized) { return; }

    $self->_initialized(1);

    $self->add_around_method_modifier('begin', sub {
        my $orig = shift;
        my $self = shift;
        my $c = shift;

        my @check_profiles;
        my $profiles = $self->meta->_security_profiles;

        # If the action has an 'Access' attribute, use that to define which profiles to check
        if(exists $c->action->attributes->{ Access }) {
            my ($profile_spec) = @{ $c->action->attributes->{ Access } };

            @check_profiles = split/,\s*/, $profile_spec
        } elsif($self->meta->_default_security_profile) { # otherwise, check the default
            push(@check_profiles, $self->meta->_default_security_profile);
        }

        for my $profile (@check_profiles) {
            $c->log->debug(sprintf('Checking ACL for %s:%s profile', $c->action, $profile));

            next if $profile eq '*';

            unless(exists $profiles->{ $profile }) {
                throw('acl/unknown_profile', sprintf(
                    "Security profile '%s' for '%s' could not be found, please use one of the following available profiles: %s.",
                    $profile,
                    $c->action,
                    join ', ', map { "'$_'" } keys(%{ $profiles })
                ));
            }

            unless($profiles->{ $profile }->($c)) {
                throw('acl/denied', "You do not have permissions to access this action.");
            }
        }

        return $self->$orig($c, @_);
    });
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

