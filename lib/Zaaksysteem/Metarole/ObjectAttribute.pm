package Zaaksysteem::Metarole::ObjectAttribute;

use Moose::Role;
use namespace::autoclean;

use Zaaksysteem::Object::Attribute;

use Zaaksysteem::Tools;

=head1 NAME

Zaaksysteem::Metarole::ObjectAttribute - Hijack L<Moose::Meta::Attribute>s for
L<Zaaksysteem::Object> infrastructure

=head1 DESCRIPTION

This L<role|Moose::Role> is meant to be applied on Moose attributes in classes
that are intended to behave like Objects.

    package Zaaksysteem::Object::Types::SomeType;

    use Moose;

    extends 'Zaaksysteem::Object';

    has attr => (
        is => 'ro',
        isa => 'Str',
        traits => [qw[OA]],

        label => 'Attribute',
        documentation => 'Attribute description',
        required => 1,

        # Cludgy bits, passed through to Object::Attribute infra...
        data => { passed => { as => 'is' } },
        alias => [ 'kenmerk' ]                # Can also be scalar string
    );

=cut

# OA as initialism, we're probably going to be using this one all the thyme,
# so it'd better be short.
Moose::Util::meta_attribute_alias('OA');

=head1 ATTRIBUTES

=head2 label

=cut

has label => (
    isa => 'Str',
    is => 'ro',
    required => 1,
    default => sub {
        shift->name
    }
);

=head2 type

=cut

has type => (
    isa => 'Str',
    is => 'ro',
    required => 0,
    default => 'text'
);

=head2 options

XXX Temporary, until we figure out a better way to represent radio/checkbox options in hardcoded types

=cut

has options => (
    isa => 'ArrayRef[Str]',
    is => 'ro',
    required => 0,
    default => sub { [] },
);

=head2 data

=cut

has data => (
    isa => 'HashRef',
    is => 'ro',
    required => 0,
    predicate => 'has_data'
);

=head2 alias

=cut

has alias => (
    is => 'ro',
    isa => 'Str | ArrayRef[Str]',
    predicate => 'has_alias'
);

=head2 aliases

=cut

has aliases => (
    is => 'ro',
    isa => 'HashRef[CodeRef|Undef]',
    predicate => 'has_aliases'
);

has unique => (
    is        => 'ro',
    isa       => 'Bool',
    predicate => 'has_unique'
);

=head2 index

Boolean property of an Object attribute. If true, signals that the value of
the attribute should be indexed as part of the full-text search index of the
object.

=cut

has index => (
    is => 'ro',
    isa => 'Bool',
    default => 0
);

=head1 METHODS

=head2 build_object_attribute_instance

This method hides the logic of converting a L<Class::MOP::Attribute> instance
with this role into a L<Zaaksysteem::Object::Attribute> instance.

    if($object->isa('Zaaksysteem::Object')) {
        my $moose_attr = $object->find_attribute_by_name('id');
        my $zs_attr = $moose_attr->build_object_attribute_instance(
            object => $object,
            object_row => $row
        );
    }

If provided, C<object> will be used as a source of actual value data to pass
to the constructor of L<Attribute|Zaaksysteem::Object::Attribute>. All other
values are passed along to it's constructor as-is.

=cut

sub build_object_attribute_instance {
    my ($self, %params) = @_;

    my $object = delete $params{ object };
    my $reader = $self->get_read_method;
    my $type = $self->type;

    # XXX Domain-crossing workaround for Object::Attributes with design bug
    #
    # An attribute isn't really a spot-enlighter, frontend should
    # receive that information from a different level, but for now
    # we can dereference the actual type, as long as the JSON API
    # to the frontend for spot-enlighters doesn't change.
    if($type eq 'spot-enlighter') {
        unless ($self->has_data) {
            throw('object/attribute/spot-enlighter', sprintf(
                'Attribute "%s" indicates it is a spotenlighter field, but it has no data',
                $self->name
            ), $self);
        }
        $type = $self->data->{ restrict };
    }

    my %args = (
        name => $self->name,
        attribute_type => $type,
        is_systeemkenmerk => 1,
        label => $self->label
    );

    my $attr = $object->meta->find_attribute_by_name($self->name);

    # Make sure we don't create the 'value' key when no value is set
    # (prevent null/undef bugs)
    if(blessed($object)) {
        if ($attr->has_value($object)) {
            $args{ value } = $object->$reader();
        }
        elsif ($attr->has_default) {
            # Pass object instance, so that subrefs in the default receive it
            $args{ value } = $attr->default($object);
        }
    }

    if ($self->has_aliases) {
        $args{ bwcompat_name } = [ keys %{ $self->aliases } ];
    }

    if ($self->has_unique) {
        $args{ unique } = $self->unique;
    }

    return Zaaksysteem::Object::Attribute->new(%args, %params);
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

