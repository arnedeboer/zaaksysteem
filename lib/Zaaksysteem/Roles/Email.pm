package Zaaksysteem::Roles::Email;
use Moose::Role;

=head2 assert_file_from_id

Get file by ID and return the file object

=head3 ARGUMENTS

=over

=item interface

An interface object

=item ID

The file ID

=back

=head3 RETURNS

The file or dies with an error

=cut

sub assert_file_from_id {
    my ($self, $interface, $id) = @_;
    my $schema = $interface->result_source->schema;
    my $file = $schema->resultset('File')->find($id);

    if (!$file) {
        throw ("mail/nomail", "No mail found with file id $id");
    }
    return $file;
}

=head2 assert_path

Get the path of the file

=head3 ARGUMENTS

=over

=item file

A file object

=back

=head3 RETURNS

The path  or dies with an error

=cut

sub assert_path {
    my ($self, $file) = @_;
    my $path = $file->filestore->get_path;
    if (!$path || ! -f $path) {
        throw ("mail/missing path", sprintf("No mail found on disk %s with file id %s", $path // '<undefined>', $file->id));
    }
    return $path;
}

1;

__END__

=head1 NAME

Zaaksysteem::Roles::Email

=head1 DESCRIPTION

=head1 SYNOPSIS

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
