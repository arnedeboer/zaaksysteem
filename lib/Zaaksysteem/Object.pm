package Zaaksysteem::Object;

use Moose;
use Moose::Util qw[ensure_all_roles];

use DateTime;
use JSON;
use List::Util qw(first);

use Zaaksysteem::Tools;

# Load metaroles so their aliasses get registered
use Zaaksysteem::Metarole::ObjectAttribute;
use Zaaksysteem::Metarole::ObjectRelation;

use Zaaksysteem::Types qw[Timestamp];

use overload '""' => sub { shift->TO_STRING };

with 'Zaaksysteem::Object::Reference';

=head1 NAME

Zaaksysteem::Object - In-memory representation of "objects"

=head1 SYNOPSIS

    my $obj = Zaaksysteem::Object->new(
        id             => "Str",
        date_modified  => DateTime->new(...), # Defaults to DateTime->now()
        date_created   => DateTime->new(...), # Defaults to DateTime->now()
        authorizations => [],
    );

    $json = $obj->TO_JSON;

    $obj = Zaaksysteem::Object->new_from_json(
        q/{"properties": {}, authorizations: []}
    /);

=head1 DESCRIPTION

This is the base class for in-memory representations of Zaaksysteem "objects".

Its subclasses should be able to validate the contents of its properties, to
ensure the object can only serialize into JSON (or the database) if it's valid.

=head1 PUBLIC ATTRIBUTES

This is the list of attributes that get serialized to JSON, CSV and other
target formats.

=head2 date_modified

The "last-modified" timestamp. Defaults to "now".

=cut

has date_modified => (
    traits  => [qw[OA]],
    is      => 'rw',
    isa     => Timestamp,
    coerce  => 1,
    default => sub { return DateTime->now(); },
    label   => 'Laatst gewijzigd',
    type    => 'date'
);

=head2 date_created

The creation timestamp of the object. Defaults to "now".

=cut

has date_created => (
    traits  => [qw[OA]],
    is      => 'rw',
    isa     => Timestamp,
    coerce  => 1,
    default => sub { return DateTime->now(); },
    label   => 'Datum aangemaakt',
    type    => 'date'
);

=head2 actions

An array containing the authorizations for this object. That is, the names
of the actions the user accessing the object has permissions for.

=cut

has actions => (
    traits   => [qw[Array]],
    is       => 'rw',
    default => sub { return []; },

    handles => {
        add_action => 'push',
        all_actions => 'elements',
        has_action => 'first',
        map_actions => 'map'
    }
);

=head1 METHODS

=head2 attribute_instances

This attribute is an interface to a memoized list of
L<Zaaksysteem::Object::Attribute> instances.

=cut

sub attribute_instances {
    my $self = shift;

    return map { $_->build_object_attribute_instance(object => $self) }
           $self->object_attributes;
}

=head2 object_attribute

Retrieve a object attribute by name. This method filters the
L<Moose::Meta::Attribute> instances by selecting only those attributes that
have the L<Zaaksysteem::Metarole::ObjectAttribute> or
L<Zaaksysteem::Metarole::ObjectRelation> trait. Plain attributes do not exist
for L<Zaaksysteem::Object> instances, within the context of working with
those objects.

    my $attr = $object->object_attribute('my_attr');

=cut

sub object_attribute {
    my $self = shift;
    my $name = shift;

    # Backwards compatible with old attribute slugs
    if ($name =~ m[^attribute\.(.*)]) {
        $name = $1;
    }

    my $attr = $self->meta->find_attribute_by_name($name);

    return unless $attr;
    return unless grep { $attr->does($_) } qw[
        Zaaksysteem::Metarole::ObjectAttribute
        Zaaksysteem::Metarole::ObjectRelation
    ];

    return $attr;
}

=head2 object_attributes

This method returns a list of all L<Moose::Meta::Attributes> for this class,
which have the L<Zaaksysteem::Metarole::ObjectAttribute> role.

=cut

sub object_attributes {
    my $self = shift;

    return grep {
        $_->does('Zaaksysteem::Metarole::ObjectAttribute')
    } $self->meta->get_all_attributes;
}

=head2 object_relation_attributes

Returns a list of all L<Moose::Meta::Attribute> instances implementing the
L<Zaaksysteem::Metarole::ObjectRelation> trait.

=cut

sub object_relation_attributes {
    my $self = shift;

    return grep {
        $_->does('Zaaksysteem::Metarole::ObjectRelation')
    } $self->meta->get_all_attributes;
}

=head2 attribute_instance

This convenience method finds one attribute instance associated with the
object, matching on name.

=cut

sub attribute_instance {
    my $self = shift;

    # Don't use $self->object_attribute yet, build_object_attribute_instance
    # can't handle Metarole::ObjectRelation yet.
    my $mop_attr = $self->meta->find_attribute_by_name(shift);

    return unless $mop_attr && $mop_attr->does('Zaaksysteem::Metarole::ObjectAttribute');

    return $mop_attr->build_object_attribute_instance(object => $self);
}

=head2 attribute_names

Returns a list of the names of all attributes this object has.

=cut

sub attribute_names {
    my $self = shift;

    return map  { $_->name }
           grep { $_->does('Zaaksysteem::Metarole::ObjectAttribute') }
                $self->meta->get_all_attributes;
}

=head2 get_attribute_value

Returns the value of the given named attribute (independent of the actual
accessor name).

=cut

sub get_attribute_value {
    my $self = shift;

    return $self->meta->find_attribute_by_name(shift)->get_value($self);
}

=head2 type

Returns the type of the object. By default, this is the last part of the
package name, lowercased.

This works as a class method or as an instance method.

=cut

sub type {
    my $self = shift;
    my $class = ref($self) || $self;

    my $stripped_class = (split /::/, $class)[-1];

    # http://www.perlmonks.org/?node_id=639595
    # Break up CamelCaseStrings => camel_case_strings
    my @subclass_parts = $stripped_class =~ m/[A-Z](?:[A-Z]+|[a-z]*)(?=$|[A-Z])/g;

    return join '_', map { lc } @subclass_parts;
}

=head2 index_attributes

An object can have attributes that when saved to a database require search
indexes to be built. This method builds a list of names of attributes that
should be used for that purpose.

This method returns a list of names that is heuristically chosen, but it's
not very smart about it. If indexing matters, it's best to C<override> this
method in a subclass.

=cut

sub index_attributes {
    my $self = shift;

    return grep { defined && length $_ > 2 }
           map  { $_->vectorize_value }
           map  { $_->build_object_attribute_instance(object => $self) }
           grep { $_->index }
                $self->object_attributes;
}

=head2 capabilities

This method returns a list of capabilities an object has. This analogous to
actions that the frontend can execute on objects.

This package being the base class for all objects, it exports capabilities
all object inherently has, like creation, updating and deletion.

=over 4

=item read

This capability allows users to retrieve and read the contents of instances
of this object type.

=item write

This capability allows users to update the contents of instances of this
object type.

=item manage

This capability allows users to manage instances of this object type. This
capability replaces the deprecated C<delete> capability by providing a broader
capability that implies C<delete>.

=back

=cut

sub capabilities {
    return qw[read write manage];
}

=head2 relatable_types

This is an abstract method that implements relatable_types, as used by the
L<Zaaksysteem::Object::Roles::Relation> role. It returns the default relatable
type C<case>, because cases may relate to all object types.

=cut

sub relatable_types {
    return ('case');
}

=head2 is_deleteable

Returns a true value if the object can be deleted (based on relations with
other objects, mostly).

=cut

sub is_deleteable {
    return 1;
}

=head2 model_hook

Placeholder for the L<Zaaksysteem::Object::Model/"Model hooks"> feature.

=cut

sub model_hook { }

=head2 TO_JSON

Returns a data structure representing the object. L<JSON/encode_json> automatically
calls this method, but it can safely be used for other purposes.

=cut

sub TO_JSON {
    my $self = shift;

    my %values = map  { @{ $_->TO_JSON }{qw[name value]} }
                 grep { $_->has_value }
                      $self->attribute_instances;

    for my $rel ($self->object_relation_attributes) {
        # Ignore undefined relations
        next unless $rel->has_value($self);

        my $value;
        my $data = $rel->get_value($self);

        # Serialize the related objects/references
        my @value_data = map {
            # Depending on being embedded, serialize the complete related
            # object, or just a reference to it. Also, force instantiation of
            # the supplied value.
            $rel->embed ? $_->_instance->TO_JSON : {
                id => $_->id,
                type => $_->type
            }
        } ($rel->isa_set ? @{ $data } : $data);

        $values{ $rel->name } = $rel->isa_set ? \@value_data : $value_data[0];
    }

    return {
        id              => $self->id,
        type            => $self->type,
        date_modified   => $self->date_modified,
        date_created    => $self->date_created,
        actions         => $self->actions,
        label           => $self->TO_STRING,
        values          => \%values,

        # Provide keys with empty values for consistant API exposure to fronted
        relatable_types => [],
        related_objects => [],
        security_rules  => []
    };
}

=head2 TO_STRING

Generic stringification of an object. Returns object type with the last 6
hexadecimal digits of the object's UUID, or 'unsynched' when the id field
is false-ish, implying the object has no
L<ObjectData|Zaaksysteem::Backend::Object::Data::Component> counterpart.

    my $str = "$object";

    => "object(...2ea4ff)"

=cut

sub TO_STRING {
    return shift->_as_string;
}

=head2 _instance

Returns the instance itself, this is here so the instantiator logic from
L<Zaaksysteem::Object::Reference> can be safely executed on an existing object
instance without method call failures.

=cut

sub _instance {
    return shift;
}

=head2 _instantiable

Always returns true-ish. Implements logic required by
L<Zaaksysteem::Object::Reference>.

=cut

sub _instantiable { return 1; }

# Because "namespace::autoclean" would clean out our overloads, use this:
no Moose;

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

