package Zaaksysteem::Auth::Store;

use Moose;

use Zaaksysteem::Auth::User;

use JSON;

use Zaaksysteem::Profile;
use Zaaksysteem::Exception;

=head1 NAME

Zaaksysteem::Auth::Store - Find users for authentication purposes

=head1 DESCRIPTION

This class implements an authentication store compatible with the
L<Catalyst::Plugin::Authentication>-plugin.

For more information, see L<Catalyst::Plugin::Authentication::Internals>.

=head1 ATTRIBUTES

The following attributes store the arguments we receive during instantiation
of this class. They are not used internally by this class.

=head2 config

=cut

has config => (
    is => 'ro'
);

=head2 app

=cut

has app => (
    is => 'ro'
);

=head2 realm

=cut

has realm => (
    is => 'ro'
);

=head1 METHODS

=cut

# We receive our arguments non-named, wrap them as named args so Moose plays
# nicely.
around BUILDARGS => sub {
    my $orig = shift;
    my $class = shift;

    return $class->$orig(
        config => shift,
        app => shift,
        realm => shift
    );
};

=head2 find_user

This method implements a piece of the Store interface from
L<Catalyst::Plugin::Authentication>.

For more information, see L<Catalyst::Plugin::Authentication::Internals>.

=cut

define_profile find_user => (
    required => [qw[username source]],
);

sub find_user {
    my $self = shift;
    my $authinfo = assert_profile(shift)->valid;

    my $entity = shift->model('DB::UserEntity')->find({
        source_identifier   => $authinfo->{ username },
        source_interface_id => $authinfo->{ source }
    });

    return unless $entity && $entity->active;

    my $subject = $entity->subject_id;
    my $schema = $subject->result_source->schema;

    # Subject->has_many(UserEntity), make sure the buject we have here knows
    # about the specific entity that was logged in with.
    $subject->login_entity($entity);

    # <hack type="nasty">
    # Many resultsets and dbix components rely on the current user
    # being available in the schema.
    # TODO XXX Dismantle this use case
    $schema->default_resultset_attributes->{ current_user } = $schema->current_user($subject);
    # </hack>

    return $subject;
}

=head2 for_session

This method implements a piece of the Store interface from
L<Catalyst::Plugin::Authentication>.

For more information, see L<Catalyst::Plugin::Authentication::Internals>.

=cut

sub for_session {
    my $self    = shift;
    my $c       = shift;
    my $user    = shift;

    return to_json($user->TO_JSON);
}

=head2 from_session

This method implements a piece of the Store interface from
L<Catalyst::Plugin::Authentication>.

For more information, see L<Catalyst::Plugin::Authentication::Internals>.

=cut

sub from_session {
    my $self = shift;
    my $c = shift;
    my $session_auth_params = from_json(shift);

    my $entity = $c->model('DB::UserEntity')->find(
        $session_auth_params->{ login_entity_id }
    );

    return unless defined $entity && $entity->active;

    my $subject = $c->model('DB::Subject')->find(
        $session_auth_params->{ id }
    );

    return unless defined $subject;

    # If our login was via an API, make sure the subject we have here knows
    if ($session_auth_params->{is_external_api}) {
        $subject->is_external_api($session_auth_params->{is_external_api});
        $subject->external_api_objects($session_auth_params->{external_api_objects})
    }

    # Subject->has_many(UserEntity), so we make it explicitly know which one
    # was used to log in.
    $subject->login_entity($entity);

    # <hack type="nasty">
    # Many resultsets and dbix components rely on the current user
    # being available in the schema.
    # TODO XXX Dismantle this use case
    my $schema = $subject->result_source->schema;

    $schema->default_resultset_attributes->{ current_user } = $schema->current_user($subject);
    # </hack>

    return $subject;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
