package Zaaksysteem::Object::Iterator;
use Moose;
use namespace::autoclean;

use Moose::Util qw(ensure_all_roles);
use Zaaksysteem::Tools;

=head1 NAME

Zaaksysteem::Object::Iterator - Simple object-creating iterator for ObjectData resultsets

=head1 SYNOPSIS

    my $iter = Zaaksysteem::Object::Iterator->new(
        rs    => $rs,
        model => $model,
    );

    OR

    my $iter = $model->iterator($rs);

    while ( my $obj = $iter->next ) {
        print $obj->id, "\n";
    }

=head1 ATTRIBUTES

=head2 rs

The resultset to iterate over. Required.

=cut

has rs => (
    is       => 'rw',
    isa      => 'Zaaksysteem::Backend::Object::Data::ResultSet',
    required => 1,
    handles => {
        count => 'count',
        reset => 'reset',
        first => 'first',
        pager => 'pager',
        is_paged => 'is_paged',
        fields => 'object_requested_attributes'
    }
);

=head2 inflator

The inflator to convert a result provided by L</rs> in some way. Required, but
has a default implementation that returns the result unmodified.

=cut

has inflator => (
    is => 'rw',
    isa => 'CodeRef',
    traits => [qw[Code]],
    required => 1,
    handles => { inflate => 'execute' },
    default => sub {
        return sub { shift };
    }
);

=head1 METHODS

=head2 all

=cut

sub all {
    my $self = shift;

    my @results;

    $self->reset;

    while (my $result = $self->next) {
        push @results, $result;
    }

    return @results;
}

=head2 next

Return the next object from the resultset. If there aren't any more objects,
undef will be returned.

=cut

sub next {
    my $self = shift;

    my $next = $self->rs->next;
    return unless $next;

    my $object = $self->inflate($next);

    # TODO: Find a better way to do "describe rows"
    if ($self->rs->describe_rows) {
        ensure_all_roles($object, 'Zaaksysteem::Object::Roles::Describe');

        my $requested = $self->rs->object_requested_attributes;
        if (@{ $requested // [] }) {
            $object->describe_attributes($requested);
        }
    }

    return $object;
}

=head2 total_entries

=cut

sub total_entries {
    my $self = shift;

    return int(
        $self->is_paged ? $self->pager->total_entries : $self->count
    );
}

=head2 set_pager_attributes

Set the pager attributes (page number, page size) on the embedded resultset.

=cut

define_profile set_pager_attributes => (
    required => {
        current_page => 'Int',
        page_size    => 'Int',
    },
);

sub set_pager_attributes {
    my ($self, %params) = @_;
    my $args = assert_profile(\%params)->valid;

    my %options = (
        page => $args->{current_page},
        rows => $args->{page_size},
    );

    $self->rs( $self->rs->search_rs({}, \%options) );

    return $self;
}

=head2 clone

This method returns a deep-copy clone of the iterator instance.

Parameters to C<new> will default to the values of the invocant, but can be
overridden by passing them to clone directly.

=cut

sub clone {
    my ($self, %params) = @_;

    $params{ rs } //= $self->rs->search;
    $params{ inflator } //= $self->inflator;

    return $self->meta->new_object(%params);
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
