package Zaaksysteem::Object::TypeModel;

use Moose;

use Moose::Meta::Class;

use Zaaksysteem::Tools;

with 'Zaaksysteem::Log::LoggerRole';

=head1 NAME

Zaaksysteem::Object::TypeModel - Model for user-defined implicit class definitions

=head1 DESCRIPTION

    my $plantenbak = $c->model('ObjectType')->new_object('plantenbak');

=head1 ATTRIBUTES

=head2 meta_class_cache

This attribute holds a cache of ObjectTypes that have been converted into
metaclasses, keyed on the ObjectType's prefix.

=cut

has meta_class_cache => (
    is => 'rw',
    isa => 'HashRef[Moose::Meta::Class]',
    traits => [qw[Hash]],
    handles => {
        meta_class_is_cached => 'exists',
        cached_meta_class => 'accessor',
        get_cached_meta_class => 'get',
        set_cached_meta_class => 'set'
    }
);

=head1 METHODS

=head2 get_meta_class

This method will return a L<Moose::Meta::Class> instance for the given
L<Zaaksysteem::Object::Types::Type> object. It uses an internal cache so
successive calls will always return the same reference.

The cache will be keyed on the ObjectType's
L<prefix|Zaaksysteem::Object::Types::Type/prefix>, so care should be taken to
avoid collisions.

This method also accepts a C<type> key in the argument list, which will return
a cached meta_class if it exists, undef in other cases.

    my $meta = $c->model('ObjectType')->get_meta_class(
        object_type => $my_object_type_object
    );

    # OR, in case the metaclass has previously been cached

    my $meta = $c->model('ObjectType')->get_meta_class(
        type => 'object_type_prefix'
    );

=cut

define_profile get_meta_class => (
    optional => {
        object_type => 'Zaaksysteem::Object::Types::Type',
        type => 'Str'
    },
    require_some => {
        object_or_str => [ 1, qw[object_type type] ]
    }
);

sub get_meta_class {
    my ($self, %params) = @_;

    my $opts = assert_profile(\%params)->valid;

    my $key;

    if(exists $opts->{ object_type }) {
        $key = $opts->{ object_type }->prefix;
    } else {
        $key = $opts->{ type }
    }

    if($self->meta_class_is_cached($key)) {
        return $self->get_cached_meta_class($key);
    }

    # When a string key was given as parameter, and no such key could
    # be found, we return undef because it's uncached, and we can't query the
    # Object model for the actual object. (yet, not sure this model should...)
    unless($opts->{ object_type }) {
        return;
    }

    my $meta_class = $opts->{ object_type }->instance_meta_class;

    $self->set_cached_meta_class($key, $meta_class);

    return $meta_class;
}

=head2 cache_object_types

This is a convenience method for filling the typemodel's cache with
metaclasses. The arguments are expected to be a list of
L<Zaaksysteem::Object::Types::Type> instances.

This method may throw a C<object/type/model/not_a_type_object> exception.

    $c->model('ObjectType')->cache_object_types($type1, $type2, ..., $typeN);

=cut

sub cache_object_types {
    my $self = shift;

    for my $object_type (@_) {
        unless(eval { $object_type->isa('Zaaksysteem::Object::Types::Type') }) {
            throw('object/type/model/not_a_type_object', sprintf(
                "Cannot cache a non-ObjectType object in ObjectType model (got '%s')",
                $object_type
            ));
        }

        $self->get_meta_class(object_type => $object_type);
    }

    return;
}

=head2 new_object

This method takes a object type key (it's object_class / prefix) and will try
to create a new instance of the type the key resolves to. If no type could be
found, it will throw a C<object/type/model/unknown_type> exception.

    my $plantenbak = $c->model('ObjectType')->new_object('udt');

=cut

sub new_object {
    my $self = shift;
    my $type = shift;

    my $meta = $self->get_meta_class(type => $type);

    unless(defined $meta) {
        throw('object/type/model/unknown_type', sprintf(
            "The type '%s' could not be resolved to a meta-class. Make sure the ObjectType has been added to the model's cache",
            $type
        ));
    }

    return $meta->new_object(@_);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
