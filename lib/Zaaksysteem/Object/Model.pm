package Zaaksysteem::Object::Model;

use Moose;
use namespace::autoclean;

use Moose::Util::TypeConstraints;
use Moose::Util qw/ensure_all_roles/;

use Array::Compare;
use IO::Scalar;
use JSON ();
use Module::Load ();
use Module::Load::Conditional ();
use Pod::Find qw(pod_where);
use Pod::Text;

use Zaaksysteem::Object;
use Zaaksysteem::Object::Constants qw(OBJECT_TYPE_NAME_MAP);
use Zaaksysteem::Search::ZQL;
use Zaaksysteem::Types qw(UUID);
use Zaaksysteem::Tools;

use Zaaksysteem::Object::Iterator;
use Zaaksysteem::Object::Relation;
use Zaaksysteem::Object::SecurityIdentity;
use Zaaksysteem::Object::SecurityIdentity::Position;
use Zaaksysteem::Object::SecurityIdentity::Group;
use Zaaksysteem::Object::SecurityIdentity::Role;

use Zaaksysteem::Object::SecurityRule;

with 'Zaaksysteem::Log::LoggerRole';

=head1 NAME

Zaaksysteem::Object::Model - Main in-code API for L<Zaaksysteem::Object>
interactions

=head1 SUMMARY

This model is one of the major parts in Zaaksysteem's "Object" infrastructure.
The main purpose of instances of this package is to handle persistance layer
interactions. Things like retrieving, updating, and other low-level actions
that cam modify the global object state directly.

    # Retrieve an object
    my $model = Zaaksysteem::Object::Model->new(...) # or get it via $c->model()
    my $object = $model->retrieve(uuid => '...');

    # Update some data with tandard Moose attribute accessors
    $object->some_attribute('foo');

    # And save it back to the database.
    $model->save($object);

Instances of this model will usually not be passed around in object instances,
which is a departure from ActiveRecord pattern found in L<DBIx::Class>, and
more closely follows the Data Mapper pattern, like L<DBIx::DataMapper> does.

This model class wraps around our database layer to provide a single wrapper
around things Zaaksysteem calls objects.

=head1 USAGE

This package represents some early glue between ordinary
L<DBIx::Class::ResultSet> objects and ZQL queryable resultsets.

When creating new objects, use this pattern:

    my $obj = $c->model('DB::ObjectData')->create(
        { object_class => 'saved_search' }
    );

    $obj->update();

Idiomatically the interaction to get some data via a ZQL query looks like this:

    my $rs = $c->model('Object')->rs;

The above example will query all types of objects there are, under
no logical constraints.

    use Zaaksysteem::Search::ZQL;

    my $zql = Zaaksysteem::Search::ZQL->new('SELECT {} FROM case');

    my $rs = $zql->apply_to_resultset($c->model('Object')->rs);

The above will query the object model for full-object hydrations of any
C<case> type object.

Complexer queries can of course be constructed, you can find more on the
syntax and usage in the L<Zaaksysteem::Search::ZQL> documentation.

=head2 Extracting data from the ResultSet

Being able to construct resultsets is all fine and dandy, but how to use the
data?

    use Zaaksysteem::Search::ZQL;
    use Data::Dumper;

    my $zql = Zaaksysteem::Search::ZQL->new('SELECT {} FROM case');

    my $rs = $zql->apply_to_resultset($c->model('Object')->rs);

    for my $row ($rs->all) {
        warn Dumper($row->TO_JSON);
    }

=head1 Audit dumping of objects

This model is also capable of dumping arbitrary objects, along with associated
files and metadata for use in audit trailing of the data.

See L<Zaaksysteem::Object::ExportFile> for a complete file format description.

=head2 Model hooks

Sometimes it is neccesary, or sane, to break the strict Data Mapper pattern.
This feature of the object model is such a break, only use on metaclass
object instances (such as type, casetype, etc).

Objects should know nothing about the persistence layer, but in Zaaksysteem
this strict seperation is not always completely true. In the case of
L<type|Zaaksysteem::Object::Types::Type> object instances it's clear the
the instance doubles in function as a meta-class instance. Metaclasses and
the (type)model all exist at the same 'level' of abstraction, so it's useful
that these parts can communicate.

The model does this by calling the C<model_hook> method on the object before,
during, or after specific persistance operations like saving, updating, and
deleting. These operations pass along the operation type, the model and any
number of additional parameters relevant to the operation.

Implementing a hook can be accomplished by overriding the C<model_hook> method
in the objecttype class.

    package My::Nice::Object;

    use Moose;

    extends 'Zaaksysteem::Object';

    override model_hook => sub {
        my ($self, $action, $model, @args) = @_;

        # Split up the logic by action type
        if ($action eq 'update') {
            my ($object) = @args;

            ...
        }
    }

=head3 Operation types

=over 4

=item create

This operation is triggered just before the call to L<save_object> is made by
the model. No arguments are passed.

=item update

This operation is triggered just before the call to L<save_object> is made by
the model. The invocant for the C<model_hook> call is an instance of the old,
already persisted object. The new version of this object is passed as an
additional argument.

=back

=head1 ATTRIBUTES

=head2 schema

This attribute holds a reference to a L<DBIx::Class::Schema> instance. For
L<Zaaksysteem> this will usually be, specifically, an instance of
L<Zaaksysteem::Schema>.

=cut

has schema => (
    is => 'ro',
    required => 1,
    isa => 'DBIx::Class::Schema'
);

=head2 user

This attribute holds a reference to a conceptual 'user' of the system. This
object is used to check the ACL tables when provided.

Although it is called 'user', it can be any object that implements a
C<security_identity> method, so it should be technically possible to 'view'
the entire object namespace through any authorizable entity.

=cut

has user => (
    is => 'ro',
    isa => duck_type([qw[security_identity]]),
    predicate => 'has_user'
);


=head2 source_name

This attribute holds the source name of the table the model instance is to
interact with. Defaults to C<ObjectData>.

=cut

has source_name => (
    is => 'ro',
    isa => 'Str',
    default => 'ObjectData'
);

=head2 inflate_with

This attribute relates to the behavior an instance of this model has when
inflating a source object
(L<ObjectData|Zaaksysteem::Backend::Object::Data::Component>, JSON message)
into a L<Object|Zaaksysteem::Object>.

The strings contained in the ArrayRef this accessor holds are used as
packagenames for roles to be applied after instantiation of the Object.

    my $model = Zaaksysteem::Object::Model->new(
        schema => $schema,
        inflate_with => [qw[Zaaksysteem::Object::Type::FormRole]]
    );

    my $object = $model->retrieve(uuid => $uuid);

    assert $object->does('Zaaksysteem::Object::Type::FormRole');

=cut

has inflate_with => (
    is => 'rw',
    isa => 'ArrayRef[Str]',
);

has prefetch_relations => (
    is => 'rw',
    isa => 'Bool',
    default => 0
);

has type_model => (
    is => 'rw',
    isa => 'Zaaksysteem::Object::TypeModel',
    predicate => 'has_type_model'
);

=head2 json

This internal, autovivified, L<JSON> instance specifically crafted to make the
behavior of reading a JSON object serialization of Objects and instantiating
the right one inheritable. Lazy man's dependency injection >:3

=cut

has json => (
    is => 'ro',
    isa => 'JSON',
    lazy => 1,
    default => sub {
        my $self = shift;

        return JSON->new->utf8->filter_json_object(sub {
            my $args = shift;

            my $type;
            if ($args->{type}) {
                $type = $args->{type};
            }
            else {
                $type = $args->{object_class};
            };

            return $args unless $type;

            my $class = $self->load_object_package($type);

            return {} unless $class;

            unless($class->can('new')) {
                throw('object/from_json', "Unable to call ${class}->new");
            }

            $args->{ relations } = [ map {
                delete $_->{ related_object };

                Zaaksysteem::Object::Relation->new($_)
            } @{ $args->{ related_objects } } ];

            my $values = delete $args->{ values } || {};

            # Frontend fix, it still posts related_objects as an empty array
            delete $values->{ relations };

            return $class->new(%{ $args }, %{ $values }, update_security_rules => 0);
        });
    }
);

=head2 package_type_mapping

This accessor is a HashRef representation of type string keys to package name values.

    $c->model('Object')->package_type_mapping;

    => {
        '' => 'Zaaksysteem::Object',
        case => 'Zaaksysteem::Object::Case',
        ...
    }

=cut

has package_type_mapping => (
    is => 'ro',
    isa => 'HashRef[Str]',
    default => sub {
        { object => 'Zaaksysteem::Object' }
    }
);

=head1 METHODS

=cut

sub BUILD {
    my $self = shift;

    if($self->has_type_model) {
        # Load all known user-defined types proactively, not taking ACL rules
        # into consideration. (As to prevent errors when the user *is*
        # authorized to read an object, but not its type.
        $self->type_model->cache_object_types(
            $self->_search_rs($self->new_resultset, 'type')->all
        );
    }
}

=head2 load_object_package

This method attempts to infer what package should be loaded, based on the
provided typestring. It adds to the instance's C<package_type_mapping> the
type => packagename when a class is found.

    $c->model('Object')->load_object_package('case');

    => 'Zaaksysteem::Object::Types::Case'

=cut

sub load_object_package {
    my $self = shift;
    my $type = shift;

    my $map = $self->package_type_mapping;

    unless (exists $map->{ $type }) {
        my $guess;

        if($self->has_type_model) {
            my $meta = $self->type_model->get_meta_class(type => $type);

            if ($meta) {
                $guess = $meta->name;
            }
        }

        unless(defined $guess) {
            $guess = $self->inflect_package_by_type($type);

            my $check = Module::Load::Conditional::can_load(
                modules => { $guess => undef },
                verbose => $ENV{ CATALYST_DEBUG }
            );

            if ($check) {
                unless(eval { $guess->can('new') }) {
                    throw('object/inflate', sprintf(
                        'Made a guess to load %s, but %s::new is unavailable',
                        $guess, $guess
                    ));
                }
            } else {
                throw('object/inflate', sprintf(
                    'Unable to find package for object_class "%s"',
                    $type
                ));
            }
        }

        $map->{ $type } = $guess;
    }

    return $map->{ $type };
}

=head2 new_resultset

This method builds a fresh L<DBIx::Class::ResultSet> for the object table,
with all the bells and whistles.

    my $rs = $c->model('Object')->new_resultset;

=cut

sub new_resultset {
    my $self = shift;

    my $rs = $self->schema->resultset($self->source_name);

    ensure_all_roles($rs, 'Zaaksysteem::Backend::Object::Roles::ObjectResultSet');

    $rs->hydrate_actions(1);

    return $rs->search_rs({ invalid => 0 });
}

=head2 rs

Returns a resultset that can be used to query hstore properties. The returned
object will be of type L<Zaaksysteem::Backend::Object::Roles::ObjectResultSet>.
The rows returned by the resultset are of type
L<Zaaksysteem::Backend::Object::Roles::ObjectComponent>.

=cut

sub rs {
    my $self = shift;
    my $alias = shift;

    my $rs = $self->new_resultset;

    if ($alias) {
        $rs = $rs->search(undef, { alias => $alias });
    }

    # If we were instantiated without a user, admin rights are implied and
    # no capability checks should be done.
    return $rs unless $self->has_user;

    return $rs->search_rs(
        {
            '-or'  => $self->acl_items,
        }
    );
}

=head2 acl_items

This method returns a HASH reference that L<SQL::Abstract> will interpret as
an 'or' disjunction. It builds this OR query by looking at the current user,
his/her group (=ou) and roles. Each returned item in the OR is a normal
L<SQL::Abstract> condition.

=cut

sub acl_items {
    my ($self, $alias, %conditions) = @_;

    my @entities;

    my @roles = map { $_->id } @{ $self->user->roles };
    my @groups = map { $_->id } @{ $self->user->primary_groups }, @{ $self->user->inherited_groups };

    # All possible positions for the current user, which is the cartesian product of
    # groups over roles, a rule may exist for any item in that product.
    for my $group (@groups) {
        for my $role (@roles) {
            push @entities, sprintf('%s|%s', $group, $role);
        }
    }

    ### Define the two different queries. One for "instance" and one for "type"
    my $acl_scope_where = {
        instance    => [
            { # This unit checks for object-level rules based on the current user's name
                scope => 'instance',
                entity_type => 'user',
                entity_id => $self->user->username,
            },
            { # This unit checks for object and object_class level rules based on the current user's group/role combinations
                scope => 'instance',
                entity_type => 'position',
                entity_id => { -in => \@entities },
            },
            { # This unit checks for object and object_class level rules based on the current user's roles
                scope => 'instance',
                entity_type => 'role',
                entity_id => { -in => \@roles },
            },
            { # This unit checks for object and object_class level rules based on the current user's group lineage
                scope => "instance",
                entity_type => 'group',
                entity_id => { -in => \@groups },
            }
        ],
        type        => [
            { # This unit checks for object and object_class level rules based on the current user's group/role combinations
                scope => 'type',
                entity_type => 'position',
                entity_id => { -in => \@entities },
            },
            { # This unit checks for object and object_class level rules based on the current user's roles
                scope => 'type',
                entity_type => 'role',
                entity_id => { -in => \@roles },
            },
            { # This unit checks for object and object_class level rules based on the current user's group lineage
                scope => "type",
                entity_type => 'group',
                entity_id => { -in => \@groups },
            }
        ]
    };

    ### Retrieve all possible groupnames from the object_acl table
    ###
    ### Althought the RECURSIVE query below is nothing more than:
    ### SELECT me.groupname FROM object_acl_entry me GROUP BY me.groupname;
    ### It will act a lot faster: 1ms vs 330ms on a table with 74623 rows

    ### Watch it: this query below does NOT come through the DBIC_TRACE logger
    my @groupnames = $self->schema->storage->dbh_do(sub{
             my ($storage, $dbh, @args) = @_;
             my $sth = $dbh->prepare(q{
                WITH RECURSIVE t(n) AS (
                    SELECT MIN(groupname) FROM object_acl_entry
                  UNION
                    SELECT (SELECT groupname FROM object_acl_entry WHERE groupname > n ORDER BY groupname LIMIT 1)
                    FROM t WHERE n IS NOT NULL
                )
                SELECT n FROM t;
             });
             my @data;
             $sth->execute();
             while( my $row = $sth->fetchrow_hashref){
                 push @data, $row->{n};
             }
             return @data;
        },()
    );

    my %col_map = (
        instance    => 'uuid',
        type        => 'class_uuid',
    );
    my @qs;

    ### Compile a query by groupname and scope
    for my $groupname (@groupnames, undef) {
        for my $scope (keys %$acl_scope_where) {
            my $acl_query = $self->schema->resultset('ObjectAclEntry')->search_rs(
                {
                    scope       => $scope,
                    groupname   => $groupname,
                    capability  => 'read',
                    '-or'       => $acl_scope_where->{$scope}
                },
                {
                    'alias'     => 'perm_' . ($groupname || 'undef') . '_' . $scope,
                }
            );

            my $q = {
                defined $groupname ? (acl_groupname       => $groupname) : (),
                $col_map{$scope}    => { '-in' => $acl_query->get_column('object_uuid')->as_query }
            };

            push(@qs, $q);
        }
    }

    return \@qs;
}

=head2 inflect_package_by_type

The method tries to infer the fully qualified packagename for a given object
type string.

    my $package = $model->inflect_package_by_type('case');

It tries to be smart by converting C<_> and C</> chars into, respectively,
camelcase and sub-package packagename parts.

=head3 Examples

    case        => Zaaksysteem::Object::Types::Case
    case_meta   => Zaaksysteem::Object::Types::CaseMeta
    case/event  => Zaaksysteem::Object::Types::Case::Event

=cut

sub inflect_package_by_type {
    my $self = shift;
    my $type = shift // '';

    my @pkg_parts = qw[Zaaksysteem Object Types];

    for my $subtype (split m[/], lc($type)) {
        my @words = split m[_], $subtype;

        my $subpkg = join '', map { ucfirst } @words;

        push @pkg_parts, $subpkg;
    }

    return join '::', @pkg_parts;
}

=head2 inflate

Inflate a L<Zaaksysteem::Object> instance from a source. Instantiators
delegated by provided named arguments.

    my $object = $c->model('Object')->inflate(row => $dbix_row);

    my $maybe_object_tree = $c->model('Object')->inflate(json => $string);

    my @objects = $c->model('Object')->inflate(rs => $object_data_resultset);

=cut

define_profile inflate => (
    optional => {
        json => 'Str',
        row => 'DBIx::Class::Row',
        rs => 'DBIx::Class::ResultSet'
    },
    require_some => {
        source => [ 1, qw[json row rs] ]
    }
);

sub inflate {
    my ($self, %params) = @_;

    my $args = assert_profile(\%params)->valid;

    if(exists $args->{ json }) {
        return $self->inflate_from_json($args->{ json });
    } elsif(exists $args->{ row }) {
        return $self->inflate_from_row($args->{ row });
    } elsif(exists $args->{ rs }) {
        return $self->inflate_from_rs($args->{ rs });
    }

    throw('object/inflate', 'Unable to decide on inflation implementation');
}

=head2 inflate_from_json

This method takes a JSON object serialization of an Object or an array
of objects, and produces a list of Object instances. The behavior of the
JSON decoder is such that for every new C<{}>-object found in the input it
will test if it looks like an L<Object|Zaaksysteem::Object> and attempt to
instantiate it in-place.

This means that deeply nested objects will generate a large tree of objects,
which are hard to validate compared to flat lists of, and singular, objects.

    my $maybe_object_tree = $model->inflate_from_json($json_plaintext);

=cut

sub inflate_from_json {
    my $self = shift;

    return $self->json->decode(shift);
}

=head2 inflate_from_row

This method takes a L<DBIx::Class::Row> and inflates the data contained therein
to a L<Zaaksysteem::Object> instance.

    my $object = $model->inflate_from_row($object_data_row);

=cut

sub inflate_from_row {
    my $self = shift;
    my $row = shift;

    my $class = $self->load_object_package($row->object_class);

    unless($class) {
        throw('object/inflate', sprintf(
            'Unable to determine class for type "%s"',
            $row->object_class
        ));
    }

    # TODO: Make it pretty
    my $data = $row->TO_JSON;

    delete $data->{ actions };

    my $rawvalues = delete $data->{values};

    my %args = (
        %{ $rawvalues },
        %{ $data },
        date_created => $row->date_created,
        date_modified => $row->date_modified
    );

    # If the column was not loaded, get_column will except, so guard against
    # it.
    if($row->has_column_loaded('authorizations')) {
        $args{ actions } = $row->get_column('authorizations');
    }

    my $object = $class->new(%args);

    my @actions;

    if($object->does('Zaaksysteem::Object::Roles::Security')) {
        my @rules;

        for my $acl ($row->object_acl_entries, $row->inherited_acl_entries) {
            my $entity = Zaaksysteem::Object::SecurityIdentity->new(
                entity_type => $acl->entity_type,
                entity_id   => $acl->entity_id
            );

            push @rules, Zaaksysteem::Object::SecurityRule->new(
                entity      => $entity,
                capability  => $acl->capability,
            );
        }

        if($self->has_user) {
            my @user_rules = grep {
                $self->user->matches_security_rule(rule => $_);
            } @rules;

            # Iterate over permissions first, add all capabilities the user
            # matches on
            for my $permission (@user_rules) {
                next if grep { $_ eq $permission->capability } @actions;

                push @actions, $permission->capability;
            }
        } else {
            push @actions, $object->capabilities;
        }

        $object->security_rules(\@rules);
    } else {
        push @actions, $object->capabilities;
    }

    $object->actions(\@actions);

    if($object->does('Zaaksysteem::Object::Roles::Relation')) {
        my @relations;

        for my $rel ($row->object_relationships_object1_uuids) {
            my $args = {
                related_object_id    => $rel->get_column('object2_uuid'),
                related_object_type  => $rel->object2_type,
                relationship_name_a  => $rel->type1,
                relationship_name_b  => $rel->type2,
                relationship_title_a => $rel->title1,
                relationship_title_b => $rel->title2,
                blocks_deletion      => $rel->blocks_deletion,
            };

            if($rel->get_column('owner_object_uuid')) {
                $args->{ owner_object_id } = $rel->get_column('owner_object_uuid');
            }

            my $relation = Zaaksysteem::Object::Relation->new($args);

            push @relations, $relation;
        }

        for my $rel ($row->object_relationships_object2_uuids) {
            my $args = {
                related_object_id    => $rel->get_column('object1_uuid'),
                related_object_type  => $rel->object1_type,
                relationship_name_a  => $rel->type2,
                relationship_name_b  => $rel->type1,
                relationship_title_a => $rel->title2,
                relationship_title_b => $rel->title1,
                blocks_deletion      => $rel->blocks_deletion,
            };

            if($rel->get_column('owner_object_uuid')) {
                $args->{ owner_object_id } = $rel->get_column('owner_object_uuid');
            }

            my $relation = Zaaksysteem::Object::Relation->new($args);

            push @relations, $relation;
        }

        $object->relations(\@relations);

        if($self->prefetch_relations) {
            for my $rel ($object->all_relations, $object->all_inherited_relations) {
                next if blessed $rel->related_object;

                # Prevent relation-inflation loops
                try {
                    $self->prefetch_relations(0);
                    $rel->related_object($self->retrieve(uuid => $rel->related_object_id));
                } catch {
                    $self->log->warn($_);
                } finally {
                    $self->prefetch_relations(1);
                };
            }
        }
    }

    return $object;
}

=head2 inflate_from_rs

This method takes a L<DBIx::Class::ResultSet> and inflates the data contained
therein to a list of L<Zaaksysteem::Object> instances.

    my @objects = $model->inflate_from_rs($resultset);

=cut

# XXX TODO: Contemplate the need for a Object-specific resultset equivalent
# instead of plain list hydration, paging could be internalized, if Data::Page
# proves annoying with DBIx on the lower levels.

sub inflate_from_rs {
    my $self = shift;
    my $rs = shift;
    my @args = @_;

    my @results = map { $self->inflate_from_row($_, @args) } $rs->all;

    if($rs->describe_rows) {
        map { ensure_all_roles($_, 'Zaaksysteem::Object::Roles::Describe') } @results;

        my @requested = @{ $rs->object_requested_attributes };

        if(scalar @requested) {
            map { $_->describe_attributes(\@requested) } @results;
        }
    }

    return @results;
}

=head2 zql_search

This method provides an easier interface to query by a
L<ZQL|Zaaksysteem::Search::ZQL> query.

=head3 Signature

C<< Str => Zaaksysteem::Object::Iterator >>

=head3 Example

    my $iter = $c->model('Object')->zql_search('SELECT ...');

=cut

sig zql_search => 'Str => Zaaksysteem::Object::Iterator';

sub zql_search {
    my $self = shift;
    my $query = shift;

    my $zql = Zaaksysteem::Search::ZQL->new($query);

    return Zaaksysteem::Object::Iterator->new(
        rs => $zql->apply_to_resultset($self->rs),
        inflator => sub { $self->inflate_from_row($_) }
    );
}

=head2 search

This method implements a minimal search capability on the Object model. It
currently implements exact-value searches in the L<DBIx::Class> style.

    # Populate @objects with objects that have 'column' set to 'value'.
    my @objects = $model->search('my object class', { column => 'value' });

=cut

sub search {
    my $self = shift;

    my $iter = $self->_search_rs($self->rs, @_);

    return wantarray ? $iter->all : $iter;
}

=head2 search_rs

See L</search>. Except this doesn't return a list of objects, but a
L<DBIx::Class::ResultSet>.

=cut

sub search_rs {
    my $self = shift;

    return $self->_search_rs($self->rs, @_)->rs;
}

=head2 _search

This method implements a generic object-attribute search capability on the
provided resultset. It is mainly used for internal queries where we want to
query the object tables without taking ACL rules into consideration.

    my @objects = $model->_search($model->new_resultset, 'product', { external_id => '1234' });

B<DO NOT> use this method with L</new_resultset> unless you are sure the
resulting objects will not leak to the frontend, via this flow unauthorized
users could inspect and modify all objects in the database.

=cut

sub _search {
    my ($self, $rs, $object_type, $args) = @_;

    my $subrs = $self->_search_rs($rs, $object_type, $args);

    return $subrs->all;
}

=head2 _search_rs

The guts of "_search". Returns the generated L<DBIx::Class::ResultSet>, instead
of calling L</inflate_from_rs> on it.

=cut

sub _search_rs {
    my ($self, $rs, $object_type, $args) = @_;

    $rs = $rs->search_rs({ object_class => $object_type });

    if(defined $args && ref $args eq 'HASH') {
        $rs = $rs->search_hstore(
            $rs->map_search_args($args)
        );
    }

    return Zaaksysteem::Object::Iterator->new(
        rs => $rs,
        inflator => sub { $self->inflate_from_row(shift) },
    );
}

=head2 count

This method implements the same logic as L</search>, but instead of returning
a list of L<Object|Zaaksysteem::Object>s, it returns the count for that list
indead.

This method exists because there is no drop-in replacement for Object
resultsets as an abstraction over L<DBIx::Class::ResultSet>s, and should be
evicted from the codebase when we have such a replacement in place.

    my $object_count = $model->count({ column => 'value' });

=cut

sub count {
    my $self = shift;
    my $rs = $self->rs->search({ object_class => shift });
    my $search_args = $rs->map_search_args(shift);

    return $rs->search_hstore($search_args)->count;
}

=head2 delete

This method takes an object or uuid argument and permanently destroys the
object, permanently. This method knows nothing about versions, so deleting
in the middle of a version-chain might result in fuzzy state.

    $model->delete(object => $ie8);

=cut

define_profile delete => (
    optional => {
        object => 'Zaaksysteem::Object',
        uuid   => UUID
    },
    require_some => {
        object_or_uuid => [ 1, qw[object uuid] ]
    }
);

sub delete {
    my ($self, %params) = @_;

    my $args = assert_profile(\%params)->valid;

    if(exists $args->{ object }) {
        return $self->delete_by_uuid($args->{ object }->id);
    } elsif(exists $args->{ uuid }) {
        return $self->delete_by_uuid($args->{ uuid });
    }

    throw('object/delete', 'Cannot delete object, unable to interpret parameters for object retrieval');
}

=head2 delete_by_uuid

This method is the hard implementation of object deletion from the database,
as such it really only finds the object in the database and calls
C<< L<DBIx::Class::Row>::delete >> on the retrieved row instance.

I<< When in doubt, use L</delete> instead of this method >>, unless you are
extending the Object model for some reason, override this so that you are
spared the logic checks done by L</delete>.

    $model->delete_by_uuid($uuid);

The returned value comes directly from C<< L<DBIx::Class::Row>::delete >>,
so it may not mean anything contextually correct.

=cut

sub delete_by_uuid {
    my $self = shift;
    my $uuid = shift;

    my $object_data = $self->rs->find($uuid);
    my $object = $self->inflate_from_row($object_data);

    if (!$object->is_deleteable) {
        throw(
            'object_model/delete_disallowed_by_relationship',
            "Cannot delete object $uuid, because other objects depend on it."
        );
    }

    $self->_create_log_entry(
        action => 'delete',
        pre_object => $object
    );

    return $object_data->delete;
}

=head2 retrieve

This method allows for generic object retrieval from the database. Returns a
L<Zaaksysteem::Object> instance in case a row with the provided UUID exists.
Returns C<undef> when no row could be found.

    my $obj = $c->model('Object')->retrieve(uuid => $uuid);

=cut

define_profile retrieve => (
    required => {
        uuid => UUID,
    }
);

sub retrieve {
    my ($self, %params) = @_;

    my $args = assert_profile(\%params)->valid;

    my $row = $self->rs->find($args->{ uuid });

    return unless $row;

    if ($row->invalid) {
        throw('object/model/object_invalid', 'Object is marked as invalid');
    }

    return $self->inflate_from_row($row);
}

=head2 save

This is the most generic save object. It adheres to the hashmap options calling
convention. On the highest level, this method merely auto-instantiates a JSON
argument into an L<Object|Zaaksysteem::Object> before delegating the actual work
to C<save_object>.

    # Will instantiate an $object from a JSON message, and save it to the db.
    my $object = $c->model('Object')->save(json => '{ ... }');

    # This does exactly the same, but no JSON is decoded since the object
    # is already available.
    my $object = $c->model('Object')->save(object => $object_instance);

Optimization note; only assign the return value of C<save> when actually
needed. If this call is made in a void context, the expensive re-inflation of
the updated object_data will not be executed (unless the model requires it for
post-processing).

=cut

define_profile save => (
    optional => {
        object => 'Zaaksysteem::Object',
        json => 'Str'
    },
    require_some => {
        object_or_json => [ 1, qw[object json] ]
    }
);

sub save {
    my ($self, %params) = @_;

    my $args = assert_profile(\%params)->valid;

    if(exists $args->{ json }) {
        my $res = $self->inflate_from_json(delete $args->{ json });
        if (exists $res->{object}) {
            $args = { %$args, %$res };
        }
        else {
            $args->{object} = $res;
        }
    }

    return $self->save_object(
        object         => $args->{ object },
        log            => 1,
    );
}

=head2 invalidate

=cut

sig invalidate => 'Zaaksysteem::Backend::Object::Data::ResultSet';

sub invalidate {
    my $self = shift;
    my $rs = shift;

    $rs->update({ invalid => 1 });

    return;
}

=head2 save_object

This method does a generic 'save' on an L<Object|Zaaksysteem::Object>
instance. It will check if the object has a true-ish id, which implies an
object_data row update. If the Object's id is false-like, the object_data row
will be created.

Returns a new inflation of the object_data row.

    # $object has no id, so $new_object will derive from a new object_data row
    my $new_object = $c->model('Object')->save_object(object => $object);

    # $object_new has an id, so $new_new_object will derive from an existing object_data row
    my $new_new_object = $c->model('Object')->save_object(object => $new_object);

Optimization note; only assign the return value of C<save_object> when
actually needed. If this call is made in a void context, the expensive
re-inflation of the updated object_data will not be executed (unless the model
requires it for post-processing).

=cut


define_profile save_object => (
    required => {
        object => 'Zaaksysteem::Object',
    },
    optional => {
        log            => 'Bool',
    },
);

sub save_object {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    my $object = $args->{object};

    my $object_data;
    my (@added_related, @deleted_related);

    $self->assert_unique_object($object);

    my ($action, $old_object);

    if($object->has_id) {
        $old_object = $self->retrieve(uuid => $object->id);
        $action = 'update';

        unless($old_object->has_action(sub { $_ eq 'write' })) {
            throw('object/save', 'Unable to save object, user does not have write access');
        }

        $old_object->model_hook($action, $self, $object);

        $object_data = $self->update_object_data($object);
    } else {
        $action = 'create';

        $object->model_hook($action, $self);

        $object_data = $self->create_object_data($object);
    }

    if($object->does('Zaaksysteem::Object::Roles::Relation')) {
        my @old_relations = defined $old_object ? $old_object->all_relations : ();

        # We can't use the saved object yet, unless we want to take the penalty
        # of retrieving it twice (the first time with possibly incorrect
        # relationships)
        if ($action eq 'create') {
            $object->id($object_data->uuid);
        }

        for my $relation ($object->filter_relations(sub { not $_->{ is_inherited } })) {
            # If the relation already exists, remove from the list (so we can
            # use the list for removals at the end), and start next iteration.
            if (grep { $relation->equal_to($_) } @old_relations) {
                @old_relations = grep {
                    not $relation->equal_to($_)
                } @old_relations;

                next;
            }

            my $related_object = $self->retrieve(
                uuid => $relation->related_object_id
            );

            $self->schema->resultset('ObjectRelationships')->create({
                owner_object_uuid => $relation->owner_object_id,

                object1_uuid => $object_data->uuid,
                object1_type => $object_data->object_class,
                type1        => $relation->relationship_name_a,
                title1       => $object->TO_STRING(),

                object2_uuid => $relation->related_object_id,
                object2_type => $relation->related_object_type,
                type2        => $relation->relationship_name_b,
                title2       => $related_object->TO_STRING(),

                blocks_deletion => $relation->blocks_deletion,
            });

            push @added_related, $related_object;
        }

        for my $old_relation (@old_relations) {
            unless ($old_relation->can_unrelate($object)) {
                throw('object/save/relation_not_owned', sprintf(
                    'Unable to remove relation to %s, other side owns it.',
                    $old_relation->related_object_id
                ));
            }

            my $related_object = $self->retrieve(uuid => $old_relation->related_object_id);
            push @deleted_related, $related_object;

            $object_data->object_relationships_object1_uuids->search({
                object2_uuid => $old_relation->related_object_id,
                type2 => $old_relation->relationship_name_b,
                type1 => $old_relation->relationship_name_a
            })->delete_all;

            $object_data->object_relationships_object2_uuids->search({
                object1_uuid => $old_relation->related_object_id,
                type1 => $old_relation->relationship_name_b,
                type2 => $old_relation->relationship_name_a
            })->delete_all;
        }

        # And put it back the way it was
        if ($action eq 'create') {
            $object->clear_id();
        }
    }

    # Optimization, no need to inflate the new object if it's not going to be
    # used, and we don't have to log.
    return if not ($args->{ log } || defined wantarray);

    my $new_object = $self->inflate_from_row($object_data);

    # TODO Re-build this in a new subscribe/publish framework
    if($args->{log} && $object->does('Zaaksysteem::Object::Roles::Log')) {
        $self->_create_log_entry(
            action            => $action,
            pre_object        => $old_object,
            post_object       => $new_object,
        );

        $self->_create_relation_log('add', $new_object, \@added_related)
            if (@added_related);
        $self->_create_relation_log('delete', $new_object, \@deleted_related)
            if (@deleted_related);
    }

    return $new_object;
}

sub _create_log_entry {
    my $self = shift;
    my %args = @_;

    my @changes;

    if ($args{post_object}) {
        my $comp = Array::Compare->new();

        # Create or update:
        @changes = map {
            my $attr_name = $_->name;
            my $result = {
                field       => $_->name,
                field_label => $_->label,
                old_value   => (defined $args{pre_object})
                    ? ($args{pre_object}->get_attribute_value($attr_name))
                    : (undef),
                new_value   => $args{post_object}->get_attribute_value($attr_name),
            };

            for my $val (qw(old_value new_value)) {
                if (blessed($result->{$val}) && $result->{$val}->isa('DateTime')) {
                    $result->{$val} = $result->{$val}->set_time_zone('Europe/Amsterdam')->dmy;
                }
                else {
                    $result->{$val} = $result->{$val};
                }
            }

            $result;
        } grep {
            my $attr_name = $_->name;
            if (defined($args{pre_object})) {
                my $old_value = $args{pre_object}->get_attribute_value($attr_name);
                my $new_value = $args{post_object}->get_attribute_value($attr_name);

                if (defined $old_value && defined $new_value) {
                    if (ref($old_value) eq 'ARRAY') {
                        # Returns true if arguments are equal
                        not $comp->compare($old_value, $new_value);
                    }
                    else {
                        $old_value ne $new_value;
                    }
                }
                else {
                    defined $old_value != defined $new_value;
                }
            }
            else {
                1;
            }
        } $args{post_object}->object_attributes;
    }

    # Determine which object to use for logging:
    # - "delete" only gets a "pre_object"
    # - "create" only gets a "post_object"
    # - "update" gets both
    my $log_object = $args{post_object} // $args{pre_object};

    my $filter = sub {
        return unless $_->related_object_type eq 'case';
        return unless ($_->relationship_name_a eq 'job_dependency' && $_->relationship_name_b eq 'scheduled_by') ||
                      ($_->relationship_name_b eq 'job_dependency' && $_->relationship_name_a eq 'scheduled_by');

        return 1;
    };

    my $case_id;
    if ($log_object->does('Zaaksysteem::Object::Roles::Relation')) {
        my @cases = $log_object->filter_relations($filter);

        if(scalar @cases > 1) {
            $self->log->warn(sprintf(
                "Multiple cases claim to have %s as a job dependency, this shouldn't happen.",
                $log_object->TO_STRING
            ));
        }

        my $rel = shift @cases;

        if($rel) {
            if($rel->related_object) {
                $case_id = $rel->related_object->case_number;
            } else {
                my $case = $self->retrieve(uuid => $rel->related_object_id);

                $case_id = $case->case_number if $case;
            }

            unless($case_id) {
                $self->log->warn('Unable to find case object for given relation, has it been deleted?');
            }
        }
    }

    if (@changes) {
        $self->schema->resultset('Logging')->trigger(
            sprintf('object/%s', $args{action}),
            {
                object_uuid => $log_object->id,
                zaak_id     => $case_id,
                data        => {
                    object_uuid      => $log_object->id,
                    object_label     => $log_object->TO_STRING,
                    object_type      => $log_object->type,
                    changes          => \@changes,
                },
            },
        );
    }

    return;
}

sub _create_relation_log {
    my ($self) = shift;
    my ($action, $object, $relations) = @_;

    for my $related_object (@$relations) {
        # Two log entries: one for each "direction" of the relationship
        $self->schema->resultset('Logging')->trigger(
            sprintf('object/relation/%s', $action),
            {
                object_uuid => $object->id,

                # Special "case" (heh)
                ($object->type eq 'case')
                    ? (zaak_id => $object->case_number)
                    : (),

                data => {
                    object_label             => "$object",
                    related_object_id        => $related_object->id,
                    related_object_type      => $related_object->type,
                    related_object_type_name => $self->_human_type_name($related_object),
                    related_object_label     => "$related_object",

                    ($related_object->type eq 'case')
                        ? (related_case_id => $related_object->case_number)
                        : ()
                },
            },
        );

        $self->schema->resultset('Logging')->trigger(
            sprintf('object/relation/%s', $action),
            {
                object_uuid => $related_object->id,

                # Special "case" (heh)
                ($related_object->type eq 'case')
                    ? (zaak_id => $related_object->case_number)
                    : (),

                data        => {
                    object_label             => "$related_object",
                    related_object_id        => $object->id,
                    related_object_type      => $object->type,
                    related_object_type_name => $self->_human_type_name($object),
                    related_object_label     => "$object",

                    ($object->type eq 'case')
                        ? (related_case_id => $object->case_number)
                        : (),
                },
            },
        );
    }

    return;
}

sub _human_type_name {
    my $self = shift;
    my ($object) = @_;

    my $prefix = $object->type;
    my ($type_object) = $self->_search(
        $self->new_resultset,
        'type',
        { prefix => $prefix }
    );

    if ($type_object) {
        return $type_object->name;
    }
    elsif (exists OBJECT_TYPE_NAME_MAP->{$object->type}) {
        return OBJECT_TYPE_NAME_MAP->{$object->type};
    }

    return $object->type;
}

=head2 assert_unique_object

=head3 DESCRIPTION

Asserts whether or not an object is allowed to be saved in the database. If we already have an object with a unique attribute we are not allowed to save another object which has the same value for that attribute.

=head3 SYNOPSIS

    $object_model->assert_unique_object($object);

=head3 ARGUMENTS

=over

=item object

=back

=head3 RETURNS

Dies in case of failure, returns true otherwise.

=cut

sub assert_unique_object {
    my ($self, $object) = @_;

    my @attr = grep { $_->unique && $_->has_value } $object->attribute_instances;
    return 1 if !@attr;

    my $uniq = {  map { $_->name => $_->value } @attr };
    my @objects = $self->search($object->type, $uniq);

    foreach (@objects) {
        next if ($object->has_id && $object->id eq $_->{id});
        throw('object/unique/constraint', "Object unique constraint violation: "  . join(", ", keys %$uniq), $uniq);
    }
    return 1;
}

=head2 update_object_data

This internal-ish helper method updates an existing
L<ObjectData|Zaaksysteem::Backend::Object::Data::Component> instance and syncs
the changes to the database. It returns the updated row object.

    my $object_data_row = $c->model('Object')->update_object_data($object);

=cut

sub update_object_data {
    my $self = shift;
    my $object = shift;

    my $object_data = $self->rs->find($object->id);

    unless($object_data) {
        throw(
            'object/save/update',
            sprintf('Unable to save %s, could not find referenced object_data row', $object),
            $object
        );
    }

    $object_data->object_class($object->type);
    $object_data->properties({ values => {
        map { $_->name => $_ } $object->attribute_instances
    }});

    $object_data->load_text_vector(
        $object->index_attributes
    );

    if($object->does('Zaaksysteem::Object::Roles::Type')) {
        $object_data->class_uuid($object->class_uuid);
    }

    # If we're dealing with a secure object, we will need to update the ACL
    # tables as well
    if ($object->does('Zaaksysteem::Object::Roles::Security') && $object->update_security_rules) {
        # First we clean the existing ACL rows
        $object_data->object_acl_entries->delete_all;

        # Then we re-write all ACLs as the object defines them
        for my $rule ($object->all_rules) {
            my %args = %{ $rule };

            # Safeguard for ZS-4269, this is probably safe to remove by
            # the time you read this.
            delete $args{ verdict };

            my $entity = delete $args{ entity };
            my %security_identity = $entity->security_identity;

            for my $type (keys %security_identity) {
                $object_data->object_acl_entries->create({
                    object_uuid => $object_data->uuid,
                    entity_type => $type,
                    entity_id   => $security_identity{ $type },
                    %args
                });
            }
        }
    }

    return $object_data->update;
}

=head2 create_object_data

This internal-ish helper method creates a new
L<ObjectData|Zaaksysteem::Backend::Object::Data::Component> instance and syncs
it to the database. It returns the created row object.

    my $object_data_row = $c->model('Object')->create_object_data($object);

This method will also automatically create ACL entries for the current user,
if set in the model instance. It will grant the C<read>, C<write>, and
C<delete> permissions to the security identity returned by the L</user>.

Also, if the object being saved does the
L<Zaaksysteem::Object::Roles::Security>-role, this method will create ACL
entries for all L<security roles|Zaaksysteem::Object::Roles::Security/rules>
the object references. This last behavior does B<not> require the L</user>
attribute to be set.

=cut

sub create_object_data {
    my $self = shift;
    my $object = shift;

    my %data = (
        object_class => $object->type,
        text_vector => join(' ', $object->index_attributes),
        properties => { values => {
            map { $_->name => $_ } $object->attribute_instances
        }}
    );

    if($object->does('Zaaksysteem::Object::Roles::Type')) {
        $data{ class_uuid } = $object->class_uuid
    }

    my $object_data = $self->new_resultset->create(\%data);

    $object_data->load_text_vector($object->index_attributes);
    $object_data = $object_data->update;

    return $object_data unless $object->does('Zaaksysteem::Object::Roles::Security');

    # Automagically grant the current user basic rights on the new object.
    if ($self->has_user) {
        $object->permit($self->user, $object->capabilities);
    }

    for my $rule ($object->all_rules) {
        my %args = %{ $rule };

        # Safeguard for ZS-4269, this is probably safe to remove by
        # the time you read this.
        delete $args{ verdict };

        my $entity = delete $args{ entity };
        my %security_identity = $entity->security_identity;

        for my $type (keys %security_identity) {
            $object_data->object_acl_entries->create({
                object_uuid => $object_data->uuid,
                entity_type => $type,
                entity_id   => $security_identity{ $type },
                %args
            });
        }
    }

    return $object_data;
}

=head2 find_position

This method returns a L<Zaaksysteem::Object::SecurityIdentity::Position>
instance after validatating the provided argument(s).

    my $entity = $model->find_position('123|987');

    # Or

    my $entity = $model->find_position('123', '987');

=cut

sub find_position {
    my $self = shift;

    # Allow both find_position('123|987') and find_position('123', '987')
    # calling styles
    my ($group_id, $role_id) = map { split m[\|] } @_;
    my $schema = $self->new_resultset->result_source->schema;

    unless(defined $group_id && defined $role_id) {
        throw('object/model/find_position', sprintf(
            'Unable to determine ou and role id from argument(s) "%s"',
            join ", ", @_
        ));
    }

    my $role_count      = $schema->resultset('Roles')->search(
        {id => $role_id}
    )->count;

    my $group_count     = $schema->resultset('Groups')->search(
        {id => $group_id}
    )->count;

    # If ou_id is provided, but is false-y (0), assume role identity
    unless ($group_id) {
        return unless $role_count;

        return Zaaksysteem::Object::SecurityIdentity::Role->new(
            role_id => $role_id
        );
    }

    # If role_id is provided, but false-y (0), assume group identity
    unless ($role_id) {
        return unless $group_count;

        return Zaaksysteem::Object::SecurityIdentity::Group->new(
            ou_id => $group_id
        );
    }

    # Default case, assume position
    return unless $role_count && $group_count;

    return Zaaksysteem::Object::SecurityIdentity::Position->new(
        ou_id => $group_id,
        role_id => $role_id
    );
}

=head2 mutate

Execute a mutation row against the model.

    my $mutation_spec = $c->model('Object')->mutate($mutation_row);

This method returns a simple hashref with information about the executed
mutation. In the case of a creation mutation, it looks like this;

    {
        mutation_type => 'create',
        object_uuid => UUID_of_new_instance,
        object_label => new_instance->TO_STRING,
        changes => [
            { field => "attr1", new_value => "new_value_attr1" },
            { field => "foo", new_value => "bar" },
            { field => "aap", new_value => "nootmies" },
        ]
    }

In the case of update mutations, the objects in the C<changes> key will also
have the C<old_value> field.

    {
        mutation_type => 'update',
        object_uuid => UUID_of_object,
        object_label => object->TO_STRING,
        changes => [
            { field => "foo", old_value => "bar", new_value => "baz" },
            { field => "qux", old_value => "quux", new_value => "corge" }
        ]
    }


And deletions, something akin to this;

    {
        mutation_type => 'delete',
        object_uuid => UUID_of_deleted_object,
        object_label => deleted_object->TO_STRING
    }

=cut

sub mutate {
    my $self = shift;
    my $mutation = shift;

    unless(blessed $mutation && $mutation->isa('Zaaksysteem::Schema::ObjectMutation')) {
        throw('object/model/mutate', 'Expecting a ObjectMutation row, got something else');
    }

    my $retval = {
        mutation_type => $mutation->type,
    };

    my %attr_name_value_map;

    for my $attr_namespaced_name (keys %{ $mutation->values }) {
        my ($namespace, $attr_name) = split m[\.], $attr_namespaced_name;

        next unless $namespace eq 'attribute';

        $attr_name_value_map{ $attr_name } = $mutation->values->{ $attr_namespaced_name };
    }

    my $lock_object = $self->inflate_from_row($mutation->lock_object_uuid);

    if($mutation->type eq 'create') {
        my $new_instance = $self->type_model->new_object(
            $mutation->object_type,
            %attr_name_value_map
        );

        $new_instance->relate($lock_object, owner_object_id => $lock_object->id);

        my $object = $self->save_object(object => $new_instance);

        my @files = map { @{ $_->value } }
                   grep { $_->attribute_type eq 'file' && $_->has_value }
                        $object->attribute_instances;

        my $case = $mutation->lock_object_uuid->get_source_object;

        for my $file (@files) {
            my $filestore = $case->result_source->schema->resultset('Filestore')->find({ uuid => $file->{ uuid } });

            $case->files->file_create({
                name => $file->{ original_name },
                db_params => {
                    filestore_id => $filestore->id,
                    case_id => $mutation->lock_object_uuid->object_id
                }
            });
        }

        my @changes;

        for my $attr_name (keys %{ $mutation->values }) {
            push @changes, {
                field => $attr_name,
                new_value => $mutation->values->{ $attr_name }
            };
        }

        $retval->{ object_uuid } = $object->id;
        $retval->{ object_label } = $object->TO_STRING;
        $retval->{ object_type } = $object->type;
        $retval->{ changes } = \@changes;
    } elsif($mutation->type eq 'update') {
        my $instance = $self->inflate_from_row($mutation->object_uuid);

        my @changes;

        for my $attr_name (keys %attr_name_value_map) {
            my $attr = $instance->meta->find_attribute_by_name($attr_name);
            my $value = $attr_name_value_map{ $attr_name };

            push @changes, {
                field => $attr_name,
                old_value => $attr->get_value($instance),
                new_value => $value
            };

            $attr->set_value($instance, $value);
        }

        $instance->relate($lock_object, owner_object_id => $lock_object->id);

        my $object = $self->save_object(object => $instance);

        my @files = map { @{ $_->value } }
                   grep { $_->attribute_type eq 'file' && $_->has_value }
                        $object->attribute_instances;

        my $case = $mutation->lock_object_uuid->get_source_object;

        my @known_files = $case->files->search({
            filestore_id => { -in => [ map { $_->{ id } } @files ] }
        });

        for my $file (@files) {
            next if grep { $file->{ id } eq $_->get_column('filestore_id') } @known_files;

            $case->files->file_create({
                name => $file->{ original_name },
                db_params => {
                    filestore_id => $file->{ id },
                    case_id => $mutation->lock_object_uuid->object_id
                }
            });
        }

        $retval->{ object_uuid } = $object->id;
        $retval->{ object_label } = $object->TO_STRING;
        $retval->{ object_type } = $object->type;
        $retval->{ changes } = \@changes;
    } elsif($mutation->type eq 'delete') {
        my $instance = $self->inflate_from_row($mutation->object_uuid);

        $self->delete_by_uuid($instance->id);

        $retval->{ object_label } = $instance->TO_STRING;
        $retval->{ object_type } = $instance->type;
    } elsif($mutation->type eq 'relate') {
        my $instance = $self->inflate_from_row($mutation->object_uuid);

        $instance->relate($lock_object, owner_object_id => $lock_object->id);

        $self->save_object(object => $instance);

        $retval->{ object_uuid } = $instance->id;
        $retval->{ object_label } = $instance->TO_STRING;
        $retval->{ object_type } = $instance->type;
    }

    return $retval;
}

=head2 validate_mutation

Checks if a mutation is complete by applying the supposed mutation to an
in-memory object instance.

This method will just return true-ish if everything checks out, and the
mutations *should* apply cleanly when passed to L</mutate>. A failure is
signalled via exceptions, most likely a Moose type constraint exception.

=cut

sig validate_mutation => 'Zaaksysteem::Backend::Object::Mutation::Component';

sub validate_mutation {
    my $self = shift;
    my $mutation = shift;

    my %attr_name_value_map;

    for my $attr_namespaced_name (keys %{ $mutation->values }) {
        my ($namespace, $attr_name) = split m[\.], $attr_namespaced_name;

        next unless $namespace eq 'attribute';

        $attr_name_value_map{ $attr_name } = $mutation->values->{ $attr_namespaced_name };
    }

    if ($mutation->type eq 'create') {
        return $self->type_model->new_object(
            $mutation->object_type,
            %attr_name_value_map
        );
    }

    if ($mutation->type eq 'update') {
        my $instance = $self->inflate_from_row($mutation->object_uuid);

        for my $attr_name (keys %attr_name_value_map) {
            my $attr = $instance->meta->find_attribute_by_name($attr_name);
            my $value = $attr_name_value_map{ $attr_name };

            $attr->set_value($instance, $value);
        }

        return $instance;
    }

    return 1;
}

=head2 export_single

Export a single object in an export file.

=cut

define_profile export_single => (
    optional => {
        tar_handle => 'Archive::Tar::Stream',
        object     => 'Zaaksysteem::Object',
        metadata   => 'HashRef',
    },
);

sub export_single {
    my $self = shift;
    my %params = @_;

    my $args = assert_profile(\%params)->valid;

    my $meta_json = JSON->new->pretty->canonical->encode({
        %{ $args->{metadata} },

        version     => '1',
        time        => DateTime->now()->set_time_zone('UTC')->iso8601 . 'Z',
        export_type => 'single',
    });
    $self->_add_scalar_to_export($args->{tar_handle}, "meta.json", $meta_json);

    $self->_add_readme_to_export($args->{tar_handle});

    $self->_add_object_to_export($args->{tar_handle}, $args->{object});

    return;
}

=head2 export_multiple

Export a ResultSet of objects to an export file.

=cut

define_profile export_multiple => (
    optional => {
        tar_handle => 'Archive::Tar::Stream',
        resultset  => 'Zaaksysteem::Backend::Object::Data::ResultSet',
        metadata   => 'HashRef',
    },
);

sub export_multiple {
    my $self = shift;
    my %params = @_;

    my $args = assert_profile(\%params)->valid;
    my $meta_json = JSON->new->pretty->canonical->encode({
        %{ $args->{metadata} },

        version     => '1',
        time        => DateTime->now()->set_time_zone('UTC')->iso8601 . 'Z',
        export_type => 'query',
    });
    $self->_add_scalar_to_export($args->{tar_handle}, "meta.json", $meta_json);

    $self->_add_readme_to_export($args->{tar_handle});

    while (my $row = $args->{resultset}->next) {
        my $object = $self->inflate_from_row($row);
        $self->_add_object_to_export($args->{tar_handle}, $object);
    }

    return;
}

sub _add_readme_to_export {
    my $self = shift;
    my ($tar) = @_;

    my $parser = Pod::Text->new(loose => 1);
    my $readme = pod_where({-inc => 1}, "Zaaksysteem::Object::ExportFile");

    my $text;
    $parser->output_string(\$text);
    $parser->parse_file($readme);

    $self->_add_scalar_to_export($tar, "README.txt", $text);

    return;
}

sub _add_object_to_export {
    my $self = shift;
    my ($tar, $object) = @_;

    my $object_id = $object->id;

    my $files;
    if ($object->type eq 'case') {
        # Case is a special case. It's not fully "object" yet, so we need to
        # JSONify the database row, not the in-memory object.
        $object = $self->schema->resultset('ObjectData')->find($object_id);

        # Only case objects can have files attached to them for now.
        $files = $self->schema->resultset('File')->search(
            { 'me.case_id' => $object->object_id }
        )->active_rs;
    }

    my $object_json = do {
        my $json = JSON->new->utf8->pretty->allow_nonref->allow_blessed->convert_blessed->canonical();

        no warnings 'redefine';
        local *DateTime::TO_JSON = sub { shift->iso8601 };

        $json->encode($object);
    };

    $self->_add_scalar_to_export($tar, "data/$object_id.json", $object_json);

    if ($files) {
        while(my $file = $files->next) {
            $self->_add_attachment_to_export($tar, $file, $object_id);
        }
    }

    return;
}

sub _add_scalar_to_export {
    my $self = shift;
    my ($tar, $filename, $scalar) = @_;

    my $fh = IO::Scalar->new(\$scalar);

    $tar->AddFile($filename, length($scalar), $fh, mode => 0644);

    $fh->close();

    return;
}

sub _add_attachment_to_export {
    my $self = shift;
    my ($tar, $file, $object_id) = @_;

    my $filestore = $file->filestore_id;
    my $path      = $filestore->get_path();
    my $file_uuid = $filestore->uuid;

    my $case_document_ids = $file->case_documents->search(
        {},
        {
            prefetch => {
                'case_document_id' => 'bibliotheek_kenmerken_id'
            },
        }
    );
    my $attributes = [
        map {
            "attribute." . $_->case_document_id->bibliotheek_kenmerken_id->magic_string
        } $case_document_ids->all
    ];

    my $file_json = do {
        my $json = JSON->new->utf8->pretty->allow_nonref->allow_blessed->convert_blessed->canonical();

        no warnings 'redefine';
        local *DateTime::TO_JSON = sub { shift->iso8601 };

        $json->encode({
            directory  => $file->directory_id ? $file->directory_id->name : '',
            attributes => $attributes,
            metadata   => $file,
        });

    };

    $self->_add_scalar_to_export($tar, "data/$object_id/$file_uuid.json", $file_json);

    my $fh = $filestore->read_handle();
    $tar->AddFile("data/$object_id/$file_uuid.blob", -s $fh, $fh, mode => 0644);

    return;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
