package Zaaksysteem::Object::Constants;

use warnings;
use strict;
use utf8;

use Exporter qw[import];

our @EXPORT = qw[
    OBJECT_TYPES
    ATTRIBUTE_TYPES
    SEARCH_OBJECT_TYPE_BLACKLIST
];

our @EXPORT_OK = qw[
    OBJECT_TYPE_NAME_MAP
];

=head1 NAME

Zaaksysteem::Object::Constants - Constants for the "Object"

=head1 CONSTANTS

=head2 OBJECT_TYPES

A hash of database-level object types, mapped to the role(s) that should be
applied to the L<Zaaksysteem::Object::Data::Component> object.

=over 4

=item case

L<Zaaksysteem::Object::Data::Roles::Case>

=item saved_search

L<Zaaksysteem::Object::Data::Roles::SavedSearch>

=item type

L<Zaaksysteem::Object::Data::Roles::Type>

=item product

L<Zaaksysteem::Object::Data::Roles::Product>

=back

=cut

use constant OBJECT_TYPES    => {
    case            => 'Zaaksysteem::Backend::Object::Data::Roles::Case',
    saved_search    => 'Zaaksysteem::Backend::Object::Data::Roles::SavedSearch',
    type            => 'Zaaksysteem::Backend::Object::Data::Roles::Type',
    casetype        => 'Zaaksysteem::Backend::Object::Data::Roles::Casetype'
};

=head2 ATTRIBUTE_TYPES

A hash of database-level attribute types, mapped to the packages that
implement type-specific behaviors.

=over 4

=item bag

L<Zaaksysteem::Object::Attribute::BAG>

=item boolean

L<Zaaksysteem::Object::Attribute::Boolean>

=item timestamp

L<Zaaksysteem::Object::Attribute::Timestamp>

=item number, integer

L<Zaaksysteem::Object::Attribute::Number>

=item object

L<Zaaksysteem::Object::Attribute::Object>

=item timestamp_op_text

L<Zaaksysteem::Object::Attribute::TimestampOrText>

=back

=cut

use constant ATTRIBUTE_TYPES => {
    bag                     => [qw[BAG]],
    boolean                 => [qw[Boolean]],
    casetype                => [qw[]],
    category                => [qw[]],
    checkbox                => [qw[Checkbox]],
    hidden                  => [qw[]],
    integer                 => [qw[Number]],
    number                  => [qw[Number]],
    object                  => [qw[Object]],
    'org-unit'              => [qw[]],
    radio                   => [qw[]],
    richtext                => [qw[]],
    textarea                => [qw[]],
    text                    => [qw[]],
    email                   => [qw[]],
    bankaccount             => [qw[]],
    bag_straat_adres        => [qw[]],
    bag_straat_adressen     => [qw[]],
    bag_adres               => [qw[]],
    bag_adressen            => [qw[]],
    bag_openbareruimte      => [qw[]],
    bag_openbareruimtes     => [qw[]],
    valuta                  => [qw[Number]],
    valutain                => [qw[Number]],
    valutaex                => [qw[Number]],
    valutain6               => [qw[Number]],
    valutaex6               => [qw[Number]],
    valutain21              => [qw[Number]],
    valutaex21              => [qw[Number]],
    subject                 => [qw[Object]],
    select                  => [qw[]],
    url                     => [qw[]],
    image_from_url          => [qw[]],
    text_uc                 => [qw[]],
    numeric                 => [qw[]],
    date                    => [qw[Timestamp]],
    timestamp_or_text       => [qw[TimestampOrText]],
    timestamp               => [qw[Timestamp]],
    file                    => [qw[Filestore]],
    googlemaps              => [qw[]],
    option                  => [qw[]],
    calendar                => [qw[]],
    'object-attribute-list' => [qw[]],
};

=head2 SEARCH_OBJECT_TYPE_BLACKLIST

This constant exports a list of object type prefixes that should not be
included in autocomplete or spot-enlighter resultsets.

=over 4

=item case

Cases are a special case in object search.

=item casetype

Casetypes are management objects, and won't be seached for by the primary
users.

=item customer_d

CustomerD objects are system-internal.

=item scheduled_job

ScheduledJob objects are system-internal.

=item saved_search

SavedSearches are exposed via a different UI element

=item type

Types are management objects, and won't be searched for by the primary users.

=back

=cut

use constant SEARCH_OBJECT_TYPE_BLACKLIST => [qw[
    case
    casetype
    customer_d
    scheduled_job
    saved_search
    type
]];

=head2 OBJECT_TYPE_NAME_MAP

Mapping of object type names to their human-readable equivalents.

=cut

use constant OBJECT_TYPE_NAME_MAP => {
    'scheduled_job' => 'Geplande zaak',
    'casetype'      => 'Zaaktype',
    'case'          => 'Zaak',
};


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

