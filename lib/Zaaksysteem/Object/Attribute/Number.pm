package Zaaksysteem::Object::Attribute::Number;

use Moose::Role;

has 'value' => (
    is      => 'rw',
    isa     => 'Maybe[Num] | ArrayRef[Num]'
);

=head2 _build_human_value

On our Text role, the value is identical to human_value, unless overridden

=cut

sub _build_human_value {
    my $self = shift;

    return $self->value;
}

=head2 TO_JSON

This is a wrapper for L<Zaaksysteem::Object::Attribute#TO_JSON> that
forces the "value" attribute to look numeric for JSON.

=cut

around TO_JSON => sub {
    my $orig = shift;
    my $self = shift;

    my $json_data = $self->$orig(@_);

    if (not defined $json_data->{value}) {
        return $json_data;
    }

    if (ref $json_data->{value} eq 'ARRAY') {
        $json_data->{value} = [
            map {
                $self->_value_to_json($_)
            } @{ $json_data->{value} }
        ];
    }
    else {
        $json_data->{value} = $self->_value_to_json($json_data->{value});
    }

    return $json_data;
};

sub _value_to_json {
    my $self = shift;
    my ($value) = @_;

    # Force the numeric nature of the value
    return 0 + $value;
}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

