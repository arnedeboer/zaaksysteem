package Zaaksysteem::Object::Attribute::Checkbox;

use Moose::Role;

has 'value' => (
    is      => 'rw',
    isa     => 'Maybe[Bool] | ArrayRef[Str]'
);

=head2 _build_human_value

The human_value is identical to the value.

=cut

sub _build_human_value {
    my $self = shift;

    return $self->value;
}

=head2 TO_JSON

This is a wrapper for L<Zaaksysteem::Object::Attribute#TO_JSON> that
forces the "value" attribute to look like a(n array of) boolean for JSON.

=cut

around TO_JSON => sub {
    my $orig = shift;
    my $self = shift;

    my $json_data = $self->$orig(@_);

    if (not defined $json_data->{value}) {
        return $json_data;
    }

    if (ref($json_data->{value}) ne 'ARRAY') {
        $json_data->{value} = $json_data->{value} ? \1 : \0;
    }

    return $json_data;
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

