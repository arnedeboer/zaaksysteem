package Zaaksysteem::Object::Importer::Sdu::Product;

use Moose::Role;
use XML::Twig;

use Zaaksysteem::Search::ZQL;
use Zaaksysteem::Tools;

use Zaaksysteem::Object::Model;

with 'Zaaksysteem::Object::Importer::Sdu';

=head2 tablename

The inner object table in which items are stored

=cut

has tablename => (is => 'ro', default => 'product');

=head2 field_mapping

Maps xml fields to db columns.

XML file naming => local naming

=cut

has field_mapping => (
    is => 'ro',
    default => sub {
        return {
            id => 'pdc_external_id',
            name => 'pdc_name',
            description => 'pdc_description',
            conditions => 'pdc_terms',
            procedure => 'pdc_approach',
            costs => 'pdc_price',
            appeal => 'pdc_appeal',
            law_rules => 'pdc_law_rules',
            contact => 'pdc_contact',
            uitvoerende_instanties => 'pdc_submission_addresses',
            audience => 'pdc_audience',
            #uitvoerende_instanties => 'pdc_executive_authorities'
        }
    }
);

sub get_new_row {
    my ($self, $properties) = @_;

    return {
        %$properties,
        category_id => $self->state->library_id,
        internal_explanation => '',
        target => ['internal'],
        commit_message => 'Geimporteerd',
    };
}

=head2 get_xml_twig

This returns the parser object that eats the xml

=cut

sub get_xml_twig {
    my $self = shift;

    return XML::Twig->new(
        twig_handlers => {
            'page/product' => sub {
                my $product = $_->simplify;

                # id is represented in the XML as an attribute:
                # <product id="123">...</product>
                # include as a member here
                $product->{ id } = $_->{ att }{ id };
                $product->{ type } = 'product';

                my $law_rule_hash = $product->{ law_rules }{ law_rule };
                my @law_rules;

                if($law_rule_hash) {
                    @law_rules = map { { %{ $law_rule_hash->{ $_ } }, id => $_ } } keys %{ $law_rule_hash };

                    $product->{ law_rules } = sprintf('<p><ul>%s</ul></p>', join("\n", map {
                        sprintf('<li><a href="%s">%s</a></li>', $_->{ url }, $_->{ law_rule_name })
                    } @law_rules));
                }
                else {
                    $product->{ law_rules } = undef;
                }

                my @uitvoerende_instanties;
                my $uitvoerende_instantie_hash = $product->{ uitvoerende_instanties }{ instantie };

                if ($uitvoerende_instantie_hash) {
                    @uitvoerende_instanties = map {
                        { %{ $uitvoerende_instantie_hash->{ $_ } }, id => $_ }
                    } keys %{ $uitvoerende_instantie_hash };
#
                    $product->{ uitvoerende_instanties } = sprintf(
                        '<p><ul>%s</ul></p>',
                        join("\n", map {
                            sprintf('<li><a href="%s">%s</a></li>', $_->{ url } // '', $_->{ naam })
                        } @uitvoerende_instanties)
                    );
                }
                else {
                    $product->{ uitvoerende_instanties } = undef;
                }

                if ($product->{ doelgroepen }) {
                    my $doelgroep = $product->{ doelgroepen }{ doelgroep };
                    my @doelgroepen = ref $doelgroep eq 'ARRAY' ? @{ $doelgroep } : $doelgroep;

                    $product->{ audience } = join ", ", @doelgroepen;
                }

                $self->parse($product, []);
            }
        }
    );
}

around validate => sub {
    my $orig = shift;
    my $self = shift;
    my $file = shift;

    return unless $self->$orig($file, @_);

    my $xml = XML::XPath->new(filename => $file->get_path);

    return unless $xml;
    return unless scalar $xml->find('/page/product');
};

=head2 new_instance

Create new instance of product object

=cut

sub new_instance {
    my $self = shift;
    my $params = shift || {};

    return $self->type_model->new_object('product', {
        pdc_name => '',
        %{ $params }
    });
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 get_new_row

TODO: Fix the POD

=cut

