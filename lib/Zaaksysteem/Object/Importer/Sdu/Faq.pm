package Zaaksysteem::Object::Importer::Sdu::Faq;

use Moose::Role;
use XML::Twig;

use Zaaksysteem::Search::ZQL;
use Zaaksysteem::Tools;

use Zaaksysteem::Object::Model;

with 'Zaaksysteem::Object::Importer::Sdu';

=head2 tablename

The inner object table in which items are stored

=cut

has tablename => (is => 'ro', default => 'vraag');

=head2 field_mapping

Maps xml fields to db columns.

XML file naming => local naming

=cut

has field_mapping => (
    is => 'ro',
    default => sub {
        return {
            id => 'pdc_external_id',
            title => 'faq_title',
            question => 'faq_question',
            answer => 'faq_answer',
        }
    }
);

sub get_new_row {
    my ($self, $properties) = @_;

    return {
        %$properties,
        category_id => $self->state->library_id,
        internal_explanation => '',
        target => ['internal'],
        commit_message => 'Geimporteerd',
    };
}

=head2 get_xml_twig

This returns the parser object that eats the xml

=cut

sub get_xml_twig {
    my $self = shift;

    return XML::Twig->new(
        twig_handlers => {
            'vac:vac' => sub {
                my $twig = shift;
                my $vac = shift;

                my $item = $vac->simplify;
                my $kanaal = $item->{'vac:body'}{'vac:kanaal'};

                my $parsed = {
                    type => 'faq',
                    id => $item->{'vac:meta'}{'vac:owmskern'}{'dcterms:identifier'},
                    title => $item->{'vac:meta'}{'vac:owmskern'}{'dcterms:title'},
                    name => $kanaal->{'vac:vraag'},
                    question => $kanaal->{'vac:vraag'},
                    answer => $kanaal->{'vac:antwoord'}{'vac:antwoordProductVeld'} ||
                                $kanaal->{'vac:antwoord'}{'vac:antwoordTekst'},
                };

                my @relations;

                # Retrieve faq relations
                for my $vacRelation ($vac->get_xpath('vac:body/vac:verwijzingVac')) {
                    push @relations, {
                        external_id => $vacRelation->att('resourceIdentifier'),
                        label => $vacRelation->text,
                        type => 'vraag'
                    };
                }

                # Retrieve product relations
                for my $productRelation ($vac->get_xpath('vac:body/vac:verwijzingProduct')) {
                    push @relations, {
                        external_id => $productRelation->att('resourceIdentifier'),
                        label => $productRelation->text,
                        type => 'product'
                    };
                }

                return $self->parse($parsed, \@relations);
            }
        }
    );
}

around validate => sub {
    my $orig = shift;
    my $self = shift;
    my $file = shift;

    return unless $self->$orig($file, @_);

    my $xml = XML::XPath->new(filename => $file->get_path);

    return unless $xml;
    return unless scalar $xml->find('/vac:vacs/vac:vac');

    return 1;
};

=head2 new_instance

Create new instance of product object

=cut

sub new_instance {
    my $self = shift;
    my $params = shift || {};

    return $self->type_model->new_object('vraag', {
        faq_title => '',
        %{ $params }
    });
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 get_new_row

TODO: Fix the POD

=cut

