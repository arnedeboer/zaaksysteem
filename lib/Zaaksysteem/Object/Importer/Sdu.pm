package Zaaksysteem::Object::Importer::Sdu;

use Moose::Role;
use XML::Twig;

use Zaaksysteem::Search::ZQL;
use Zaaksysteem::Tools;

use Zaaksysteem::Object::Model;

=head1 NAME

Zaaksysteem::Object::Importer::Sdu - Shared features for SDU PDC/FAQ imports

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head2 type_model

=cut

has type_model => (
    is => 'rw',
    lazy => 1,
    default => sub {
        return shift->model->type_model;
    }
);

=head2 handles

Determine which types will be handled

=cut

sub handles {
    my $self = shift;
    my $object_type = shift;

    return $object_type =~ m/product|qa/;
}

=head2 execute_import_state

When calling this, the admin has had a chance to make decisions. This
loops through the import items and executes whatever the admin has
expressed is desirable.

=cut

sub execute_import_state {
    my $self = shift;

    my $items = $self->state->items;

    # first store each entry
    $self->import_entry($_) for @$items;

    # then apply relations
    # $self->update_relations($_) for @$items;
}

=head2 import_entry

Import entry into db

=cut

sub import_entry {
    my ($self, $state_item) = @_;

    my $selected_option = $state_item->selected_option;
    my $properties = $state_item->properties;
    my $new_row = $self->get_new_row($properties);

    my $object;

    if ($selected_option eq 'create') {
        $object = $self->create_object($new_row);
    } elsif ($selected_option eq 'update') {
        $object = $self->update_object($state_item->{id}, $new_row);
    }

    for my $relation (@{ $state_item->relations || [] }) {
        next unless $relation->{ local_id };

        $object->relate($self->model->retrieve(uuid => $relation->{ local_id }));
    }

    $object = $self->model->save(object => $object);

    if ($object) {
        $state_item->source_uri(sprintf(
            '/object/%s',
            $object->id
        ));
    }
}

=head2 update_object

id param is the id attribute from the xml, aka external id

Confidently find the existing row - if this would fail we'll see
a ZSOD, and it probably means an expired session or a concurrent
session that has updated the database. Since this feature
will be used sparingly by a limited number of expert users I can't
defend eloborate error handling here.

=cut

sub update_object {
    my ($self, $id, $new_row, $relations) = @_;

    my $existing = $self->find_existing($id);
    my $mapping = $self->field_mapping;

    for my $attribute (values %$mapping) {
        $existing->$attribute($new_row->{ $attribute });
    }

    return $self->model->save(object => $existing);
}

=head2 hydrate_import_state

Reads import file, parses it, creates entries that will be used
by the GUI to have admin decide.

=cut

sub hydrate_import_state {
    my $self = shift;
    my $fileref = shift;

    throw('importer/sdu/invalid_file',
        'Importbestand kon niet geinterpreteerd worden als geldige SDU XML')
            unless $self->validate($fileref);

    $self->state->import_fileref($fileref->uuid);
    $self->state->format($self->format);
    $self->state->object_type($self->object_type);

    $self->get_xml_twig->parsefile($fileref->get_path);
}

=head2 parse_item

Return hashref filled in with the relevant details of the given xml node.
Substitutes the local name for all fields.

=cut

sub parse_item {
    my ($self, $item) = @_;

    my $mapping = $self->field_mapping;

    return {
        map { $mapping->{$_} => ($item->{$_} || '') } keys %$mapping
    }
}

=head2 model

Convenience method to only once create a resultset.

=cut

has model => (
    is => 'ro',
    lazy => 1,
    default => sub {
        my $self = shift;

        return Zaaksysteem::Object::Model->new(schema => $self->schema);
    }
);

=head2 find_existing

Query the object platform for an item with a given external id.

In dbix-speak:

    my $product = $schema->resultset('Product')->find({external_id => $external_id});
    return { $product->get_columns };

=cut

sub find_existing {
    my ($self, $external_id) = @_;

    my $count = $self->model->count($self->tablename, {
        pdc_external_id => $external_id
    });

    return unless $count;

    unless ($count == 1) {
        throw('importer/sdu/external_id_conflict', 'Multiple objects with the same reference id found');
    }

    # Since there is no Object::ResultSet yet, model returns a list
    my ($object) = $self->model->search($self->tablename, {
        pdc_external_id => $external_id
    });

    return $object;
}

=head2 create_object

Save new item in the object platform database.

=cut

sub create_object {
    my ($self, $values) = @_;

    my $object = $self->new_instance($values);

    return $self->model->save(object => $object);
}

=head2 parse

"parse" is a misnomer here, but changing it will have to happen in
all the sibling modules which are currently not covered in the
testsuite, which is why i will not change this now.
i propose "get_item_options".

This sub determines the import options for the given item, depending
on wether given option is already in the database.

=cut

use constant STRINGS => {
    faq => {
        create => 'Importeer als nieuwe vraag',
        update => 'Update bestaande vraag',
        ignore => 'Negeer vraag'
    },

    product => {
        create => 'Importeer als nieuw product',
        update => 'Update bestaand product',
        ignore => 'Negeer product'
    }
};

sub parse {
    my ($self, $item, $relations) = @_;

    my $existing = $self->find_existing($item->{ id });

    for my $relation (@{ $relations }) {
        my ($related) = $self->model->search($relation->{ type }, {
            pdc_external_id => $relation->{ external_id }
        });

        next unless $related;

        $relation->{ local_id } = $related->id;
    }

    my $state_item = $self->state->new_item({
        id => $item->{ id },
        label => $item->{ name },
        properties => $self->parse_item($item),
        relations => $relations,
    });

    my $strings = STRINGS()->{ $item->{ type } };

    # New import item. full control over it
    unless($existing) {
        $state_item->add_default_option('create', $strings->{ create });
        $state_item->add_option('ignore', $strings->{ ignore });

        return;
    }

    $state_item->existing($existing->TO_JSON->{ values });

    $state_item->add_default_option('update', $strings->{ update });
    $state_item->add_option('ignore', $strings->{ ignore });
}

=head2 validate

Determine wether given file is suitable for import.

=cut

sub validate {
    my ($self, $file) = @_;

    return $file->mimetype eq 'application/xml';
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 STRINGS

TODO: Fix the POD

=cut

