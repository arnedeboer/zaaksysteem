package Zaaksysteem::Object::Importer::Inav;

use Moose::Role;

use constant CASETYPE_OBJECT_TYPE => 'casetype';

sub handles {
    my $self = shift;
    my $object_type = shift;

    return $object_type eq CASETYPE_OBJECT_TYPE;
}

sub hydrate_from_files { }

sub redirect {
    '/beheer/import/inavigator/upload'
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 handles

TODO: Fix the POD

=cut

=head2 hydrate_from_files

TODO: Fix the POD

=cut

=head2 redirect

TODO: Fix the POD

=cut

