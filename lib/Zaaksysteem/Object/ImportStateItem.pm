package Zaaksysteem::Object::ImportStateItem;

use Moose;

use Zaaksysteem::Tools;

=head1 NAME

Zaaksysteem::Object::ImportStateItem - Encapsulate logic and data for
to-be-imported objects

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head2 id

=cut

has id => (
    is => 'rw'
);

=head2 label

=cut

has label => (
    is => 'rw'
);

=head2 selected_option

=cut

has selected_option => (
    is => 'rw',
    predicate => 'has_selected_option'
);

=head2 default_option

=cut

has default_option => (
    is => 'rw',
    predicate => 'has_default_option'
);

=head2 config

=cut

has config => (
    is => 'rw',
    isa => 'Maybe[HashRef]'
);

=head2 properties

During import the admin wants to inspect details of imported objects.
These are stored here to prevent xml parsing for every request.

=cut

has 'properties' => (
    is => 'rw',
    isa => 'Maybe[HashRef]'
);

=head2 existing

When an object has been imported once already there could be a
reference to it in the local database. This can contain the raw data
of that so the admin can analyse.

=cut

has existing => (
    is => 'rw',
    isa => 'Maybe[HashRef]'
);

=head2 relations

A list of related items.

=cut

has relations => (
    is => 'rw',
    isa => 'Maybe[ArrayRef]'
);

=head2 source_uri

=cut

has source_uri => (
    is => 'rw'
);

=head2 options

This attribute contains all the options available to the user as an array of
hashrefs.

=cut

has options => (
    is => 'rw',
    isa => 'ArrayRef[HashRef]',
    traits => [qw[Array]],
    default => sub { [] },
    handles => {
        push_option => 'push'
    }
);

=head1 METHODS

=head2 get_state

This method returns a hashref that can be safely used for serializing instances
of this object.

    my $serialized_hash = $import_state_item->get_state;

=cut

sub get_state {
    my $self = shift;

    return {
        id => $self->id,
        label => $self->label,
        options => $self->options,
        selected_option => $self->selected_option,
        default_option => $self->default_option,
        source_uri => $self->source_uri,
        config => $self->config,
        properties => $self->properties,
        existing => $self->existing,
        relations => $self->relations,
    }
}

=head2 add_option

This method adds an option to the L</options> attribute and, if no option is
yet selected, selects it.

It returns C<$self> for method-chaining.

    $import_state_item->add_option('ignore', 'Ignore this item')->add_option('create', 'Create this item');

=cut

sub add_option {
    my $self = shift;
    my $option = shift;
    my $label = shift;

    $self->push_option({
        option => $option,
        label => $label
    });

    unless($self->has_selected_option) {
        $self->select_option($option);
    }

    return $self;
}

=head2 add_default_option

This method adds an option to the L</options> attribute and sets it as the
default option for the instance. It also sets the given option as the current
selection.

It returns C<$self> for method-chaining.

    $import_state_item->add_default_option('ignore', 'Ignore this item');

=cut

sub add_default_option {
    my $self = shift;
    my $option = shift;
    my $label = shift;

    $self->add_option($option, $label);
    $self->default_option($option);
    $self->select_option($option);

    return $self;
}

=head2 select_option

This method selects the provided argument as the current option, if it is
available in the L</options> attribute.

Returns C<$self> for method-chaining.

May throw a C<object/import/import_state> exception if the provided argument
cannot be resolved.

    try {
        $import_state_item->select_option('ignore');
    } catch {
        # Do cleanup
    }

=cut

sub select_option {
    my $self = shift;
    my $selection = shift;
    my $config = shift // {};

    my ($option) = grep { $_->{ option } eq $selection } @{ $self->options };

    unless($option) {
        throw('object/import/item_state', sprintf(
            'Attempted to set non-existant option: %s',
            $selection
        ));
    }

    $self->config($config);
    $self->selected_option($selection);

    return $self;
}

=head2 selected

This method returns the currently selected option, if any, as the option's
hashref configuration. If no option is selected it will return the empty list.

    my $option = $import_state_item->selected;

=cut

sub selected {
    my $self = shift;
    my $selected = $self->selected_option;

    return unless $selected;

    my ($option) = grep { $_->{ option } eq $selected } @{ $self->options };

    return $option;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

