package Zaaksysteem::Object::Types::CasetypeACL;
use Moose;
use namespace::autoclean;

with 'MooseX::Log::Log4perl';

extends 'Zaaksysteem::Object';

use Zaaksysteem::Types qw(UUID JSONBoolean);
use Zaaksysteem::Tools;

=head1 NAME

Zaaksysteem::Object::Types::CasetypeACL - Built-in object type implementing
a class for Casetype ACL

=head1 DESCRIPTION

An object class for casetype ACL's for displaying information to external partners.
This object is a very minimalistic.


=head1 ATTRIBUTES

=head2 casetype_uuid

The UUID of the casetype

=cut

has casetype_uuid => (
    is       => 'rw',
    isa      => UUID,
    traits   => [qw(OA)],
    label    => 'UUID of the casetype',
    required => 1,
);

=head2 casetype_active

Boolean to indicate if the casetype is active.

=cut

has casetype_active => (
    is       => 'rw',
    isa      => JSONBoolean,
    traits   => [qw(OA)],
    label    => 'Active casetype',
    required => 1,
    coerce   => 1,
);

=head2 public

Lists all public users.

=cut

has public => (
    is       => 'rw',
    isa      => 'HashRef',
    traits   => [qw(OA)],
    label    => 'Value',
    required => 0,
    default  => sub { return { read => []} },
);

=head2 trusted

Lists all trusted users.

=cut

has trusted => (
    is       => 'rw',
    isa      => 'HashRef',
    traits   => [qw(OA)],
    label    => 'Value',
    required => 0,
    default  => sub { return { read => []} },
);

sub add_user {
    my ($self, $ct_acl) = @_;

    my $level = $ct_acl->confidential ? 'trusted' : 'public';
    my $users = $self->$level;

    # Currently we only have read access
    push(@{$users->{read}}, $ct_acl->username) if defined $ct_acl->username;

    $self->$level($users);

    return 1;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
