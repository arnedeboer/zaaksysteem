package Zaaksysteem::Object::Types::Location;

use Moose;

use Zaaksysteem::Tools;

extends 'Zaaksysteem::Object';

=head1 NAME

Zaaksysteem::Object::Types::Location - Generic type for 'location'-like data.

=head1 DESCRIPTION

This type is a uniform data structure for storing and handling location-like
datums.

=head1 SYNOPSIS

    my $location = Zaaksysteem::Object::Types::Location->new(...);

=head1 ATTRIBUTES

=head2 latitude

WSG84 latitude of the location

=cut

has latitude => (
    is => 'rw',
    isa => 'Num',
    label => 'Breedtegraad',
    traits => [qw[OA]],
    required => 1,
);

=head2 longitude

WSG84 longitude of the location

=cut

has longitude => (
    is => 'rw',
    isa => 'Num',
    label => 'Lengtegraad',
    traits => [qw[OA]],
    required => 1
);

=head2 label

Optional human-readable reference to the location.

=cut

has label => (
    is => 'rw',
    isa => 'Str',
    label => 'Label',
    traits => [qw[OA]],
    predicate => 'has_label'
);

=head2 address

Embedded reference to a L<Zaaksysteem::Object::Types::Address> instance.

=cut

has address => (
    is => 'rw',
    isa => 'Zaaksysteem::Object::Types::Address',
    embed => 1,
    label => 'Adres',
    traits => [qw[OR]],
    predicate => 'has_address'
);

=head1 METHODS

=head2 coordinates

Stringifies the L</latitude> and L</longitude> fields as a 'point' pair.

=cut

sub coordinates {
    my $self = shift;

    return join(', ', $self->latitude, $self->longitude)
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
