package Zaaksysteem::Object::Types::SavedSearch;

use Moose;

extends 'Zaaksysteem::Object';
with 'Zaaksysteem::Object::Roles::Security';

use Zaaksysteem::Types qw[Boolean NonEmptyStr];

=head1 NAME

Zaaksysteem::Object::Types::SavedSearch - Represent user-defined ZQL searches

=head1 DESCRIPTION

=head1 PUBLIC ATTRIBUTES

This is the list of public interfaces to objects of this type

=head2 title

A generic title for the saved search

=cut

has title => (
    is => 'rw',
    isa => NonEmptyStr,
    traits => [qw[OA]]
);

=head2 query

This attribute contains a object-tree of keys and values used by the frontend
to hydrate the object search screen, it is 1-1 equivalent to the ZQL also
stored in this object.

=cut

has query => (
    is => 'rw',
    isa => NonEmptyStr,
    traits => [qw[OA]],
    required => 1
);

=head2 public

This attribute indicates if the object can be used by externally coupled
systems.

=cut

has public => (
    is => 'rw',
    isa => Boolean,
    coerce => 1,
    traits => [qw[OA]],
    type => 'checkbox'
);

=head1 METHODS

=head2 TO_STRING

Returns the title of the saved search.

See L<Zaaksysteem::Object/TO_STRING>.

=cut

override TO_STRING => sub {
    return shift->title;
};

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

