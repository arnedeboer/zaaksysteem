package Zaaksysteem::Object::Types::Case;

use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Object';

with 'Zaaksysteem::Object::Roles::Relation';

=head1 NAME

Zaaksysteem::Object::Types::Case - Object type representing "Cases"

=head1 DESCRIPTION

Implements specific code behaviors exclusive to object instances with class
'case'.

=head1 CONSTANTS

=head2 INDEX_ATTRIBUTES

This constant holds a list of attribute names to be used as indexables.

=cut

use constant INDEX_ATTRIBUTES => [qw[
    case.requestor.full_name
    case.requestor.family_name
    case.requestor.bsn
    case.requestor.email
    case.requestor.name
    case.recipient.full_name
    case.recipient.family_name
    case.recipient.bsn
    case.recipient.email
    case.assignee
    case.assignee.email
    case.assignee.phone_number
    case.coordinator

    case.number
    case.subject
    case.date_of_registration
    case.date_of_completion

    case.casetype.name
]];

=head2 ACTIONS

This constant holds a list of actions that can be performed on a case.

=cut

use constant ACTIONS => [
    { slug => 'allocate' },
    { slug => 'acquire' },
    { slug => 'suspend' },
    { slug => 'resume' },
    { slug => 'prolong' },
    { slug => 'relate' },
    { slug => 'manage' },
    { slug => 'destroy' },
    { slug => 'publish' },
    { slug => 'export' }
];

=head1 ATTRIBUTES

=cut

has 'case.number' => (
    is => 'rw',
    isa => 'Int',
    traits => [qw[OA]],
    accessor => 'case_number'
);

has 'case.casetype.name' => (
    is => 'rw',
    isa => 'Str',
    traits => [qw[OA]],
    accessor => 'case_casetype_name'
);

has 'case.casetype.node.id' => (
    is => 'rw',
    isa => 'Num',
    traits => [qw[OA]],
    accessor => 'case_casetype_node_id'
);

has 'case.casetype.id' => (
    is => 'rw',
    isa => 'Num',
    traits => [qw[OA]],
    accessor => 'case_casetype_id'
);

=head2 actions

This attribute inherits from L<Object|Zaaksysteem::Object>, and overrides
the default value to be the constant L</ACTIONS>.

=cut

has '+actions' => (
    default => sub {
        [ map { $_->{ slug } } @{ ACTIONS() } ];
    }
);

=head1 METHODS

=head2 index_attributes

This method overrides from L<Object|Zaaksysteem::Object>, and dumps the
L</INDEX_ATTRIBUTES> constant's values instead. Cases have a well-defined
list of indexable columns "set" in stone.

=cut

override index_attributes => sub {
    my $self = shift;

    return @{ INDEX_ATTRIBUTES() };
};

override TO_STRING => sub {
    my $self = shift;

    return sprintf('%d %s', $self->case_number, $self->case_casetype_name);
};

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

