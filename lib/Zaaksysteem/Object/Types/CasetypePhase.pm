package Zaaksysteem::Object::Types::CasetypePhase;

use Moose;

extends 'Zaaksysteem::Object';

=head1 NAME

Zaaksysteem::Object::Types::CasetypePhase - Data wrapper for casetype 'phase'
objects.

=head1 ATTRIBUTES

=head2 casetype_phase_id

Reference to the underlying/old C<zaaktype_status> row.

=cut

has casetype_phase_id => (
    is => 'rw',
    isa => 'Int',
    type => 'integer',
    label => 'Zaaktypefase ID',
    traits => [qw[OA]],
    required => 1
);

=head2 label

Defines the present continuous label for this phase.

=cut

has label => (
    is => 'rw',
    isa => 'Str',
    type => 'text',
    label => 'Fase',
    traits => [qw[OA]],
    required => 1
);

=head2 name

Defines the adjective label for this phase.

=cut

has name => (
    is => 'rw',
    isa => 'Str',
    type => 'text',
    label => 'Naam',
    traits => [qw[OA]],
    required => 1
);

=head2 sequence

Serial integer defining the order which the phases appear in the object.

=cut

has sequence => (
    is => 'rw',
    isa => 'Int',
    type => 'integer',
    label => 'Statusnummer',
    traits => [qw[OA]],
    required => 1
);

=head2

When an object is transitioned from this phase to the next, the route is used
to re-assign the object to another position (group/role).

=cut

has route => (
    is => 'rw',
    isa => 'HashRef',
    traits => [qw[OA]],
);

=head2

Contains a set of user-defined attribute references and configs.

=cut

has attributes => (
    is => 'rw',
    isa => 'ArrayRef',
    label => 'Attributen',
    traits => [qw[OA]],
);

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
