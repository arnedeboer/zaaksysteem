package Zaaksysteem::Object::Types::CustomerConfig;
use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Object';
with qw(Zaaksysteem::Object::Roles::Relation);

use Zaaksysteem::Types qw(FQDN zs_ipv4);
use Zaaksysteem::Tools;

=head1 NAME

Zaaksysteem::Object::Types::CustomerConfig - Built-in object type implementing
a class for customer configuration entries.

=head1 DESCRIPTION

CustomerConfig objects are objects in which generic customer related information is stored.

=head1 ATTRIBUTES

=head2 customer_id

customer id van de klant

=cut

has customer_id => (
    is       => 'ro',
    isa      => 'Str',
    traits   => [qw(OA)],
    label    => 'Klant ID',
    required => 1,
    unique   => 1,
);

=head2 template

Default template

=cut

has template => (
    is       => 'ro',
    isa      => 'Str',
    traits   => [qw(OA)],
    label    => 'Template',
    required => 1,
);


=head2 domain

Domain van de klant

=cut

has domain => (
    is       => 'ro',
    isa      => FQDN,
    traits   => [qw(OA)],
    label    => 'Domein',
    required => 1,
    unique   => 1,
);

=head2 naam_lang

Officiele naam van de klant (eg. Gemeente Bussum).

=cut

has naam_lang => (
    is     => 'ro',
    isa    => 'Str',
    traits => [qw(OA)],
    label  => 'Officiele naam',
);

=head2 naam_kort

naam_kort

=cut

has naam_kort => (
    is     => 'ro',
    isa    => 'Str',
    traits => [qw(OA)],
    label  => 'Naam kort'
);

=head2 woonplaats

Woonplaats

=cut

has woonplaats => (
    is     => 'ro',
    isa    => 'Str',
    traits => [qw(OA)],
    label  => 'Woonplaats'
);

=head2 straatnaam

straatnaam

=cut

has straatnaam => (
    is     => 'ro',
    isa    => 'Str',
    traits => [qw(OA)],
    label  => 'straatnaam'
);

=head2 huisnummer

Huisnummer

=cut

has huisnummer => (
    is     => 'ro',
    isa    => 'Str',
    traits => [qw(OA)],
    label  => 'Huisnummer'
);

=head2 postcode

Postcode

=cut

has postcode => (
    is     => 'ro',
    isa    => 'Str',
    traits => [qw(OA)],
    label  => 'postcode'
);

=head2 postbus

postbus

=cut

has postbus => (
    is      => 'ro',
    isa     => 'Str',
    traits  => [qw(OA)],
    label   => 'postbus',
    default => '',
);

=head2 postbus_postcode

postbus_postcode

=cut

has postbus_postcode => (
    is      => 'ro',
    isa     => 'Str',
    traits  => [qw(OA)],
    label   => 'postbus_postcode',
    default => '',
);

=head2 website

website

=cut

has website => (
    is     => 'ro',
    isa    => 'Str',
    traits => [qw(OA)],
    label  => 'website'
);

=head2 email

email

=cut

has email => (
    is     => 'ro',
    isa    => 'Str',
    traits => [qw(OA)],
    label  => 'email'
);

=head2 telefoonnummer

telefoonnummer

=cut

has telefoonnummer => (
    is     => 'ro',
    isa    => 'Str',
    traits => [qw(OA)],
    label  => 'telefoonnummer'
);

=head2 faxnummer

faxnummer

=cut

has faxnummer => (
    is     => 'ro',
    isa    => 'Str',
    traits => [qw(OA)],
    label  => 'faxnummer'
);

=head2 latitude

latitude

=cut

has latitude => (
    is     => 'ro',
    isa    => 'Str',
    traits => [qw(OA)],
    label  => 'latitude'
);

=head2 longitude

longitude

=cut

has longitude => (
    is     => 'ro',
    isa    => 'Str',
    traits => [qw(OA)],
    label  => 'longitude'
);

=head2 gemeente_id_url

gemeente_id_url

=cut

has gemeente_id_url => (
    is     => 'ro',
    isa    => 'Str',
    traits => [qw(OA)],
    label  => 'gemeente_id_url'
);

=head2 gemeente_portal

gemeente_portal

=cut

has gemeente_portal => (
    is     => 'ro',
    isa    => 'Str',
    traits => [qw(OA)],
    label  => 'gemeente_portal'
);

=head2 cloud

The cloud the customer is configured on

=cut

has cloud => (
    is      => 'ro',
    isa     => 'Str',
    traits  => [qw[OA]],
    label   => 'Cloud',
    default => 'commercial',
);

=head2 ad_key

The active directory key used for AD synchronisation

=cut

has ad_key => (
    is     => 'ro',
    isa    => 'Str',
    traits => [qw[OA]],
    label  => 'Active Directory key'
);

=head2 production_url

The production URL of the customer

=cut

has production_url => (
    is      => 'ro',
    isa     => 'Str',
    traits  => [qw[OA]],
    label   => 'Production URL',
    unique  => 1,
);

=head2 production_ip

The IP address on which the URL is configured.

=cut

has production_ip => (
    is      => 'ro',
    isa    => zs_ipv4,
    traits  => [qw[OA]],
    label   => 'Production IP',
);

=head2 accept_url

The accept URL of the customer

=cut

has accept_url => (
    is     => 'ro',
    isa    => 'Str',
    traits => [qw[OA]],
    label  => 'Accept URL',
    unique => 1,
);

=head2 accept_ip

The IP address on which the accept URL is configured.

=cut

has accept_ip => (
    is     => 'ro',
    isa    => zs_ipv4,
    traits => [qw[OA]],
    label  => 'Accept IP',
);

=head2 new_empty

This instantiator returns a new instance of a generic type, with enough
default variables to make it validate. It can be used to save a temporary
ObjectType that will be edited later. It is also used by the form API for
creating empty forms.

=cut

sub new_empty {
    my $self = shift;

    $self->new();

}

=head2 relatable_types

Returns a list of types that can be related to a CustomerConfig object

=cut

sub relatable_types {
    return ('customer_d');
}

=head2 add_customer_d

Shortcut method for adding a relationship with a CustomerD object.

Dies when you want to set a relationship if there is already an existing relationship with another CustomerConfig object.

=cut

sub add_customer_d {
    my ($self, $customer_d) = @_;

    my $existing_relation = $customer_d->relations();
    if (@$existing_relation > 0) {
        return 1 if $existing_relation->[-1]->related_object_id eq $self->id;
        throw('/customerconfig/set_parent', 'Existing relationship, unable to set new relationship');
    }

    return $self->relate(
        $customer_d,
        relationship_name_a => 'mother',
        relationship_name_b => 'child',
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
