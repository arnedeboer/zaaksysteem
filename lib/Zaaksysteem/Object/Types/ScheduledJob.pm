package Zaaksysteem::Object::Types::ScheduledJob;
use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Object';

with qw[
    Zaaksysteem::Object::Roles::Log
    Zaaksysteem::Object::Roles::Relation
    Zaaksysteem::Object::Roles::Security
];

use Zaaksysteem::Types qw(IntervalStr PackageElementStr Timestamp);

=head1 NAME

Zaaksysteem::Object::Types::ScheduledJob - Built-in object type implementing a scheduled job

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head2 job

Type of job that's scheduled (usually "case_create")

This is validated as a L<PackageElement|Zaaksysteem::Types/PackageElementStr>
to prevent namespace/escaping attacks.

=cut

has job => (
    is     => 'rw',
    isa    => PackageElementStr,
    traits => [qw[OA]],
    label  => 'Job',
);

=head2 next_run

Timestamp of next planned run.

=cut

has next_run => (
    is     => 'rw',
    isa    => Timestamp,
    coerce => 1,
    traits => [qw[OA]],
    label  => 'Volgende instantie',
);

=head2 interval_period

Time period between runs. The job will run every C<interval_value>
C<interval_period>s.

The allowed periods are specified in
L<IntervalStr|Zaaksysteem::Types/IntervalStr>.

=cut

has interval_period => (
    is     => 'rw',
    isa    => IntervalStr,
    traits => [qw[OA]],
    label  => 'Terugkeerpatroon (waarde)',
    clearer => 'clear_interval_period',
);

=head2 interval_value

Units of "interval_period" between runs.

=cut

has interval_value => (
    is     => 'rw',
    isa    => 'Maybe[Int]',
    traits => [qw[OA]],
    label  => 'Type terugkeerpatroon',
    clearer => 'clear_interval_value',
);

=head2 runs_left

Number of times this job will run.

=cut

has runs_left => (
    is     => 'rw',
    isa    => 'Int',
    traits => [qw[OA]],
    label  => 'Aantal herhalingen',
    predicate => 'has_runs_left',
    clearer => 'clear_runs_left',
);

=head2 interface_id

Optional reference to the interface row that is associated with this job.

=cut

has interface_id => (
    is => 'rw',
    isa => 'Int',
    traits => [qw[OA]],
    label => 'Koppeling configuratie ID',
    predicate => 'has_interface_id'
);

=head2 data

Storage for arbitrary data, to be interpreted by the loaded job role.

Exposes C<has_data> and C<clear_data>, and the generic accessor C<data>.

=cut

has data => (
    is => 'rw',
    predicate => 'has_data',
    traits => [qw[OA]],
    clearer => 'clear_data'
);

=head2 new_empty

This instantiator returns a new instance of a generic type, with enough
default variables to make it validate. It can be used to save a temporary
ObjectType that will be edited later. It is also used by the form API for
creating empty forms.

=cut

sub new_empty {
    my $self = shift;

    $self->new(
        job             => 'DummyJob',
        next_run        => DateTime->now(),
        interval_period => 'once',
        interval_value  => undef,
        runs_left       => 0,
    );
}

=head2 setup_next_run

Calculate the next run date, and set the attributes (C<runs_left> and
C<next_run>) accordingly.

If there is no next run, a true value will be returned, and the object will not
be updated.

=cut

sub setup_next_run {
    my $self = shift;

    return 0 if $self->interval_period eq 'once';

    if ($self->has_runs_left) {
        return 0 if $self->runs_left <= 1;

        $self->runs_left($self->runs_left - 1);
    }

    my $next = $self->next_run->clone;
    $next->add( $self->interval_period => $self->interval_value );

    $self->next_run($next);

    return $self->has_runs_left ? $self->runs_left : 1;
}

=head2 relatable_types

Returns a list of types that can be related to a ScheduledJob

=cut

sub relatable_types {
    return undef;
}

sub type { return "scheduled_job" }

=head2 is_deleteable

Scheduled jobs can always be deleted, so this method is overridden to always return true.

=cut

around is_deleteable => sub {
    return 1;
};

=head2 TO_STRING

Stringification of a scheduled job.

This may stop working when other job types than "CreateCase" are built.

=cut

sub TO_STRING {
    my $self = shift;

    if ($self->has_data && exists $self->data->{ label }) {
        return $self->data->{ label }
    }

    my ($casetype_rel) = $self->filter_relations(sub {
        $_->related_object_type eq 'casetype'
    });

    return '' unless $casetype_rel;
    return $casetype_rel->relationship_title_b;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 type

TODO: Fix the POD

=cut

