package Zaaksysteem::Object::Types::CustomerD;

=head1 NAME

Zaaksysteem::Object::Types::CustomerD - Built-in object type implementing
a class for customer.d configuration entries.

=head1 DESCRIPTION

=cut

use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Object';
with qw(Zaaksysteem::Object::Roles::Relation);

use Zaaksysteem::Types qw(ZSNetworkACLs JSONBoolean Timestamp zs_ipv4 zs_ipv6 FQDN ZSvhosts);
use Zaaksysteem::Tools;

=head1 ATTRIBUTES

=head2 fqdn

Fully qualified domainname

=cut

has fqdn => (
    is       => 'rw',
    isa      => FQDN,
    traits   => [qw[OA]],
    label    => 'Fully Qualified Domain Name',
    unique   => 1,
    required => 1,
);

=head2 ipv4

The IPv4 address of the instance

=cut

has ipv4 => (
    is     => 'rw',
    isa    => zs_ipv4,
    traits => [qw[OA]],
    label  => 'IPv4 address',
);

=head2 ipv6

The IPv6 address of the instance. Zaaksysteem has not been tested on IPv6, YMMV.

=cut

has ipv6 => (
    is     => 'rw',
    isa    => zs_ipv6,
    traits => [qw[OA]],
    label  => 'IPv6 address',
);

=head2 label

Human-readxiaible name that identifies the customer.d entry

=cut

has label => (
    is       => 'rw',
    isa      => 'Str',
    traits   => [qw[OA]],
    label    => 'Label',
    required => 1,
);

=head2 owner

The customer id

=cut

has owner => (
    is       => 'rw',
    isa      => 'Str',
    traits   => [qw[OA]],
    label    => 'Owner',
    required => 1,
);

=head2 certificate

ASCII-armored X.509 certificate text

=cut

has certificate => (
    is     => 'rw',
    isa    => 'Str',
    traits => [qw[OA]],
    label  => 'SSL certificate',
);

=head2 key

ASCII-armored key

=cut

has key => (
    is     => 'rw',
    isa    => 'Str',
    traits => [qw[OA]],
    label  => 'SSL key',
);

=head2 release

The release of the instance, defaults to 'accept'

=cut

has release => (
    is       => 'rw',
    isa      => 'Str',
    traits   => [qw[OA]],
    label    => 'Release',
    default  => 'accept',
    required => 1,
);

=head2 aliases

The aliases for this instance

=cut

has aliases => (
    is     => 'rw',
    isa    => ZSvhosts,
    traits => [qw[OA]],
    label  => 'Server aliases',
);

=head2 cloud

The cloud on which the customer is provisioned, defaults to 'commercial'

=cut

has cloud => (
    is       => 'rw',
    isa      => 'Str',
    traits   => [qw[OA]],
    label    => 'Cloud',
    default  => 'commercial',
    required => 1,
);

=head2 loadbalancer

The loadbalancer which serves the instance, defaults to 'accept'

=cut

has loadbalancer => (
    is       => 'rw',
    isa      => 'Str',
    traits   => [qw[OA]],
    label    => 'Loadbalancer',
    required => 1,
    default  => 'accept',
);

=head2 ldap

The LDAP details

=cut

has ldap => (
    is     => 'rw',
    isa    => 'Str',
    traits => [qw[OA]],
    label  => 'LDAP',
);

=head2 fallback

The fallback URL

=cut

has fallback => (
    is     => 'rw',
    isa    => 'Str',
    traits => [qw[OA]],
    label  => 'Fallback URL',
);

=head2 deleted

Boolean to indicate that this customer config may be deleted from the platform, defaults to 'false'

=cut

has deleted => (
    is      => 'rw',
    isa     => JSONBoolean,
    traits  => [qw[OA]],
    label   => 'Deleted',
    default => 0,
    coerce  => 1,
);

=head2 disabled

Boolean to indicate that this customer config is disabled, defaults to 'true'

=cut

has disabled => (
    is      => 'rw',
    isa     => JSONBoolean,
    traits  => [qw[OA]],
    label   => 'Disabled',
    default => 1,
    coerce  => 1,
);

=head2 delete_on

DateTime value to trigger the actual deletion of the object from the technical platform

=cut

has delete_on => (
    is     => 'rw',
    isa    => Timestamp,
    coerce => 1,
    traits => [qw[OA]],
    label  => 'Delete on',
);

=head2 provisioned_on

DateTime value of the last provisioning action

=cut

has provisioned_on => (
    is     => 'rw',
    isa    => Timestamp,
    coerce => 1,
    traits => [qw[OA]],
    label  => 'Provisioned on',
);

=head2 origin

The origin is from what source the Customer D configuration is provisioned from.
Possible values:

=over

=item * Production

=item * Accept

=item * Template

=back

=cut

has origin => (
    is     => 'rw',
    isa    => 'Str',
    traits => [qw[OA]],
    label  => 'Origin',
);

=head2 network_acl

An arrayref for network ACL's, or Toegangsbeheer in Dutch.

=cut

has network_acl => (
    is     => 'rw',
    isa    => ZSNetworkACLs,
    traits => [qw[OA]],
    label  => 'Toegangsbeheer',
);

=head2 database

An predefined name for the database

=cut

has database => (
    is     => 'ro',
    isa    => 'Str',,
    traits => [qw[OA]],
    label  => 'Databasename',
);

=head2 ldap_name

An predefined name for LDAP

=cut

has ldap_name => (
    is     => 'ro',
    isa    => 'Str',,
    traits => [qw[OA]],
    label  => 'LDAP name',
);

=head2 template

The template to use for this instance.

=cut

has template => (
    is     => 'ro',
    isa    => 'Str',,
    traits => [qw[OA]],
    label  => 'Template name',
);

has [qw(ldap_provisioned database_provisioned filestore_provisioned ssl_provisioned)] => (
    is     => 'rw',
    isa    => Timestamp,
    coerce => 1,
    traits => [qw(OA)],
    label  => "Provisioned subpart",
);

=head2 new_empty

This instantiator returns a new instance of a generic type, with enough
default variables to make it validate. It can be used to save a temporary
ObjectType that will be edited later. It is also used by the form API for
creating empty forms.

=cut

sub new_empty {
    my $self = shift;

    $self->new(
        fqdn  => '',
        label => '',
        owner => '',
    );

}


=head2 relatable_types

Returns a list of types that can be related to a CustomerD object

=cut

sub relatable_types {
    return ('customer_config');
}

=head2 add_to_customer

Shortcut method for adding a relationship with a CustomerConfig object.

Dies when you want to set a relationship if there is already an existing relationship with another CustomerConfig object.

=cut

sub add_to_customer {
    my ($self, $customer_config) = @_;

    my $existing_relation = $self->relations();
    if (@$existing_relation > 0) {
        return 1 if $existing_relation->[-1]->related_object_id eq $customer_config->id;
        throw('/customerd/set_parent', 'Existing relationship, unable to set new relationship');
    }

    return $self->relate(
        $customer_config,
        relationship_name_a => 'child',
        relationship_name_b => 'mother',
    );

}



__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
