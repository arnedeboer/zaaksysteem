package Zaaksysteem::Object::Types::Type;

use Moose;
use namespace::autoclean;

use Moose::Meta::Class;

use Zaaksysteem::Types qw(RelatedCaseTypes);
use Zaaksysteem::Tools;
use Zaaksysteem::Search::ZQL;
use JSON qw[encode_json];

use Zaaksysteem::ZTT;
use Zaaksysteem::ZTT::Element;

use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::ZAPI::Form::FieldSet;
use Zaaksysteem::ZAPI::Form::Field;

extends 'Zaaksysteem::Object';

with qw[
    Zaaksysteem::Object::Roles::MetaType
    Zaaksysteem::Object::Roles::Interfaceable
];

=head1 NAME

Zaaksysteem::Object::Type - The infamously hard-to-name Used-defined ObjectType-Type

=head1 DESCRIPTION

This built-in object-type encodes the validation profile for in-database
objects with C<object_class = 'type'>. It's main function is validation and
wrapping of the normalized JSON representation of ObjectTypes to and fro
the frontend.

=head1 ATTRIBUTES

=head2 name

This attribute holds the name of a user-defined type in a human-readable
format.

=cut

has name => (
    traits   => [qw[OA]],
    is       => 'rw',
    isa      => 'Str',
    label    => 'Naam Object-type',
    type     => 'text',
    required => 1,
    index    => 1
);

=head2 prefix

This is the internal name that will end up in the C<object_class> of the
instances of this ObjectType.

If not explicitly passed to the constructor of this class, it will be
heuristically deduced from the type name, by replacing all non-word characters
characters with C<_>, and lower-casing the entire string.

=cut

has prefix => (
    traits   => [qw[OA]],
    is       => 'rw',
    isa      => 'Str',
    label    => 'Prefix',
    type     => 'text',
    required => 1,
    lazy     => 1,
    unique   => 1,
    index    => 1,
    default  => sub {
        my $name = lc(shift->name);
        $name =~ s/[^\w\.]/_/ga;
        return $name;
    }
);

=head2 title_template

This attribute can be filled with a L<Zaaksysteem::ZTT> plaintext template
that can be used to implement a C<TO_STRING> method on instances of types.

=cut

has title_template => (
    traits    => [qw[OA]],
    is        => 'rw',
    isa       => 'Str',
    label     => 'Titel template',
    predicate => 'has_title_template',
    type      => 'text'
);

=head2 instance_authorizations

This attribute stores a simple representation of the permissions that are to
be used for the instances of this type. These items are synched with the ACL
table(s) and can be used by instances of this type for 'type-based'
permissions and proscriptions.

=cut

has instance_authorizations => (
    traits   => [qw[Array OA]],
    is       => 'rw',
    isa      => 'ArrayRef',
    default  => sub { return [] },
    type     => 'org-unit',
    label    => 'Rechten'
);

=head2 related_casetypes

This attribute holds an array of references to casetypes that instances of
this ObjectType can use for quick reference in the UI. It serves no major
infrastructural purpose otherwise.

=cut

has related_casetypes => (
    traits   => [qw[Array OA]],
    is       => 'rw',
    isa      => 'ArrayRef[HashRef]',
    type     => 'spot-enlighter',
    label    => 'Zaaktyperelaties',
    default  => sub { return [] },

    data     => {
        restrict => 'casetype'
    },

    handles => {
        all_related_casetypes => 'elements',
        map_related_casetypes => 'map',
        has_related_casetypes => 'count'
    }
);

=head2 related_objecttypes

=cut

has related_objecttypes => (
    traits  => [qw[Array OA]],
    is      => 'rw',
    isa     => 'ArrayRef[HashRef]',
    default => sub { return [] },
    label   => 'Objecttyperelaties',

    handles => {
        all_related_objecttypes => 'elements',
        map_related_objecttypes => 'map',
        has_related_objecttypes => 'count'
    }
);

=head2 attributes

This attribute is the meat of instances of this package. It holds and array of
references to all the attributes instances of this type will present to the
frontend for editing and creating instances of the type implemented by this
instance.

=cut

has attributes => (
    traits   => [qw[Array OA]],
    is       => 'rw',
    isa      => 'ArrayRef',
    default  => sub { return [] },
    type     => 'object-attribute-list',
    label    => 'Kenmerken',
    handles  => {
        all_attributes => 'elements',
        map_attributes => 'map',
        add_attribute  => 'push',
        filter_attributes => 'grep'
    }
);

=head2 modified_sections

=cut

has modified_sections => (
    traits   => [qw[OA]],
    is       => 'rw',
    isa      => 'ArrayRef[Str]',
    default  => sub { [] },
    type     => 'checkbox',
    label    => 'Componenten gewijzigd',
    #options  => [
    #    { name => 'default', label => 'Algemeen' },
    #    { name => 'relations', label => 'Relaties' },
    #    { name => 'instance_authorizations', label => 'Rechten' }
    #]
);

=head2 modification_rationale

This attribute holds a textual string that is intended to be filled with a
simple, human-readable rationale for updating or creating the objecttype.

=cut

has modification_rationale => (
    traits   => [qw[OA]],
    is       => 'rw',
    isa      => 'Str',
    default  => '',
    type     => 'textarea',
    label    => 'Wijzigingsomschrijving'
);

=head2 category_id

This domain-crossing attribute holds the id of the category the item is placed
in.

This attribute should not exist in the object, but there is no overlaying
infrastructure that abstracts the category away as a role on object types.

=cut

has category_id => (
    traits   => [qw[OA]],
    is       => 'rw',
    isa      => 'Int',
    type     => 'category',
    label    => 'Categorie'
);

=head1 INSTANTIATORS

=head2 new_empty

This instantiator returns a new instance of a generic type, with enough
default variables to make it validate. It can be used to save a temporary
ObjectType that will be edited later. It is also used by the form API for
creating empty forms.

=cut

sub new_empty {
    my $class = shift;

    return $class->new(
        name => '',
        prefix => ''
    );
}

=head1 ACTIONS

These methods represent API-callable actions that can be run on instances of
this type.

=head2 instance_form

This action builds a L<Zaaksysteem::ZAPI::Form> based on the user-defined type
contained in instances of the Type objecttype.

=head3 URL

C</api/object/[UUID]/action/instance_form>

=head3 Example output

    {
        "next" : null,
        "status_code" : "200",
        "prev" : null,
        "num_rows" : 1,
        "rows" : 1,
        "comment" : null,
        "at" : null,
        "result" : [
            {
                "actions" : [],
                "options" : {},
                "name" : "plantenbak",
                "fieldsets" : [
                    {
                    "actions" : [],
                    "fields" : [
                        {
                            "when" : null,
                            "name" : "naam",
                            "default" : null,
                            "data" : {
                                "options" : []
                            },
                            "description" : null,
                            "required" : true,
                            "type" : "text",
                            "label" : "Naam"
                        },
                        {
                            "when" : null,
                            "name" : "locatie",
                            "default" : null,
                            "data" : {
                                "options" : []
                            },
                            "description" : null,
                            "required" : false,
                            "type" : "bag_adres",
                            "label" : "Locatie"
                        },
                        {
                            "when" : null,
                            "name" : "opties",
                            "default" : null,
                            "data" : {
                                "options" : [
                                {
                                    "name" : "Optie 1",
                                    "label" : "Optie 1"
                                },
                                {
                                    "name" : "Optie B",
                                    "label" : "Optie B"
                                },
                                {
                                    "name" : "Optie Z,
                                    "label" : "Optie Z"
                                }
                                ]
                            },
                            "description" : null,
                            "required" : false,
                            "type" : "checkbox",
                            "label" : "Opties"
                        }
                    ],
                    "name" : "plantenbak-fieldset",
                    "title" : "Plantenbak",
                    "description" : "Autogenerated fieldset"
                    }
                ]
            }
        ]
    }

=cut

sub instance_form {
    my $self = shift;
    my $c = shift;

    # Build up form fields
    my @fields = $self->map_attributes(sub {
        my $cat_attr = $c->model('DB::BibliotheekKenmerken')->find($_->{ attribute_id });

        unless(defined $cat_attr) {
            throw('object/type/instance_form', sprintf(
                'Unable to find BibliotheekKenmerken with id "%s"',
                $_->{ attribute_id }
            ));
        }

        my @options = map { { value => $_->value, label => $_->value } }
            $cat_attr->bibliotheek_kenmerken_values->search({ active => 't' })->all;

        return Zaaksysteem::ZAPI::Form::Field->new(
            name        => sprintf('attribute.%s', $cat_attr->magic_string),
            type        => $cat_attr->value_type,
            label       => $_->{ label } || $_->{ attribute_label } || $cat_attr->label,
            default     => $cat_attr->value_default,
            description => $_->{ internal_description } || $cat_attr->help,
            required    => $_->{ required },
            data        => { options => \@options }
        );
    });

    my $form = Zaaksysteem::ZAPI::Form->new(
        name => $self->prefix,
        fieldsets => [ Zaaksysteem::ZAPI::Form::FieldSet->new(
            name => $self->prefix . '-fieldset',
            title => $self->name,
            description => 'Autogenerated fieldset',
            fields => \@fields
        ) ]
    );

    return [ $form ];
}

=head2 validate

=cut

define_profile validate => (
    required => [qw[object]]
);

sub validate {
    my $self = shift;
    my $c = shift;

    my $params = assert_profile($c->req->params)->valid;
    my $model = $c->model('Object');

    my $object = $model->inflate_from_json(encode_json($params->{ object }));

    my $rs = $self->get_update_conflicts_rs($model, $object);

    return Zaaksysteem::Object::Iterator->new(
        rs => $rs,
        inflator => sub { $model->inflate_from_row(shift) }
    );
}

=head1 METHODS

=head2 instance_type

Implements required interface for L<Zaaksysteem::Object::Roles::MetaType>.

Returns L</prefix>.

=cut

sub instance_type {
    return shift->prefix;
}

=head2 instance_superclass

Implements required interface for L<Zaaksysteem::Object::Roles::MetaType>.

Hardcoded to return the string C<Zaaksysteem::Object>.

=cut

sub instance_superclass {
    return 'Zaaksysteem::Object::TypeMetaClass'
}

=head2 instance_roles

Implements required interface for L<Zaaksysteem::Object::Roles::MetaType>.

Returns a list of roles that should be consumed by any metaclass derived from
this type.

=cut

sub instance_roles {
    my $self = shift;

    my @roles;

    if ($self->has_interfaces) {
        push @roles, 'Zaaksysteem::Object::Roles::Synchronizable';
    }

    return @roles;
}

=head2 instance_attributes

Implements required interface for L<Zaaksysteem::Object::Roles::MetaType>.

Should return a list of object attribute definitions in HashRef form.

=cut

sub instance_attributes {
    shift->all_attributes;
}

=head2 instance_relations

Implements required interface for L<Zaaksysteem::Object::Roles::MetaType>.

Should return a list of object relation definitions in HashRef form.

=cut

sub instance_relations { }

=head2 instance_meta_class

Builds a L<Moose::Meta::Class> instance based on the type. This implementation
extends L<Zaaksysteem::Object::Roles::MetaType/instance_meta_role>.

=cut

around instance_meta_class => sub {
    my $orig = shift;
    my $self = shift;

    my $meta = $self->$orig(@_);

    if ($self->has_title_template) {
        my $template = $self->title_template;

        $meta->add_around_method_modifier(TO_STRING => sub {
            my $orig = shift;
            my $self = shift;

            my $ztt = Zaaksysteem::ZTT->new->add_context($self);
            my $tpl = $ztt->process_template($template);
            my $str = $tpl->string;

            return $str || $self->$orig(@_);
        });
    }

    if ($self->has_related_objecttypes) {
        # Dereference objecttypes so closure does not reference the Type
        # instance object.
        my @related_objecttypes = $self->map_related_objecttypes(sub {
            return $_->{ object_type };
        });

        $meta->add_around_method_modifier(relatable_types => sub {
            my $orig = shift;
            my $self = shift;

            return $self->$orig, @related_objecttypes;
        });
    }

    if($self->has_related_casetypes) {
        my @inherited_relations = map {
            my %args = (%{ $_ }, is_inherited => 1);

            delete $args{ related_object };

            Zaaksysteem::Object::Relation->new(%args);
        } grep {
            not $_->{ related_object_id } =~ m[^\d+$]
        } $self->all_related_casetypes;

        # Silly Moose... this MM *must* be an around, where override would
        # make more contextual sense, but since the method is defined in a
        # directly consumed role, we cannot 'replace' it as such (without
        # resorting to stuff like subclassing $meta so we *can* override
        # build_inherited_relations...)
        $meta->add_method(build_inherited_relations => sub {
            return \@inherited_relations;
        });
    }

    if ($self->has_interfaces) {
        my @interfaces = $self->all_interfaces;

        $meta->add_method(build_synchronization_interfaces => sub {
            return \@interfaces;
        })
    }

    # Commented here because of this explicit reason: DO NOT USE make_immutable, its slow...
    # $meta->make_immutable;

    return $meta;
};

=head2 model_hook

=cut

override model_hook => sub {
    my ($self, $event, @args) = @_;

    if ($event eq 'update') {
        my ($model, $new_object) = @args;

        $model->invalidate($self->get_update_conflicts_rs($model, $new_object));
    }

    return;
};

=head2 get_update_conflicts_rs

=cut

sig get_update_conflicts_rs => 'Zaaksysteem::Object::Model, Zaaksysteem::Object';

sub get_update_conflicts_rs {
    my $self = shift;
    my $model = shift;
    my $new_object = shift;
    my $rs = $model->new_resultset;

    my @required_attrs;

    for my $attr ($new_object->all_attributes) {
        if ($attr->{ required }) {
            my ($name) = $attr->{ name } =~ m[^attribute\.(.*)$];

            push @required_attrs, $name;
        }
    }

    # If there are no attributes where values are required, return a query
    # that cannot match any rows.
    unless (scalar @required_attrs) {
        return $rs->search_rs({ object_class => undef });
    }

    # Handcrafted ZQL because $model->search(...) does not support OR
    # queries yet
    my $query = sprintf('SELECT {} FROM %s WHERE %s', $self->prefix, join(
        ' OR ',
        map { sprintf('(%s = NULL)', $_) } @required_attrs
    ));

    my $zql = Zaaksysteem::Search::ZQL->new($query);

    return $zql->apply_to_resultset($rs);
}

=head2 infer_moose_type

This method returns a string that Moose can resolve to a
L<Moose::Meta::TypeConstraint> to check against.

If no 'proper' type can be inferred, this method will default to returning
'Str'.

    print $type->infer_moose_type('numeric');
    => 'Num'

=cut

sub infer_moose_type {
    my $self = shift;
    my $value_type = shift;

    # XXX TODO EWWWWWW EW EW EW KILL WITH FIRE
    my %typemap = (
        backaccount         => 'Str',
        email               => 'Str',
        url                 => 'Str',
        text                => 'Str',
        richtext            => 'Str',
        image_from_url      => 'Str',
        text_uc             => 'Str',
        numeric             => 'Num',
        valuta              => 'Num',
        valutain            => 'Num',
        valutain6           => 'Num',
        valutain21          => 'Num',
        valutaex            => 'Num',
        valutaex6           => 'Num',
        valutaex21          => 'Num',
        date                => 'Str',
        googlemaps          => 'Str',
        geolatlon           => 'Str',
        textarea            => 'Str',
        option              => 'Str',
        select              => 'Str',
        checkbox            => 'ArrayRef',
        file                => 'ArrayRef[HashRef]',
        subject             => 'ArrayRef[HashRef]',
        calendar            => 'Str',
        bag_straat_adres    => 'HashRef',
        bag_straat_adressen => 'HashRef',
        bag_adres           => 'HashRef',
        bag_adressen        => 'HashRef',
        bag_openbareruimte  => 'HashRef',
        bag_openbareruimtes => 'HashRef'
    );

    # Default to Str. This is bad, flattens objects and such.
    return $typemap{ $value_type } // 'Str';
}

=head2 type

The C<type> method is overridden from L<Zaaksysteem::Object>'s smart
implementation, we already know our type is "type".

=cut

override type => sub {
    return 'type';
};

=head2 capabilities

This method wraps L<Zaaksysteem::Object/capabilities> and adds the
C<instance_form> capability.

This capability indicates that the object supports generating a form for
the defined object type.

=cut

around capabilities => sub {
    my $orig = shift;
    my $self = shift;

    return $self->$orig(@_), 'instance_form', 'validate';
};

=head2 TO_STRING

Override L<Zaaksysteem::Object/TO_STRING> for better stringification behavior.

It simply returns the Type's name.

=cut

override TO_STRING => sub {
    return shift->name;
};

__PACKAGE__->meta->make_immutable();

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

