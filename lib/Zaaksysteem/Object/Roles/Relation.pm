package Zaaksysteem::Object::Roles::Relation;

use Moose::Role;
use namespace::autoclean;

use Zaaksysteem::Tools;

requires 'relatable_types';

=head1 NAME

Zaaksysteem::Object::Roles::Relation - Augment an object class with relation
extensions.

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head2 relations

This attribute holds an array of L<Zaaksysteem::Object::Relation>s. It can be
manipulated as a plain attribute, or via the C<add_relation>, C<all_relations>,
and C<map_relations> methods.

=cut

has relations => (
    is => 'rw',
    isa => 'ArrayRef[Zaaksysteem::Object::Relation]',
    default => sub { [] },
    traits => [qw[Array]],

    handles => {
        add_relation => 'push',
        all_relations => 'elements',
        map_relations => 'map',
        filter_relations => 'grep'
    }
);

=head2 inherited_relations

=cut

has inherited_relations => (
    is => 'rw',
    isa => 'ArrayRef[Zaaksysteem::Object::Relation]',
    traits => [qw[Array]],
    builder => 'build_inherited_relations',

    handles => {
        all_inherited_relations => 'elements',
        map_inherited_relations => 'map'
    }
);

=head1 METHODS

=head2 add_relation

This method takes a L<Zaaksysteem::Object::Relation> object and pushes it on
the array of relations.

    $object->add_relation(Zaaksysteem::Object::Relation->new(...));

Returns the new count of relations.

=head2 all_relations

This method dereferences the L</relations> array attribute and returns a list
of L<Zaaksysteem::Object::Relation>s.

    my @relations = $object->all_relations;

    # This does exactly the same thing
    my @relations = @{ $object->relations };

=head2 map_relations

This method takes a coderef and runs it against all L</relations> in the
object. It returns the list of return values that are produced in the process.

    my @retval = $object->map_relations(sub {
        return $_->do_something_fancy;
    });

=head2 filter_relations

This method takes a coderef and runs it against all L</relations> in the
object. It returns a list of relations for which the provided coderef returned
true-ish.

    my @plain_relations = $object->filter_relations(sub {
        return $_->relationship_name_a eq 'related';
    });

=head2 build_inherited_relations

=cut

sub build_inherited_relations {
    return [];
};

=head2 relate

This method is a convenience call for L</add_relation>. The expected argument
is an object, and it will return the new relation object.

    my $rel = $object1->relate($object2);

=cut

sub relate {
    my $self = shift;
    my $related_object = shift;
    my %opts = @_;

    unless (blessed $related_object && $related_object->does(__PACKAGE__)) {
        throw('object/relation/no_relatable_object', sprintf(
            'Unable to make a relation between "%s" and another object, either it is not blessed, or it does not implement %s',
            $self,
            __PACKAGE__
        ));
    }

    unless(defined $self->relatable_types &&
           grep { $_ eq $related_object->type } $self->relatable_types) {
        throw('object/relation/incompatible_relatable_type', sprintf(
            'Unable to relate "%s" to "%s", the types are incompatible (must be one of %s).',
            $self,
            $related_object,
            join(',', $self->relatable_types)
        ));
    }

    # First, strip all known relations with related_object, recreate the
    # relation if it had existed.
    $self->unrelate($related_object);

    my $relation = Zaaksysteem::Object::Relation->new(
        %opts,
        related_object_id   => $related_object->id,
        related_object_type => $related_object->type,
        related_object      => $related_object,
    );

    $self->add_relation($relation);

    return $relation;
}

=head2 unrelate

This method will take any object that implements this role, loop over the
instance's relations and filter any relation that matches the identifier of the
object-to-be-unrelated.

Returns the object this method is called on.

    $object1->unrelate($object2)->unrelate($object3);

=cut

sub unrelate {
    my $self = shift;
    my $related_object = shift;

    unless (blessed $related_object && $related_object->does(__PACKAGE__)) {
        throw('object/relation', sprintf(
            'Unable to unrelate between "%s" and another object, either it is not blessed, or it does not implement %s',
            $self,
            __PACKAGE__
        ));
    }

    $self->relations([
        $self->filter_relations(sub {
            $_->related_object_id ne $related_object->id
        })
    ]);

    return $self;
}

=head2 is_deleteable

Returns a true value if the object is deleteable, false otherwise (based on relations).

=cut

around 'is_deleteable' => sub {
    my $orig = shift;
    my $self = shift;

    my $is_deleteable = $self->$orig(@_);

    if ($self->filter_relations(sub { $_->blocks_deletion }) ) {
        $is_deleteable = 0;
    }

    return $is_deleteable;
};

=head2 TO_JSON

This method extends the existing L<Zaaksysteem::Object/TO_JSON> method by
hydrating the object relations and relatable types.

=cut

around TO_JSON => sub {
    my $orig = shift;
    my $self = shift;

    my $retval = $self->$orig(@_);

    $retval->{ related_objects } = [ $self->all_relations, $self->all_inherited_relations ];
    $retval->{ relatable_types } = [ $self->relatable_types ];

    return $retval;
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

