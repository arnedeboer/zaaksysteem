package Zaaksysteem::Object::Roles::MetaType;

use Moose::Role;
use namespace::autoclean;

use Moose::Meta::Class;
use Moose::Meta::Attribute;

use Zaaksysteem::Object;
use Zaaksysteem::Tools;
use Zaaksysteem::ZTT::Element;

=head1 NAME

Zaaksysteem::Object::Roles::MetaType - Role that builds metas for objects
that are metatype implementations

=head1 DESCRIPTION

This role is used by object types that wish to provide an implementation for
other object types. It defines some generic interfaces required for building a
L<Moose::Meta::Class> instance.

=head1 SYNOPSIS

Examples can be found in the C<TestFor::General::Object::Roles::MetaType>
test.

=head1 REQUIRED INTERFACES

Consumers of this role must implement the following as methods or attributes.

=head2 instance_superclass

This property is expected to return a packagename which will be used as the
superclass for the meta this role builds.

=head2 instance_roles

Expected to return a list of role packagenames which will be consumed by the
meta this role builds.

=head2 instance_attributes

Expected to return a list of attribute definitions as C<HashRef>. These data
will be used to construct L<Moose::Meta::Attribute> instances on the meta this
role builds. The attribute meta instances will have the
L<Zaaksysteem::Metarole::ObjectAttribute> trait applied.

=head2 instance_relations

Expected to return a list of relation defintions as C<HashRef>. These data will
be used to construct L<Moose::Meta::Attribute> instances on the meta this role
builds. The attribute meta instances will have the
L<Zaaksysteem::Metarole::ObjectRelation> trait applied.

=cut

requires qw[
    instance_type
    instance_superclass
    instance_roles
    instance_attributes
    instance_relations
];

sig instance_type       => '=> Str';
sig instance_superclass => '=> Str';
sig instance_roles      => '=> @Str';
sig instance_attributes => '=> @HashRef';
sig instance_relations  => '=> @HashRef';

=head1 METHODS

=head2 instance_meta_class

This method builds the L<Moose::Meta::Class> as configured by the consumer of
this role.

=cut

sub instance_meta_class {
    my $self = shift;

    my $superclass = $self->instance_superclass;
    my @roles = $self->instance_roles;

    my %meta_class_args = (
        cache => 0,
        superclasses => [$superclass]
    );

    if (scalar @roles) {
        $meta_class_args{ roles } = \@roles;
    }

    my $meta = Moose::Meta::Class->create_anon_class(%meta_class_args);

    my $ref;

    if ($self->has_id) {
        $ref = $self->_ref;
    }

    my $type_name = $self->instance_type;

    $meta->add_override_method_modifier(type => sub { return $type_name });
    $meta->add_method(type_reference => sub { return $ref });

    $meta->add_attribute($_) for $self->instance_meta_object_attributes;
    $meta->add_attribute($_) for $self->instance_meta_object_relations;

    # This implements the interface for ZTT processing
    $meta->add_method(get_string_fetchers => sub {
        my $self = shift;

        return sub {
            my $attr = $self->attribute_instance(shift->name);

            return unless defined $attr;

            return Zaaksysteem::ZTT::Element->new(value => $attr->human_value);
        };
    });

    return $meta;
}

=head2 instance_meta_object_attributes

This method transforms the attribute definition data supplied by the required
C<instance_attributes> interface into L<Moose::Meta::Attribute> instances.

=cut

sub instance_meta_object_attributes {
    my $self = shift;

    my @attribute_metas;

    for my $attr ($self->instance_attributes) {
        # Guard case in place for types saved in db lacking the required
        # fields, this applies mostly to plain object types that haven't
        # been updated in ages, the fix is to re-save the offending type.
        unless (defined $attr->{ name }) {
            warn sprintf(
                'Cannot build type attribute in metatype %s, skipping',
                $self->id
            );

            next;
        }

        my $name = $attr->{ name };

        # Existing objecttypes use the now deprecated attribute.magic_string
        # format, while newer type implementers use plain magic_string.
        if ($name =~ m[^attribute\.(.*)]) {
            $name = $1;
        }

        my %oa_args = (
            is => 'rw',
            isa => $self->_infer_moose_type($attr->{ attribute_type }),
            required => $attr->{ required } // 0,
            traits => [qw[OA]],
            label => $attr->{ label } // $attr->{ name },
            type => $attr->{ attribute_type },
            index => $attr->{ index }
        );

        if ($attr->{ data }) {
            $oa_args{ data } = $attr->{ data };
        }

        # Use alternative constructor for attribute so the attribute's traits
        # are applied before the arguments are parsed, Moose won't pick up on
        # the additional keys label/type/index otherwise.
        push @attribute_metas, Moose::Meta::Attribute->interpolate_class_and_new($name, %oa_args);
    }

    return @attribute_metas;
}

=head2 instance_meta_object_relations

This method transforms the relation definition data supplied by the required
C<instance_relations> interface into L<Moose::Meta::Attribute> instances.

=cut

sub instance_meta_object_relations {
    my $self = shift;

    my @attribute_metas;

    for my $attr ($self->instance_relations) {
        # Use alternative constructor for attribute so the attribute's traits
        # are applied before arguments are parsed, Moose won't pick up on the
        # additional keys label/type otherwise.
        push @attribute_metas, Moose::Meta::Attribute->interpolate_class_and_new($attr->{ name } => (
            is => 'rw',
            required => $attr->{ required } // 0,
            traits => [qw[OR]],
            label => $attr->{ label },
            type => $attr->{ type },
        ));
    }

    return @attribute_metas;
}

=head2 _infer_moose_type

This method translates attribute type strings to Moose type strings, by way
of hardcoding a lookup table and defaulting to the C<Str> type. It is ugly and
non-maintainable and should be replaced by a smarter bit of code.

=cut

sub _infer_moose_type {
    my $self = shift;
    my $value_type = shift;

    my %typemap = (
        backaccount         => 'Str',
        email               => 'Str',
        url                 => 'Str',
        text                => 'Str',
        richtext            => 'Str',
        image_from_url      => 'Str',
        text_uc             => 'Str',
        numeric             => 'Num',
        valuta              => 'Num',
        valutain            => 'Num',
        valutain6           => 'Num',
        valutain21          => 'Num',
        valutaex            => 'Num',
        valutaex6           => 'Num',
        valutaex21          => 'Num',
        date                => 'Str',
        googlemaps          => 'Str',
        textarea            => 'Str',
        option              => 'Str',
        select              => 'Str',
        checkbox            => 'ArrayRef',
        file                => 'ArrayRef[HashRef]',
        subject             => 'ArrayRef[HashRef]',
        calendar            => 'Str',
        bag_straat_adres    => 'HashRef',
        bag_straat_adressen => 'HashRef',
        bag_adres           => 'HashRef',
        bag_adressen        => 'HashRef',
        bag_openbareruimte  => 'HashRef',
        bag_openbareruimtes => 'HashRef'
    );

    return $typemap{ $value_type } // 'Str';
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
