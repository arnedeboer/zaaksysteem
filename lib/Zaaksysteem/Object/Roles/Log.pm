package Zaaksysteem::Object::Roles::Log;

use Moose::Role;

requires 'capabilities';

use Zaaksysteem::Tools;

=head1 NAME

Zaaksysteem::Object::Roles::Log - Adds actions and infrastructure for
maintaining and using logs on objects

=head1 DESCRIPTION

This role implements the capability to inspect logs on objects.

Usage in the object type package:

    package Zaaksysteem::Object::Types::MyType;

    with 'Zaaksysteem::Object::Roles::Log';

At which point, a generic action becomes available on the object via the
object-action infrastructure (C</api/object/[UUID]/action/log>.

=head1 ACTIONS

=head2 log

This action produces a list of events that reference the object this action is
called on.

=cut

sub log {
    my $self = shift;
    my $c = shift;

    return $c->model('DB::Logging')->search(
        { object_uuid => $self->id },
        { order_by => { -desc => 'created' } }
    );
}

=head2 create_note

This action will create a note on the timeline of an object. It requires one
argument, a string named C<content>.

The newly created event will be returned in the ZAPI response.

=cut

define_profile create_note => (
    required => {
        content => 'Str'
    }
);

sub create_note {
    my $self = shift;
    my $c = shift;

    my $content = assert_profile($c->req->params)->valid->{ content };

    return $c->model('DB::Logging')->trigger('object/note', {
        component => $self->type,
        object_uuid => $self->id,
        data => {
            content => $content
        }
    });
}

=head1 METHODS

=head2 capabilities

This method is a wrapper for L<Zaaksysteem::Object/capabilities> and adds the
C<log> capability.

=cut

around capabilities => sub {
    my $orig = shift;
    my $self = shift;

    return $self->$orig(@_), 'log', 'create_note';
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
