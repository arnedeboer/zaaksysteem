package Zaaksysteem::StatsD;

use Moose;
use Scalar::Util qw/blessed/;

=head1 NAME

Zaaksysteem::StatsD - StatsD Module for Zaaksysteem

=head1 SYNOPSIS

    ### Increment by one
    $c->statsd->increment('employee.login.failed', 1);

=head1 DESCRIPTION

Interface to statsd daemon

=head1 METHODS

=head2 $c->statsd

Return value: $STATSD_OBJECT

    my $statsd_object = $c->statsd;

Returns the L<Zaaksysteem::StatsD::Backend> object, which is documented below

=cut

has 'statsd' => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my $c       = shift;

        die('Cannot run StatsD from unblessed object') unless blessed($c);

        return Zaaksysteem::StatsD::Backend->new(
            config          => $c->config->{statsd},
            active          => $c->customer_instance->{start_config}->{statsd_enabled},
            calledhost      => $c->req->uri->host,
        );
    }
);


package Zaaksysteem::StatsD::Backend;

use Net::Statsd;
use Moose;

has 'config'    => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub { {}; }
);

has [qw/active calledhost prefix/]    => (
    'is'        => 'rw',
);

sub BUILD {
    my $self            = shift;

    return unless $self->active;

    unless (
        UNIVERSAL::isa($self->config, 'HASH') &&
        defined $self->config->{host} &&
        $self->config->{host}
    ) {
        $self->active(undef);
        return;
    }

    $self->_load_prefix;
}

sub _load_prefix {
    my $self            = shift;

    my $prefix          = 'host.';
    if ($self->calledhost) {
        $prefix .= $self->calledhost;
    } else {
        $prefix .= 'unknown';
    }

    $prefix .= '.metric.zaaksysteem.';

    $self->prefix($prefix);
}


sub _prepare {
    my $self            = shift;

    return unless $self->active;

    $Net::Statsd::HOST = $self->config->{host};

    if (defined $self->config->{port} && $self->config->{port}) {
        $Net::Statsd::PORT = $self->config->{port};
    }

    return 1;
}

=head1 BACKEND METHODS

=head2 $c->statsd->increment($STRING_PREFIX, $INTEGER)

Return value: $BOOL_SUCCESS

    $c->statsd->increment('employee.login.failed', 1);

Increments a statsd counter with one.

=cut

sub increment {
    my $self            = shift;
    my $metric          = shift;

    return unless $self->_prepare;

    return Net::Statsd::increment($self->prefix . $metric, @_);
}

=head2 $c->statsd->timing($STRING_PREFIX, $TIME_MICROSECONDS)

Return value: $BOOL_SUCCESS

    $c->statsd->timing('request.time', '2500');

Increments a statsd counter with one.

=cut

sub timing {
    my $self            = shift;
    my $metric          = shift;

    return unless $self->_prepare;

    return Net::Statsd::timing($self->prefix . $metric, @_);
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
