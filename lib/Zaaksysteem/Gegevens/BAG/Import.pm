package Zaaksysteem::Gegevens::BAG::Import;

use Moose::Role;
use Data::Dumper;


sub import_start {
    my $self        = shift;

    my $import      = $self->_load_iterator;

    $import->run;
}

sub _load_iterator {
    my $self        = shift;

    die('Could not find import class') unless
        $self->config->{import_class};

    my $package     = __PACKAGE__ . '::'
        . $self->config->{import_class};

    eval "use $package";

    if ($@) {
        die('Error loading package: ' . $package . ':' . $@);
    }

    my $object      = $package->new(
        dbic    => $self->dbic,
        config  => $self->config,
        prod    => $self->prod,
        'log'   => $self->log,
    );

    return $object;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 import_start

TODO: Fix the POD

=cut

