package Zaaksysteem::CLI;
use Moose;

=head1 NAME

Zaaksysteem::CLI - A Zaaksysteem CLI tool

=head1 SYNOPSIS

For use in specific cases

    use Zaaksysteem::CLI;

    my $cli = Zaaksysteem::CLI->new(
        # Required
        config    => '/etc/zaaksysteem/zaaksysteem.conf',
        hostname  => 'mintlab.zaaksysteem.nl',

        # Optional
        customerd => '/etc/zaaksysteem/customer.d',
        dry       => 1, # Roll back transactions, defaults to true
        debug     => 1,
    );

    my $schema = $cli->schema;

    $cli->do_transaction(sub { my ($self, $schema) = @_ ; # Your DB actions });

For use in more general applications for our production platform

    use Zaaksysteem::CLI;

    my $cli = Zaaksysteem::CLI->init();

    my $options = $cli->options;

    # Check all the required options

    $cli->log->info("Starting action");

    $cli->do_transaction(sub { my ($self, $schema) = @_; # Your DB action });

    $cli->log->info("Done");

=cut

use File::Basename;
use File::Spec::Functions;
use Getopt::Long qw[GetOptionsFromArray :config no_auto_abbrev no_ignore_case];
use Log::Log4perl qw(:easy);
use Pod::Usage;
use Try::Tiny;

use Zaaksysteem::Config;
use Zaaksysteem::Tools;

use Zaaksysteem::Object::Model;

with 'MooseX::Log::Log4perl';

use constant COMMANDLINE_OPTIONS => [qw/
    dry|n!
    help|h
    verbose|v!
    config|c=s
    customer_d|d=s
    hostname|t=s
    debug|D!
    audit_log=s
    error_log=s
    option|o=s%
/];

=head1 ATTRIBUTES

=head2 dry

Flag to roll back any transaction made.

    $ my_script.pl --dry ...

or

    $ my_script.pl -n ...

=cut

has dry => (
    is      => 'ro',
    isa     => 'Bool',
    default => 1,
);

=head2 volatile

Indicates a script cannot be used in dry mode.

=cut

has volatile => (
    is      => 'ro',
    isa     => 'Bool',
    default => 0,
);

has commandline_options => (
    is      => 'ro',
    default => sub {
        return COMMANDLINE_OPTIONS();
    }
);


=head2 verbose

This attribute holds the verbose flag. This is an indication by the invocant
expects additional output from the script, usually for debugging purposes.

    $ my_script.pl --verbose ...

or

    $ my_script.pl -v ...

=cut

has verbose => (
    is  => 'ro',
    isa => 'Bool',
);

=head2 options

This attribute contains free-format options passed to the script.

Calling it like this:

    $ my_script.pl --option foo=bar -o baz=burp

Will result in C<< $cli->options >> containing:

    {
        foo => 'bar',
        baz => 'burp
    }

=cut

has options => (
    is       => 'ro',
    isa      => 'HashRef',
    traits   => [qw[Hash]],
    required => 0,
    default  => sub { {} },
    handles  => {
        get_option => 'get',
        has_option => 'exists'
    }
);

=head2 help

This flags indicates to the consuming script that the usage/help/info section
should be rendered.

    $ my_script.pl --help ...

or

    $ my_script.pl -h ...

=cut

has help => (
    is  => 'ro',
    isa => 'Bool',
);

=head2 hostname

The hostname you want to use for the execution. This is basically a reference
to the intended customer environment.

    $ my_script.pl --hostname template-accept.zaaksysteem.nl ...

or

    $ my_script.pl -h template-accept.zaaksysteem.nl ...

=cut

has hostname => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

=head2 config

This attribute is meant to hold a string that is the path to a Zaaksysteem
L<configuration|Zaaksysteem::Manual::Zaaksysteem::Config> file.

    $ my_script.pl --config /vagrant/etc/zaaksysteem.conf ...

or

    $ my_script.pl -c /vagrant/etc/zaaksysteem.conf ...

=cut

has config => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

=head2 customer_d

The customer_d dir, defaults to the relative directory where zaaksysteem.conf is found.

    $ my_script.pl --customer_d /vagrant/etc/customer.d ...

or

    $ my_script.pl -d /vagrant/etc/customer.d ...

=cut

has customer_d => (
    is       => 'ro',
    isa      => 'Str',
    lazy     => 1,
    default  => sub {
        my $self = shift;
        return catdir(dirname($self->config), 'customer.d');
    },
);

=head2 zs_config

This attribute lazily builds a L<Zaaksysteem::Config> instance, based on the
provided configfile and customer_d options.

=cut

has zs_config => (
    is => 'ro',
    isa => 'Zaaksysteem::Config',
    lazy => 1,
    default => sub {
        my $self = shift;

        return Zaaksysteem::Config->new(
            zs_customer_d => $self->customer_d,
            zs_conf => $self->config
        );
    }
);

=head2 objectmodel

This attribute lazily loads the L<objectmodel|Zaaksysteem::Object::Model>;

=cut

has 'objectmodel' => (
    is      => 'ro',
    lazy    => 1,
    default => sub {
        my $self = shift;

        return Zaaksysteem::Object::Model->new(
            schema => $self->schema
        );
    }
);


=head2 schema

This attribute lazily loads the L<schema|Zaaksysteem::Schema> for the selected
L</hostname>.

=cut

has schema => (
    is   => 'ro',
    lazy => 1,
    isa  => 'Zaaksysteem::Schema',
    default => sub {
        my $self = shift;

        return $self->zs_config->get_customer_schema($self->hostname, { with_defaults => 1 });
    },
);

around BUILDARGS => sub {
    my $orig = shift;
    my $class = shift;
    my %params = @_;

    if ($params{verbose}) {
        Log::Log4perl->easy_init($params{debug} ? $TRACE : $DEBUG);
    }
    else {
        Log::Log4perl->easy_init($params{debug} ? $DEBUG : $INFO);
    }

    # n is used by some legacy scripts.
    if (exists $params{n}) {
        WARN "Your are using a deprecated option, please convert your script to use the 'dry' option";
        $params{ dry } = delete $params{ n };
    }
    elsif (exists $params{dry}) {
        $params{ dry } = delete $params{ dry };
    }

    if (defined $params{audit_log}) {
        $params{audit_log_path} = delete $params{audit_log};
    }

    if (defined $params{error_log}) {
        $params{error_log_path} = delete $params{error_log};
    }

    return $class->$orig(%params);
};

=head1 FUNCTIONS

=head2 init

=cut

sub init {
    my ($class, $args) = @_;

    if (defined $args && ref $args ne 'ARRAY') {
        throw('zs/cli/args_as_array_expected', sprintf(
            'Expected %s::init(\@args), got a "%s"',
            $class,
            ref $args || $args
        ));
    }

    $args //= \@ARGV;
    my %cli_args;
    my %app_args;

    {
        local $SIG{ __WARN__ };

        my @spec = (@{ COMMANDLINE_OPTIONS() }, \%app_args);

        unless (try { GetOptionsFromArray($args, \%cli_args, @spec) }) {
            throw('zs/cli/getopts_failure');
        }
    }

    if ($cli_args{help}) {
        pod2usage(-verbose => 1, -exitval => 0);
    }

    # In case you init, we assume you are a CLI script in that case, don't roll
    # back automaticly
    $cli_args{dry} //= 0;

    $cli_args{ options } = \%app_args if scalar keys %app_args;

    return $class->new(%cli_args);
}

=head1 METHODS

=head2 do_transaction

Execute a function within a transaction, dies once a failure is encountered.

=cut

sub do_transaction {
    my ($self, $sub) = @_;

    $self->schema->txn_do(sub {
        my $rv = $sub->($self, $self->schema);

        $self->schema->txn_rollback if $self->dry;
        return $rv;
    });
}

=head2 assert_run

Checks if you are in a state where you can run the script based on the dry and volatile settings.

=cut

sub assert_run {
    my $self = shift;

    if ($self->dry && $self->volatile) {
        throw('ZS/CLI/volatile', "Unable to proceed, you cannot run in dry mode with a volatile script!");
    }

    if ($self->dry) {
        $self->log->info("Running in dry mode, not commiting any changes during this run!");
    }
    if ($self->volatile) {
        $self->log->info("Running in volatile mode, commiting permanent changes without the option to rollback");
    }
}

=head2 run

Run the script. This somewhat abstract method does some checks for the child class.

=cut

sub run {
    my $self = shift;
    $self->assert_run;
}


__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
