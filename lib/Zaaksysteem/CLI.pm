package Zaaksysteem::CLI;
use Moose;

=head1 NAME

Zaaksysteem::CLI - A Zaaksysteem CLI tool

=head1 SYNOPSIS

    my $cli = Zaaksysteem::CLI->new(
        hostname  => 'mintlab.zaaksysteem.nl',
        config    => '/etc/zaaksysteem/zaaksysteem.conf',
        customerd => '/etc/zaaksysteem/customer.d',
        dry       => 0, # Don't roll back transactions,
    );

    my $schema = $cli->schema;

    $cli->do_transaction(sub { # do something with the DB });

=cut

use Zaaksysteem::Config;
use Zaaksysteem::Tools;
use File::Spec::Functions;
use File::Basename;
use Getopt::Long qw[GetOptionsFromArray];

=head1 ATTRIBUTES

=head2 dry

Flag to roll back any transaction made. Defaults to a rollback.

    $ my_script.pl --dry ...

or

    $ my_script.pl -n ...

=cut

has dry => (
    is       => 'ro',
    isa      => 'Bool',
    required => 0,
    default  => 1,
);

=head2 verbose

This attribute holds the verbose flag. This is an indication by the invocant
expects additional output from the script, usually for debugging purposes.

    $ my_script.pl --verbose ...

or

    $ my_script.pl -v ...

=cut

has verbose => (
    is       => 'ro',
    isa      => 'Bool',
    required => 0,
    default  => 0
);

=head2 options

This attribute contains free-format options passed to the script.

Calling it like this:

    $ my_script.pl --option foo=bar -o baz=burp

Will result in C<< $cli->options >> containing:

    {
        foo => 'bar',
        baz => 'burp
    }

=cut

has options => (
    is       => 'ro',
    isa      => 'HashRef',
    traits   => [qw[Hash]],
    required => 0,
    default  => sub { {} },
    handles  => {
        get_option => 'get',
        has_option => 'exists'
    }
);

=head2 help

This flags indicates to the consuming script that the usage/help/info section
should be rendered.

    $ my_script.pl --help ...

or

    $ my_script.pl -h ...

=cut

has help => (
    is => 'ro',
    isa => 'Bool',
    required => 0,
    default => 0
);

=head2 hostname

The hostname you want to use for the execution. This is basically a reference
to the intended customer environment.

    $ my_script.pl --hostname template-accept.zaaksysteem.nl ...

or

    $ my_script.pl -h template-accept.zaaksysteem.nl ...

=cut

has hostname => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

=head2 config

This attribute is meant to hold a string that is the path to a Zaaksysteem
L<configuration|Zaaksysteem::Manual::Zaaksysteem::Config> file.

    $ my_script.pl --config /vagrant/etc/zaaksysteem.conf ...

or

    $ my_script.pl -c /vagrant/etc/zaaksysteem.conf ...

=cut

has config => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

=head2 customer_d

The customer_d dir, defaults to the relative directory where zaaksysteem.conf is found.

    $ my_script.pl --customer_d /vagrant/etc/customer.d ...

or

    $ my_script.pl -d /vagrant/etc/customer.d ...

=cut

has customer_d => (
    is       => 'ro',
    isa      => 'Str',
    lazy     => 1,
    default  => sub {
        my $self = shift;
        return catdir(dirname($self->config), 'customer.d');
    },
);

=head2 zs_config

This attribute lazily builds a L<Zaaksysteem::Config> instance, based on the
provided configfile and customer_d options.

=cut

has zs_config => (
    is => 'ro',
    isa => 'Zaaksysteem::Config',
    lazy => 1,
    default => sub {
        my $self = shift;

        return Zaaksysteem::Config->new(
            zs_customer_d => $self->customer_d,
            zs_conf => $self->config
        );
    }
);

=head2 schema

This attribute lazily loads the L<schema|Zaaksysteem::Schema> for the selected
L</hostname>.

=cut

has schema => (
    is   => 'ro',
    lazy => 1,
    isa  => 'Zaaksysteem::Schema',
    default => sub {
        my $self = shift;

        return $self->zs_config->get_customer_schema($self->hostname, 1);
    },
);

around BUILDARGS => sub {
    my $orig = shift;
    my $class = shift;
    my %params = @_;

    $params{ dry } = delete $params{ n } if exists $params{ n };

    return $class->$orig(%params);
};

=head1 FUNCTIONS

=head2 init

=cut

sub init {
    my ($class, $args) = @_;

    if (defined $args && ref $args ne 'ARRAY') {
        throw('zs/cli/args_as_array_expected', sprintf(
            'Expected %s::init(\@args), got a "%s"',
            $class,
            ref $args || $args
        ));
    }

    $args //= \@ARGV;
    my %cli_args;
    my %app_args;

    {
        local $SIG{ __WARN__ };

        my @spec = (qw[
            dry|n!
            help|h
            verbose|v!
            config|c=s
            customer_d|d=s
            hostname|t=s
            option|o=s%
        ], \%app_args);

        unless (try { GetOptionsFromArray($args, \%cli_args, @spec) }) {
            throw('zs/cli/getopts_failure');
        }
    }

    $cli_args{ options } = \%app_args if scalar keys %app_args;

    return $class->new(%cli_args);
}

=head1 METHODS

=head2 do_transaction

Execute a function within a transaction, dies once a failure is encountered.

=cut

sub do_transaction {
    my ($self, $sub) = @_;

    $self->schema->txn_do(sub {
        $sub->($self, $self->schema);

        $self->schema->txn_rollback if $self->dry;
    });
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
