=head1 NAME

Zaaksysteem::Manual::API::V1::Namespaces::Controlpanel - Controlpanel API
reference documentation

=head1 NAMESPACE URL

    /api/v1/controlpanel

=head1 DESCRIPTION

This page documents the endpoints within the C<controlpanel> API namespace.

=head1 ENDPOINTS

=head2 Core endpoints

=head3 C<GET />

Returns a L<set|Zaaksysteem::Manual::API::V1::Types::Set> of
L<controlpanel|Zaaksysteem::Manual::API::V1::Types::Controlpanel> instances.

=head3 C<GET /list_for_daemon>

Returns a L<set|Zaaksysteem::Manual::API::V1::Types::Set> of
L<controlpanel|Zaaksysteem::Manual::API::V1::Types::Controlpanel> instances.

=head3 C<GET /[controlpanel:id]>

Returns a L<controlpanel|Zaaksysteem::Manual::API::V1::Types::Controlpanel>
instance.

=head3 C<POST /create>

Returns a L<controlpanel|Zaaksysteem::Manual::API::V1::Types::Controlpanel>
instance.

=head3 C<POST /[controlpanel:id]/update>

Returns a L<controlpanel|Zaaksysteem::Manual::API::V1::Types::Controlpanel>
instance.

=head3 C<GET /host_available>

Returns a L<freeform|Zaaksysteem::Manual::API::V1::Types::Freeform> instance.

=head2 Host endpoints

=head3 C<GET /[controlpanel:id]/host>

Returns a L<set|Zaaksysteem::Manual::API::V1::Types::Set> of
L<host|Zaaksysteem::Manual::API::V1::Types::Host> instances.

=head3 C<GET /[controlpanel:id]/host/[host:id]>

Returns a L<host|Zaaksysteem::Manual::API::V1::Types::Host> instance.

=head3 C<POST /[controlpanel:id]/host/create>

Returns a L<host|Zaaksysteem::Manual::API::V1::Types::Host> instance.

=head3 C<POST /[controlpanel:id]/host/[host:id]/update>

Returns a L<host|Zaaksysteem::Manual::API::V1::Types::Host> instance.

=head2 Instance endpoints

=head3 C<GET /[controlpanel:id]/instance>

Returns a L<set|Zaaksysteem::Manual::API::V1::Types::Set> of
L<instance|Zaaksysteem::Manual::API::V1::Types::Instance> instances.

=head3 C<GET /[controlpanel:id]/instance/[instance:id]>

Returns a L<instance|Zaaksysteem::Manual::API::V1::Types::Instance> instance.

=head3 C<POST /[controlpanel:id]/instance/create>

Returns a L<instance|Zaaksysteem::Manual::API::V1::Types::Instance> instance.

=head3 C<POST /[controlpanel:id]/instance/[instance:id]/update>

Returns a L<instance|Zaaksysteem::Manual::API::V1::Types::Instance> instance.

=head2 NAW endpoints

=head3 C<GET /[controlpanel:id]/naw>

Returns a L<set|Zaaksysteem::Manual::API::V1::Types::Set> of
L<naw|Zaaksysteem::Manual::API::V1::Types::NAW> instances.

=head3 C<GET /[controlpanel:id]/naw/[naw:id]>

Returns a L<naw|Zaaksysteem::Manual::API::V1::Types::NAW> instance.

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
