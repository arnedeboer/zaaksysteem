=head1 NAME

Zaaksysteem::Manual::API::V1::Controlpanel::Host - Controlpanel host retrieval and creation

=head1 Description

This API-document describes the usage of our JSON Controlpanel Host API. Via the Controlpanel host API it
is possible to retrieve, create and edit hosts (zaaksystemen) below a customer controlpanel.

=head2 API

This document is based on the V1 API of Zaaksysteem, more information about the default format
of this API can be found in L<Zaaksysteem::Manual::API::V1>. Please make sure you read this
document before continuing.

=head2 URL

The base URL for this API is:

    /api/v1/controlpanel/UUID/host

Make sure you use the HTTP Method C<GET> for retrieving, and C<POST> for change requests.

=head1 Retrieve data

=head2 get

   /api/v1/controlpanel/d7c73f04-5fcc-4c03-b089-b5fc760a08d5/host/c25d2daf-0e00-4fa5-8bc7-b61fff072234

Retrieving an object from our database is as simple as calling the URL C</api/v1/controlpanel/[CONTROLPANEL_UUID]/host/[UUID]>. You will get
the response in the C<result> parameter. It will only contain one single result, and as always, the C<type>
will tell you what kind of object you received. The C<host> property will contain the contents of this
object.

B<Example call>

  curl --anyauth -k -H "Content-Type: application/json" --digest -u "zaaksysteem:abcdefghijklmnop123qrs" https://localhost/api/v1/controlpanel/d7c73f04-5fcc-4c03-b089-b5fc760a08d5/host/c25d2daf-0e00-4fa5-8bc7-b61fff072234

B<Request JSON>

An empty request body suffices

B<Response JSON>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-9c0ab8-92edb9",
   "development" : false,
   "result" : {
      "instance" : {
         "fqdn" : "mijn.example.com",
         "id" : "832a095f-1da5-4b11-b1a4-d1e393573415",
         "ip" : "127.0.0.44",
         "label" : "productie host",
         "owner" : "betrokkene-bedrijf-369",
         "ssl_cert" : null,
         "ssl_key" : null
      },
      "reference" : "832a095f-1da5-4b11-b1a4-d1e393573415",
      "type" : "host"
   },
   "status_code" : 200
}

=end javascript

=head2 list

   /api/v1/controlpanel/d7c73f04-5fcc-4c03-b089-b5fc760a08d5/host

Retrieving multiple objects from our database is as simple as calling the URL C</api/v1/controlpanel/CONTROLPANEL_UUID/host> without arguments. The
C<result> property will contain an object of type B<set>, which allows us to page the data.

B<Example call>

 curl --anyauth -k -H "Content-Type: application/json" --digest -u "zaaksysteem:abcdefghijklmnop123qrs" https://localhost/api/v1/controlpanel/d7c73f04-5fcc-4c03-b089-b5fc760a08d5/host/

B<Request JSON>

An empty request body suffices

B<Response JSON>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-9c0ab8-f25de0",
   "development" : false,
   "result" : {
      "instance" : {
         "pager" : {
            "next" : null,
            "page" : 1,
            "pages" : 1,
            "prev" : null,
            "rows" : 2,
            "total_rows" : 2
         },
         "rows" : [
            {
               "instance" : {
                  "fqdn" : "mijn.example.com",
                  "id" : "3cdaec64-1ae5-48f7-ac57-8f22721fcd56",
                  "ip" : "127.0.0.44",
                  "label" : "productie host",
                  "owner" : "betrokkene-bedrijf-375",
                  "ssl_cert" : null,
                  "ssl_key" : null
               },
               "reference" : "3cdaec64-1ae5-48f7-ac57-8f22721fcd56",
               "type" : "host"
            },
            {
               "instance" : {
                  "fqdn" : "test.example.com",
                  "id" : "47be4991-bd91-490f-a7d7-45d334875d84",
                  "ip" : "127.0.0.45",
                  "label" : "productie host",
                  "owner" : "betrokkene-bedrijf-375",
                  "ssl_cert" : null,
                  "ssl_key" : null
               },
               "reference" : "47be4991-bd91-490f-a7d7-45d334875d84",
               "type" : "host"
            }
         ]
      },
      "reference" : null,
      "type" : "set"
   },
   "status_code" : 200
}

=end javascript

=head1 Mutate data

Mutations via our API will have to be send via the HTTP C<POST> method. The inputdata for these mutations
are one single JSON object containing the parameters. When no input is needed, make sure you send an empty
object (C< {} >)

=head2 create

   /api/v1/controlpanel/d7c73f04-5fcc-4c03-b089-b5fc760a08d5/host/create

It is possible to create a controlpanel host in zaaksysteem.nl.

The input (request) data for this call is a JSON object containing the following properties

B<Properties>

=over 4

=item fqdn [required]

B<TYPE>: Fully qualified hostname

The primary hostname this host will run under

=item label [optional]

B<TYPE>: String

A user defined label for this host, e.g. "Testomgeving"

=item ip

B<TYPE>: IPv4 Address

The ip address of this host

=back

B<Example call>

 curl --anyauth -k -H "Content-Type: application/json" --data @file_with_request_json --digest -u "zaaksysteem:abcdefghijklmnop123qrs" https://localhost/api/v1/controlpanel/d7c73f04-5fcc-4c03-b089-b5fc760a08d5/host/create

B<Request JSON JSON>

=begin javascript

{
   "fqdn" : "mijn.example.com",
   "ip" : "127.0.0.44",
   "label" : "productie host"
}

=end javascript

B<Response JSON JSON>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-9c0ab8-92edb9",
   "development" : false,
   "result" : {
      "instance" : {
         "fqdn" : "mijn.example.com",
         "id" : "832a095f-1da5-4b11-b1a4-d1e393573415",
         "ip" : "127.0.0.44",
         "label" : "productie host",
         "owner" : "betrokkene-bedrijf-369",
         "ssl_cert" : null,
         "ssl_key" : null
      },
      "reference" : "832a095f-1da5-4b11-b1a4-d1e393573415",
      "type" : "host"
   },
   "status_code" : 200
}

=end javascript

=head2 update

   /api/v1/controlpanel/CONTROLPANEL_UUID/host/UUID/update

It is possible to update a controlpanel host in zaaksysteem.nl.

The input (request) data for this call is a JSON object containing the following properties

B<Properties>

=over 4

=item fqdn [required]

B<TYPE>: Fully qualified hostname

The primary hostname this host will run under

=item label [optional]

B<TYPE>: String

A user defined label for this host, e.g. "Testomgeving"

=item ip

B<TYPE>: IPv4 Address

The ip address of this host

=back

B<Example call>

 curl --anyauth -k -H "Content-Type: application/json" --data @file_with_request_json --digest -u "zaaksysteem:abcdefghijklmnop123qrs" https://localhost/api/v1/controlpanel/d7c73f04-5fcc-4c03-b089-b5fc760a08d5/host/221a5015-d427-4853-b045-98a8d4311e9f/update

B<Request JSON JSON>

=begin javascript

{
   "fqdn" : "moir.example.com",
   "ip" : "127.0.0.55",
   "label" : "productie host 2"
}

=end javascript

B<Response JSON JSON>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-9c0ab8-af2cd4",
   "development" : false,
   "result" : {
      "instance" : {
         "fqdn" : "moir.example.com",
         "id" : "59a0d0c4-07f6-4a00-b711-f762e968a42f",
         "ip" : "127.0.0.55",
         "label" : "productie host 2",
         "owner" : "betrokkene-bedrijf-376",
         "ssl_cert" : null,
         "ssl_key" : null
      },
      "reference" : "59a0d0c4-07f6-4a00-b711-f762e968a42f",
      "type" : "host"
   },
   "status_code" : 200
}

=end javascript

=head1 Objects

=head2 host

Most of the calls in this document return an host of type C<host>. Below we provide more information about
the contents of this object.

B<Properties>

=over 4

=item id

B<TYPE>: UUID

The unique identifier of this controlpanelhost. You can use this identifier to request more information about the
related objects of this object, like the download API call.

=item fqdn [required]

B<TYPE>: Fully qualified hostname

The primary hostname this host will run under

=item label [optional]

B<TYPE>: String

A user defined label for this host, e.g. "Testomgeving"

=item ip

B<TYPE>: IPv4

IP address this host runs under

=item owner

B<TYPE>: Identifier of betrokkene (betrokkene-bedrijf-ID)

The owner of this controlpanel, the customer where these panels belong to.

=item ssl_cert

B<TYPE>: String

Text containing the SSL certificate

=item ssl_key

B<TYPE>: String

Text containing the SSL key

=back

=head1 Support

The data in this document is supported by the following test. Please make sure you use the API as described
in this test. Any use of this API outside the scope of this test is B<unsupported>

L<TestFor::Catalyst::API::V1::Controlpanel::Host>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
