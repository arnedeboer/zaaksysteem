=head1 NAME

Zaaksysteem::Manual::API::V1::Query - Search for specific objects

=head1 DESCRIPTION

This page documents the usage of the Zaaksysteem API v1 Query API. Using
this API the connecting client can search for and retrieve object instances
using several query languages.

B<Note>: Currently, the query API is only available in the C</api/v1/case>
namespace.

=head1 QUERY LANGUAGES

=head2 ElasticSearch Query DSL 2.3

See L<Zaaksysteem::Manual::API::V1::Query::ElasticSearch> for examples and
development instructions.

L<ElasticSearch|https://www.elastic.co/products/elasticsearch> is an advanced
full-text search engine for document-based searching, with support for faceted
search, distributed servers, and a lot more. It's ubiquity in document search
technology prompted us to support its
L<Query DSL|https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl.html>.

=head2 Zaaksysteem Query Language (ZQL v1)

ZQL is our own answer to the question of how to query Zaaksysteem. It is
extensively used by our own frontend application, and has seen some use in the
backend itself. It seemed like a good idea at the time.

The ZQL API will continue to be available for internal use, and while reading
API v1 documentation one might find references to it's usage and quirks, but
it is considered B<deprecated> for new integrations with Zaaksysteem. As such
it remains undocumented aside from the class documentation in
L<Zaaksysteem::Search::ZQL>.

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
