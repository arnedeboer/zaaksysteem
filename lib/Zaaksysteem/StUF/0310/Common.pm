package Zaaksysteem::StUF::0310::Common;
use Moose::Role;

use DateTime;
use List::Util qw(max);
use Zaaksysteem::Tools;

=head1 NAME

Zaaksysteem::StUF::0310::Common - Common functions for StUF 0310 APIs

=head1 DESCRIPTION

Helper methods used by multiple StUF 0310 "sector models" (DCR, ZKN).

=head1 METHODS

=head2 generate_bv03

Generate a "Bv03" response message, which is used as a response to most "write"
actions in STUF APIs.

=cut

sig generate_bv03  => 'Str, HashRef => Str';

sub generate_bv03 {
    my $self = shift;
    my ($reference, $remote) = @_;

    my $instance = $self->xml_backend->stuf0310;

    my $xml = $instance->bv03(
        'writer',
        {
            stuurgegevens => {
                berichtcode => 'Bv03',
                # We just swap the sender/receiver from the incoming message around.
                zender => {
                    applicatie    => $remote->{receiver},
                    administratie => $remote->{receiver_admin},
                    organisatie   => $remote->{receiver_organisation},
                    gebruiker     => $remote->{receiver_user},
                },
                ontvanger => {
                    applicatie    => $remote->{sender},
                    administratie => $remote->{sender_admin},
                    organisatie   => $remote->{sender_organisation},
                    gebruiker     => $remote->{sender_user},
                },
                referentienummer => $reference,
                tijdstipBericht  => DateTime->now()->strftime('%Y%m%d%H%M%S%3N'),
                crossRefnummer   => $remote->{reference},
            },
        }
    );

    return $xml;
}

=head2 generate_fo03

Generate a "Fo03" response message, which the generic "error" response
in the STUF-ZKN API.

=cut

sig generate_fo03  => 'Str, HashRef, Any => Str';

sub generate_fo03 {
    my $self = shift;
    my ($reference, $stuurgegevens, $exception) = @_;

    my $instance = $self->xml_backend->stuf0310;

    my $description;
    if (blessed $exception) {
        if ($exception->can('type')) {
            $description = $exception->type;
        }
        else {
            $description = ref($exception);
        }
    }

    $description ||= "Onbekende fout opgetreden";

    my $xml = $instance->fo03(
        'writer',
        {
            stuurgegevens => {
                berichtcode => 'Fo03',
                # We just swap the sender/receiver from the incoming message around.
                zender => {
                    applicatie    => $stuurgegevens->{receiver},
                    administratie => $stuurgegevens->{receiver_admin},
                    organisatie   => $stuurgegevens->{receiver_organisation},
                    gebruiker     => $stuurgegevens->{receiver_user},
                },
                ontvanger => {
                    applicatie    => $stuurgegevens->{sender},
                    administratie => $stuurgegevens->{sender_admin},
                    organisatie   => $stuurgegevens->{sender_organisation},
                    gebruiker     => $stuurgegevens->{sender_user},
                },
                referentienummer => $reference,
                tijdstipBericht  => DateTime->now()->strftime('%Y%m%d%H%M%S%3N'),
                crossRefnummer   => $stuurgegevens->{reference},
            },
            body => {
                code => 'StUF058', # Proces voor afhandelen bericht geeft fout
                plek => 'server',
                omschrijving => substr($description, 0, 200),
                details      => substr("$exception", 0, 1000),
            },
        }
    );

    return $xml;
}

=head2 parse_stuurgegevens

Parse a "stuurgegevens"

=cut

sub parse_stuurgegevens {
    my ($self, $xpc, $stuurgegevens_node) = @_;

    my %rv;

    $rv{reference} = $xpc->findvalue('StUF:referentienummer', $stuurgegevens_node);

    $rv{sender}    = $xpc->findvalue('StUF:zender/StUF:applicatie', $stuurgegevens_node);
    $rv{sender_admin}        = $xpc->findvalue('StUF:zender/StUF:administratie', $stuurgegevens_node);
    $rv{sender_organisation} = $xpc->findvalue('StUF:zender/StUF:organisatie', $stuurgegevens_node);
    $rv{sender_user}         = $xpc->findvalue('StUF:zender/StUF:gebruiker', $stuurgegevens_node);

    $rv{receiver}  = $xpc->findvalue('StUF:ontvanger/StUF:applicatie', $stuurgegevens_node);
    $rv{receiver_admin}        = $xpc->findvalue('StUF:ontvanger/StUF:administratie', $stuurgegevens_node);
    $rv{receiver_organisation} = $xpc->findvalue('StUF:ontvanger/StUF:organisatie', $stuurgegevens_node);
    $rv{receiver_user}         = $xpc->findvalue('StUF:ontvanger/StUF:gebruiker', $stuurgegevens_node);

    return %rv;
}

=head2 build_stuurgegevens

Build a hash suitable to create a C<< <stuurgegevens> >> XML block.

Will base some of its contents on the "incoming" stuurgegevens, the node name
of which should be specified as the first argument (for example,
"ZKN:stuurgegevens" or "DCr:stuurgegevens")

=cut

sub build_stuurgegevens {
    my ($self, $xpc, $record_id, $source_node_name) = @_;

    my %remote = $self->parse_stuurgegevens($xpc, $xpc->findnodes( $source_node_name ));

    return (
        zender => {
            applicatie    => $remote{receiver},
            administratie => $remote{receiver_admin},
            organisatie   => $remote{receiver_organisation},
            gebruiker     => $remote{receiver_user},
        },
        ontvanger => {
            applicatie    => $remote{sender},
            administratie => $remote{sender_admin},
            organisatie   => $remote{sender_organisation},
            gebruiker     => $remote{sender_user},
        },
        tijdstipBericht => DateTime->now()->strftime('%Y%m%d%H%M%S%3N'),

        defined($record_id)
            ? (referentienummer => $record_id)
            : (),
        defined($remote{reference})
            ? (crossRefnummer => $remote{reference})
            : ()
    );
}

=head2 format_case

Takes a "zaak" database row and formats it as a StUF-ZKN XML message.

Takes one (positional) argument, the L<Zaaksysteem::Schema::Zaak> row.

=cut

sub format_case {
    my $self = shift;
    my $case = shift;

    # TODO add status
    my %rv = (
        identificatie            => { _ => '0000' . $case->id },
        einddatum                => $self->format_stuf_date($case->afhandeldatum),
        einddatumGepland         => $self->format_stuf_date($case->streefafhandeldatum),
        startdatum               => $self->format_stuf_date($case->registratiedatum),
        uiterlijkeEinddatum      => $self->format_stuf_date($case->streefafhandeldatum),
        registratiedatum         => $self->format_stuf_date($case->registratiedatum),
        datumVernietigingDossier => $self->format_stuf_date($case->vernietigingsdatum),

        zaakniveau => $case->get_column('pid') ? 2 : 1,
        deelzakenIndicatie => { _ => $self->format_stuf_boolean($case->zaak_pids->count()) },

        $self->_format_case_status($case),
        $self->_format_case_betrokkenen($case),
        $self->_format_case_documents(scalar $case->active_files),

        # TODO opschortingen/verlengingen

        isVan => {
            gerelateerde => {
                code         => $case->zaaktype_node_id->code,
                omschrijving => $case->zaaktype_node_id->titel,
                ingangsdatum => $self->format_stuf_date($case->zaaktype_node_id->created)
            },
        },
    );

    if (my $o = $case->onderwerp) {
        if (length($o) > 80) {
            $o = substr($o, 0, 80);
        }
        $rv{omschrijving}{_} = $o;
    }

    if (my $t = $case->zaaktype_node_id->toelichting) {
        $rv{toelichting}{_} = $t;
    }

    if (my $resultaat = $case->get_zaaktype_result()) {
        $rv{resultaat} = {
            omschrijving => { _ => $case->resultaat },
            toelichting  => { _ => $resultaat->label // $resultaat->resultaat },
        };
    }

    if ($self->log->is_trace) {
        $self->log->trace("format case: " . dump_terse(\%rv, 5));
    }
    # TODO Related cases?

    return \%rv;
}

sub _format_case_status {
    my $self = shift;
    my ($case) = @_;

    my $node = $case->zaaktype_node_id;

    # Key on status
    my %zaaktype_statuses = map {
        ($_->status => $_)
    } $node->zaaktype_statussen->search(
        {},
        { order_by => { -asc => 'me.status' } }
    )->all;
    my $last_status = max(keys(%zaaktype_statuses));

    my @statuses;
    for my $status (1 .. $case->milestone) {
        my $is_last = $zaaktype_statuses{$status}->status == $last_status;

        push @statuses, {
            entiteittype => 'ZAKSTT',
            toelichting => $zaaktype_statuses{$status}->naam,
            indicatieLaatsteStatus => $self->format_stuf_boolean($is_last),
            gerelateerde => {
                entiteittype => 'STT',
                volgnummer => $status,
                code => $node->code . "/" . $status,
                omschrijving => $zaaktype_statuses{$status}->naam,
                omschrijvingGeneriek => $zaaktype_statuses{$status}->fase,
                zaaktype_code => $node->code,
                zaaktype_omschrijving => $node->titel,
            },
        };
    }

    # StUF-ZKN requires statuses to be in a "newest first" order
    return (
        heeft => [ reverse @statuses ]
    );
}

sub _format_case_betrokkenen {
    my $self = shift;
    my $case = shift;

    my %rv;
    if (my $initiator = $self->_format_person('ZAKBTRINI', $case->aanvrager_object)) {
        $rv{heeftAlsInitiator} = $initiator;
    }
    if (my $uitvoerende = $self->_format_person('ZAKBTRUTV', $case->behandelaar_object)) {
        $rv{heeftAlsUitvoerende} = $uitvoerende;
    }
    if (my $verantwoordelijke = $self->_format_person('ZAKBTRVRA', $case->coordinator_object)) {
        $rv{heeftAlsVerantwoordelijke} = $verantwoordelijke;
    }

    my @gemachtigden = $case->get_betrokkene_objecten({ rol => 'Gemachtigde' });
    for my $gemachtigde (@gemachtigden) {
        push @{ $rv{heeftAlsGemachtigde} }, $self->_format_person('ZAKBTRGMC', $gemachtigde);
    }

    my @belanghebbenden = $case->get_betrokkene_objecten({ rol => 'Belanghebbende' });
    for my $belanghebbende (@belanghebbenden) {
        push @{ $rv{heeftAlsBelanghebbende} }, $self->_format_person('ZAKBTRBLH', $belanghebbende);
    }

    my @overig_betrokkenen = $case->get_betrokkene_objecten(
        {
            -and => [
                { rol => { -not_in => ['Gemachtigde', 'Belanghebbende'] } },
                { rol => { '!=' => undef } },
            ],
        }
    );
    for my $overig_betrokkene (@overig_betrokkenen) {
        push @{ $rv{heeftAlsOverigBetrokkene} }, $self->_format_person('ZAKBTROVR', $overig_betrokkene);
    }

    return %rv;
}

sub _format_case_documents {
    my $self = shift;
    my $files_rs = shift;

    my @rv;

    while (my $file = $files_rs->next) {
        push @rv, {
            entiteittype => 'ZAKEDC',
            gerelateerde => $self->format_case_document_minimal($file),
        };
    }

    if (@rv) {
        return (heeftRelevant => \@rv);
    }

    return;
}

sub _format_person {
    my $self = shift;
    my ($entiteittype, $betrokkene) = @_;

    return if not defined $betrokkene;

    my %rv = (
        entiteittype => $entiteittype,
    );

    if ($betrokkene->btype eq 'medewerker') {
        $rv{gerelateerde}{medewerker} = {
            "entiteittype"  => "MDW",
            "identificatie" => $betrokkene->username,
            "achternaam"    => $betrokkene->geslachtsnaam,
            "roepnaam"      => $betrokkene->voornamen,
            "voorletters"   => $betrokkene->voorletters,

            defined($betrokkene->telefoonnummer)
                ? ("telefoonnummer" => $betrokkene->telefoonnummer)
                : (),
            defined($betrokkene->email)
                ? ("emailadres" => $betrokkene->email)
                : (),
        };
    }
    elsif ($betrokkene->btype eq 'natuurlijk_persoon') {
        $rv{gerelateerde}{natuurlijkPersoon} = {
            "entiteittype" => 'NPS',
            "inp.bsn" => $betrokkene->bsn,
            "authentiek" => $self->format_stuf_boolean($betrokkene->authenticated),
        };
    }
    elsif ($betrokkene->btype eq 'bedrijf') {
        $rv{gerelateerde}{vestiging} = {
            "entiteittype" => 'VES',
            "vestigingsNummer" => $betrokkene->vestigingsnummer,
            "authentiek"       => $self->format_stuf_boolean($betrokkene->authenticated),
        };
    }
    else {
        throw(
            "stufzkn/unknown_betrokkene_type",
            sprintf("Unknown betrokkene type '%s'", $betrokkene->btype)
        );
    }

    return \%rv;
}

=head2 format_case_document_minimal

Format the list of case documents in a minimal way (no file contents, etc.).

=cut

sub format_case_document_minimal {
    my $self = shift;
    my ($file) = @_;

    my %rv = (
        identificatie  => { _ => '0000' . ($file->get_column('root_file_id') || $file->id) },
        type_omschrijving => "",

        isRelevantVoor => {
            gerelateerde => {
                identificatie => '0000' . $file->get_column('case_id'),
            }
        },
        creatiedatum   => $self->format_stuf_date($file->date_created),

        titel                   => { _ => $file->name },
        formaat                 => { _ => $file->extension },
        versie                  => { _ => $file->version },
        auteur                  => substr($file->created_by, 0, 200) // 'Onbekend',

        taal   => { _ => 'NIL', noValue => 'waardeOnbekend'  },

        vertrouwelijkAanduiding => { _ => 'NIL' , noValue => 'geenWaarde' },
    );

    if (my $md = $file->metadata_id) {
        $rv{vertrouwelijkAanduiding} = { _ => uc($md->trust_level) };
        if ($md->document_category) {
            $rv{type_omschrijving} = $md->document_category;
        }

        if ($md->origin eq 'Inkomend' || $md->origin eq 'Intern') {
            $rv{ontvangstdatum} = $self->format_stuf_date($md->origin_date);
        }
        elsif ($md->origin eq 'Uitgaand') {
            $rv{verzenddatum}   = $self->format_stuf_date($md->origin_date);
        }
    }

    return \%rv;
}

=head2 format_stuf_date

Format a DateTime in a StUF-compliant format.

=cut

sub format_stuf_date {
    my $self = shift;
    my $date = shift;

    if (not defined $date) {
        return {
            noValue => 'geenWaarde',
            _ => 'NIL',
        }
    }

    # StUF has its own special date format.
    return {
        _ => $date->strftime('%Y%m%d'),
    };
}

=head2 format_stuf_boolean

Formats a boolean value for StUF ("J" or "N")

=cut

sub format_stuf_boolean {
    my $self = shift;
    my ($value) = @_;

    return $value ? 'J' : 'N';
}


1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
