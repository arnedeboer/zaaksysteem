package Zaaksysteem::StUF::PRS;

use Moose::Role;

use Zaaksysteem::Constants;
use Zaaksysteem::Exception;
use Zaaksysteem::Profile;

use Data::FormValidator;

use Zaaksysteem::StUF::Body::Field;

use constant NATUURLIJK_PERSOON_MAP                 => {
    'a-nummer'                      => 'a_nummer',
    'bsn-nummer'                    => 'burgerservicenummer',
    'voornamen'                     => 'voornamen',
    'voorletters'                   => 'voorletters',
    'voorvoegselGeslachtsnaam'      => 'voorvoegsel',
    'geslachtsnaam'                 => 'geslachtsnaam',
    'geboortedatum'                 => 'geboortedatum',
    'geslachtsaanduiding'           => 'geslachtsaanduiding',
    'datumOverlijden'               => 'datum_overlijden',
    'indicatieGeheim'               => 'indicatie_geheim',
    'burgerlijkeStaat'              => 'burgerlijkestaat',
    'aanduidingNaamgebruik'         => 'aanduiding_naamgebruik',
};

use constant NATUURLIJK_PERSOON_PARTNER_MAP         => {
    'a-nummer'                      => 'partner_a_nummer',
    'bsn-nummer'                    => 'partner_burgerservicenummer',
    'voorvoegselGeslachtsnaam'      => 'partner_voorvoegsel',
    'geslachtsnaam'                 => 'partner_geslachtsnaam',
};

use constant NATUURLIJK_PERSOON_ADRES_MAP => {
    'postcode'   => 'postcode',
    'woonplaats' => ['extra.authentiekeWoonplaatsnaam', 'woonplaatsnaam'],
    'straatnaam' => 'straatnaam',
    'huisnummer' => 'huisnummer',
    'huisletter' => 'huisletter',
    'huisnummertoevoeging' => 'huisnummertoevoeging',
    'gemeente_code'        => 'gemeentecode',
    'landcode'             => 'landcode',
    'adres_buitenland1'    => 'adresBuitenland1',
    'adres_buitenland2'    => 'adresBuitenland2',
    'adres_buitenland3'    => 'adresBuitenland3',
};

=head2 METHODS

=head2 get_params_for_natuurlijk_persoon

Gets a set of params for manipulating natuurlijk_persoon

=cut

sub get_params_for_natuurlijk_persoon {
    my $self            = shift;
    my $index           = shift || 0;

    my $params          = {};
    my $object_params   = (
        UNIVERSAL::isa($self->as_params, 'ARRAY')
            ? $self->as_params->[$index]->{PRS}
            : $self->as_params->{PRS}
    );

    for my $key (keys %{ NATUURLIJK_PERSOON_MAP() }) {
        next unless exists($object_params->{$key});

        $params->{NATUURLIJK_PERSOON_MAP->{$key}} = $object_params->{ $key };
    }

    $self->_get_params_for_natuurlijk_persoon_partner($index, $params, $object_params);

    ### Validate params
    my $valid_params    = Data::FormValidator->check(
        $params,
        GEGEVENSMAGAZIJN_GBA_PROFILE
    )->valid;

    # Make sure we did not collect extra "default" params from formvalidator check
    delete($valid_params->{landcode});

    return $valid_params;
}

=head2 get_params_for_natuurlijk_persoon_adres

Gets a set of params for manipulating the adres entry for a PRS.

=cut

sub get_params_for_natuurlijk_persoon_adres {
    my $self                        = shift;
    my $index                       = shift || 0;
    my $object_params               = shift;
    my ($params, $address, $coraddress) = ({});

    unless ($object_params) {
        $object_params   = (
            UNIVERSAL::isa($self->as_params, 'ARRAY')
                ? $self->as_params->[$index]->{PRS}
                : $self->as_params->{PRS}
        );
    }

    return unless (
        exists($object_params->{PRSADRVBL}) ||
        exists($object_params->{PRSADRCOR})
    );

    if (
        exists($object_params->{PRSADRVBL}) &&
        scalar(@{ $object_params->{PRSADRVBL}})
    ) {
        my $error;

        ($address, $error) = grep (
            { $_->{is_active} }
            @{ $object_params->{PRSADRVBL} }
        );

        if ($error) {
            throw(
                'stuf/prs/get_params_for_natuurlijk_persoon_adres/multiple',
                'Got multiple address..that cannot be right'
            );
        }

        $address = undef unless ($address->{ADR}->{straatnaam} || $address->{ADR}->{adresBuitenland1} || $address->{ADR}->{gemeentecode});
        $address->{ADR}->{landcode} = 6030 if ($address && !$address->{ADR}->{landcode});

        $params->{functie_adres} = 'W';
    }

    if (
        exists($object_params->{PRSADRCOR}) &&
        scalar(@{ $object_params->{PRSADRCOR}})
    ) {
        my $error;

        ($coraddress, $error) = grep (
            { $_->{is_active} }
            @{ $object_params->{PRSADRCOR} }
        );

        if ($error) {
            throw(
                'stuf/prs/get_params_for_natuurlijk_persoon_adres/multiple',
                'Got multiple address..that cannot be right'
            );
        }

        $coraddress = undef unless ($coraddress->{ADR}->{straatnaam} || $coraddress->{ADR}->{adresBuitenland1} || $coraddress->{ADR}->{gemeentecode});

        ### Vicrea patch...which does not always send a landcode
        $coraddress->{ADR}->{landcode} = 6030 if ($coraddress && !$coraddress->{ADR}->{landcode});

        $params->{functie_adres} = 'B';
    }

    return $params unless $address || $coraddress;

    while ( my ($key, $value) = each %{ NATUURLIJK_PERSOON_ADRES_MAP() }) {
        if (ref($value) ne 'ARRAY') {
            $value = [ $value ];
        }

        ### We apply the OrIs ( ||= ) on the params, to make sure we always have a value. For instance,
        ### we loop over woonplaatsnaamAuthentiek en woonplaatsNaam. When one of two is set, use it.
        if ($address) {
            for my $v (@$value) {
                if ($v =~ /^extra\./) {
                    my $checkkey = $v;
                    $checkkey    =~ s/^extra\.//;

                    next unless exists($address->{ADR}{extraElementen}{ $checkkey });
                    $params->{ $key } ||= $address->{ADR}{extraElementen}{ $checkkey };
                }
                else {
                    next unless exists($address->{ADR}->{$v});
                    $params->{ $key } ||= $address->{ADR}{ $v };
                }

                # Once we find a valid value from the list of "value containing
                # keys", don't try others.
                last;
            }
        }

        if ($coraddress) {
            for my $v (@$value) {
                if ($v =~ /^extra\./) {
                    my $checkkey = $v;
                    $checkkey    =~ s/^extra\.//;

                    next unless exists($coraddress->{ADR}{extraElementen}{ $checkkey });
                    $params->{"correspondentie_$key" } ||= $coraddress->{ADR}{extraElementen}{ $checkkey };
                }
                else {
                    next unless exists($coraddress->{ADR}->{$v});
                    $params->{"correspondentie_$key"} ||= $coraddress->{ADR}{ $v };
                }

                # Once we find a valid value from the list of "value containing
                # keys", don't try others.
                last;
            }
        }
    }

    ### Defaults. We (unfortunatly) cannot rely on the defaults in GBA_PROFILE:


    ### Validate params
    my $valid_params    = Data::FormValidator->check(
        $params,
        GEGEVENSMAGAZIJN_GBA_PROFILE
    )->valid;

    return $valid_params;
}

=head2 updated_components

    my $updated = $stuf->updated_components

    # {
    #     verblijfsadres    => 1,
    #     buitenland        => 1,
    #     correspondentie   => 1,
    #     partner           => 1,
    # }

Returns a list of changed components in this stuf message. Sometimes, when a "mutatiesoort = W" is given, not everything
is sent to us. This way we know if we have to empty or ignore the change.

When is a component updated?

1) There is an entry in the "new" block (second block) of the StUF "wijziging/correctie" message.
2) There is an entry in the (single) block of the StUF "toevoeging" message.
3) There is an entry in the "old" block (first block) of the StUF "wijziging/correctie" message, but none in the "new" block (second block).

=cut

sub updated_components {
    my $self        = shift;
    my %changed;

    if ($self->mutatiesoort eq 'T') {
        ### When T is set on an existing entry, assume _everything_ has changed.
        return {
            verblijfsadres => 1,
            correspondentie => 1,
            partner => 1,
        };
    }

    my $old = $self->body->[0]->as_params->{'PRS'};
    my $new = $self->body->[1]->as_params->{'PRS'};

    $new            = $self->updated_component($new);
    $old            = $self->updated_component($old) if $old;

    return $new if !$old;

    ## Compare
    %changed        = %$new;
    for my $key (keys %$old) {
        if (!$new->{$key}) {
            $changed{$key} = 1;
        }
    }

    return \%changed;
}

=head2 updated_component

    my $updated = $stuf->updated_component($params)

    # {
    #     verblijfsadres    => 1,
    #     buitenland        => 1,
    #     correspondentie   => 1,
    #     partner           => 1,
    # }

Returns a list of changed components from the given list of parameters.

See for more information L<updated_components>

=cut

sub updated_component {
    my $self    = shift;
    my $params  = shift;
    my %changed;

    if ($params->{'PRSADRVBL'}) {
        my ($address, $error) = grep (
            { $_->{is_active} }
            @{ $params->{PRSADRVBL} }
        );
        $changed{'verblijfsadres'} = 1 if (exists $address->{ADR}->{straatnaam} || exists $address->{ADR}->{adresBuitenland1});
    }

    if ($params->{'PRSADRCOR'}) {
        my ($address, $error) = grep (
            { $_->{is_active} }
            @{ $params->{PRSADRCOR} }
        );
        $changed{'correspondentie'} = 1 if (exists $address->{ADR}->{straatnaam} || exists $address->{ADR}->{adresBuitenland1});
    }

    if ($params->{'PRSPRSHUW'}) {
        $changed{'partner'} = 1;
    }

    return \%changed;
}



=head2 get_params_for_natuurlijk_persoon_partner

Gets a set of params for manipulating the partner entry for a PRS.

=cut

sub _get_params_for_natuurlijk_persoon_partner {
    my $self                                = shift;
    my ($index, $params, $object_params)    = @_;

    ### Ignore partner data when exists
    if (exists($object_params->{extraElementen}{ 'geslachtsnaamEchtgenoot' })) {
        $params->{partner_voorvoegsel}      = $object_params->{extraElementen}{ 'voorvoegselsGeslachtsnaamEchtgenoot' };
        $params->{partner_geslachtsnaam}    = $object_params->{extraElementen}{ 'geslachtsnaamEchtgenoot' };

        return 1;
    }

    return unless exists($object_params->{PRSPRSHUW});

    my $partner            = $self->get_active_partner($index);

    for my $key (keys %{ NATUURLIJK_PERSOON_PARTNER_MAP() }) {
        $params->{NATUURLIJK_PERSOON_PARTNER_MAP->{$key}} = $partner->{ $key };
    }
}

sub get_active_partner {
    my $self                        = shift;
    my $index                       = shift || 0;

    my $as_params   = (
        UNIVERSAL::isa($self->as_params, 'ARRAY')
            ? $self->as_params->[$index]
            : $self->as_params
    );

    return unless exists($as_params->{PRS}->{PRSPRSHUW});

    my ($partner)            = sort { $b->{datumSluiting} <=> $a->{datumSluiting} } @{ $as_params->{PRS}->{PRSPRSHUW} };

    return $partner->{PRS};
}

=head2 Active calls


=head2 search

=cut

define_profile search => (
    required        => [qw/reference_id date/],
    optional        => [qw/
        bsn-nummer
        voornamen
        voorletters
        voorvoegsel
        geslachtsnaam
        geslachtsaanduiding
        geboortedatum
        sleutelGegevensbeheer
        sleutelVerzendend
    /],
);

sub search {
    my $self                        = shift;
    my $options                     = assert_profile(shift || {})->valid;

    throw(
        'stuf/set_afnemerindicatie/invalid_stuurgegevens',
        'Cannot run this function without stuurgegevens set on object'
    ) unless $self->stuurgegevens;

    my $stufobject      = Zaaksysteem::StUF->new(
        entiteittype    => $self->entiteittype,
        stuurgegevens   => $self->stuurgegevens,
        namespace       => 'http://www.egem.nl/StUF/sector/bg/0204',
        element         => 'beantwoordSynchroneVraagIntegraal',
        asynchroon      => 1,
        soap_ssl_key    => $self->soap_ssl_key,
        soap_ssl_crt    => $self->soap_ssl_crt,
    );

    $stufobject->stuurgegevens->berichtsoort('Lv01');
    $stufobject->stuurgegevens->entiteittype($self->entiteittype);

    $stufobject->stuurgegevens->referentienummer(
        $options->{reference_id}
    );
    $stufobject->stuurgegevens->tijdstipBericht(
        $options->{date}->strftime('%Y%m%d%H%M%S00')
    );

    my $params          = { map(
        {
            $_ => Zaaksysteem::StUF::Body::Field->new(value => $options->{ $_ })
        } grep(
            {
                $_ ne 'date' && $_ ne 'reference_id' && $options->{ $_ }
            }
            keys %{ $options }
        )
    ) };

    $stufobject->stuurgegevens->vraag({});

    my $entiteit_class = 'Zaaksysteem::StUF::Body::' . $self->entiteittype;

    $stufobject->body(
        [
            $entiteit_class->new(
                %$params
            ),
            $entiteit_class->new(
                %$params
            ),
            $entiteit_class->new(
                is_description          => 1,
            ),
        ]
    );

    return $stufobject;
}

sub binnen_gemeente {
    my $self            = shift;
    my $gemeente_code   = shift;

    my $old_params      = $self->get_params_for_natuurlijk_persoon_adres(0, $self->body->[0]->as_params->{PRS});

    ### Mutation or new.
    my $new_params;
    if ($self->body->[1]) {
        $new_params     = $self->get_params_for_natuurlijk_persoon_adres(0, $self->body->[1]->as_params->{PRS});
    } else {
        $new_params     = $old_params;
    }

    if (
        (!$new_params->{landcode} || $new_params->{landcode} == 6030) &&
        $new_params->{gemeente_code} && int($new_params->{gemeente_code}) == int($gemeente_code)
    ) {
        return 1;
    }

    return;
}



1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 binnen_gemeente

TODO: Fix the POD

=cut

=head2 get_active_partner

TODO: Fix the POD

=cut

