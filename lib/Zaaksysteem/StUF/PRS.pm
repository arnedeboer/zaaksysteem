package Zaaksysteem::StUF::PRS;

use Moose::Role;

use Zaaksysteem::Constants;
use Zaaksysteem::Exception;
use Zaaksysteem::Profile;

use Data::FormValidator;

use Zaaksysteem::StUF::Body::Field;

use constant NATUURLIJK_PERSOON_MAP                 => {
    'a-nummer'                      => 'a_nummer',
    'bsn-nummer'                    => 'burgerservicenummer',
    'voornamen'                     => 'voornamen',
    'voorletters'                   => 'voorletters',
    'voorvoegselGeslachtsnaam'      => 'voorvoegsel',
    'geslachtsnaam'                 => 'geslachtsnaam',
    'geboortedatum'                 => 'geboortedatum',
    'geslachtsaanduiding'           => 'geslachtsaanduiding',
    'datumOverlijden'               => 'datum_overlijden',
    'indicatieGeheim'               => 'indicatie_geheim',
    'burgerlijkeStaat'              => 'burgerlijkestaat',
    'aanduidingNaamgebruik'         => 'aanduiding_naamgebruik',
};

use constant NATUURLIJK_PERSOON_PARTNER_MAP         => {
    'a-nummer'                      => 'partner_a_nummer',
    'bsn-nummer'                    => 'partner_burgerservicenummer',
    'voorvoegselGeslachtsnaam'      => 'partner_voorvoegsel',
    'geslachtsnaam'                 => 'partner_geslachtsnaam',
};

use constant NATUURLIJK_PERSOON_ADRES_MAP           => {
    'postcode'                      => 'postcode',
    'woonplaatsnaam'                => 'woonplaats',
    'straatnaam'                    => 'straatnaam',
    'huisnummer'                    => 'huisnummer',
    'huisletter'                    => 'huisletter',
    'huisnummertoevoeging'          => 'huisnummertoevoeging',
    'gemeentecode'                  => 'gemeente_code',
    'landcode'                      => 'landcode',
    'adresBuitenland1'              => 'adres_buitenland1',
    'adresBuitenland2'              => 'adres_buitenland2',
    'adresBuitenland3'              => 'adres_buitenland3',
};

=head2 METHODS

=head2 get_params_for_natuurlijk_persoon

Gets a set of params for manipulating natuurlijk_persoon

=cut

sub get_params_for_natuurlijk_persoon {
    my $self            = shift;
    my $index           = shift || 0;

    my $params          = {};
    my $object_params   = (
        UNIVERSAL::isa($self->as_params, 'ARRAY')
            ? $self->as_params->[$index]->{PRS}
            : $self->as_params->{PRS}
    );

    for my $key (keys %{ NATUURLIJK_PERSOON_MAP() }) {
        next unless exists($object_params->{$key});

        $params->{NATUURLIJK_PERSOON_MAP->{$key}} = $object_params->{ $key };
    }

    $self->_get_params_for_natuurlijk_persoon_partner($index, $params, $object_params);

    ### Validate params
    my $valid_params    = Data::FormValidator->check(
        $params,
        GEGEVENSMAGAZIJN_GBA_PROFILE
    )->valid;

    # Make sure we did not collect extra "default" params from formvalidator check
    delete($valid_params->{landcode});

    return $valid_params;
}

=head2 get_params_for_natuurlijk_persoon_adres

Gets a set of params for manipulating the adres entry for a PRS.

=cut

sub get_params_for_natuurlijk_persoon_adres {
    my $self                        = shift;
    my $index                       = shift || 0;
    my $object_params               = shift;
    my ($params, $address)          = ({}, {});

    unless ($object_params) {
        $object_params   = (
            UNIVERSAL::isa($self->as_params, 'ARRAY')
                ? $self->as_params->[$index]->{PRS}
                : $self->as_params->{PRS}
        );
    }

    return unless (
        exists($object_params->{PRSADRVBL}) ||
        exists($object_params->{PRSADRCOR})
    );

    if (
        exists($object_params->{PRSADRVBL}) &&
        scalar(@{ $object_params->{PRSADRVBL}})
    ) {
        my $error;

        ($address, $error) = grep (
            { $_->{is_active} }
            @{ $object_params->{PRSADRVBL} }
        );

        if ($error) {
            throw(
                'stuf/prs/get_params_for_natuurlijk_persoon_adres/multiple',
                'Got multiple address..that cannot be right'
            );
        }

        $params->{functie_adres} = 'W';
    } elsif (
        exists($object_params->{PRSADRCOR}) &&
        scalar(@{ $object_params->{PRSADRCOR}})
    ) {
        my $error;

        ($address, $error) = grep (
            { $_->{is_active} }
            @{ $object_params->{PRSADRCOR} }
        );

        if ($error) {
            throw(
                'stuf/prs/get_params_for_natuurlijk_persoon_adres/multiple',
                'Got multiple address..that cannot be right'
            );
        }

        $params->{functie_adres} = 'B';
    }

    return $params unless $address;

    for my $key (keys %{ NATUURLIJK_PERSOON_ADRES_MAP() }) {
        next unless exists($address->{ADR}->{$key});

        $params->{NATUURLIJK_PERSOON_ADRES_MAP->{$key}} = $address->{ADR}->{ $key };
    }

    ### Validate params
    my $valid_params    = Data::FormValidator->check(
        $params,
        GEGEVENSMAGAZIJN_GBA_PROFILE
    )->valid;

    return $valid_params;
}


=head2 get_params_for_natuurlijk_persoon_partner

Gets a set of params for manipulating the partner entry for a PRS.

=cut

sub _get_params_for_natuurlijk_persoon_partner {
    my $self                                = shift;
    my ($index, $params, $object_params)    = @_;

    return unless exists($object_params->{PRSPRSHUW});

    my $partner            = $self->get_active_partner($index);

    for my $key (keys %{ NATUURLIJK_PERSOON_PARTNER_MAP() }) {
        $params->{NATUURLIJK_PERSOON_PARTNER_MAP->{$key}} = $partner->{ $key };
    }
}

sub get_active_partner {
    my $self                        = shift;
    my $index                       = shift || 0;

    my $as_params   = (
        UNIVERSAL::isa($self->as_params, 'ARRAY')
            ? $self->as_params->[$index]
            : $self->as_params
    );

    return unless exists($as_params->{PRS}->{PRSPRSHUW});

    my ($partner)            = sort { $b->{datumSluiting} <=> $a->{datumSluiting} } @{ $as_params->{PRS}->{PRSPRSHUW} };

    return $partner->{PRS};
}

=head2 Active calls


=head2 search

=cut

define_profile search => (
    required        => [qw/reference_id date/],
    optional        => [qw/
        bsn-nummer
        voornamen
        voorletters
        voorvoegsel
        geslachtsnaam
        geslachtsaanduiding
        geboortedatum
        sleutelGegevensbeheer
        sleutelVerzendend
    /],
);

sub search {
    my $self                        = shift;
    my $options                     = assert_profile(shift || {})->valid;

    throw(
        'stuf/set_afnemerindicatie/invalid_stuurgegevens',
        'Cannot run this function without stuurgegevens set on object'
    ) unless $self->stuurgegevens;

    my $stufobject      = Zaaksysteem::StUF->new(
        entiteittype    => $self->entiteittype,
        stuurgegevens   => $self->stuurgegevens,
        namespace       => 'http://www.egem.nl/StUF/sector/bg/0204',
        element         => 'beantwoordSynchroneVraagIntegraal',
        asynchroon      => 1,
        soap_ssl_key    => $self->soap_ssl_key,
        soap_ssl_crt    => $self->soap_ssl_crt,
    );

    $stufobject->stuurgegevens->berichtsoort('Lv01');
    $stufobject->stuurgegevens->entiteittype($self->entiteittype);

    $stufobject->stuurgegevens->referentienummer(
        $options->{reference_id}
    );
    $stufobject->stuurgegevens->tijdstipBericht(
        $options->{date}->strftime('%Y%m%d%H%M%S00')
    );

    my $params          = { map(
        {
            $_ => Zaaksysteem::StUF::Body::Field->new(value => $options->{ $_ })
        } grep(
            {
                $_ ne 'date' && $_ ne 'reference_id' && $options->{ $_ }
            }
            keys %{ $options }
        )
    ) };

    $stufobject->stuurgegevens->vraag({});

    my $entiteit_class = 'Zaaksysteem::StUF::Body::' . $self->entiteittype;

    $stufobject->body(
        [
            $entiteit_class->new(
                %$params
            ),
            $entiteit_class->new(
                %$params
            ),
            $entiteit_class->new(
                is_description          => 1,
            ),
        ]
    );

    return $stufobject;
}

sub binnen_gemeente {
    my $self            = shift;
    my $gemeente_code   = shift;

    my $old_params      = $self->get_params_for_natuurlijk_persoon_adres(0, $self->body->[0]->as_params->{PRS});

    ### Mutation or new.
    my $new_params;
    if ($self->body->[1]) {
        $new_params     = $self->get_params_for_natuurlijk_persoon_adres(0, $self->body->[1]->as_params->{PRS});
    } else {
        $new_params     = $old_params;
    }

    if (
        (!$new_params->{landcode} || $new_params->{landcode} == 6030) &&
        $new_params->{gemeente_code} && int($new_params->{gemeente_code}) == int($gemeente_code)
    ) {
        return 1;
    }

    return;
}



1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 binnen_gemeente

TODO: Fix the POD

=cut

=head2 get_active_partner

TODO: Fix the POD

=cut

