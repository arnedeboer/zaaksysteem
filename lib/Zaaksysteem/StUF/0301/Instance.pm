package Zaaksysteem::StUF::0301::Instance;

use File::Spec::Functions qw(catfile);
use Moose;

use Zaaksysteem::Constants qw(STUF_XSD_PATH STUF_XML_URL);

with 'Zaaksysteem::XML::Compile::Instance';

use constant STUF_ELEMENTS => [
    {
        element  => '{http://www.egem.nl/StUF/sector/bg/0310}npsLk01',
        compile  => 'RW',
        method   => 'npslk01',
    },
    {
        element  => '{http://www.egem.nl/StUF/sector/bg/0310}aoaLk01',
        compile  => 'RW',
        method   => 'aoalk01',
    },
    {
        element  => '{http://www.egem.nl/StUF/sector/bg/0310}oprLk01',
        compile  => 'RW',
        method   => 'oprlk01',
    },
    {
        element  => '{http://www.egem.nl/StUF/StUF0301}Bv01Bericht',
        compile  => 'RW',
        method   => 'bevestigingsbericht',
    },
    {
        element  => '{http://www.egem.nl/StUF/StUF0301}Fo02Bericht',
        compile  => 'RW',
        method   => 'foutbericht',
    },

];

=head1 NAME

Zaaksysteem::StUF::0301::Instance - StUF0301 instance object for L<Zaaksysteem::XML::Compile>

=head1 SYNOPSIS

    ### Within a different module
    my $backend     = Zaaksysteem::XML::Compile->xml_compile;

    $backend->add_class('Zaaksysteem::StUF::0301::Instance');

    ### Load StUF 0301 instance
    my $stuf0204    = $backend->stuf0204;

    $stuf0204->npslk01('WRITER', { stuurgegevens => {...}});
    $stuf0204->bevestigingsbericht('READER', $xml);


=head1 DESCRIPTION

=head1 REQUIRED ATTRIBUTES

See L<Zaaksysteem::XML::Compile::Instance> for a description of Required attributes

=cut

has 'name'  => (
    'is'        => 'ro',
    'default'   => 'stuf0301'
);

has 'elements'  => (
    'is'        => 'ro',
    'default'   => sub {
        return STUF_ELEMENTS;
    }
);

has 'stuf_version' => (
    is      => 'ro',
    default => '0301',
    isa     => 'Str',
);

has 'stuf_bg_version' => (
    is      => 'ro',
    default => '0310',
    isa     => 'Str',
);

has 'schemas'   => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my $self        = shift;

        return [
            catfile($self->home, STUF_XSD_PATH, sprintf("%s/stuf%s.xsd", $self->stuf_version, $self->stuf_version)),
            catfile($self->home, STUF_XSD_PATH, sprintf("bg%s/bg%s_msg_totaal.xsd", $self->stuf_bg_version, $self->stuf_bg_version)),
            catfile($self->home, STUF_XSD_PATH, 'bg0310/bag/bg0310_ent_bag.xsd'),
            catfile($self->home, STUF_XSD_PATH, 'bg0310/bag/bg0310_msg_bag.xsd'),
            catfile($self->home, STUF_XSD_PATH, 'bg0310/bag/bg0310_msg_stuf_bag.xsd'),
            catfile($self->home, STUF_XSD_PATH, 'bg0310/bg0310_msg_totaal.xsd'),
            catfile($self->home, STUF_XSD_PATH, 'bg0310/entiteiten/bg0310_ent_basis.xsd'),
            catfile($self->home, STUF_XSD_PATH, 'bg0310/entiteiten/bg0310_simpleTypes.xsd'),
            catfile($self->home, STUF_XSD_PATH, 'bg0310/entiteiten/bg0310_stuf_simpleTypes.xsd'),
            catfile($self->home, STUF_XSD_PATH, 'bg0310/mutatie/bg0310_ent_mutatie.xsd'),
            catfile($self->home, STUF_XSD_PATH, 'bg0310/mutatie/bg0310_msg_mutatie.xsd'),
            catfile($self->home, STUF_XSD_PATH, 'bg0310/mutatie/bg0310_msg_stuf_mutatie.xsd'),
            catfile($self->home, STUF_XSD_PATH, 'bg0310/vraagAntwoord/bg0310_ent_vraagAntwoord.xsd'),
            catfile($self->home, STUF_XSD_PATH, 'bg0310/vraagAntwoord/bg0310_msg_stuf_vraagAntwoord.xsd'),
            catfile($self->home, STUF_XSD_PATH, 'bg0310/vraagAntwoord/bg0310_msg_vraagAntwoord.xsd'),
            catfile($self->home, STUF_XSD_PATH, 'gml/bag-gml.xsd'),
            catfile($self->home, STUF_XSD_PATH, 'xlink/xlinks.xsd'),
        ];
    }
);

=head1 METHODS

=head2 egem

Arguments: none

Return Value: $URL_OF_STUF_STANDARD

    print $self->egem;

    # http://www.egem.nl/StUF/StUF0301

=cut

sub egem {
    my $self            = shift;

    return join('/', STUF_XML_URL, 'StUF0301');
}

has '_reader_config' => (
    'is'        => 'rw',
    'default'   => sub {
        my $self = shift;

        my $egem = $self->egem;

        my $res  = {
             sloppy_integers                => 1,
             interpret_nillable_as_optional => 1,
             check_values                   => 0,
             'hooks'                        => [{
                     after => sub {
                         my ($node, $value, $path) = @_;

                         my @find = qw(
                           sleutelGegevensbeheer
                           verwerkingssoort
                           sleutelVerzendend
                         );

                         foreach (@find) {
                             _parse_node_atribute(
                                 $egem,
                                 node     => $node,
                                 atribute => $_,
                                 value    => $value
                             );
                         }

                         if ($node->getAttributeNS($egem, 'noValue')) {
                            $value = {
                                '_'     => 'NIL',
                                noValue => $node->getAttributeNS($egem, 'noValue')
                            };
                         }

                         return $value;
                    }
            }]
        };
    }
);

has '_writer_config' => (
    'is'        => 'rw',
    'default'   => sub {
        my $self = shift;

        my $egem = $self->egem;

        my $res  = {
             ignore_unused_tags             => 1,
        };
    }
);


=head2 _parse_node_attribute

Parses an attribute of a C<$node>, supplied for C<_reader_config> of this class.

=cut

sub _parse_node_atribute {
    my $egem = shift;
    my $args = {@_};
    if ($args->{node}->hasAttributeNS($egem, $args->{atribute})) {
        $args->{value}{$args->{atribute}} = $args->{node}->getAttributeNS($egem, $args->{atribute});
    }
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Install>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 STUF_ELEMENTS

TODO: Fix the POD

=cut

=head2 STUF_XML_URL

TODO: Fix the POD

=cut

=head2 STUF_XSD_PATH

TODO: Fix the POD

=cut

