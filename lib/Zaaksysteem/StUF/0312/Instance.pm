package Zaaksysteem::StUF::0312::Instance;

use File::Spec::Functions qw(catfile);
use Moose;

use XML::Compile::WSDL11;
use XML::Compile::SOAP11;
use XML::Compile::Util;
use XML::Compile::Transport::SOAPHTTP;

use Zaaksysteem::Constants qw(STUF_XSD_PATH);

with qw/
    Zaaksysteem::XML::Compile::Instance
    Zaaksysteem::StUF::GeneralInstance
/;

=head2 name

Through this the instance will be accessible for the app, from the compiled singleton.

=cut

has 'name'  => (
    'is'        => 'ro',
    'default'   => 'stuf0312'
);

has elements => (
    is      => 'ro',
    default => sub {
        [
            {
                element => '{http://www.egem.nl/StUF/sector/lvo/0312}omvDi01AanbiedenAanvraag',
                compile => 'READER',
                #method   => 'aanvraag_omgevingsloket',
            },
            {
                element => '{http://www.egem.nl/StUF/sector/lvo/0312}vrgDi01IndienenAanvulling',
                compile => 'READER',
#                method   => 'aanvulling_omgevingsloket',
            }
        ];
    }
);

use constant XML_DEFINITIONS => qw|
    lvo0312/stuflvo0312.xsd
    0301/stuf0301.xsd
    xmlmime/xmlmime.xsd
    gml/bag-gml.xsd
    xlink/xlinks.xsd
    0301/stuf0301mtom.xsd
    bg0310/entiteiten/bg0310_ent_basis.xsd
    bg0310/entiteiten/bg0310_simpleTypes.xsd
    bg0310/entiteiten/bg0310_stuf_simpleTypes.xsd
    lvo0312/standardtypes0312.ent.xsd
    lvo0312/lvo0312.ent.xsd
    lvo0312/lvo0312.msg.xsd
    lvo0312/zknlvo0312.ent.xsd
    lvo0312/bglvo0312.ent.xsd
    lvo0312/lvowerkzaamheden.ent.xsd
    zkn0310/entiteiten/zkn0310_bg0310_ent.xsd
    zkn0310/entiteiten/zkn0310_ent_basis.xsd
    zkn0310/entiteiten/zkn0310_simpleTypes.xsd
    zkn0310/mutatie/zkn0310_ent_mutatie.xsd
|;

=head2 schemas

List of file paths to xsd files.

=cut

has schemas   => (
    is        => 'ro',
    lazy      => 1,
    default   => sub {
        my $self = shift;
        return [ map { catfile($self->home, STUF_XSD_PATH, $_) } XML_DEFINITIONS ];
    }
);

=head2 get_schema

Return XML::Compile schema object, in this case the WSDL object.

=cut

sub get_schema {
    my $self = shift;

    my $base = catfile($self->home, STUF_XSD_PATH);
    my $wsdl_path = 'lvo0312/lvo0312.bevoegdGezag.ontvangAsynchroon.wsdl';

    my $wsdl = XML::Compile::WSDL11->new(
        catfile($base, $wsdl_path),
    );

    $wsdl->importDefinitions(catfile($base, $_))
        for XML_DEFINITIONS;;

    $wsdl->addWSDL(catfile($base, $_))
        for ('0301/stuf0301_services.wsdl', '0301/stuf0301_types.wsdl');

    $wsdl->compileClient('omvDi01AanbiedenAanvraag');
    $wsdl->compileClient('vrgDi01IndienenAanvulling');

    return $wsdl;
}

=head2 aanvraag_omgevingsloket

Parse the XML from OLO aanvraag

=cut

sub aanvraag_omgevingsloket {
    my ($self, $rw, $xml) = @_;
    return $self->omvDi01AanbiedenAanvraag_reader($xml);

}

=head2 aanvulling_omgevingsloket

Parse the XML from OLO aanvulling

=cut

sub aanvulling_omgevingsloket {
    my ($self, $rw, $xml) = @_;
    return $self->vrgDi01IndienenAanvulling_reader($xml);
}

=head2 _read_xml

Read the XML and return the XML node object.

=cut

sub _read_xml {
    my ($self, $xml) = @_;
    my $dom = XML::LibXML->load_xml(string => $xml);
    my ($soap_body) = $dom->findnodes("//*[local-name()='Body']");
    my ($xml_node) = $soap_body->getChildrenByTagName('*');
    return $xml_node;
}

=head2 omvDi01AanbiedenAanvraag_reader

Read xml message into a parsed nodetree.

=cut

sub omvDi01AanbiedenAanvraag_reader {
    my ($self, $xml) = @_;

    my $element = '{http://www.egem.nl/StUF/sector/lvo/0312}omvDi01AanbiedenAanvraag';
    return $self->schema->reader($element)->($self->_read_xml($xml));
}

=head2 vrgDi01IndienenAanvulling_reader

Read xml message into a parsed nodetree.

=cut

sub vrgDi01IndienenAanvulling_reader {
    my ($self, $xml) = @_;

    my $element = '{http://www.egem.nl/StUF/sector/lvo/0312}vrgDi01IndienenAanvulling';
    return $self->schema->reader($element)->($self->_read_xml($xml));
}

=head2 _reader_config

Required override for additional configuration for the reader,
we like it the way it is right now.

=cut

has _reader_config => (is => 'ro', default => sub {});

=head2 extract_scalar

Given a hashref with an _ member and potentially more -- return a scalar
representation.

=cut

sub extract_scalar {
    my ($self, $value) = @_;

    my $payload = ref $value eq 'HASH' && exists $value->{_} && $value->{_};
    return ref $payload eq 'Math::BigFloat' ? $payload->bstr : $payload;
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Install>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
