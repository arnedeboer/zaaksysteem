package Zaaksysteem::StUF::GeneralInstance;

use Moose::Role;

use XML::LibXML;
use Zaaksysteem::Tools;

use XML::Compile::Util;
use Scalar::Util qw/blessed/;


=head1 NAME

Zaaksysteem::StUF::GeneralInstance - General tools for StUF 0204/0301 instances.

=head1 SYNOPSIS

    my $xml = $stuf0204->retrieve_xml_string({ xml => '<xml><element /></xml>'});
    my $xml = $stuf0204->retrieve_xml_string({ filename => '/path/to/xml_file.xml'});

    # Returns: plain text xml

    my ($xml_element) = $stuf0204->retrieve_xml_element(
        {
            xml     => '<xml><soap><kennisgevingsbericht></kennisgevingsbericht></soap></xml>',
            element => '{namespace}kennisgevingsbericht'
        }
    );

    # Returns: XML::LibXML::Element containing "kennisgevingsbericht"

    $instance->plaintext_value(
        {
          '_' => 'Tinus',
          'exact' => 1,
          'gegevengroep' => 'PRS1',
          'kerngegeven' => 1
        },
    );

    # Returns: "Tinus"

    $instance->get_params_by_definition(
        {
            hash => {
                person => {
                    firstname => 'Wim',
                    lastname  => 'de Bie',
                }
            },
            definition => [
                {
                    path    => '$.person.firstname',
                    name    => 'first_name'
                },
                {
                    path    => '$.person.lastname',
                    name    => 'last_name'
                }
            ]
        }
    );

    # Returns: { first_name => "Wim", last_name => "de Bie" }

=head1 DESCRIPTION

StUF related functions for inclusion by either stuf 0301 and stuf 0204.

=head1 METHODS

=head2 retrieve_xml_string

Arguments: \%OPTIONS

Return value: $STRING_XML

    my $xml = $stuf0204->retrieve_xml_string({ xml => '<xml><element /></xml>'});

    my $xml = $stuf0204->retrieve_xml_string({ filename => '/path/to/xml_file.xml'});

Extracts xml from one of the options given, does some checks to ensure proper error handling.

B<OPTIONS>

=over 4

=item xml

String containing the xml. This charade will let this function just return the input, when
it is set ofcourse.

=item filename

String containing the filename. This function will read the xml, and return it as string

=back

=cut

define_profile 'retrieve_xml_string' => (
    required        => [],
    require_some    => {
        'xml_or_filename'   => [1, qw/xml filename/],
    }
);

sub retrieve_xml_string {
    my $self            = shift;
    my $params          = assert_profile(shift || {})->valid;


    if ($params->{xml}) {
        throw(
            'stuf/generalinstance/retrieve_xml_string/invalid_xml',
            'Given "xml" option does not contain plain xml'
        ) if ref $params->{xml};

        return $params->{xml};
    } elsif ($params->{filename}) {
        open(my $FH, '<:encoding(utf-8)', $params->{filename}) or throw(
            'stuf/generalinstance/retrieve_xml_string/file_error',
            'Could not open file: ' . $params->{filename}
        );

        my $string  = '';
        while (<$FH>) {
            $string .= $_;
        }

        close($FH);

        return $string;
    }
}

=head2 retrieve_xml_element

Arguments: \%OPTIONS

Return value: @elements

    ### Returns the first XML::LibXML::Element matching "element"
    my ($xml_element) = $stuf0204->retrieve_xml_element(
        {
            xml     => '<xml><soap><kennisgevingsbericht></kennisgevingsbericht></soap></xml>',
            element => '{namespace}kennisgevingsbericht'
        }
    );

    Returns the first XML::LibXML::Element matching "element" supplied by method
    my $xml = $stuf0204->retrieve_xml_element(
        {
            xml     => '<xml><soap><kennisgevingsbericht></kennisgevingsbericht></soap></xml>',
            method  => 'kennisgevingsbericht'
        }
    );

Inherits from retrieve_xml_string, but makes sure any unnecessary xml surrounding the
element will be stripped.

B<OPTIONS>

See L<< $instance->retrieve_xml_string >> for base options

=over 4

=item element

String containing the xml. This charade will let this function just return the input, when
it is set ofcourse.

=item method

String containing the filename. This function will read the xml, and return it as string

=back

=cut

define_profile 'retrieve_xml_element' => (
    required        => [],
    require_some    => {
        'method_or_element'   => [1, qw/element method/],
    }
);

sub retrieve_xml_element {
    my $self            = shift;
    my $params          = assert_profile($_[0] || {})->valid;

    my $xml             = $self->retrieve_xml_string($_[0]);

    my $element;
    if ($params->{method}) {
        my ($element_data) = grep(
            { $_->{method} eq $params->{method} }
            @{ $self->elements }
        );

        throw(
            'stuf/generalinstance/retrieve_xml_element/method_not_found',
            'Given method "' . $params->{method} . '" was not found'
        ) unless $element_data;

        $element = $element_data->{element};
    } else {
        $element = $params->{element};
    }

    my $doc             = XML::LibXML->load_xml(string => $xml);

    my ($ns, $tag)      = unpack_type $element;

    my @elements        = $doc->getElementsByTagNameNS($ns, $tag);

    return @elements;
}


=head2 plaintext_value

Arguments: $rawvalue

Return value: $PLAINTEXT_STRING

    $instance->plaintext_value(
        {
          '_' => 'Tinus',
          'exact' => 1,
          'gegevengroep' => 'PRS1',
          'kerngegeven' => 1
        },
    );

    # Return: 'Tinus'

    $instance->plaintext_value(
        bless( {
              '_e' => [
                0
              ],
              '_es' => '+',
              '_m' => [
                19620529
              ],
              'sign' => '+'
            }, 'Math::BigFloat'
        )
    );

    # Return: '19620529'

Returns the plain text representation of a given value. Essentially stripping the surrounding object

=cut

sub plaintext_value {
    my $self        = shift;
    my $rawvalue    = shift;

    ### Normal values, just return the input
    if (
        !ref $rawvalue
    ) {
        ### NIL = undef
        if ($rawvalue && $rawvalue eq 'NIL') {
            return undef;
        }

        return $rawvalue;
    } elsif (
        ref $rawvalue eq 'ARRAY'
    ) {
        return $rawvalue;
    }

    ### Hash, could contain a value, or is just a hash...
    if (ref $rawvalue eq 'HASH') {
        ### Return hash when it is not a value
        if (!exists $rawvalue->{'_'}) {
            return $rawvalue;
        }

        ### Value itself could be blessed
        return $self->plaintext_value($rawvalue->{_});

    ### Blessed reference, probably Math::BigFloat or BigInt or some kind
    } elsif (blessed($rawvalue)) {

        if ($rawvalue->can('bstr')) {
            return $rawvalue->bstr;
        } else {
            throw(
                'stuf/generalinstance/plaintext_value/unknown_object',
                'Don\'t know how to return a string value for object: ' . ref($rawvalue)
            );
        }
    }

}

=head2 get_params_by_definition

Arguments: \%OPTIONS

Return value: \%HASH_OF_VALUES

    $instance->get_params_by_definition(
        {
            hash => {
                person => {
                    firstname => 'Wim',
                    lastname  => 'de Bie',
                }
            },
            definition => [
                {
                    path    => '$.person.firstname',
                    name    => 'first_name'
                },
                {
                    path    => '$.person.lastname',
                    name    => 'last_name'
                }
            ]
        }
    );

    # Return: 'Tinus'

    {
        first_name  => 'Wim',
        last_name   => 'de Bie',
    }

Given a "json_path" plan, it will return a hash with parameters. This way you could translate
a complex perl hash into a simpler, one dimensional, version.

B<Options>

=over 4

=item hash

Perl hash containing the complex definition

=item name

Key name in the returned simplified one dimensional hash.

=back

=cut

define_profile 'get_params_by_definition' => (
    required        => [qw/definition hash/]
);

sub get_params_by_definition {
    my $self                    = shift;
    my $options                 = shift;

    my $data                    = $options->{hash};

    my %rv;
    for my $def (@{ $options->{definition} }) {
        my $value           = JSON::Path->new($def->{path})->value($data);

        ### TODO: Verify if value is "defined".

        $rv{ $def->{name} } = $self->plaintext_value($value);
    }

    return \%rv;
}


=head2 handle_message

Arguments: $METHOD_NAME, \%PERLHASH [, \%OPTIONS]

Return value: $options->{dispatch} ? L<Zaaksysteem::XML::Compile::Instance#call> : \%HASH_RESPONSE

    my ($hashresponse, $trace) = $self->handle_message(
        'vraagbericht',
        $perldata,
        {
            %{ $options },
            translate   => 'WRITER',
            dispatch    => {
                transport_hook          => sub {
                    return HTTP::Response->new(200, 'answer manually created',
                        [ 'Content-Type' => 'text/xml' ], '<xml><my /><custom /></xml>'
                    );
                }
            }
        }
    );

    ### OR

    my $xmlrequest = $self->handle_message(
        'vraagbericht',
        $perldata,
        {
            %{ $options },
            translate   => 'WRITER',
        }
    );

Handles the message depending on the whether the dispatch option is given. Either generates
an XML message from the given C<\%perldata> or dispatches this data to the configured SOAP call

B<Options>

=over 4

=item dispatch [optional]: ArrayRef

When set, the given dispatch hash will be brought to C<< $self->call >>, and a soap call
is fired.

The response is the same response as L<XML::Compile::WSDL11#call>

When no dispatch is given, the C<\%PERLHASH> will be translated to plain text XML.

=back

=cut

sub handle_message {
    my $self                = shift;
    my $method              = shift;
    my $perldata            = shift;
    my $options             = shift;

    if ($options->{dispatch}) {
        my ($rv, $trace) = $self->call($method, $perldata, $options->{dispatch});

        # print STDERR "Handle message: " . Data::Dumper::Dumper($trace) . "\n";

        return ($rv, $trace);
    } else {
        return $self->$method($options->{translate}, $perldata);
    }
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::StUF::0204::Instance> L<Zaaksysteem::Manual>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
