package Zaaksysteem::StUF::NNP;

use Moose::Role;
use Zaaksysteem::Constants;

use constant ORGANIZATION_MAP                       => {
    'handelsRegisternummer'         => 'dossiernummer',
    'indicatieFaillisement'         => 'faillisement',
    'indicatieSurseanceVanBetaling' => 'surseance',
    'rechtsvorm'                    => 'rechtsvorm',
};

use constant ORGANIZATION_ADDRESS_MAP => {
    'huisnummer'            => 'huisnummer',
    'huisletter'            => 'huisletter',
    'huisnummertoevoeging'  => 'huisnummertoevoeging',
    'postcode'              => 'postcode',
    'straatnaam'            => 'straatnaam',
    'adresBuitenland1'      => 'adres_buitenland1',
    'adresBuitenland2'      => 'adres_buitenland2',
    'adresBuitenland3'      => 'adres_buitenland3',
    'landcode'              => 'landcode',
};

=head2 METHODS

=head2 get_params_for_organization

Gets a set of params for manipulating NNP (Bedrijf)

=cut

sub get_params_for_organization {
    my $self            = shift;

    my $params          = {};
    my $object_params   = $self->as_params->{NNP};

    for my $key (keys %{ ORGANIZATION_MAP() }) {
        next unless exists($object_params->{$key});

        $params->{ORGANIZATION_MAP->{$key}} = $object_params->{ $key };
    }

    if (exists($object_params->{extraElementen}->{handelsRegisterVolgnummer})) {
        $params->{subdossiernummer} = $object_params->{extraElementen}->{handelsRegisterVolgnummer};
    }

    if (exists($object_params->{extraElementen}->{vestigingsnummer})) {
        $params->{vestigingsnummer} = $object_params->{extraElementen}->{vestigingsnummer};
    }

    # Get correct naam of organisation
    $params->{handelsnaam} = $self->_get_handelsnaam($object_params);

    my $address_params  = $self->get_params_for_organization_adres;

    $params             = { %{ $params }, %{ $address_params } };

    ### Validate params
    my $valid_params    = Data::FormValidator->check(
        $params,
        GEGEVENSMAGAZIJN_KVK_PROFILE
    )->valid;

    ### Mark as deleted:
    if (
        $object_params->{'datumEindeNietNatuurlijkPersoon'} &&
        $object_params->{'datumEindeNietNatuurlijkPersoon'} < DateTime->now->strftime('%Y%m%d')
    ) {
        $valid_params->{deleted_on} = $object_params->{'datumEindeNietNatuurlijkPersoon'};
    }

    #warn(Data::Dumper::Dumper($valid_params));
    return $valid_params;
}

sub _get_handelsnaam {
    my $self                        = shift;
    my $object_params               = shift;

    ### Handelsnaam preferred, otherwise: zaaknaam.
    return $object_params->{handelsnaam} || $object_params->{zaaknaam} || $object_params->{statutaireNaamVennootschapsnaam};
}

=head2 get_params_for_natuurlijk_persoon_adres

Gets a set of params for manipulating the adres entry for a PRS.

=cut

sub get_params_for_organization_adres {
    my $self                        = shift;
    my ($params, $address)          = ({}, {});

    my $object_params               = $self->as_params->{NNP};

    return $params unless (
        exists($object_params->{NNPADRVBL}) ||
        exists($object_params->{NNPADRCOR})
    );

    my $address_prefix = '';
    if (exists($object_params->{NNPADRVBL})) {
        my $error;

        ($address, $error) = grep (
            { $_->{is_active} }
            @{ $object_params->{NNPADRVBL} }
        );

        if ($error) {
            throw(
                'stuf/nnp/get_params_for_organization_adres/multiple',
                'Got multiple address..that cannot be right'
            );
        }

        $address_prefix = 'vestiging_';
    } elsif (
        exists($object_params->{NNPADRCOR})
    ) {
        my $error;

        ($address, $error) = grep (
            { $_->{is_active} }
            @{ $object_params->{NNPADRCOR} }
        );

        if ($error) {
            throw(
                'stuf/nnp/get_params_for_organization_adres/multiple',
                'Got multiple address..that cannot be right'
            );
        }

        $address_prefix = 'correspondentie_';
    }

    return $params unless $address;

    for my $key (keys %{ ORGANIZATION_ADDRESS_MAP() }) {
        next unless exists($address->{ADR}->{$key});

        $params->{$address_prefix . ORGANIZATION_ADDRESS_MAP->{$key}} = $address->{ADR}->{ $key };
    }

    my $woonplaats = $address->{ADR}->{extraElementen}->{authentiekeWoonplaatsnaam}
        || $address->{ADR}->{woonplaatsnaam} || $address->{ADR}->{extraElementen}->{woonplaatsNaamBAG};

    $params->{$address_prefix . 'woonplaats'} = $woonplaats;

    ### Validate params
    my $valid_params    = Data::FormValidator->check(
        $params,
        GEGEVENSMAGAZIJN_KVK_PROFILE
    )->valid;

    return $valid_params;
}

1;


__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 get_params_for_organization_adres

TODO: Fix the POD

=cut

