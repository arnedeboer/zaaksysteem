package Zaaksysteem::StUF::0204::Instance;

use File::Spec::Functions qw(catfile);
use Moose;

use Zaaksysteem::Tools;
use Zaaksysteem::Constants qw(STUF_XSD_PATH STUF_XML_URL);

with qw/
    Zaaksysteem::XML::Compile::Instance
    Zaaksysteem::StUF::0204::NatuurlijkPersoon
/;

use constant STUF_ELEMENTS => [
    {
        element  => '{http://www.egem.nl/StUF/sector/bg/0204}kennisgevingsBericht',
        compile  => 'RW',
        method   => 'kennisgevingsbericht',
        soapcall => {
            call    => 'ontvangKennisgeving',
            service     => 'BG:StUFBGAsynchroon',
            port        => 'StUFBGAsynchronePort',
        },
    },
    {
        element  => '{http://www.egem.nl/StUF/sector/bg/0204}synchroonAntwoordBericht',
        compile  => 'RW',
        method   => 'antwoordbericht',
    },
    {
        element  => '{http://www.egem.nl/StUF/sector/bg/0204}vraagBericht',
        compile  => 'RW',
        method   => 'vraagbericht',
        soapcall => {
            call    => 'beantwoordSynchroneVraag',
            service     => 'BG:StUFBGSynchroon',
            port        => 'StUFBGSynchronePort',
        },
    },
    {
        element  => '{http://www.egem.nl/StUF/StUF0204}bevestigingsBericht',
        compile  => 'RW',
        method   => 'bevestigingsbericht',
    },
    {
        element  => '{http://www.egem.nl/StUF/StUF0204}foutBericht',
        compile  => 'RW',
        method   => 'foutbericht',
    },

    ### Pink
    {
        element  => '{http://makelaarsuite.nl/CML}plaatsAfnemersIndicatieVerzoek',
        compile  => 'RW',
        method   => 'pink_voa_kennisgeving',
        soapcall => {
            call    => 'plaatsAfnemersIndicatieVerzoek',
            service     => 'PlaatsenAfnemersIndicatie',
            port        => 'PlaatsenAfnemersIndicatiePort',
        },
    },
    {
        element  => '{http://makelaarsuite.nl/CML}plaatsAfnemersIndicatieAntwoord',
        compile  => 'RW',
        method   => 'pink_voa_bevestiging',
    },
];

=head1 NAME

Zaaksysteem::StUF::0204::Instance - StUF0204 instance object for L<Zaaksysteem::XML::Compile>

=head1 SYNOPSIS

    ### Within a different module
    my $backend     = Zaaksysteem::XML::Compile->xml_compile;

    $backend->add_class('Zaaksysteem::StUF::0204::Instance');

    ### Load StUF 0204 instance
    my $stuf0204    = $backend->stuf0204;

    $stuf0204->kennisgevingsbericht('WRITER', { stuurgegevens => {...}});
    $stuf0204->kennisgevingsbericht('READER', $xml);


=head1 DESCRIPTION

=head1 REQUIRED ATTRIBUTES

See L<Zaaksysteem::XML::Compile::Instance> for a description of Required attributes

=head1 ENTITIES

There are different files for handling of different entities. See the following list of
a manual map.

=over 4

=item L<Zaaksysteem::StUF::0204::NatuurlijkPersoon>

Natuurlijk Persoon specific stuf0204 instances.

=back

=cut

has 'name'  => (
    'is'        => 'ro',
    'default'   => 'stuf0204'
);

has 'elements'  => (
    'is'        => 'ro',
    'default'   => sub {
        return STUF_ELEMENTS;
    }
);

has 'wsdl'      => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my $self    = shift;

        return [
            catfile($self->home, STUF_XSD_PATH, 'bg0204/bg0204.wsdl'),
            catfile($self->home, '/share/wsdl/CML/afnemersindiciatie_voa.wsdl'),
        ];
    }
);


has 'schemas'   => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my $self        = shift;

        return [
            catfile($self->home, STUF_XSD_PATH, '0204/stuf0204.xsd'),
            catfile($self->home, STUF_XSD_PATH, 'bg0204/bgstuf0204.xsd'),
            catfile($self->home, STUF_XSD_PATH, 'bg0204/bg0204.xsd'),
            catfile($self->home, '/share/wsdl/CML/afnemersindiciatie_voa.xsd'),
            catfile($self->home, '/share/wsdl/CML/cgs_fouten.xsd'),
        ];
    }
);

# sub get_schema {
#     my $self        = shift;

#     my $wsdl = XML::Compile::WSDL11->new(
#         catfile($self->home, STUF_XSD_PATH, 'bg0204/bg0204.wsdl'),
#         opts_readers => $self->_reader_config,
#         opts_writers => $self->_writer_config,
#     );

#     $wsdl->importDefinitions($self->schemas);

#     $wsdl->compileClient(
#         'beantwoordSynchroneVraag',
#         service     => 'BG:StUFBGSynchroon',
#         port        => 'StUFBGSynchronePort'
#     );

#     return $wsdl;
# }

has '_reader_config' => (
    'is'        => 'rw',
    'default'   => sub {
        my $self = shift;

        my $egem = $self->egem;

        my $res  = {
            sloppy_integers                => 1,
            interpret_nillable_as_optional => 1,
            check_values                   => 0,
            'hooks'                        => [{
                    before => sub {
                        my ($node, $value, $path) = @_;

                        ### StUF Patch, somehow we do not know how to parse PRSADRINS messages. Also,
                        ### we ignore the PRSADRINS information, so this information is not needed
                        ### for zaaksysteem. Therefore, we remove the PRSADRINS blocks to keep the sysin
                        ### from halting
                        if ($node->localName =~ /^PRS$/) {
                            my @children = $node->childNodes();
                            my $found    = {};
                            for my $child (@children) {
                                if (   $child->localName
                                    && $child->localName =~ /^PRSADRINS$/) {
                                    if ($found->{$child->nodeName}) {
                                        $node->removeChild($child);
                                    }

                                    $found->{$child->nodeName} = 1;
                                }
                            }
                        }

                        _apply_vicrea_patches($egem, @_);

                        return $node;
                    },
                    after => sub {
                        my ($node, $value, $path) = @_;

                        my @find = qw(
                          sleutelGegevensbeheer
                          sleutelOntvangend
                          verwerkingssoort
                          sleutelVerzendend
                        );

                        foreach (@find) {
                            _parse_node_atribute(
                                $egem,
                                node     => $node,
                                atribute => $_,
                                value    => $value
                            );
                        }

                        if ($node->getAttributeNS($egem, 'noValue')) {
                            $value->{'noValue'} =
                              $node->getAttributeNS($egem, 'noValue');
                        }

                        return $value;
                      }
                }]
        };
    }
);

has '_writer_config' => (
    'is'        => 'rw',
    'default'   => sub {
        my $self            = shift;

        return {
            ignore_unused_tags  => 1,
            prefixes            => [
                'StUF'      => 'http://www.egem.nl/StUF/StUF0204',
                'BG'        => 'http://www.egem.nl/StUF/sector/bg/0204',
            ],
            'hooks' => [
                {
                    before => sub {
                        my ($doc, $value, $path) = @_;

                        if (blessed($value) && UNIVERSAL::isa($value,'Zaaksysteem::StUF::0204::Element')) {
                            $value = $value->toXML($value, $doc, $path);
                        }

                        return $value;
                    }
                },
            ],
        };
    }
);

=head1 METHODS

=head2 egem

Arguments: none

Return Value: $URL_OF_STUF_STANDARD

    print $self->egem;

    # http://www.egem.nl/StUF/StUF0204

=cut

sub egem {
    my $self            = shift;

    return join('/', STUF_XML_URL, 'StUF0204');
}

=head2 disable_subscription

Arguments: \%PARAMS, \%OPTIONS

Return value: $STRING_XML

    my $xml             = $self->xmlcompile->stuf0204->disable_subscription(
        {
            external_id             => '22553',
            subscription_id         => '84734',
            entity_type             => 'PRS'
        },
        {
            sender                  => 'ZSNL',
            receiver                => 'CMODIS',
            reference               => 'ZS0000229154',
            datetime                => '2014030209011458',
        },
    );

Generates the XML for a "kennisgevingsbericht" of type "V" (verwijderbericht). Will send
the "key_type", so it will be a subscription removal.

=over 4

=item external_id [required]

The "key" (sleutel) of the entry, so the other party knows which entry to delete.

=item subscription_id [required]

Our local ID of the entry in the L<Zaaksysteem::DB::ObjectSubscription> table.

=item entity_type [required]

One of C<PRS> or C<NNP>

=back

=cut

define_profile 'disable_subscription' => (
    required => [qw/
        external_id
        subscription_id
        entity_type
    /],
    optional    => [],
    ### Below constraints are from official StUF XSD
    constraint_methods  => {
        external_id     => qr/^\w+$/,
        subscription_id => qr/^\d+$/,
        entity_type     => qr/^(?:PRS|NNP)$/,
    }
);

sub disable_subscription {
    my $self                = shift;
    my $params              = assert_profile(shift || {})->valid;
    my ($options)           = @_;
    my $object              = {};

    $object                 = $self->_load_identification($params, $object, $options);

    my $perldata            = {
        'body'              => {
            $params->{entity_type}   => [ $object ],
        },
        'stuurgegevens'     => $self->generate_stuurgegevens(
            {
                %{ $options },
                messagetype     => 'Lk01',
                mutation_type   => 'delete',
                entitytype      => $params->{entity_type},
            }
        ),
    };

    return $self->handle_message(
        'kennisgevingsbericht',
        $perldata,
        {
            %{ $options },
            translate   => 'WRITER',
        }
    );
}

=head1 INTERNAL METHODS

=head2 _apply_vicrea_patches

Applies vicrea patches, supplied for C<_reader_config> of this class.

=cut


sub _apply_vicrea_patches {
    my ($egem, $node, $value, $path)   = @_;

    if ($node->localName =~ /^PRSPRS.+$/) {
        my @children = $node->childNodes();

        for my $child (@children) {
            next unless (
                $child->localName &&
                $child->localName eq 'PRS'
            );

            my @prs_elements = $child->childNodes();

            for my $prs_element (@prs_elements) {
                if (
                    $prs_element->localName &&
                    !grep({ lc($prs_element->localName) eq lc($_) }
                        qw/
                            a-nummer
                            bsn-nummer
                            voornamen
                            voorletters
                            voorvoegselGeslachtsnaam
                            geslachtsnaam
                            geboortedatum
                            geboorteplaats
                            codeGeboorteland
                            geslachtsaanduiding
                            adellijkeTitelPredikaat
                        /
                    )
                ) {
                    $child->removeChild($prs_element);
                }
            }
        }
    }

    ### Vicrea Patch, add nillable when we get a noValue
    ### attribute
    if (
        $node->hasAttributeNS($egem, 'noValue')
        && !$node->hasAttributeNS(
            'http://www.w3.org/2001/XMLSchema-instance', 'nil'
        )
      ) {
        $node->setAttributeNS(
            'http://www.w3.org/2001/XMLSchema-instance',
            'nil',
            'true'
        );
    }
}

=head2 _parse_node_attribute

Parses an attribute of a C<$node>, supplied for C<_reader_config> of this class.

=cut

sub _parse_node_atribute {
    my $egem = shift;
    my $args = {@_};
    if ($args->{node}->hasAttributeNS($egem, $args->{atribute})) {
        $args->{value}{$args->{atribute}} = $args->{node}->getAttributeNS($egem, $args->{atribute});
    }
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Install>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 STUF_ELEMENTS

TODO: Fix the POD

=cut

=head2 STUF_XML_URL

TODO: Fix the POD

=cut

=head2 STUF_XSD_PATH

TODO: Fix the POD

=cut

