package Zaaksysteem::StUF::0310;

use Moose::Role;
use Zaaksysteem::Profile;

sub _parse_body {
    my $self            = shift;
    my $body            = $self->parser->data->{object};

    my $objectclass     = 'Zaaksysteem::StUF::Body::' . $self->entiteittype;

    for my $xml_object (@{ $body }) {
        my $object      = $objectclass->new();

        $object->_body_params($xml_object);
        $object->_load_body_params;

        push(
            @{ $self->body },
            $object
        );
    }
}

after '_parse_stuurgegevens' => sub {
    my $self        = shift;

    if (
        $self->parser->data->{parameters} &&
        $self->parser->data->{parameters}->{mutatiesoort}
    ) {
        $self->mutatiesoort($self->parser->data->{parameters}->{mutatiesoort});
    }
};

define_profile fout => (
    required => [qw[reference_id date code omschrijving]],
    optional => [qw[plek]]
);

define_profile acknowledge => (
    required        => [qw/reference_id date/],
);

sub acknowledge {
    my $self                    = shift;
    my $options                 = assert_profile(shift || {})->valid;

    my $stufobject      = Zaaksysteem::StUF->new(
        entiteittype    => $self->entiteittype,
        stuurgegevens   => $self->stuurgegevens,
        namespace       => 'http://www.egem.nl/StUF/StUF0301',
        element         => 'Bv01Bericht',
        version         => '0310'
    );

    $stufobject->body(undef);

    $stufobject->stuurgegevens->berichtcode('Bv01');
    $stufobject->stuurgegevens->tijdstipBericht(
        $options->{date}->strftime('%Y%m%d%H%M%S00')
    );

    $stufobject->stuurgegevens->referentienummer(
        $options->{reference_id}
    );

    $stufobject->stuurgegevens->crossRefnummer(
        $options->{reference_id}
    );

    return $stufobject;
}

sub fout {
    my $self                    = shift;
    my $options                 = assert_profile(shift || {})->valid;

    $self->stuurgegevens->{crossRefnummer} = $options->{code};
    $self->stuurgegevens->berichtcode('Fo02');

    my $parser      = Zaaksysteem::StUF::Parser::0301->new(
        %{ $self->parser_options },
        namespace       => 'http://www.egem.nl/StUF/StUF0301',
        element         => 'Fo02Bericht',
        stuurgegevens   => $self->stuurgegevens,
        body            => {
            code            => $options->{code},
            plek            => $options->{plek} || 'server',
            omschrijving    => $options->{omschrijving},
        }
    );

    return $parser->to_xml;
};

# sub TO_STUF {
#     my $self        = shift;

#     my $rv          = {
#         stuurgegevens   => $self->stuurgegevens->TO_STUF,
#         parameters      => {
#             sortering                   => 0,
#             indicatorVervolgvraag       => 1,
#             maximumAantal               => 10,
#         },
#     };

#     return $rv unless $self->body;

#     my @body;
#     for my $body (@{ $self->body }) {
#         push(@body, $body->TO_STUF);
#     }

#     $rv->{body}     = \@body;

#     $rv->{body}     = {
#         gelijk  => {
#                 'entiteittype' => 'NPS'
#             }
#         };


#     return $rv;
# }

1;


__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 acknowledge

TODO: Fix the POD

=cut

=head2 fout

TODO: Fix the POD

=cut

