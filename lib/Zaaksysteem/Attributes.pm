package Zaaksysteem::Attributes;
use warnings;
use strict;

use Zaaksysteem::Constants qw/
    ZAAKSYSTEEM_BETROKKENE_SUB
    ZAAKSYSTEEM_CONSTANTS
    ZAAKSYSTEEM_LEVERANCIER
    ZAAKSYSTEEM_LICENSE
    ZAAKSYSTEEM_NAAM
    ZAAKSYSTEEM_OPTIONS
    ZAAKSYSTEEM_STARTDATUM
/;

use Zaaksysteem::Object::Attribute;
use Zaaksysteem::Types::MappedString;

use Exporter 'import';
our @EXPORT     = qw/
    ZAAKSYSTEEM_SYSTEM_ATTRIBUTES
    ZAAKSYSTEEM_MAGIC_STRINGS
/;


=head2 _make_array

Convenience routine.

When given a scalar, returns a list with one item.
When given a list, derefs and returns the list.

=cut

sub _make_array {
    my $thing = shift;

    return () unless $thing;
    return ref $thing eq 'ARRAY' ? @$thing : ($thing);
}

=head2 check_bwcompat

Purpose is to add a bwcompat => 'bla' entry to
a config hash, if given backwards compatible names.

=cut

sub _check_bwcompat {
    my ($bwcompat, $prefix) = @_;

    return () unless defined $bwcompat;
    return (bwcompat_name => [
        map { $prefix . $_ } _make_array($bwcompat)
    ]);
}

=head2 _get_betrokkene_attribute

If config has a systeemkenmerk_ref key, run it, otherwise defer to
the predefined sub in Constants.pm

=cut

sub _get_betrokkene_attribute {
    my ($betrokkene, $config) = @_;

    return exists $config->{systeemkenmerk_reference} ?
        $config->{systeemkenmerk_reference}->($betrokkene) :
        ZAAKSYSTEEM_BETROKKENE_SUB->($betrokkene, $config->{internal_name});
}

BEGIN {
    my @person_fields = (
        {
            name          => 'subject_type',
            label         => 'Betrokkenetype',
            internal_name => 'btype',
        },
        {
            bwcompat      => 'aanhef',
            name          => 'salutation',
            label         => 'Aanhef',
            internal_name => 'aanhef',
        },
        {
            bwcompat      => 'aanhef1',
            label         => 'Aanhef 1',
            name          => 'salutation1',
            internal_name => 'aanhef1',
        },
        {
            bwcompat      => 'aanhef2',
            label         => 'Aanhef 2',
            name          => 'salutation2',
            internal_name => 'aanhef2',
        },
        {
            bwcompat      => 'afdeling',
            label         => 'Afdeling',
            name          => 'department',
            internal_name => 'afdeling',
        },
        {
            bwcompat      => 'achternaam',
            label         => 'Achternaam',
            name          => 'surname',
            internal_name => 'achternaam',
        },
        {
            bwcompat      => 'naamgebruik',
            label         => 'Naamgebruik',
            name          => 'naamgebruik', # pending a translation from Frontoffice department
            internal_name => 'naamgebruik'
        },
        {
            bwcompat      => 'burgerservicenummer',
            label         => 'Burgerservicenummer',
            name          => 'bsn',
            internal_name => 'burgerservicenummer',
        },
        {
            bwcompat      => 'email',
            name          => 'email',
            label         => 'E-mailadres',
            internal_name => 'email',
        },
        {
            bwcompat      => 'geboortedatum',
            label         => 'Geboortedatum',
            name          => 'date_of_birth',
            internal_name => 'geboortedatum',
            type          => 'date',
        },
        {
            bwcompat      => 'geboorteplaats',
            label         => 'Geboorteplaats',
            name          => 'place_of_birth',
            internal_name => 'geboorteplaats',
        },
        {
            bwcompat      => 'geboorteland',
            label         => 'Geboorteland',
            name          => 'country_of_birth',
            internal_name => 'geboorteland',
        },
        {
            bwcompat      => 'geslacht',
            label         => 'Geslacht',
            name          => 'gender',
            internal_name => 'geslacht',
        },
        {
            bwcompat      => 'geslachtsnaam',
            label         => 'Geslachtsnaam',
            name          => 'family_name',
            internal_name => 'geslachtsnaam',
        },
        {
            bwcompat      => 'handelsnaam',
            label         => 'Handelsnaam',
            name          => 'trade_name',
            internal_name => 'handelsnaam',
        },
        {
            bwcompat      => ['huisnummer', 'nummeraanduiding'],
            name          => 'house_number',
            label         => 'Huisnummer',
            internal_name => 'huisnummer',
        },
        {
            bwcompat      => 'huwelijksdatum',
            name          => 'date_of_marriage',
            internal_name => 'datum_huwelijk',
            label         => 'Huwelijksdatum',
            type          => 'date',
        },
        {
            bwcompat      => 'kvknummer',
            label         => 'KvK-nummer',
            name          => 'coc',
            internal_name => 'kvknummer',
        },
        {
            bwcompat      => 'mobiel',
            label         => 'Telefoonnummer (mobiel)',
            name          => 'mobile_number',
            internal_name => 'mobiel',
        },
        {
            bwcompat      => 'naam',
            label         => 'Naam',
            name          => 'name',
            internal_name => 'naam',
        },
        {
            bwcompat      => 'overlijdensdatum',
            label         => 'Overlijdensdatum',
            name          => 'date_of_death',
            internal_name => 'datum_overlijden',
            type          => 'date',
        },
        {
            bwcompat      => 'postcode',
            label         => 'Postcode',
            name          => 'zipcode',
            internal_name => 'postcode',
            systeemkenmerk_reference => sub {
                my $betrokkene = shift;

                my $value = ZAAKSYSTEEM_BETROKKENE_SUB->($betrokkene, 'postcode');

                # format with a space (1234 AB instead of 1234AB).
                # doing this in a lower level may impact
                # other processes
                $value =~ s/^(\d{4})(\w{2})$/$1 $2/ if $value;

                return $value;
            }
        },
        {
            bwcompat      => 'straat',
            label         => 'Straat',
            name          => 'street',
            internal_name => 'straat',
        },
        {
            bwcompat      => 'tel',
            label         => 'Telefoonnummer',
            name          => 'phone_number',
            internal_name => 'tel',
        },
        {
            bwcompat      => 'type',
            label         => 'Type',
            name          => 'type',
            internal_name => 'type',
        },
        {
            bwcompat      => 'volledigenaam',
            label         => 'Volledige naam',
            name          => 'full_name',
            internal_name => 'volledigenaam',
        },
        {
            bwcompat      => 'voornamen',
            label         => 'Voornamen',
            name          => 'first_names',
            internal_name => 'voornamen',
        },
        {
            bwcompat      => 'voorvoegsel',
            label         => 'Voorvoegsel',
            name          => 'surname_prefix',
            internal_name => 'voorvoegsel',
        },
        {
            bwcompat      => 'woonplaats',
            label         => 'Woonplaats',
            name          => 'place_of_residence',
            internal_name => 'woonplaats',
        },
        {
            bwcompat      => 'status',
            name          => 'status',
            label         => 'Status',
            systeemkenmerk_reference => sub {
                my $betrokkene = shift;

                # Actief, Overleden, Verhuisd, (opgeheven?)
                my $deceased = ZAAKSYSTEEM_BETROKKENE_SUB->($betrokkene, 'datum_overlijden');
                my $has_moved = ZAAKSYSTEEM_BETROKKENE_SUB->($betrokkene, 'is_verhuisd');

                # overleden takes precedence.
                return 'Overleden' if $deceased;
                return 'Verhuisd' if $has_moved;
                return 'Actief';
            },
        },
        {
            bwcompat      => 'in_onderzoek',
            label         => 'In onderzoek',
            name          => 'investigation',
            systeemkenmerk_reference => sub {
                my $betrokkene = shift;

                return ZAAKSYSTEEM_BETROKKENE_SUB->($betrokkene, 'in_onderzoek') ?
                    'In onderzoek' :
                    'Niet in onderzoek';
            },
        },
        {
            bwcompat      => 'datum_ontbinding_partnerschap',
            label         => 'Datum ontbinding partnerschap',
            name          => 'date_of_divorce',
            internal_name => 'datum_huwelijk_ontbinding',
            type          => 'date',
        },
        {
            bwcompat      => 'rechtsvorm',
            label         => 'Rechtsvorm',
            name          => 'type_of_business_entity',
            internal_name => 'rechtsvorm'
        },
        {
            bwcompat      => 'vestigingsnummer',
            label         => 'Vestigingsnummer',
            name          => 'establishment_number',
            internal_name => 'vestigingsnummer',
        },
    );

    sub _get_aanvrager_attributes {
        my @attributes;

        my @aanvrager_fields = (
            @person_fields,
            {
                bwcompat      => 'anummer',
                label         => 'A Nummer',
                name          => 'a_number',
                internal_name => 'a_nummer',
            },
            {
                bwcompat      => 'login',
                label         => 'Login',
                name          => 'login',
                internal_name => 'login',
            },
            {
                bwcompat      => 'password',
                label         => 'Wachtwoord',
                name          => 'password',
                internal_name => 'password',
            },
        );

        for my $aanvrager_field (@aanvrager_fields) {
            push @attributes, {
                name                     => "case.requestor.$aanvrager_field->{name}",
                attribute_type           => $aanvrager_field->{type} || 'text',
                label                    => sprintf("Aanvrager %s", $aanvrager_field->{ label }),
                is_systeemkenmerk        => 1,
                systeemkenmerk_reference => sub {
                    my $case = shift;

                    return _get_betrokkene_attribute(
                        $case->aanvrager_object,
                        $aanvrager_field
                    );
                },
                _check_bwcompat($aanvrager_field->{bwcompat}, 'aanvrager_')
            };
        }

        push @attributes, {
            name                     => 'case.requestor.id',
            label                    => 'Aanvragernummer',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                my $aanvrager = $zaak->aanvrager_object;

                # This happens when the aanvrager can't be found in LDAP
                return unless defined $aanvrager;
                return $aanvrager->betrokkene_identifier;
            },
        };

        return @attributes;
    }

    sub _get_ontvanger_attributes {
        my @attributes;

        for my $ontvanger_field (@person_fields) {
            push @attributes, {

                name                     => "case.recipient.$ontvanger_field->{name}",
                attribute_type           => $ontvanger_field->{type} || 'text',
                label                    => $ontvanger_field->{ label },
                is_systeemkenmerk        => 1,
                systeemkenmerk_reference => sub {
                    my $case = shift;

                    return _get_betrokkene_attribute(
                        $case->ontvanger_object,
                        $ontvanger_field
                    );
                },
            };
        }

        push @attributes, {
            name                     => 'case.recipient.display_name',
            label                    => 'Ontvanger',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $case = shift;

                return _get_betrokkene_attribute($case->ontvanger_object, {
                    internal_name => 'display_name'
                });
            }
        };

        push @attributes, {
            name                     => 'case.recipient.id',
            label                    => 'Ontvangernummer',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return unless $zaak->ontvanger_object;
                return $zaak->ontvanger_object->betrokkene_identifier;
            },
        };

        return @attributes;
    }

    sub _get_behandelaar_attributes {

        my @behandelaar_attributes = (
            {
                bwcompat_name => 'voornaam',
                label => 'Voornaam',
                name => 'first_names',
                internal_name => 'voornamen'
            },
            {
                bwcompat_name => 'initialen',
                label => 'Initialen',
                name => 'initials',
                internal_name => 'voorletters'
            },
            {
                bwcompat_name => 'achternaam',
                label => 'Achternaam',
                name => 'last_name',
                internal_name => 'geslachtsnaam'
            },
        );

        my @attributes;
        for my $field (@behandelaar_attributes) {
            push @attributes, {
                bwcompat_name            => 'behandelaar_' . $field->{bwcompat_name},
                name                     => 'case.assignee.' . $field->{name},
                label                    => sprintf('Behandelaar %s', $field->{ label }),
                attribute_type           => 'text',
                is_systeemkenmerk        => 1,
                systeemkenmerk_reference => sub {
                    my $zaak = shift;
                    my $method = $field->{internal_name};

                    return $zaak->behandelaar_object &&
                        $zaak->behandelaar_object->$method;
                },
            };
        }
        return @attributes;
    }

    sub _get_zaaktype_attributes {
        my @attributes;

        my @properties = (
            {
                internal_name => 'aanleiding',
                name          => 'case.casetype.motivation',
                label         => 'Aanleiding'
            },
            {
                internal_name => 'archiefclassicatiecode', # [sic] ?
                name          => 'case.casetype.archive_classification_code',
                label         => 'Archiefclassificatiecode',
            },
            {
                internal_name => 'bag',
                name          => 'case.casetype.registration_bag',
                label         => 'Basisregistraties Adressen en Gebouwen'
            },
            {
                internal_name => 'beroep_mogelijk',
                bwcompat      => 'bezwaar_en_beroep_mogelijk',
                name          => 'case.casetype.objection_and_appeal',
                label         => 'Begroep mogelijk'
            },
            {
                internal_name => 'doel',
                bwcompat      => 'doel',
                name          => 'case.casetype.goal',
                label         => 'Doel'
            },
            {
                internal_name => 'e_formulier',
                name          => 'case.casetype.eform',
                label         => 'E-Formulier'
            },
            {
                internal_name => 'lex_silencio_positivo',
                name          => 'case.casetype.lex_silencio_positivo',
                label         => 'Lex Silencio Positivo'
            },
            {
                internal_name => 'lokale_grondslag',
                name          => 'case.casetype.principle_national',
                label         => 'Lokale grondslag'
            },
            {
                internal_name => 'opschorten_mogelijk',
                name          => 'case.casetype.suspension',
                label         => 'Opschorten mogelijk'
            },
            {
                internal_name => 'publicatie',
                name          => 'case.casetype.publication',
                label         => 'Publicatie'
            },
            {
                internal_name => 'publicatietekst',
                name          => 'case.casetype.text_for_publication',
                label         => 'Publicatietekst'
            },
            {
                internal_name => 'verantwoordelijke',
                name          => 'case.casetype.supervisor',
                label         => 'Verantwoordelijke'
            },
            {
                internal_name => 'verantwoordingsrelatie',
                name          => 'case.casetype.supervisor_relation',
                label         => 'Verantwoordingsrelatie'
            },
            {
                internal_name => 'verlenging_mogelijk',
                name          => 'case.casetype.extension',
                label         => 'Verlenging mogelijk'
            },
            {
                internal_name => 'verlengingstermijn',
                name          => 'case.casetype.extension_period',
                label         => 'Verlengingstermijn'
            },
            {
                internal_name => 'verdagingstermijn',
                name          => 'case.casetype.adjourn_period',
                label         => 'Verdagingstermijn'
            },
            {
                internal_name => 'vertrouwelijkheidsaanduiding',
                name          => 'case.casetype.designation_of_confidentiality',
                label         => 'Vertrouwelijkheidsaanduiding'
            },
            {
                internal_name => 'wet_dwangsom',
                name          => 'case.casetype.penalty',
                label         => 'Wet dwangsom'
            },
            {
                internal_name => 'wkpb',
                name          => 'case.casetype.wkpb',
                label         => 'WKPB'
            },
        );

        for my $property (@properties) {
            push @attributes, {
                bwcompat_name            => $property->{ bwcompat } // $property->{ internal_name },
                label                    => $property->{ label },
                name                     => $property->{ name },
                attribute_type           => 'text',
                is_systeemkenmerk        => 1,
                systeemkenmerk_reference => sub {
                    my $zaak = shift;
                    return $zaak->zaaktype_property($property->{internal_name});
                }
            };
        }

        return @attributes;
    }

    sub _get_location_attributes {
        my @attributes;

        my %location_types = (
            locatie_zaak            => 'case_location',
            locatie_correspondentie => 'correspondence_location',
        );
        for my $location_type (keys %location_types) {
            for my $bag_type (qw(verblijfsobject openbareruimte nummeraanduiding pand standplaats ligplaats)) {
                my $bag_type_id = "${bag_type}_id";

                my $prefix = $location_type eq 'locatie_zaak' ? 'Zaaklocatie' : 'Correspondentielocatie';

                push @attributes, {
                    name                     => "case.$location_types{$location_type}.${bag_type}",
                    attribute_type           => 'bag',
                    label                    => sprintf('%s %s', $prefix, ucfirst($bag_type)),
                    is_systeemkenmerk        => 1,
                    systeemkenmerk_reference => sub {
                        my $zaak = shift;
                        return unless $zaak->$location_type;
                        return unless $zaak->$location_type->$bag_type_id;

                        my $rs = $zaak->result_source->schema->resultset('BagNummeraanduiding');
                        my $human_identifier = eval {
                            $rs->get_human_identifier_by_source_identifier(
                                $zaak->$location_type->$bag_type_id,
                                {prefix_with_city => 1}
                            ),
                        };
                        my $address_data = eval {
                            $rs->get_address_data_by_source_identifier(
                                $zaak->$location_type->$bag_type_id
                            ),
                        };

                        return {
                            bag_id => $zaak->$location_type->$bag_type_id,
                            human_identifier => $human_identifier,
                            address_data => $address_data,
                        };
                    },
                };
            }
        }

        return @attributes;
    }
}



=head2 ZAAKSYSTEEM_STANDARD_COLUMNS

Legacy fields, not safe to remove because of possible use in user
templates.

=cut

sub create_system_attribute {
    my ($bwcompat_name, $name, $attribute_type, $ref) = @_;

    return {
        bwcompat_name            => $bwcompat_name,
        name                     => $name,
        label                    => ucfirst($bwcompat_name),
        attribute_type           => $attribute_type,
        is_systeemkenmerk        => 1,
        systeemkenmerk_reference => $ref
    };
}

use constant ZAAKSYSTEEM_STANDARD_COLUMNS => (
    # ...what the...
    map { create_system_attribute(@{$_}) } (

        [qw/status status text/, sub { shift->status }],

        [qw/aanvrager requestor text/, sub { shift->aanvrager->naam }],
    )
);

sub _get_casetype_prices {
    my %casetype_price_fields = (
        tarief_balie       => 'case.casetype.price.counter',
        tarief_telefoon    => 'case.casetype.price.telephone',
        tarief_email       => 'case.casetype.price.email',
        tarief_behandelaar => 'case.casetype.price.employee',
        tarief_post        => 'case.casetype.price.post'
    );

    return map {
        my $key = $_;

        {
            bwcompat_name            => $key,
            name                     => $casetype_price_fields{$key},
            label                    => sprintf('Tarief (%s)', (split m[_], $key)[1]),
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                return shift->zaaktype_property('pdc_' . $key);
        }
    }}   keys %casetype_price_fields;
}

sub _get_document_attributes {
    return ({
        bwcompat_name            => 'zaak_dossierdocumenten',
        name                     => 'case.case_documents',
        label                    => 'Zaakdossierdocumenten',
        attribute_type           => 'text',
        is_systeemkenmerk        => 1,
        systeemkenmerk_reference => sub {
            my $zaak = shift;

            my @case_documents = $zaak->files->search({
                'me.date_deleted' => undef,
                'case_documents.case_document_id' => { -not => undef },
            }, {
                join => { case_documents => 'file_id' },
                order_by => 'me.name'
            })->active_files;

            return join ", ", map { sprintf "%s (%s)", $_->filename, $_->document_status } @case_documents;
        }
    },
    {
        bwcompat_name            => 'zaak_documenten',
        label                    => 'Zaakdocumenten',
        name                     => 'case.documents',
        attribute_type           => 'text',
        is_systeemkenmerk        => 1,
        systeemkenmerk_reference => sub {
            my $zaak = shift;

            return join ", ", sort map { sprintf "%s (%s)", $_->filename, $_->document_status }
                grep { !$_->date_deleted } $zaak->active_files;
        }
    });
}

=head2 DYNAMIC_ATTRIBUTES

These are calculated on runtime because of time dependency.

=cut

use constant DYNAMIC_ATTRIBUTES => (
    {
        bwcompat_name  => ['dagen_resterend', 'dagen', 'days_left'],
        label          => 'Dagen resterend',
        name           => 'case.days_left',
        attribute_type => 'integer',
        dynamic_class  => 'DaysLeft',
        object_table   => '',
    },
    {
        bwcompat_name  => ['voortgang', 'days_perc'],
        label          => 'Voortgang',
        name           => 'case.progress_days',
        attribute_type => 'integer',
        dynamic_class  => 'DaysPercentage',
        object_table   => '',
    },
    {
        name           => 'case.destructable',
        label          => 'Vernietigbaar',
        attribute_type => 'integer',
        dynamic_class  => 'CaseDestructable',
        object_table   => '',
    },
    {
        name           => 'case.destruction_blocked',
        label          => 'Vernietiging geblokkeerd',
        attribute_type => 'integer',
        dynamic_class  => 'CaseDestructionBlocked',
        object_table   => '',
    },
    {
        name           => 'case.lead_time_real',
        attribute_type => 'text',
        dynamic_class  => 'DaysLeadTime',
        object_table   => '',
        bwcompat_name  => 'zaak_doorlooptijd',
        label          => 'Zaakdoorlooptijd'
    },
);


use constant ZAAKSYSTEEM_SYSTEM_ATTRIBUTES => (
        {
            bwcompat_name            => 'zaaknummer',
            label                    => 'Zaaknummer',
            name                     => 'case.number',
            attribute_type           => 'integer',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->id;
            }
        },
        {
            bwcompat_name            => 'startdatum',
            label                    => 'Startdatum',
            name                     => 'case.startdate',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->registratiedatum->dmy
                    if $zaak->registratiedatum;
            }
        },
        {
            bwcompat_name            => 'zaaktype',
            label                    => 'Zaaktype',
            name                     => 'case.casetype.name',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->zaaktype_node_id->titel;
            }
        },
        {
            bwcompat_name            => 'zaak_fase',
            label                    => 'Zaak fase',
            name                     => 'case.phase',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;

                if ($zaak->volgende_fase) {
                    return $zaak->volgende_fase->fase;
                }
                return;
            }
        },
        {
            bwcompat_name            => 'zaak_mijlpaal',
            label                    => 'Zaak mijlpaal',
            name                     => 'case.milestone',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;

                if ($zaak->volgende_fase) {
                    return $zaak->huidige_fase->naam;
                }
                return;
            }
        },
        {
            bwcompat_name            => 'uname',
            label                    => 'Systeeminformatie',
            name                     => 'system.uname',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;

                my $zaaksysteem_location    = $INC{'Zaaksysteem.pm'};

                return 'Zaaksysteem (unknown)' unless($zaaksysteem_location);

                my @file_information        = stat($zaaksysteem_location);

                my @uname   = (
                    ZAAKSYSTEEM_NAAM,
                    Zaaksysteem->config->{'ZS_VERSION'},
                    ZAAKSYSTEEM_STARTDATUM,
                    ZAAKSYSTEEM_LEVERANCIER,
                    ZAAKSYSTEEM_LICENSE,
                    'zaaksysteem.nl',
                );

                return join(', ', @uname);
            },
        },
        {
            bwcompat_name            => 'datum',
            label                    => 'Datum',
            name                     => 'system.current_date',
            attribute_type           => 'date',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                return DateTime->now();
            },
        },

        # This attribute is mainly for internal use, the end-user will not be
        # confronted with it, if all goes well. Context is that I needed to
        # use a filter that defined the actual text, instead of an attribute
        # So this unit always returns the empty string.
        {
            name                     => 'null',
            label                    => 'Lege waarde',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub { return '' }
        },
        {
            bwcompat_name            => 'zaaktype_versie',
            label                    => 'Zaaktypeversie',
            name                     => 'case.casetype.version',
            attribute_type           => 'integer',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->zaaktype_node_id->version;
            }
        },
        {
            bwcompat_name            => 'zaaktype_versie_begindatum',
            label                    => 'Zaaktypeversie begindatum',
            name                     => 'case.casetype.version_date_of_creation',
            attribute_type           => 'date',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->zaaktype_node_id->created;
            }
        },
        {
            bwcompat_name            => 'zaaktype_versie_einddatum',
            label                    => 'Zaaktypeversie einddatum',
            name                     => 'case.casetype.version_date_of_expiration',
            attribute_type           => 'date',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->zaaktype_node_id->deleted;
            }
        },
        {
            bwcompat_name            => 'registratiedatum',
            label                    => 'Registratiedatum',
            name                     => 'case.date_of_registration',
            attribute_type           => 'date',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->registratiedatum;
            }
        },
        {
            bwcompat_name            => 'registratiedatum_volledig',
            label                    => 'Registratiedatum (volledig)',
            name                     => 'case.date_of_registration_full',
            attribute_type           => 'date',
            is_systeemkenmerk        => 1,
            format                   => '%d-%m-%Y %H:%M:%S',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->registratiedatum;
            }
        },
        {
            bwcompat_name            => 'afhandeldatum',
            label                    => 'Afhandeldatum',
            name                     => 'case.date_of_completion',
            attribute_type           => 'date',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->afhandeldatum;
            }
        },
        {
            bwcompat_name            => 'afhandeldatum_volledig',
            label                    => 'Afhandeldatum (volledig)',
            name                     => 'case.date_of_completion_full',
            attribute_type           => 'date',
            is_systeemkenmerk        => 1,
            format                   => '%d-%m-%Y %H:%M:%S',
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->afhandeldatum;
            },
        },
        {
            bwcompat_name            => 'afdeling',
            label                    => 'Afdeling',
            name                     => 'case.casetype.department',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;

                my $ou_object = $zaak->ou_object;
                return $ou_object->name if $ou_object;
            }
        },
        {
            bwcompat_name            => 'uiterste_vernietigingsdatum',
            label                    => 'Uiterste vernietigingsdatum',
            name                     => 'case.date_destruction',
            attribute_type           => 'date',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->vernietigingsdatum;
            },
        },
        {
            bwcompat_name            => 'alle_relaties',
            label                    => 'Alle relaties',
            name                     => 'case.relations',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;

                my @relaties = ();
                if($zaak->get_column('pid')) {
                    push @relaties, $zaak->get_column('pid');
                }
                if ($zaak->zaak_children->count) {
                    my $children = $zaak->zaak_children->search;
                    while (my $child = $children->next) {
                        push @relaties, $child->id;
                    }
                }
                if($zaak->get_column('vervolg_van')) {
                    push @relaties, $zaak->get_column('vervolg_van');
                }
                if($zaak->get_column('relates_to')) {
                    push @relaties, $zaak->get_column('relates_to');
                }

                push @relaties, map { $_->case_id }
                    $zaak->result_source->schema->resultset('CaseRelation')->get_sorted($zaak->id);

                return join ", ", @relaties;
            },
        },
        {
            bwcompat_name            => 'zaak_relaties',
            label                    => 'Zaakrelaties',
            name                     => 'case.related_cases',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return join ", ", map { $_->case->id } $zaak->zaak_relaties;
            },
        },
        {
            bwcompat_name            => 'archiefnominatie',
            label                    => 'Archiefnominatie',
            name                     => 'case.type_of_archiving',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                if(my $zaaktype_resultaat = $zaak->zaaktype_resultaat) {
                    return $zaaktype_resultaat->archiefnominatie;
                }
            },
        },
        {
            bwcompat_name            => ['zaak_bedrag', 'bedrag_web', 'pdc_tarief'],
            label                    => 'Zaakbedrag',
            name                     => 'case.price',
            attribute_type           => 'text', # format_payment_amount will return '-' when no value present
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->format_payment_amount();
            },
        },
        {
            bwcompat_name            => 'betaalstatus',
            label                    => 'Betaalstatus',
            name                     => 'case.payment_status',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                my $ps = $zaak->payment_status;
                return unless $ps;

                return Zaaksysteem::Types::MappedString->new(
                    original => $ps,
                    mapped   => ZAAKSYSTEEM_CONSTANTS->{payment_statuses}->{$ps} || $ps,
                );
            },
        },
        {
            name                     => 'case.status',
            bwcompat_name            => 'status',
            label                    => 'Status',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->status();
            },
        },
        {
            bwcompat_name            => 'statusnummer',
            label                    => 'Statusnummer',
            name                     => 'case.number_status',
            attribute_type           => 'integer',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->milestone();
            },
        },
        {
            bwcompat_name            => 'streefafhandeldatum',
            label                    => 'Streefafhandeldatum',
            name                     => 'case.date_target',
            attribute_type           => 'timestamp_or_text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;

                # XXX 2 returntypes (timestamp en string)
                if ($zaak->status eq 'stalled') {
                    return 'Opgeschort';
                }
                return $zaak->streefafhandeldatum;
            },
        },
        {
            bwcompat_name            => 'bewaartermijn',
            label                    => 'Bewaartermijn',
            name                     => 'case.period_of_preservation',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;

                if(my $zaaktype_resultaat = $zaak->zaaktype_resultaat) {
                    my $bewaartermijn = $zaaktype_resultaat->bewaartermijn;
                    return ZAAKSYSTEEM_OPTIONS->{BEWAARTERMIJN}->{$bewaartermijn};
                }
            },
        },
        {
            bwcompat_name            => 'contactkanaal',
            label                    => 'Contactkanaal',
            name                     => 'case.channel_of_contact',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->contactkanaal;
            },
        },
        {
            bwcompat_name            => 'pid',
            label                    => 'PID',
            name                     => 'case.number_parent',
            attribute_type           => 'integer',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->get_column('pid');
            },
        },
        {
            bwcompat_name            => 'vervolg_van',
            label                    => 'Vervolg van', # oorsprongzaak?
            name                     => 'case.number_previous',
            attribute_type           => 'integer',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->get_column('vervolg_van');
            },
        },
        {
            bwcompat_name            => 'voortgang_status',
            label                    => 'Voortgang status',
            name                     => 'case.progress_status',
            attribute_type           => 'integer',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->status_perc;
            },
        },
        {
            bwcompat_name            => 'zaaknummer_hoofdzaak',
            label                    => 'Zaaknummer hoofdzaak',
            name                     => 'case.number_master',
            attribute_type           => 'integer',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->id unless $zaak->pid;

                my $parent = $zaak->pid;
                while ($parent->pid) {
                    $parent = $parent->pid;
                }

                return $parent->id;
            },
        },
        {
            bwcompat_name            => 'relates_to', # doesn't sound bw-compat-ish
            label                    => 'Gerelateerd aan',
            name                     => 'case.number_relations',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->get_column('relates_to');
            },
        },
        {
            bwcompat_name            => 'resultaat',
            label                    => 'Resultaat',
            name                     => 'case.result',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->resultaat;
            },
        },
        {
            bwcompat_name            => 'resultaat_omschrijving',
            label                    => 'Resultaatomschrijving',
            name                     => 'case.result_description',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                if(my $zaaktype_resultaat = $zaak->zaaktype_resultaat) {
                    return $zaaktype_resultaat->label;
                }
            },
        },
        {
            bwcompat_name            => 'resultaat_toelichting',
            label                    => 'Resultaattoelichting',
            name                     => 'case.result_explanation',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                if(my $zaaktype_resultaat = $zaak->zaaktype_resultaat) {
                    return $zaaktype_resultaat->comments;
                }
            },
        },
        {
            bwcompat_name            => 'deelzaken_afgehandeld',
            label                    => 'Deelzaken afgehandeld',
            name                     => 'case.relations_complete',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;

                return $zaak->zaak_children->search({
                    'me.status' => { -not_in => ['resolved'] },
                })->count ? 'Nee' : 'Ja';
            },
        },
        {
            bwcompat_name            => 'doorlooptijd_wettelijk',
            label                    => 'Doorlooptijd (wettelijk)',
            name                     => 'case.lead_time_legal',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->zaaktype_node_id->zaaktype_definitie_id->servicenorm;
            },
        },
        {
            bwcompat_name            => 'doorlooptijd_service',
            label                    => 'Doorlooptijd (service)',
            name                     => 'case.lead_time_service',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->zaaktype_node_id->zaaktype_definitie_id->afhandeltermijn;
            },
        },

        {
            bwcompat_name            => 'trefwoorden',
            label                    => 'Trefwoorden',
            name                     => 'case.casetype.keywords',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->zaaktype_node_id->zaaktype_trefwoorden;
            },
        },
        {
            bwcompat_name            => 'trigger',
            label                    => 'Trigger', # onduidelijk
            name                     => 'case.casetype.initiator_source',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->zaaktype_node_id->trigger;
            },
        },

        {
            bwcompat_name            => 'gebruiker_naam',
            label                    => 'Gebruiker naam',
            name                     => 'user.name',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                if ($zaak->result_source->resultset->current_user) {
                    return $zaak->result_source->resultset->current_user->naam;
                }
                return;
            },
        },

        {
            bwcompat_name            => 'generieke_categorie',
            label                    => 'Categorie (generiek)',
            name                     => 'case.casetype.generic_category',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;

                return $zaak->zaaktype_id->bibliotheek_categorie_id->naam
                    if $zaak->zaaktype_id->bibliotheek_categorie_id;
            },
        },
        {
            bwcompat_name            => 'handelingsinitiator',
            label                    => 'Handelingsinitiator',
            name                     => 'case.casetype.initiator_type',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->zaaktype_node_id->zaaktype_definitie_id->handelingsinitiator;
            },
        },
        {
            bwcompat_name            => 'zaaktype_id',
            label                    => 'Zaaktypenummer',
            name                     => 'case.casetype.id',
            attribute_type           => 'integer',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->zaaktype_id->id;
            },
        },
        {
            name                     => 'case.casetype.node.id',
            attribute_type           => 'integer',
            label                    => 'Zaaktypenodenummer',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->zaaktype_id->zaaktype_node_id->id;
            },
        },
        {
            bwcompat_name            => 'identificatie',
            label                    => 'Identificatie',
            name                     => 'case.casetype.identification',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->zaaktype_node_id->code;
            },
        },
        {
            bwcompat_name            => 'omschrijving_of_toelichting',
            label                    => 'Omschrijving of toelichting',
            name                     => 'case.casetype.description',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->zaaktype_node_id->zaaktype_omschrijving;
            },
        },
        {
            bwcompat_name            => 'openbaarheid',
            label                    => 'Openbaarheid',
            name                     => 'case.casetype.publicity',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->zaaktype_node_id->zaaktype_definitie_id->openbaarheid;
            },
        },
        {
            bwcompat_name            => 'published',
            label                    => 'Gepubliceerd',
            name                     => 'case.published',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->published;
            },
        },

        {
            bwcompat_name            => 'opgeschort_tot',
            label                    => 'Opgeschort tot',
            name                     => 'case.stalled_until',
            attribute_type           => 'date',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->stalled_until;
            },
        },
        {
            bwcompat_name            => 'procesbeschrijving',
            label                    => 'Procesbeschrijving',
            name                     => 'case.casetype.process_description',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->zaaktype_node_id->zaaktype_definitie_id->procesbeschrijving;
            },
        },
        {
            bwcompat_name            => 'selectielijst',
            label                    => 'Selectielijst',
            name                     => 'case.selection_list',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->zaaktype_node_id->zaaktype_definitie_id->selectielijst;
            },
        },
        {
            bwcompat_name            => 'wettelijke_grondslag',
            label                    => 'Wettelijke grondslag',
            name                     => 'case.principle_local',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->zaaktype_node_id->zaaktype_definitie_id->grondslag;
            },
        },
        {
            bwcompat_name            => 'sjabloon_aanmaakdatum',
            label                    => 'Aanmaakdatum sjabloon',
            name                     => 'case.date_current',
            attribute_type           => 'date',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                return DateTime->now();
            },
        },
        {
            bwcompat_name            => 'vertrouwelijkheid',
            label                    => 'Vertrouwelijkheid',
            name                     => 'case.confidentiality',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                my $setting = $zaak->confidentiality;

                return Zaaksysteem::Types::MappedString->new(
                    original => $setting,
                    mapped   => ZAAKSYSTEEM_CONSTANTS->{confidentiality}->{$setting} || $setting,
                );
            },
        },

        # "Behandelaar"
        {
            bwcompat_name            => 'behandelaar',
            label                    => 'Behandelaar',
            name                     => 'case.assignee',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return unless $zaak->behandelaar_object;
                return $zaak->behandelaar_object->naam;
            },
        },
        {
            bwcompat_name            => 'behandelaar_afdeling',
            label                    => 'Behandelaar afdeling',
            name                     => 'case.assignee.department',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return unless $zaak->behandelaar_object;

                return $zaak->behandelaar_object->org_eenheid->name
                    if (
                        $zaak->behandelaar_object->btype eq 'medewerker' &&
                        $zaak->behandelaar_object->org_eenheid
                    );

                return '';
            },
        },
        {
            bwcompat_name            => 'behandelaar_email',
            label                    => 'Behandelaar e-mail',
            name                     => 'case.assignee.email',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return unless $zaak->behandelaar_object;
                return $zaak->behandelaar_object->email;
            },
        },
        {
            bwcompat_name            => 'behandelaar_tel',
            label                    => 'Behandelaar telefoonnummer',
            name                     => 'case.assignee.phone_number',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return unless $zaak->behandelaar_object;
                return $zaak->behandelaar_object->telefoonnummer;
            },
        },
        {
            bwcompat_name            => 'behandelaar_id',
            label                    => 'Behandelaarnummer',
            name                     => 'case.assignee.id',
            attribute_type           => 'integer',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return unless $zaak->behandelaar_object;
                return $zaak->behandelaar_object->ex_id;
            },
        },
        {
            bwcompat_name            => 'behandelaar_handtekening',
            label                    => 'Behandelaar handtekening',
            name                     => 'case.assignee.signature',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my  $zaak   = shift;
                return unless $zaak->behandelaar_object;

                my $schema  = $zaak->result_source->schema;
                my $rs      = $schema->resultset('Subject');
                my $subject = $rs->find($zaak->behandelaar_object->id);

                my $current_id = $subject->settings->{signature_filestore_id};

                return '' unless $current_id;

                my $file = $schema->resultset('Filestore')->find($current_id);
                return $file->get_path;
            },
        },

        # "Coordinator"
        {
            bwcompat_name            => 'coordinator',
            label                    => 'Coordinator',
            name                     => 'case.coordinator',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return unless $zaak->coordinator_object;
                return $zaak->coordinator_object->naam;
            },
        },
        {
            bwcompat_name            => 'coordinator_email',
            label                    => 'Coordinator e-mail',
            name                     => 'case.coordinator.email',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return unless $zaak->coordinator_object;
                return $zaak->coordinator_object->email;
            },
        },
        {
            bwcompat_name            => 'coordinator_tel',
            label                    => 'Coordinator telefoonummer',
            name                     => 'case.coordinator.phone_number',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return unless $zaak->coordinator_object;
                return $zaak->coordinator_object->telefoonnummer;
            },
        },
        {
            bwcompat_name            => 'coordinator_id',
            label                    => 'Coordinatornummer',
            name                     => 'case.coordinator.id',
            attribute_type           => 'integer',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return unless $zaak->coordinator_object;
                return $zaak->coordinator_object->ex_id;
            },
        },

        # Internal
        {
            name                     => 'case.route_ou',
            label                    => 'Intaketoewijzing (groep)',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->route_ou;
            },
        },
        {
            name                     => 'case.route_role',
            label                    => 'Intaketoewijzing (rol)',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->route_role;
            },
        },

        {
            name                     => 'case.num_unaccepted_updates',
            label                    => 'Ongeaccepteerde PIP updates',
            attribute_type           => 'integer',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->unaccepted_pip_updates;
            },
        },
        {
            name                     => 'case.num_unaccepted_files',
            label                    => 'Ongeaccepteerde bestanden',
            attribute_type           => 'integer',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;

                # Force integerness
                return 0 + $zaak->files->search->unaccepted;
            },
        },
        {
            name                     => 'case.subject',
            bwcompat_name            => 'zaak_onderwerp',
            label                    => 'Zaakonderwerp',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                my $zaak = shift;
                return $zaak->onderwerp;
            },
        },
        {
            name                     => 'case.subject_external',
            bwcompat_name            => 'zaak_onderwerp_extern',
            label                    => 'Zaakonderwerp Extern',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                return shift->onderwerp_extern;
            }
        },

        _get_aanvrager_attributes(),
        _get_ontvanger_attributes(),
        _get_zaaktype_attributes(),
        _get_location_attributes(),
        _get_behandelaar_attributes,

        {
            name                     => 'case.casetype.price.web',
            bwcompat_name            => 'tarief_web',
            label                    => 'Tarief (web)',
            attribute_type           => 'text',
            is_systeemkenmerk        => 1,
            systeemkenmerk_reference => sub {
                return shift->zaaktype_node_id->zaaktype_definitie_id->pdc_tarief;
            },
        },

        _get_casetype_prices,
        _get_document_attributes,
        ZAAKSYSTEEM_STANDARD_COLUMNS,
        DYNAMIC_ATTRIBUTES
);



=head2 get_bwcompat_name

accessor function that always returns a list (or scalar that will shove
snugly into a list if offered)

my @aliases = $attribute->get_bwcompat_name;

=cut

sub _get_bwcompat_name {
    my $bwcompat_name = shift;

    return unless $bwcompat_name;
    return ref $bwcompat_name ? @$bwcompat_name : $bwcompat_name;
}

=head2 ZAAKSYSTEEM_MAGIC_STRINGS

This is the predefined list with attributes that are defined by Zaaksysteem,
like reserved keywords. These are not available for custom use, but always
available.

=cut

use constant ZAAKSYSTEEM_MAGIC_STRINGS => (
    map { _get_bwcompat_name($_->{bwcompat_name}) } ZAAKSYSTEEM_SYSTEM_ATTRIBUTES
);


=head2

Creates instances of all predefined case attributes.

=cut

sub predefined_case_attributes {
    my $self = shift;

    return map {
        Zaaksysteem::Object::Attribute->new(%$_)
    } ZAAKSYSTEEM_SYSTEM_ATTRIBUTES
}


1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 create_system_attribute

TODO: Fix the POD

=cut

=head2 predefined_case_attributes

TODO: Fix the POD

=cut

