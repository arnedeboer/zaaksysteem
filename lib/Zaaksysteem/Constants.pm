package Zaaksysteem::Constants;

use strict;
use warnings;

use DateTime;
use DateTime::Format::Strptime qw(strptime);
use Scalar::Util qw/blessed/;
use Data::Dumper;

use utf8;

require Exporter;
our @ISA        = qw/Exporter/;
our @EXPORT     = qw/
    ZAAKSYSTEEM_CONSTANTS
    ZAAKSYSTEEM_NAMING
    ZAAKSYSTEEM_OPTIONS
    ZAAKTYPE_DB_MAP

    ZAAKTYPE_PREFIX_SPEC_KENMERK
    ZAAKTYPE_KENMERKEN_ZTC_DEFINITIE
    ZAAKTYPE_KENMERKEN_DYN_DEFINITIE

    GEGEVENSMAGAZIJN_GBA_PROFILE
    GEGEVENSMAGAZIJN_GBA_ADRES_EXCLUDES

    GEGEVENSMAGAZIJN_KVK_PROFILE
    GEGEVENSMAGAZIJN_KVK_RECHTSVORMCODES

    ZAAKSYSTEEM_AUTHORIZATION_ROLES
    ZAAKSYSTEEM_AUTHORIZATION_PERMISSIONS

    SJABLONEN_EXPORT_FORMATS
    SJABLONEN_TARGET_FORMATS

    ZAAKSYSTEEM_GM_AUTHENTICATEDBY_DIGID
    ZAAKSYSTEEM_GM_AUTHENTICATEDBY_BEDRIJFID
    ZAAKSYSTEEM_GM_AUTHENTICATEDBY_GBA
    ZAAKSYSTEEM_GM_AUTHENTICATEDBY_KVK
    ZAAKSYSTEEM_GM_AUTHENTICATEDBY_BEHANDELAAR

    ZAAKSYSTEEM_ZAAK_URGENTIE_PERC_HIGH
    ZAAKSYSTEEM_ZAAK_URGENTIE_PERC_MEDIUM
    ZAAKSYSTEEM_ZAAK_URGENTIE_PERC_LATE

    ZAAKSYSTEEM_BETROKKENE_KENMERK
    ZAAKSYSTEEM_BETROKKENE_SUB

    ZAAKSYSTEEM_LOGGING_LEVELS
    LOGGING_COMPONENT_ZAAK
    LOGGING_COMPONENT_USER
    LOGGING_COMPONENT_ZAAKTYPE
    LOGGING_COMPONENT_CATEGORIE
    LOGGING_COMPONENT_NOTITIE
    LOGGING_COMPONENT_BETROKKENE
    LOGGING_COMPONENT_KENMERK
    LOGGING_COMPONENT_SJABLOON
    LOGGING_COMPONENT_NOTIFICATIE
    LOGGING_COMPONENT_DOCUMENT

    LDAP_DIV_MEDEWERKER

    STUF_VERSIONS
    STUF_XSD_PATH
    STUF_XML_URL

    DEFAULT_KENMERKEN_GROUP_DATA

    ZAKEN_STATUSSEN
    ZAKEN_STATUSSEN_DEFAULT

    SEARCH_QUERY_SESSION_VAR
    SEARCH_QUERY_TABLE_NAME

    ZAAKSYSTEEM_CONTACTKANAAL_WEBFORM
    ZAAKSYSTEEM_CONTACTKANAAL_BALIE

    VALIDATION_CONTACT_DATA
    VALIDATION_EXTERNAL_CONTACT_DATA

    ZAAK_CREATE_PROFILE
    ZAAKTYPE_DEPENDENCY_IDS
    ZAAKTYPE_DEPENDENCIES

    BETROKKENE_RELATEREN_PROFILE
    BETROKKENE_RELATEREN_MAGIC_STRING_SUGGESTION

    VERNIETIGINGS_REDENEN
    ZAAK_WIJZIG_VERNIETIGINGSDATUM_PROFILE
    ZAAK_WIJZIG_STREEFAFHANDELDATUM_PROFILE
    ZAAK_WIJZIG_REGISTRATIEDATUM_PROFILE

    PARAMS_PROFILE_MESSAGES_SUB

    ZAAKSYSTEEM_NAAM
    ZAAKSYSTEEM_OMSCHRIJVING
    ZAAKSYSTEEM_LEVERANCIER
    ZAAKSYSTEEM_STARTDATUM
    ZAAKSYSTEEM_LICENSE
    PARAMS_PROFILE_DEFAULT_MSGS

    DOCUMENTS_STORE_TYPE_NOTITIE
    DOCUMENTS_STORE_TYPE_FILE
    DOCUMENTS_STORE_TYPE_MAIL
    DOCUMENTS_STORE_TYPE_JOB

    FILE_STORE_LOCATION

    BETROKKENE_TYPE_BEDRIJF
    BETROKKENE_TYPE_NATUURLIJK_PERSOON
    BETROKKENE_TYPE_MEDEWERKER

    OBJECTSEARCH_TABLENAMES
    OBJECTSEARCH_TABLE_ORDER

    OBJECT_ACTIONS

    EDE_PUBLICATION_STRINGS

    STATUS_LABELS
    MIMETYPES_ALLOWED

    ALLOW_NO_HTML
    ALLOW_ONLY_TRUSTED_HTML

    JOBS_INFORMATION_MAP

    STRONG_RELATED_COLUMNS
    ZAAK_EMPTY_COLUMNS

    EVENT_TYPE_GROUPS

    DENIED_BROWSERS
    FRIENDLY_BETROKKENE_MESSAGES

    CASE_PAYMENT_STATUS_FAILED
    CASE_PAYMENT_STATUS_SUCCESS
    CASE_PAYMENT_STATUS_PENDING

    SUBJECT_TYPE_EMPLOYEE

    CHILD_CASETYPE_OPTIONS

    SAML_TYPE_LOGIUS
    SAML_TYPE_KPN_LO
    SAML_TYPE_ADFS
    SAML_TYPE_SPOOF
    SAML_TYPE_MINIMAL
/;

our @EXPORT_OK = qw(
    BAG_TYPES
    PROFILE_BAG_TYPES_OK

    SERVICE_NORM_TYPES
    SERVICE_NORM_TYPES_OK

    ZAAKTYPE_ATTRIBUTEN

    ZAAKSYSTEEM_BOFH

    ZAAKTYPE_TRIGGER
    ZAAK_CREATE_PROFILE_BETROKKENE

    VALID_FQDN
    ZS_PROFILE_CREATE_DOMAIN
    OBJECT_CLASS_DOMAIN

    ZAAK_CONFIDENTIALITY
    CASE_PAYMENT_STATUS_FAILED
    CASE_PAYMENT_STATUS_SUCCESS
    CASE_PAYMENT_STATUS_PENDING
    ZAAKSYSTEEM_CONSTANTS

    RGBZ_LANDCODES
    RGBZ_GEMEENTECODES
    VALIDATION_RELATEREN_PROFILE
    BETROKKENE_RELATEREN_PROFILE

    STUF_SUBSCRIPTION_VIEWS

    DOCUMENT_STATUS
    PUBLIC_CASE_FINISH_MESSAGE
);

### DO NOT FREAKING TOUCH ;)
### {
use constant ZAAKSYSTEEM_GM_AUTHENTICATEDBY_DIGID       => 'digid';
use constant ZAAKSYSTEEM_GM_AUTHENTICATEDBY_BEDRIJFID   => 'bedrijfid';
use constant ZAAKSYSTEEM_GM_AUTHENTICATEDBY_GBA         => 'gba';
use constant ZAAKSYSTEEM_GM_AUTHENTICATEDBY_KVK         => 'kvk';
use constant ZAAKSYSTEEM_GM_AUTHENTICATEDBY_BEHANDELAAR => 'behandelaar';

use constant ZAAKTYPE_PREFIX_SPEC_KENMERK   => 'spec';

use constant ZAAKSYSTEEM_ZAAK_URGENTIE_PERC_MEDIUM      => 0.2;
use constant ZAAKSYSTEEM_ZAAK_URGENTIE_PERC_HIGH        => 0.1;
use constant ZAAKSYSTEEM_ZAAK_URGENTIE_PERC_LATE        => 1;

use constant ZAAKSYSTEEM_LOGGING_LEVEL_DEBUG            => 'debug';
use constant ZAAKSYSTEEM_LOGGING_LEVEL_INFO             => 'info';
use constant ZAAKSYSTEEM_LOGGING_LEVEL_WARN             => 'warn';
use constant ZAAKSYSTEEM_LOGGING_LEVEL_ERROR            => 'error';
use constant ZAAKSYSTEEM_LOGGING_LEVEL_FATAL            => 'fatal';

use constant ZAAKSYSTEEM_LOGGING_LEVELS                 => {
    ZAAKSYSTEEM_LOGGING_LEVEL_DEBUG     => 1,
    ZAAKSYSTEEM_LOGGING_LEVEL_INFO      => 2,
    ZAAKSYSTEEM_LOGGING_LEVEL_WARN      => 3,
    ZAAKSYSTEEM_LOGGING_LEVEL_ERROR     => 4,
    ZAAKSYSTEEM_LOGGING_LEVEL_FATAL     => 5
};

use constant LOGGING_COMPONENT_USER         => 'user';
use constant LOGGING_COMPONENT_ZAAK         => 'zaak';
use constant LOGGING_COMPONENT_ZAAKTYPE     => 'zaaktype';
use constant LOGGING_COMPONENT_CATEGORIE    => 'bibliotheek_categorie';
use constant LOGGING_COMPONENT_NOTITIE      => 'notitie';

use constant LOGGING_COMPONENT_BETROKKENE   => 'betrokkene';
use constant LOGGING_COMPONENT_KENMERK      => 'kenmerk';
use constant LOGGING_COMPONENT_SJABLOON     => 'sjabloon';
use constant LOGGING_COMPONENT_NOTIFICATIE  => 'notificatie';
use constant LOGGING_COMPONENT_DOCUMENT     => 'document';

use constant ZAKEN_STATUSSEN                => [ qw/new open stalled resolved deleted overdragen/ ];
use constant ZAKEN_STATUSSEN_DEFAULT        => 'new';

use constant ZAAKSYSTEEM_NAAM               => 'zaaksysteem.nl';
use constant ZAAKSYSTEEM_OMSCHRIJVING       => 'Het zaaksysteem.nl is een '
                                                .'complete oplossing '
                                                .'(all-in-one) voor '
                                                .'gemeenten om de '
                                                .'dienstverlening te '
                                                .'verbeteren.';
use constant ZAAKSYSTEEM_LEVERANCIER        => 'Mintlab B.V.';
use constant ZAAKSYSTEEM_STARTDATUM         => '01-10-2009';
use constant ZAAKSYSTEEM_LICENSE            => 'EUPL';

### } END DO NOT FREAKING TOUCH

use constant ZAAKSYSTEEM_NAMING     => {
    TRIGGER_EXTERN                              => 'extern',
    TRIGGER_INTERN                              => 'intern',
    AANVRAGER_TYPE_NATUURLIJK_PERSOON           => 'natuurlijk_persoon',
    AANVRAGER_TYPE_NATUURLIJK_PERSOON_NA        => 'natuurlijk_persoon_na',
    AANVRAGER_TYPE_NIET_NATUURLIJK_PERSOON      => 'niet_natuurlijk_persoon',
    AANVRAGER_TYPE_MEDEWERKER                   => 'medewerker',
    AANVRAGER_ADRES_TYPE_ADRES                  => 'aanvrager_adres',
    AANVRAGER_ADRES_TYPE_ANDERS                 => 'anders',
    WEBFORM_TOEGANG                             => 'webform_toegang',
    WEBFORM_AUTHENTICATIE_AAN                   => 'authenticatie',
    WEBFORM_AUTHENTICATIE_OPTIONEEL             => 'optie',
    HANDELINGSINITIATOR_AANGAAN                 => 'aangaan',
    HANDELINGSINITIATOR_AANGEVEN                => 'aangeven',
    HANDELINGSINITIATOR_AANMELDEN               => 'aanmelden',
    HANDELINGSINITIATOR_AANVRAGEN               => 'aanvragen',
    HANDELINGSINITIATOR_AFKOPEN                 => 'afkopen',
    HANDELINGSINITIATOR_AFMELDEN                => 'afmelden',
    HANDELINGSINITIATOR_INDIENEN                => 'indienen',
    HANDELINGSINITIATOR_INSCHRIJVEN             => 'inschrijven',
    HANDELINGSINITIATOR_MELDEN                  => 'melden',
    HANDELINGSINITIATOR_OPZEGGEN                => 'opzeggen',
    HANDELINGSINITIATOR_REGISTREREN             => 'registreren',
    HANDELINGSINITIATOR_RESERVEREN              => 'reserveren',
    HANDELINGSINITIATOR_STELLEN                 => 'stellen',
    HANDELINGSINITIATOR_VOORDRAGEN              => 'voordragen',
    HANDELINGSINITIATOR_VRAGEN                  => 'vragen',
    HANDELINGSINITIATOR_ONTVANGEN               => 'ontvangen',
    HANDELINGSINITIATOR_AANSCHRIJVEN            => 'aanschrijven',
    HANDELINGSINITIATOR_VASTSTELLEN             => 'vaststellen',
    HANDELINGSINITIATOR_VERSTUREN               => 'versturen',
    HANDELINGSINITIATOR_UITVOEREN               => 'uitvoeren',
    HANDELINGSINITIATOR_OPSTELLEN               => 'opstellen',
    HANDELINGSINITIATOR_STARTEN                 => 'starten',
    OPENBAARHEID_OPENBAAR                       => 'openbaar',
    OPENBAARHEID_GESLOTEN                       => 'gesloten',
    TERMS_TYPE_KALENDERDAGEN                    => 'kalenderdagen',
    TERMS_TYPE_WEKEN                            => 'weken',
    TERMS_TYPE_WERKDAGEN                        => 'werkdagen',
    TERMS_TYPE_EINDDATUM                        => 'einddatum',
    TERMS_TYPE_MINUTES                          => 'minuten', # minutes from now, for testing agendering
    TERMS_TYPE_MONTHS                           => 'maanden',
    RESULTAATINGANG_VERVALLEN                   => 'vervallen',
    RESULTAATINGANG_ONHERROEPELIJK              => 'onherroepelijk',
    RESULTAATINGANG_AFHANDELING                 => 'afhandeling',
    RESULTAATINGANG_VERWERKING                  => 'verwerking',
    RESULTAATINGANG_GEWEIGERD                   => 'geweigerd',
    RESULTAATINGANG_VERLEEND                    => 'verleend',
    RESULTAATINGANG_GEBOORTE                    => 'geboorte',
    RESULTAATINGANG_EINDE_DIENSTVERBAND         => 'einde-dienstverband',
    DOSSIERTYPE_DIGITAAL                        => 'digitaal',
    DOSSIERTYPE_FYSIEK                          => 'fysiek',
};

use constant PUBLIC_CASE_FINISH_MESSAGE => 'Bedankt voor het [[case.casetype.initiator_type]] van een <strong>[[case.casetype.name]]</strong>. Uw registratie is bij ons bekend onder <strong>zaaknummer [[case.number]]</strong>. Wij verzoeken u om bij verdere communicatie dit zaaknummer te gebruiken. De behandeling van deze zaak zal spoedig plaatsvinden.';

use constant ZAAKSYSTEEM_OPTIONS    => {
    'PUBLIC_CASE_FINISH_MESSAGE' => PUBLIC_CASE_FINISH_MESSAGE,
    'RESULTAATINGANGEN'   => [
        ZAAKSYSTEEM_NAMING->{RESULTAATINGANG_VERVALLEN},
        ZAAKSYSTEEM_NAMING->{RESULTAATINGANG_ONHERROEPELIJK},
        ZAAKSYSTEEM_NAMING->{RESULTAATINGANG_AFHANDELING},
        ZAAKSYSTEEM_NAMING->{RESULTAATINGANG_VERWERKING},
        ZAAKSYSTEEM_NAMING->{RESULTAATINGANG_GEWEIGERD},
        ZAAKSYSTEEM_NAMING->{RESULTAATINGANG_VERLEEND},
        ZAAKSYSTEEM_NAMING->{RESULTAATINGANG_GEBOORTE},
        ZAAKSYSTEEM_NAMING->{RESULTAATINGANG_EINDE_DIENSTVERBAND},
    ],
    'DOSSIERTYPE'       => [
        ZAAKSYSTEEM_NAMING->{DOSSIERTYPE_DIGITAAL},
        ZAAKSYSTEEM_NAMING->{DOSSIERTYPE_FYSIEK}
    ],
    'BEWAARTERMIJN'     => {
        62      => '3 maanden',
        365     => '1 jaar',
        548     => '1,5 jaar',
        730     => '2 jaar',
        1095    => '3 jaar',
        1460    => '4 jaar',
        1825    => '5 jaar',
        2190    => '6 jaar',
        2555    => '7 jaar',
        2920    => '8 jaar',
        3285    => '9 jaar',
        3650    => '10 jaar',
        4015    => '11 jaar',
        4380    => '12 jaar',
        4745    => '13 jaar',
        5110    => '14 jaar',
        5475    => '15 jaar',
        7300    => '20 jaar',
        10950   => '30 jaar',
        14600   => '40 jaar',
        40150   => '110 jaar',
        99999   => 'Bewaren',
    },
    WEBFORM_AUTHENTICATIE   => [
        ZAAKSYSTEEM_NAMING->{WEBFORM_AUTHENTICATIE_AAN},
        ZAAKSYSTEEM_NAMING->{WEBFORM_AUTHENTICATIE_OPTIONEEL},
    ],
    TRIGGERS                => [
        ZAAKSYSTEEM_NAMING->{TRIGGER_EXTERN},
        ZAAKSYSTEEM_NAMING->{TRIGGER_INTERN},
    ],
    AANVRAGERS_INTERN       => [
        ZAAKSYSTEEM_NAMING->{AANVRAGER_TYPE_NATUURLIJK_PERSOON},
        ZAAKSYSTEEM_NAMING->{AANVRAGER_TYPE_NATUURLIJK_PERSOON_NA},
        ZAAKSYSTEEM_NAMING->{AANVRAGER_TYPE_NIET_NATUURLIJK_PERSOON},
    ],
    AANVRAGERS_EXTERN       => [
        ZAAKSYSTEEM_NAMING->{AANVRAGER_TYPE_MEDEWERKER},
    ],
    AANVRAGER_ADRES_TYPEN   => [
        ZAAKSYSTEEM_NAMING->{AANVRAGER_ADRES_TYPE_ADRES},
        ZAAKSYSTEEM_NAMING->{AANVRAGER_ADRES_TYPE_ANDERS},
    ],
    HANDELINGSINITIATORS    => [
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_AANGEVEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_AANMELDEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_AANVRAGEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_AFKOPEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_AFMELDEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_INDIENEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_INSCHRIJVEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_MELDEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_RESERVEREN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_STELLEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_VOORDRAGEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_VRAGEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_ONTVANGEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_AANSCHRIJVEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_VASTSTELLEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_UITVOEREN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_OPSTELLEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_STARTEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_OPZEGGEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_AANGAAN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_REGISTREREN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_VERSTUREN},
        ],
    OPENBAARHEDEN           => [
        ZAAKSYSTEEM_NAMING->{OPENBAARHEID_OPENBAAR},
        ZAAKSYSTEEM_NAMING->{OPENBAARHEID_GESLOTEN},
    ],
    ARCHIEFNOMINATIE_OPTIONS => {
        bewaren => 'Bewaren (B)',
        vernietigen => 'Vernietigen (V)',
        overbrengen => 'Overbrengen (O)'
    },
    RESULTAATTYPEN => [
        'aangehouden',
        'aangekocht',
        'aangesteld',
        'aanvaard',
        'afgeboekt',
        'afgebroken',
        'afgehandeld',
        'afgesloten',
        'afgewezen',
        'akkoord',
        'akkoord met wijzigingen',
        'behaald',
        'betaald',
        'buiten behandeling gesteld',
        'definitief toegekend',
        'geannuleerd',
        'gedeeltelijk gegrond',
        'gedeeltelijk verleend',
        'gedoogd',
        'gegrond',
        'gegund',
        'geweigerd',
        'gewijzigd',
        'geïnd',
        'ingeschreven',
        'ingesteld',
        'ingetrokken',
        'niet aangekocht',
        'niet aangesteld',
        'niet akkoord',
        'niet behaald',
        'niet betaald',
        'niet gegund',
        'niet gewijzigd',
        'niet geïnd',
        'niet ingesteld',
        'niet ingetrokken',
        'niet nodig',
        'niet ontvankelijk',
        'niet opgeleverd',
        'niet toegekend',
        'niet vastgesteld',
        'niet verleend',
        'niet verstrekt',
        'niet verwerkt',
        'ongegrond',
        'ontvankelijk',
        'opgeheven',
        'opgeleverd',
        'opgelost',
        'opgezegd',
        'toegekend',
        'uitgevoerd',
        'vastgesteld',
        'verhuurd',
        'verkocht',
        'verleend',
        'vernietigd',
        'verstrekt',
        'verwerkt',
        'voorlopig toegekend',
        'voorlopig verleend',
    ],
};


use constant ZAAKTYPE_DB_MAP    => {
    'kenmerken'                     => {
        'id'            => 'id',
        'naam'          => 'key',
        'label'         => 'label',
        'type'          => 'value_type',
        'omschrijving'  => 'description',
        'help'          => 'help',
        #'value'         => 'value'             # Value of kenmerken_values
                                                # FOR: ztc
#        'magicstring'   => 'magicstring',
    },
    'kenmerken_values'              => {
        'value'         => 'value',
    },
};

use constant ZAAKTYPE_KENMERKEN_ZTC_DEFINITIE   => [
    {
        'naam'          => 'zaaktype_id',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'zaaktype_nid',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'zaaktype_naam',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'zaaktype_code',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'categorie_naam',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'categorie_id',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'mogelijke_aanvragers',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'trigger',
        'in_node'       => 1,
    },
    {
        'naam'          => 'webform_authenticatie',
        'in_node'       => 1,
    },
    {
        'naam'          => 'adres_relatie',
        'in_node'       => 1,
    },
    {
        'naam'          => 'handelingsinitiator'
    },
    {
        'naam'          => 'grondslag',
    },
    {
        'naam'          => 'selectielijst',
    },
    {
        'naam'          => 'afhandeltermijn',
    },
    {
        'naam'          => 'afhandeltermijn_type',
    },
    {
        'naam'          => 'servicenorm',
    },
    {
        'naam'          => 'servicenorm_type',
    },
    {
        'naam'          => 'openbaarheid',
    },
    {
        'naam'          => 'procesbeschrijving',
    },
];

use constant ZAAKTYPE_KENMERKEN_DYN_DEFINITIE   => [
    {
        'naam'          => 'status',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'bag_items',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'streefafhandeldatum',
    },
    {
        'naam'          => 'contactkanaal',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'behandelaar',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'zaakeigenaar',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'aanvrager',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'org_eenheid',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'aanvrager_verificatie',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'aanvrager_geslachtsnaam',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'aanvrager_naam',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'aanvrager_telefoon',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'aanvrager_mobiel',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'aanvrager_email',
        'in_rt_only'    => 1,
    },
    { naam => 'gebruiker_naam', in_rt_only => 1 },
    {
        'naam'          => 'registratiedatum',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'afhandeldatum',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'vernietigingsdatum',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'besluit',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'bezwaar',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'locatie',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'depend_info',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'vroegtijdig_info',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'registratiedatum',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'urgentiedatum_high',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'urgentiedatum_medium',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'route_ou_role',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'opgeschort_info',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'resultaat',
        'in_rt_only'    => 1,
    },
];

use constant 'GEGEVENSMAGAZIJN_GBA_ADRES_EXCLUDES'     => [qw/
    straatnaam
    huisnummer
    postcode
    huisnummertoevoeging
    huisletter
    woonplaats
    functie_adres
    gemeente_code
    landcode
    adres_buitenland1
    adres_buitenland2
    adres_buitenland3
/];

use constant PARAMS_PROFILE_MESSAGES_SUB                => sub {
    my $dfv     = shift;
    my $rv      = {};

    for my $missing ($dfv->missing) {
        $rv->{$missing}  = 'Veld is verplicht.';
    }
    for my $missing ($dfv->invalid) {
        $rv->{$missing}  = 'Veld is niet correct ingevuld.';
    }

    return $rv;
};

use constant 'GEGEVENSMAGAZIJN_GBA_PROFILE'     => {
    'missing_optional_valid'  => 1,
    'required'      => [qw/
        burgerservicenummer
        geslachtsnaam
        geslachtsaanduiding
        geboortedatum
    /],
    'optional'      => [qw/
        straatnaam
        huisnummer
        postcode
        woonplaats

        voornamen
        huisnummertoevoeging
        huisletter
        woonplaats
        geboorteplaats
        geboorteland
        a_nummer

        voorletters
        voorvoegsel
        nationaliteitscode1
        nationaliteitscode2
        nationaliteitscode3
        aanhef_aanschrijving
        voorletters_aanschrijving
        voornamen_aanschrijving
        naam_aanschrijving
        voorvoegsel_aanschrijving
        burgerlijke_staat
        indicatie_geheim
        adres_buitenland1
        adres_buitenland2
        adres_buitenland3
        landcode
        import_datum
        adres_id
        authenticated
        authenticatedby

        aanduiding_naamgebruik
        functie_adres
        onderzoek_persoon
        onderzoek_huwelijk
        onderzoek_overlijden
        onderzoek_verblijfplaats

        datum_overlijden

        partner_a_nummer
        partner_voorvoegsel
        partner_geslachtsnaam
        partner_burgerservicenummer
        datum_huwelijk
        datum_huwelijk_ontbinding

        gemeente_code
        in_gemeente
    /],
    defaults    => {
        landcode => 6030,
    },
    constraint_methods => {
        'geslachtsaanduiding'   => qr/^[MV]$/,
        'in_gemeente'           => qr/^[01]$/,
    },
    dependencies => {
        landcode => sub {
            my $dfv     = shift;
            my $code    = shift;

            if ($code eq '6030') {
                return ['postcode','huisnummer','straatnaam','woonplaats'];
            } else {
                return ['adres_buitenland1'];
            }
        }
    },
    field_filters     => {
        'burgerservicenummer'   => sub {
            my ($field) = @_;

            return $field if length($field) == 9;

            return sprintf("%09d", $field);
        },
        'postcode'    => sub {
            my ($field) = @_;

            $field = uc($field);
            $field =~ s/\s*//g;

            return $field;
        },
        'huisnummer'    => sub {
            my ($field) = @_;

            return undef unless $field =~ /^\d+$/;

            return $field;
        },
        'huisnummertoevoeging'    => sub {
            my ($field) = @_;

            return undef unless $field =~ /^[\w\d\s-]+$/;

            return $field;
        },
        'geboortedatum'             => sub {
            my ($field) = @_;

            return undef unless $field =~ /^[\d-]+$/;

            if ($field =~ /^\d{8}$/) {
                my ($year, $month, $day) = $field =~ /^(\d{4})(\d{2})(\d{2})$/;

                $month  = 1 if $month    < 1;
                $day    = 1 if $day      < 1;

                my $dt;

                eval {
                    $dt      = DateTime->new(
                        'year'          => $year,
                        'month'         => $month,
                        'day'           => $day,
                        #'time_zone'     => 'Europe/Amsterdam',
                    );
                };

                if ($@) {
                    $dt = undef;
                }

                return $dt;
            } elsif ($field =~ /^(\d{2})-(\d{2})-(\d{4})$/) {
                my ($day, $month, $year) = $field =~
                    /^(\d{2})-(\d{2})-(\d{4})$/;

                $month  = 1 if $month    < 1;
                $day    = 1 if $day      < 1;

                my $dt;

                eval {
                    $dt      = DateTime->new(
                        'year'          => $year,
                        'month'         => $month,
                        'day'           => $day,
                        #'time_zone'     => 'Europe/Amsterdam',
                    );
                };

                if ($@) {
                    $dt = undef;
                }

                return $dt;
            }

            return undef;
        },
        'geslachtsaanduiding'       => sub {
            my ($field) = @_;

            return $field unless $field =~ /^[mMvV]$/;

            return uc($field);
        },
        'partner_burgerservicenummer' => sub {
            my ($field) = @_;

            return '' if $field =~ /^[0 ]+$/;

            return $field;
        },
    },
    msgs                => sub {
        my $dfv     = shift;
        my $rv      = {};

        for my $missing ($dfv->missing) {
            $rv->{$missing}  = 'Veld is verplicht.';
        }
        for my $missing ($dfv->invalid) {
            $rv->{$missing}  = 'Veld is niet correct ingevuld.';
        }

        return $rv;
    },
};

use constant GEGEVENSMAGAZIJN_KVK_PROFILE   => {
    missing_optional_valid  => 1,
    required => [ qw/
        handelsnaam

        vestiging_adres
    /],
    optional => [ qw/
        dossiernummer
        subdossiernummer
        hoofdvestiging_dossiernummer
        hoofdvestiging_subdossiernummer

        fulldossiernummer
        vorig_dossiernummer
        vorig_subdossiernummer

        vestigingsnummer

        vestiging_postcodewoonplaats
        vestiging_woonplaats
        vestiging_straatnaam
        vestiging_huisnummer
        vestiging_postcode
        vestiging_huisletter
        vestiging_huisnummertoevoeging
        vestiging_adres_buitenland2
        vestiging_adres_buitenland3
        vestiging_landcode

        rechtsvorm
        telefoonnummer
        surseance
        kamernummer

        correspondentie_adres
        correspondentie_straatnaam
        correspondentie_huisnummer
        correspondentie_huisletter
        correspondentie_huisnummertoevoeging
        correspondentie_postcodewoonplaats
        correspondentie_postcode
        correspondentie_woonplaats
        correspondentie_adres_buitenland1
        correspondentie_adres_buitenland2
        correspondentie_adres_buitenland3
        correspondentie_landcode

        hoofdactiviteitencode
        nevenactiviteitencode1
        nevenactiviteitencode2

        werkzamepersonen

        contact_naam
        contact_aanspreektitel
        contact_voorletters
        contact_geslachtsnaam
        contact_voorvoegsel
        contact_geslachtsaanduiding

        email
    /],
    dependencies => {
        dossiernummer => ['vestigingsnummer'],
        vestiging_landcode => sub {
            my $dfv     = shift;
            my $code    = shift;

            if ($code eq '6030') {
                return [
                    'vestiging_postcode',
                    'vestiging_straatnaam',
                    'vestiging_huisnummer',
                    'vestiging_postcodewoonplaats',
                    'vestiging_woonplaats',
                    'dossiernummer',
                    'hoofdvestiging_dossiernummer',
                ];
            } else {
                return ['vestiging_adres_buitenland1'];
            }
        },
        correspondentie_landcode => sub {
            my $dfv     = shift;
            my $code    = shift;

            if ($code eq '6030') {
                return ['correspondentie_postcode','correspondentie_straatnaam','correspondentie_huisnummer','correspondentie_postcodewoonplaats','correspondentie_woonplaats'];
            } else {
                return ['correspondentie_buitenland1'];
            }
        },
    },
    constraint_methods => {
        'dossiernummer'                     => qr/^\d{6,8}$/,
        'subdossiernummer'                  => qr/^\d{4}$/,
        'vestigingsnummer'                  => qr/^\d{1,12}$/,
        'hoofdvestiging_dossiernummer'      => qr/^\d*$/,
        'hoofdvestiging_subdossiernummer'   => qr/^\d*$/,

        'handelsnaam'                       => qr/^.{0,150}$/,
        'rechtsvorm'                        => qr/^\d{0,3}$/,

        'hoofdactiviteitencode'             => qr/^\d{0,6}$/,
        'nevenactiviteitencode1'            => qr/^\d{0,6}$/,
        'nevenactiviteitencode2'            => qr/^\d{0,6}$/,

        'vestiging_adres'                   => qr/^.{0,512}$/,
        'vestiging_straatnaam'              => qr/^.{0,255}$/,
        'vestiging_huisnummer'              => qr/^\d{0,6}$/,
        'vestiging_huisletter'              => qr/^[a-zA-Z]+$/,
        'vestiging_huisnummertoevoeging'    => qr/^.{0,12}$/,
        'vestiging_postcodewoonplaats'      => qr/^.{0,512}$/,
        'vestiging_postcode'                => qr/^[\d]{4}[\w]{2}$/,
        'vestiging_woonplaats'              => qr/^.{0,512}$/,

        'correspondentie_adres'                 => qr/^.{0,512}$/,
        'correspondentie_straatnaam'            => qr/^.{0,255}$/,
        'correspondentie_huisnummer'            => qr/^.{0,6}$/,
        'correspondentie_huisletter'            => qr/^[a-zA-Z]+$/,
        'correspondentie_huisnummertoevoeging'  => qr/^.{0,12}$/,
        'correspondentie_postcodewoonplaats'    => qr/^.{0,512}$/,
        'correspondentie_postcode'              => qr/^[\d]{4}[\w]{2}$/,
        'correspondentie_woonplaats'            => qr/^.{0,512}$/,
    },
    defaults => {
        vestiging_adres => sub {
            my ($dfv) = @_;

            if ($dfv->get_filtered_data->{'vestiging_straatnaam'}) {
                return
                    $dfv->get_filtered_data->{'vestiging_straatnaam'} . ' ' .
                    $dfv->get_filtered_data->{'vestiging_huisnummer'} .
                    ($dfv->get_filtered_data->{'vestiging_huisnummertoevoeging'}
                        ?  ' ' .  $dfv->get_filtered_data->{'vestiging_huisnummertoevoeging'}
                        : ''
                    );
            } elsif ($dfv->get_filtered_data->{'adres_buitenland1'}) {
                my @address      = grep(
                    { $dfv->get_filtered_data->{$_} }
                    qw/
                        vestiging_adres_buitenland1
                        vestiging_adres_buitenland2
                        vestiging_adres_buitenland3
                    /
                );

                return join(' ', @address);
            }

            return undef;
        },
        vestiging_postcodewoonplaats => sub {
            my ($dfv) = @_;

            return unless $dfv->get_filtered_data->{'vestiging_postcode'};

            return
                $dfv->get_filtered_data->{'vestiging_postcode'} . ' ' .
                $dfv->get_filtered_data->{'vestiging_woonplaats'};
        },
        correspondentie_adres => sub {
            my ($dfv) = @_;

            if ($dfv->get_filtered_data->{'correspondentie_straatnaam'}) {
                return
                    $dfv->get_filtered_data->{'correspondentie_straatnaam'} . ' ' .
                    $dfv->get_filtered_data->{'correspondentie_huisnummer'} .
                    ($dfv->get_filtered_data->{'correspondentie_huisnummertoevoeging'}
                        ?  ' ' .  $dfv->get_filtered_data->{'correspondentie_huisnummertoevoeging'}
                        : ''
                    );
            } elsif ($dfv->get_filtered_data->{'adres_buitenland1'}) {
                my @address      = grep(
                    { $dfv->get_filtered_data->{$_} }
                    qw/
                        correspondentie_adres_buitenland1
                        correspondentie_adres_buitenland2
                        correspondentie_adres_buitenland3
                    /
                );

                return join(' ', @address);
            }

            return undef;
        },
#        telefoonnummer => sub {
#            my ($dfv) = @_;
#
#            return
#                ($dfv->get_input_data->{'telefoonnummer_netnummer'} || '') . '-' .
#                ($dfv->get_input_data->{'telefoonnummer_nummer'} || '');
#        },
        fulldossiernummer => sub {
            my ($dfv) = @_;

            return unless $dfv->get_filtered_data->{'dossiernummer'};

            return
                ($dfv->get_filtered_data->{'dossiernummer'} || '') .
                ($dfv->get_filtered_data->{'subdossiernummer'} || '');
        },
        correspondentie_postcodewoonplaats => sub {
            my ($dfv) = @_;

            return unless (
                $dfv->get_filtered_data->{'correspondentie_postcode'} &&
                $dfv->get_filtered_data->{'correspondentie_woonplaats'}
            );

            return
                $dfv->get_filtered_data->{'correspondentie_postcode'} . ' ' .
                $dfv->get_filtered_data->{'correspondentie_woonplaats'};
        },
        hoofdvestiging_dossiernummer => sub {
            return unless $_[0]->get_filtered_data->{dossiernummer};

            return $_[0]->get_filtered_data->{dossiernummer};
        },
    },
    field_filters     => {
        'werkzamepersonen'  => sub {
            my ($field) = @_;

            $field =~ s/^0*//;

            return $field;
        },
        'kamernummer'  => sub {
            my ($field) = @_;

            $field =~ s/^0*//;

            return $field;
        },
        'vestigingsnummer'  => sub {
            my ($field) = @_;

            $field =~ s/^0*//;

            return $field;
        },
        'rechtsvorm'  => sub {
            my ($field) = @_;

            $field =~ s/^0*//;

            return $field;
        },
        'vestiging_huisnummer'  => sub {
            my ($field) = @_;

            $field =~ s/^0*//;

            return $field;
        },
        'surseance'  => sub {
            my ($field) = @_;

            if (lc($field) eq 'y') {
                return 1;
            } else {
                return 0;
            }

            return $field;
        },
        'telefoonnummer'  => sub {
            my ($field) = @_;

            $field =~ s/\-//;
            $field =~ s/ //;

            return substr($field, 0, 10);
        },
        'vestiging_postcode'    => sub {
            my ($field) = @_;

            $field = uc($field);
            $field =~ s/\s*//g;

            return $field;
        },
        'correspondentie_postcode'    => sub {
            my ($field) = @_;

            $field = uc($field);
            $field =~ s/\s*//g;

            return $field;
        },
    },
    msgs    => PARAMS_PROFILE_MESSAGES_SUB,
};

use constant GEGEVENSMAGAZIJN_KVK_RECHTSVORMCODES   => {
    '01'    => 'Eenmanszaak',
    '02'    => 'Eenmanszaak met meer dan één eigenaar',
    '03'    => 'N.V./B.V. in oprichting op A-formulier',
    '05'    => 'Rederij',
    '07'    => 'Maatschap',
    11    => 'Vennootschap onder firma',
    12    => 'N.V/B.V. in oprichting op B-formulier',
    21    => 'Commanditaire vennootschap met een beherend vennoot',
    22    => 'Commanditaire vennootschap met meer dan één beherende vennoot',
    23    => 'N.V./B.V. in oprichting op D-formulier',
    40    => 'Rechtspersoon in oprichting',
    41    => 'Besloten vennootschap met gewone structuur',
    42    => 'Besloten vennootschap blijkens statuten structuurvennootschap',
    51    => 'Naamloze vennootschap met gewone structuur',
    52    => 'Naamloze vennootschap blijkens statuten structuurvennootschap',
    53    => 'Naamloze vennootschap beleggingsmaatschappij met veranderlijk kapitaal',
    54    => 'Naamloze vennootschap beleggingsmaatschappij met veranderlijk kapitaal blijkens statuten structuurvennootschap',
    55    => 'Europese naamloze vennootschap (SE) met gewone structuur',
    56    => 'Europese naamloze vennootschap (SE) blijkens statuten structuurvennootschap',
    61    => 'Coöperatie U.A. met gewone structuur',
    62    => 'Coöperatie U.A. blijkens statuten structuurcoöperatie',
    63    => 'Coöperatie W.A. met gewone structuur',
    64    => 'Coöperatie W.A. blijkens statuten structuurcoöperatie',
    65    => 'Coöperatie B.A. met gewone structuur',
    66    => 'Coöperatie B.A. blijkens statuten structuurcoöperatie',
    70    => 'Vereniging van eigenaars',
    71    => 'Vereniging met volledige rechtsbevoegdheid',
    72    => 'Vereniging met beperkte rechtsbevoegdheid',
    73    => 'Kerkgenootschap',
    74    => 'Stichting',
    81    => 'Onderlinge waarborgmaatschappij U.A. met gewone structuur',
    82    => 'Onderlinge waarborgmaatschappij U.A. blijkens statuten structuuronderlinge',
    83    => 'Onderlinge waarborgmaatschappij W.A. met gewone structuur',
    84    => 'Onderlinge waarborgmaatschappij W.A. blijkens statuten structuuronderlinge',
    85    => 'Onderlinge waarborgmaatschappij B.A. met gewone structuur',
    86    => 'Onderlinge waarborgmaatschappij B.A. blijkens statuten structuuronderlinge',
    88    => 'Publiekrechtelijke rechtspersoon',
    89    => 'Privaatrechtelijke rechtspersoon',
    91    => 'Buitenlandse rechtsvorm met hoofdvestiging in Nederland',
    92    => 'Nevenvest. met hoofdvest. in buitenl.',
    93    => 'Europees economisch samenwerkingsverband',
    94    => 'Buitenl. EG-venn. met onderneming in Nederland',
    95    => 'Buitenl. EG-venn. met hoofdnederzetting in Nederland',
    96    => 'Buitenl. op EG-venn. lijkende venn. met onderneming in Nederland',
    97    => 'Buitenl. op EG-venn. lijkende venn. met hoofdnederzetting in Nederland',

    ### BUSSUM SPECIFIC
    201    => 'Coöperatie',
    202    => 'Vereniging',
};

### Contactkanalen
use constant ZAAKSYSTEEM_CONTACTKANAAL_BALIE        => 'balie';
use constant ZAAKSYSTEEM_CONTACTKANAAL_TELEFOON     => 'telefoon';
use constant ZAAKSYSTEEM_CONTACTKANAAL_POST         => 'post';
use constant ZAAKSYSTEEM_CONTACTKANAAL_EMAIL        => 'email';
use constant ZAAKSYSTEEM_CONTACTKANAAL_WEBFORM      => 'webformulier';
use constant ZAAKSYSTEEM_CONTACTKANAAL_BEHANDELAAR  => 'behandelaar';

### Hoofd en deelzaken
use constant ZAAKSYSTEEM_SUBZAKEN_DEELZAAK          => 'deelzaak';
use constant ZAAKSYSTEEM_SUBZAKEN_GERELATEERD       => 'gerelateerd';
use constant ZAAKSYSTEEM_SUBZAKEN_VERVOLGZAAK       => 'vervolgzaak';

use constant CASE_PAYMENT_STATUS_FAILED             => 'failed';
use constant CASE_PAYMENT_STATUS_SUCCESS            => 'success';
use constant CASE_PAYMENT_STATUS_PENDING            => 'pending';

use constant ZAAKSYSTEEM_CONSTANTS  => {
    'zaaksysteem_about' => [ qw/
        applicatie
        omschrijving
        leverancier
        versie
        licentie
        startdatum
    /],
    'mimetypes'         => {
        'default'                                   => 'icon-txt-32.gif',
        'dir'                                       => 'icon-folder-32.gif',
        'application/msword'                        => 'icon-doc-32.gif',
        'application/pdf'                           => 'icon-pdf-32.gif',
        'application/msexcel'                       => 'icon-xls-32.gif',
        'application/vnd.ms-excel'                  => 'icon-xls-32.gif',
        'application/vnd.ms-powerpoint'             => 'icon-ppt-32.gif',
        'text/email'                                => 'icon-email-32.gif',
        'image/jpeg'                                => 'icon-jpg-32.gif',
        'application/vnd.oasis.opendocument.text'   => 'icon-odt-32.gif',
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document' => 'icon-doc-32.gif',
        'application/vnd.openxmlformats-officedocument.presentationml.presentation' => 'icon-ppt-32.gif',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' => 'icon-xls-32.gif',
    },
    'zaken_statussen'   => ZAKEN_STATUSSEN,
    'contactkanalen'    => [
        ZAAKSYSTEEM_CONTACTKANAAL_BEHANDELAAR,
        ZAAKSYSTEEM_CONTACTKANAAL_BALIE,
        ZAAKSYSTEEM_CONTACTKANAAL_TELEFOON,
        ZAAKSYSTEEM_CONTACTKANAAL_POST,
        ZAAKSYSTEEM_CONTACTKANAAL_EMAIL,
        ZAAKSYSTEEM_CONTACTKANAAL_WEBFORM,
    ],
    'payment_statuses'  => {
        CASE_PAYMENT_STATUS_FAILED() => 'Niet geslaagd',
        CASE_PAYMENT_STATUS_SUCCESS() => 'Geslaagd',
        CASE_PAYMENT_STATUS_PENDING() => 'Wachten op bevestiging'
    },
    'subzaken_deelzaak'         => ZAAKSYSTEEM_SUBZAKEN_DEELZAAK,
    'subzaken_gerelateerd'      => ZAAKSYSTEEM_SUBZAKEN_GERELATEERD,
    'subzaken_vervolgzaak'      => ZAAKSYSTEEM_SUBZAKEN_VERVOLGZAAK,
    'authenticatedby'   => {
        'digid'         => ZAAKSYSTEEM_GM_AUTHENTICATEDBY_DIGID,
        'bedrijfid'     => ZAAKSYSTEEM_GM_AUTHENTICATEDBY_BEDRIJFID,
        'behandelaar'   => ZAAKSYSTEEM_GM_AUTHENTICATEDBY_BEHANDELAAR,
        'gba'           => ZAAKSYSTEEM_GM_AUTHENTICATEDBY_GBA,
        'kvk'           => ZAAKSYSTEEM_GM_AUTHENTICATEDBY_KVK
    },
    'mail_rcpt' => {
        'aanvrager'     => {
            naam            => 'Aanvrager',
        },
        'coordinator'   => {
            naam            => 'Coordinator',
        },
        'behandelaar'   => {
            naam            => 'Behandelaar',
            in_status       => 1,
        },
        'gemachtigde'   => {
            naam            => 'Gemachtigde',
            in_status       => 1,
        },
        'overig'        => {
            naam            => 'Overig',
            in_status       => 1,
        },
    },
    'zaaktype'  => {
        'zt_trigger'    => {
            extern  => 'Extern',
            intern  => 'Intern',
        },
        'betrokkenen'   => {
            'niet_natuurlijk_persoon'       => 'Niet natuurlijk persoon',
            'natuurlijk_persoon'            => 'Natuurlijk persoon',
            'natuurlijk_persoon_na'         => 'Natuurlijk persoon (Ongeauthoriseerd)',
            'medewerker'                    => 'Behandelaar',
            'org_eenheid'                   => 'Organisatorische eenheid',
        },
        'deelvervolg_eigenaar'  => {
            'behandelaar'   => {
                'label'     => 'Behandelaar van de huidige zaak',
            },
            'aanvrager'     => {
                'label'     => 'Aanvrager van de huidige zaak',
            },
            anders => { label => 'Andere aanvrager' }
        }
    },

    'veld_opties' => {
        'bankaccount' => {
            'label'              => 'Rekeningnummer',
            'object_search_type' => 'text',
        },
        'email' => {
            'label'              => 'E-mail',
            'object_search_type' => 'text',
        },
        'url' => {
            'label'               => 'Webadres',
            'allow_default_value' => 1,
            'object_search_type'  => 'text',
        },
        'text' => {
            'label'                    => 'Tekstveld',
            'rt'                       => 'Freeform-1',
            'allow_multiple_instances' => 1,
            'allow_default_value'      => 1,
            'object_search_type'  => 'text',
        },
        'richtext' => {
            'label'                    => 'Rich text',
            'allow_multiple_instances' => 0,
            'object_search_type'  => 'text',
        },
        'image_from_url' => {
            'label'                    => 'Afbeelding (URL)',
            'rt'                       => 'Freeform-1',
            'allow_multiple_instances' => 0,
            'allow_default_value'      => 1,
            'object_search_type'  => 'text',
        },
        'text_uc' => {
            'label'                    => 'Tekstveld (HOOFDLETTERS)',
            'rt'                       => 'Freeform-1',
            'allow_multiple_instances' => 1,
            'allow_default_value'      => 1,
            'object_search_type'       => 'text',
        },
        'numeric' => {
            'label'                    => 'Numeriek',
            'rt'                       => 'Freeform-1',
            'constraint'               => qr/^\d*$/,
            'allow_multiple_instances' => 1,
            'allow_default_value'      => 1,
            'object_search_type'       => 'number',
            'object_search_filter'     => \&_numeric_fix_filter,
        },
        'valuta' => {
            'label'                    => 'Valuta',
            'rt'                       => 'Freeform-1',
            'constraint'               => qr/^[\d,.]*$/,
            'allow_multiple_instances' => 0,
            'allow_default_value'      => 1,
            'object_search_type'       => 'number',
            'object_search_filter'     => \&_numeric_fix_filter,
        },
        'valutain' => {
            'label'                    => 'Valuta (inclusief BTW)',
            'rt'                       => 'Freeform-1',
            'type'                     => 'valuta',
            'options'                  => { 'btwin' => 1, },
            'constraint'               => qr/^[\d,.]*$/,
            'allow_multiple_instances' => 0,
            'allow_default_value'      => 1,
            'object_search_type'       => 'number',
            'object_search_filter'     => \&_numeric_fix_filter,
        },
        'valutaex' => {
            'label'                    => 'Valuta (exclusief BTW)',
            'rt'                       => 'Freeform-1',
            'type'                     => 'valuta',
            'options'                  => { 'btwex' => 1, },
            'constraint'               => qr/^[\d,.]*$/,
            'allow_multiple_instances' => 0,
            'allow_default_value'      => 1,
            'object_search_type'       => 'number',
            'object_search_filter'     => \&_numeric_fix_filter,
        },
        'valutain6' => {
            'label'                    => 'Valuta (inclusief BTW (6))',
            'rt'                       => 'Freeform-1',
            'type'                     => 'valuta',
            'options'                  => { 'btwin' => 1, },
            'constraint'               => qr/^[\d,.]*$/,
            'allow_multiple_instances' => 0,
            'allow_default_value'      => 1,
            'object_search_type'       => 'number',
            'object_search_filter'     => \&_numeric_fix_filter,
        },
        'valutaex6' => {
            'label'                    => 'Valuta (exclusief BTW (6))',
            'rt'                       => 'Freeform-1',
            'type'                     => 'valuta',
            'options'                  => { 'btwex' => 1, },
            'constraint'               => qr/^[\d,.]*$/,
            'allow_multiple_instances' => 0,
            'allow_default_value'      => 1,
            'object_search_type'       => 'number',
            'object_search_filter'     => \&_numeric_fix_filter,
        },
        'valutain21' => {
            'label'                    => 'Valuta (inclusief BTW (21))',
            'rt'                       => 'Freeform-1',
            'type'                     => 'valuta',
            'options'                  => { 'btwin' => 1, },
            'constraint'               => qr/^[\d,.]*$/,
            'allow_multiple_instances' => 0,
            'allow_default_value'      => 1,
            'object_search_type'       => 'number',
            'object_search_filter'     => \&_numeric_fix_filter,
        },
        'valutaex21' => {
            'label'                    => 'Valuta (exclusief BTW (21))',
            'rt'                       => 'Freeform-1',
            'type'                     => 'valuta',
            'options'                  => { 'btwex' => 1, },
            'constraint'               => qr/^[\d,.]*$/,
            'allow_multiple_instances' => 0,
            'allow_default_value'      => 1,
            'object_search_type'       => 'number',
            'object_search_filter'     => \&_numeric_fix_filter,
        },
        'date' => {
            'label'                    => 'Datum',
            'rt'                       => 'Freeform-1',
            'type'                     => 'datetime',
            'allow_multiple_instances' => 0,
            'allow_default_value'      => 1,
            'object_search_type'       => 'timestamp',
            'object_search_filter'     => sub {
                my ($schema, $value) = @_;

                return if not $value;

                my $rv;
                my @formats = ("%d-%m-%Y", "%Y-%m-%d", "%Y%m%d");
                while(@formats && !$rv) {
                    $rv = eval { strptime(pop @formats, $value) };
                }

                return $rv;
            },
        },
        'googlemaps' => {
            'label'               => 'Adres (Google Maps)',
            'can_zaakadres'       => 1,
            'rt'                  => 'Freeform-1',
            'allow_default_value' => 0,
            'object_search_type'  => 'text',
        },
        'textarea' => {
            'label'                    => 'Groot tekstveld',
            'rt'                       => 'Freeform-1',
            'allow_multiple_instances' => 1,
            'allow_default_value'      => 1,
            'object_search_type'  => 'text',
        },
        'option' => {
            'multiple'            => 1,
            'label'               => 'Enkelvoudige keuze',
            'rt'                  => 'Freeform-1',
            'multiple_values'     => 0,
            'allow_default_value' => 1,
            'object_search_type'  => 'text',
        },
        'select' => {
            'multiple'                 => 1,
            'label'                    => 'Keuzelijst',
            'rt'                       => 'Freeform-1',
            'multiple_values'          => 0,
            'allow_multiple_instances' => 1,
            'allow_default_value'      => 1,
            'object_search_type'  => 'text',
        },
        'checkbox' => {
            'multiple'            => 1,
            'label'               => 'Meervoudige keuze',
            'rt'                  => 'Freeform-0',
            'multiple_values'     => 1,
            'allow_default_value' => 0,
            'object_search_type'  => 'text',
        },
        'file' => {
            'label'               => 'Document',
            'allow_default_value' => 0,
            'object_search_type'  => 'text',
        },
        'subject' => {
            label => 'Betrokkene',
            object_search_type => 'text',
            allow_default_value => 0
        },
        'calendar' => {
            'label'               => 'Kalender afspraak',
            'allow_default_value' => 0,
            'object_search_type'  => 'text',
        },
        'bag_straat_adres' => {
            'label'               => 'Adres (dmv straatnaam) (BAG)',
            'object_search_type'  => 'text',
            'rt'                  => 'Freeform-1',
            'can_zaakadres'       => 1,
            'allow_default_value' => 0,
            'trigger'             => sub {
                my ($c, $newvalue, $attrobject, $veldoptie) = @_;

                my $rttriggertag = $c->model('Gegevens::Bag')
                    ->get_rt_kenmerk_trigger($newvalue);

                if ($veldoptie->{bag_zaakadres}) {
                    if (!UNIVERSAL::isa($attrobject->bag_items, 'ARRAY')) {
                        $attrobject->bag_items([$rttriggertag]);
                    }
                    else {
                        $attrobject->bag_items(
                            [@{ $attrobject->bag_items }, $rttriggertag]);
                    }
                }

                return $rttriggertag;
            },
            'filter' => sub {
                my ($c, $value) = @_;

                return $c->model('Gegevens::Bag')
                    ->remove_rt_kenmerk_trigger($value);
            },
            'object_search_type'  => 'bag',
            'object_search_filter' => \&_bag_filter,
        },
        'bag_straat_adressen' => {
            'multiple'            => 1,
            'label'               => 'Adressen (dmv straatnaam) (BAG)',
            'rt'                  => 'Freeform-0',
            'multiple_values'     => 1,
            'can_zaakadres'       => 1,
            'allow_default_value' => 0,
            'trigger'             => sub {
                my ($c, $newvalue, $attrobject, $veldoptie) = @_;

                my $rttriggertag = $c->model('Gegevens::Bag')
                    ->get_rt_kenmerk_trigger($newvalue);

                if ($veldoptie->{bag_zaakadres}) {
                    if (!UNIVERSAL::isa($attrobject->bag_items, 'ARRAY')) {
                        $attrobject->bag_items([$rttriggertag]);
                    }
                    else {
                        $attrobject->bag_items(
                            [@{ $attrobject->bag_items }, $rttriggertag]);
                    }
                }

                return $rttriggertag;
            },
            'filter' => sub {
                my ($c, $value) = @_;

                return $c->model('Gegevens::Bag')
                    ->remove_rt_kenmerk_trigger($value);
            },
            'object_search_type'  => 'bag',
            'object_search_filter' => \&_bag_filter,
        },
        'bag_adres' => {
            'label'               => 'Adres (dmv postcode) (BAG)',
            'rt'                  => 'Freeform-1',
            'can_zaakadres'       => 1,
            'allow_default_value' => 0,
            'trigger'             => sub {
                my ($c, $newvalue, $attrobject, $veldoptie) = @_;

                my $rttriggertag = $c->model('Gegevens::Bag')
                    ->get_rt_kenmerk_trigger($newvalue);

                if ($veldoptie->{bag_zaakadres}) {
                    if (!UNIVERSAL::isa($attrobject->bag_items, 'ARRAY')) {
                        $attrobject->bag_items([$rttriggertag]);
                    }
                    else {
                        $attrobject->bag_items(
                            [@{ $attrobject->bag_items }, $rttriggertag]);
                    }
                }

                return $rttriggertag;
            },
            'filter' => sub {
                my ($c, $value) = @_;

                my $cleanvalue = $c->model('Gegevens::Bag')
                    ->remove_rt_kenmerk_trigger($value);

                return $cleanvalue;

                ## BELOW DEPREACTED
                return '' unless $cleanvalue;

                return $c->model('Gegevens::Bag')
                    ->bag_human_view_by_id($cleanvalue);
            },
            'object_search_type'  => 'bag',
            'object_search_filter' => \&_bag_filter,
        },
        'bag_adressen' => {
            'label'               => 'Adressen (dmv postcode) (BAG)',
            'rt'                  => 'Freeform-0',
            'multiple'            => 1,
            'multiple_values'     => 1,
            'can_zaakadres'       => 1,
            'allow_default_value' => 0,
            'trigger'             => sub {
                my ($c, $newvalue, $attrobject, $veldoptie) = @_;

                my $rttriggertag = $c->model('Gegevens::Bag')
                    ->get_rt_kenmerk_trigger($newvalue);

                if ($veldoptie->{bag_zaakadres}) {
                    if (!UNIVERSAL::isa($attrobject->bag_items, 'ARRAY')) {
                        $attrobject->bag_items([$rttriggertag]);
                    }
                    else {
                        $attrobject->bag_items(
                            [@{ $attrobject->bag_items }, $rttriggertag]);
                    }
                }

                return $rttriggertag;
            },
            'filter' => sub {
                my ($c, $value) = @_;

                my $cleanvalue = $c->model('Gegevens::Bag')
                    ->remove_rt_kenmerk_trigger($value);

                return $cleanvalue;

                ## BELOW DEPREACTED
                return '' unless $cleanvalue;

                return $c->model('Gegevens::Bag')
                    ->bag_human_view_by_id($cleanvalue);
            },
            'object_search_type'  => 'bag',
            'object_search_filter' => \&_bag_filter,
        },
        'bag_openbareruimte' => {
            'label'               => 'Straat (BAG)',
            'rt'                  => 'Freeform-1',
            'can_zaakadres'       => 1,
            'allow_default_value' => 0,
            'trigger'             => sub {
                my ($c, $newvalue, $attrobject, $veldoptie) = @_;

                my $rttriggertag = $c->model('Gegevens::Bag')
                    ->get_rt_kenmerk_trigger($newvalue);

                if ($veldoptie->{bag_zaakadres}) {
                    if (!UNIVERSAL::isa($attrobject->bag_items, 'ARRAY')) {
                        $attrobject->bag_items([$rttriggertag]);
                    }
                    else {
                        $attrobject->bag_items(
                            [@{ $attrobject->bag_items }, $rttriggertag]);
                    }
                }

                return $rttriggertag;
            },
            'filter' => sub {
                my ($c, $value) = @_;

                my $cleanvalue = $c->model('Gegevens::Bag')
                    ->remove_rt_kenmerk_trigger($value);

                return $cleanvalue;

                ## BELOW DEPREACTED
                return '' unless $cleanvalue;

                return $c->model('Gegevens::Bag')
                    ->bag_human_view_by_id($cleanvalue);
            },
            'object_search_type'  => 'bag',
            'object_search_filter' => \&_bag_filter,
        },
        'bag_openbareruimtes' => {
            'label'               => 'Straten (BAG)',
            'rt'                  => 'Freeform-0',
            'multiple'            => 1,
            'can_zaakadres'       => 1,
            'allow_default_value' => 0,
            'trigger'             => sub {
                my ($c, $newvalue, $attrobject, $veldoptie) = @_;

                my $rttriggertag = $c->model('Gegevens::Bag')
                    ->get_rt_kenmerk_trigger($newvalue);

                if ($veldoptie->{bag_zaakadres}) {
                    if (!UNIVERSAL::isa($attrobject->bag_items, 'ARRAY')) {
                        $attrobject->bag_items([$rttriggertag]);
                    }
                    else {
                        $attrobject->bag_items(
                            [@{ $attrobject->bag_items }, $rttriggertag]);
                    }
                }

                return $rttriggertag;
            },
            'filter' => sub {
                my ($c, $value) = @_;

                my $cleanvalue = $c->model('Gegevens::Bag')
                    ->remove_rt_kenmerk_trigger($value);

                return $cleanvalue;

                ## BELOW DEPREACTED
                return '' unless $cleanvalue;

                return $c->model('Gegevens::Bag')
                    ->bag_human_view_by_id($cleanvalue);
            },
            'object_search_type'  => 'bag',
            'object_search_filter' => \&_bag_filter,
        },
    },
    'document'      => {
        'categories'        => [qw/
            Advies
            Afbeelding
            Audio
            Begroting
            Behandelvoorstel
            Beleidsnota
            Besluit
            Bewijsstuk
            Brief
            Contract
            Document
            E-mail
            Envelop
            Factuur
            Formulier
            Foto
            Legitimatie
            Memo
            Offerte
            Presentatie
            Procesverbaal
            Product
            Projectplan
            Rapport
            Tekening
            Uittreksel
            Vergaderdocument
            Verslag
            Video
            Anders
        /],
        'types'             => {
            file        => {},
            mail        => {},
            dir         => {},
            sjabloon    => {},
        },
        'sjabloon'  => {
            'export_types'  => {
                'odt'   => {
                    mimetype    => 'application/vnd.oasis.opendocument.text',
                    label       => 'OpenDocument',
                },
                'pdf'   => {
                    mimetype    => 'application/pdf',
                    label       => 'PDF',
                },
                'doc'   => {
                    mimetype    => 'application/msword',
                    label       => 'MS Word',
                }
            },
        },
    },
    'kvk_rechtsvormen'          => GEGEVENSMAGAZIJN_KVK_RECHTSVORMCODES,
    'kvk_rechtsvormen_enabled'  => [qw/
        01
        07
        11
        21
        41
        51
        55
        70
        73
        74
        88
        201
        202
    /],
    # make a hash because checkbox widget expect that
    vertrouwelijkheidsaanduiding_options => {map { $_ => $_ } (
        '-',
        'Openbaar',
        'Beperkt openbaar',
        'Intern',
        'Zaakvertrouwelijk',
        'Vertrouwelijk',
        'Confidentieel',
        'Geheim',
        'Zeer geheim'
    )},
    # seemingly convoluted but useful for making sure the same options exist
    # in the rules and casetype admin screens
    casetype_boolean_property_options => {map {$_ => $_} ('Nee', 'Ja') },
    CASETYPE_RULE_PROPERTIES => [qw/
        vertrouwelijkheidsaanduiding
        beroep_mogelijk
        publicatie
        bag
        lex_silencio_positivo
        opschorten_mogelijk
        verlenging_mogelijk
        wet_dwangsom
        wkpb
    /],
    confidentiality => {
        public => 'Openbaar',
        internal => 'Intern',
        confidential => 'Vertrouwelijk'
    },
    CHILD_CASETYPE_OPTIONS => [
        {
            "value" => "betrokkenen",
            "label" => "Relaties"
        },
        {
            "value" => "actions",
            "label" => "Acties"
        },
        {
            "value" => "allocation",
            "label" => "Toewijzing (Fase Registreren)"
        },
        {
            "value" => "first_phase",
            "label" => "Fase Registreren"
        },
        {
            "value" => "last_phase",
            "label" => "Fase Afhandelen"
        },
        {
            "value" => "resultaten",
            "label" => "Resultaten"
        },
        {
            "value" => "authorisaties",
            "label" => "Rechten"
        },
    ]
};

use constant ZAAKSYSTEEM_AUTHORIZATION_PERMISSIONS => {
    'admin'                     => {
        'label'             => 'Administrator',
        'is_systeem_recht'  => 0,
    },
    'gebruiker'                 => {
        'label'             => 'Gebruiker',
        'is_systeem_recht'  => 0,
    },
    'dashboard'                 => {},
    'zaak_intake'               => {},
    'zaak_eigen'                => {},
    'zaak_afdeling'             => {},
    'search'                    => {},
    'plugin_mgmt'               => {},
    'contact_nieuw'             => {},
    'contact_search'            => {},
#    'beheer_kenmerken_admin'    => {},
#    'beheer_sjablonen_admin'    => {},
    'beheer_gegevens_admin'     => {},
    'beheer_basisregistratie'    => {},
#    'beheer_zaaktype_admin'     => {},
    'beheer_plugin_admin'       => {},
    'vernietigingslijst'        => {},
    'zaak_add'                  => {
        'label'             => 'Mag zaak aanmaken',
        'deprecated'        => 1,
    },
    'zaak_edit'                  => {
        'label'             => 'Mag zaken behandelen',
        'is_systeem_recht'  => 1,
    },
    'zaak_read'                  => {
        'label'             => 'Mag zaken raadplegen',
        'is_systeem_recht'  => 1,
    },
    'zaak_beheer'                  => {
        'label'             => 'Mag zaken beheren',
        'is_systeem_recht'  => 1,
    },
#    'zaak_edit'                 => {
#        'label'             => 'Mag zaken behandelen (wijzigen)',
#        'is_systeem_recht'  => 1,
#    },
#    'zaak_volgende_status'      => {
#        'label'             => 'Mag zaak naar volgende status zetten',
#        'is_systeem_recht'  => 1,
#    },
#    'zaak_vorige_status'        => {
#        'label'             => 'Mag zaak naar vorige status zetten',
#        'is_systeem_recht'  => 1,
#    },
#    'zaak_behandelaar_edit'     => {
#        'label'             => 'Mag behandelaar wijzigen',
#        'is_systeem_recht'  => 1,
#    },
#    'zaak_coordinator_edit'     => {
#        'label'             => 'Mag coordinator wijzigen',
#        'is_systeem_recht'  => 1,
#    },
#    'zaak_aanvrager_edit'       => {
#        'label'             => 'Mag aanvrager wijzigen',
#        'is_systeem_recht'  => 1,
#    },
#    'zaak_verlengen'            => {
#        'label'             => 'Mag een zaak verlengen',
#        'is_systeem_recht'  => 1,
#    },
#    'zaak_opschorten'           => {
#        'label'             => 'Mag een zaak opschorten/activeren',
#        'is_systeem_recht'  => 1,
#    },
#    'zaak_vroegtijdig_afhandelen' => {
#        'label'             => 'Mag een zaak vroegtijdig afhandelen',
#        'is_systeem_recht'  => 1,
#    },
#    'zaak_relatie_edit'         => {
#        'label'             => 'Mag een relatie aanbrengen',
#        'is_systeem_recht'  => 1,
#    },
#    'zaak_deelzaak_add'         => {
#        'label'             => 'Mag een deelzaak aanmaken',
#        'is_systeem_recht'  => 1,
#    },
};


use constant ZAAKSYSTEEM_BETROKKENE_KENMERK => {
    id                  => {
        'bedrijf'               => 'id',
        'medewerker'            => 'id',
        'natuurlijk_persoon'    => 'id'
    },
    btype               => {
        'bedrijf'               => 'btype',
        'medewerker'            => 'btype',
        'natuurlijk_persoon'    => 'btype'
    },
    naam                => {
        'bedrijf'               => 'handelsnaam',
        'medewerker'            => 'naam',
        'natuurlijk_persoon'    => 'naam'
    },
    handelsnaam         => {
        'bedrijf'               => 'handelsnaam',
    },
    display_name        => {
        'bedrijf'               => 'display_name',
        'medewerker'            => 'display_name',
        'natuurlijk_persoon'    => 'display_name'
    },
    kvknummer           => {
        'bedrijf'               => 'dossiernummer',
    },
    burgerservicenummer => {
        'natuurlijk_persoon'    => 'burgerservicenummer',
    },
    a_nummer => {
        'natuurlijk_persoon'    => 'a_nummer',
    },
    login     => {
        'bedrijf'               => 'login',
    },
    password  => {
        'bedrijf'               => 'password',
    },
    'achternaam' => {
        'medewerker'            => 'geslachtsnaam',
        'natuurlijk_persoon'    => 'achternaam'
    },
    'volledigenaam' => {
        'medewerker'            => 'display_name',
        'natuurlijk_persoon'    => 'volledige_naam'
    },
    'geslachtsnaam' => {
        'medewerker'            => 'geslachtsnaam',
        'natuurlijk_persoon'    => 'geslachtsnaam'
    },
    'voorvoegsel' => {
        'natuurlijk_persoon'    => 'voorvoegsel'
    },
    'voornamen'   => {
        'natuurlijk_persoon'    => 'voornamen',
        'medewerker'            => 'voornamen',
    },
    'geslacht'    => {
        'natuurlijk_persoon'    => 'geslacht'
    },
    'aanhef'      => {
        'natuurlijk_persoon'    => 'aanhef'
    },
    'aanhef1'     => {
        'natuurlijk_persoon'    => 'aanhef1'
    },
    'aanhef2'     => {
        'natuurlijk_persoon'    => 'aanhef2'
    },
    'straat'      => {
        'natuurlijk_persoon'    => 'straatnaam',
        'bedrijf'               => 'straatnaam',
        'medewerker'            => 'straatnaam'
    },
    'huisnummer'  => {
        'natuurlijk_persoon'    => 'volledig_huisnummer',
        'bedrijf'               => 'volledig_huisnummer',
        'medewerker'            => 'huisnummer'
    },
    'postcode'    => {
        'natuurlijk_persoon'    => 'postcode',
        'bedrijf'               => 'postcode',
        'medewerker'            => 'postcode'
    },
    'woonplaats'  => {
        'natuurlijk_persoon'    => 'woonplaats',
        'bedrijf'               => 'woonplaats',
        'medewerker'            => 'woonplaats'
    },
    'tel'           => {
        'natuurlijk_persoon'    => 'telefoonnummer',
        'bedrijf'               => 'telefoonnummer',
        'medewerker'            => 'telefoonnummer'
    },
    'mobiel'       => {
        'natuurlijk_persoon'    => 'mobiel',
        'bedrijf'               => 'mobiel',
    },
    'email'       => {
        'natuurlijk_persoon'    => 'email',
        'bedrijf'               => 'email',
        'medewerker'            => 'email'
    },
    'geboortedatum'             => {
        'natuurlijk_persoon'    => 'geboortedatum',
    },
    'geboorteplaats'             => {
        'natuurlijk_persoon'    => 'geboorteplaats',
    },
    'geboorteland'             => {
        'natuurlijk_persoon'    => 'geboorteland',
    },
    'datum_huwelijk'            => {
        'natuurlijk_persoon'    => 'datum_huwelijk',
    },
    'datum_overlijden'            => {
        'natuurlijk_persoon'    => 'datum_overlijden',
    },
    'is_verhuisd' => {
        'natuurlijk_persoon'    => 'is_verhuisd'
    },
    'afdeling'              => {
        'medewerker'            => 'afdeling'
    },
    'type'        => {
        'natuurlijk_persoon'    => 'human_type',
        'bedrijf'               => 'human_type',
        'medewerker'            => 'human_type'
    },
    naamgebruik => {
        natuurlijk_persoon      => 'achternaam',
        medewerker              => 'geslachtsnaam',
    },
    in_onderzoek => {
        natuurlijk_persoon => 'in_onderzoek'
    },
    datum_huwelijk_ontbinding => {
        natuurlijk_persoon => 'datum_huwelijk_ontbinding'
    },
    rechtsvorm => {
        bedrijf => 'rechtsvorm',
    },
    vestigingsnummer => {
        bedrijf => 'vestigingsnummer'
    },
};

use constant ZAAKSYSTEEM_BETROKKENE_SUB     => sub {
    my $betrokkene  = shift || return;
    my $attr        = shift || return;
    my ($config, $sub);

    unless (
        ($config    = ZAAKSYSTEEM_BETROKKENE_KENMERK->{$attr}) &&
        ($config    = $config->{ $betrokkene->btype }) &&
        ($sub       = $betrokkene->can( $config ))
    ) {
        return;
    }

    return $sub->($betrokkene);
};


use constant LDAP_DIV_MEDEWERKER             => 'Zaakbeheerder';

use constant ZAAKSYSTEEM_AUTHORIZATION_ROLES => {
    'admin'             => {
        'ldapname'          => 'Administrator',
        'rechten'       => {
            'global'        => {
                'admin'                     => 1,
                'gebruiker'                 => 1,
                'dashboard'                 => 1,
                'zaak_eigen'                => 1,
                'zaak_afdeling'             => 1,
                'search'                    => 1,
                'plugin_mgmt'               => 1,
                'contact_search'            => 1,
                'beheer'                    => 1,
#                'beheer_kenmerken_admin'    => 1,
#                'beheer_sjablonen_admin'    => 1,
                'beheer_gegevens_admin'     => 1,
#                'beheer_zaaktype_admin'     => 1,
                'beheer_plugin_admin'       => 1,
                'vernietigingslijst'        => 1,
                'owner_signatures'           => 1,
            }
        },
    },
    'beheerder'         => {
        'ldapname'          => 'Zaaksysteembeheerder',
        'rechten'       => {
            'global'        => {
                'admin'                     => 1,
                'gebruiker'                 => 1,
                'dashboard'                 => 1,
                'zaak_eigen'                => 1,
                'zaak_afdeling'             => 1,
                'zaak_beheer'               => 1,
                'search'                    => 1,
                'plugin_mgmt'               => 1,
                'contact_nieuw'             => 1,
                'contact_search'            => 1,
                'beheer'                    => 1,
#                'beheer_kenmerken_admin'    => 1,
#                'beheer_sjablonen_admin'    => 1,
                'beheer_zaaktype_admin'     => 1,
#                'beheer_gegevens_admin'     => 1,
                'beheer_plugin_admin'       => 1,
                'vernietigingslijst'        => 1,
                'owner_signatures'           => 1,
            }
        },
    },
    'gebruikersbeheerder'       => {
        'ldapname'          => 'Gebruikersbeheerder',
        'rechten'       => {
            'global'        => {
                'admin'                     => 1,
                'gebruiker'                 => 1,
                'documenten_intake_subject' => 1,
                'dashboard'                 => 1,
                'zaak_eigen'                => 1,
                'zaak_afdeling'             => 1,
                'search'                    => 1,
                'contact_search'            => 1,
            }
        },
    },
    'zaaktypebeheerder' => {
        'ldapname'          => 'Zaaktypebeheerder',
        'rechten'       => {
            'global'        => {
                'gebruiker'                 => 1,
                'dashboard'                 => 1,
                'zaak_eigen'                => 1,
                'zaak_afdeling'             => 1,
                'zaak_beheer'               => 1,
                'search'                    => 1,
                'contact_search'            => 1,
                'beheer'                    => 1,
 #               'beheer_kenmerken_admin'    => 1, # TODO cleanup
 #               'beheer_sjablonen_admin'    => 1,
                'beheer_zaaktype_admin'     => 1,
            }
        },
    },
    'zaakbeheerder' => {
        'ldapname'          => 'Zaakbeheerder',
        'rechten'       => {
            'global'        => {
                'gebruiker'                 => 1,
                'dashboard'                 => 1,
                'zaak_eigen'                => 1,
                'zaak_afdeling'             => 1,
                'zaak_beheer'               => 1,
                'search'                    => 1,
                'contact_search'            => 1,
            }
        },
    },
    'contactbeheerder' => {
        'ldapname'          => 'Contactbeheerder',
        'rechten'       => {
            'global'        => {
                'contact_nieuw'             => 1,
            }
        },
    },
    'basisregistratiebeheerder' => {
        'ldapname'          => 'Basisregistratiebeheerder',
        'rechten'       => {
            'global'        => {
                'woz_objects'              => 1,
                'beheer_gegevens_admin'    => 1,
            }
        },
    },
    'wethouder'    => {
        'ldapname'          => 'Wethouder',
        'rechten'       => {
            'global'        => {
                'gebruiker'                 => 1,
                'dashboard'                 => 1,
                'zaak_eigen'                => 1,
                'zaak_afdeling'             => 1,
                'search'                    => 1,
                'contact_search'            => 1,
                'plugin_mgmt'               => 1,
            }
        },
    },
    'directielid'    => {
        'ldapname'          => 'Directielid',
        'rechten'       => {
            'global'        => {
                'gebruiker'                 => 1,
                'dashboard'                 => 1,
                'zaak_eigen'                => 1,
                'zaak_afdeling'             => 1,
                'search'                    => 1,
                'contact_search'            => 1,
                'plugin_mgmt'               => 1,
            }
        },
    },
    'afdelingshoofd'    => {
        'ldapname'          => 'Afdelingshoofd',
        'rechten'       => {
            'global'        => {
                'gebruiker'                 => 1,
                'dashboard'                 => 1,
                'zaak_eigen'                => 1,
                'zaak_afdeling'             => 1,
                'search'                    => 1,
                'contact_search'            => 1,
                'plugin_mgmt'               => 1,
            }
        },
    },
    'div-medewerker'    => {
        'ldapname'          => LDAP_DIV_MEDEWERKER,
        'rechten'       => {
            'global'        => {
                'documenten_intake_all'     => 1,
                'documenten_intake_subject' => 1,
                'gebruiker'                 => 1,
                'dashboard'                 => 1,
                'zaak_eigen'                => 1,
                'zaak_beheer'               => 1,
                'zaak_afdeling'             => 1,
                'search'                    => 1,
                'contact_search'            => 1,
                'plugin_mgmt'               => 1,
            }
        },
    },
    'kcc-medewerker'    => {
        'ldapname'          => 'Kcc-medewerker',
        'rechten'       => {
            'global'        => {
                'gebruiker'                 => 1,
                'dashboard'                 => 1,
                'zaak_intake'               => 1,
                'zaak_eigen'                => 1,
                'zaak_afdeling'             => 1,
                'contact_search'            => 1,
                'plugin_mgmt'               => 1,
            }
        },
    },
    'zaakverdeler'      => {
        'ldapname'          => 'Zaakverdeler',
        'rechten'       => {
            'global'        => {
                'gebruiker'                 => 1,
                'dashboard'                 => 1,
                'zaak_intake'               => 1,
                'zaak_eigen'                => 1,
                'zaak_afdeling'             => 1,
                'search'                    => 1,
                'contact_search'            => 1,
                'plugin_mgmt'               => 1,
            }
        },
    },
    'behandelaar'       => {
        'ldapname'          => 'Behandelaar',
        'rechten'       => {
            'global'        => {
                'gebruiker'                 => 1,
                'documenten_intake_subject' => 1,
                'dashboard'                 => 1,
                'zaak_eigen'                => 1,
                'zaak_afdeling'             => 1,
                'search'                    => 1,
                'contact_search'            => 1,
            }
        },
    },
};

use constant PARAMS_PROFILE_DEFAULT_MSGS => PARAMS_PROFILE_MESSAGES_SUB;

use constant DEFAULT_KENMERKEN_GROUP_DATA => {
    help        => 'Vul de benodigde velden in voor uw zaak',
    label       => 'Benodigde gegevens',
};

use constant SEARCH_QUERY_SESSION_VAR => 'SearchQuery_search_query_id';
use constant SEARCH_QUERY_TABLE_NAME  => 'DB::SearchQuery';

use constant VALIDATION_CONTACT_DATA    => {
    optional    => [qw/
        npc-telefoonnummer
        npc-email
        npc-mobiel
    /],
    constraint_methods  => {
        'npc-email'                 => qr/^.+?\@.+\.[a-z0-9]{2,}$/,
        'npc-telefoonnummer'        => qr/^[\d\+]{6,15}$/,
        'npc-mobiel'                => qr/^[\d\+]{6,15}$/,
    },
    msgs                => {
        'format'    => '%s',
        'missing'   => 'Veld is verplicht.',
        'invalid'   => 'Veld is niet correct ingevuld.',
        'constraints' => {
            '(?-xism:^\d{4}\w{2}$)' => 'Postcode zonder spatie (1000AA)',
            '(?-xism:^[\d\+]{6,15}$)' => 'Nummer zonder spatie (e.g: +312012345678)',
        }
    }, };

use constant ZAAKTYPE_TRIGGER => sub {
    my $trigger = pop;
    return grep { $trigger eq $_ } @{ZAAKSYSTEEM_OPTIONS->{TRIGGERS}};
};

use constant ZAAK_CONFIDENTIALITY => sub {
    my $c = pop;
    return grep { $c eq $_ } keys %{ZAAKSYSTEEM_CONSTANTS->{confidentiality}}
};

# TODO: Rename to ASSERT_BETROKKENE

use constant VALIDATION_EXTERNAL_CONTACT_DATA => sub {
    my $zaaktype_node = shift;
    my $profile = {
        required           => [],
        optional           => [],
        constraint_methods => VALIDATION_CONTACT_DATA->{constraint_methods},
        msgs               => VALIDATION_CONTACT_DATA->{msgs},
    };

    my %mapping = (
        contact_info_email_required        => 'npc-email',
        contact_info_phone_required        => 'npc-telefoonnummer',
        contact_info_mobile_phone_required => 'npc-mobiel',
    );

    for my $contact_type (keys %mapping) {
        if ($zaaktype_node->$contact_type) {
            push @{ $profile->{required} }, $mapping{$contact_type};
        }
        else {
            push @{ $profile->{optional} }, $mapping{$contact_type};
        }
    }

    return $profile;
};

use constant ZAAK_CREATE_PROFILE_BETROKKENE => sub {
    my $val = pop;

    # TODO: Figure this out
    my $BETROKKENE_DEFAULT_HASH = {
        'betrokkene_type'   =>
            qr/^natuurlijk_persoon|medewerker|bedrijf|org_eenheid$/,
        'betrokkene_id'     => qr/^\d+$/,
        'betrokkene'        => qr/^[\w\d-]+$/,
        'verificatie'       => qr/^digid|medewerker$/,
    };

    my @betrokkenen;
    push(@betrokkenen, $val) if UNIVERSAL::isa($val, 'HASH');
    push(@betrokkenen, $val) if blessed($val);
    push(@betrokkenen, @{ $val }) if UNIVERSAL::isa($val, 'ARRAY');

    return 0 if !@betrokkenen;

    for my $betrokkene (@betrokkenen) {
        # Object? Assume betrokkene object
        # TODO: Be more strict in checking
        next if blessed($betrokkene);

        if (!$betrokkene->{betrokkene} && !$betrokkene->{create} and
            !$betrokkene->{betrokkene_id} || !$betrokkene->{betrokkene_type}) {
                return 0;
        }

        if ($betrokkene->{create} && !$betrokkene->{betrokkene_type}) {
            return 0;
        }

        if (!$betrokkene->{verificatie}) {
            return 0;
        }
    }
    return 1;
};

use constant ZAAK_CREATE_PROFILE        => {
    required        => [ qw/
        aanvraag_trigger

        aanvragers

        registratiedatum
        contactkanaal
    /],
    'optional'      => [ qw/
        status
        milestone

        onderwerp
        resultaat
        besluit

        route_ou
        route_role

        ou_id
        role_id

        betrokkene_id
        assignee_id

        streefafhandeldatum
        afhandeldatum
        vernietigingsdatum

        coordinators
        behandelaars

        kenmerken

        created
        last_modified
        deleted

        id
        override_zaak_id

        locatie_zaak
        locatie_correspondentie

        relatie
        zaak

        actie_kopieren_kenmerken
        streefafhandeldatum_data

        ontvanger
        betrokkene_relaties
        bestemming

        uuid
        confidentiality

    /],
    'require_some'  => {
        'zaaktype_id_or_zaaktype_node_id'    => [
            1,
            'zaaktype_id',
            'zaaktype_node_id'
        ],
    },
    'constraint_methods'            => {
        'status'            => sub {
            my $val     = pop;

            return 1 unless $val;

            my $statussen = ZAKEN_STATUSSEN;

            return 1 if grep { $_ eq $val } @{ $statussen };
            return;
        },
        milestone         => qr/^\d+$/,
        onderwerp => sub {
            my $x = pop;
            return 0 if !defined $x;
            return 0 if length($x) > 256;
            return 1;
        },
        contactkanaal     => qr/^\w{1,128}$/,
        aanvragers        => ZAAK_CREATE_PROFILE_BETROKKENE,
        coordinators      => ZAAK_CREATE_PROFILE_BETROKKENE,
        behandelaars      => ZAAK_CREATE_PROFILE_BETROKKENE,
        aanvraag_trigger  => ZAAKTYPE_TRIGGER,
        confidentiality   => ZAAK_CONFIDENTIALITY,
        assignee_id         => sub {
            my $val = pop;

            return 1 if $val =~ /-/;
            return;
        },
        ontvanger         => sub {
            my $val = pop;

            return 1 if $val =~ /-/;
            return;
        },
        'betrokkene_relaties' => sub {
            my $val = pop;

            return 1 if UNIVERSAL::isa($val, 'HASH');
            return;
        }
    },
    dependencies => {
        aanvraag_trigger => sub {
            my ($dfv, $val) = @_;

            if (   lc($val) eq 'intern'
                && lc($dfv->get_filtered_data->{'bestemming'}) eq 'extern') {
                return ['ontvanger'];
            }

            return [];
        },
    },
    dependency_groups => {
        zaak_and_relatie => ['relatie', 'zaak'],
    },
    defaults => {
        status          => ZAKEN_STATUSSEN_DEFAULT,
        milestone       => 1,
        confidentiality => 'public',
    },
#    typed => {
#        aanvraag_trigger => 'Str',
#        confidentiality  => 'Str',
#    },
    msgs                => sub {
        my $dfv     = shift;
        my $rv      = {};

        for my $missing ($dfv->missing) {
            $rv->{$missing}  = 'Veld is verplicht.';
        }
        for my $missing ($dfv->invalid) {
            $rv->{$missing}  = 'Veld is niet correct ingevuld.';
        }

        return $rv;
    }
};

#
# this is the configuration for importing zaaktypes from one system into another. the challenge
# is that configurations will differ, thus not every dependency is present on every system. at
# the same time, it is not helpful to just re-import any dependency that is missing, this will
# cause duplicate items. e.g. when importing a zaaktype it looks to re-link to all it's needed
# kenmerken. if one is not found, it will ask the user if the kenmerk must be imported, or that
# another kenmerk must be selected in it's place.
#
#
# when exporting, all dependencies are exported with the zaaktype.
#
# the 'match' subroutine tries to match these exported dependencies with items that are in the
# local database. it returns a filter that is used in a query.
#
#
use constant ZAAKTYPE_DEPENDENCY_IDS => {
    'zaaktype_id$'                      => 'Zaaktype',
    '_kenmerk$'                         => 'BibliotheekKenmerken',
    '^bibliotheek_kenmerken_id$'        => 'BibliotheekKenmerken',
    '^bibliotheek_sjablonen_id$'        => 'BibliotheekSjablonen',
    '^bibliotheek_categorie_id$'        => 'BibliotheekCategorie',
    '^bibliotheek_notificaties_id$'     => 'BibliotheekNotificaties',
    '^role_id$'                         => 'LdapRole',
    '^ou_id$'                           => 'LdapOu',
    '^object_id$'                       => 'ObjectData',
    '^filestore_id$'                    => 'Filestore',
    '^checklist_vraag_status_id$'       => 'ChecklistVraagStatus',
    '^actie_\d+_datum_bibliotheek_kenmerken_id$' => 'BibliotheekKenmerken',
};

use constant ZAAKTYPE_DEPENDENCIES => {
    ChecklistVraagStatus => {
        match => ['naam'],
        name  => 'naam',
        label => 'checklistvraag',
        title => 'Checklistvraag',
        letter_e => '',
    },
    Filestore => {
        match => ['md5'],
        name  => 'original_name',
        label => 'bestand',
        title => 'Bestand',
        letter_e => '',
    },
    Zaaktype => {
        match => [],
        name  => 'zaaktype_titel',
        label => 'zaaktype',
        title => 'Zaaktype',
        letter_e => '',
        has_category => 1,
    },
    BibliotheekKenmerken => {
        match => [qw/naam deleted value_type value_mandatory value_length besluit/],
        name  => 'naam',
        label => 'kenmerk',
        title => 'Kenmerken',
        letter_e => '',
        has_category => 1,
    },
    BibliotheekNotificaties => {
        match => [qw/label subject message/],
        name  => 'label',
        label => 'bericht',
        title => 'Berichten',
        letter_e => '',
        has_category => 1,
    },
    BibliotheekSjablonen         => {
        match => [qw/naam/],
        name  => 'naam',
        label => 'sjabloon',
        letter_e => 'e',
        has_category => 1,
    },
    LdapRole => {
        match => [qw/short_name/],
        name  => 'short_name',
        label => 'rol',
        title => 'Rol',
        letter_e => 'e',
    },
    LdapOu => {
        match => [qw/ou/],
        name  => 'ou',
        label => 'organisatorische eenheid',
        letter_e => 'e',
    },
    BibliotheekCategorie        => {
        match => [qw/naam/],
        name  => 'naam',
        label => 'categorie',
        letter_e => 'e',
    },
    ObjectData => {
        match => [qw/uuid/],
        name => 'naam',
        label => 'object',
        letter_e => '',
    },
};

use constant BETROKKENE_RELATEREN_PROFILE => {
    required    => [qw/
        betrokkene_identifier
        magic_string_prefix
        rol
    /],
    optional => [qw[
        pip_authorized
        send_auth_confirmation
    ]],
    msgs                => sub {
        my $dfv     = shift;
        my $rv      = {};

        for my $missing ($dfv->missing) {
            $rv->{$missing}  = 'Veld is verplicht.';
        }
        for my $missing ($dfv->invalid) {
            $rv->{$missing}  = 'Veld is niet correct ingevuld.';
        }

        return $rv;
    }
};

use constant BETROKKENE_RELATEREN_MAGIC_STRING_SUGGESTION => sub {
    my (@used_columns)              = @{ shift(@_) };
    my ($prefix, $rol) = @_;

    ### make a suggestion or give back given string
    my $suggestion  = lc($prefix || $rol);
    $suggestion =~ s/[^a-z0-9]//g;

    my $counter     = '';

    while (
        grep {
            $suggestion . $counter .'_naam' eq $_ ||
            $suggestion .  $counter eq $_
        } @used_columns, Zaaksysteem::Attributes::ZAAKSYSTEEM_MAGIC_STRINGS()
             # Attributes.pm already uses Constants.pm
    ) {
        $counter = 0 if !$counter;
        $counter++;
    }

    $suggestion     .= $counter;

    return $suggestion;

};

use constant VERNIETIGINGS_REDENEN  => [
    'In belang van de aanvrager',
    'Uniek of bijzonder karakter voor de organisatie',
    'Bijzondere tijdsomstandigheid of gebeurtenis',
    'Beeldbepalend karakter',
    'Samenvatting van gegevens',
    'Betrokkene(n) is van bijzondere betekenis geweest',
    'Vervanging van stukken bij calamiteit',
    'Aanleiding van algemene regelgeving',
    'Verstoring van logische samenhang',
];



use constant ZAAK_WIJZIG_VERNIETIGINGSDATUM_PROFILE         => {
    required            => [qw/
    /],
    optional            => [qw/
        reden
        vernietigingsdatum_type
        vernietigingsdatum
    /],
    constraint_methods  => {
        vernietigingsdatum_type => sub {
            my ($dfv, $val) = @_;

            if($val eq 'termijn') {
                my $vernietigingsdatum = $dfv->{'__INPUT_DATA'}->{vernietigingsdatum};

                if(
                    !UNIVERSAL::isa($vernietigingsdatum, 'DateTime') &&
                    $vernietigingsdatum !~ /^\d{2}\-\d{2}\-\d{4}$/
                ) {
                    return;
                }
            }

            return 1;
        },
        vernietigingsdatum  => sub {
            my ($dfv, $val) = @_;

            if (UNIVERSAL::isa($val, 'DateTime')) {
                return 1;
            }

            if ($val =~ /^\d{2}\-\d{2}\-\d{4}$/) {
                return 1;
            }

            return;
        },
        reden               => sub {
            my ($dfv, $value) = @_;

            my $redenen = VERNIETIGINGS_REDENEN;

            if (grep { $_ eq $value } @{ $redenen }) {
                return 1;
            }

            return;
        },
    },
    field_filters       => {
        vernietigingsdatum  => sub {
            my ($val) = @_;

            if (UNIVERSAL::isa($val, 'DateTime')) {
                return $val;
            }

            if (
                (my ($day, $month, $year) = $val =~
                    /^(\d{2})\-(\d{2})\-(\d{4})$/)
            ) {
                return DateTime->new(
                    year        => $year,
                    day         => $day,
                    month       => $month
                );
            }

            return $val;
        }
    },
    msgs                => PARAMS_PROFILE_MESSAGES_SUB,
};



use constant ZAAK_WIJZIG_STREEFAFHANDELDATUM_PROFILE         => {
    required            => [qw/
        streefafhandeldatum
    /],
    constraint_methods  => {
        streefafhandeldatum  => sub {
            my ($dfv, $val) = @_;

            if (UNIVERSAL::isa($val, 'DateTime')) {
                return 1;
            }

            if ($val =~ /^\d{2}\-\d{2}\-\d{4}$/) {
                return 1;
            }

            return;
        },
    },
    field_filters       => {
        streefafhandeldatum  => sub {
            my ($val) = @_;

            if (UNIVERSAL::isa($val, 'DateTime')) {
                return $val;
            }

            if (
                (my ($day, $month, $year) = $val =~
                    /^(\d{2})\-(\d{2})\-(\d{4})$/)
            ) {
                return DateTime->new(
                    year        => $year,
                    day         => $day,
                    month       => $month
                );
            }

            return $val;
        }
    },
    msgs                => PARAMS_PROFILE_MESSAGES_SUB,
};

use constant ZAAK_WIJZIG_REGISTRATIEDATUM_PROFILE         => {
    required            => [qw/
        registratiedatum
    /],
    constraint_methods  => {
        registratiedatum  => sub {
            my ($dfv, $val) = @_;

            if (UNIVERSAL::isa($val, 'DateTime')) {
                return 1;
            }

            if ($val =~ /^\d{2}\-\d{2}\-\d{4}$/) {
                return 1;
            }

            return;
        },
    },
    field_filters       => {
        registratiedatum  => sub {
            my ($val) = @_;

            if (UNIVERSAL::isa($val, 'DateTime')) {
                return $val;
            }

            if (
                (my ($day, $month, $year) = $val =~
                    /^(\d{2})\-(\d{2})\-(\d{4})$/)
            ) {
                return DateTime->new(
                    year        => $year,
                    day         => $day,
                    month       => $month
                );
            }

            return $val;
        }
    },
    msgs => PARAMS_PROFILE_MESSAGES_SUB,
};

use constant DOCUMENTS_STORE_TYPE_NOTITIE       => 'notitie';
use constant DOCUMENTS_STORE_TYPE_FILE          => 'file';
use constant DOCUMENTS_STORE_TYPE_MAIL          => 'mail';
use constant DOCUMENTS_STORE_TYPE_JOB           => 'job';

use constant BETROKKENE_TYPE_BEDRIJF            => 'bedrijf';
use constant BETROKKENE_TYPE_NATUURLIJK_PERSOON => 'natuurlijk_persoon';
use constant BETROKKENE_TYPE_MEDEWERKER         => 'medewerker';

use constant OBJECTSEARCH_TABLE_ORDER => qw[contact zaak file];

use constant OBJECTSEARCH_TABLENAMES => {
    bedrijf                  => { tablename => 'Bedrijf' },
    natuurlijk_persoon       => { tablename => 'NatuurlijkPersoon', },
    zaak                     => { tablename => 'Zaak' },
    zaaktype                 => { tablename => 'Zaaktype' },
    file                     => { tablename => 'File' },
    bibliotheek_kenmerken    => { tablename => 'BibliotheekKenmerken' },
    bibliotheek_sjablonen    => { tablename => 'BibliotheekSjablonen' },
    bibliotheek_categorie    => { tablename => 'BibliotheekCategorie' },
    bibliotheek_notificaties => { tablename => 'BibliotheekNotificaties' }
};

use constant OBJECT_ACTIONS => {
    case => {
        allocate => {
            label => 'Toewijzing wijzigen',
            path => '/bulk/update/allocation'
        },
        acquire => {
            label => 'In behandeling nemen',
            path => '/bulk/update/owner'
        },
        suspend => {
            label => 'Opschorten',
            path => '/bulk/update/opschorten'
        },
        resume => {
            label => 'Hervatten',
            path => '/bulk/update/resume'
        },
        prolong => {
            label => 'Verlengen',
            path => '/bulk/update/verlengen'
        },
        relate => {
            label => 'Relateren',
            path => '/bulk/update/relatie'
        },
        manage => {
            label => 'Beheeracties',
            path => '/bulk/update/set_settings'
        },
        destroy => {
            label => 'Vernietigen',
            path => '/bulk/update/destroy'
        },
        publish => {
            label => 'Publiceren',
            path => '/bulk/publish'
        },
        export => {
            label => 'Exporteren',
            path => undef # implies current zql query
        }
    }
};

use constant ALLOW_ONLY_TRUSTED_HTML    => sub {
    my $val     = shift;

    use HTML::TagFilter;

    my $tf      = new HTML::TagFilter;

    return $tf->filter($val);
};

use constant ALLOW_NO_HTML    => sub {
    my $val     = shift;

    use HTML::TagFilter;

    my $tf      = HTML::TagFilter->new(allow => {});

    return $tf->filter($val);
};

use constant STATUS_LABELS => {
    new         => 'Nieuw',
    open        => 'In behandeling',
    stalled     => 'Opgeschort',
    deleted     => 'Vernietigd',
    resolved    => 'Afgehandeld',
    overdragen  => 'Overdragen',
    vernietigen => 'Te vernietigen',
};

use constant MIMETYPES_ALLOWED => {
    '.docx'    => {
        mimetype    => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        conversion  => 'jodconvertor',
        alternate_mimetypes => ['application/x-zip'],
    },
    '.xls'     => {
        mimetype    => 'application/vnd.ms-excel',
        conversion  => 'jodconvertor',
        alternate_mimetypes => ['application/msword'],
    },
    '.xlsx'    => {
        mimetype    => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        conversion  => 'jodconvertor',
    },
    '.ppt'     => {
        mimetype    => 'application/vnd.ms-powerpoint',
        conversion  => 'jodconvertor',
        alternate_mimetypes => ['application/msword','application/vnd.openxmlformats-officedocument.presentationml.presentation'],
    },
    '.pptx'    => {
        mimetype    => 'application/vnd.ms-powerpoint',
        conversion  => 'jodconvertor',
        alternate_mimetypes => ['application/msword','application/vnd.openxmlformats-officedocument.presentationml.presentation'],
    },
    '.odp'     => {
        mimetype    => 'application/vnd.oasis.opendocument.presentation',
        conversion  => 'jodconvertor',
        alternate_mimetypes => ['application/vnd.openxmlformats-officedocument.presentationml.presentation'],
    },
    '.pdf'     => {
        mimetype    => 'application/pdf',
        conversion  => 'none',
    },
    '.bmp'     => {
        mimetype    => 'image/bmp',
        conversion  => 'imagemagick',
    },
    '.gif'     => {
        mimetype    => 'image/gif',
        conversion  => 'imagemagick',
    },
    '.jpeg'    => {
        mimetype    => 'image/jpeg',
        conversion  => 'imagemagick',
    },
    '.jpg'     => {
        mimetype    => 'image/jpeg',
        conversion  => 'imagemagick',
    },
    '.png'     => {
        mimetype    => 'image/png',
        conversion  => 'imagemagick',
    },
    '.tiff'    => {
        mimetype    => 'image/tiff',
        conversion  => 'imagemagick',
    },
    '.tif'     => {
        mimetype    => 'image/tiff',
        conversion  => 'imagemagick',
    },
    '.htm'     => {
        mimetype    => 'text/html',
        conversion  => 'xhtml2pdf',
    },
    '.html'    => {
        mimetype    => 'text/html',
        conversion  => 'xhtml2pdf',
    },
    '.xml'     => {
        mimetype    => 'text/xml',
    },
    '.rtf'     => {
        mimetype    => 'text/rtf', #application/rtf
        conversion  => 'jodconvertor',
    },
    '.doc'     => {
        mimetype    => 'application/msword',
        conversion  => 'jodconvertor',
    },
    '.dot'     => {
        mimetype    => 'application/msword',
        conversion  => 'jodconvertor',
    },
    '.odt'     => {
        mimetype    => 'application/vnd.oasis.opendocument.text',
        conversion  => 'jodconvertor',
        alternate_mimetypes => ['application/x-zip'],
    },
    '.ods'     => {
        mimetype    => 'application/vnd.oasis.opendocument.spreadsheet',
        conversion  => 'jodconvertor',
    },
    '.odf' => {
        mimetype => 'application/vnd.oasis.opendocument.text',
        conversion => 'jodconvertor'
    },
    '.svg'     => {
        mimetype    => 'image/svg+xml',
        conversion  => 'imagemagick',
    },
    '.csv'     => {
        mimetype    => 'text/csv',
        conversion  => 'jodconvertor',
    },
    '.xps'     => {
        mimetype    => 'application/vnd.ms-xpsdocument',
        conversion  => 'xpstopdf',
        alternate_mimetypes => ['application/x-zip'],
    },
    '.asc'     => {
        mimetype    => 'text/plain',
        conversion  => '',
    },
    '.txt'     => {
        mimetype    => 'text/plain',
        conversion  => 'jodconvertor',
    },
    '.gml'     => {
        mimetype    => 'application/xml',
        conversion  => '',
        alternate_mimetypes => ['application/gml', 'application/gml+xml'],
    },
    '.shp'     => {
        mimetype    => 'application/octet-stream',
        conversion  => '',
    },
    '.crt' => {
        mimetype => 'text/plain',
        conversion => '',
        alternate_mimetypes => ['application/x-pem-file']
    },
    '.pem' => {
        mimetype => 'text/plain',
        conversion => '',
        alternate_mimetypes => ['application/x-pem-file']
    },
    '.key' => {
        mimetype => 'text/plain',
        conversion => '',
        alternate_mimetypes => ['application/x-pem-file']
    },
    '.msg' => {
        mimetype => 'application/vnd.ms-outlook'
    },
    '.eml' => {
        mimetype => 'message/rfc822'
    }
};

use constant JOBS_INFORMATION_MAP   => {
    'zaak::update_kenmerk'    => {
        change_type => 'wijziging',
        label       => 'Wijzigen van kenmerk',
    },
};

use constant STRONG_RELATED_COLUMNS => [qw/
    aanvrager
    behandelaar
    coordinator
    locatie_correspondentie
    locatie_zaak
/];

use constant ZAAK_EMPTY_COLUMNS => [qw/
    aanvrager
    behandelaar
    coordinator
    locatie_correspondentie
    locatie_zaak
/];

=head2 Event type groupings

Ability to group events into categories for easier consumption.

=cut

use constant EVENT_TYPE_GROUPS => {
    document        => [qw[document case/document]],
    case            => [qw[case]],
    contactmoment   => [qw[subject/contact_moment]],
    note            => [qw[case/note subject/note]]
};

=head2 DENIED_BROWSERS

Deny browsers or certain browser versions, examples:

 ie         # Deny ie
 ie::<10    # Deny ie below version 10
 ie::>10    # Deny ie above version 10
 ie::7      # Deny ie 7

=cut

use constant DENIED_BROWSERS   => [qw/
    ie::<9
/];

use constant FILE_STORE_LOCATION =>
    '/home/zaaksysteem/dev/Zaaksysteem_Documentairly/files/storage';

use constant FRIENDLY_BETROKKENE_MESSAGES => {
    deceased    => 'Betrokkene is overleden',
    secret      => 'Betrokkene heeft een indicatie "Geheim"',
    moved       => 'Betrokkene is verhuisd',
    onderzoek   => 'Betrokkene staat in onderzoek',
    briefadres  => 'Betrokkene heeft een briefadres'
};

use constant EDE_PUBLICATION_STRINGS => {
    '_empty' => [],
    'Collegevergadering' => [],
    'Raadscommissie AZ&M' => [],
    'Raadscommissie RO' => [],
    'Raadscommissie MZ' => [],
    'Raadscommissie BFO' => [],
    'Raadsvergadering' => [],
    'College van B en W' => [],
    'Besluitvormende vergadering' => ['Opening en mededelingen van de voorzitter', 'Vaststelling agenda', 'Vragenuurtje'],
    'Oordeelvormende vergadering' => ['Opening en mededelingen van de voorzitter'],
    'Beeldvormende/informatieve ronde' => ['Opening en mededelingen van de voorzitter']
};

use constant SUBJECT_TYPE_EMPLOYEE => 'employee';

use constant SJABLONEN_TARGET_FORMATS => sub {
    my $format = shift;
    if ($format && $format =~ m/^(?:odt|pdf)$/) {
        return 1;
    }
    return 0;
};

use constant ZAAKTYPE_ATTRIBUTEN => {
    ztc_grondslag             => 'text',
    ztc_handelingsinitiator   => 'text',
    ztc_selectielijst         => 'text',
    ztc_afhandeltermijn       => 'text',
    ztc_afhandeltermijn_type  => 'text',
    ztc_servicenorm           => 'text',
    ztc_servicenorm_type      => 'text',
    ztc_escalatiegeel         => 'text',
    ztc_escalatieoranje       => 'text',
    ztc_escalatierood         => 'text',
    ztc_openbaarheid          => 'text',
    ztc_webform_toegang       => 'text',
    ztc_webform_authenticatie => 'text',
    pdc_meenemen              => 'text',
    pdc_description           => 'text',
    pdc_voorwaarden           => 'text',
    pdc_tarief                => 'text',
    ztc_procesbeschrijving    => 'text',
};


use constant STUF_VERSIONS => qr/^(?:0204|0301)$/;
use constant STUF_XSD_PATH => 'share/wsdl/stuf';
use constant STUF_XML_URL  => 'http://www.egem.nl/StUF';

=head2 BAG_TYPES

The types of Basis Administratie Gemeentes supported by Zaaksysteem

=cut

use constant BAG_TYPES => [qw/ligplaats nummeraanduiding openbareruimte pand standplaats verblijfsobject verblijfsobjectpand woonplaats/];

=head2 PROFILE_BAG_TYPES_OK

Helper function for parameter checking for BAG_TYPES

=cut

use constant PROFILE_BAG_TYPES_OK => sub {
    my $type = shift;
    if ($type) {
        $type = lc($type);
        if (grep { $type eq $_ } @{BAG_TYPES()}) {
            return $type;
        }
    }
    return undef;
};


use constant SERVICE_NORM_TYPES => [
      ZAAKSYSTEEM_NAMING->{TERMS_TYPE_WERKDAGEN},
      ZAAKSYSTEEM_NAMING->{TERMS_TYPE_KALENDERDAGEN},
      ZAAKSYSTEEM_NAMING->{TERMS_TYPE_WEKEN},
      ZAAKSYSTEEM_NAMING->{TERMS_TYPE_EINDDATUM},
];

use constant SERVICE_NORM_TYPES_OK => sub {
    my $type = shift;
    if ($type) {
        $type = lc($type);
        if (grep { $type eq lc($_) } @{SERVICE_NORM_TYPES()}) {
            return $type;
        }
    }
    return undef;
};

use constant ZAAKSYSTEEM_BOFH => 'betrokkene-medewerker-20000';

{
    #
    # The code in the _domain function is stolen from
    # http://cpansearch.perl.org/src/CREIN/Regexp-Common-dns-0.00_01/lib/Regexp/Common/dns.pm
    #
    # Original author: Chris Reinhardt cpan at triv.org
    # Copyright (c) 2003 Chris Reinhardt.
    #
    # All rights reserved.  This program is free software; you may redistribute
    # it and/or modify it under the same terms as Perl itself.
    #
    # Wesley Schwengle (2014)
    # I've added support for RFC 1123.
    # The license is compatible with the license we use at Mintlab:
    # https://joinup.ec.europa.eu/software/page/eupl/eupl-compatible-open-source-licences
    #
    sub _domain {
        my %flags = @_;

        my $sep         = '\.';
        my $letter      = '[a-zA-Z]';
        my $let_dig     = '[a-zA-Z0-9]';
        my $let_dig_hyp = '[-a-zA-Z0-9]';

        my %labels = (
            1035   => "(?:$letter(?:$let_dig|$let_dig_hyp\{1,61}$let_dig)?)",
            1123   => "(?:(?:$let_dig|$let_dig$let_dig_hyp*$let_dig)$sep)*(?:$let_dig|$let_dig$let_dig_hyp*$let_dig)",
            2181   => '[^.]{1,63}',
            hybrid => '[a-zA-Z0-9_-]{1,63}'
        );

        $flags{'-rfc'} ||= 1035;

        my $label = $labels{$flags{'-rfc'}} || die("Unknown DNS RFC: $flags{'-rfc'}");

        if ($flags{'-rfc'} ne 2181 && exists $flags{'-wildcard'} && not defined $flags{'-wildcard'}) {
            $label = "(?:\\*|$label)";
        }

        my $quant = '*';
        if ($flags{'-minlabels'}) {
            $quant = '{' . ($flags{'-minlabels'} - 1) . ',}';
        }

        return qr/^(?:$label$sep)$quant$label$sep?$/;
    }

    my $fqdn_regexp = _domain(-rfc => 1123, -minlabels => 2);

    use constant VALID_FQDN => sub {
        my $fqdn = pop;
        if ($fqdn && $fqdn =~ m/$fqdn_regexp/) {
            $fqdn =~ s/\.$//;
            return 0 if (length($fqdn) > 255);
            return 0 if (grep { length($_) > 63 } split(/\./, $fqdn));
            return 1;
        }
        return 0;
    };
}

use constant DOCUMENT_STATUS => sub {
    my $status = pop;
    if ($status && $status =~ m/^(?:original|copy|converted|replaced)$/) {
        return 1;
    }
    return 0;
};

use constant OBJECT_CLASS_DOMAIN  => 'zs_domain';
use constant OBJECT_CLASS_CASE    => 'case';
use constant OBJECT_CLASS_DEFAULT => 'object';

use constant ZS_PROFILE_CREATE_DOMAIN => {
    required => {
        fqdn  => 'Str',
        label => 'Str',
    },
    optional => {
        owner       => 'Any',    # TODO: Type this correctly
        certificate => 'Any',    # TODO: This should create a file
    },
    # This breaks, for some vague reason
    #constraint_methods => { fqdn  => VALID_FQDN },
    defaults           => { owner => '' },
};

sub _bag_filter {
    # XXX Almost cloned from lib/Zaaksysteem/Attributes.pm
    # Probably needs to become part of B::O::A::BAG somehow
    my ($schema, $bag_id) = @_;
    return unless $bag_id;
    my $rs = $schema->resultset('BagNummeraanduiding');

    my $human_identifier = eval {
        $rs->get_human_identifier_by_source_identifier(
            $bag_id,
            {prefix_with_city => 1}
        ),
    };
    my $address_data = eval {
        $rs->get_address_data_by_source_identifier($bag_id),
    };

    return {
        bag_id           => $bag_id,
        human_identifier => $human_identifier,
        address_data     => $address_data,
    };
}

sub _numeric_fix_filter {
    my ($schema, $value) = @_;

    return if not length($value);

    # Assumption:
    # - 123,12       -> 123 + 0.12
    # - 123,123      -> 123123
    # - 123,123,123  -> 123123123
    # - 123.45,67.89 -> 1234567.89
    # - €            -> undef
    $value =~ s/,([0-9][0-9]?)$/.$1/g;
    $value =~ s/\.([0-9]{3})/$1/g;
    $value =~ s/[^0-9.]//g;

    # Zero-width look-ahead: All "." not followed by another "." later on (i.e.
    # all but the last ".") are removed.
    $value =~ s/\.(?=.*\.)//g;

    # Make sure values with no valid characters at all are returned as undef,
    # not the empty string (which isn't a valid number)
    if (length($value) && $value ne '.') {
        return $value;
    }
    else {
        return;
    }
}

=head1 SAML2 Constants

=over 4

=item SAML_TYPE_LOGIUS

This the main (only?) provider for DigiD services.

=item SAML_TYPE_KPN_LO

This is our currently supported supplier for eHerkenning services.

=item SAML_TYPE_ADFS

The identifier for Microsoft's AD Federation Services

=item SAML_TYPE_MINIMAL

Minimal SAML instance, only given attribute is the login name as login identifier

=back

=cut

use constant SAML_TYPE_LOGIUS   => 'digid';
use constant SAML_TYPE_KPN_LO   => 'eherkenning';
use constant SAML_TYPE_ADFS     => 'adfs';
use constant SAML_TYPE_SPOOF    => 'spoof';
use constant SAML_TYPE_MINIMAL  => 'minimal';

use constant RGBZ_LANDCODES => {
    '0' => 'Onbekend',
    '5001' => 'Canada',
    '5002' => 'Frankrijk',
    '5003' => 'Zwitserland',
    '5004' => 'Rhodesië',
    '5005' => 'Malawi',
    '5006' => 'Cuba',
    '5007' => 'Suriname',
    '5008' => 'Tunesië',
    '5009' => 'Oostenrijk',
    '5010' => 'België',
    '5011' => 'Botswana',
    '5012' => 'Iran',
    '5013' => 'Nieuwzeeland',
    '5014' => 'Zuidafrika',
    '5015' => 'Denemarken',
    '5016' => 'Noordjemen',
    '5017' => 'Hongarije',
    '5018' => 'Saoediarabië',
    '5019' => 'Liberia',
    '5020' => 'Etiopië',
    '5021' => 'Chili',
    '5022' => 'Marokko',
    '5023' => 'Togo',
    '5024' => 'Ghana',
    '5025' => 'Laos',
    '5026' => 'Angola',
    '5027' => 'Filipijnen',
    '5028' => 'Zambia',
    '5029' => 'Mali',
    '5030' => 'Ivoorkust',
    '5031' => 'Burma',
    '5032' => 'Monaco',
    '5033' => 'Colombia',
    '5034' => 'Albanië',
    '5035' => 'Kameroen',
    '5036' => 'Zuidviëtnam',
    '5037' => 'Singapore',
    '5038' => 'Paraguay',
    '5039' => 'Zweden',
    '5040' => 'Cyprus',
    '5041' => 'Australisch Nieuwguinea',
    '5042' => 'Brunei',
    '5043' => 'Irak',
    '5044' => 'Mauritius',
    '5045' => 'Vaticaanstad',
    '5046' => 'Kashmir',
    '5047' => 'Myanmar',
    '5048' => 'Jemen',
    '5049' => 'Slovenië',
    '5050' => 'Zaïre',
    '5051' => 'Kroatië',
    '5052' => 'Taiwan',
    '5053' => 'Rusland',
    '5054' => 'Armenië',
    '5055' => 'Ascension',
    '5056' => 'Azoren',
    '5057' => 'Bahrein',
    '5058' => 'Bhutan',
    '5059' => 'Britse Antillen',
    '5060' => 'Comoren',
    '5061' => 'Falklandeilanden',
    '5062' => 'Frans Guyana',
    '5063' => 'Frans Somaliland',
    '5064' => 'Gilbert- en Ellice-eilanden',
    '5065' => 'Groenland',
    '5066' => 'Guadeloupe',
    '5067' => 'Kaapverdische Eilanden',
    '5068' => 'Macau',
    '5069' => 'Martinique',
    '5070' => 'Mozambique',
    '5071' => 'Pitcairneilanden',
    '5072' => 'Guinee Bissau',
    '5073' => 'Réunion',
    '5074' => 'Saint Pierre en Miquelon',
    '5075' => 'Seychellen en Amiranten',
    '5076' => 'Tonga',
    '5077' => 'Wallis en Futuna',
    '5078' => 'Zuidwest Afrika',
    '5079' => 'Frans Indië',
    '5080' => 'Johnston',
    '5081' => 'Kedah',
    '5082' => 'Kelantan',
    '5083' => 'Malakka',
    '5084' => 'Mayotte',
    '5085' => 'Negri Sembilan',
    '5086' => 'Pahang',
    '5087' => 'Perak',
    '5088' => 'Perlis',
    '5089' => 'Portugees Indië',
    '5090' => 'Selangor',
    '5091' => 'Sikkim',
    '5092' => 'Sint Vincent en de Grenadinen',
    '5093' => 'Spitsbergen',
    '5094' => 'Trengganu',
    '5095' => 'Aruba',
    '5096' => 'Burkina Faso',
    '5097' => 'Azerbajdsjan',
    '5098' => 'Belarus (Wit-Rusland)',
    '5099' => 'Kazachstan',
    '5100' => 'Macedonië',
    '5101' => 'Timor Leste',
    '5102' => 'Servië en Montenegro',
    '5103' => 'Servië',
    '5104' => 'Montenegro',
    '5105' => 'Kosovo',
    '5106' => 'Bonaire',
    '5107' => 'Curaçao',
    '5108' => 'Saba',
    '5109' => 'Sint Eustatius',
    '5110' => 'Sint Maarten',
    '6000' => 'Moldavië',
    '6001' => 'Burundi',
    '6002' => 'Finland',
    '6003' => 'Griekenland',
    '6004' => 'Guatemala',
    '6005' => 'Nigeria',
    '6006' => 'Libië',
    '6007' => 'Ierland',
    '6008' => 'Brazilië',
    '6009' => 'Rwanda',
    '6010' => 'Venezuela',
    '6011' => 'IJsland',
    '6012' => 'Liechtenstein',
    '6013' => 'Somalia',
    '6014' => 'Verenigde Staten van Amerika',
    '6015' => 'Bolivia',
    '6016' => 'Australië',
    '6017' => 'Jamaica',
    '6018' => 'Luxemburg',
    '6019' => 'Tsjaad',
    '6020' => 'Mauritanië',
    '6021' => 'Kyrgyzstan',
    '6022' => 'China',
    '6023' => 'Afganistan',
    '6024' => 'Indonesië',
    '6025' => 'Guyana',
    '6026' => 'Noordviëtnam',
    '6027' => 'Noorwegen',
    '6028' => 'San Marino',
    '6029' => 'Duitsland',
    '6030' => 'Nederland',
    '6031' => 'Kambodja',
    '6032' => 'Fiji',
    '6033' => 'Bahama-eilanden',
    '6034' => 'Israël',
    '6035' => 'Nepal',
    '6036' => 'Zuidkorea',
    '6037' => 'Spanje',
    '6038' => 'Oekraine',
    '6039' => 'Grootbrittannië',
    '6040' => 'Niger',
    '6041' => 'Haïti',
    '6042' => 'Jordanië',
    '6043' => 'Turkije',
    '6044' => 'Trinidad en Tobago',
    '6045' => 'Joegoslavië',
    '6046' => 'Bovenvolta',
    '6047' => 'Algerije',
    '6048' => 'Gabon',
    '6049' => 'Noordkorea',
    '6050' => 'Oezbekistan',
    '6051' => 'Sierra Leone',
    '6052' => 'Brits Honduras',
    '6053' => 'Canarische eilanden',
    '6054' => 'Frans Polynesië',
    '6055' => 'Gibraltar',
    '6056' => 'Portugees Timor',
    '6057' => 'Tadzjikistan',
    '6058' => 'Britse Salomons-eilanden',
    '6059' => 'São Tomé en Principe',
    '6060' => 'Sint Helena',
    '6061' => 'Tristan Da Cunha',
    '6062' => 'Westsamoa',
    '6063' => 'Toerkmenistan',
    '6064' => 'Georgië',
    '6065' => 'Bosnië-Herzegovina',
    '6066' => 'Tsjechië',
    '6067' => 'Slowakije',
    '6068' => 'Federale Republiek Joegoslavië',
    '6069' => 'Democratische Republiek Congo',
    '7001' => 'Uganda',
    '7002' => 'Kenya',
    '7003' => 'Malta',
    '7004' => 'Barbados',
    '7005' => 'Andorra',
    '7006' => 'Mexico',
    '7007' => 'Costa Rica',
    '7008' => 'Gambia',
    '7009' => 'Syrië',
    '7011' => 'Nederlandse Antillen',
    '7012' => 'Zuidjemen',
    '7014' => 'Egypte',
    '7015' => 'Argentinië',
    '7016' => 'Lesotho',
    '7017' => 'Honduras',
    '7018' => 'Nicaragua',
    '7020' => 'Pakistan',
    '7021' => 'Senegal',
    '7023' => 'Dahomey',
    '7024' => 'Bulgarije',
    '7026' => 'Maleisië',
    '7027' => 'Dominicaanse Republiek',
    '7028' => 'Polen',
    '7029' => 'Rusland (oud)',
    '7030' => 'Britse Maagdeneilanden',
    '7031' => 'Tanzania',
    '7032' => 'El Salvador',
    '7033' => 'Sri Lanka',
    '7034' => 'Soedan',
    '7035' => 'Japan',
    '7036' => 'Hongkong',
    '7037' => 'Panama',
    '7038' => 'Uruguay',
    '7039' => 'Ecuador',
    '7040' => 'Guinee',
    '7041' => 'Maldiven',
    '7042' => 'Thailand',
    '7043' => 'Libanon',
    '7044' => 'Italië',
    '7045' => 'Koeweit',
    '7046' => 'India',
    '7047' => 'Roemenië',
    '7048' => 'Tsjechoslowakije',
    '7049' => 'Peru',
    '7050' => 'Portugal',
    '7051' => 'Oman',
    '7052' => 'Mongolië',
    '7054' => 'Verenigde Arabische Emiraten',
    '7055' => 'Tibet',
    '7057' => 'Nauru',
    '7058' => 'Nederlands Nieuwguinea',
    '7059' => 'Tanganyika',
    '7060' => 'Palestina',
    '7062' => 'Brits Westindië',
    '7063' => 'Portugees Afrika',
    '7064' => 'Letland',
    '7065' => 'Estland',
    '7066' => 'Litouwen',
    '7067' => 'Brits Afrika',
    '7068' => 'Belgisch Congo',
    '7070' => 'Brits Indië',
    '7071' => 'Noordrhodesië',
    '7072' => 'Zuidrhodesië',
    '7073' => 'Saarland',
    '7074' => 'Frans Indo China',
    '7075' => 'Brits Westborneo',
    '7076' => 'Goudkust',
    '7077' => 'Ras-El-Cheima',
    '7079' => 'Frans Congo',
    '7080' => 'Siam',
    '7082' => 'Brits Oostafrika',
    '7083' => 'Brits Noordborneo',
    '7084' => 'Bangladesh',
    '7085' => 'Duitse Democratische Republiek',
    '7087' => 'Madeira-eilanden',
    '7088' => 'Amerikaanse Maagdeneilanden',
    '7089' => 'Australische Salomonseilanden',
    '7091' => 'Spaanse Sahara',
    '7092' => 'Caymaneilanden',
    '7093' => 'Caicoseilanden',
    '7094' => 'Turkseilanden',
    '7095' => 'Brits Territorium in Antarctica',
    '7096' => 'Brits Territorium in de Indische Oceaan',
    '7097' => 'Cookeilanden',
    '7098' => 'Tokelau-eilanden',
    '7099' => 'Nieuwcaledonië',
    '8000' => 'Hawaii-eilanden',
    '8001' => 'Guam',
    '8002' => 'Amerikaans Samoa',
    '8003' => 'Midway',
    '8004' => 'Ryukyueilanden',
    '8005' => 'Wake',
    '8006' => 'Pacific eilanden',
    '8008' => 'Grenada',
    '8009' => 'Marianen',
    '8010' => 'Cabinda',
    '8011' => 'Canton en Enderbury',
    '8012' => 'Christmaseiland',
    '8013' => 'Cocoseilanden',
    '8014' => 'Faeröer',
    '8015' => 'Montserrat',
    '8016' => 'Norfolk',
    '8017' => 'Belize',
    '8018' => 'Tasmanië',
    '8019' => 'Turks- en Caicoseilanden',
    '8020' => 'Puerto Rico',
    '8021' => 'Papua-Nieuwguinea',
    '8022' => 'Solomoneilanden',
    '8023' => 'Benin',
    '8024' => 'Viëtnam',
    '8025' => 'Kaapverdië',
    '8026' => 'Seychellen',
    '8027' => 'Kiribati',
    '8028' => 'Tuvalu',
    '8029' => 'Sint Lucia',
    '8030' => 'Dominica',
    '8031' => 'Zimbabwe',
    '8032' => 'Doebai',
    '8033' => 'Nieuwehebriden',
    '8034' => 'Kanaaleilanden',
    '8035' => 'Man',
    '8036' => 'Anguilla',
    '8037' => 'Saint Kitts-Nevis',
    '8038' => 'Antigua',
    '8039' => 'Sint Vincent',
    '8040' => 'Gilberteilanden',
    '8041' => 'Panamakanaalzone',
    '8042' => 'Saint Kitts-Nevis-Anguilla',
    '8043' => 'Belau',
    '8044' => 'Republiek van Palau',
    '8045' => 'Antigua en Barbuda',
    '9000' => 'Newfoundland',
    '9001' => 'Nyasaland',
    '9003' => 'Eritrea',
    '9005' => 'Ifni',
    '9006' => 'Brits Kameroen',
    '9007' => 'Kaiser Wilhelmsland',
    '9008' => 'Kongo',
    '9009' => 'Kongo Kinshasa',
    '9010' => 'Madagaskar',
    '9013' => 'Kongo Brazzaville',
    '9014' => 'Leewardeilanden',
    '9015' => 'Windwardeilanden',
    '9016' => 'Frans gebied van Afars en Issa\'s',
    '9017' => 'Phoenixeilanden',
    '9020' => 'Portugees Guinee',
    '9022' => 'Duits Zuidwestafrika',
    '9023' => 'Namibië',
    '9027' => 'Brits Somaliland',
    '9028' => 'Italiaans Somaliland',
    '9030' => 'Nederlands Indië',
    '9031' => 'Brits Guyana',
    '9036' => 'Swaziland',
    '9037' => 'Katar',
    '9041' => 'Aden',
    '9042' => 'Zuidarabische Federatie',
    '9043' => 'Equatoriaalguinee',
    '9044' => 'Spaans Guinee',
    '9047' => 'Verenigde Arabische Republiek',
    '9048' => 'Bermuda',
    '9049' => 'Sovjetunie',
    '9050' => 'Duits Oostafrika',
    '9051' => 'Zanzibar',
    '9052' => 'Ceylon',
    '9053' => 'Muscat en Oman',
    '9054' => 'Trucial Oman',
    '9055' => 'Indo China',
    '9056' => 'Marshalleilanden',
    '9057' => 'Sarawak',
    '9058' => 'Brits Borneo',
    '9060' => 'Sabah',
    '9061' => 'Aboe Dhabi',
    '9062' => 'Adjman',
    '9063' => 'Basoetoland',
    '9064' => 'Bechuanaland',
    '9065' => 'Foedjaira',
    '9066' => 'Frans Kameroen',
    '9067' => 'Johore',
    '9068' => 'Korea',
    '9069' => 'Labuan',
    '9070' => 'Oem el Koewein',
    '9071' => 'Oostenrijk-Hongarije',
    '9072' => 'Portugees Oost Afrika',
    '9073' => 'Portugees West Afrika',
    '9074' => 'Sjardja',
    '9075' => 'Straits Settlements',
    '9076' => 'Abessinië',
    '9077' => 'Frans West Afrika',
    '9078' => 'Frans Equatoriaal Afrika',
    '9081' => 'Oeroendi',
    '9082' => 'Roeanda-Oeroendi',
    '9084' => 'Goa',
    '9085' => 'Dantzig',
    '9086' => 'Centrafrika',
    '9087' => 'Djibouti',
    '9088' => 'Transjordanië',
    '9089' => 'Bondsrepubliek Duitsland',
    '9090' => 'Vanuatu',
    '9091' => 'Niue',
    '9092' => 'Spaans Noordafrika',
    '9093' => 'Westelijke Sahara',
    '9094' => 'Micronesia',
    '9095' => 'Svalbardeilanden',
    '9999' => 'Internationaal gebied',
};

use constant RGBZ_GEMEENTECODES => {
    3 => "Appingedam",
    5 => "Bedum",
    7 => "Bellingwedde",
    9 => "Ten Boer",
    10 => "Delfzijl",
    14 => "Groningen",
    15 => "Grootegast",
    17 => "Haren",
    18 => "Hoogezand-Sappemeer",
    22 => "Leek",
    24 => "Loppersum",
    25 => "Marum",
    34 => "Almere",
    37 => "Stadskanaal",
    39 => "Scheemda",
    40 => "Slochteren",
    47 => "Veendam",
    48 => "Vlagtwedde",
    50 => "Zeewolde",
    51 => "Skarsterlân",
    52 => "Winschoten",
    53 => "Winsum",
    55 => "Boarnsterhim",
    56 => "Zuidhorn",
    58 => "Dongeradeel",
    59 => "Achtkarspelen",
    60 => "Ameland",
    63 => "het Bildt",
    64 => "Bolsward",
    1891 => "Dantumadiel",
    70 => "Franekeradeel",
    72 => "Harlingen",
    74 => "Heerenveen",
    79 => "Kollumerland en Nieuwkruisland",
    80 => "Leeuwarden",
    81 => "Leeuwarderadeel",
    82 => "Lemsterland",
    83 => "Menaldumadeel",
    85 => "Ooststellingwerf",
    86 => "Opsterland",
    88 => "Schiermonnikoog",
    90 => "Smallingerland",
    91 => "Sneek",
    93 => "Terschelling",
    96 => "Vlieland",
    98 => "Weststellingwerf",
    104 => "Nijefurd",
    106 => "Assen",
    109 => "Coevorden",
    114 => "Emmen",
    118 => "Hoogeveen",
    119 => "Meppel",
    140 => "Littenseradiel",
    141 => "Almelo",
    147 => "Borne",
    148 => "Dalfsen",
    150 => "Deventer",
    153 => "Enschede",
    158 => "Haaksbergen",
    160 => "Hardenberg",
    163 => "Hellendoorn",
    164 => "Hengelo",
    166 => "Kampen",
    168 => "Losser",
    171 => "Noordoostpolder",
    173 => "Oldenzaal",
    175 => "Ommen",
    177 => "Raalte",
    180 => "Staphorst",
    183 => "Tubbergen",
    184 => "Urk",
    189 => "Wierden",
    193 => "Zwolle",
    196 => "Rijnwaarden",
    197 => "Aalten",
    200 => "Apeldoorn",
    202 => "Arnhem",
    203 => "Barneveld",
    209 => "Beuningen",
    213 => "Brummen",
    214 => "Buren",
    216 => "Culemborg",
    221 => "Doesburg",
    222 => "Doetinchem",
    225 => "Druten",
    226 => "Duiven",
    228 => "Ede",
    230 => "Elburg",
    232 => "Epe",
    233 => "Ermelo",
    236 => "Geldermalsen",
    1586 => "Oost Gelre",
    241 => "Groesbeek",
    243 => "Harderwijk",
    244 => "Hattem",
    246 => "Heerde",
    252 => "Heumen",
    262 => "Lochem",
    263 => "Maasdriel",
    265 => "Millingen aan de Rijn",
    267 => "Nijkerk",
    268 => "Nijmegen",
    269 => "Oldebroek",
    273 => "Putten",
    274 => "Renkum",
    275 => "Rheden",
    277 => "Rozendaal",
    279 => "Scherpenzeel",
    281 => "Tiel",
    282 => "Ubbergen",
    285 => "Voorst",
    289 => "Wageningen",
    293 => "Westervoort",
    294 => "Winterswijk",
    296 => "Wijchen",
    297 => "Zaltbommel",
    299 => "Zevenaar",
    301 => "Zutphen",
    302 => "Nunspeet",
    303 => "Dronten",
    304 => "Neerijnen",
    305 => "Abcoude",
    307 => "Amersfoort",
    308 => "Baarn",
    310 => "De Bilt",
    311 => "Breukelen",
    312 => "Bunnik",
    313 => "Bunschoten",
    1581 => "Utrechtse Heuvelrug",
    317 => "Eemnes",
    321 => "Houten",
    327 => "Leusden",
    329 => "Loenen",
    331 => "Lopik",
    333 => "Maarssen",
    335 => "Montfoort",
    339 => "Renswoude",
    340 => "Rhenen",
    342 => "Soest",
    344 => "Utrecht",
    345 => "Veenendaal",
    351 => "Woudenberg",
    352 => "Wijk bij Duurstede",
    353 => "IJsselstein",
    355 => "Zeist",
    356 => "Nieuwegein",
    358 => "Aalsmeer",
    361 => "Alkmaar",
    362 => "Amstelveen",
    363 => "Amsterdam",
    364 => "Andijk",
    365 => "Graft-De Rijp",
    366 => "Anna Paulowna",
    370 => "Beemster",
    373 => "Bergen (NH.)",
    375 => "Beverwijk",
    376 => "Blaricum",
    377 => "Bloemendaal",
    381 => "Bussum",
    383 => "Castricum",
    384 => "Diemen",
    385 => "Edam-Volendam",
    388 => "Enkhuizen",
    392 => "Haarlem",
    393 => "Haarlemmerliede en Spaarnwoude",
    394 => "Haarlemmermeer",
    395 => "Harenkarspel",
    396 => "Heemskerk",
    397 => "Heemstede",
    398 => "Heerhugowaard",
    399 => "Heiloo",
    400 => "Den Helder",
    402 => "Hilversum",
    405 => "Hoorn",
    406 => "Huizen",
    412 => "Niedorp",
    415 => "Landsmeer",
    416 => "Langedijk",
    417 => "Laren",
    424 => "Muiden",
    425 => "Naarden",
    431 => "Oostzaan",
    432 => "Opmeer",
    437 => "Ouder-Amstel",
    439 => "Purmerend",
    441 => "Schagen",
    448 => "Texel",
    450 => "Uitgeest",
    451 => "Uithoorn",
    453 => "Velsen",
    457 => "Weesp",
    458 => "Schermer",
    459 => "Wervershoof",
    462 => "Wieringen",
    463 => "Wieringermeer",
    420 => "Medemblik",
    473 => "Zandvoort",
    476 => "Zijpe",
    478 => "Zeevang",
    479 => "Zaanstad",
    482 => "Alblasserdam",
    484 => "Alphen aan den Rijn",
    489 => "Barendrecht",
    491 => "Bergambacht",
    1621 => "Lansingerland",
    497 => "Bodegraven",
    498 => "Drechterland",
    499 => "Boskoop",
    501 => "Brielle",
    502 => "Capelle aan den IJssel",
    503 => "Delft",
    504 => "Dirksland",
    505 => "Dordrecht",
    511 => "Goedereede",
    512 => "Gorinchem",
    513 => "Gouda",
    518 => "'s-Gravenhage",
    523 => "Hardinxveld-Giessendam",
    530 => "Hellevoetsluis",
    531 => "Hendrik-Ido-Ambacht",
    532 => "Stede Broec",
    534 => "Hillegom",
    537 => "Katwijk",
    542 => "Krimpen aan den IJssel",
    545 => "Leerdam",
    546 => "Leiden",
    547 => "Leiderdorp",
    553 => "Lisse",
    556 => "Maassluis",
    1598 => "Koggenland",
    559 => "Middelharnis",
    563 => "Moordrecht",
    567 => "Nieuwerkerk aan den IJssel",
    568 => "Bernisse",
    569 => "Nieuwkoop",
    571 => "Nieuw-Lekkerland",
    575 => "Noordwijk",
    576 => "Noordwijkerhout",
    579 => "Oegstgeest",
    580 => "Oostflakkee",
    584 => "Oud-Beijerland",
    585 => "Binnenmaas",
    588 => "Korendijk",
    589 => "Oudewater",
    590 => "Papendrecht",
    595 => "Reeuwijk",
    597 => "Ridderkerk",
    599 => "Rotterdam",
    600 => "Rozenburg",
    603 => "Rijswijk",
    1525 => "Teylingen",
    606 => "Schiedam",
    608 => "Schoonhoven",
    610 => "Sliedrecht",
    611 => "Cromstrijen",
    612 => "Spijkenisse",
    613 => "Albrandswaard",
    614 => "Westvoorne",
    617 => "Strijen",
    620 => "Vianen",
    622 => "Vlaardingen",
    623 => "Vlist",
    626 => "Voorschoten",
    627 => "Waddinxveen",
    629 => "Wassenaar",
    632 => "Woerden",
    637 => "Zoetermeer",
    1884 => "Kaag en Braassem",
    638 => "Zoeterwoude",
    642 => "Zwijndrecht",
    643 => "Nederlek",
    644 => "Ouderkerk",
    653 => "Gaasterlân-Sleat",
    654 => "Borsele",
    664 => "Goes",
    668 => "West Maas en Waal",
    677 => "Hulst",
    678 => "Kapelle",
    683 => "Wymbritseradiel",
    687 => "Middelburg",
    689 => "Giessenlanden",
    693 => "Graafstroom",
    694 => "Liesveld",
    703 => "Reimerswaal",
    707 => "Zederik",
    710 => "Wûnseradiel",
    715 => "Terneuzen",
    716 => "Tholen",
    717 => "Veere",
    718 => "Vlissingen",
    733 => "Lingewaal",
    736 => "De Ronde Venen",
    737 => "Tytsjerksteradiel",
    738 => "Aalburg",
    743 => "Asten",
    744 => "Baarle-Nassau",
    748 => "Bergen op Zoom",
    753 => "Best",
    755 => "Boekel",
    756 => "Boxmeer",
    757 => "Boxtel",
    758 => "Breda",
    762 => "Deurne",
    765 => "Pekela",
    766 => "Dongen",
    770 => "Eersel",
    772 => "Eindhoven",
    777 => "Etten-Leur",
    779 => "Geertruidenberg",
    784 => "Gilze en Rijen",
    785 => "Goirle",
    786 => "Grave",
    788 => "Haaren",
    794 => "Helmond",
    796 => "'s-Hertogenbosch",
    797 => "Heusden",
    798 => "Hilvarenbeek",
    808 => "Lith",
    809 => "Loon op Zand",
    815 => "Mill en Sint Hubert",
    820 => "Nuenen, Gerwen en Nederwetten",
    823 => "Oirschot",
    824 => "Oisterwijk",
    826 => "Oosterhout",
    828 => "Oss",
    840 => "Rucphen",
    844 => "Schijndel",
    845 => "Sint-Michielsgestel",
    846 => "Sint-Oedenrode",
    847 => "Someren",
    848 => "Son en Breugel",
    851 => "Steenbergen",
    852 => "Waterland",
    855 => "Tilburg",
    856 => "Uden",
    858 => "Valkenswaard",
    860 => "Veghel",
    861 => "Veldhoven",
    865 => "Vught",
    866 => "Waalre",
    867 => "Waalwijk",
    870 => "Werkendam",
    873 => "Woensdrecht",
    874 => "Woudrichem",
    879 => "Zundert",
    880 => "Wormerland",
    881 => "Onderbanken",
    882 => "Landgraaf",
    885 => "Arcen en Velden",
    888 => "Beek",
    889 => "Beesel",
    893 => "Bergen (L.)",
    899 => "Brunssum",
    905 => "Eijsden",
    907 => "Gennep",
    1640 => "Leudal",
    917 => "Heerlen",
    918 => "Helden",
    928 => "Kerkrade",
    929 => "Kessel",
    934 => "Maasbree",
    935 => "Maastricht",
    936 => "Margraten",
    938 => "Meerssen",
    941 => "Meijel",
    944 => "Mook en Middelaar",
    946 => "Nederweert",
    951 => "Nuth",
    957 => "Roermond",
    962 => "Schinnen",
    964 => "Sevenum",
    965 => "Simpelveld",
    971 => "Stein",
    981 => "Vaals",
    983 => "Venlo",
    984 => "Venray",
    986 => "Voerendaal",
    988 => "Weert",
    993 => "Meerlo-Wanssum",
    994 => "Valkenburg aan de Geul",
    995 => "Lelystad",
    1507 => "Horst aan de Maas",
    1509 => "Oude IJsselstreek",
    1651 => "Eemsmond",
    1652 => "Gemert-Bakel",
    1655 => "Halderberge",
    1658 => "Heeze-Leende",
    1659 => "Laarbeek",
    1661 => "Reiderland",
    1663 => "De Marne",
    1666 => "Zevenhuizen-Moerkapelle",
    1667 => "Reusel-De Mierden",
    1669 => "Roerdalen",
    1671 => "Maasdonk",
    1672 => "Rijnwoude",
    1674 => "Roosendaal",
    1676 => "Schouwen-Duiveland",
    1680 => "Aa en Hunze",
    1681 => "Borger-Odoorn",
    1684 => "Cuijk",
    1685 => "Landerd",
    1690 => "De Wolden",
    1695 => "Noord-Beveland",
    1696 => "Wijdemeren",
    1699 => "Noordenveld",
    1700 => "Twenterand",
    1701 => "Westerveld",
    1702 => "Sint Anthonis",
    1705 => "Lingewaard",
    1706 => "Cranendonck",
    1708 => "Steenwijkerland",
    1709 => "Moerdijk",
    1711 => "Echt-Susteren",
    1714 => "Sluis",
    1719 => "Drimmelen",
    1721 => "Bernheze",
    1722 => "Ferwerderadiel",
    1723 => "Alphen-Chaam",
    1724 => "Bergeijk",
    1728 => "Bladel",
    1729 => "Gulpen-Wittem",
    1730 => "Tynaarlo",
    1731 => "Midden-Drenthe",
    1734 => "Overbetuwe",
    1735 => "Hof van Twente",
    1740 => "Neder-Betuwe",
    1742 => "Rijssen-Holten",
    1771 => "Geldrop-Mierlo",
    1773 => "Olst-Wijhe",
    1774 => "Dinkelland",
    1783 => "Westland",
    1842 => "Midden-Delfland",
    1859 => "Berkelland",
    1876 => "Bronckhorst",
    1883 => "Sittard-Geleen",
    1896 => "Zwartewaterland",
    1916 => "Leidschendam-Voorburg",
    1926 => "Pijnacker-Nootdorp",
    1641 => "Maasgouw",
    1955 => "Montferland",
    1987 => "Menterwolde",
};

use constant STUF_SUBSCRIPTION_VIEWS => {
    subscriptions_without_cases             => 'Personen zonder zaken',
    subscriptions_without_cases_since_1d    => 'Personen zonder zaken (persoon minimaal 1 dag geleden geimporteerd)',
    subscriptions_with_cases                => 'Personen met zaken',
    subscriptions_outside_municipality_without_cases    => 'Buitengemeentelijke personen zonder zaken',
    subscriptions_outside_municipality_with_cases       => 'Buitengemeentelijke personen met zaken',
};

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 CHILD_CASETYPE_OPTIONS

TODO: Fix the POD

=cut

=head2 SJABLONEN_EXPORT_FORMATS

TODO: Fix the POD

=cut

=head2 VALIDATION_RELATEREN_PROFILE

TODO: Fix the POD

=cut

=head2 MIMETYPES_ALLOWED

TODO: Fix the POD

=cut
