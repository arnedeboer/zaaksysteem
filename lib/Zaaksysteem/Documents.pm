package Zaaksysteem::Documents;

use strict;
use warnings;
use Data::Dumper;
use File::Copy;
use Digest::MD5::File qw/-nofatals file_md5_hex/;
use Text::Wrap;
use File::stat;
use File::MMagic;

use DateTime;
use Zaaksysteem::Constants;

use Moose;


has 'context' => (
    'is'    => 'rw',
);


{
    Zaaksysteem->register_profile(
        method  => 'list',
        profile => {
            'required'      => [ qw/
            /],
            'optional'      => [ qw/
                zaaktype_kenmerken_id
                pid
                search
                pip
                search_recursive

                queue
                all_documents
                id
                alleen_mappen
            /],
            'defaults'      => {
                pid     => undef,
            },
            'require_some'  => {
                'zaak_id_or_betrokkene' => [1, qw/zaak_id betrokkene/],
            }
        }
    );

    sub list {
        my ($self, $opts)   = @_;
        my $sargs           = {};

        my $dv              = $self->context->check(
            params  => $opts,
            method  => [caller(0)]->[3],
        );

        return unless $dv->success;

        if ($dv->valid('zaak_id')) {
            $sargs->{zaak_id}       = $dv->valid('zaak_id');
        } elsif ($dv->valid('betrokkene')) {
            $sargs->{betrokkene}    = $dv->valid('betrokkene');
        }

        if ($dv->valid('zaaktype_kenmerken_id')) {
            $sargs->{zaaktype_kenmerken_id} =
                $dv->valid('zaaktype_kenmerken_id')
        }

        $sargs->{pid}   = $dv->valid('pid') unless
            $dv->valid('search_recursive');

        if ($dv->valid('pip')) {
            $sargs->{'-nest'} = ['pip' => '1', mimetype => 'dir'];
        }

        if (!$dv->valid('all_documents')) {
            $sargs->{queue} = undef;
            $sargs->{queue} = 1 if $dv->valid('queue');
        }

        if ($dv->valid('id')) {
            $sargs->{id} = $dv->valid('id');
        }

        if ($dv->valid('alleen_mappen')) {
            $sargs->{mimetype} = 'dir';
        }

        return
            $self->context->model('DB::Documents')->search(
                {
                    %{ $sargs },
                    'deleted_on'    => undef,
                },
                {
                    'order_by'  => { -asc => [qw/documenttype filename/] }
                },
        );
    }
}

{
    Zaaksysteem->register_profile(
        method  => 'add',
        profile => {
            'required'      => [ qw/
                documenttype
                betrokkene_id
            /],
            'optional'      => [ qw/
                filename
                zaakstatus
                pid
                document_id
                filestore_id
            /],
            'constraint_methods'    => {
                'documenttype'  => sub {
                    my $val = pop;

                    if (
                        ZAAKSYSTEEM_CONSTANTS->{document}->{types}
                            ->{ $val }
                    ) {
                        return 1;
                    }

                    return;
                },
                'zaakstatus'    => qr/^\d+$/,
            },
            'defaults'      => {
                pid     => undef,
            },
            'require_some'  => {
                'zaak_id_or_betrokkene' => [1, qw/zaak_id betrokkene/],
            }
        }
    );

    sub add {
        my ($self, $opts, $upload)  = @_;
        $opts                       = $opts || {};

        my $dv                      = $self->context->check(
            params  => $opts,
            method  => [caller(0)]->[3],
        );

        unless ($dv->success) {
            $self->context->log->error('DOCUMENT VALUES NOT OK:' .
                Dumper($dv)
            );

            return;
        }

        my $add_function = '_add_' . $dv->valid('documenttype');
        return $self->$add_function($opts, $upload);
    }
}

sub _add_dir {
    my ($self, $opts, $upload)  = @_;
    my $cargs                   = {};

    if ($opts->{zaak_id}) {
        $cargs->{zaak_id}       = $opts->{zaak_id};
    } else {
        $cargs->{betrokkene}    = $opts->{betrokkene};
    }

    $cargs->{filename}          = $opts->{filename};
    $cargs->{pid}               = $opts->{pid};
    $cargs->{zaakstatus}        = $opts->{zaakstatus};

    if ( $self->context->model('DB::Documents')->search($cargs)->count) {
        $self->context->push_flash_message('Mapnaam bestaat al');
        $self->context->log->debug('Directory exists');
        return;
    }

    my ($document);
    $cargs->{'mimetype'}        = 'dir';

    unless (
        $document = $self->context->model('DB::Documents')->create({
            %{ $cargs }
        })
    ) {
        $self->context->push_flash_message('Probleem bij aanmaken map');
        return;
    }

    $self->context->push_flash_message('Map succesvol aangemaakt');

    return $document;
}


{
    Zaaksysteem->register_profile(
        method  => '_add_file',
        profile => {
            'required'      => [ qw/
                documenttype
                betrokkene_id
            /],
            'optional'      => [ qw/
                category
                filename
                document_id
                zaakstatus
                pid
                catalogus
                verplicht
                post_registratie
                pip
                help
                ontvangstdatum
                dagtekeningdatum
                versie
                queue
                edited_filename
                actie_rename_when_exists
                filestore_id
            /],
            'constraint_methods'    => {
                'documenttype'  => sub {
                    my $val = pop;

                    if (
                        ZAAKSYSTEEM_CONSTANTS->{document}->{types}
                            ->{ $val }
                    ) {
                        return 1;
                    }

                    return;
                },
                'zaakstatus'    => qr/^\d+$/,
            },
            'defaults'      => {
                pid     => undef,
            },
            'require_some'  => {
                'zaak_id_or_betrokkene' => [1, qw/zaak_id betrokkene/],
                'document_id_or_filename' => [1, qw/document_id filename/],
            }
        }
    );

    sub _add_file {
        my ($self, $opts, $upload)  = @_;

        my $dv = $self->context->check(
            params  => $opts,
            method  => [caller(0)]->[3],
        );
        my $saved_document_id = $dv->valid('document_id');

        unless ($dv->success) {
            $self->context->log('Incorrect parameters for Documents::_add_file: ' . Dumper $dv);
            return;
        }

        my $cargs = $dv->valid;

        ### Delete extra BI vars not for database

        my $result = $self->_check_file_exists({
            dv      => $dv,
            cargs   => $cargs
        });

        if($result->{result} eq 'exists') {
            return;
        } elsif($result->{result} eq 'renamed') {
            $cargs->{filename} = $result->{filename};
        }

        delete($cargs->{actie_rename_when_exists});

        my $files_dir = $self->context->config->{files} . '/documents';

        if (!$upload && !$dv->valid('document_id')) {
            $self->context->push_flash_message('Probleem bij het uploaden van document, contact systeembeheer');
            $self->context->log->error('Problem uploading, no upload object given');
            return;
        }

        my $olddoc;
        if ($upload) {
            $cargs->{'mimetype'}        = $upload->type;
            $cargs->{'filesize'}        = $upload->size;
        } else {
            return unless $dv->valid('document_id');
        }

        if ($dv->valid('document_id')) {
            $olddoc  = $self->context->model('DB::Documents')->find(
                $dv->valid('document_id')
            );

            return unless $olddoc;

            if (!$upload) {
                $cargs->{'mimetype'}        = $olddoc->mimetype;
                $cargs->{'filesize'}        = $olddoc->filesize;
                $cargs->{'filename'}        = $cargs->{'edited_filename'};
                $cargs->{'documenttype'}    = $olddoc->documenttype;
            }

            $cargs->{'versie'}              = ($olddoc->versie + 1);
        }

        # the field 'edited_filename' is passed to allow override of filename
        delete $cargs->{'edited_filename'} if(exists $cargs->{'edited_filename'});

        $cargs->{'versie'}                  = $cargs->{versie} || 1;

        if ($cargs->{'catalogus'} && $cargs->{'zaak_id'}) {
            my $z_object        = $self->context->model('DB::Zaak')->find($cargs->{'zaak_id'});

            $self->context->log->debug('M::D->add_file: search kenmerk in'
                . 'kenmerken bibliotheek'
            );
            my $file_kenmerken  = $z_object->zaaktype_node_id->zaaktype_kenmerken->search(
                {
                    'me.id'                                 => $cargs->{'catalogus'},
                },
            );

            if ($file_kenmerken->count == 1) {
                $self->context->log->debug('M::D->add_file: found kenmerk in'
                    . 'kenmerken bibliotheek'
                );
                my $kdocument = $file_kenmerken->first;
                if ($kdocument->type eq 'file') {
                    $cargs->{'verplicht'}      =
                        $kdocument->value_mandatory;
                    $cargs->{'category'}       =
                        $kdocument->bibliotheek_kenmerken_id->document_categorie;
                    $cargs->{'pip'}       = $kdocument->pip;
                }

                $cargs->{'zaaktype_kenmerken_id'}   = $cargs->{catalogus};
            }

        }

        if ($dv->valid('queue')) {
            $cargs->{queue} = $dv->valid('queue');
        } else {
            $cargs->{queue} = undef;
        }

        $self->context->log->debug('M::D->add_file: going to create'
        #    . Dumper($cargs)
        );

        my $document;

        delete($cargs->{document_id});
        my $filestore_id = $cargs->{filestore_id};
        delete $cargs->{filestore_id};

        $self->context->log->debug("upload2: [" . ref ($upload) . ']');

        $self->context->model("DB")->txn_do(sub {
            eval {

                if($upload) {
                    my $validator = $self->context->model('DocumentValidator');
                    my $determined_mimetype;

                    # this function can be called from a different stage.
                    if(ref $upload eq 'Catalyst::Request::Upload' && -e $upload->tempname) {
                        $determined_mimetype = $validator->validate_upload({
                            filename => $cargs->{filename},
                            upload => $upload,
                        });
                    } else {
                        $determined_mimetype = $upload->type;
                    }

                    unless($determined_mimetype) {
                        $self->context->push_flash_message('Fout: Het bestandsformaat van dit document is helaas niet toegestaan vanwege digitale duurzaamheid');
                        $self->context->log->debug('Documents.pm: Document rejected by NEN validation rules');
                        die "Document rejected by documentvalidator";
                    }

                    # the documentvalidator gets to decide the mimetype for a document.
                    $cargs->{mimetype} = $determined_mimetype;
                }
                #my $document_id = $dv->valid('document_id');

                my $insert_params = {
                    cargs           => $cargs,
                    upload          => $upload,
                    filestore_id    => $filestore_id,
                    files_dir       => $files_dir,
                    document_id     => $saved_document_id,
                };
                $self->context->log->debug('Inserting Document: ' .
                    Dumper($insert_params));

                $document = $self->_insert_document($insert_params);
            };

            if ($@) {
                warn "Could not create document: " . $@;
            }
        });


        if($document) {

            ### Make old version disappear
            if ($olddoc) {
                $olddoc->deleted_on(DateTime->now());
                $olddoc->update;
            }

            ### Logging
            if ($document->zaak_id) {
                $document->zaak_id->logging->add({
                    'component'     => LOGGING_COMPONENT_DOCUMENT,
                    'component_id'  => $document->id,
                    'onderwerp'     => 'Document "'
                        . $document->filename . '" ['
                        . $document->id . '] succesvol '
                        . ($olddoc
                            ? 'bewerkt[' . $olddoc->id .']'
                            : 'aangemaakt'
                        )
                });
            }

            $self->context->push_flash_message('Document succesvol aangemaakt');
        }
        return $document;
    }
}


sub _check_file_exists {
    my ($self, $args) = @_;

    my $cargs   = $args->{cargs};
    my $dv      = $args->{dv};

    my $checkargs = {
        'filename'  => $cargs->{filename},
        'pid'       => $cargs->{pid},
        'deleted_on' => undef,
    };

    ### Rename file to something sane
    $checkargs->{filename}          =~ s/[^\w_\-\.=,\s]/_/g;

    if($cargs->{edited_filename}) {
        $cargs->{edited_filename}       =~ s/[^\w_\-\.=,\s]/_/g;
    }

    if ($cargs->{zaak_id}) {
        $checkargs->{zaak_id}       = $cargs->{zaak_id};
    } else {
        $checkargs->{betrokkene}    = $cargs->{betrokkene};
    }

    my $existing_doc = $self->context->model('DB::Documents')->search($checkargs);

    if (
        (
            !$dv->valid('document_id') &&
            $existing_doc->count
        ) ||
        (
            $dv->valid('document_id') &&
            $existing_doc->count &&
            $existing_doc->first->id != $dv->valid('document_id')
        )
    ) {
        if ($cargs->{actie_rename_when_exists}) {
            my $count = 0;
            while (
                $self->context
                    ->model('DB::Documents')
                    ->search($checkargs)->count
            ) {
                my ($file, $ext) = $checkargs->{filename} =~ /(.*)\.(.*)$/;

                $checkargs->{filename} = $file . '_' . ++$count . '.' .  $ext;
            }

            return {
                result      => 'renamed',
                filename    => $checkargs->{filename}
            };
        } else {
            $self->context->push_flash_message('Bestand bestaat al');
            $self->context->log->debug('Filename exists');
            return {
                result => 'exists'
            };
        }
    }
    return {
        result => 'notfound'
    };
}


Params::Profile->register_profile(
    method  => '_insert_document',
    profile => {
        required => [ qw/cargs files_dir/ ],
        optional => [ qw/document_id upload filestore_id/ ],
    }
);
sub _insert_document {
    my ($self, $params) = @_;

    my $dv = Params::Profile->check(params => $params);
    die "invalid options for _insert_document" . Dumper $dv unless $dv->success;

    my $cargs           = $params->{cargs};
    my $upload          = $params->{upload};
    my $filestore_id    = (
        ref($params->{filestore_id})
            ? $params->{filestore_id}->id
            : $params->{filestore_id}
    );

    my $files_dir       = $params->{files_dir};
    my $document_id     = $params->{document_id};

    unless(-d $files_dir) {
        mkdir($files_dir);
    }

    unless(-w $files_dir) {
        $self->context->log("files_dir ({$files_dir}) not writeable by this process");
        $self->context->push_flash_message('Probleem bij het aanmaken van document (map is niet schrijfbaar)');

        return;
    }

    my $document;
    $self->context->log->debug("create args: " . Dumper $cargs);
    unless ( $document = $self->context->model('DB::Documents')->create($cargs) ) {
        $self->context->log("Probleem bij aanmaken document: " . $@);
        $self->context->push_flash_message('Probleem bij aanmaken document');
        return;
    }

    my $success;

    $self->context->log->debug('Created document ' . $document->id);
    if ($upload) {

        my $success = 0;
        if($filestore_id) {
            $success = rename(
                $self->context->config->{files} . '/filestore/' . $filestore_id,
                $files_dir . '/' . $document->id
            );
        } else {
            $success = $upload->copy_to($files_dir . '/' . $document->id);
        }

        unless ($success) {
            $self->context->push_flash_message('Probleem bij aanmaken document');
            $self->context->log->debug('Probleem bij aanmaken document, couldnt move to '.$files_dir.' (1): ' .$document_id . ' - ' . $!
            );
            $document->delete;
            return;
        }

    } else {
        my $source = $files_dir . '/' . $document_id;
        my $destination = $files_dir . '/' . $document->id;

        unless (copy($source, $destination)) {
            my $message = "Could not copy document from $source to $destination: $!";
            $self->context->push_flash_message($message);
            $self->context->log->debug($message);
            $document->delete;
            return;
        }
    }

    ### CREATE MD5 FROM HASH
    my $md5sum = file_md5_hex($files_dir . '/' .  $document->id);

    $self->context->log->debug('file: ' . $files_dir . '/' .  $document->id .'MD5: ' . $md5sum);
    do {
        $self->log->debug("Deleting document because MD5sum couldn't be fixed");
        $document->delete;
        return;
    } unless $md5sum;

    $document->md5($md5sum);
    $document->update;
    return $document;
}



{
    Zaaksysteem->register_profile(
        method  => '_add_mail',
        profile => {
            'required'      => [ qw/
                documenttype
                filename
                message
                betrokkene_id
            /],
            'optional'      => [ qw/
                zaakstatus
                pid
                rcpt
                subject
            /],
            'constraint_methods'    => {
                'zaakstatus'    => qr/^\d+$/,
            },
            'defaults'      => {
                pid     => undef,
            },
            'require_some'  => {
                'zaak_id_or_betrokkene' => [1, qw/zaak_id betrokkene/],
            }
        }
    );

    sub _add_mail {
        my ($self, $opts)           = @_;
        my $cargs                   = {};

        my $dv                      = $self->context->check(
            params  => $opts,
            method  => [caller(0)]->[3],
        );

        return unless $dv->success;

        $cargs                      = $dv->valid;

        ### Mail defaults
        $cargs->{'category'}        = 'Email';
        $cargs->{'mimetype'}        = 'text/email';

        ### WRAP message
        $Text::Wrap::columns = 78;
        $opts->{'message'} = wrap('', '', $opts->{'message'});

        $cargs->{'filesize'}        = length($opts->{'message'});

        my $document;
        my %appends;
        for my $append (qw/subject message rcpt/) {
            $appends{$append} = $cargs->{$append};
            delete($cargs->{$append});
        }


        if ($document = $self->context->model('DB::Documents')->create($cargs)) {
            $self->context->log->debug('Created document for mail ' . $document->id);

            my $mail = $self->context->model('DB::DocumentsMail')->create({
                'document_id'       => $document->id,
                %appends
            });

            ## Fail...
            if (!$mail) {
                $document->delete;
                $self->context->push_flash_message('Probleem bij aanmaken e-mail');
                return;
            }

            $self->context->log->debug('Created mail document ' . $mail->id);
        } else {
            $self->context->push_flash_message(
                'Probleem bij aanmaken document voor e-mail');
            return;
        }

        if ($document->zaak_id) {
            $document->zaak_id->logging->add({
                'component'     => LOGGING_COMPONENT_DOCUMENT,
                'component_id'  => $document->id,
                'onderwerp'     => 'Document "'
                    . $document->filename . '" ['
                    . $document->id . '] succesvol '
                    . 'aangemaakt'
            });
        }


        return $document;
    }
}

{
    Zaaksysteem->register_profile(
        method  => '_add_sjabloon',
        profile => {
            'required'      => [ qw/
                documenttype
                betrokkene_id
                sjabloon_id
            /],
            'optional'      => [ qw/
                category
                filename
                document_id
                zaakstatus
                pid
                catalogus
                verplicht
                post_registratie
                pip
                help
                ontvangstdatum
                dagtekeningdatum
                versie
                actie_rename_when_exists
            /],
            'constraint_methods'    => {
                'documenttype'  => sub {
                    my $val = pop;

                    if (
                        ZAAKSYSTEEM_CONSTANTS->{document}->{types}
                            ->{ $val }
                    ) {
                        return 1;
                    }

                    return;
                },
                'zaakstatus'    => qr/^\d+$/,
            },
            'defaults'      => {
                pid     => undef,
            },
            'require_some'  => {
                'zaak_id_or_betrokkene' => [1, qw/zaak_id betrokkene/],
                'document_id_or_filename' => [1, qw/document_id filename/],
            }
        }
    );

    sub _add_sjabloon {
        my ($self, $opts, $upload)  = @_;
        my $cargs                   = {};

        my $dv                      = $self->context->check(
            params  => $opts,
            method  => [caller(0)]->[3],
        );

        $self->context->log->debug('M::Doc->_add_sjabloon: validate');

        return unless $dv->success;

        $cargs                      = { %{ $dv->valid } };

        delete($cargs->{actie_rename_when_exists});


        my $checkargs = {
            'filename'  => $cargs->{filename},
            'pid'       => $cargs->{pid},
            'deleted_on' => undef,
        };

        if ($cargs->{zaak_id}) {
            $checkargs->{zaak_id}       = $cargs->{zaak_id};
        } else {
            $checkargs->{betrokkene}    = $cargs->{betrokkene};
        }


        $self->context->log->debug('M::Doc->_add_sjabloon: prepare');

        my $existing_doc =
            $self->context->model('DB::Documents')->search($checkargs);

        if (
            (
                !$dv->valid('document_id') &&
                $existing_doc->count
            ) ||
            (
                $dv->valid('document_id') &&
                $existing_doc->count &&
                $existing_doc->first->id != $dv->valid('document_id')
            )
        ) {
            if ($dv->valid('actie_rename_when_exists')) {
                my $count = 0;
                while (
                    $self->context
                        ->model('DB::Documents')
                        ->search($checkargs)->count
                ) {
                    my ($file, $ext) = $checkargs->{filename} =~ /(.*)\.(.*)$/;

                    $checkargs->{filename}  = $file . '_'
                        . ++$count . '.' .  $ext;
                }

                $cargs->{filename} = $checkargs->{filename};
            } else {
                $self->context->push_flash_message('Bestand bestaat al');
                $self->context->log->debug('Filename exists');
                return;
            }
        }

        my $files_dir = $self->context->config->{files} . '/documents';

        my $olddoc;
        if ($dv->valid('document_id')) {
            $olddoc  = $self->context->model('DB::Documents')->find(
                $dv->valid('document_id')
            );

            return unless $olddoc;

            if (!$upload) {
                $cargs->{'mimetype'}        = $olddoc->mimetype;
                $cargs->{'filesize'}        = $olddoc->filesize;
                $cargs->{'filename'}        = $olddoc->filename;
            }

            $cargs->{'versie'}          = ($olddoc->versie + 1);
        }

        if ($cargs->{'catalogus'} && $cargs->{'zaak_id'}) {
            my $z_object        = $self->context->model('DB::Zaak')->find($cargs->{'zaak_id'});
            my $zt_object       = $z_object->zaaktype_node_id;

            $self->context->log->debug('M::D->add_sjabloon: search kenmerk in '
                . 'kenmerken bibliotheek'
            );
            my $file_kenmerken  = $zt_object->zaaktype_kenmerken->search(
                {
                    'me.id'                                 => $cargs->{'catalogus'},
                },
            );

            if ($file_kenmerken->count == 1) {
                $self->context->log->debug('M::D->add_sjabloon: found kenmerk in '
                    . 'kenmerken bibliotheek'
                );
                my $kdocument = $file_kenmerken->first;
                if ($kdocument->type eq 'file') {
                    $cargs->{'verplicht'}      =
                        $kdocument->value_mandatory;
                    $cargs->{'category'}       =
                        $kdocument->bibliotheek_kenmerken_id->document_categorie;
                    $cargs->{'pip'}       = $kdocument->pip;
                }

                $cargs->{'zaaktype_kenmerken_id'}   = $cargs->{catalogus};
            }

        }
        $cargs->{'versie'}                  = $cargs->{versie} || 1;

        my $document;

        $self->context->log->debug('M::Doc->_add_sjabloon: create doc');
        delete($cargs->{sjabloon_id});
        delete($cargs->{document_id});

        if ( $document = $self->context->model('DB::Documents')->create($cargs) ) {
            $self->context->log->debug('Created document ' . $document->id);

            if ($dv->valid('document_id')) {
                $self->context->log->debug('M::Doc->_add_sjabloon: edit sjabloon document');
                unless (copy(
                        $files_dir . '/' . $dv->valid('document_id'),
                        $files_dir . '/' . $document->id
                    )
                ) {
                    $self->context->push_flash_message('Probleem bij aanmaken document');
                    $self->context->log->debug('Probleem bij aanmaken document (3): ' .
                        $dv->valid('document_id') . ' [' . $!
                    );
                    $document->delete;
                    return;
                }
            } else {
                $self->context->log->debug('M::Doc->_add_sjabloon: create sjabloon');
                if (
                    $self->context->model('Bibliotheek::Sjablonen')->create_sjabloon(
                        sjabloon_id => $dv->valid('sjabloon_id'),
                        document_id => $document->id,
                        zaak_nr     => $cargs->{zaak_id}
                    )
                ) {
                    # Define filesize etc
                    if (my $fstat = stat($files_dir . '/' . $document->id)) {
                        my $mm      = new File::MMagic;
                        my $mimetype = $mm->checktype_filename($files_dir . '/' . $document->id);

                        $document->filesize($fstat->size);
                        $document->mimetype('application/vnd.oasis.opendocument.text');

                        $document->update;
                    }
                } else {
                    $self->context->push_flash_message('Probleem bij aanmaken document');
                    $self->context->log->debug('Probleem bij aanmaken document (4): '
                        .$dv->valid('document_id') . ' [' . $! . ']'
                    );
                    $document->delete;
                    return;
                }
            }

            ### CREATE MD5 FROM HASH
            my $md5sum = file_md5_hex($files_dir . '/' .  $document->id);
            $self->context->log->debug('MD5: ' . $md5sum);
            do {
                $document->delete;
                return;
            } unless $md5sum;

            $document->md5($md5sum);
            $document->update;
        } else {
            $self->context->push_flash_message('Probleem bij aanmaken document');
            return;
        }

        ### Make old version disappear
        if ($olddoc) {
            $olddoc->deleted_on(DateTime->now());
            $olddoc->update;
        }

        if ($document->zaak_id) {
            $document->zaak_id->logging->add({
                'component'     => LOGGING_COMPONENT_DOCUMENT,
                'component_id'  => $document->id,
                'onderwerp'     => 'Sjabloon "'
                    . $document->filename . '" ['
                    . $document->id . '] succesvol '
                    . 'gegenereerd'
            });
        }

        $self->context->push_flash_message('Document succesvol aangemaakt');

        return $document;
    }
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 LOGGING_COMPONENT_DOCUMENT

TODO: Fix the POD

=cut

=head2 ZAAKSYSTEEM_CONSTANTS

TODO: Fix the POD

=cut

=head2 add

TODO: Fix the POD

=cut

=head2 list

TODO: Fix the POD

=cut

