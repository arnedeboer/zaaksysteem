package Zaaksysteem::Backend::Filestore::Component;

use strict;
use warnings;

use Digest::MD5::File qw(file_md5_hex);
use File::Temp qw(tempfile);
use File::stat;
use Moose;
use OpenOffice::OODoc;
use Params::Profile;
use Zaaksysteem::Constants;
use Zaaksysteem::Exception qw(throw);
use Zaaksysteem::ZTT;

with 'Zaaksysteem::Roles::UStore';

extends 'DBIx::Class';

use Exception::Class (
    'Zaaksysteem::Backend::Filestore::Component::Exception' => {fields => 'code'},
    'Zaaksysteem::Backend::Filestore::Component::Exception::General' => {
        isa         => 'Zaaksysteem::Backend::Filestore::Component::Exception',
        description => 'General exception',
        alias       => 'throw_general_exception',
    },
    'Zaaksysteem::Backend::Filestore::Component::Exception::Parameter' => {
        isa         => 'Zaaksysteem::Backend::Filestore::Component::Exception',
        description => 'Parameter exception',
        alias       => 'throw_parameter_exception',
    },
    'Zaaksysteem::Backend::Filestore::Component::Exception::Logic' => {
        isa         => 'Zaaksysteem::Backend::Filestore::Component::Exception',
        description => 'Logic exception',
        alias       => 'throw_logic_exception',
    },
    'Zaaksysteem::Backend::Filestore::Component::Exception::ImageMagick' => {
        isa         => 'Zaaksysteem::Backend::Filestore::Component::Exception',
        description => 'ImageMagick error',
        alias       => 'throw_magick_exception',
    },
);

=head2 $filestore->verify_integrity

Check a file's integrity by comparing the database md5 with the filesystem md5.

=head3 Returns

True on succes, 0 on failure.

=cut

sub verify_integrity {
    my $self = shift;

    my $db_md5    = $self->md5;
    my $file_path = $self->ustore->getPath($self->uuid);
    my $file_md5  = file_md5_hex($file_path);

    if ($db_md5 ne $file_md5) {
        return 0;
    }
    return 1;
}

=head2 $filestore->generate_thumbnail

Generates a thumbnail for the filestore object.

=head3 Returns

The newly updated Filestore object with thumbnail_uuid set.

=cut

sub generate_thumbnail {
    my $self = shift;

    # Create tempfiles
    my ($pdf_handle, $pdf_file) = tempfile(SUFFIX => '.pdf');
    my ($tn_handle, $tn_file)   = tempfile(SUFFIX => '.jpg');

    my $is_imagemagick_mimetype = grep {
        $_->{mimetype} eq $self->mimetype
        && defined $_->{conversion}
        && $_->{conversion} eq 'imagemagick'
    } values %{ MIMETYPES_ALLOWED() };

    # Imagemagick has no problem handling images and PDF, so they can be skipped
    # for conversion.
    my $magick_file;
    if ($self->mimetype eq 'application/pdf') {
        # First page only
        $magick_file = $self->ustore->getPath($self->uuid)."[0]";
    }
    elsif(!$is_imagemagick_mimetype) {
        $pdf_file = $self->convert({
            target_format => 'pdf',
            target_file   => $pdf_file,
        });
        # Only get the first page of the PDF.
        $magick_file = $pdf_file."[0]";
    }
    else {
        $magick_file = $self->ustore->getPath($self->uuid) . '[0]';   
    }

    $self->create_thumbnail({
        source_file  => $magick_file,
        target_file  => $tn_file,
    });

    # Add to file store
    my $uuid = $self->ustore->add($tn_file);

    # Nuke the tempfiles
    unlink($pdf_file);
    unlink($tn_file);

    return $self->update({thumbnail_uuid => $uuid});
}

=head2 $filestore->get_path

Returns the path for the file.

=cut


sub get_path {
    my $self = shift;
    return $self->ustore->getPath($self->uuid);
}

=head2 $filestore->convert

Convert to a different format. Returns a location of the newly converted file. It
is the responsibility of the caller to unlink this (temporary) file.

=cut

sub convert {
    my $self = shift;
    my $opts = $_[0];
    my $target_format = $opts->{target_format};
    my $target_file   = $opts->{target_file};
    my $case_id       = $opts->{case_id};
    my $magic_strings_convert = $opts->{magic_strings_convert};

    my ($name, $extension) = $self->original_name =~ qr/(.*)(\.[0-9A-Za-z]{1,5}$)/;

    if ($extension =~ /$target_format/i) {
        return $self->get_path;
    }

    if ($target_format =~ /pdf/i) {
        $target_file = $self->convert_to_pdf(
            $extension, $target_file, $magic_strings_convert, $case_id
        );
    }
    elsif ($target_format =~ /doc/i) {
        $target_file = $self->convert_to_doc(
            $extension, $target_file, $magic_strings_convert, $case_id
        );
    }
    else {
        throw_general_exception(
            code  => '/filestore/convert/target_format_unsupported',
            error => "Requested target format for conversion is not supported",
        );
    }
    return $target_file;
}

=head2 $filestore->convert_to_doc

Convert a filestore entry to DOC and return the new file's location.

=head3 Returns

Returns a file path.

=cut

sub convert_to_doc {
    my $self = shift;
    my ($extension, $target_file, $magic_strings_convert, $case_id) = @_;

    if ($extension ne '.odt') {
        throw_general_exception(
            code  => '/filestore/convert/source_and_target_format_incompatible',
            error => "Bronbestand-extensie '$extension' kan niet omgezet worden naar '.doc'",
        );
    }

    my $can_convert = MIMETYPES_ALLOWED;

    my $content;
    # Handle magic string conversion when desired
    if ($magic_strings_convert && $case_id) {
        my $tmp_file = $self->save_magic_string_document($case_id);
        open FILE, '<', $tmp_file or die "Could not open converted document: $tmp_file";
        $content = join "", <FILE>;
        unlink($tmp_file);
    }
    else {
        open FILE, '<', $self->get_path or die "Could not open file at ".$self->get_path;
        $content  = join "", <FILE>;
    }

    my $converter_result = $self->result_source->schema->resultset('Filestore')->send_to_converter(
        Content      => $content,
        Content_Type => $can_convert->{$extension}{mimetype},
        Accept       => 'application/msword'
    );

    open TARGET, '>', $target_file or die "could not write to: " . $target_file . ": " . $!;
    print TARGET $converter_result;
    close TARGET;

    return $target_file;
}

=head2 $filestore->convert_to_pdf

Convert a filestore entry to PDF and return the data.

=head3 Returns

The raw data as received from Office.

=cut

sub convert_to_pdf {
    my $self = shift;
    my ($extension, $target_file, $magic_strings_convert, $case_id) = @_;

    my $ext = lc($extension);

    my $can_convert = MIMETYPES_ALLOWED;
    # Images are converted with ImageMagick, the rest with LibreOffice
    if (   exists($can_convert->{$ext}{conversion})
        && $can_convert->{$ext}{conversion} eq 'imagemagick') {
        my $magick = Image::Magick->new;
        my $error  = $magick->Read($self->get_path);
        if ($error) {
            throw_magick_exception(
                code  => '/filestore/create_thumbnail/imagemagick_error',
                error => "ImageMagick error: $error",
            );
        }
        $magick->Write("pdfa:$target_file");

        return $target_file;
    }
    elsif (   $can_convert->{$ext}{conversion} eq 'xpstopdf'
           || $can_convert->{$ext}{conversion} eq 'xhtml2pdf'
    ) {
        use autodie qw(:all);
        system($can_convert->{$ext}{conversion}, $self->get_path, $target_file);

        return $target_file;
    }
    elsif (   !exists($can_convert->{$ext}{conversion})
           || $can_convert->{$ext}{conversion} ne 'jodconvertor'
    ) {
        throw_logic_exception(
            code  => '/filestore/convert_to_pdf/unsupported_filetype',
            error => "Cannot convert filetype $extension to PDF",
        );
    }

    my $content;
    # Handle magic string conversion when desired
    if ($magic_strings_convert && $case_id) {
        my $tmp_file = $self->save_magic_string_document($case_id);
        open FILE, '<', $tmp_file or throw "Could not open converted document: $tmp_file";
        $content = join "", <FILE>;
        unlink($tmp_file);
    }
    else {
        open FILE, '<', $self->get_path or die "Could not open file at ".$self->get_path;
        $content  = join "", <FILE>;
    }

    my $converter_result = $self->result_source->schema->resultset('Filestore')->send_to_converter(
        Content      => $content,
        ### Do NOT pass the mimetype stored in filestore.mimetype. While it is
        ### likely 100% correct, jodconvertor's mimetype list is limited. In
        ### particular for .doc documents there are simply too many variants.
        Content_Type => $can_convert->{$ext}{mimetype},
        Accept       => 'application/pdf',
    );

    open TARGET, '>', $target_file or die "could not write to: " . $target_file . ": " . $!;
    print TARGET $converter_result;
    close TARGET;

    return $target_file;
}

=head2 $filestore->create_thumbnail

Convert a PDF or JPEG to a JPEG thumbnail.

=head3 Parameters

=over

=item source_file

The file that needs converting.

=item target_file

Where the new file will be saved.

=back

=head3 Returns

True on succes, 0 on failure.

=cut

Params::Profile->register_profile(
    method  => 'create_thumbnail',
    profile => {
        required => [qw/
            source_file
            target_file
        /],
    }
);

sub create_thumbnail {
    my $self = shift;
    my $opts = $_[0];

    # Check database parameters
    my $dv = Params::Profile->check(
        params  => $opts,
    );
    if ($dv->has_invalid) {
        my @invalid = join ',',$dv->invalid;
        throw_parameter_exception(
            code  => '/filestore/create_thumbnail/invalid_parameters',
            error => "Invalid options given: @invalid",
        );
    }
    if ($dv->has_missing) {
        my @missing = join ',',$dv->missing;
        throw_parameter_exception(
            code  => '/filestore/create_thumbnail/missing_parameters',
            error => "Missing options: @missing",
        );
    }

    # Shove the PDF/JPEG through ImageMagick and store as new JPEG.
    use Image::Magick;
    my $image = Image::Magick->new;

    # Image::Magick throws non-fatal error messages. Even if the error is fatal. Die on
    # every message returned. (Succes is always undefined)
    my $error = $image->Read($opts->{source_file});
    if ($error) {
        throw_magick_exception(
            code  => '/filestore/create_thumbnail/imagemagick_error',
            error => "ImageMagick error: $error",
        );
    }

    $error = $image->Resize(geometry => '220x');

    # pull the different layers together. this will prevent black thumbnails
    # in some cases. mysteriously became necessary on Dec 7 2013, in combination
    # with Resize
    $image = $image->Flatten();

    if ($error) {
        throw_magick_exception(
            code  => '/filestore/create_thumbnail/imagemagick_error',
            error => "ImageMagick error: $error",
        );
    }
    $error = $image->Write($opts->{target_file});
    if ($error) {
        throw_magick_exception(
            code  => '/filestore/create_thumbnail/imagemagick_error',
            error => "ImageMagick error: $error",
        );
    }

    return $opts->{source_file};
}

=head2 $filestore->get_thumbnail_path

Returns the path for the file's thumbnail.

=cut

sub get_thumbnail_path {
    my $self = shift;
    return $self->ustore->getPath($self->thumbnail_uuid);
}

=head2 $filestore->get_thumbnail

Return the file's thumbnail file handle.

=cut

sub get_thumbnail {
    my $self = shift;
    return $self->ustore->get($self->thumbnail_uuid);
}

=head2 $filestore->name_without_extension

Return the file's name stripped of its extension.

=cut

sub name_without_extension {
    my $self = shift;

    my ($name, $extension) = $self->original_name =~ qr/(.*)(\.[0-9A-Za-z]{1,5}$)/;

    return $name;
    # return $self->original_name;
}

sub filestat {
    return stat(shift->get_path);
}

=head2 save_magic_string_document

Converts and stores a given filestore entry. Case_id is NOT optional.

=cut

sub save_magic_string_document {
    my $self = shift;
    my ($case_id) = @_;

    my $ztt = Zaaksysteem::ZTT->new;
    $ztt->add_context(
        $self->result_source->schema->resultset('Zaak')->find($case_id)
    );

    return $self->save_template_document($ztt);
}

=head2 save_template_document

Converts and stores the filestore entry, using the specified
L<Zaaksysteem::ZTT> instance.

=cut

sub save_template_document {
    my $self = shift;
    my ($ztt) = @_;

    my $tmp_dir = $self->result_source->schema->tmp_path;
    my ($tmp_fh, $tmp_file) = tempfile(
        "jodconverter-XXXXXX",
        DIR => $tmp_dir,

        # If there is no suffix, and the name of the DIR above contains a ".",
        # a call to Archive::ZIP done by "save" below will do bad things (it
        # strips everything after the last "." and adds ".zbk")
        SUFFIX => '.tmp',
    );

    odfWorkingDirectory($tmp_dir);
    my $encoding = $OpenOffice::OODoc::XPath::LOCAL_CHARSET;
    my $document = odfDocument(
        file            => $self->get_path || undef,
        local_encoding  => $encoding,
    );

    $document = $ztt->process_template($document)->document;

    $document->save($tmp_file);

    return $tmp_file;
}

=head2 content

Returns content of this file as a string

=cut

sub content {
    my $self = shift;

    my $file = $self->read_handle();
    my $content = join "", <$file>;
    close $file;

    return $content;
}

=head2 read_handle

Return a handle to the file (on disk), ready for reading.

=cut

sub read_handle {
    my $self = shift;

    my $path = $self->get_path;

    open my $file, '<', $path
        or throw ('filestore/cant_open', "Could not open filestore $path");

    return $file;
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 filestat

TODO: Fix the POD

=cut

