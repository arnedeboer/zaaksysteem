package Zaaksysteem::Backend::Filestore::ResultSet;

use strict;
use warnings;

use ClamAV::Client;
use Digest::MD5::File qw(file_md5_hex);
use Encode qw(encode_utf8);
use File::Basename;
use File::MimeInfo::Magic;
use File::stat;
use Params::Profile;
use Moose;
use Zaaksysteem::Constants;
use Zaaksysteem::Exception;

extends 'DBIx::Class::ResultSet';

with 'Zaaksysteem::Roles::UStore';

use Exception::Class (
    'Zaaksysteem::Backend::Filestore::ResultSet::Exception' => {fields => 'code'},
    'Zaaksysteem::Backend::Filestore::ResultSet::Exception::Parameter' => {
        isa         => 'Zaaksysteem::Backend::Filestore::ResultSet::Exception',
        description => 'Parameter exception',
        alias       => 'throw_parameter_exception',
    },
    'Zaaksysteem::Backend::Filestore::ResultSet::Exception::FileType' => {
        isa         => 'Zaaksysteem::Backend::Filestore::ResultSet::Exception',
        description => 'Filetype exception',
        alias       => 'throw_filetype_exception',
    },
);

=head2 filestore_create

Creates a Filestore entry.

=head3 Arguments

=over

=item file_path [required]

The path to the file that needs to be added to the file storage.

=item original_name [required]

The name the file originally has or had. This is always required to prevent
issues where a file storage (uuid-based) file name gets used.

=item ignore_extension [optional]

Does not execute the allowed extension check. Use with care.

=back

=head3 Returns

The newly created Filestore object.

=cut

Params::Profile->register_profile(
    method  => 'filestore_create',
    profile => {
        required => [qw/
            original_name
            file_path
        /],
        optional => [qw/
            ignore_extension
            id
        /]
    }
);

sub filestore_create {
    my $self = shift;
    my $opts = $_[0];

    # Check database parameters
    my $dv = Params::Profile->check(
        params  => $opts,
    );
    if ($dv->has_invalid) {
        my @invalid = join ',',$dv->invalid;
        throw_parameter_exception(
            code  => '/filestore/filestore_create/invalid_parameters',
            error => "Invalid options given: @invalid",
        );
    }
    if ($dv->has_missing) {
        my @missing = join ',',$dv->missing;
        throw_parameter_exception(
            code  => '/filestore/filestore_create/missing_parameters',
            error => "Missing options: @missing",
        );
    }

    # Check if the filetype is allowed
    unless ($opts->{ignore_extension}) {
        $self->assert_allowed_filetype($opts->{original_name});
    }

    my $file_path = $opts->{file_path};

    if (!-f $file_path || !-r $file_path) {
        throw('filestore/no_readable_file', "File '$file_path' is does not exist or isn't readable");
    }

    $self->clamscan($file_path);

    # Open the file and add it to the filestore
    open(my $file, $file_path) or die "Unable to open file: $file_path";
    my $store = $self->ustore;

    my $t0 = Zaaksysteem::StatsD->statsd->start;
    my $uuid  = $store->add($file);
    Zaaksysteem::StatsD->statsd->end('filestore.io.add.time', $t0);

    # File database properties
    $t0 = Zaaksysteem::StatsD->statsd->start;
    my $stat = stat($file);
    Zaaksysteem::StatsD->statsd->end('filestore.io.stat.time', $t0);

    my $db_params = {
        md5           => file_md5_hex($file_path),
        size          => $stat->size,
        mimetype      => $self->get_mimetype($file_path),
        original_name => encode_utf8($opts->{original_name}),
        uuid          => $uuid,
    };

    $db_params->{id} = $opts->{id} if $opts->{id};

    $t0 = Zaaksysteem::StatsD->statsd->start;
    my $create = $self->create($db_params);
    Zaaksysteem::StatsD->statsd->end('filestore.db.create.time', $t0);
    Zaaksysteem::StatsD->statsd->increment('filestore.create', 1);


    return $create->discard_changes;
}

=head2 get_mimetype

    $self->get_mimetype($file_path);

    # Returns e.g.:
    # application/msword

Will use module L<File::MimeInfo::Magic> for calculating the mimetype of the given file. Unfortunatly,
this module is a little outdated and has troubles with some mimetypes. We convert these types to the
expected mimetype.

B<Corrected mimetypes>

=over

=item application/vnd.ms-word

Corrects the wrong C<application/vnd.ms-word> to C<application/msword>. References:
L<http://stackoverflow.com/questions/4212861/what-is-a-correct-mime-type-for-docx-pptx-etc>
L<https://blogs.msdn.microsoft.com/vsofficedeveloper/2008/05/08/office-2007-file-format-mime-types-for-http-content-streaming-2/>

=back

=cut

sub get_mimetype {
    my $self        = shift;
    my $mimetype    = mimetype(shift);

    return unless $mimetype;

    my %mime_map    = (
        'application/vnd.ms-word' => 'application/msword'
    );

    return $mime_map{$mimetype} if $mime_map{$mimetype};
    return $mimetype;
}


=head2 clamscan($path)

Scan a new file, throw exception if it contains a virus.

=cut

sub clamscan {
    my ($self, $path) = @_;

    throw('filestore/clamscan/file_not_found', "Bestand niet gevonden: $path")
        unless -e $path;

    my $t0 = Zaaksysteem::StatsD->statsd->start;

    my $scanner = ClamAV::Client->new();

    if (!$scanner || !$scanner->ping) {
        Zaaksysteem::StatsD->statsd->increment('filestore.clamav.down', 1);
        throw("filestore/clamscan/scanner_not_available", "Virusscanner niet operationeel")
    }

    open my $file, "<", $path or
        throw('filestore/clamscan/file_not_found', 'Bestand niet gevonden');

    my $virus = $scanner->scan_stream($file);

    close $file;

    Zaaksysteem::StatsD->statsd->end('filestore.clamav.scan.time', $t0);
    Zaaksysteem::StatsD->statsd->increment('filestore.clamav.scan', 1);


    if ($virus) {
        Zaaksysteem::StatsD->statsd->increment('filestore.clamav.virus_found', 1);
        throw("filestore/clamscan/virus_found", "Virus aangetroffen in bestand, niet toegevoegd.")
    }
}


sub find_by_uuid {
    shift->search({ uuid => shift })->first;
}

=head2 $self->assert_allowed_filetype($filename)

Check whether or not a filetype is allowed within the file storage.

Returns true or error.

=cut

sub assert_allowed_filetype {
    my $self = shift;
    my ($name) = @_;

    my ($extension) = $self->get_file_extension($name);

    if (!$extension) {
        throw_filetype_exception(
            code  => '/filestore/assert_allowed_filetype/no_extension',
            error => "File does not have an extension: $name",
        );
    }

    # Match against lowercase
    $extension = lc($extension);

    if (!MIMETYPES_ALLOWED->{$extension}) {
        throw(
            '/filestore/assert_allowed_filetype/extension_not_allowed',
            "Bestandstype niet toegestaan: $extension",
        );
    }
    return 1;
}


=head2 get_file_extension

Central utility method to determine the file extension based on a file path.
Looks for a dot (.) and 1 to 5 characters at the end of the given path.

E.g. /tmp/something/sub/filename.ext => yields '.ext'

Return file extension including the dot (.)

=cut

sub get_file_extension {
    my ($self, $path) = @_;

    my ($extension) = $path =~ qr/(\.[0-9A-Za-z]{1,5}$)/;

    return $extension;
}


=head2

Send a document through the JODconverter. Typically requires these parameters:

 Content      => $to_be_converted_data,
 Content_Type => $mimetype_of_source_format,
 Accept       => $mimetype_of_target_format,

=cut

sub send_to_converter {
    my $self = shift;
    my %params = @_;

    my $converter_url = $self->result_source->schema->resultset('Config')->get_value('jodconverter_url');

    my $t0 = Zaaksysteem::StatsD->statsd->start;

    use HTTP::Request::Common;
    my $ua = LWP::UserAgent->new;
    my $result = $ua->request(POST $converter_url,
        %params,
    );

    Zaaksysteem::StatsD->statsd->end('filestore.converter.time', $t0);
    Zaaksysteem::StatsD->statsd->increment('filestore.converter', 1);

    return $result->content if ($result->is_success);

    Zaaksysteem::StatsD->statsd->increment('filestore.converter.down', 1);
    throw('fs/jod_converter/error/' . $result->code, $result->status_line, $result);
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 find_by_uuid

TODO: Fix the POD

=cut

=head2 send_to_converter

TODO: Fix the POD

=cut

