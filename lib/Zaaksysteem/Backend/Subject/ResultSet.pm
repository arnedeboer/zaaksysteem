package Zaaksysteem::Backend::Subject::ResultSet;

use Moose;

use Zaaksysteem::Backend::Sysin::Modules;
use Zaaksysteem::Constants;
use Zaaksysteem::Tools;

use Crypt::SaltedHash;
use Hash::Merge qw//;


extends 'Zaaksysteem::Backend::ResultSet';

use constant ROLE_PROFILES  => {
    'employee'  => {
        required    => [qw/sn givenname initials mail displayname/],
        optional    => [qw/telephonenumber password/],
    }
};

has '_get_role_profiles' => (
    is      => 'ro',
    lazy    => 1,
    default => sub {
        return ROLE_PROFILES();
    }
);

sub search_active {
    my $self        = shift;

    my $active      = $self->search(
        {
            'user_entities.date_deleted'    => undef,
        },
        {
            join => 'user_entities'
        }
    );

    return $active->search(@_);
}

# DEPRECATED AFTER 20150301 because of saml
# =head2 $subject_resultset->create_from_ldap(\%params)

# Return value: $SUBJECT_ROW

#     $subject         = $c->model('DB::Subject')->create_from_ldap(
#         {
#             interface_id        => $interface_id,
#             ldap_user           => $ldap_user           # Net::LDAP entry
#         }
#     );

# Creates a user from given Net::LDAP::Row(PosixAccount) user into tables
# user_entity and subject.

# Will throw an error when user has already been created. When you are not sure,
# please use C<update_or_create_from_ldap>, which will update an existing ldap_user
# when it exists under the username, and creates it when it isn't.

# =cut

# define_profile create_from_ldap => (
#     required => [qw[ldap_user interface_id]],
# );

# sub create_from_ldap {
#     my $self            = shift;
#     my $params          = assert_profile(
#         shift || {}
#     )->valid;

#     my $ldap_user       = $params->{ldap_user};
#     my $interface_id    = $params->{interface_id};

#     my ($subject, $user_entity) = $self->_create_user($ldap_user->get_value('cn'), $interface_id);

#     $subject->update_properties_from_ldap(
#         $ldap_user,
#         {
#             interface_id        => $interface_id,
#             created             => 1,
#         }
#     );
#     $subject->login_entity($user_entity);

#     return $subject;
# }

# =head2 $subject_resultset->update_or_create_from_ldap(\%params)

# Return value: $SUBJECT_ROW

#     $subject         = $c->model('DB::Subject')->update_or_create_from_ldap(
#         {
#             interface_id        => $interface_id,
#             ldap_user           => $ldap_user           # Net::LDAP entry
#         }
#     );

# Updates or creates a user from given Net::LDAP::Row(PosixAccount) user into tables
# user_entity and subject.

# =cut

# define_profile update_or_create_from_ldap => (
#     required => [qw[ldap_user interface_id]],
# );

# sub update_or_create_from_ldap {
#     my $self            = shift;
#     my $params          = assert_profile(
#         shift || {}
#     )->valid;

#     my $ldap_user       = $params->{ldap_user};
#     my $interface_id    = $params->{interface_id};

#     my $user_entity     = $self
#                         ->result_source
#                         ->schema
#                         ->resultset('UserEntity')
#                         ->search(
#                             {
#                                 'me.source_interface_id'    => $interface_id,
#                                 'me.source_identifier'      => $ldap_user->get_value('cn')
#                             },
#                             {
#                                 join        => 'subject_id'
#                             }
#                         )->first;

#     if ($user_entity) {
#         unless ($user_entity->active && !$user_entity->date_deleted) {
#             $user_entity->active(1);
#             $user_entity->date_deleted(undef);
#             $user_entity->update;
#         }

#         my $subject             = $user_entity->subject_id;

#         $subject->update_properties_from_ldap(
#             $ldap_user,
#             {
#                 interface_id        => $interface_id,
#             }
#         );

#         return $subject;
#     }

#     return $self->create_from_ldap($params);
# }

=head2 create_from_saml

Create a subject based on a hash of SAML authentication info (as created by
L<Zaaksysteem::SAML> when a user logs in).

=cut

define_profile create_from_saml => (
    required => [
        'interface_id',
        'saml_data',
    ],
);

sub create_from_saml {
    my $self = shift;
    my $params = assert_profile(
        shift || {}
    )->valid;

    my $username        = $params->{saml_data}{uid};
    my $interface_id    = $params->{interface_id};

    my $user_entity     = $self->create_user(
        {
            username    => $username,
            interface   => $self->result_source->schema->resultset('Interface')->find($interface_id),
            empty_user  => 1,
        }
    );

    my $subject         = $user_entity->subject_id;

    $subject->update_properties_from_saml(
        $params->{saml_data},
        {
            interface_id => $interface_id,
            created      => 1,
        },
    );
    $subject->login_entity($user_entity);

    return $subject;
}

=head2 create_user

Create a subject + user entity, given a username and an interface id.

Returns ($subject, $user_entity).

=cut

has 'default_create_user_profile' => (
    is      => 'ro',
    lazy    => 1,
    default => sub {
        return {
            required    => [qw/
                username

                interface
            /],
            optional    => [qw/
                role_ids

                set_behandelaar
                group_id

                empty_user
            /],
            # require_some => {
            #     'group_id_or_queue' => [1, qw/group_id queue/],
            # }
        };
    }
);

sub _get_create_user_profile {
    my $self        = shift;
    my $type        = shift;

    my $profile     = $self->default_create_user_profile;

    return $profile unless ROLE_PROFILES->{$type};

    return Hash::Merge::merge($profile, ROLE_PROFILES->{$type});
}

sub create_user {
    my $self        = shift;
    my $rparams     = shift || {};

    ### Validation
    my $opts        = assert_profile(
        $rparams,
        profile => ($rparams->{empty_user} ? $self->default_create_user_profile : $self->_get_create_user_profile($rparams->{interface}->module_object->subject_type))
    )->valid;

    my $existing_entity = $self->result_source->schema->resultset('UserEntity')->search(
        {
            source_identifier   => $opts->{username},
            source_interface_id => $opts->{interface}->id,
        }
    )->first;

    throw(
        'subject/create_user/exists',
        'Error creating subject, entity already exists in database'
    ) if $existing_entity && !$existing_entity->date_deleted;

    my $subject_data = {
        subject_type => (
            $opts->{interface}->module_object->subject_type
        ),
        username     => lc($opts->{username}),
    };

    my $subject_rs = $self->search($subject_data);

    my $subject;
    if ($subject_rs->count) {
        $subject = $subject_rs->first;
    }
    else {
        $subject = $self->create($subject_data);
    }


    my $user_entity;
    if ($existing_entity) {
        $existing_entity->update(
            {
                date_deleted    => undef,
                subject_id      => $subject->id
            }
        );
        $user_entity = $existing_entity;
    } else {
        my $csh = Crypt::SaltedHash->new(algorithm => 'SHA-1');
        $csh->add($opts->{password});
        my $salted = $csh->generate;

        $user_entity = $subject->user_entities->create(
            {
                source_identifier   => $opts->{username},
                source_interface_id => $opts->{interface}->id,
                active              => 1,
                subject_id          => $subject->id,
                password            => $salted,
            }
        );
    }

    unless ($rparams->{empty_user}) {
        $subject->update_user($opts);
    }

    return $user_entity;
}

# sub create_user {
#     my $self = shift;
#     my ($username, $interface_id) = @_;

#     my $existing_entity = $self->result_source->schema->resultset('UserEntity')->search(
#         {
#             source_identifier   => $username,
#             source_interface_id => $interface_id,
#         }
#     )->first;

#     throw(
#         'subject/create_user/exists',
#         'Error creating subject, entity already exists in database'
#     ) if $existing_entity && !$existing_entity->date_deleted;

#     if ($existing_entity) {
#         $existing_entity->update({ date_deleted => undef });
#         return ($existing_entity->subject_id, $existing_entity);
#     }

#     my $subject_data = {
#         subject_type => SUBJECT_TYPE_EMPLOYEE,
#         username     => $username,
#     };
#     my $subject_rs = $self->search($subject_data);

#     my $subject;
#     if ($subject_rs->count) {
#         $subject = $subject_rs->first;
#     }
#     else {
#         $subject = $self->create($subject_data);
#     }

#     my $user_entity = $subject->user_entities->create(
#         {
#             source_identifier   => $username,
#             source_interface_id => $interface_id,
#             active              => 1
#         }
#     );

#     return ($subject, $user_entity);
# }

=head2 search_by_group_id

Arguments: $NUMBER_GROUP_ID

Return value: L<Zaaksysteem::Backend::Subject::ResultSet>

    $rs->search_by_group_id(44);

Will return a resultset containing all subject who belong to given group_id

=cut

sub search_by_group_id {
    my $self            = shift;
    my $group_id        = shift;

    throw(
        'subject/search_by_group_id/invalid_id',
        'Given ID not valid integer'
    ) unless $group_id && $group_id =~ /^\d+$/;

    return $self->search(
        {
            $group_id => \'= ANY (group_ids)',
        },
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 search_active

TODO: Fix the POD

=cut

