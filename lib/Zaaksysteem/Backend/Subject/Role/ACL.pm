package Zaaksysteem::Backend::Subject::Role::ACL;

use Moose::Role;
use Zaaksysteem::Tools;

use Zaaksysteem::Constants qw/ZAAKSYSTEEM_AUTHORIZATION_ROLES/;

=head1 NAME

Zaaksysteem::Backend::Subject::Role::ACL - Authorization roles and groups

=head1 SYNOPSIS

    # See USAGE tests:
    # TEST_METHOD="roles_usage.*" ./zs_prove -v t/testclass/100-general.t
    # TEST_METHOD="groups_usage.*" ./zs_prove -v t/testclass/100-general.t


=head1 DESCRIPTION

Roles and group functions for subject

=head1 ATTRIBUTES

=head2 primary_groups

isa: ArrayRef[Zaaksysteem::Schema::Groups]

A list containing exactly one group, defining the group this subject primary
belongs to

=cut

has 'primary_groups' => (
    'is'        => 'rw',
    'isa'       => 'ArrayRef[Zaaksysteem::Schema::Groups]',
    'lazy'      => 1,
    'builder'   => '_build_primary_groups',
);

sub _build_primary_groups {
    my $self        = shift;

    return $self->_groups_for_subject->{primary_groups};
}

=head2 primary_group

isa: Zaaksysteem::Schema::Groups

Returns the first entry from primary_Groups

=cut

has 'primary_group' => (
    'is'        => 'rw',
    'isa'       => 'Maybe[Zaaksysteem::Schema::Groups]',
    'lazy'      => 1,
    'default'   => sub {
        my $self        = shift;

        return $self->primary_groups->[0];
    }
);

### _groups_for_subject
###
### Caching attribute containing the primary and inherited groups for this subject

has '_groups_for_subject' => (
    'is'        => 'rw',
    'isa'       => 'HashRef',
    'lazy'      => 1,
    'builder'   => '_build_groups_for_subject',
);

sub _build_groups_for_subject {
    my $self        = shift;
    my $schema      = $self->result_source->schema;

    my $groups      = $schema->resultset('Groups')->groups_for_subject($self);
    return $groups;
}

has groups => (
    'lazy'      => 1,
    'is'        => 'rw',
    'isa'       => 'ArrayRef',
    'builder'   => '_groups',
);

sub _groups {
    my $self = shift;

    my $groups = $self->_groups_for_subject;
    return [ map { @{$groups->{$_}} } keys %$groups ];
}

=head2 inherited_groups

isa: ArrayRef[Zaaksysteem::Schema::Groups]

A list of groups the user inherits the rights from

=cut

has 'inherited_groups' => (
    'is'        => 'rw',
    'isa'       => 'ArrayRef[Zaaksysteem::Schema::Groups]',
    'lazy'      => 1,
    'builder'   => '_build_inherited_groups',
);

sub _build_inherited_groups {
    my $self        = shift;

    return $self->_groups_for_subject->{inherited_groups};
}

=head2 roles

isa: ArrayRef[Zaaksysteem::Schema::Roles]

A list of roles this subject has.

=cut

has 'roles' => (
    'is'        => 'rw',
    'isa'       => 'ArrayRef[Zaaksysteem::Schema::Roles]',
    'lazy'      => 1,
    'builder'   => '_build_roles',
);

sub _build_roles {
    my $self        = shift;
    my $schema      = $self->result_source->schema;

    return $schema->resultset('Roles')->roles_for_subject($self);
}

=head2 system_roles

isa: ArrayRef[Zaaksysteem::Schema::Roles]

A list containing all the system roles this user has access to. E.g., where C<system_role = 1>

=cut

has 'system_roles' => (
    'is'        => 'rw',
    'isa'       => 'ArrayRef[Zaaksysteem::Schema::Roles]',
    'lazy'      => 1,
    'builder'   => '_build_system_roles',
);

sub _build_system_roles {
    my $self        = shift;

    my @systemroles = grep { $_->system_role } @{ $self->roles };

    return \@systemroles;
}

=head2 system_permissions

B<Type>: ArrayRef of String permissions

Returns a list of strings like 'zaak_eigen', 'admin','search' etc. For a complete list, read
C<Constants.pm: ZAAKSYSTEEM_AUTHORIZATION_ROLES>

=cut

has 'system_permissions' => (
    is          => 'rw',
    isa         => 'ArrayRef[Str]',
    lazy        => 1,
    default     => sub {
        my $self        = shift;

        ## Switch constant
        my %ou_permissions   = map { ZAAKSYSTEEM_AUTHORIZATION_ROLES->{$_}->{ldapname} => ZAAKSYSTEEM_AUTHORIZATION_ROLES->{$_} } keys %{ ZAAKSYSTEEM_AUTHORIZATION_ROLES() };

        my %permissions;
        for my $role (@{ $self->system_roles }) {
            for my $right (keys %{ $ou_permissions{$role->name}->{rechten}->{global} }) {
                $permissions{$right} = 1;
            }
        }

        return [ keys %permissions ];
    }
);

=head2 all_available_groups

isa: ArrayRef[Zaaksysteem::Schema::Groups]

A list of roles this subject has.

TODO: tests

=cut

has 'all_available_groups' => (
    'is'        => 'rw',
    'isa'       => 'ArrayRef[Zaaksysteem::Schema::Groups]',
    'lazy'      => 1,
    'default'   => sub {
        my $self        = shift;
        my $schema      = $self->result_source->schema;

        return $schema->resultset('Groups')->get_all_cached;
    }
);

=head2 all_available_roles

isa: ArrayRef[Zaaksysteem::Schema::Roles]

A list of roles this subject has.

TODO: tests

=cut

has 'all_available_roles' => (
    'is'        => 'rw',
    'isa'       => 'ArrayRef[Zaaksysteem::Schema::Roles]',
    'lazy'      => 1,
    'default'   => sub {
        my $self        = shift;
        my $schema      = $self->result_source->schema;

        return $schema->resultset('Roles')->get_all_cached;
    }
);

=head1 METHODS

=head2 set_primary_groups

Arguments: \@ARRAYREF_OF_GROUP_IDS

Return value: $SUBJECT_COMPONENT

    $subject->set_primary_groups([$schema->resultset('Groups')->first->id]);

Sets the primary groups for this users. Currently, only one value is allowed.

=cut

sub set_primary_groups {
    my $self        = shift;
    my $schema      = $self->result_source->schema;

    return $schema->resultset('Groups')->set_primary_groups_on_subject($self, @_);
}

=head1 METHODS

=head2 set_roles

Arguments: \@ARRAYREF_OF_ROLE_IDS

Return value: $SUBJECT_COMPONENT

    $subject->set_roles([$schema->resultset('Roles')->first->id]);

Sets the roles for this user.

=cut

sub set_roles {
    my $self        = shift;
    my $schema      = $self->result_source->schema;

    return $schema->resultset('Roles')->set_roles_on_subject($self, @_);
}

=head2 member_of_group_id

Arguments: group_id

TODO TESTS

=cut

sub member_of_group_id {
    my $self        = shift;
    my $group_id    = shift;

    return 1 if grep { $_->id eq $group_id } @{ $self->primary_groups };
    return;
}

=head2 find_group_by_id

Find a group by a given group ID.

=head3 SYNOPSIS

    my $group = $c->user->find_group_by_id($id);
    if ($group) {
        print "Found group " . $group->name;
    }
    else {
        print "Did not find group by $id";
    }

=cut

sub find_group_by_id {
    my ($self, $id) = @_;
    my $groups = $self->all_available_groups;

    my ($group) = grep { $_->id eq $id } @$groups;
    return $group;
}

=head2 get_group_by_id

Get a group by a given group ID. If the group does not exist, we die.

=head3 SYNOPSIS

    my $group = $c->user->get_group_by_id($id);

=cut

sub get_group_by_id {
    my ($self, $id) = @_;

    my $group = $self->find_group_by_id($id);
    return $group if $group;
    throw("zs/group", "No group by $id");
}

=head2 find_role_by_id

Find a role by a given role ID.

=head3 SYNOPSIS

    my $role = $c->user->find_role_by_id($id);
    if ($role) {
        print "Found role " . $role->name;
    }
    else {
        print "Did not find role by $id";
    }

=cut

sub find_role_by_id {
    my ($self, $id) = @_;
    my $roles = $self->all_available_roles;
    my ($role) = grep { $_->id eq $id } @$roles;
    return $role;
}

=head2 get_role_by_id

Get a role by a given role ID. If the role does not exist, we die.

=head3 SYNOPSIS

    my $role = $c->user->get_role_by_id($id);

=cut

sub get_role_by_id {
    my ($self, $id) = @_;

    my $role = $self->find_role_by_id($id);
    return $role if $role;
    throw("zs/role", "No role by $id");
}

=head2 find_role_by_name

Find a role by a given role name. If the role does not exist, undef is returned.

    my $role = $c->user->find_role_by_name($name);
    if ($role) {
        # got it
    }

=cut

sub find_role_by_name {
    my ($self, $name) = @_;
    my $roles = $self->all_available_roles;
    my ($role) = grep { $_->name eq $name } @$roles;
    return $role;
}

=head2 get_role_by_name

Get a role by a given role name. If the role does not exist, we die.

    my $role = $c->user->get_role_by_name($name);

=cut

sub get_role_by_name {
    my ($self, $name) = @_;

    my $role = $self->find_role_by_name($name);
    return $role if $role;
    throw('role/name/not_found', "No role by $name");
}


1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Install>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
