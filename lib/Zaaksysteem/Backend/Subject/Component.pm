package Zaaksysteem::Backend::Subject::Component;

use Moose;
use Moose::Util qw/apply_all_roles/;

BEGIN { extends qw/DBIx::Class Catalyst::Authentication::User/; }

with qw/
    Zaaksysteem::JSON::DbAccessors
    Zaaksysteem::Backend::Subject::Role::ACL
    Zaaksysteem::BR::Subject::Component
/;

use Authen::Passphrase;
use Crypt::SaltedHash;
use Digest::SHA;
use MIME::Base64;
use List::MoreUtils qw[any];
use Zaaksysteem::Tools;
use Zaaksysteem::UserSettings;

=head1 NAME

Zaaksysteem::Backend::Subject::Component - Implement specific behaviors for
Subject rows.

=head1 DESCRIPTION

=head1 CONSTANTS

=head2 SUBJECT_TYPE_TO_ROLE

This constant is a Hashref that maps the subject type to a specific behavioural
role.

=cut

use constant SUBJECT_TYPE_TO_ROLE => {
    'employee'      => ['Zaaksysteem::Backend::Subject::Employee'],
};

=head2 DEPRECATED_IDENTIFIER_MAPPING

This constant is a Hashref that maps a subject type to the associated
deprecated typename.

=cut

use constant DEPRECATED_IDENTIFIER_MAPPING => {
    employee => 'medewerker',
};

=head1 ATTRIBUTES

=head2 login_entity

References the login_entity used to authenticate the subject, if the user is
logged in at all.

This state is set external to this package.

Exposes a C<has_login_entity> predicate.

=cut

has login_entity => (
    is => 'rw',
    predicate => 'has_login_entity'
);

=head2 preferred_group

References the preferred L<Zaaksysteem::Backend::Groups::Component|group> of
the user, if one is set.

=cut

has preferred_group => (
    is      => 'rw',
    lazy    => 1,
    default => sub {
        my $self = shift;

        my $preferred_group_id = $self->properties->{ preferred_group_id };

        return unless $preferred_group_id;

        return $self->result_source->schema->resultset('Groups')->find(
            $preferred_group_id
        );
    }
);

=head2 external_api_objects

Contains a list of object types provided via the authentication subsystem for
which the subject is authorizated.

=cut

has external_api_objects => (
    is        => 'rw',
    isa       => 'ArrayRef',
    default   => sub { []; }
);

=head2 is_external_api

Boolean flag indicating if the subject is currently used as an API 'spoof'
user.

=cut

has is_external_api => (
    is        => 'rw',
    isa       => 'Bool',
    default   => 0,
);

=head2 betrokkene_identifier

This attribute lazily builds the deprecated 'betrokkene identifier'. Returns
something like C<betrokkene-medewerker-1234>.

=cut

has betrokkene_identifier => (
    is => 'rw',
    lazy => 1,
    default => sub {
        my $self            = shift;

        return sprintf "betrokkene-%s-%d", (
            DEPRECATED_IDENTIFIER_MAPPING->{$self->subject_type},
            $self->id,
        );
    }
);

=head2 is_active

Boolean flag which indicates whether the subject is active (has active
user_entities).

B<NOTE>: this attribute loads once, so on successive calls the database state
may have been changed (although this seems very unlikely).

=cut

has is_active => (
    is => 'rw',
    lazy => 1,
    default => sub {
        my $self            = shift;

        return $self->user_entities->search(
            {
                date_deleted    => undef,
            }
        )->count ? 1 : undef;
    }
);

=head1 METHODS

=head2 usersettings

Returns a L<Zaaksysteem::UserSettings> instance, initialized using the
subject's settings.

=cut

sub usersettings {
    my $self    = shift;

    return Zaaksysteem::UserSettings->new_from_settings(
        $self->settings || {},
        schema => $self->result_source->schema
    );
}


=head2 is_sorted

Returns true if the subject has been sorted into a department, false
otherwise.

=cut

sub is_sorted {
    my $self = shift;

    my $group_ids = $self->group_ids;

    return unless $group_ids;

    return scalar @{ $group_ids } > 0;
}

=head2 is_system_user

Convenience method to check if a subject/user is a system user.

=cut

sub is_system_user {
    my $self = shift;
    return $self->system;
}

=head2 is_nobody_user

Convenience method to check if a subject/user is a nobody user.

=cut

sub is_nobody_user {
    my $self = shift;
    return $self->nobody;
}

=head2 check_password

Arguments: $password

Return value: $TRUE_OR_FALSE

    print $self->check_password('jaddaj@dda')

    # Prints '1' on success

Validates the password according to RFC2307. In other words, it will look at the
first few characters in the database, to find out the hashing algorythm:

    {SSHA}DjSkeWeacd8a9dc8a9ASDf8dadscfasdaa9dSddsdf8D

    or

    {SHA}DjSkeWeacd8a9dc8a9ASDf8dadscfasdaa9dSddsdf8D

    etc.

=cut


sub check_password {
    my $self        = shift;
    my $password    = shift;

    my $authldap    = $self->result_source->schema->resultset('Interface')->search_active(
        {
            module => 'authldap'
        }
    )->first;

    my @entities    = $self->user_entities->search(
        {
            source_interface_id => $authldap->id,
        }
    );

    for my $entity (@entities) {
        my $entity_password = $entity->password;

        my $ppr = Authen::Passphrase->from_rfc2307($entity->password);

        return 1 if $ppr->match($password);
    }

    return;
};

=head2 update_password

Arguments: $USER_ENTITY_ROW, \%OPTS

    $self->update_password($USER_ENTITY, { password => 'p@ssword'});

Will update the password for this subject in the belonging user_entity object

=cut

sig update_password => "Zaaksysteem::Schema::UserEntity,HashRef";

sub update_password {
    my $self        = shift;
    my $user_entity = shift;
    my $opts        = shift;

    return unless $opts->{password};

    my $hashed_pass;
    if ($opts->{password} =~ /^\{S?SHA\}/) {
        $hashed_pass    = $opts->{password};
    } else {
        my $csh = Crypt::SaltedHash->new(algorithm => 'SHA-1');
        $csh->add($opts->{password});

        $hashed_pass = $csh->generate();
    }

    $user_entity->password($hashed_pass);
    $user_entity->update;
}

=head2 new

Added hook for creating custom accessors for the json data, for more info, see
L<DBIx::Class::Row>

=cut

sub new {
    my $class   = shift;

    my $self    = $class->next::method(@_);

    ### Apply roles depending on subject_types
    $self->_apply_subject_roles;

    $self->_generate_jsonaccessors;

    return $self;
}

=head2 inflate_result

Hooks into L<DBIx::Class::Row/inflate_result> logic in order to initialize
the subject's state.

=cut

sub inflate_result {
    my $class   = shift;

    my $self    = $class->next::method(@_);

    $self->_apply_subject_roles;

    $self->_generate_jsonaccessors;

    return $self;
}

=head2 modify_setting

Convenience method for modifying the subject's settings.

B<NOTE>: This method calls L<DBIx::Class::Row/update>.

=cut

sub modify_setting {
    my ($self, $key, $value) = @_;

    my $settings = $self->settings;

    $settings->{$key} = $value;

    $self->settings($settings);
    $self->update;
}

=head2 matches_security_rule

This method returns a true value if the provided security rule matches
the user instance. It does this intelligently by walking up the group tree
the user is in, and cross-checking the roles currently in the group/role DB.

=head3 Parameters

=over 4

=item rule

This required parameter is expected to be a
L<Zaaksysteem::Object::SecurityRule> instance. The subject instance is
compared to this datum.

=back

=cut

define_profile matches_security_rule => (
    required => {
        rule => 'Zaaksysteem::Object::SecurityRule',
    }
);

sub matches_security_rule {
    my ($self, %params) = @_;

    my $opts = assert_profile(\%params)->valid;

    my $identity = $opts->{ rule }->entity;

    if ($identity->entity_type eq 'tag') {
        return 1 if $identity->entity_id eq 'all_users';

        return;
    }

    # Simple case for user
    if($identity->entity_type eq 'user') {
        return $identity->entity_id eq $self->username;
    }

    if($identity->entity_type eq 'position') {
        my ($ou_id, $role_id) = split m[\|], $identity->entity_id;

        # ou_id may be 0, indicating "all OUs", in which case it implicitly matches
        if($ou_id) {
            return unless grep { $_->id eq $ou_id } @{ $self->primary_groups }, @{ $self->inherited_groups };
        }

        # Contrast with groups, which we do not support wildcards for
        return unless grep { $_->id eq $role_id } @{ $self->roles };

        return 1;
    }

    if ($identity->entity_type eq 'group') {
        return unless grep {
            $_->id eq $identity->entity_id
        } @{ $self->primary_groups }, @{ $self->inherited_groups };

        return 1;
    }

    if ($identity->entity_type eq 'role') {
        return unless grep {
            $_->id eq $identity->entity_id
        } @{ $self->roles };

        return 1;
    }

    throw('subject/acl/unimplemented_entity_type', sprintf(
        'Unsupported security identity entity_type "%s", security rule matching failed.',
        $identity->entity_type
    ));
}

=head2 has_legacy_permission

Checks if any of the subject's roles imply the supplied permission

    if ($user->has_legacy_permission('admin')) {
        $user->bobby_tables;
    }

=cut

sig has_legacy_permission => 'Str';

sub has_legacy_permission {
    my $self = shift;
    my $permission = shift;

    return any {
        $_->implies_legacy_permission($permission)
    } @{ $self->roles };
}

=head2 remove_inavigator_settings

settings are stored per casetype. this clears out settings for a specific casetype

=cut

sub remove_inavigator_settings {
    my ($self, $code) = @_;

    throw('subject/remove_inavigator_settings/missing_code',
        'Zaaktypecode is verplicht') unless $code;

    my $current = $self->settings->{inavigator_settings} || {};

    map { delete $current->{$_}->{$code} } keys %$current;

    $self->modify_setting('inavigator_settings', $current);
}

=head2 update_user

Arguments: \%PARAMS, $USER_ENTITY_ROW

    $self->update_user(
        {
            group_id            => 44,
            set_behandelaar     => 1,
            username            => 'dedon',
            sn                  => 'Don',
            initials            => 'D',
            [...]
        }
    )

Will update the user with the given params.

B<Acties>

=over 4

=item set_behandelaar

Will make sure this row gets the behandelaar row, so it can login

=back

=cut

sub update_user {
    my $self        = shift;
    my $opts        = shift;
    my $entity      = shift;

    if ($opts->{group_id}) {
        my $group       = $self->result_source->schema->resultset('Groups')->find($opts->{group_id});

        throw(
            'subject/create_user/group_not_found',
            'Cannot find requested group for id: ' . $opts->{group_id}
        ) unless $group;

        $self->group_ids([ $group->id ]);
    }

    ### Behandelaar roles
    my $roles = $self->role_ids || [];
    if ($opts->{set_behandelaar}) {
        my $behandelaar_role    = $self->result_source->schema->resultset('Roles')->find(
            {
                name            => 'Behandelaar',
                system_role     => 1,
            }
        );

        throw(
            'subject/create_user/no_behandelaar_role',
            'Cannot find system role "Behandelaar"'
        ) unless $behandelaar_role;

        if (!grep { $behandelaar_role->id == $_ } @{ $roles }) {
            push(@$roles, $behandelaar_role->id);
        }
    }

    ### Set roles if given
    if ($opts->{role_ids}) {
        push(@$roles, @{ $opts->{role_ids} });
    }

    $self->role_ids($roles);

    ### Update subject properties
    for my $key (keys %$opts) {
        next if $key =~ /^(?:subject_type|interface|role_ids)$/;
        $self->$key($opts->{$key});
    }
    $self->update;

    if ($entity) {
        if ($opts->{username}) {
            $entity->source_identifier($opts->{username});
            $entity->update;
        }

        $self->update_password($entity, $opts);
    }

    return $self;
}

=head2 supported_features

This method returns a hashref of feature supported by this object type. It is
a required part of Catalyst's authentication infrastructure.

=cut

sub supported_features {
    return {
        session => 1
    };
}

=head2 security_identity

The method implements an interface required by the security infrastructure, it
plainly returns C<< user => $username >>.

For more info, see L<Zaaksysteem::Object::Model>.

=cut

sub security_identity {
    return ('user', shift->username);
}

=head2 TO_JSON

Data aggregator for L<JSON/encode_json>.

=cut

sub TO_JSON {
    my $self        = shift;

    my $json = {
        id                   => $self->id,
        external_api_objects => $self->external_api_objects,
        is_external_api      => $self->is_external_api,
    };

    if ($self->has_login_entity) {
        $json->{ login_entity_id } = $self->login_entity->id
    }

    return $json;
}

=head1 PRIVATE METHODS

=head2 _apply_subject_roles

Applies roles on the subject instance conditional on the subject type. See
L</SUBJECT_TYPE_TO_ROLE> for a definition of related types and roles.

=cut

sub _apply_subject_roles {
    my $self        = shift;

    if (SUBJECT_TYPE_TO_ROLE->{ $self->subject_type }) {
        apply_all_roles($self, @{ SUBJECT_TYPE_TO_ROLE->{ $self->subject_type } });
    }
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
