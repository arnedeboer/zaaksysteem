package Zaaksysteem::Backend::Subject::Component;

use Moose;
use Moose::Util qw/apply_all_roles/;

BEGIN { extends qw/DBIx::Class Catalyst::Authentication::User/; }

with qw/
    Zaaksysteem::JSON::DbAccessors
    Zaaksysteem::Backend::Subject::Role::ACL
/;

use Authen::Passphrase;
use Crypt::SaltedHash;
use Digest::SHA;
use MIME::Base64;
use Zaaksysteem::Tools;

=head1 NAME

Zaaksysteem::Backend::Subject::Component - Implement specific behaviors for
Subject rows.

=head1 DESCRIPTION

=head1 CONSTANTS

=head2 SUBJECT_TYPE_TO_ROLE

This constant is a Hashref that maps the subject type to a specific behavioural
role.

=cut

use constant SUBJECT_TYPE_TO_ROLE => {
    'employee'      => ['Zaaksysteem::Backend::Subject::Employee'],
};

=head2 DEPRECATED_IDENTIFIER_MAPPING

This constant is a Hashref that maps a subject type to the associated
deprecated typename.

=cut

use constant DEPRECATED_IDENTIFIER_MAPPING => {
    employee => 'medewerker',
};

=head1 ATTRIBUTES

=head2 login_entity

=cut

has login_entity => (
    is => 'rw',
);

has preferred_group => (
    is      => 'rw',
    lazy    => 1,
    default => sub {
        my $self            = shift;

        return unless $self->properties->{preferred_group_id};

        return $self->result_source->schema->resultset('Groups')->find(
            $self->properties->{preferred_group_id}
        );
    }
);

=head2 check_password

Arguments: $password

Return value: $TRUE_OR_FALSE

    print $self->check_password('jaddaj@dda')

    # Prints '1' on success

Validates the password according to RFC2307. In other words, it will look at the
first few characters in the database, to find out the hashing algorythm:

    {SSHA}DjSkeWeacd8a9dc8a9ASDf8dadscfasdaa9dSddsdf8D

    or

    {SHA}DjSkeWeacd8a9dc8a9ASDf8dadscfasdaa9dSddsdf8D

    etc.

=cut


sub check_password {
    my $self        = shift;
    my $password    = shift;

    my $authldap    = $self->result_source->schema->resultset('Interface')->search_active(
        {
            module => 'authldap'
        }
    )->first;

    my @entities    = $self->user_entities->search(
        {
            source_interface_id => $authldap->id,
        }
    );

    for my $entity (@entities) {
        my $entity_password = $entity->password;

        my $ppr = Authen::Passphrase->from_rfc2307($entity->password);

        return 1 if $ppr->match($password);
    }

    return;
};

has 'external_api_objects' => (
    'is'        => 'rw',
    'isa'       => 'ArrayRef',
    'default'   => sub { []; }
);

has 'is_external_api' => (
    'is'        => 'rw',
    'isa'       => 'Bool',
    'default'   => 0,
);

=head2 is_active

=cut

has is_active => (
    is => 'rw',
    lazy => 1,
    default => sub {
        my $self            = shift;

        return $self->user_entities->search(
            {
                date_deleted    => undef,
            }
        )->count ? 1 : undef;
    }
);

=head2 betrokkene_identifier

This attribute lazily builds the deprecated 'betrokkene identifier'. Returns
something like C<betrokkene-medewerker-1234>.

=cut

has betrokkene_identifier => (
    is => 'rw',
    lazy => 1,
    default => sub {
        my $self            = shift;

        return sprintf "betrokkene-%s-%d", (
            DEPRECATED_IDENTIFIER_MAPPING->{$self->subject_type},
            $self->id,
        );
    }
);

=head2 supported_features

This method returns a hashref of feature supported by this object type. It is
a required part of Catalyst's authentication infrastructure.

=cut

sub supported_features {
    return {
        session => 1
    };
}

=head2 TO_JSON

=cut

sub TO_JSON {
    my $self        = shift;

    my $json        = {
        id                  => $self->id,
        external_api_objects => $self->external_api_objects,
        is_external_api     => $self->is_external_api,
    };

    if ($self->login_entity) {
        $json->{login_entity_id}        = $self->login_entity->id
    }

    return $json;
}

=head2 new

Added hook for creating custom accessors for the json data, for more info, see
L<DBIx::Class::Row>

=cut

sub new {
    my $class   = shift;

    my $self    = $class->next::method(@_);

    ### Apply roles depending on subject_types
    $self->_apply_subject_roles;

    $self->_generate_jsonaccessors;

    return $self;
}

=head2 inflate_result

=cut

sub inflate_result {
    my $class   = shift;

    my $self    = $class->next::method(@_);

    $self->_apply_subject_roles;

    $self->_generate_jsonaccessors;

    return $self;
}

=head2 modify_setting

=cut

sub modify_setting {
    my ($self, $key, $value) = @_;

    my $settings = $self->settings;

    $settings->{$key} = $value;

    $self->settings($settings);
    $self->update;
}

=head2 security_identity

The method implements an interface required by the security infrastructure, it
plainly returns C<< user => $username >>.

For more info, see L<Zaaksysteem::Object::Model>.

=cut

sub security_identity {
    return ('user', shift->username);
}

=head2 matches_security_rule

This method returns a true value if the provided security rule matches
the user instance. It does this intelligently by walking up the group tree
the user is in, and cross-checking the roles currently in the group/role DB.

=head3 Parameters

=over 4

=item rule

This required parameter is expected to be a
L<Zaaksysteem::Object::SecurityRule> instance. The subject instance is
compared to this datum.

=back

=cut

define_profile matches_security_rule => (
    required => {
        rule => 'Zaaksysteem::Object::SecurityRule',
    }
);

sub matches_security_rule {
    my ($self, %params) = @_;

    my $opts = assert_profile(\%params)->valid;

    my $identity = $opts->{ rule }->entity;

    # Simple case for user
    if($identity->entity_type eq 'user') {
        return $identity->entity_id eq $self->username;
    }

    if($identity->entity_type eq 'position') {
        my ($ou_id, $role_id) = split m[\|], $identity->entity_id;

        # ou_id may be 0, indicating "all OUs", in which case it implicitly matches
        if($ou_id) {
            return unless grep { $_->id eq $ou_id } @{ $self->primary_groups }, @{ $self->inherited_groups };
        }

        # Contrast with groups, which we do not support wildcards for
        return unless grep { $_->id eq $role_id } @{ $self->roles };

        return 1;
    }

    if ($identity->entity_type eq 'group') {
        return unless grep {
            $_->id eq $identity->entity_id
        } @{ $self->primary_groups }, @{ $self->inherited_groups };

        return 1;
    }

    if ($identity->entity_type eq 'role') {
        return unless grep {
            $_->id eq $identity->entity_id
        } @{ $self->roles };

        return 1;
    }

    throw('subject/acl/unimplemented_entity_type', sprintf(
        'Unsupported security identity entity_type "%s", security rule matching failed.',
        $identity->entity_type
    ));
}

=head2 remove_inavigator_settings

settings are stored per casetype. this clears out settings for a specific casetype

=cut

sub remove_inavigator_settings {
    my ($self, $code) = @_;

    throw('subject/remove_inavigator_settings/missing_code',
        'Zaaktypecode is verplicht') unless $code;

    my $current = $self->settings->{inavigator_settings} || {};

    map { delete $current->{$_}->{$code} } keys %$current;

    $self->modify_setting('inavigator_settings', $current);
}

=head1 INTERNAL METHODS

=head2 _apply_subject_roles

=cut

sub _apply_subject_roles {
    my $self        = shift;

    if (SUBJECT_TYPE_TO_ROLE->{ $self->subject_type}) {
        apply_all_roles($self, @{ SUBJECT_TYPE_TO_ROLE->{ $self->subject_type} });
    }
}

=head2 update_user

Arguments: \%PARAMS, $USER_ENTITY_ROW

    $self->update_user(
        {
            group_id            => 44,
            set_behandelaar     => 1,
            username            => 'dedon',
            sn                  => 'Don',
            initials            => 'D',
            [...]
        }
    )

Will update the user with the given params.

B<Acties>

=over 4

=item set_behandelaar

Will make sure this row gets the behandelaar row, so it can login

=back

=cut

sub update_user {
    my $self        = shift;
    my $opts        = shift;
    my $entity      = shift;

    if ($opts->{group_id}) {
        my $group       = $self->result_source->schema->resultset('Groups')->find($opts->{group_id});

        throw(
            'subject/create_user/group_not_found',
            'Cannot find requested group for id: ' . $opts->{group_id}
        ) unless $group;

        $self->group_ids([ $group->id ]);
    }

    ### Behandelaar roles
    my $roles = $self->role_ids || [];
    if ($opts->{set_behandelaar}) {
        my $behandelaar_role    = $self->result_source->schema->resultset('Roles')->find(
            {
                name            => 'Behandelaar',
                system_role     => 1,
            }
        );

        throw(
            'subject/create_user/no_behandelaar_role',
            'Cannot find system role "Behandelaar"'
        ) unless $behandelaar_role;

        if (!grep { $behandelaar_role->id == $_ } @{ $roles }) {
            push(@$roles, $behandelaar_role->id);
        }
    }

    ### Set roles if given
    if ($opts->{role_ids}) {
        push(@$roles, @{ $opts->{role_ids} });
    }

    $self->role_ids($roles);

    ### Update subject properties
    for my $key (keys %$opts) {
        next if $key =~ /^(?:subject_type|interface|role_ids)$/;
        $self->$key($opts->{$key});
    }
    $self->update;

    if ($entity) {
        if ($opts->{username}) {
            $entity->source_identifier($opts->{username});
            $entity->update;
        }

        $self->update_password($entity, $opts);
    }

    return $self;
}

=head2 update_password

Arguments: $USER_ENTITY_ROW, \%OPTS

    $self->update_password($USER_ENTITY, { password => 'p@ssword'});

Will update the password for this subject in the belonging user_entity object

=cut

sub update_password {
    my $self        = shift;
    my $user_entity = shift;
    my $opts        = shift;

    return unless $opts->{password};

    my $hashed_pass;
    if ($opts->{password} =~ /^\{S?SHA\}/) {
        $hashed_pass    = $opts->{password};
    } else {
        my $csh = Crypt::SaltedHash->new(algorithm => 'SHA-1');
        $csh->add($opts->{password});

        $hashed_pass = $csh->generate();
    }

    $user_entity->password($hashed_pass);
    $user_entity->update;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

