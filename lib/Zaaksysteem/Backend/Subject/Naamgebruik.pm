package Zaaksysteem::Backend::Subject::Naamgebruik;

use strict;
use warnings;
use Zaaksysteem::Profile;

use Exporter 'import';
our @EXPORT_OK = qw/naamgebruik/;

=head2 naamgebruik

Determine the naming convention for a person. For instance
the name of a married woman.

Returns a formatted name.

=cut

define_profile naamgebruik => (
    required => [qw//],
    optional => [qw/
        aanduiding
        voorvoegsel
        geslachtsnaam
        partner_voorvoegsel
        partner_geslachtsnaam
    /],
    constraint_methods => {
        aanduiding => sub {
            my ($dfv, $value) = @_;
            return $value eq '' || uc($value) ~~ [qw/P V N E/],
        }
    }
);
sub naamgebruik {
    my $params = assert_profile(shift)->valid;

    my $aanduiding = uc($params->{aanduiding} || '');
    my $dispatch_table = {
        'P' => sub { partner($params) },
        'V' => sub { partner($params) . '-' . eigen($params) },
        'N' => sub { eigen($params) . '-' . partner($params) },
        'E' => sub { eigen($params) },
    };

    return exists $dispatch_table->{$aanduiding} ?
        $dispatch_table->{$aanduiding}->($params) :
        eigen($params);
}

sub separate {
    my $voorvoegsel = shift;
    return $voorvoegsel ? $voorvoegsel . ' ' : '';
}

sub partner {
    my $params = shift;
    return separate($params->{partner_voorvoegsel}) . ($params->{partner_geslachtsnaam} || '');
}

sub eigen {
    my $params = shift;
    return separate($params->{voorvoegsel}) . ($params->{geslachtsnaam} || '');
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 eigen

TODO: Fix the POD

=cut

=head2 partner

TODO: Fix the POD

=cut

=head2 separate

TODO: Fix the POD

=cut

