package Zaaksysteem::Backend::Directory::Component;

use strict;
use warnings;

use Moose;
use Params::Profile;
use Zaaksysteem::Constants;

extends 'DBIx::Class';

use Exception::Class (
    'Zaaksysteem::Backend::Directory::Component::Exception' => {fields => 'code'},
    'Zaaksysteem::Backend::Directory::Component::Exception::General' => {
        isa         => 'Zaaksysteem::Backend::Directory::Component::Exception',
        description => 'General exception',
        alias       => 'throw_general_exception',
    },
    'Zaaksysteem::Backend::Directory::Component::Exception::Parameter' => {
        isa         => 'Zaaksysteem::Backend::Directory::Component::Exception',
        description => 'Parameter exception',
        alias       => 'throw_parameter_exception',
    },
    'Zaaksysteem::Backend::Directory::Component::Exception::Logic' => {
        isa         => 'Zaaksysteem::Backend::Directory::Component::Exception',
        description => 'Logic error',
        alias       => 'throw_logic_exception',
    },
);

=head2 update_properties

Updates the properties of a Directory entry.

=head3 Arguments

=over

=item name [optional]

The new name of the directory.

=item case_id [optional]

The case this directory belongs to.

=back

=head3 Returns

The updated Directory object.

=cut

Params::Profile->register_profile(
    method  => 'update_properties',
    profile => {
        required => [qw/
        /],
        optional => [qw/
            name
            case_id
        /],
        constraint_methods => {
            name => qr/\w+/,
            case_id => qr/^\d+$/,
        },
    }
);

sub update_properties {
    my ($self, $opts) = @_;

    my $dv = Params::Profile->check(params  => $opts);

    my $valid = $dv->valid;

    if ($dv->has_invalid) {
        my @invalid = join ',',$dv->invalid;
        throw_parameter_exception(
            code  => '/directory/update_properties/invalid_parameters',
            error => "Invalid options given: @invalid",
        );
    }

    my $rs = $self->result_source->schema->resultset('Directory');

    my $new = {
        name    => $valid->{name}    || $self->name,
        case_id => $valid->{case_id} || $self->case->id
    };

    # check if name is valid
    die "invalid name given" unless $rs->validate_name($new);


    # Check if name + case are already taken
    if ($rs->find($new)) {
        throw_logic_exception(
            code  => '/directory/update_properties/directory_exists',
            error => sprintf("Directory with name %s and case %d already exists",
                (
                    $valid->{name} || $self->name,
                    $valid->{case_id} || $self->case->id,
                )
            ),
        );
    }

    return $self->update($valid);
}

=head2 delete

Extends the default delete method for Directory to check if there are still
files using this directory. Without this an ugly DBIx::Class exception would get
thrown.

=cut

sub delete {
    my $self = shift;

    if (my @files = $self->files) {
        throw_general_exception(
            code  => '/directory/delete/not_empty',
            error => 'There are still files referencing this directory',
        );
    }

    return $self->next::method(@_);
}

sub TO_JSON {
    my $self            = shift;

    return { $self->get_columns };
}





__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 TO_JSON

TODO: Fix the POD

=cut

