package Zaaksysteem::Backend::Message::ResultSet;

use Moose;
use Zaaksysteem::Exception;
use Zaaksysteem::Profile;

extends 'Zaaksysteem::Backend::ResultSet';

=head2 message_create

Add a message entry to the database.

=head3 Arguments

=over

=item case_id [required]

=item event_type [required]

=item message [required]

=item subject_id [optional]

=back

=head3 Returns

A newly created Sysin::Message object.

=cut


Params::Profile->register_profile(
    method  => 'message_create',
    profile => {
        required => [qw/
            event_type
            message
        /],
        optional => [qw/
            subject_id
            case_id
            data
        /]
    }
);

sub message_create {
    my $self   = shift;
    my $opts   = assert_profile(
        {
            %{ $_[0] },
            schema  => $self->result_source->schema,
        },
    )->valid;

    my $log = $self->result_source->schema->resultset('Logging')->trigger(
        $opts->{event_type}, {
            component => 'zaak',
            $opts->{case_id} ? (zaak_id   => $opts->{case_id}) : (),
            data => {
                $opts->{case_id} ? (case_id => $opts->{case_id}) : (),
                content => $opts->{message},
                %{ $opts->{data} || {} }
            }
        }
    );

    return $self->create({
        message => $opts->{message},
        subject_id => $opts->{subject_id},
        logging_id => $log->id
    });
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

