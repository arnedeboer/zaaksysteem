package Zaaksysteem::Backend::Config::ResultSet;

use Moose;

BEGIN { extends 'DBIx::Class::ResultSet'; }

use constant STANDARD_PARAMETERS => qw/
    users_can_change_password
    signature_upload_role
    allocation_notification_template_id
    pdf_annotations_public
    requestor_search_extension_active
    requestor_search_extension_href
    requestor_search_extension_name
/;

use constant ADVANCED_PARAMETERS => qw/
    jodconverter_url
    public_manpage
/;


sub get_value {
    my ($self, $parameter) = @_;

    die "need parameter" unless $parameter;

    my $row = $self->search({ parameter => $parameter })->first;

    return $row->value if $row;
}

sub get {
    my $self = shift;
    my $retval = undef;

    eval {
        $retval = $self->get_value(@_);
    };

    return $retval;
}

sub save {
    my ($self, $params, $advanced) = @_;

    my $config = $self->apply_defaults($params, $advanced);

    while (my ($key, $value) = each %$config) {
        next if $key eq 'advanced'; # advanced thingy needs to go bye bye

        my $row = $self->find_or_create({parameter => $key, advanced => $advanced});

        if ($row) {
            $row->value($value);
            $row->update;
        }
    }
}

sub get_all {
    my ($self, $advanced) = @_;

    my @settings = $self->search({advanced => $advanced})->all;

    my $config = { map { $_->parameter => $_->value } @settings };

    return $self->apply_defaults($config, $advanced);
}

sub apply_defaults {
    my ($self, $config, $advanced) = @_;

    $config->{$_} ||= '' for ($advanced ? ADVANCED_PARAMETERS : STANDARD_PARAMETERS);

    return $config;
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 apply_defaults

TODO: Fix the POD

=cut

=head2 get

TODO: Fix the POD

=cut

=head2 get_all

TODO: Fix the POD

=cut

=head2 get_value

TODO: Fix the POD

=cut

=head2 save

TODO: Fix the POD

=cut

