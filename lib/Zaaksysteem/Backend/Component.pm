package Zaaksysteem::Backend::Component;

use Moose;

extends 'DBIx::Class::Row';

with 'Zaaksysteem::Backend::Component::Searchable';

=head1 NAME

Zaaksysteem::Backend::Component - Generic Component for _all_ schema files

=head1 SYNOPSIS

 $json->_json_data({})      # Attr: Contains all json_data for this row
 $json->TO_JSON()           # Returns _json_data with other data defined in Roles

=head1 DESCRIPTION

These Component methods provides some generic methods as an extension
of L<DBIx::Class>.

=head1 ATTRIBUTES

=head2 _json_data [optional]

ISA: HashRef

=cut

has '_json_data'    => (
    is          => 'rw',
    isa         => 'HashRef',
    default     => sub { return {}; },
    lazy        => 1,
);

=head1 METHODS

=head2 $self->TO_JSON

Return value: $HashRef

This call depends on a set attribute: C<< $rs->_json_data >>. When set, it will
return all data in _json_data. When C<< $rs->_json_data >> contains an empty
HashRef, it will try to use the TO_JSON functionality from L<DBIx::Class>

=cut

sub TO_JSON {
    my $self        = shift;

    unless (DateTime->can('TO_JSON')) {
        no strict 'refs';
        *DateTime::TO_JSON = sub {shift->iso8601};

        use strict;
    }

    return $self->next::method(@_) unless (
        ref $self->_json_data eq 'HASH' &&
        scalar(keys %{ $self->_json_data }) > 0
    );

    return $self->_json_data;
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::API>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
