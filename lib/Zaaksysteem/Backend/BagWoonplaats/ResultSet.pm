package Zaaksysteem::Backend::BagWoonplaats::ResultSet;

use Moose;

use Zaaksysteem::Exception;
use Zaaksysteem::Profile;

use Zaaksysteem::Backend::Sysin::Transaction::Mutation;

extends 'Zaaksysteem::SBUS::ResultSet::BAG';


use constant VERBLIJFSOBJECT_TYPE   => {
    1       => 'BagVerblijfsobject',
    2       => 'BagLigplaats',
    3       => 'BagStandplaats',
    99      => 'Overig'
};

use constant VERBLIJFSOBJECT_TYPE_LABEL   => {
    1       => 'Verblijfsobject',
    2       => 'Ligplaats',
    3       => 'Standplaats',
    99      => 'Overig'
};

use constant STUF_STATUS_CODES          => {
    1   => 'Woonplaats aangewezen',
    2   => 'Woonplaats ingetrokken',
    3   => 'Naamgeving uitgegeven',
    4   => 'Naamgeving ingetrokken',
    5   => 'Naamgeving uitgegeven',
    6   => 'Naamgeving ingetrokken',
    7   => 'Ligplaats aangewezen',
    8   => 'Ligplaats ingetrokken',
    9   => 'Standplaats aangewezen',
    10  => 'Standplaats ingetrokken',
    11  => 'Verblijfsobject gevormd',
    12  => 'Niet gerealiseerd verblijfsobject',
    13  => 'Verblijfsobject in gebruik (niet ingemeten)',
    14  => 'Verblijfsobject in gebruik',
    15  => 'Verblijfsobject ingetrokken',
    16  => 'Verblijfsobject buiten gebruik',
    17  => 'Bouwvergunning verleend',
    18  => 'Niet gerealiseerd pand',
    19  => 'Bouw gestart',
    20  => 'Pand in gebruik (niet ingemeten)',
    21  => 'Pand in gebruik',
    22  => 'Sloopvergunning verleend',
    23  => 'Pand gesloopt',
    24  => 'Pand buiten gebruik',
};

=head2 bag_create_or_update

Creates a complete bag entry into the database

=cut

Params::Profile->register_profile(
    method  => 'bag_create_or_update',
    profile => {
        missing_optional_valid  => 1,
        required => [qw/
        /],
        optional => [qw/
            woonplaats_identificatie
            woonplaats_begindatum
            woonplaats_einddatum
            woonplaats_officieel
            woonplaats_status
            woonplaats_inonderzoek

            openbareruimte_identificatie
            openbareruimte_begindatum
            openbareruimte_einddatum
            openbareruimte_inonderzoek
            openbareruimte_type
            openbareruimte_status

            nummeraanduiding_identificatie
            nummeraanduiding_begindatum
            nummeraanduiding_einddatum
            nummeraanduiding_huisletter
            nummeraanduiding_huisnummertoevoeging
            nummeraanduiding_postcode
            nummeraanduiding_inonderzoek
            nummeraanduiding_status

            nummeraanduiding_id

            verblijfsobject_identificatie
            verblijfsobject_status
            verblijfsobject_gebruiksdoel
            verblijfsobject_begindatum
            verblijfsobject_einddatum
            verblijfsobject_oppervlakte
            verblijfsobject_status
            verblijfsobject_inonderzoek

            verblijfsobject_id

            pand_identificatie
            pand_begindatum
            pand_einddatum
            pand_status
            pand_inonderzoek
            pand_bouwjaar
        /],
        require_some           => {
            woonplaats_of_nummeraanduiding => [1, qw/openbareruimte_identificatie woonplaats_identificatie/],
        },
        dependencies                => {
            woonplaats_identificatie    => [qw/
                woonplaats_begindatum
                woonplaats_naam
                woonplaats_status
                woonplaats_inonderzoek
            /],
            openbareruimte_identificatie    => [qw/
                openbareruimte_naam
                openbareruimte_begindatum
                openbareruimte_type
                openbareruimte_status
                openbareruimte_inonderzoek
            /],
            nummeraanduiding_identificatie  => [qw/
                nummeraanduiding_huisnummer
                nummeraanduiding_type
                nummeraanduiding_begindatum
                nummeraanduiding_status
                nummeraanduiding_inonderzoek
            /],
            verblijfsobject_status          => [qw/
                verblijfsobject_identificatie
                verblijfsobject_status
            /],
            pand_identificatie              => [qw/
                pand_begindatum
                pand_status
                pand_inonderzoek
                pand_bouwjaar
            /],
        },
        constraint_methods          => {
            woonplaats_identificatie            => qr/^\d{1,4}$/,
            woonplaats_begindatum               => qr/^\d{4}-\d{2}-\d{2}$/,
            woonplaats_einddatum                => qr/^\d{4}-\d{2}-\d{2}$/,
            woonplaats_inonderzoek              => qr/^J|N$/,


            openbareruimte_identificatie        => qr/^\d{16}$/,
            openbareruimte_begindatum           => qr/^\d{4}-\d{2}-\d{2}$/,
            openbareruimte_einddatum            => qr/^\d{4}-\d{2}-\d{2}$/,
            openbareruimte_inonderzoek          => qr/^J|N$/,



            nummeraanduiding_identificatie      => qr/^\d{16}$/,
            nummeraanduiding_begindatum         => qr/^\d{4}-\d{2}-\d{2}$/,
            nummeraanduiding_einddatum          => qr/^\d{4}-\d{2}-\d{2}$/,
            nummeraanduiding_huisnummer         => qr/^\d+$/,
            nummeraanduiding_huisnummertoevoeging => qr/^.{1,4}$/,
            nummeraanduiding_postcode           => qr/^\d{4}[a-zA-Z]{2}$/,
            nummeraanduiding_inonderzoek        => qr/^J|N$/,
            nummeraanduiding_type               => qr/^Verblijfsobject|Ligplaats|Standplaats|Overig$/,

            verblijfsobject_identificatie       => qr/^\d{16}$/,
            verblijfsobject_begindatum          => qr/^\d{4}-\d{2}-\d{2}$/,
            verblijfsobject_einddatum           => qr/^\d{4}-\d{2}-\d{2}$/,
            verblijfsobject_oppervlakte         => qr/^\d+$/,
            verblijfsobject_inonderzoek         => qr/^J|N$/,

            pand_identificatie                  => qr/^\d{16}$/,
            pand_begindatum                     => qr/^\d{4}-\d{2}-\d{2}$/,
            pand_einddatum                      => qr/^\d{4}-\d{2}-\d{2}$/,
            pand_inonderzoek                    => qr/^J|N$/,
            pand_bouwjaar                       => qr/^\d+$/,
        },
        field_filters           => {
            nummeraanduiding_type           => sub {
                my $val         = shift;

                return unless $val;

                return ucfirst($val);
            },
            nummeraanduiding_begindatum     => sub {
                my $val         = shift;

                return $val unless $val =~ /^\d+/;

                $val            =~ s/(\d{4})(\d{2})(\d{2})/$1-$2-$3/;

                return $val;
            },
            woonplaats_status               => sub {
                my $val         = shift;

                return $val unless $val =~ /^\d+$/;

                ### Cast to numeric
                $val            = $val + 0;

                return STUF_STATUS_CODES->{$val};
            },
            openbareruimte_status           => sub {
                my $val         = shift;

                return $val unless $val =~ /^\d+$/;

                ### Cast to numeric
                $val            = $val + 0;

                return STUF_STATUS_CODES->{$val};
            },
            verblijfsobject_status          => sub {
                my $val         = shift;

                return $val unless $val =~ /^\d+$/;

                ### Cast to numeric
                $val            = $val + 0;

                return STUF_STATUS_CODES->{$val};
            },
            nummeraanduiding_status         => sub {
                my $val         = shift;

                return $val unless $val =~ /^\d+$/;

                ### Cast to numeric
                $val            = $val + 0;

                return STUF_STATUS_CODES->{$val};
            },
            pand_status                     => sub {
                my $val         = shift;

                return $val unless $val =~ /^\d+$/;

                ### Cast to numeric
                $val            = $val + 0;

                return STUF_STATUS_CODES->{$val};
            },
        },
        defaults                => {
            woonplaats_begindatum               => '1990-01-01',
            woonplaats_status                   => 'Woonplaats aangewezen',
            woonplaats_inonderzoek              => 'N',

            openbareruimte_begindatum           => '1990-01-01',
            openbareruimte_status               => 'Naamgeving uitgegeven',
            openbareruimte_type                 => 'Weg',
            openbareruimte_inonderzoek          => 'N',

            nummeraanduiding_begindatum         => '1990-01-01',
            nummeraanduiding_status             => 'Naamgeving uitgegeven',
            nummeraanduiding_inonderzoek        => 'N',

            verblijfsobject_begindatum          => '1990-01-01',
            verblijfsobject_inonderzoek         => 'N',

            pand_begindatum                     => '1990-01-01',
            nummeraanduiding_type               => sub {
                my ($dfv) = @_;

                return 'Verblijfsobject' unless $dfv->get_filtered_data->{'verblijfsobject_identificatie'};

                my ($typeid) = $dfv->get_filtered_data->{'verblijfsobject_identificatie'}
                    =~ /^\d{5}(\d)/;

                return (VERBLIJFSOBJECT_TYPE_LABEL->{ $typeid } || VERBLIJFSOBJECT_TYPE_LABEL->{ 99 });
            },
        },
    }
);

sub bag_create_or_update {
    my $self                        = shift;
    my $opts                        = shift;
    my $config                      = shift || {};

    my $return_entry;

    $opts                           = assert_profile(
        {
            %{ $opts },
        }
    )->valid;

    ### Top down
    my $mutations                   = [];

    my $narrowest_object;

    ### Create Woonplaats
    my $woonplaats                  = $self->bag_woonplaats_create_or_update(
        $opts,
        $mutations,
        @_
    );

    if ($woonplaats) {
        $narrowest_object   = $woonplaats;

        $return_entry       = $woonplaats if (
            $config->{return_entry} &&
            $config->{return_entry} eq 'BagWoonplaats'
        );
    }

    ### Create Openbare Ruimte
    my $openbareruimte;
    if ($opts->{openbareruimte_identificatie}) {
        $openbareruimte              = $self->bag_openbareruimte_create_or_update(
            {
                %$opts,
                openbareruimte_woonplaats      => $woonplaats->identificatie,
            },
            $mutations,
            @_
        );
    }

    if ($openbareruimte) {
        $narrowest_object   = $openbareruimte;

        $return_entry       = $openbareruimte if (
            $config->{return_entry} &&
            $config->{return_entry} eq 'BagOpenbareruimte'
        );
    }

    my $nummeraanduiding;
    if ($opts->{nummeraanduiding_identificatie}) {
        ### Create Nummeraanduiding
        $nummeraanduiding                   = $self->bag_nummeraanduiding_create_or_update(
            {
                %$opts,
                nummeraanduiding_woonplaats         => $woonplaats->identificatie,
                nummeraanduiding_openbareruimte     => $opts->{openbareruimte_identificatie},
            },
            $mutations,
            @_
        );

        if ($nummeraanduiding) {
            $narrowest_object   = $nummeraanduiding;

            $return_entry       = $nummeraanduiding if (
                $config->{return_entry} &&
                $config->{return_entry} eq 'BagNummeraanduiding'
            );
        }
    }

    ### Crate verblijfsobject
    my $verblijfsobject;
    if ($opts->{verblijfsobject_identificatie}) {
        my $verblijfsobject             = $self->bag_verblijfsobject_create_or_update(
            {
                %$opts,
                verblijfsobject_hoofdadres          => $nummeraanduiding->identificatie,
            },
            $mutations,
            @_
        );

        if ($verblijfsobject) {
            $return_entry       = $verblijfsobject if (
                $config->{return_entry} &&
                $config->{return_entry} eq 'BagVerblijfsobject'
            );
        }
    }


    return ($mutations, $narrowest_object, $return_entry);
}

sub bag_woonplaats_create_or_update {
    my $self                = shift;
    my $rawparams           = shift;
    my $mutations           = shift;

    my $resultset           = $self
                            ->result_source
                            ->schema
                            ->resultset('BagWoonplaats');

    my $params              = {
        map {
            my $key     = $_;
            $key        =~ s/^woonplaats_//;
            $key        => $rawparams->{ $_ }
        } grep { $_ =~ /^woonplaats_/ } keys %{ $rawparams }
    };


    ### Workaround when no woonplaats_identificatie is given
    if (
        !$params->{identificatie}
    ) {
        if (!$rawparams->{openbareruimte_identificatie}){
            throw(
                'bagwoonplaats/resultset/create/missing_identification',
                'No woonplaats_identificatie AND openbareruimte_identificatie found'
            );
        }

        my $openbareruimte = $self->result_source->schema->resultset('BagOpenbareruimte')->find(
            {
                identificatie => $rawparams->{openbareruimte_identificatie}
            }
        );


        if (!$openbareruimte){
            throw(
                'bagwoonplaats/resultset/create/no_openbareruimte',
                'No woonplaats_identificatie AND openbareruimte NOT found'
            );
        }

        $params->{identificatie} = $openbareruimte->woonplaats->identificatie;
    }

    return $self->_do_bag_update_or_create(
        $resultset,
        {
            identificatie => $params->{identificatie},
        },
        $mutations,
        $params
    );
}

sub _do_bag_update_or_create {
    my $self                = shift;
    my $resultset           = shift;
    my $find                = shift;
    my $mutations           = shift;
    my $rawparams           = shift;

    my $entry               = $resultset->find($find);

    my $schema_name         = $resultset->result_source->source_name;

    ### Strip not found columns
    my $params              = {};
    for my $key (keys %{ $rawparams }) {
        next unless grep { $key eq $_ } $resultset->result_source->columns;

        $params->{ $key } = $rawparams->{ $key };
    }

    my $mutation_record;
    if ($entry) {
        my %old_values      = $entry->get_columns;
        $mutation_record    = Zaaksysteem::Backend::Sysin::Transaction::Mutation
                            ->new(
                                table       => $schema_name,
                                table_id    => $entry->id,
                                update      => 1
                            );

        ### Mutate row
        $entry->set_inflated_columns($params);
        $entry->update;

        ### Log mutation
        $mutation_record->from_dbix($entry, \%old_values);
    } else {
        ### No entry yet? create it
        $entry                      = $resultset->create($params);

        $mutation_record    = Zaaksysteem::Backend::Sysin::Transaction::Mutation
                            ->new(
                                table       => $schema_name,
                                table_id    => $entry->id,
                                create      => 1
                            );

        $mutation_record->from_dbix($entry);
    }

    push(@$mutations, $mutation_record);

    return $entry;
}

sub bag_openbareruimte_create_or_update {
    my $self                = shift;
    my $rawparams           = shift;
    my $mutations           = shift;

    my $resultset           = $self
                            ->result_source
                            ->schema
                            ->resultset('BagOpenbareruimte');

    my $params              = {
        map {
            my $key     = $_;
            $key        =~ s/^openbareruimte_//;
            $key        => $rawparams->{ $_ }
        } grep { $_ =~ /^openbareruimte_/ } keys %{ $rawparams }
    };

    return $self->_do_bag_update_or_create(
        $resultset,
        {
            identificatie => $params->{identificatie}
        },
        $mutations,
        $params
    );
}

sub bag_nummeraanduiding_create_or_update {
    my $self                = shift;
    my $rawparams           = shift;
    my $mutations           = shift;

    my $resultset           = $self
                            ->result_source
                            ->schema
                            ->resultset('BagNummeraanduiding');

    my $params              = {
        map {
            my $key     = $_;
            $key        =~ s/^nummeraanduiding_//;
            $key        => $rawparams->{ $_ }
        } grep { $_ =~ /^nummeraanduiding_/ } keys %{ $rawparams }
    };

    return $self->_do_bag_update_or_create(
        $resultset,
        {
            identificatie   => $params->{identificatie},

        },
        $mutations,
        $params,
    );
}


sub bag_verblijfsobject_create_or_update {
    my $self                = shift;
    my $rawparams           = shift;
    my $mutations           = shift;

    ### Identify type, is it verblijfsobject, ligplaats or standplaats
    my $params              = {
        map {
            my $key     = $_;
            $key        =~ s/^verblijfsobject_//;
            $key        => $rawparams->{ $_ }
        } grep { $_ =~ /^verblijfsobject_/ } keys %{ $rawparams }
    };

    return unless $params->{status};

    #return unless $params->{identificatie};
    my ($type_id)           = $params->{identificatie} =~ /^\d{5}(\d)/;

    return unless VERBLIJFSOBJECT_TYPE->{ $type_id };

    my $resultset           = $self
                            ->result_source
                            ->schema
                            ->resultset(VERBLIJFSOBJECT_TYPE->{ $type_id });

    if (!$params->{verblijfsobject_begindatum}) {
        $params->{verblijfsobject_begindatum} = '1990-01-01';
    }

    my $entry = $self->_do_bag_update_or_create(
        $resultset,
        {
            identificatie   => $params->{identificatie},
        },
        $mutations,
        $params,
    );

    return $entry unless $type_id == 1;

    ### Update gebruiksdoel

    if ($params->{gebruiksdoel}) {
        my @gebruiksdoelen = (
            UNIVERSAL::isa($params->{gebruiksdoel}, 'ARRAY')
                ? @{ $params->{gebruiksdoel} }
                : $params->{gebruiksdoel}
        );

        $self->_update_gebruiksdoelen(
            $params,
            \@gebruiksdoelen,
            $mutations
        );
    }

    ### Update pand
    return $entry unless $rawparams->{pand_identificatie};

    ### Create pand

    my $pandparams              = {
        map {
            my $key     = $_;
            $key        =~ s/^pand_//;
            $key        => $rawparams->{ $_ }
        } grep { $_ =~ /^pand_/ } keys %{ $rawparams }
    };

    $pandparams->{begindatum} = '1990-01-01' unless $pandparams->{begindatum};

    my $pand                    = $self->_do_bag_update_or_create(
        $self->result_source->schema->resultset('BagPand'),
        {
            identificatie   => $pandparams->{identificatie},
        },
        $mutations,
        $pandparams
    );

    $pandparams->{begindatum} = '1990-01-01' unless $pandparams->{begindatum};

    ### Koppel pand
    my $pand_to_verblijfsobject = $self->_do_bag_update_or_create(
        $self->result_source->schema->resultset('BagVerblijfsobjectPand'),
        {
            identificatie   => $params->{identificatie},
        },
        $mutations,
        {
            begindatum      => '1990-01-01',
            identificatie   => $params->{identificatie},
            pand            => $pandparams->{identificatie},
        }
    );
}

sub _update_gebruiksdoelen {
    my $self            = shift;
    my $params          = shift;
    my $gebruiksdoelen  = shift;
    my $mutations       = shift;


    ### Get gebruiksdoelen
    my @db_doelen       = $self
                        ->result_source
                        ->schema
                        ->resultset('BagVerblijfsobjectGebruiksdoel')
                        ->search(
                            {
                                identificatie => $params->{identificatie},
                                gebruiksdoel  => $gebruiksdoelen,
                            }
                        );

    ### Database count same as gebruiksdoelen, just return, everything ok.
    return if (scalar(@{ $gebruiksdoelen }) == scalar(@db_doelen));

    my @functies = map { $_->gebruiksdoel } @db_doelen;

    for my $gebruiksdoel (@{ $gebruiksdoelen }) {
        ### Gebruiksdoel already in database
        next if grep { $_ eq $gebruiksdoel } @functies;

        my $doel = $self   ->result_source
                ->schema
                ->resultset('BagVerblijfsobjectGebruiksdoel')
                ->create(
                    {
                        identificatie   => $params->{identificatie},
                        gebruiksdoel    => $gebruiksdoel,
                        begindatum      => '1990-01-01'
                    },
                );

        my $mutation_record    = Zaaksysteem::Backend::Sysin::Transaction::Mutation
                            ->new(
                                table       => 'BagVerblijfsobjectGebruiksdoel',
                                table_id    => $doel->id,
                                create      => 1
                            );

        $mutation_record->from_dbix($doel);

        push(@{ $mutations }, $mutation_record);
    }

    ### Delete removed gebruiksdoelen
    $self
        ->result_source
        ->schema
        ->resultset('BagVerblijfsobjectGebruiksdoel')
        ->search(
            {
                identificatie => $params->{identificatie},
                gebruiksdoel  => {
                    'not in' => $gebruiksdoelen,
                }
            }
        )->delete;
}


1;





__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 bag_nummeraanduiding_create_or_update

TODO: Fix the POD

=cut

=head2 bag_openbareruimte_create_or_update

TODO: Fix the POD

=cut

=head2 bag_verblijfsobject_create_or_update

TODO: Fix the POD

=cut

=head2 bag_woonplaats_create_or_update

TODO: Fix the POD

=cut

