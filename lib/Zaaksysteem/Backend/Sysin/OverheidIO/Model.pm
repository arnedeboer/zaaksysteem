package Zaaksysteem::Backend::Sysin::OverheidIO::Model;
use Moose;

=head1 NAME

Zaaksysteem::Backend::Sysin::OverheidIO::Model - A model to talk to Overheid.IO

=head1 SYNOPSIS

    use Zaaksysteem::Backend::Sysin::OverheidIO::Model;
    my $overheidio = Zaaksysteem::Backend::Sysin::OverheidIO::Model->new(
        key => "your developer key",
    );


=cut

use URI;
use IO::Socket::SSL;
use Zaaksysteem::Tools;

with qw(
    MooseX::Log::Log4perl
    Zaaksysteem::Backend::Sysin::Roles::UA
);

=head1 ATTRIBUTES

=head2 username

=head2 password

Both the username and password attributes from L<Zaaksysteem::Backend::Sysin::Roles::UA>
are not required nor used by this API.

=cut

has '+username' => (required => 0);
has '+password' => (required => 0);

=head2 base_uri

The base URI of the Overheid.IO, lazyly loaded.

=cut

has base_uri => (
    is      => 'ro',
    isa     => 'URI',
    builder => '_build_base_uri',
    lazy => 1,
);

sub _build_base_uri {
    my $self = shift;
    return URI->new_abs( "/api/". $self->type, 'https://overheid.io');
}

=head2 max_query_size

The max query size, defaults to 30.

=cut

has max_query_size => (
    is      => 'ro',
    isa     => 'Int',
    default => 30,
);

=head2 key

The required Overheid.IO API key.

=cut

has key => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

=head2 type

The type of Overheid.IO api, defaults to C<kvk>

=cut

has type => (
    is      => 'ro',
    isa     => 'Str',
    default => 'kvk',
);

=head2 fieldnames

The names of the fields which the Overheid.IO will respond with

=cut

has fieldnames => (
    is      => 'rw',
    isa     => 'ArrayRef',
    lazy    => 1,
    default => sub {
        return [
            qw(
                dossiernummer
                handelsnaam
                huisnummer
                huisnummertoevoeging
                plaats
                postcode
                straat
                vestigingsnummer
                )
        ];
    },
);

=head2 queryfields

The names of the fields which will be used to query on

=cut

has queryfields => (
    is      => 'rw',
    isa     => 'ArrayRef',
    lazy    => 1,
    default => sub {
        return [qw(dossiernummer handelsnaam vestigingsnummer subdossier)];
    },
);

around _build_useragent => sub {
    my $orig = shift;
    my $self = shift;

    my $ua = $self->$orig(@_);
    $ua->default_header('ovio-api-key', $self->key);
    return $ua;
};

=head2 search

Search OverheidIO by a search term, you can apply additional filters for zipcodes and such

    $overheidio->search(
        "Mintlab",
        filter => {
            postcode => '1051JL',
        }
    );

=cut

define_profile search => (
    optional => { filter => 'HashRef', },
    defaults => { filter => sub { return {} }, },
);

sub search {
    my $self        = shift;
    my $search_term = shift;
    my $opts        = assert_profile({@_})->valid;

    my $filter = $opts->{filter};
    $filter->{actief} = 'true';

    my %query = map { sprintf('filters[%s]', $_), => $filter->{$_} }
        grep { defined $filter->{$_} } keys %{$filter};

    $query{size} = $self->max_query_size;

    my $uri = $self->base_uri->clone;

    $uri->query_form(
        %query,
        'fields[]'      => $self->fieldnames,
        'queryfields[]' => $self->queryfields,
        query           => $search_term,
    );

    return $self->_call_overheid_io($uri);
}

sub _call_overheid_io {
    my $self = shift;
    my $uri  = shift;

    $self->log->trace('Send Overheid.IO request: ' . $uri->as_string);

    my $res = $self->ua->get($uri->as_string);

    if (!$res->is_success) {
        $self->log->fatal("Overheid.io JSON answer: ", dump_terse($res->decoded_content));
        throw("overheidio/error", $res->status_line, $res);
    }

    my $result = JSON->new->decode($res->decoded_content);

    if ($self->log->is_trace) {
        $self->log->trace("Overheid.io JSON answer: ", dump_terse($res->decoded_content));
        $self->log->trace("Overheid.io decoded answer: ", dump_terse($result));
    }

    return $result;
}


__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
