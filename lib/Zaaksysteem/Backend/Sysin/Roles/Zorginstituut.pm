package Zaaksysteem::Backend::Sysin::Roles::Zorginstituut;
use Moose::Role;

=head1 NAME

Zaaksysteem::Backend::Sysin::Roles::Zorginstituut - A role for Zorginstituut modules

=head1 DESCRIPTION

This role some, minimal, housekeeping for Zorginstituut like modules.

=cut

use Moose::Util::TypeConstraints;
class_type('Zaaksysteem::Backend::Sysin::iWMO::Model');
class_type('Zaaksysteem::Backend::Sysin::iJW::Model');

use Zaaksysteem::Tools;
use Zaaksysteem::Constants;
use Zaaksysteem::XML::Zorginstituut::WMO302;
use Zaaksysteem::XML::Zorginstituut::JW302;
use DateTime::Format::Strptime;

=head1 ATTRIBUTES

=head2 instance

A required instance of a model from one of the following objects:

=over

=item L<Zaaksysteem::Backend::Sysin::iWMO::Model>

=item L<Zaaksysteem::Backend::Sysin::iJW::Model>

=back

=cut

has instance => (
    is => 'ro',
    isa =>
        'Zaaksysteem::Backend::Sysin::iWMO::Model|Zaaksysteem::Backend::Sysin::iJW::Model',
    required => 1,
);

=head2 spoof

Spoof mode, disable enable things for easy testing

=cut

has spoof => (
    is      => 'ro',
    isa     => 'Bool',
    default => 0,
);

=head2 converter

A required instance of a L<Zaaksysteem::Backend::Sysin::Zorginstituut::Converter> object

=cut

has converter => (
    is        => 'ro',
    isa       => 'Zaaksysteem::Backend::Sysin::Zorginstituut::Converter',
    predicate => 'has_converter',
);

=head2 synchronous

Boolean value to indicate the calls to the external partner are synchronus or not. If the call is synchronus we get the reponse directly and validate that directly as well.

=cut

has synchronous => (
    is      => 'ro',
    isa     => 'Bool',
    default => 0,
);

=head2 berichtcode

A berichtcode

=cut

has berichtcode => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

=head2 berichttype

A berichttype

=cut

has berichttype => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

=head2 berichtidentificatienummer

The ID found in our object subscription table to match the message to the response

=cut

has berichtidentificatienummer => (
    is  => 'rw',
    isa => 'Int',
);

=head2 case

A case type object L<Zaaksysteem::Schema::Zaak>

=cut

has case => (
    is  => 'rw',
    isa => 'Zaaksysteem::Schema::Zaak',
);

=head2 attribute_data

Attribute data from a case, fetched via an interface mapping

=cut

has attribute_data => (
    is      => 'ro',
    isa     => 'HashRef',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return $self->interface->get_mapped_attributes_from_case($self->case);
    }
);

=head2 is_start

Boolean value which indicates that the message is a C<start> message

=cut

has is_start => (
    is      => 'ro',
    isa     => 'Bool',
    lazy    => 1,
    default => sub {
        my $self = shift;
        if ($self->attribute_data->{aanbieder}) {
            return 1;
        }
        return 0;
    }
);

=head2 is_stop

Boolean value which indicates that the message is a C<stop> message

=cut

has is_stop => (
    is      => 'ro',
    isa     => 'Bool',
    lazy    => 1,
    default => sub {
        my $self    = shift;
        my $stop_id = $self->attribute_data->{beschikkingsnummer};

        return if (!$stop_id);

        if ($stop_id ne $self->beschikkingsnummer) {
            return 1;
        }
        return 0;
    }
);

=head2 beschikkingsnummer

The number of the beschikking, is derived from the case ID

=cut

has beschikkingsnummer => (
    is      => 'ro',
    isa     => 'Int',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return $self->case->id;
    }
);

=head2 product_enddates

HashRef of which fields are product enddates

=cut

has product_enddates => (
    is      => 'ro',
    isa     => 'HashRef',
    lazy    => 1,
    default => sub {
        return {
            toegewezen_product_einddatum => 'toegewezen_product_ingangsdatum',
            toegewezen_product_einddatum_1 =>
                'toegewezen_product_ingangsdatum_1',
            toegewezen_product_einddatum_2 =>
                'toegewezen_product_ingangsdatum_2',
            toegewezen_product_einddatum_3 =>
                'toegewezen_product_ingangsdatum_3',
        };
    },
);

=head2 product_endreasons

HashRef of which fields are product end reasons

=cut

has product_endreasons => (
    is      => 'ro',
    isa     => 'HashRef',
    lazy    => 1,
    default => sub {
        return {
            toegewezen_product_reden_intrekking =>
                'toegewezen_product_ingangsdatum',
            toegewezen_product_reden_intrekking_1 =>
                'toegewezen_product_ingangsdatum_1',
            toegewezen_product_reden_intrekking_2 =>
                'toegewezen_product_ingangsdatum_2',
            toegewezen_product_reden_intrekking_3 =>
                'toegewezen_product_ingangsdatum_3',
        };
    },
);

=head2 dtf

An L<DateTime::Format::Strptime> object. Defaults to one with a set pattern of:
C<%d-%m-%Y>

=cut

has dtf => (
    is => 'ro',
    isa => 'DateTime::Format::Strptime',
    default => sub {
        my $self = shift;
        return DateTime::Format::Strptime->new(pattern => '%d-%m-%Y');
    },
);


=head1 METHODS

=head2 send_301_message

Send a 301 message from the case

=cut

sig send_301_message => 'Str';

sub send_301_message {
    my ($self, $xml) = @_;

    return 1 if $self->spoof;

    if ($self->has_converter) {
        $xml = $self->converter->to_ascii($xml);
    }

    return $self->_send_301_message(
        endpoint => $self->endpoints->{message},
        xml      => $xml,
    );
}

=head2 validate_synchronous

Validate and log a message if the interface is synchronous.

=cut

sub validate_synchronous {
    my ($self, $xml) = @_;
    if ($self->synchronous) {
        return $self->validate_302_message($xml);
    }
    return undef;
}

=head2 validate_302_message

Validate a message and log the result to a case.

=cut

sub validate_302_message {
    my ($self, $xml) = @_;

    if ($self->has_converter && !$self->spoof) {
        $xml = $self->converter->to_xml($xml);
    }

    my $type = $self->berichttype;
    my $validator;

    if ($type eq 'wmo301') {
        $validator
            = Zaaksysteem::XML::Zorginstituut::WMO302->new(xml => $xml);
    }
    elsif ($type eq 'jw301') {
        $validator = Zaaksysteem::XML::Zorginstituut::JW302->new(xml => $xml);
    }

    my $valid = $validator->is_valid();

    if (!$self->spoof) {
        $self->berichtidentificatienummer($validator->berichtidentificatienummer);
    }

    if (!$self->case) {
        my $nummer = $self->berichtidentificatienummer;
        my $rs = $self->schema->resultset('ObjectSubscription')->search_rs({
                external_id  => $nummer,
                interface_id => $self->interface->id,
                local_table  => 'Zaak',
        });

        my $os = $rs->next;

        if (!$os) {
            throw('Zorginstituut/berichtidentificatie/notfound', "$nummer cannot be matched against a case");
        }
        elsif ($rs->next) {
            throw('Zorginstituut/berichtidentificatie/duplicate', "$nummer can is linked to multiple cases");
        }

        my $case = $self->schema->resultset('Zaak')->find($os->get_column('local_id'));
        if (!$case || $case->is_afgehandeld) {
            throw(
                'Zorginstituut/berichtidentificatie/no case',
                "No open case can be found by object_subscription: $nummer",
            );
        }
        $self->case($case);
    }

    return $self->log_302_to_case(
        errors => !$valid ? $validator->show_errors() : undef,
        xml    => $xml,
        type   => $self->berichttype,
        code   => $self->berichtcode,
        case_id => $self->case->id,
    );
}

=head2 log_302_to_case

Trigger logging and create a user notification for the 302 message.

=cut

sub log_302_to_case {
    my ($self, %data) = @_;

    my $event = $self->case->trigger_logging('case/update/zorginstituut',
        { component => LOGGING_COMPONENT_ZAAK, data => \%data });

    my $msg = sprintf(
        "%s antwoordbericht is ontvangen voor zaak %d",
        uc($self->berichttype),
        $self->case->id
    );

    $self->case->create_message_for_behandelaar(
        event_type => 'case/update/zorginstituut',
        message    => $msg,
        log        => $event,
    );
    return $msg;
}

=head2 _send_301_message

You want to override this function if the API is different.

=cut

define_profile _send_301_message => (
    required => {
        endpoint => 'Str',
        xml      => 'Str',
    },
);

sub _send_301_message {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    return $self->call(
        endpoint => $opts->{endpoint},
        message  => $opts->{xml},
    );
}

sub _assert_case {
    my ($self, $id) = @_;
    my $case = $self->schema->resultset('Zaak')->find($id);
    return $case if $case;

    throw("Zorginstituut/case/not_found",
        "Unable to find case by number $id");
}


sub start_xml {
    my $self = shift;
    if (!$self->is_start) {
        throw('zorginstituut/message/start/invalid',
            "Unable to generate start");
    }

    my $data = $self->attribute_data;

    $self->_set_earliest_product_enddates($data);

    return $self->instance->encode_301_message(
        berichtcode => $self->berichtcode,
        case        => $self->case,
        data        => $data,
    );
}

sub stop_xml {
    my $self = shift;
    if (!$self->is_stop) {
        throw('zorginstituut/message/stop/invalid',
            "Unable to generate stop");
    }

    my $orig = $self->attribute_data;
    my $case = $self->_assert_case($orig->{beschikkingsnummer});
    my $data = $self->interface->get_mapped_attributes_from_case($case);

    $data->{beschikkingseinddatum} = $orig->{stop_beschikkingseinddatum};

    $self->_set_earliest_product_enddates($data, $orig);
    $self->_set_product_end_reasons($data, $orig);

    return $self->instance->encode_301_message(
        berichtcode => $self->berichtcode,
        case        => $case,
        data        => $data,
    );
}

=head2 _set_product_end_reasons

Set the product end reasons

=cut

sub _set_product_end_reasons {
    my ($self, $data, $orig) = @_;

    return unless $orig;

    my %reasons = %{ $self->product_endreasons };

    foreach (keys %reasons) {
        next if !exists $data->{ $reasons{$_} };

        if ($orig->{stop_reden_intrekking_algemeen}) {
            $data->{$_} = $orig->{stop_reden_intrekking_algemeen};
        }
        elsif ($orig->{"stop_$_"}) {
            $data->{$_} = $orig->{"stop_$_"};
        }
    }
}

=head2 _set_earliest_product_enddates

Set the earliest possible end date
And include the start date for when the enddate
exceeds the original start date

=cut

sub _set_earliest_product_enddates {
    my ($self, $data, $orig) = @_;

    my %dates = %{ $self->product_enddates };
    foreach (keys %dates) {

        next if !exists $data->{ $dates{$_} };
        my $start = $data->{$dates{$_}};

        if ($orig && $orig->{"stop_$_"}) {
            $data->{$_} = $orig->{"stop_$_"};
        }

        ($data->{$_})
            = sort { $a cmp $b }
            map { $self->dtf->parse_datetime($_) }
            grep { defined $_ } ($data->{$_}, $data->{beschikkingseinddatum});

        # Start dates
        ($data->{$dates{$_}})
            = sort { $a cmp $b }
            map { $self->dtf->parse_datetime($_) }
            grep { defined $_ } ($start, $data->{beschikkingseinddatum});
    }
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
