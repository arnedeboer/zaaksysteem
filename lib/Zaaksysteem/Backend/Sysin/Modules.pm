package Zaaksysteem::Backend::Sysin::Modules;

use Moose;
with 'MooseX::Log::Log4perl';

use JSON;
use Module::Load;

use Zaaksysteem::Tools;
use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::ZAPI::Form::Field;
use Zaaksysteem::ZAPI::Form::FieldSet;
use Zaaksysteem::ZAPI::Form::Action;
use Zaaksysteem::Types qw(Boolean);

use Storable qw(dclone);

#  1 nov 2013 - jw@mintlab.nl
#
# added these to purposefully trigger compile time errors.
# more errors is = more better.
#
# remarks:
# it's a bit weird that the parent is depending on the child,
# i think this module should be an abstract base class, and there should
# be a different object that loads the different modules.
#
# So i propose:
# - base module (you're looking at it)
#     - child module 1
#     - child module 2
# - module loader (completely separate object)
#
# for now i'll settle for explicit compile time errors.
#
use constant MODULELIST  => qw/
    API
    AuthInternal
    AuthRestriction
    BAGCSV
    Buitenbeter
    ControlPanel
    Email
    EmailConfiguratie
    EmailIntake
    ExternKoppelprofiel
    GWS4ALL
    KCC
    Key2BurgerzakenVerhuizing
    Key2Finance
    LegacyPublicatie
    MijnOverheid
    Ogone
    Omgevingsloket
    QMatic
    SAMLIDP
    SAMLSP
    STUFADR
    STUFAOA
    STUFCONFIG
    STUFNNP
    STUFNPS
    STUFPRS
    STUFVBO
    Scanstraat
    WOZ
/;

foreach (MODULELIST) {
    load "Zaaksysteem::Backend::Sysin::Modules::$_";
}

use constant    MODULE_ATTRS    => [qw/
    name
    direction
    is_multiple
    allow_multiple_configurations
    is_casetype_interface
    has_attributes
    attribute_type
    manual_type
    max_retries
    retry_interval
    label
    test_interface
    additional_description
/];

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules - A base class for Sysin Interface Modules.

=head1 SYNOPSIS

    my $module = Zaaksysteem::Backend::Sysin::Modules::CSV->new();

    $module->process();

=head1 DESCRIPTION

Modules are interfaces with the outside world in catalyst. These interfaces can
be configured in our sysin/interface component.

=head1 ATTRIBUTES

=head2 name

Type: String

Identifier of this module, use module name in lower case,
e.g. 'gba_via_stuf'. Please do not use whitespace for the identifier and make
sure it is lowercase.

=cut

has 'name'        => (
    is          => 'ro',
    required    => 1,
    isa         => 'Str',
);

=head2 label

Type: String

Friendly string describing this integration module.

=cut

has 'label'        => (
    is          => 'ro',
    isa         => 'Str',
    lazy        => 1,
    default     => sub {
        return ucfirst(shift->name);
    }
);

=head2 essential

Type: Boolean.

An interface that's marked "Essential" can't be disabled from the UI.

=cut

has essential => (
    is       => 'ro',
    required => 0,
    default  => 0,
    isa      => 'Bool',
);

=head2 direction

Type: String [One of "outgoing", "incoming"]

Identifies whether input data is generated for an outgoing or incoming request.

e.g:
incoming    - A StUF GBA kennisgeving
outgoing    - A generated CSV for download by external parties.

=cut

has 'direction'     => (
    is          => 'rw',
    default     => 'incoming',
    lazy        => 1,
);

=head2 allow_multiple_configurations

Type: Bool

Defines whether or not this module can be configured more than once. For instance,
a module named Digid SAML can only be used once.

=cut

has 'allow_multiple_configurations' => (
    is          => 'ro',
    lazy        => 1,
    isa         => 'Bool',
    default     => 0,
);

=head2 module_type [optional]

Type: ArrayRef

There are different types of modules, which can be used for automatic module
resolving. For instance: module_type(['auth']), defines this is an authentication
module.

This way, our frontend knows which authentication types we support, for example,
by authenticating to zaaksysteem.nl

=cut

has 'module_type'   => (
    is          => 'ro',
    lazy        => 1,
    isa         => 'ArrayRef',
    default     => sub { return []; }
);

=head2 is_multiple

Type: Boolean

Defines whether this interface receives bulk transactions, or single
transactions

=cut

has 'is_multiple'  => (
    is          => 'ro',
    required    => 1,
    isa         => 'Bool',
);


=head2 is_manual

Type: Boolean

Defines whether this interface is a manual interface, this could be an upload
interface.

When manual is set, we do not supply active or test infromation.

=cut

has 'is_manual'  => (
    is          => 'ro',
    default     => '0',
    isa         => 'Bool',
);

=head2 manual_type

Type: String (one of 'file',)

When interface is a manual interface, defines the way of inserting information

=cut

has 'manual_type'  => (
    is          => 'ro',
    default     => sub { ['file'] },
    isa         => 'Maybe[ArrayRef]',
);

=head2 retry_on_error

Type: BOOL
Default: 0

Retries a transaction on error, when transaction failed.

=cut

has 'retry_on_error'    => (
    is          => 'ro',
    default     => 0,
    isa         => Boolean,
);

=head2 max_retries

Type: Integer

Maximum number of retries, before giving up trying to connect to the other side

=cut

has 'max_retries' => (
    is          => 'ro',
    isa         => 'Num',
    lazy        => 1,
    default     => 10,
);

=head2 retry_interval

Type: Array

Array of retry intervals in seconds between request. Example:

 [
    300,
    300,
    900,
    3600,
    7200,
    84600
 ]

This example will retry after 300 seconds the first and second time, than after
900 seconds, 3600 seconds, 7200 seconds, and finally after one day. If
C<max_retries> is higher than the amount of retry intervals, it will use the
last use retry interval. In the case of this example, it will try for another
4 days before giving up.

Default:

        300,        # 5 minutes
        300,
        1800,       # 30 minutes
        14400,      # 4 hours
        14400,
        14400,
        86400,      # 1 day
        86400,
        604800,     # 1 week

Which totals to approx. 2 weeks and 2.5 days (when using 10 retries):

=cut

has 'retry_interval' => (
    is          => 'ro',
    isa         => 'ArrayRef',
    lazy        => 1,
    required    => 1,
    default     => sub { return [
            300,        # 5 minutes
            300,
            1800,       # 30 minutes
            14400,      # 4 hours
            14400,
            14400,
            86400,      # 1 day
            86400,
            604800,     # 1 week
        ];
    }
);

=head2 has_attributes

Type: Boolean

Defines whether this interface has attributes which need to be mapped. This
can be a predefined set of attributes, or could be a dynamic set of attributes
within a given object.

=cut

has 'attribute_type'  => (
    is          => 'ro',
    default     => 'predefined',   # OR freeform
    isa         => 'Str',
);

=head2 has_attributes

Type: Boolean

Defines whether this interface has attributes which need to be mapped. This
can be a predefined set of attributes, or could be a dynamic set of attributes
within a given object.

=cut

has 'has_attributes'  => (
    is          => 'ro',
    default     => '0',
    isa         => 'Bool',
);

=head2 attribute_list

Type: HashRef

An attribute list defines a possibility to match interface attributes with
zaaksysteem.nl attributes. There are a couple of ways to do this, which are
explained below.

Every mode has a few generic options

=head3 Options

=over 4

=item optional

Defines whether the given attribute is optional. Some interface attributes are
not necessary for proper function of the interface, other are.

All attributes are default required, unless the C<optional> attribute is
defined.

=back

=head3 Attribute Modes

=over 4

=item Match against casetype

    [
        {
            external_name   => 'melding_number'
            from_casetype   => 1,
            optional        => 1,
        },
        {
            external_name   => 'melding_categorie',
            from_casetype   => 1,
        }
    ]

In this mode, you match a given attribute against an attribute in the given
case_type.

=item Match against an object attribute

 ### Freeform, with given attribute list
 [
    {
        external_name   => 'bsn-number',
        freeform        => 1,
        object          => 'natuurlijk_persoon' ### Spotenlighter object
    },
    {
        external_name   => 'kvk-nummber',
        freeform        => 1,
        object          => 'bedrijf'            ### Spotenlighter object
    },
 ]

This is especially useful when using the future object management module. By
defining an object, an administrator can use the spotenlighter to find the
belonging attribute

=item Predefined list

 ### Solid list
 [
    {
        external_name   => 'bsn-number',
        internal_name   => 'burgerservicenummer',
        internal_object => 'natuurlijk_persoon'
    },
    {
        external_name   => 'a-nummer',
        internal_name   => 'a_nummer',
        internal_object => 'natuurlijk_persoon'
    },
    {
        external_name   => 'geboortePlaats',
        internal_name   => 'geboorteplaats',
        internal_object => 'natuurlijk_persoon',
        optional        => 1
    },
    {
        external_name   => 'bsn-number',
        internal_name   => 'burgerservicenummer',
        internal_object => 'natuurlijk_persoon'
    },
 ]

In this case, all attributes are solid. You can think of an import module for
organizational contacts. This is a predefined object in zaaksysteem, and there
is not much room left for customization.

We still show this list for more information, and the possibility to turn off
optional fields.

=back

=cut

has 'attribute_list'  => (
    is          => 'ro',
    isa         => 'ArrayRef',
);

=head2 interface_config

Type: ArrayRef of L<Zaaksysteem::ZAPI::Form::Field>

Interface configuration. An object information about the required fields
for this interface. See L<Zaaksysteem::ZAPI::Form::Field> for more details.

=cut

has 'interface_config'  => (
    is          => 'ro',
    required    => 1,
    isa         => 'ArrayRef'
);

=head2 is_casetype_interface

Type: Boolean

Marks this interface as an interface related to a single zaaktype. This makes
the zaaktype configurable in the interface configuration.

=cut

has 'is_casetype_interface'  => (
    is          => 'ro',
    required    => 1,
    isa         => 'Bool',
);

=head2 parser_options

Type: HashRef

Options for the specific parser, a way to define the seperator for the CSV
parser etc.

=cut

has 'parser_options'  => (
    is          => 'ro',
    required    => 0,
    isa         => 'HashRef',
);

=head2 trigger_definition

Type: HashRef

Hashref containing available methods on this module, which can be triggered.

=cut

has 'trigger_definition'  => (
    is          => 'ro',
    required    => 0,
    isa         => 'HashRef',
    lazy        => 1,
    default     => sub { return {}; }
);

=head2 case_hooks

Type: ArrayRef

=cut

has 'case_hooks'            => (
    'is'        => 'rw',
    'isa'       => 'Maybe[ArrayRef]',
    'default'   => sub { return []; },
    'lazy'      => 1,
);

=head2 test_interface

Type: Bool

=cut

has 'test_interface'            => (
    'is'        => 'rw',
    'isa'       => 'Maybe[Bool]',
);

=head2 test_definition

Type: HashRef

=cut

has 'test_definition'            => (
    'is'        => 'rw',
    'isa'       => 'Maybe[HashRef]',
    'default'   => sub { return []; },
    'lazy'      => 1,
);

has 'additional_description'    => (
    'is'        => 'rw',
);

=head1 METHODS

=head2 get_attribute_mapping([ $interface_row ])

 my $mapping = $self->get_attribute_mapping;

=cut

sub get_attribute_mapping {
    my $self        = shift;
    my $interface   = shift;
    my $rv          = {
        attributes          => [],
    };
    my ($attributes);

    if ($interface && exists($interface->get_interface_config->{attribute_mapping})) {
        $attributes     = $interface->get_interface_config->{attribute_mapping};
    }

    $rv->{attributes}               = (
        $self->_prepare_attribute_list($attributes, $interface)
    );

    return $rv;
}

sub _prepare_attribute_list {
    my $self                = shift;
    my $interface_config    = shift;
    my $interface           = shift;

    unless ($interface_config) {
        $interface_config ||= dclone($self->attribute_list || []);
    }


    # when we need access the database to define the list of attributes,
    # a different route needs to be taken
    my $attribute_list = $self->can('dynamic_attribute_list') ?
        $self->dynamic_attribute_list($interface) :
        $self->attribute_list;


    ### Merge missed
    my @new_config          = ();

    my @missed_attributes;
    {
        my %configured_attributes   = map {
            $_->{external_name} => 1
        } @{ $interface_config };

        @missed_attributes          = grep {
            !$configured_attributes{ $_->{external_name} }
        } @{ $attribute_list };
    }


    for my $attribute (@{ $interface_config }, @missed_attributes) {
        my ($module_attr)           = grep {
            $_->{external_name} eq $attribute->{external_name}
        } @{ $attribute_list };

        ### First, drop, when type is magic_string or defined, when it
        ### is not found as $module_attr
        push(@new_config, $attribute) if $module_attr;

        ### Predefined
        if ($module_attr->{attribute_type} || ''  eq 'magic_string') {

            $attribute->{case_type_id} = $interface->case_type_id ?
                $interface->case_type_id->id :
                undef;

            $attribute->{include_system} = 1 if $self->direction eq 'outgoing';
        }

        $attribute->{optional}      = 0 unless $module_attr->{optional};

        ### Append missed keys:
        for my $key (
            grep { !exists($attribute->{ $_ }) }
            keys %{ $module_attr }
        ) {
            $attribute->{ $key } = $module_attr->{ $key };
        }


        ### Defined? Do not change anything
        if ($module_attr->{attribute_type} || '' eq 'defined') {
            $attribute->{$_}        = $module_attr->{$_} for keys %{
                $module_attr
            };

            next;
        }

        ### Modifyable (checked, internal_name, etc)
        $attribute->{checked}       = (
            $attribute->{optional} ? 0 : 1
        );

        # XXX TODO throw if one of the required attributes does not exist
    }

    return \@new_config;
}

Params::Profile->register_profile(
    method  => 'set_attribute_mapping',
    profile => {
        required => [qw/
            attributes
        /],
    }
);

sub set_attribute_mapping {
    my $self        = shift;
    my $interface   = shift;
    my $params      = shift;

    my $dv          = assert_profile($params)->valid;

    my $interface_config                    = $interface->get_interface_config;

    $interface_config->{attribute_mapping}  =
        $self->_prepare_attribute_list($params->{'attributes'}, $interface);

    $interface->update_interface_config($interface_config);

    return $self->get_attribute_mapping($interface->discard_changes());
}

=head2 generate_interface_form

 my $json = $m->generate_interface_form;

Generates a HashRef describing the form

=head3 Arguments

=over

=item entry

=item url

Endpoint url, should be supplied by the controller. This is where the
form will be submitted to.

=back

=cut

define_profile generate_interface_form => (
    required => {
        entry => 'Zaaksysteem::Backend::Sysin::Interface::Component'
    },
    optional => {
        update_url => 'Str',
        base_url => 'Str'
    }
);

sub generate_interface_form {
    my ($self, %params) = @_;

    my $opts = assert_profile(\%params)->valid;

    my $update_url = $opts->{ update_url };

    my $f = Zaaksysteem::ZAPI::Form->new(
        name    => $self->name,
        options => { autosave => 1 },
        actions => [
            Zaaksysteem::ZAPI::Form::Action->new(
                name          => "test",
                label         => "Test",
                type          => "popup",
                importance    => "secondary",
                when          => "getModuleByName(activeLink.module).test_interface",
                data          => {
                    template_url => "/html/sysin/links/test.html",
                    title => "Test koppeling"
                }
            ),

            Zaaksysteem::ZAPI::Form::Action->new(
                name => "submit",
                label => "Submit",
                type => "submit",
                importance => "primary",
                data => {
                    url => $update_url
                }
            )
        ]
    );

    #$self->_generate_interface_form_manual($f);

    if (scalar @{ $self->interface_config }) {

        push(
            @{ $f->fieldsets },
            Zaaksysteem::ZAPI::Form::FieldSet->new(
                name        => 'fieldset-config',
                title       => 'Configuratie',
                description => 'Hieronder vult u de specifieke'
                    . ' configuratieparameters in met betrekking tot deze koppeling.'
            )
        );

        my $index = ( scalar(@{ $f->fieldsets }) - 1);

        push(
            @{ $f->fieldsets->[$index]->fields },
            @{ $self->interface_config },
        );
    }

    $self->_generate_interface_form_casetype($f);
    $self->_generate_interface_form_attribute_mapping($f);
    $self->_generate_interface_form_general($f);
    $self->_generate_interface_form_extra($f);

    return $self->_load_values_into_form_object($f, $opts);
}

=head2 $module->process_trigger(\%OPTIONS)

Return value: $ZAPI_READABLE_RESPONSE

    $module->process_trigger(
        {
            interface   => $interface_obj,
            action      => 'get_naw_csv',
            params      => {
                opt1        => 'val1',
                opt2        => 'val2'
            }
        }
    )

B<Options>

=over 4

=item interface

isa: Zaaksysteem::Backend::Sysin::Interface::Component

Interface object L<Zaaksysteem::Backend::Sysin::Interface::Component>, from
where this module has been triggered

=item action

isa: String

The action to trigger for this interface

=item params

isa: HashRef

A hashref containing parameters for this action

=back

=cut

Params::Profile->register_profile(
    method  => 'process_trigger',
    profile => {
        missing_optional_valid  => 1,
        required => [qw/
            interface
            action
        /],
        optional => [qw/
            params
        /],
    }
);

sub process_trigger {
    my $self        = shift;
    my $options     = assert_profile(
        {
            %{ $_[0] },
            schema  => $_[0]->{interface}->result_source->schema
        }
    )->valid;

    my $action  = $options->{action};

    ### Just call this in void contect to see if it throws an error
    my $def     = $self->get_trigger_definition($action);


    my $method  = ($def->{method} || $action);

    throw(
        'sysin/modules/process_trigger/not_found',
        'Definition is set, but no method found by name: ' . $action
    ) unless $self->can($method);

    return $self->$method($options->{params}, $options->{interface});
}

=head2 $module->get_trigger_definition($STRING_TRIGGER)

Return value: $HASHREF_DEFINITION

    $RETURN = {
        update  => 1,
    };

=cut

sub get_trigger_definition {
    my $self        = shift;
    my $action      = shift;

    unless (exists($self->trigger_definition->{ $action })) {
        throw(
            'sysin/modules/get_trigger_definition/not_found',
            'No trigger found for action: ' . $action
        );
    }

    return $self->trigger_definition->{ $action };
}

=head2 $module->run_test($interface, TEST_ID)

Return value: $TRUE_OR_EXCEPTION

Returns a true value on success, an exception on error.

B<Options>

=over 4

=item $interface

L<Zaaksysteem::Backend::Sysin::Interface::Component> row pointing to the interface
this module belongs to

=item TEST_ID

The id of the test in C<test_definition>

=back

=cut


sub run_test {
    my $self                        = shift;
    my ($interface, $test_id)       = @_;

    my ($test)                      = grep(
        { $_->{id} eq $test_id }
        @{ $self->test_definition->{tests} }
    );

    throw(
        'sysin/modules/run_test/id_not_found',
        'Cannot find test belonging to given id, id correct?'
    ) unless $test;

    my $method                      = $self->can($test->{method});

    throw(
        'sysin/modules/run_test/method_not_found',
        'Cannot find test method belonging to given id, module correct?'
    ) unless $method;

    return $method->($self, $interface, $test_id);
}


=head2 $module->process

Return value: $ROW_TRANSACTION

    ### Process by given filestore[uuid]
    my $self        = $interface;

    my $transaction = $module->process(
        {
            input_filestore_uuid    => 'c13685f0-e969-11e2-91e2-0800200c9a66',
            interface               => $self,
        }
    );

    ### Process by given input_string
    my $transaction = $interface->process(
        {
            input_string            => '<xml><name1>Wim</name1><address>Street 33</address>'
            interface               => $self,
        }
    );

Interprets the given input data, and dispatches this to the appropriate module which
is configured for this interface.

It returns a L<Zaaksysteem::Backend::Sysin::Transaction::Component> row, which contains
information about the result and the feedback for your interface.

This function receives a C<HashRef> with options:

=over 4

=item input_filestore_uuid [optional]

ISA: String UUID


=item input_string [optional]

ISA: String

=item label [optional]

ISA: String

Optional descriptive label for this transaction

=item direct [optional]

ISA: Bool
Default: false

Defines whether the resulting transaction get processed immediatly, or will be
queued for delivery.

=back

=cut


Params::Profile->register_profile(
    method  => 'process',
    profile => {
        missing_optional_valid  => 1,
        required => [qw/
            interface
        /],
        optional => [qw/
            label
            external_transaction_id

            processor_params
            direct
            schedule
            direction
        /],
        require_some    => {
            'filestore_or_string'   => [
                1,
                'input_data',
                'input_filestore_uuid',
            ],
        },
        constraint_methods => {
            direct              => qr/^\d?$/,
            input_filestore_uuid => sub {
                my $dfv     = shift;
                my $val     = shift;

                my $schema  = $dfv->get_input_data->{'schema'};

                return 1 if $schema
                    ->resultset('Filestore')
                    ->search(
                        {
                            uuid    => $val,
                        }
                    )
                    ->count;

                return;
            }
        },
        defaults    => {
            direct                  => 1,
            external_transaction_id => sub {
                my $dfv     = shift;

                my $interface = $dfv->get_input_data->{'interface'};

                return $interface->module . '-' . DateTime->now()->iso8601;
            }
        },
    }
);

has 'process_stash' => (
    is      => 'rw',
    isa     => 'HashRef',
    lazy    => 1,
    default => sub { return {}; }
);

sub process {
    my $self        = shift;
    my $options     = assert_profile(
        {
            %{ $_[0] },
            schema  => $_[0]->{interface}->result_source->schema
        }
    )->valid;

    my $interface   = $options->{interface};

    throw(
        'sysin/modules/process/inactive_module',
        'De koppeling "' . $interface->module . '" is inactief. Neem contact op met de beheerder.'
    ) unless $interface->active;

    ### Input file or input data?
    if ($options->{input_filestore_uuid}) {
        my $filestore   = $interface
                        ->result_source
                        ->schema
                        ->resultset('Filestore')
                        ->search(
                            {
                                uuid    => $options->{input_filestore_uuid},
                            }
                        )->first;

        throw(
            'sysin/modules/invalid_filestore_entry',
            'Invalid input_filestore_uuid'
        ) unless $filestore;

        $options->{input_file} = $filestore;
    }

    ### Create an empty transaction, for feedback.
    my $transaction = $self->_process_create_transaction($options);

    ### Validate input format, module needs to implement an AROUND modifier
    ### to throw an error in case of invalid data structure
    unless ($self->{processor_sub}) {
        ### Start transaction
        eval {
            $transaction->result_source->schema->txn_do(sub {
                $self->_process_validate_input($transaction);
            });
        };

        if ($@) {
            $transaction->error_count(1);
            $transaction->success_count(0);
            $transaction->error_fatal(1);
            $transaction->error_message($self->_handle_transaction_error($@, $transaction));

            ### STOP transaction
            $transaction->update;
            return $transaction;
        }
    }

    ### Need to run this transaction directly, or is it pending?
    if (!defined($options->{direct}) || $options->{direct}) {
        $self->process_transaction($transaction);
    } else {
        $self->_set_retry_delay(
            $transaction,
            ($options->{schedule} || DateTime->now())
        );
    }

    return $transaction;
}


sub _set_retry_delay {
    my $self            = shift;
    my $transaction     = shift;
    my $given_time        = shift;

    ### Do not retry on a succesfull transaction
    return if $transaction->processed && !$transaction->error_count;

    ### Module is configured for retry on error
    return unless $self->retry_on_error;

    ### Fatal errors will not be retried
    return if $transaction->error_fatal;

    ### Set retry date
    my $new_time        = DateTime->now();
    my $retry_count;
    if ($transaction->automated_retry_count) {
        $retry_count    = $transaction->automated_retry_count;
    } else {
        $retry_count    = 0;
    }

    if ($given_time) {
        $transaction->date_next_retry($given_time);
    } else {
        my $next_interval   = $self->retry_interval->[$retry_count++];

        if ($next_interval) {
            $new_time->add(
                seconds => $next_interval
            );

            $transaction->date_next_retry($new_time);
            $transaction->automated_retry_count($retry_count);
        }
    }

    $transaction->update;
    return $transaction->date_next_retry;
}

sub process_transaction {
    my $self            = shift;
    my $transaction     = shift;

    ### Clear process stash
    $self->process_stash({});

    ### Disable next_retry for this moment
    $transaction->date_next_retry(undef);
    $transaction->update;

    ### Start transaction
    eval {
        $transaction->result_source->schema->txn_do(sub {
            $self->_process_input($transaction);
        });
    };

    if ($@) {
        ### XXX TODO: An error on transaction level
        ### $transaction->error_message(
        ###    'Could not process entire interface->process request: ' . $@
        ### );
        $transaction->error_count(1);
        $transaction->success_count(0);
        $transaction->error_fatal(1);
        $transaction->error_message($self->_handle_transaction_error($@, $transaction));
    } else {
        ### Mark transaction as processed
        $transaction->processed(1);
    }

    ### Clear process stash
    $self->process_stash({});

    ### Check for fatal failure, and set retry counts
    $self->_set_retry_delay($transaction);

    $transaction->date_last_retry(DateTime->now());
    $transaction->update;

    return $transaction;

}

=head2 _process_input

For each row of input, a separate transaction will be started, and the selected
module's processor function will be called.

The default processor function is '_process_row'.
The purpose of _process_selector is to make a selection
between different operations on a higher level.

=cut

sub _process_input {
    my $self            = shift;
    my $transaction     = shift;

    $self->process_stash(
        {
            transaction => $transaction,
        }
    );

    my ($error, $success) = (0, 0);
    my @preview_data;
    while (my ($row, $input) = $self->_process_get_next_row()) {
        $self->process_stash->{row} = {};

        my $record  = $transaction
                    ->transaction_records
                    ->transaction_record_create(
            {
                transaction_id  => $transaction->id,
                input           => $input,
                output          => 'bla',
            }
        );

        ### Normal processor, or custom processor
        my $processor;
        if ($self->can('_process_selector')) {
            $processor          = $self->_process_selector($row, $record);
        } else {
            $processor          = $self->can('_process_row');
        }

        throw('sysin/modules/no_processor', 'Systeemfout: Geen processor functie aanwezig voor invoer')
            unless $processor;

        my $output;
        eval {
            $transaction->result_source->schema->txn_do(sub {
                $output = $processor->($self, $record, $row);
            });
        };

        if ($@) {
            $record->is_error(1);
            $record->output($self->_handle_transaction_record_error($@, $transaction));

            $error++;
        } elsif ($record->is_error) {
            $error++;
        } else {
            $record->output($output) unless $record->output;

            eval {
                $self->_process_mutations($record);
            };

            if ($@) {
                warn('Mutation register failed: ' . $@);
            }

            $success++;
        }

        if ($record->preview_string) {
            my %record_columns = $record->get_columns;

            push(
                @preview_data,
                {
                    map { $_ => $record_columns{$_} }
                    qw/
                        transaction_id
                        id
                        preview_string
                    /
                }
            );

        }

        $record->date_executed(DateTime->now());
        $record->update;
    }

    $transaction->preview_data(\@preview_data) if scalar(@preview_data);

    $self->_handle_preview_and_searchfilter($transaction, \@preview_data);

    $transaction->error_count(($error ? $error : undef));
    $transaction->success_count($success);

    $transaction->update;

    #die('Errors occured') if $error;

    return 1;
}

=head2 _handle_preview_and_searchfilter

Arguments: $TRANSACTION_ROW, \@PREVIEW_DATA

Return value: $TRUE

    $self->_handle_preview_and_searchfilter($transaction, [{id=> 44, transaction_id => 443, preview_string => "Tinus testpersoon"}]);

Updates transaction column text_vector to include external_transaction_id and
every preview_string from PREVIEW_DATA. Also sets preview_data to \@PREVIEW_DATA.

=cut

sub _handle_preview_and_searchfilter {
    my $self                            = shift;
    my ($transaction, $preview_data)    = @_;

    my @searches;
    if ($preview_data && scalar(@{ $preview_data })) {
        $transaction->preview_data($preview_data);

        for my $data (@{ $preview_data }) {
            push(@searches, lc($data->{preview_string})) if ($data->{preview_string});
        }
    }

    push(@searches, lc($transaction->external_transaction_id)) if $transaction->external_transaction_id;

    $transaction->text_vector(\@searches);

    return 1;
}

=head2 _handle_transaction_error

Arguments: $ERROR

Return value: $STRING_ERROR_MESSAGE

    $self->_handle_transaction_error($@);

Retrieves an error from $@, and returns a usable error_string

=cut

sub _handle_transaction_error {
    my $self        = shift;
    my $error       = shift;

    my $message     = '';
    if (
        UNIVERSAL::isa($error, 'Zaaksysteem::Exception::Base')
    ) {
        if (UNIVERSAL::isa($error->object, 'HASH') && $error->object->{transaction_output}) {
            $message = $error->object->{transaction_output};
        } else {
            $message = 'Error: ' . $error->type . ': ' . $error->message;
        }
    } else {
        $message = 'Error: ' . $error;
    }

    return $message;
}

=head2 _handle_transaction_error

Arguments: $ERROR, $TRANSACTION

Return value: $STRING_ERROR_MESSAGE

    $self->_handle_transaction_record_error($@, $transaction);

Retrieves an error from $@, and returns a usable error_string. Also sets error_fatal to true
on transaction when the error is fatal.

Fatal can also be set by setting the process_stash key C<error_fatal> to 1:

    $self->process_stash->{error_fatal} = 1;

=cut

sub _handle_transaction_record_error {
    my $self        = shift;
    my $error       = shift;
    my $transaction = shift;

    my $message     = '';
    if (
        UNIVERSAL::isa($error, 'Zaaksysteem::Exception::Base')
    ) {
        if (UNIVERSAL::isa($error->object, 'HASH') && $error->object->{transaction_output}) {
            $message = $error->object->{transaction_output};
        } else {
            $message = 'Error: ' . $error->type . ': ' . $error->message;
        }

        if (UNIVERSAL::isa($error->object, 'HASH') && $error->object->{fatal}) {
            $transaction->error_fatal(1);
        }
    } else {
        $message = 'Error: ' . $error;
    }

    if ($self->process_stash->{error_fatal}) {
        $transaction->error_fatal(1);
    }

    return $message;
}

sub _process_mutations {
    my $self        = shift;
    my $record      = shift;

    return unless (
        exists($self->process_stash->{row}) &&
        exists($self->process_stash->{row}->{mutations}) &&
        $self->process_stash->{row}->{mutations} &&
        UNIVERSAL::isa($self->process_stash->{row}->{mutations}, 'ARRAY')
    );

    for my $mutation (
        @{ $self->process_stash->{row}->{mutations} }
    ) {
        $mutation->register($record);
    }
}

sub _process_validate_input {
    my $self        = shift;
}

sub _process_create_transaction {
    my ($self, $options) = @_;
    my $interface   = $options->{interface};

    my $create_params = {
        processor_params => $options->{processor_params},
        interface_id     => $interface->id,
        direction        => $options->{direction} || $self->direction,
        $options->{input_file}
            ? (input_file => $options->{input_file}->id)
            : (input_data => $options->{input_data}),
        external_transaction_id => $options->{external_transaction_id},
    };

    my $transaction = $interface->transactions->transaction_create($create_params);

    return $transaction;
}

=head1 CLASS METHODS

=head2 list_of_modules

Return value: @list_of_module_objects

 my @list = Zaaksysteem::Backend::Sysin::Modules->list_of_modules;

 print $list[0]->name;
 print $list[0]->max_retries;

Returns a list of all module objects currently available in this release of
zaaksysteem.nl

=cut


sub list_of_modules {
    my $self    = shift;

    my @modules;
    for my $module (MODULELIST) {
        my $package = __PACKAGE__ . '::' . $module;

        eval 'use ' . $package;

        if($@) {
            throw('sysin/modules', "Unable to instantiate module $module: $@");
        }

        push @modules, $package->new;
    }

    return @modules;
}

=head2 list_of_available_modules

Return value: @list_of_available_modules

 my @list = Zaaksysteem::Backend::Sysin::Modules->list_of_available_modules(
    $schema
 )

 print $list[0]->name;
 print $list[0]->max_retries;

Returns a list of all available module objects to be configured. Some modules
may only be configured once, like a DigiD module. This call will only return
the modules not already used in the system. See
L<Zaaksysteem::Backend::Sysin::Modules#list_of_available_modules> for more
details

=cut

sub list_of_available_modules {
    my $self                = shift;
    my $schema              = shift;

    my @modules             = $self->list_of_modules;
    my $interfaces          = $schema
                            ->resultset('Interface')
                            ->search_active();

    my @used_modules = map { $_->module } $interfaces->all;

    my $filter = sub {
        my $module = shift;

        return 1 if $module->allow_multiple_configurations;
        return 1 unless grep { $module->name eq $_ } @used_modules;
    };

    return grep { $filter->($_) } @modules;
}

=head2 list_modules_by_module_type

=cut

sub list_modules_by_module_type {
    my $self                = shift;
    my $type                = shift;

    my @modules;
    for my $module ($self->list_of_modules) {
        next unless grep { $_ eq $type } @{ $module->module_type };

        push(@modules, $module);
    }

    return @modules;
}



=head2 find_module_by_id

Return value: $module

 my $module = Zaaksysteem::Backend::Sysin::Modules->find_module_by_id('csv');

 $module->name;
 $module->max_retries;
 $module->is_numeric;
 [...]

Returns the module object by giving the internal name of the module. See
C<list_of_modules> for a list of modules.

=cut

sub find_module_by_id {
    my $self        = shift;
    my $id          = shift;

    my @modules     = $self->list_of_modules;

    my ($module)    = grep { $_->name eq $id } @modules;

    return $module;
}

sub TO_JSON {
    my $self    = shift;

    return {
        map { $_ =>  $self->$_ } @{ MODULE_ATTRS() }
    };
}


=head1 INTERNAL METHODS

=head2 _load_values_into_form_object

Loads values from a hash into a form object

=cut

sub _load_values_into_form_object {
    my ($self, $form, $opts) = @_;

    my $params = { $opts->{ entry }->get_columns };

    ### Load case_type_id
    if ($params->{case_type_id}) {
        $params->{case_type_id} = $opts->{ entry }->result_source->schema->resultset('Zaaktype')->find(
            $params->{case_type_id}
        );
    }

    my $interface_config
        = JSON->new->utf8(0)->decode($params->{interface_config});

    for my $key (keys %{$interface_config}) {
        $params->{"interface_$key"} = $interface_config->{$key};
    }

    ### Delete interface_config from value listing for form
    delete($params->{interface_config});

    $form->load_values($params);

    return $form;
}

=head2 _generate_interface_form_casetype

Generates the form fields when interface module is a zaaktype interface

=cut

sub _generate_interface_form_casetype {
    my $self    = shift;
    my $f       = shift;

    return unless $self->is_casetype_interface;

    push(
        @{ $f->fieldsets },
        Zaaksysteem::ZAPI::Form::FieldSet->new(
            name        => 'fieldset-mapping',
            title       => 'Zaaktype interface',
            description => 'U bewerkt een koppeling welke betrekking heeft op een'
                . ' zaaktype. Configureer hieronder uw zaaktype.'
        )
    );
    my $index = (scalar(@{ $f->fieldsets }) - 1);

    push(
        @{ $f->fieldsets->[ $index ]->fields },
        Zaaksysteem::ZAPI::Form::Field->new(
            name    => 'case_type_id',
            label   => 'Zaaktype',
            type    => 'spot-enlighter',
            data    => {
                'restrict'      => 'casetype',
                'placeholder'   => 'Type uw zoekterm',
                'label'         => 'zaaktype_node_id.titel',
                'resolve'       => 'id'
            },
            description => 'Zoek het zaaktype op behorende bij deze koppeling',
        ),
    )
}

sub _generate_interface_form_extra {
    my $self    = shift;
    my $f       = shift;

    return unless $self->additional_description;

    push(
        @{ $f->fieldsets },
        Zaaksysteem::ZAPI::Form::FieldSet->new(
            name        => 'fieldset-mapping',
            title       => 'Extra informatie',
            description => $self->additional_description
        )
    );
    my $index = (scalar(@{ $f->fieldsets }) - 1);

}

=head2 _generate_interface_form_attribute_mapping

Generates the form fields when interface contains attribute mappings.

=cut

sub _generate_interface_form_attribute_mapping {
    my $self    = shift;
    my $f       = shift;

    return unless $self->has_attributes;

    my $title = $self->is_casetype_interface ? "Kenmerken koppelen" : "Zaaktypen koppelen";

    push(
        @{ $f->fieldsets },
        Zaaksysteem::ZAPI::Form::FieldSet->new(
            name        => 'fieldset-mapping',
            title       => $title,
            description => 'Uw koppeling maakt gebruik van attributen, welke weer'
                . ' gekoppeld kunnen worden aan velden binnen het zaaksysteem.'
                . ' Druk op de knop "' . $title . '" om deze velden aan elkaar te koppelen'
        )
    );

    my $index = (scalar(@{ $f->fieldsets }) - 1);

    push(
        @{ $f->fieldsets->[ $index ]->actions },
        {
            "name"          => "attribute_mapping",
            "label"         => $title,
            "type"          => "popup",
            "importance"    => "secondary",
            "disabled"      => "!(isFormValid()&&!(getModuleByName(activeLink.module).is_casetype_interface&&!case_type_id))",
            "data"          => {
                "template_url"  => "/html/sysin/links/mapping.html",
                "title"         => $title
            }
        }
    );
}

=head2 _generate_interface_form_general

Generates form fields for general attributes, such as active/inactive

=cut

sub _generate_interface_form_general {
    my $self    = shift;
    my $f       = shift;

    push(
        @{ $f->fieldsets },
        $self->essential
            ? Zaaksysteem::ZAPI::Form::FieldSet->new(
                name        => 'fieldset-general',
                title       => 'Algemeen',
                description => 'Deze koppeling is essentieel voor de werking van Zaaksysteem, en kan '
                    . ' daarom niet uitgeschakeld worden.'
            )
            : Zaaksysteem::ZAPI::Form::FieldSet->new(
                name        => 'fieldset-general',
                title       => 'Algemeen',
                description => 'Wanneer u tevreden bent over uw configuratie, '
                    . ' controleer uw configuratie door op de Test knop te drukken.'
                    . ' Wanneer alles naar wens is, kunt u de koppeling actief zetten en opslaan.'
            )
    );
    my $index = ( scalar(@{ $f->fieldsets }) - 1);

    push(
        @{ $f->fieldsets->[$index]->fields },
        Zaaksysteem::ZAPI::Form::Field->new(
            name    => 'active',
            label   => 'Actief',
            type    => 'radio',
            data    => {
                options => [
                    {
                        value   => '1',
                        label   => 'Actief',
                    },
                    (!$self->essential)
                        ? {
                              value   => '0',
                              label   => 'Inactief',
                          }
                        : ()
                ],
            },
            description => 'Maak de koppeling actief',
        ),
    );
}

=head2 _get_config_fields

Return value: @list_of_config_fields

=cut

sub _get_config_fields {
    my $self    = shift;

    my @fields;
    for my $field (@{ $self->interface_config }) {
        push(@fields, $field->name);
    }

    return @fields;
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::ZAPI::Form> L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 MODULE_ATTRS

TODO: Fix the POD

=cut

=head2 TO_JSON

TODO: Fix the POD

=cut

=head2 process_transaction

TODO: Fix the POD

=cut

=head2 set_attribute_mapping

TODO: Fix the POD

=cut

