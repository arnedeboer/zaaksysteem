package Zaaksysteem::Backend::Sysin::Interface::ResultSet;
use Moose;

use Zaaksysteem::Tools;
use Zaaksysteem::Types qw(NonEmptyStr);

use Zaaksysteem::Backend::Sysin::Modules;

extends 'Zaaksysteem::Backend::ResultSet';
with 'Zaaksysteem::Backend::Sysin::Interface::Roles::InterfaceConfig';

has '_active_params' => (
    is          => 'ro',
    lazy        => 1,
    default     => sub {
        return {
            active           => 1,
            date_deleted     => undef,
        }
    }
);

=head2 interface_create

Add an interface definition to the database.

=head3 Arguments

=over

=item name [required]

A name chosen by the zaaksysteem administrator to describe this interface.

=item module [required]

The perl library that this interface is set up for. This has to be -exactly- the same.

=item interface_config [optional]

The JSON hash containing all relevant configuration details.

=item case_type_id [optional]

An optional case_type_id pointing to the zaaktype->id.

=item max_retries [optional, default 10]

Defines the number of automated retries on the transactions belonging to this
interface before the process gives up. Defaults are set in the module.

=item multiple [optional, default 0]

Defines whether or not this interface will always receive a single mutation or multiple mutations.

=item active [optional, default 0]

Defines whether or not the Interface should be considered as 'in production'.

=back

=head3 Returns

A newly created Sysin::Interface object.

=cut

define_profile interface_create => (

    missing_optional_valid => 1,
    required               => [
        qw/
            name
            module
            schema
            /
    ],
    optional => [
        qw/
            case_type_id
            max_retries
            multiple
            interface_config
            active
            /
    ],
    constraint_methods => {
        name        => qr/^\w[\w\s-]+$/,
        max_retries => qr/^\d+/,
        multiple    => qr/(0|1)/,
        active      => qr/(0|1)/,
        module      => sub {
            my ($dfv, $val) = @_;

            my $schema = $dfv->get_input_data->{schema};

            return 1
                if (grep { $_->name eq $val }
                Zaaksysteem::Backend::Sysin::Modules->list_of_modules($schema));

            return;
        },
        case_type_id => sub {
            my ($dfv, $val) = @_;
            my ($entry);

            return unless ref($val) || $val =~ /^\d+/;

            my $schema = $dfv->get_input_data->{schema};

            my $zt_id = (ref($val) ? $val->id : $val);

            return 1
                if (($entry = $schema->resultset('Zaaktype')->find($zt_id))
                && $entry->active
                && !$entry->deleted);

            return;
            }
    },
    typed    => { name => NonEmptyStr, },
    defaults => {
        active           => 0,
        interface_config => '{}',
    }
);

sub interface_create {
    my $self    = shift;
    my $opts    = assert_profile(
        {
            %{ shift() },
            schema  => $self->result_source->schema
        }
    )->valid;

    $self->_prepare_options($opts);
    delete $opts->{schema};

    my $retval = $self->create($opts);

    $retval->trigger_log('create');

    return $retval;
}

sub find_by_module_name {
    my $self    = shift;
    my $module  = shift;

    my @interfaces = $self->search_active({ module => $module })->all;

    if (scalar @interfaces > 1) {
        throw('sysin/interfaces/find_by_module_name/multiple_found', sprintf(
            'Cannot get interface for module "%s": found multiple objects',
            $module
        ));
    }

    return $interfaces[0];
}

=head2 search_module

Convenience method for retrieving modules by their moduletype.
Provides abiltity to filter with L<JSON::Path> queries, and do simple tests
for values.

Example:

    # This will search for 'samlsp' modules, that have a value at the node
    # of it's config datastructure with a stringifyable value 55.
    $rs->search_module('samlsp', '$.sp_cert[0].id', '55');

Crude, but effective until something better is thought up.

=head3 Params

=over 4

=item module

B<Required>. String containing the module type to be searched for

=item jpath

Optional. A L<JSON::Path> expression to be executed in the context of
the interface's configuration JSON content. When used without 'test_value'
parameter, any expression that makes JSON::Path->value return true-ish
will be returned.

=item test_value

Optional. Tests all interfaces with the expression in parameter 'jpath' for
string-like equality with this value, returns interfaces which test true-ish

=back

=cut

sub search_module {
    my $self = shift;
    my $module = shift;
    my $path = shift || '*';
    my $test_value = shift;

    my $module_rs = $self->search_active({ module => $module });

    if(defined $test_value) {
        return grep {
            ($_->jpath($path) || '') eq $test_value
        } $module_rs->all;
    }

    return grep { $_->jpath($path) } $module_rs->all;
}

=head1 INTERNAL METHODS

=head2 _prepare_options

Allows Moose Roles to work on the options given to a create or update call.

=cut

sub _prepare_options { shift; return shift; }


1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 find_by_module_name

TODO: Fix the POD

=cut

