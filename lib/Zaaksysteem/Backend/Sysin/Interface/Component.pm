package Zaaksysteem::Backend::Sysin::Interface::Component;

use Data::Compare;
use Moose;

use JSON::Path qw[];

use Zaaksysteem::Exception;
use Zaaksysteem::Profile;
use Zaaksysteem::Backend::Sysin::Modules;
use Zaaksysteem::Constants qw/ZAAKSYSTEEM_CONSTANTS/;

extends 'DBIx::Class';

has 'module_object' => (
    is      => 'rw',
    lazy    => 1,
    default => sub {

        my $module  = Zaaksysteem::Backend::Sysin::Modules->find_module_by_id(
            shift->module
        );

        return $module;
    }
);

=head2 $interface->interface_update

Update the properties of an existing Sysin::Interface.

=head3 Arguments

=over

=item name

=item case_type_id

=item max_retries

Defines the number of automated retries on the transactions belonging to this interface before the process gives up.

=item interface_config

The JSON hash containing all relevant configuration details.

=item multiple

Defines whether or not this interface will always receive a single mutation or multiple mutations.

=item active

Defines whether or not the Interface should be considered as 'in production'.

=back

=head3 Returns

An updated Sysin::Interface::Component object. The object used to call interface_update should also be updated.

=cut

Params::Profile->register_profile(
    method  => 'interface_update',
    profile => {
        missing_optional_valid  => 1,
        required => [],
        optional => [qw/
            case_type_id
            interface_config
            max_retries
            multiple
            active
            name
        /],
        constraint_methods => {
            case_type_id => qr/^\d+$/,
            max_retries  => qr/^\d+$/,
            multiple     => qr/^(0|1){0,1}$/,
            active       => qr/^(0|1){0,1}$/,
            name         => qr/^\w[\w\s]+$/,
            module       => sub {
                my ($dfv, $val)     = @_;

                return 1 if (
                    grep { $_->name eq $val }
                    Zaaksysteem::Backend::Sysin::Modules->list_of_modules
                );

                return;
            },
            case_type_id  => sub {
                my ($dfv, $val)     = @_;
                my ($entry);

                return unless ref($val) || $val =~ /^\d+/;

                my $schema      = $dfv->get_input_data->{schema};

                my $zt_id       = (ref($val) ? $val->id : $val);

                return 1 if (
                    ($entry = $schema->resultset('Zaaktype')->find($zt_id)) &&
                    $entry->active
                    && !$entry->deleted
                );

                return;
            }
        },
        defaults => {
            active      => 0,
            multiple    => 0,
        }
    }
);

sub interface_update {
    my $self        = shift;
    my $opts        = assert_profile(
        {
            %{ shift() },
            schema  => $self->result_source->schema
        }
    )->valid;

    $self->_merge_interface_config($opts);

    $opts           = $self
                    ->result_source
                    ->resultset
                    ->_prepare_options(
                        {
                            %{ $opts },
                            module => $self->module,
                        }
                    );

    return $self->update($opts, @_);
}

=head2 $interface->_merge_interface_config(\%OPTIONS)

Merges the current interface config with the ones given in update, prevents
the interface config from beeing overwritten

B<OPTIONS>

=over 4

=item interface_config

The interface_config to merge with the values already in the database

=back

=cut

sub _merge_interface_config {
    my $self            = shift;
    my $opts            = shift;

    return unless (
        $opts &&
        exists($opts->{interface_config}) &&
        UNIVERSAL::isa($opts->{interface_config}, 'HASH') &&
        $self->interface_config
    );

    my %given_config    = %{ $opts->{interface_config} };

    my $db_json         = $self->interface_config;
    my %db_config       = %{
        $self
            ->result_source
            ->resultset
            ->_decode_interface_config(
                $self->interface_config
            )
    };

    $opts->{interface_config} = { (%db_config, %given_config) };
}

=head2 $interface->interface_update

Delete the interface from the system

=cut

sub interface_delete {
    my $self    = shift;

    $self->update({date_deleted => DateTime->now});
    return 1;
}

=head2 $interface->get_attribute_mapping

Returns a list of attributes, which can be configured, and an instruction how
to fill this attribute list

=cut

sub get_attribute_mapping {
    my $self    = shift;

    return $self->module_object->get_attribute_mapping($self);
}

=head2 $interface->set_attribute_mapping

Sets a list of attributes, which can be configured, and an instruction how
to fill this attribute list

=cut

sub set_attribute_mapping {
    my $self    = shift;
    my $params  = shift;

    return $self->module_object->set_attribute_mapping($self, $params);
}

=head2 $interface->get_trigger_definition($action)

Convenience method for L<Zaaksysteem::Backend::Sysin::Modules#get_trigger_definition>

=cut

sub get_trigger_definition {
    my ($self, $action)    = @_;

    return $self->module_object->get_trigger_definition($action);
}

=head2 $interface->process_trigger($action, \%params)

Convenience method for L<Zaaksysteem::Backend::Sysin::Modules#process_trigger>

=cut

sub process_trigger {
    my ($self, $action, $params)    = @_;
    $params                         ||= {};

    return $self->module_object->process_trigger({
        interface   => $self,
        params      => $params,
        action      => $action
    });
}

=head2 $interface->get_interface_form

Convenience method dispatching to attached interface module for generating
the interface form.

=cut

sub get_interface_form {
    my ($self, %params) = @_;

    return $self->module_object->generate_interface_form(%params, entry => $self);
}

=head2 $interface->process

Return value: $ROW_TRANSACTION

Convenient method to L<Zaaksysteem::Backend::Sysin::Modules#process>, see this
man page for more information.

=cut

sub process {
    my $self        = shift;
    my $options     = shift || {};

    return $self->module_object->process(
        {
            %{ $options },
            interface   => $self,
        }
    );
}

sub run_test {
    my $self        = shift;
    my $test_id     = shift;


    return $self->module_object->run_test(
        $self,
        $test_id
    );
}

=head2 $interface->TO_JSON

=cut

sub TO_JSON {
    my $self                    = shift;
    my $opts                    = shift || {};

    my $values                  = { $self->get_columns };

    ### Decode interface config to perl, so JSON can re-encode it...
    $values->{interface_config} = $self
        ->result_source
        ->resultset
        ->_decode_interface_config($values->{interface_config});

    unless ($opts->{ignore_errors}) {
        $values->{transaction_errors} = $self->transactions->get_column('error_count')->sum;
    }

    return $values;
}

=head2 $interface->get_properties

=cut

sub get_interface_config {
    my $self    = shift;

    $self->result_source
        ->resultset
        ->_decode_interface_config($self->interface_config);
}

sub update_interface_config {
    my $self    = shift;
    my $config  = shift;

    $self->interface_config(
        $self   ->result_source
                ->resultset
                ->_encode_interface_config($config)
    );

    $self->update;

}

sub jpath {
    my $self = shift;

    return JSON::Path->new(shift)->value($self->get_interface_config);
}


=head2 create_case

For requestor, a preset client is used. A hashref without attribute values
is expected. For an explanation of the attribute values, see 'map_attributes'.

=cut

define_profile create_case => (
    required => [qw/values requestor/],
    optional => [qw/record/],
    typed => {
        values => 'HashRef',
    },
    constraint_methods => {
        requestor => sub {
            my ($dfv, $value) = @_;

            # betrokkene-natuurlijk_persoon-344343
            # betrokkene-medewerker-23
            # betrokkene-bedrijf-1232
            return $value =~ m/^betrokkene-(?:natuurlijk_persoon|medewerker|bedrijf)-\d+$/;
        }
    }
);

sub create_case {
    my $self = shift;
    my $params = assert_profile(shift)->valid;

    my $aanvrager = {
        betrokkene => $params->{requestor},
        verificatie => 'medewerker'
    };

    my @attributes = $self->_map_attributes($params->{values});

    my @type_warnings = $self->_get_type_warnings(@attributes);

    if ($params->{record}) {
        $params->{record}->output(join "\n", @type_warnings);
    }

    my $kenmerken = [
        map {{
            $_->{bibliotheek_kenmerk}->id => $_->{value}
        }} @attributes
    ];

    my $opts = {
        aanvraag_trigger    => 'extern',
        contactkanaal       => 'email',
        onderwerp           => 'Omgevingsloket',
        zaaktype_id         => $self->get_column('case_type_id'),
        aanvragers          => [$aanvrager],
        kenmerken           => $kenmerken,
        registratiedatum    => DateTime->now,
    };

    return $self->result_source->schema->resultset('Zaak')->create_zaak($opts);
}

=head2 _get_type_warnings

When an interface create a new case and populates the zaaktype_kenmerken,
this verifies wether anything unhandy has been done like putting a
string 'abc' into a numeric field. At the time of this writing, this would
yield weird behaviour, the value would show in the case but as soon
as you would attempt to edit it the string value will disappear. This
function predicts these problems so they can be logged.

=cut

sub _get_type_warnings {
    my ($self, @attributes) = @_;

    my @warnings;

    foreach my $attribute (@attributes) {

        my $value = $attribute->{ value };
        my $bibliotheek_kenmerk = $attribute->{bibliotheek_kenmerk};
        my $filtered = $bibliotheek_kenmerk->filter($value);

        if ($value && !Compare($value, $filtered)) {
            my $value_display = ref $value eq 'ARRAY' ? join(", ", @$value) : $value;
            my $filtered_display = ref $filtered eq 'ARRAY' ? join(", ", @$filtered) : $filtered;
            $value_display ||= '';
            $filtered_display ||= '';

            # try to get the label from the config
            my $type = ZAAKSYSTEEM_CONSTANTS->{veld_opties}->{
                $bibliotheek_kenmerk->value_type
            }->{label} || $bibliotheek_kenmerk->value_type;

            my $kenmerk_naam = $bibliotheek_kenmerk->naam;

            push @warnings, "Mogelijk dataverlies: De waarde '$value_display' " .
                "is bij het importeren gefilterd tot '$filtered_display' " .
                "om in het gekozen kenmerk '$kenmerk_naam' met type '$type' te passen.";
        }
    }

    return @warnings;
}

=head2 map_attributes

There's two steps. First there's matching the supplied raw input data
to the configured magic_strings. The interface configuration maps
'external_name' - name is it's known in the outside world, to:
'internal_name' - in this scenario that's a magic string,
(a tag that has been configured to represent a custom field)

So input data could be:
    {
        attribute1 => 'value 1',
        attribute2 => 'value 2'
    }

Interface configuration could be:
    [{
        external_name => 'attribute_1',
        internal_name => 'magic_string_1' # in reality one level deeper,
                                          # but doesn't matter here
    },
    {
        external_name => 'attribute_2',
        internal_name => 'magic_string_2'
    }]

Which is shortened to:

$attribute_mapping = {
    magic_string1 => 'attribute1',
    magic_string2 => 'attribute2'
}

And finally the casetype configuration:
{
    magic_string_1 => 123, # bibliotheek_kenmerken_id
    magic_string_2 => 124
}

The desired end result of this transmogrification looks like:
    [{
        123 => 'value 1'
    },
    {
        124 => 'value 2'
    }]

=cut

sub _map_attributes {
    my ($self, $values) = @_;

    throw('sysin/interface/no_values', 'Geen waarden ontvangen')
        unless $values && ref $values eq 'HASH';

    throw('sysin/interface/case_type_id_undefined', 'Geen zaaktype ingesteld')
        unless $self->case_type_id;

    my $lookup = $self->case_type_id->attributes_by_magic_string;

    my %attribute_mapping = map {
        # magic_string => external_name
        $_->{internal_name}{searchable_object_id} => $_->{external_name}
    } grep {
        # double exists construct to prevent auto-vivification
        exists $_->{internal_name} &&
        exists $_->{internal_name}{searchable_object_id}
    } @{ $self->get_interface_config->{attribute_mapping} };

    return map {
        $self->_format_attribute($lookup, \%attribute_mapping, $values, $_)
    } grep {
        my $external_name = $attribute_mapping{$_};
        exists $lookup->{$_} && exists $values->{$external_name}
    } keys %attribute_mapping;
}

=head2 _format_attribute

Helper function for map_attributes. Returns a hashref containing
one key and value pair, as desired by case creation logic.

=cut

sub _format_attribute {
    my ($self, $lookup, $attribute_mapping, $values, $magic_string) = @_;

    my $external_name = $attribute_mapping->{$magic_string};
    my $bibliotheek_kenmerk = $lookup->{$magic_string};

    return {
        bibliotheek_kenmerk => $bibliotheek_kenmerk,
        value => $values->{$external_name}
    };
}

=head2 register_case_attributes_as_mutations

Adds a mutation row for every attribute that has been added to the newly created case.

=cut

sub register_case_attributes_as_mutations {
    my ($self, $case) = @_;

    my $mutation = Zaaksysteem::Backend::Sysin::Transaction::Mutation->new(
        create              => 1,
        table               => 'Zaak',
        table_id            => $case->id
    );

    my $kenmerken = $case->field_values;

    foreach my $key (keys %$kenmerken) {
        $mutation->add_mutation({
            old_value   => '',
            new_value   => $kenmerken->{$key},
            column      => 'kenmerk.' . $key,
        });
    }

    return $mutation;
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 ZAAKSYSTEEM_CONSTANTS

TODO: Fix the POD

=cut

=head2 get_interface_config

TODO: Fix the POD

=cut

=head2 interface_delete

TODO: Fix the POD

=cut

=head2 jpath

TODO: Fix the POD

=cut

=head2 run_test

TODO: Fix the POD

=cut

=head2 update_interface_config

TODO: Fix the POD

=cut

