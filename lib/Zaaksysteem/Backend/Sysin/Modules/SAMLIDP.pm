package Zaaksysteem::Backend::Sysin::Modules::SAMLIDP;

use Moose;

use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::ZAPI::Form::Field;

use Storable qw/dclone/;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Tests::SAML
    Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
/;


=head1 Interface Properties

Below a list of interface properties, see
L<Zaaksysteem::Backend::Sysin::Modules> for details.

=cut

use constant INTERFACE_ID               => 'samlidp';

use constant INTERFACE_CONFIG_FIELDS    => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_authentication_level',
        type        => 'select',
        label       => 'Betrouwbaarheidsniveau',
        default     => 'urn:oasis:names:tc:SAML:2.0:ac:classes:PasswordProtectedTransport',
        description => '<p>Selecteer het minimaal vereiste betrouwbaarheidsniveau</p><ul><li>Niveau 1: geen controle</li><li>Niveau 2: Username + Password</li><li>Niveau 2+: Username + Password + SMS code</li><li>Niveau 3: Username + Password + SMS code (op een geregistreerd nummer)</li><li>Niveau 4: SmartcardPKI</li></ul>',
        required    => 1,
        data        => {
            options     => [
                {
                    value => 'urn:oasis:names:tc:SAML:2.0:ac:classes:unspecified',
                    label => 'eH:Niveau 1'
                },
                {
                    value => 'urn:oasis:names:tc:SAML:2.0:ac:classes:PasswordProtectedTransport',
                    label => 'eH:Niveau 2 / DigiD:Niveau 10'
                },
                {
                    value => 'urn:oasis:names:tc:SAML:2.0:ac:classes:MobileTwoFactorUnregistered',
                    label => 'eH:Niveau 2+'
                },
                {
                    value => 'urn:oasis:names:tc:SAML:2.0:ac:classes:MobileTwoFactorContract',
                    label => 'eH:Niveau 3 / DigiD:Niveau 20',
                },
                {
                    value => 'urn:oasis:names:tc:SAML:2.0:ac:classes:SmartcardPKI',
                    label => 'eH:Niveau 4'
                }
            ],
        }
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_binding',
        type        => 'select',
        label       => 'SSO Binding',
        default     => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
        description => 'De <abbr title="Single-Sign-On, de generieke SAML term voor de loginprocedure">SSO</abbr> Binding bepaalt hoe de SAML Protocol Exchange plaatsvindt, en is afhankelijk van de eisen van de <abbr title="Identity Provider">IdP</abbr>',
        required    => 1,
        data        => {
            options     => [
                {
                    value   => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
                    label   => 'HTTP Redirect'
                },
                {
                    value   => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
                    label   => 'HTTP POST'
                },
                {
                    value   => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Artifact',
                    label   => 'HTTP Artifact'
                },
                {
                    value   => 'urn:oasis:names:tc:SAML:2.0:bindings:SOAP',
                    label   => 'SOAP'
                },
                {
                    value   => 'urn:oasis:names:tc:SAML:2.0:bindings:POAS',
                    label   => 'Reverse SOAP'
                }
            ]
        }
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_saml_type',
        type        => 'select',
        label       => 'SAML Implementatie',
        default     => 'digid',
        data        => {
            options     => [
                {
                    value    => 'digid',
                    label    => 'Logius'
                },
                {
                    value    => 'eherkenning',
                    label    => 'KPN Lokale Overheid',
                },
                {
                    value    => 'adfs',
                    label    => 'Microsoft AD FS',
                },
                {
                    value    => 'minimal',
                    label    => 'Custom SAML (alleen voor gebruikerslogin)',
                },
                {
                    value => 'spoof',
                    label => 'Mintlab Spoofmode'
                }
            ],
        },
        required    => 1,
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_idp_metadata',
        type        => 'text',
        label       => 'SAML Metadata URL',
        description => 'Geef de URL op waar de metadata van de <abbr title="Identity Provider">IdP</abbr> te verkrijgen is. Deze URL wordt door de IdP geleverd',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_idp_metadata_filename',
        type        => 'file',
        label       => 'SAML Metadata File',
        description => 'Wanneer een URL niet mogelijk is, upload hier de metadata van uw <abbr title="Identity Provider">IdP</abbr>',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_idp_entity_id',
        type        => 'text',
        #label       => '<abbr title="Dit ID identificeerd het Zaaksysteem uniek bij de IdP">Entity ID</abbr>',
        label       => 'Entity ID',
        required    => 0,
        description => 'Voer hier uw toegewezen EntityID in in het geval de <abbr title="Identity Provider">IdP</abbr> deze aangeleverd heeft.<br />Indien geen waarde opgevoerd is wordt de basis URL uit de <abbr title="Service Provider (Zaaksysteem)">SP</abbr> definitie gebruikt.',
        data => {
            placeholder => 'urn:nl:eherkenning:DV:00000003123456780000:entities:0001'
        }
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_idp_ca',
        type        => 'file',
        #label       => '<abbr title="Certificate Authority (de partij die de IdP gecertificeert)">CA</abbr> Certificaat van de <abbr title="Identity Provider">IdP</abbr>',
        label       => 'CA Certificaat van de IdP',
        description => 'Upload hier het certificaat van de <abbr title="Certificate Authority (de partij die de IdP heeft gecertificeerd)">CA</abbr> die het certificaat gebruikt door de <abbr title="Identity Provider">IdP</abbr> heeft ondertekend.',
        required    => 0,
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_login_type_citizen',
        label       => 'Gebruik op burgerloginpagina',
        type        => 'checkbox',
        options     => [
            { value => 'company', label => 'Burgerinlogpagina' },
        ]
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_login_type_user',
        label       => 'Gebruik op medewerkerloginpagina',
        type        => 'checkbox',
        options     => [
            { value => 'company', label => 'Medewerkerinlogpagina' },
        ]
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_login_type_company',
        label       => 'Gebruik op bedrijfloginpagina',
        type        => 'checkbox',
        options     => [
            { value => 'company', label => 'Bedrijfinlogpagina' },
        ]
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_login_type_user_direct',
        label       => 'Stuur medewerkers direct naar SAML loginpagina bij inloggen',
        description => 'Wanneer deze SAML interface wordt gebruikt voor de medewerkerlogin, dan kunt u ervoor kiezen om de loginpagina van Zaaksysteem helemaal niet meer te laten zien en altijd terug te keren naar uw active directory service',
        type        => 'checkbox',
        options     => [
            { value => 'company', label => 'Medewerkerinlogpagina' },
        ]
    ),
];

use constant MODULE_SETTINGS => {
    name => INTERFACE_ID,
    label => 'SAML 2.0 Identity Provider',
    interface_config => INTERFACE_CONFIG_FIELDS,
    direction => 'outgoing',
    manual_type => ['text'],
    is_multiple => 0,
    is_manual => 0,
    retry_on_error => 0,
    allow_multiple_configurations => 1,
    is_casetype_interface => 0,
    test_interface => 1,
    credential_module => 'saml',
    test_definition => {
        description => qq|
            Om te controleren of het Zaaksysteem correct is geconfigureerd
            om een SAML Identity Provider te gebruiker als authenticatie
            middel kunt u hieronder een aantal tests uitvoeren. Hiermee
            controleert u of de geconfigureerde Identity Provider bereikbaar
            is en de certificering klopt.
        |,
        tests => [
            {
                id => 1,
                label => 'Test configuratie',
                name => 'instantiation_test',
                method => 'test_idp'
            }
        ]
    },
    trigger_definition  => {
        register_changes   => {
            method  => 'register_changes',
        },
    },
};

around BUILDARGS => sub {
    my $orig = shift;
    my $class = shift;

    $class->$orig( %{ MODULE_SETTINGS() } );
};

has 'subject_type' => (
    is      => 'ro',
    default => 'employee',
);


=head2 register_changes

Create a transaction (+ transaction record), logging a change to a SAML
IdP-based user account.

This should be called through the "trigger" feature.

=cut

sub register_changes {
    my $self                        = shift;
    my $raw_params                  = shift || {};
    my $interface                   = shift;

    my $transaction = $interface->process(
        {
            processor_params        => {
                processor       => '_process_changes',
                changes         => $raw_params->{changes},
            },
            external_transaction_id => 'unknown',
            input_data              => 'json',
            #direct                  => 1,
        }
    );

    return $transaction;
}

sub _process_changes {
    my $self = shift;
    my $record = shift;
    my $mutations;

    my $transaction = $self->process_stash->{transaction};

    my $params = $transaction->get_processor_params();

    my $changes = $params->{changes};

    my $mutation = Zaaksysteem::Backend::Sysin::Transaction::Mutation->new(
        table    => 'Subject',
        table_id => $changes->{_subject_id}
    );

    my $subject = $transaction
                ->result_source
                ->schema
                ->resultset('Subject')
                ->search({ id => $changes->{_subject_id} })
                ->first;

    throw(
        'sysin/modules/samlidp',
        'Cannot find subject by id: ' . $changes->{_subject_id}
    ) unless $subject;

    if ($changes->{_create}) {
        $mutation->create(1);
    }
    else {
        $mutation->update(1);
    }

    my $human_readable_changes = sprintf("User %s changed\n", $subject->username);

    for my $column (keys %{ $changes }) {
        my $columndata = $changes->{ $column };
        next unless ref($columndata) eq 'HASH';

        $mutation->add_mutation(
            {
                old_value   => $columndata->{old},
                new_value   => $columndata->{new},
                column      => $column,
            }
        );

        $human_readable_changes .= 'Attribuut "' . $column
                                . '" gewijzigd naar "'
                                . $columndata->{new} . "\"\n";
    }

    $self->process_stash->{row}->{mutations} = [ $mutation ];

    $record->input($human_readable_changes);
    $record->output('User updated successfully');
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
