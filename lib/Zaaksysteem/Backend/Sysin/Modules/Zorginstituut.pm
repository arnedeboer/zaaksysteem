package Zaaksysteem::Backend::Sysin::Modules::Zorginstituut;
use Moose;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw(
    Zaaksysteem::Backend::Sysin::Modules::Roles::SOAPGeneric
);

use Zaaksysteem::Backend::Sysin::Acknowledge::Model;
use Zaaksysteem::Backend::Sysin::C2GO::Model;
use Zaaksysteem::Backend::Sysin::iWMO::Model;
use Zaaksysteem::Backend::Sysin::iJW::Model;
use Zaaksysteem::Backend::Sysin::Zorginstituut::Converter;
use Zaaksysteem::Constants qw(RGBZ_GEMEENTECODES LOGGING_COMPONENT_ZAAK);
use Zaaksysteem::Object::Model;
use Zaaksysteem::Tools;
use Zaaksysteem::Types qw/UUID/;
use Zaaksysteem::XML::Zorginstituut::WMO302;
use Zaaksysteem::XML::Zorginstituut::JW302;
use Zaaksysteem::ZAPI::Form::Field;
use Zaaksysteem::ZAPI::Form;

=head1 Interface Properties

Below a list of interface properties, see
L<Zaaksysteem::Backend::Sysin::Modules> for details.

=cut

use constant C2GO        => 'c2go';
use constant ACKNOWLEDGE => 'acknowledge';

use constant WMO301 => 'wmo301';
use constant JW301  => 'jw301';

# Only supported codes are available, but I listed them all for easy
# pick and choose later on, have a look at
#
# https://modellen.istandaarden.nl/preview/wmo/index.php/COD002
# https://modellen.istandaarden.nl/preview/jw/index.php/COD002

my $codes = {
    414 => { label => 'Toewijzing Wmo-ondersteuning', type => WMO301 },
    436 => { label => 'Toewijzing Jeugd', type => JW301 },

    ## WMO
    #415  =>   'Toewijzing Wmo-ondersteuning Retour',
    #416  =>   'Wmo Declaratie',
    #417  =>   'Wmo Declaratie Retour',
    #418  =>   'Start Wmo-ondersteuning',
    #419  =>   'Start Wmo-ondersteuning Retour',
    #420  =>   'Stop Wmo-ondersteuning',
    #421  =>   'Stop Wmo-ondersteuning Retour',
    #444  =>   'Verzoek om toewijzing Wmo-ondersteuning',
    #445  =>   'Verzoek om toewijzing Wmo-ondersteuning Retour',
    #448  =>   'Factuur Wmo-ondersteuning',
    #449  =>   'Factuur Wmo-ondersteuning Retour',
    ## JW
    #432  =>   'Declaratie Jeugdhulp',
    #433  =>   'Declaratie Jeugdhulp retour',
    #434  =>   'Declaratie Jeugd-GGZ',
    #435  =>   'Declaratie Jeugd-GGZ retour',
    #437  =>   'Toewijzing Jeugd retour',
    #438  =>   'Start Jeugdhulp',
    #439  =>   'Start Jeugdhulp Retour',
    #440  =>   'Stop Jeugdhulp',
    #441  =>   'Stop Jeugdhulp Retour',
    #446  =>   'Verzoek om toewijzing Jeugdhulp',
    #447  =>   'Verzoek om toewijzing Jeugdhulp Retour',
    #450  =>   'Factuur Jeugdhulp',
    #451  =>   'Factuur Jeugdhulp Retour',
};


my $MODULE_SETTINGS = {
    name             => 'zorginstituut',
    label            => 'iWMO en iJW',
    interface_config => [
        Zaaksysteem::ZAPI::Form::Field->new(
            name     => 'interface_gemeentecode',
            type     => 'select',
            label    => 'Gemeentecode',
            required => 1,
            data     => {
                options => [
                    sort(
                        { $a->{label} cmp $b->{label} } map({ { value => $_, label => RGBZ_GEMEENTECODES()->{$_} } } keys %{ RGBZ_GEMEENTECODES() })),
                    { value => 0,    label => 'Ongedefinieerd' },
                    { value => 9999, label => 'Test' }
                ],
            },
            description => 'Selecteer de gemeente waarop deze koppeling van toepassing is',
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name        => 'interface_berichtcode',
            type        => 'select',
            label       => 'Berichttype',
            required    => 1,
            default     => '414',
            data        => { options => [map({ { value => $_, label => uc($codes->{$_}{type}) . ": $codes->{$_}{label}" } } sort keys %{$codes})] },
            description => 'Selecteer het ondersteunde berichttype voor deze koppeling',
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name  => 'interface_aanbieder',
            type  => 'select',
            label => 'Aanbieder van iWMO',
            data  => {
                options => [
                    {
                        value => C2GO,
                        label => 'C2GO',
                    },
                    {
                        value => ACKNOWLEDGE,
                        label => 'Acknowledge',
                    },
                ],
            },
            required => 1,
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name        => 'interface_endpoint_oauth',
            type        => 'text',
            label       => 'oauth endpoint',
            description => 'OAUTH URI, deze is verplicht zodra u gebruik maakt van C2GO',
            data        => { pattern => '^https:\/\/.+' },
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name        => 'interface_endpoint_message',
            type        => 'text',
            label       => 'Message endpoint',
            required    => 1,
            description => 'URI voor het versturen van de berichten',
            data        => { pattern => '^https:\/\/.+' },
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name     => 'interface_oauth_username',
            type     => 'text',
            label    => 'Gebruikersnaam',
            required => 1,
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name     => 'interface_oauth_password',
            type     => 'password',
            label    => 'Wachtwoord',
            required => 1,
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name        => 'interface_oauth_tenant',
            type        => 'text',
            label       => 'Tenant ID',
            required    => 0,
            description => 'De tenant ID van C2GO',
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name        => 'interface_use_zorginstituut_converter',
            type        => 'checkbox',
            required    => 0,
            label       => 'Gebruikmakend van Zorginstituutconversieservice',
            description => 'Maakt deze koppeling gebruik van de conversieservice van het Zorginstituut?',
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name        => 'interface_zorginstituut_endpoint',
            type        => 'text',
            required    => 0,
            label       => 'Zorginstituutconversieservice',
            description => 'Endpoint van de conversieservice van het Zorginstituut',
            data        => { pattern => '^https:\/\/.+' },
            default     => 'https://www.istandaarden.nl/services/soap',
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name     => 'interface_zorginstituut_username',
            type     => 'text',
            required => 0,
            label    => 'Zorginstituutconversieservice gebruikersnaam',
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name     => 'interface_zorginstituut_password',
            type     => 'password',
            required => 0,
            label    => 'Zorginstituutconversieservice wachtwoord',
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name        => 'interface_timeout',
            type        => 'text',
            label       => 'Timeout',
            default     => '15',
            required    => 1,
            description => 'Timeout',
            data        => { pattern => '^\d+$' },
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name        => 'interface_spoofmode',
            type        => 'checkbox',
            label       => 'Spoofmode',
            required    => 0,
            description => 'Laat Zaaksysteem de antwoorden spoofen. Handig voor development.',
        ),
    ],
    direction                     => 'outgoing',
    retry_on_error                => 0,
    manual_type                   => ['text'],
    is_multiple                   => 1,
    is_manual                     => 0,
    allow_multiple_configurations => 0,
    is_casetype_interface         => 0,
    has_attributes                => 1,
    attribute_list                => [
        map { { external_name => $_, attribute_type => 'magic_string', all_casetypes => 1} }
            qw(
            aanbieder

            beschikkingsnummer
            beschikkingsingangsdatum
            beschikkingsafgiftedatum
            beschikkingseinddatum
            stop_beschikkingseinddatum
            stop_reden_intrekking_algemeen

            toegewezen_product_code
            toegewezen_product_categorie
            toegewezen_product_omvang_eenheid
            toegewezen_product_omvang_frequentie
            toegewezen_product_omvang_volume
            toegewezen_product_toewijzingsdatum
            toegewezen_product_ingangsdatum
            toegewezen_product_einddatum
            stop_toegewezen_product_einddatum
            toegewezen_product_reden_intrekking
            stop_toegewezen_product_reden_intrekking

            toegewezen_product_code_1
            toegewezen_product_categorie_1
            toegewezen_product_omvang_eenheid_1
            toegewezen_product_omvang_frequentie_1
            toegewezen_product_omvang_volume_1
            toegewezen_product_toewijzingsdatum_1
            toegewezen_product_ingangsdatum_1
            toegewezen_product_einddatum_1
            stop_toegewezen_product_einddatum_1
            toegewezen_product_reden_intrekking_1
            stop_toegewezen_product_reden_intrekking_1

            toegewezen_product_code_2
            toegewezen_product_categorie_2
            toegewezen_product_omvang_eenheid_2
            toegewezen_product_omvang_frequentie_2
            toegewezen_product_omvang_volume_2
            toegewezen_product_toewijzingsdatum_2
            toegewezen_product_ingangsdatum_2
            toegewezen_product_einddatum_2
            stop_toegewezen_product_einddatum_2
            toegewezen_product_reden_intrekking_2
            stop_toegewezen_product_reden_intrekking_2

            toegewezen_product_code_3
            toegewezen_product_categorie_3
            toegewezen_product_omvang_eenheid_3
            toegewezen_product_omvang_frequentie_3
            toegewezen_product_omvang_volume_3
            toegewezen_product_toewijzingsdatum_3
            toegewezen_product_ingangsdatum_3
            toegewezen_product_einddatum_3
            stop_toegewezen_product_einddatum_3
            toegewezen_product_reden_intrekking_3
            stop_toegewezen_product_reden_intrekking_3
        ),
    ],
    trigger_definition => {
        PostStatusUpdate => { method => 'post_status_update', },
    },
};

=head2 BUILDARGS

Settings for module, see

=cut

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig(%{$MODULE_SETTINGS});
};

sub _get_model {
    my ($self, $opts) = @_;

    my $interface = $opts->{interface};
    my $schema    = $interface->result_source->schema;
    my $config    = $interface->get_interface_config;


    my $zorgmodel;
    my $type = $self->_get_type_by_message_code($interface);
    if ($type eq WMO301) {
        $zorgmodel = Zaaksysteem::Backend::Sysin::iWMO::Model->new(
            interface => $interface,
            schema    => $schema,
        );
    }
    elsif ($type eq JW301) {
        $zorgmodel = Zaaksysteem::Backend::Sysin::iJW::Model->new(
            interface => $interface,
            schema    => $schema,
        );
    }
    else {
        throw("zorginstituut/message_type/unknown", "Unknown message type set: $type");
    }

    my $converter;
    if ($config->{use_zorginstituut_converter}) {
        $converter = Zaaksysteem::Backend::Sysin::Zorginstituut::Converter->new(
            endpoint => $config->{zorginstituut_endpoint},
            username => $config->{zorginstituut_username},
            password => $config->{zorginstituut_password},
        );
    }

    my $external_party = $interface->get_interface_config->{aanbieder};

    if ($external_party eq C2GO) {
        return Zaaksysteem::Backend::Sysin::C2GO::Model->new(
            interface => $interface,
            username  => $config->{oauth_username} // 'username',
            password  => $config->{oauth_password} // 'password',
            tenant    => $config->{oauth_tenant}   // 'tenant',
            instance  => $zorgmodel,
            $converter ? (converter => $converter) : (),
            synchronous => 1,
            berichttype => $type,
            berichtcode => $config->{berichtcode},
            spoof       => $config->{spoofmode},
        );
    }
    elsif ($external_party eq ACKNOWLEDGE) {
        return Zaaksysteem::Backend::Sysin::Acknowledge::Model->new(
            interface => $interface,
            username  => $config->{username} // 'username',
            password  => $config->{password} // 'password',
            instance  => $zorgmodel,
            endpoints => {
                message => $config->{endpoint_message},
            },
            berichttype => $type,
            berichtcode => $config->{berichtcode},
            spoof       => $config->{spoofmode},
        );
    }

    throw("zorginstituut/external_party/unknown", "Unknown external party set: $external_party");
}

sub _set_record_output {
    my ($self, $record, $output) = @_;

    if (ref $output eq 'HASH') {
        my $msg = $output->{errors} ? "Fout opgetreden bij %s (%s) voor zaak %s" : "%s (%s) is succesvol verstuurd voor zaak %s";
        $self->set_record_output($record, $output, sprintf($msg, uc($output->{type}), $output->{code}, $output->{case_id}));
    }
    else {
        $self->set_record_output($record, $output, "Onbekende fout opgetreden: $output",);
    }

}

sub _get_xml_from_soap {
    my ($self, $record) = @_;

    my $xml = XML::LibXML->load_xml(string => $record->input);
    my $xc  = XML::LibXML::XPathContext->new($xml);
    $xc->registerNs('SOAP', 'http://schemas.xmlsoap.org/soap/envelope/');
    $xc->registerNs('zs',   'http://www.zaaksysteem.nl/xml/zaaksysteem/zorginstituut-v1');

    my @nodes = $xc->findnodes('//SOAP:Body/*[1]');
    if (@nodes) {
        # We don't care about the SOAP bits, so use the SOAP body's
        # content as the XPath context node.
        $xc->setContextNode($nodes[0]);
        my $str = $xc->findvalue('//zs:SubmitMessage/zs:Message');
        return $str if defined $str && length($str);
        throw('zorginstituut/soap/xml/missing', "XML body is missing");
    }
    throw('zorginstituut/soap/body/missing', "SOAP body is missing");

}

sub _process_row {
    my $self = shift;
    my ($record, $row) = @_;

    my $transaction = $record->transaction_id;
    my $interface   = $transaction->interface;
    my $schema      = $interface->result_source->schema;

    my $xml;
    my $external_party = $interface->get_interface_config->{aanbieder};
    if ($external_party eq ACKNOWLEDGE) {
         $xml = $self->_get_xml_from_soap($record);
    }
    else {
        throw('zorginstituut/unsupported/external_party', "Unsupported aanbieder");
    }

    my $model = $interface->model;
    my $msg   = $model->validate_302_message($xml);

    $record->preview_string($msg);
    $record->output('');
}

sub _get_type_by_message_code {
    my ($self, $interface) = @_;
    return $codes->{$interface->get_interface_config->{berichtcode}}{type};
}

sub _get_validator {
    my ($self, $interface, $xml) = @_;
    my $type = $self->_get_type_by_message_code($interface);
    if ($type eq WMO301) {
        return Zaaksysteem::XML::Zorginstituut::WMO302->new(xml => $xml);
    }
    elsif ($type eq JW301) {
        return Zaaksysteem::XML::Zorginstituut::JW302->new(xml => $xml);
    }
}

sub _post_status_update {
    my ($self, $record) = @_;

    my $transaction = $self->process_stash->{transaction};
    my $params      = $transaction->get_processor_params();

    my $interface = $transaction->interface;

    my $xml;

    try {
        $self->log_params($params);

        foreach (qw(base_url message processor statusCode statusText kenmerken)) {
            delete $params->{$_} if exists $params->{$_};
        }

        my $schema = $interface->result_source->schema;
        my $case   = $schema->resultset('Zaak')->find($params->{case_id});
        my $code   = $interface->get_interface_config->{berichtcode};
        my $model  = $interface->model;
        $model->case($case);

        my $start = $model->is_start();
        my $stop  = $model->is_stop();

        if (!$start && !$stop) {
            throw('zorginstituut/error', "No message could be delivered, not a start, or a stop message");
        }

        if ($stop) {
            $stop  = $model->stop_xml;
            $xml->{stop}  = $stop;
        }

        if ($start) {
            $start = $model->start_xml;
            $xml->{start} = $start;
        }


        $params = {
            %$params,
            type => $model->berichttype,
            code => $model->berichtcode,
            case_id => $model->case->id,
        };

        if ($model->spoof) {

            my $msg = $model->instance->instance->spoofmode_302('writer', {});
            if ($stop) {
                $params->{answer}{stop} = $msg;
                $model->validate_302_message($msg);
            }

            if ($start) {
                $params->{answer}{start} = $msg;
                $model->validate_302_message($msg);
            }
        }
        else {
            if ($stop) {
                $params->{answer}{stop}  = $model->send_301_message($stop);
                $model->validate_synchronous($params->{answer}{stop});
            }

            if ($start) {
                $params->{answer}{start} = $model->send_301_message($start);
                $model->validate_synchronous($params->{answer}{start});
            }
            # Only here for when it dies before hand.
            delete $params->{xml};
        }

        $self->set_record(
            $record,
            input  => $xml,
            output => $params,
        );
    }
    catch {
        $params->{xml} = $xml;
        $self->set_record($record, input => $params);
        $self->catch_error($record, $_);
    };
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
