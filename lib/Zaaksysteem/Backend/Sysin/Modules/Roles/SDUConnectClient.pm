package Zaaksysteem::Backend::Sysin::Modules::Roles::SDUConnectClient;

use Moose::Role;

use DateTime;
use DateTime::Format::ISO8601;
use HTTP::Request;
use JSON qw[];
use List::Util qw[first];
use LWP::UserAgent;
use URI;
use XML::XPath;
use Zaaksysteem::Object::Types::ScheduledJob;
use Zaaksysteem::Tools;

requires qw[
    entry_type
    entry_nodeset
    entry_values
    entry_relations
];

sig entry_type      => '=> Str';
sig entry_nodeset   => '=> XML::XPath::NodeSet';
sig entry_values    => '=> HashRef';
sig entry_relations => '=> @HashRef';

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::Roles::SDUConnectClient - Wraps SDU
Connect client API

=head1 DESCRIPTION

=head2 user_agent

=cut

has user_agent => (
    is => 'rw',
    isa => 'LWP::UserAgent',
    lazy => 1,
    builder => 'build_user_agent'
);

=head1 TRIGGERS

=head2 delta_sync

=cut

define_profile delta_sync => (
    required => {
        object_model => 'Zaaksysteem::Object::Model'
    }
);

sub delta_sync {
    my $self = shift;
    my $params = assert_profile(shift)->valid;
    my $interface = shift;

    my $request = $self->build_delta_request($interface);
    my $xpath = $self->do_request_xpath($request);

    my %map = (
        create_object => '/response/addedids/itemid',
        update_object => '/response/changedids/itemid',
        delete_object => '/response/removedids/itemid'
    );

    for my $process (keys %map) {
        for my $item ($xpath->findnodes($map{ $process })) {
            $interface->process_trigger($process, {
                object_model => $params->{ object_model },
                item_id => $item->string_value
            });
        }
    }

    return 1;
}

=head2 _process_get_next_row

SDUConnect implementation of
L<Zaaksysteem::Backend::Sysin::Modules/_process_get_next_row>.

This method will behave like an iterator for object items in an import file.

The iterator state is stashed in the C<xpath>, and C<xpath_nodeset> keys in
the process stash, which should be accessible throughout the processing cycle
for the returned row.

=cut

sub _process_get_next_row {
    my $self = shift;
    my $transaction = $self->process_stash->{ transaction };

    # Using defined is important here, if the nodeset is empty (no more nodes)
    # it will stringify to the empty string, which in turn is false-like,
    # which cause the entire import to be restarted...
    unless (defined $self->process_stash->{ xpath_nodeset }) {
        my $xpath;

        if ($transaction->input_file) {
            $xpath = XML::XPath->new(filename => $transaction->input_file->get_path);
        } else {
            $xpath = XML::XPath->new(xml => $transaction->input_data);
        }

        unless (defined $xpath) {
            throw('sysin/sduconnect/data_not_found', sprintf(
                'Could not find data to process'
            ));
        }

        $self->process_stash->{ xpath } = $xpath;
        $self->process_stash->{ xpath_nodeset } = $self->entry_nodeset($xpath);
    }

    my $element = $self->process_stash->{ xpath_nodeset }->pop;

    return unless $element;

    return ($element, $element->toString);
}

=head2 _process_selector

SDUConnect implementation of
L<Zaaksysteem::Backend::Sysin::Modules/_process_selector>.

We expect to receive a parsed L<XML::XPath::Node::Element> object, which will
be used to dig up the item's (external, from our perspective) id. That id is
then used to check if we already have an object by that reference, and is the
pivot for choosing the update or create processor.

=cut

sig _process_selector => 'XML::XPath::Node::Element';

sub _process_selector {
    my $self = shift;
    my $element = shift;

    my $item_id = $self->entry_id($self->get_process_stash('xpath'), $element);

    my $object = $self->find_object_by_external_id(
        item_id => "" . $item_id,
        object_model => $self->get_process_stash('object_model'),
        interface => $self->get_process_stash('interface')
    );

    if ($object) {
        # We already have an object instance, no need to re-fetch it in the
        # _process_update_object method, so we stash it when the
        # process gets called.
        return sub {
            my $self = shift;

            $self->set_process_stash(object => $object);

            return $self->_process_update_object(@_);
        }
    } else {
        return $self->can('_process_create_object');
    }
}

=head2 create_object

This trigger creates a singular object with the most recent data from SDU
Connect.

=head3 Parameters

=over 4

=item object_model

A L<Zaaksysteem::Object::Model> instance

=item item_id

A SDU entry ID for the item-to-be-created

=back

=cut

define_profile create_object => (
    required => {
        object_model => 'Zaaksysteem::Object::Model',
        item_id => 'Str'
    }
);

sub create_object {
    my $self = shift;
    my $params = assert_profile(shift)->valid;
    my $interface = shift;

    my $request = $self->build_single_request(
        $interface,
        $params->{ item_id }
    );

    my $response = $self->user_agent->request($request);

    unless ($response->is_success) {
        throw('sysin/modules/sdu_connect/response_failure', sprintf(
            'Failed to retrieve SDU item "%s": %s',
            $params->{ item_id },
            $response->status_line
        ));
    }

    return $interface->process({
        external_transaction_id => 'n/a',
        input_data => $response->decoded_content,
        object_model => $params->{ object_model },
        processor_params => {
            processor => '_process_create_object'
        }
    });
}

=head2 update_object

This trigger updates a singular object with the most recent data from SDU
Connect.

=head3 Parameters

=over 4

=item object

The L<Zaaksysteem::Object> to be updated

=item object_model

A L<Zaaksysteem::Object::Model> instance

=back

=cut

sub update_object {
    my $self = shift;
    my $params = shift;
    my $interface = shift;

    $params = $self->_fix_trigger_args($interface, $params);

    my $request = $self->build_single_request($interface, $params->{ item_id });
    my $response = $self->user_agent->request($request);

    unless ($response->is_success) {
        throw('sysin/modules/sdu_connect/response_failure', sprintf(
            'Failed to retrieve SDU item "%s": %s',
            $params->{ item_id },
            $response->status_line
        ));
    }

    return $interface->process({
        external_transaction_id => 'n/a',
        input_data => $response->decoded_content,
        object_model => $params->{ object_model },
        processor_params => {
            object => $params->{ object },
            processor => '_process_update_object'
        }
    });
}

=head2 delete_object

This trigger removes a singular object from our database

=head3 Parameters

=over 4

=item object

The L<Zaaksysteem::Object> instance to be removed

=item object_model

A L<Zaaksysteem::Object::Model> instance

=back

=cut

sub delete_object {
    my $self = shift;
    my $params = shift;
    my $interface = shift;

    $params = $self->_fix_trigger_args($interface, $params);

    return $interface->process({
        external_transaction_id => 'n/a',
        object_model => $params->{ object_model },
        processor_params => {
            object => $params->{ object },
            processor => '_process_delete_object'
        }
    });
}

=head1 METHODS

=cut

around _load_values_into_form_object => sub {
    my $orig = shift;
    my $self = shift;
    my $opts = $_[1];

    my $form = $self->$orig(@_);

    my $schema = $opts->{ entry }->result_source->schema;
    my $object_data = $schema->resultset('ObjectData');

    my $jobs = $object_data->search({ object_class => 'scheduled_job' });
    my $job = $jobs->search_hstore($jobs->map_search_args({
        interface_id => $opts->{ entry }->id,
        job => 'SDUConnectSync'
    }))->first;

    my $next_run;
    my $last_run = $opts->{ entry }->jpath('$.last_run');

    if ($job) {
        $next_run = $job->get_object_attribute('next_run')->value;
    }

    $form->load_values({
        interface_next_sync => $next_run,
        interface_last_sync => $last_run,
    });

    return $form;
};

=head2 _process_delete_object

Implement the actual call to delete a specific object.

Called by the interface process_trigger infrastructure.

=cut

sub _process_delete_object {
    my $self = shift;
    my $record = shift;

    my $transaction = $self->get_process_stash('transaction');
    my $interface = $self->get_process_stash('interface');
    my $params = $transaction->get_processor_params;
    my $model = $self->get_process_stash('object_model');
    my $object = $params->{ object } || $self->get_process_stash('object');

    my $item_id = $self->get_mapped_value(
        $interface->get_mapped_attribute('id'),
        $object,
    );

    $model->delete(object => $object);

    $record->input(JSON->new->pretty->encode({ deleted => $item_id }));
    $record->output($object->TO_STRING);
    $record->preview_string(sprintf(
        'Object "%s" verwijderd via SDU item "%s"',
        $object->TO_STRING,
        $item_id,
    ));
}

=head2 _process_update_object

Implements the actual call and update cycle that updates a specific object.
Called by the interface process_trigger infrastructure.

=cut

sub _process_update_object {
    my $self = shift;
    my $record = shift;
    my $entry = shift;

    my $transaction = $self->get_process_stash('transaction');
    my $interface = $self->get_process_stash('interface');
    my $model = $self->get_process_stash('object_model');
    my $xpath = $self->get_process_stash('xpath');

    my $params = $transaction->get_processor_params;
    my $object = $params->{ object } || $self->get_process_stash('object');

    my $values = $self->entry_values($xpath, $entry);

    my $expires = $values->{date_expires};

    if ($expires) {
        my $today = DateTime->now();
        my $date = DateTime::Format::ISO8601->parse_datetime($expires);

        if ($date->date le $today->date) {
            # If the expiration date is on or before today, the object should
            # be deleted instead of updated.
            return $self->_process_delete_object($record, $entry);
        }
    }

    my $entry_id = $self->entry_id($xpath, $entry);
    $values->{ id } = $entry_id;

    my $mapping = $interface->jpath('$.attribute_mapping') || [];

    # Iterate the interface's attribute mapping
    # and aggregate changes on the object
    for my $attr_map (@{ $mapping }) {
        # Skip over unmapped attributes
        next unless $attr_map->{ attribute_type } eq 'magic_string';
        next unless defined $attr_map->{ internal_name };

        $self->update_mapped_value(
            $attr_map->{ internal_name }{ column_name },
            $object,
            $values->{ $attr_map->{ slug } }
        );
    }

    my @related_objects = $self->find_related_objects_by_relations(
        object_model => $model,
        interface => $interface,
        relations => [ $self->entry_relations($xpath, $entry) ]
    );

    for my $related (@related_objects) {
        $object->relate($related);
    }

    $model->save(object => $object);

    $record->input(JSON->new->pretty->encode($values));
    $record->output($object->TO_STRING);
    $record->preview_string(sprintf(
        'Object "%s" geupdate via SDU item "%s"',
        $object->TO_STRING,
        $values->{ id }
    ));
}

=head2 _process_create_object

Implements the actual call and creation cycle that builds a new object based
off a call response from the 'single object' endpoint.

Called by the interface process_trigger infrastructure.

=cut

define_profile _process_create_object => (
    required => {
        object_model => 'Zaaksysteem::Object::Model',
        xpath => 'XML::XPath',
    }
);

sub _process_create_object {
    my $self = shift;
    my $record = shift;
    my $entry = shift;

    my $transaction = $self->get_process_stash('transaction');
    my $model = $self->get_process_stash('object_model');
    my $interface = $self->get_process_stash('interface');
    my $xpath = $self->get_process_stash('xpath');

    my $entry_id = $self->entry_id($xpath, $entry);
    my $values = $self->entry_values($xpath, $entry);
    my @relations = $self->entry_relations($xpath, $entry);

    $values->{ id } = $entry_id;

    my $mapping = $interface->jpath('$.attribute_mapping') || [];
    my %instance_arguments;

    for my $attr_map (@{ $mapping }) {
        next unless $attr_map->{ attribute_type } eq 'magic_string';
        next unless defined $attr_map->{ internal_name };

        my $attribute_name = $attr_map->{ internal_name }{ column_name };

        if ($attribute_name =~ m[^attribute\.(.*)$]) {
            $attribute_name = $1;
        }

        my $value = $values->{ $attr_map->{ slug } };

        next unless $value;

        $instance_arguments{ $attribute_name } = $value;
    }

    my $objecttype = $model->inflate_from_row(
        $interface->objecttype_id
    );

    my $object = $objecttype->instance_meta_class->new_object(%instance_arguments);

    $model->save(object => $object);

    $record->input(JSON->new->pretty->encode($values));
    $record->output($object->TO_STRING);
    $record->preview_string(sprintf(
        'Object "%s" aangemaakt via SDU item "%s"',
        $object->TO_STRING,
        $entry_id
    ));
}

=head2 install_scheduled_job

This method hooks into the
L<Zaaksysteem::Backend::Sysin::Modules/active_state_callback> infrastructure
and (de)installs the L<Zaaksysteem::Object::Types::ScheduledJob> according to
the current

=cut

sub install_scheduled_job {
    my $module = shift;
    my $interface = shift;
    my $model = shift;

    # Cleanup scheduled jobs
    my $iter = $model->search('scheduled_job', {
        job => 'SDUConnectSync',
        interface_id => $interface->id
    });

    map { $model->delete(object => $_) } $iter->all;

    # If the interface is inactive, just cleaning up is enough
    return unless $interface->active;

    $module->log->debug(sprintf(
        'Installing scheduled job for "%s"',
        $interface->name
    ));

    my $job = Zaaksysteem::Object::Types::ScheduledJob->new(
        job => 'SDUConnectSync',
        interval_period => 'days',
        interval_value => 1,
        interface_id => $interface->id,
        next_run => DateTime->now(), #DateTime->today->add(days => 1, hours => 2),
        data => {
            label => sprintf(
                'Synchronisatie taak voor koppeling "%s" (collectie %s)',
                $interface->name,
                $interface->jpath('$.collection_id')
            )
        }
    );

    $model->save_object(object => $job);

    return;
}

=head2 get_mapped_value

Retrieves a value from the object using the provided mapping.

    my $value = $module->get_mapped_value($attribute_map, $object);

May throw an exception via L</get_mapped_attribute>.

=cut

sub get_mapped_value {
    my $self = shift;
    my $attr_definition = shift;
    my $object = shift;

    my $attribute = $self->get_mapped_attribute($attr_definition, $object);

    return $attribute->get_value($object);
}

=head2 update_mapped_value

Updates an object attribute via the provided mapping and value, and returns a
count of attributes modified.

    my $dirty_count = $module->update_mapped_value($attribute_map, $object, $value);

May throw an exception via L</get_mapped_attribute>.

=cut

sub update_mapped_value {
    my $self = shift;
    my $attr_definition = shift;
    my $object = shift;
    my $value = shift || '';

    # Clean return if the value to be set is the same as our current value
    return 0 if $self->get_mapped_value($attr_definition, $object) eq $value;

    my $attribute = $self->get_mapped_attribute($attr_definition, $object);

    $attribute->set_value($object, $value);

    return 1;
}

=head2 get_mapped_attribute

Retrieves an object attribute via mapped attribute definition. Throws a
C<sysin/sdu_connect/attribute_not_found> exception when the attribute cannot
be found.

=cut

sub get_mapped_attribute {
    my $self = shift;
    my $attr = shift;
    my $object = shift;

    # Wrap it up, we don't want the Moose internal exception for
    # attribute-not-found bubbling up.
    my $attribute = try { $object->object_attribute($attr) };

    unless (defined $attribute) {
        throw('sysin/sdu_connect/attribute_not_found', sprintf(
            'Could not find attribute by mapped name "%s"',
            ($attr || '<undefined>')
        ));
    }

    return $attribute;
}

=head2 build_delta_request

Builds a L<HTTP::Request> instance that SDU Connect responds to with a delta
for the configured collection.

    my $request = $module->build_delta_request($interface);

=cut

sub build_delta_request {
    my $self = shift;
    my $interface = shift;
    my $type = $self->entry_type;

    my $dt = DateTime->now->subtract(days => 1);

    my @params = (
        $type => $interface->jpath('$.api_version'),
        account => $interface->jpath('$.account_id'),
        collectie => $interface->jpath('$.collection_id'),
        startdatetime => sprintf('%sT%s', $dt->ymd(''), $dt->hms(''))
    );

    return HTTP::Request->new('GET', sprintf(
        '%s/delta/%s.xml',
        $interface->jpath('$.api_url'),
        join('/', @params)
    ));
}

=head2 build_single_request

Builds a L<HTTP::Request> instance that SDU connect response to with a sigle
product in full.

    my $request = $module->build_single_request($interface, <entry_id>);

=cut

sub build_single_request {
    my $self = shift;
    my $interface = shift;
    my $item_id = shift;
    my $type = $self->entry_type;

    my @params = (
        $type => $interface->jpath('$.api_version'),
        account => $interface->jpath('$.account_id'),
        collectie => $interface->jpath('$.collection_id'),
        item => $item_id
    );

    return HTTP::Request->new('GET', sprintf(
        '%s/single/%s.xml',
        $interface->jpath('$.api_url'),
        join('/', @params)
    ));
}

=head2 do_request_xpath

Convenience helper method, takes a L<HTTP::Request>, executes the request, and
returns an L<XML::XPath> instance with the response content loaded.

May throw a C<sysin/modules/sdu_connect_pdc/response_failure> exception if the
request failed.

    my $xpath = $module->do_request_xpath($module->build_X_request(...));

=cut

sub do_request_xpath {
    my $self = shift;
    my $request = shift;

    my $response = $self->user_agent->request($request);

    unless ($response->is_success) {
        throw('sysin/modules/sdu_connect_pdc/response_failure', sprintf(
            'Failed to request PDC delta: %s',
            $response->status_line
        ));
    }

    return XML::XPath->new(xml => $response->decoded_content);
}

=head2 find_related_objects_by_relations

=cut

define_profile find_related_objects_by_relations => (
    required => {
        object_model => 'Zaaksysteem::Object::Model',
        interface => 'Zaaksysteem::Backend::Sysin::Interface::Component',
    },
    optional => {
        # This is another one of those ArrayRef[HashRef] validations
        # Optional because otherwise the empty array is non-valid
        relations => 'HashRef'
    }
);

sub find_related_objects_by_relations {
    my ($self, %args) = @_;
    my $params = assert_profile(\%args)->valid;

    my %related_by_type;
    my @related_objects;

    for my $relation (@{ $params->{ relations } || [] }) {
        my $type = $relation->{ type };

        $related_by_type{ $type } //= [];

        push @{ $related_by_type{ $type } }, $relation;
    }

    for my $type_name (keys %related_by_type) {
        my $type;

        if ($self->entry_type eq $type_name) {
            $type = $params->{ object_model }->retrieve(
                uuid => $params->{ interface }->get_column('objecttype_id')
            );
        } else {
            my $type_spec = first {
                defined $_->{ handles_type } && $_->{ handles_type } eq $type_name;
            } @{ $params->{ interface }->jpath('$.attribute_mapping') };

            unless (defined $type_spec && defined $type_spec->{ internal_name }) {
                $self->log->debug(sprintf(
                    'Not configured to handle "%s" relations',
                    $type_name
                ));

                next;
            }

            $type = $params->{ object_model }->retrieve(
                uuid => $type_spec->{ internal_name }{ id }
            );
        }

        unless (defined $type) {
            $self->log->debug(sprintf(
                'Could not retrieve "%s" objecttype for relating',
                $type
            ));

            next;
        }

        for my $relation (@{ $related_by_type{ $type_name }}) {
            push @related_objects, $params->{ object_model }->search(
                $type->prefix,
                { pdc_extern_id => $relation->{ id } }
            );
        }
    }

    return @related_objects;
}

=head2 find_object_by_external_id

Convenience method for retrieving a L<Zaaksysteem::Object> instance for the
supplied id.

=cut

define_profile find_object_by_external_id => (
    required => {
        object_model => 'Zaaksysteem::Object::Model',
        interface => 'Zaaksysteem::Backend::Sysin::Interface::Component',
        item_id => 'Str'
    }
);

sub find_object_by_external_id {
    my ($self, %args) = @_;
    my $params = assert_profile(\%args)->valid;

    my $attribute = $params->{ interface }->get_mapped_attribute('id');

    return $params->{ object_model }->search(
        $params->{ interface }->objecttype_id->get_object_attribute('prefix')->value,
        { $attribute => $params->{ item_id } },
    )->next;
}

=head2 _fix_trigger_args

Helper method for the update/delete triggers, asserts and normalizes the supplied
arguments, which may contain an object or an item_id, depending on who's calling.

=cut

define_profile _fix_trigger_args => (
    required => {
        object_model => 'Zaaksysteem::Object::Model',
    },
    optional => {
        object => 'Zaaksysteem::Object',
        item_id => 'Str'
    },
    require_some => {
        object_or_id => [ 1, qw[object item_id] ]
    }
);

sub _fix_trigger_args {
    my $self = shift;
    my $interface = shift;
    my $args = assert_profile(shift)->valid;

    if (not exists $args->{ item_id }) {
        $args->{ item_id } = $self->get_mapped_value(
            $interface->get_mapped_attribute('id'),
            $args->{ object }
        );
    }

    if (not exists $args->{ object }) {
        $args->{ object } = $self->find_object_by_external_id(
            object_model => $args->{ object_model },
            interface => $interface,
            item_id => $args->{ item_id }
        );
    }

    return $args;
}

=head2 test_connection

Checks if the configured host is connectable using a secure HTTP connection.

See L<Zaaksysteem::Backend::Sysin::Modules::Roles::Tests/test_host_port_ssl>.

=cut

sub test_connection {
    my $self = shift;
    my $interface = shift;

    return $self->test_host_port_ssl($interface->jpath('$.api_url'));
}

=head2 test_get_delta

Interface test to check if the configured data source is accessible.

=cut

sub test_get_delta {
    my $self = shift;
    my $interface = shift;

    my $xpath = try {
        my $request = $self->build_delta_request($interface);
        return $self->do_request_xpath($request);
    } catch {
        $self->log->error("Exception while reading PDC delta for test: $_");
        return;
    };

    unless ($xpath) {
        throw('sysin/modules/test/error', 'Ophalen van PDC delta mislukt');
    }

    if ($xpath->find('/response/error')) {
        throw('sysin/modules/test/error', sprintf(
            'Ophalen van PDC delta mislukt: %s',
            $xpath->findvalue('/response/error/description')
        ));
    }

    unless ($xpath->find('/response/addedids')) {
        throw('sysin/modules/test/error', sprintf(
            'Ophalen van PDC delta geeft corrupte data, neem contact op met SDU'
        ));
    }

    return 1;
}

=head2 build_user_agent

Builder for L</user_agent>.

=cut

sub build_user_agent {
    my $self = shift;

    my $agent = LWP::UserAgent->new(
        agent => sprintf('Zaaksysteem/%s', $Zaaksysteem::VERSION),
        timeout => 30,
        ssl_opts => {
            SSL_ca_path => '/etc/ssl/certs',
        },
    );

    return $agent;
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Backend::Sysin::Modules::SDUConnect::PDC>,
L<Zaaksysteem::Backend::Sysin::Modules::SDUConnect::VAC>,
L<Zaaksysteem::Manual>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
