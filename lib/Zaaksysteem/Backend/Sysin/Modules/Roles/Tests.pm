package Zaaksysteem::Backend::Sysin::Modules::Roles::Tests;

use Moose::Role;
use Zaaksysteem::Exception;
use URI;
use Try::Tiny;
use IO::Socket::INET;
use IO::Socket::SSL;

=head2 test_host_port($url)

Connection testing. Will get the host and port from the given url, and tries a simple
connect on the host and port.

=cut

sub test_host_port {
    my ($self, $endpoint) = @_;

    my $uri = try {
        URI->new($endpoint);
    } catch {
        throw(
            'sysin/modules/test/error',
            'Could not perform test, is the endpoint url correct? (' . $endpoint .')'
        );
    };

    my $host = $uri->host;
    my $port = $uri->port;

    my $sock = IO::Socket::INET->new(
        PeerPort => $port,
        PeerAddr => $host,
        Timeout  => 10,
        Proto    => 'tcp',
    );

    unless ($sock) {
        throw(
            'sysin/modules/test/error',
            "Could not connect with host and port, are you sure your firewall is correct?"
            . " [$host:$port]"
        );
    }

    return $sock;
}

=head2 test_host_port_ssl($url, $ca_cert_path)

Connection testing. Will get the host and port from the given url, tries a
simple connect on the host and port and tries to do a TLS handshake, checking
if the server's certificate is signed by the supplied CA certificate chain.

=cut

sub test_host_port_ssl {
    my ($self, $endpoint, $ca_cert) = @_;
    my $uri = URI->new($endpoint);

    my $host = $uri->host;
    my $port = $uri->port;

    if ($uri->scheme ne 'https') {
        throw(
            'sysin/modules/test/error',
            "Opgegeven URL is niet beveligd (https://)."
        );
    }

    my $sock = IO::Socket::SSL->new(
        PeerPort            => $port,
        PeerAddr            => $host,
        Timeout             => 10,
        Proto               => 'tcp',
        SSL_ca_file         => $ca_cert,
        SSL_verify_mode     => 1,
        SSL_verifycn_scheme => 'www'
    );

    unless ($sock) {
        throw(
            'sysin/modules/test/error',
            "Kon geen verbinding maken met de opgegeven host." 
            . " Staat de firewall goed en kloppen het CA-certificaat en de CN van het certificaat?"
            . " [$host:$port].",
            [
                { ssl_error => IO::Socket::SSL::errstr(), }
            ]
        );
    }

    return;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

