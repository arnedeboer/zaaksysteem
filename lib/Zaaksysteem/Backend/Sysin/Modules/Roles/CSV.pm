package Zaaksysteem::Backend::Sysin::Modules::Roles::CSV;

use Moose::Role;
use Encode qw(decode);
use Text::CSV_XS;
use Zaaksysteem::Exception;

=head2 _csv_validate_header

Return value: $BOOL_SUCCESS

    $self->_csv_validate_header($filestore_object);

Checks for a valid CSV file and validates the header against the C<attribute_list>
attribute.

A valid CSV has a header containing all the C<external_name> attributes as given
in the attribute_list.

B<Errors>

Throws a L<Zaaksysteem::Exception::Base> error, when an error occurs.

=over 4

=item modules/csv/no_attributes_list

No C<attribute_list> is set, validating a header is useless

=item modules/csv/file_error

Cannot read the given filestore->get_path path.

=item modules/csv/no_rows

Cannot find any rows in this CSV

=item modules/csv/invalid_header

Invalid header, it does not contain all the defined fields in C<attribute_list>.

The returning object is in the form:

 {
    name    => 'header_column1',
    missing => 1,
 },
 {
    name    => 'header_column2',
    invalid => 1,
 }

=back

=cut

sub _csv_validate_header {
    my $self        = shift;
    my $filestore   = shift;


    throw(
        'modules/csv/no_attributes_list',
        'Cannot find CSV header'
    ) unless $self->attribute_list && scalar (@{ $self->attribute_list });

    my ($csv, $fh)  = $self->_get_csv_and_fh($filestore);
    my $header      = $csv->getline($fh);

    $csv->eof or $csv->error_diag();
    close $fh;

    throw(
        'modules/csv/no_rows',
        'Invalid CSV'
    ) unless $header;

    my %config_columns  = map { $_->{external_name} => 1 } @{ $self->attribute_list };
    my %csv_columns     = map { $_ => 1 } @{ $header };

    my @invalid_cols;
    for my $def_column (keys %config_columns) {
        my $csv_column = $csv_columns{$def_column};

        if (!$csv_column) {
            push(@invalid_cols,
                {
                    name    => $def_column,
                    missing => 1,
                }
            );
            next;
        }
    }

    ### Check for columns in csv not found in attribute list
    my @unknown_columns;
    for my $col (@$header) {
        if (!$config_columns{$col}) {
            push (@unknown_columns, $col);
        }
    }

    throw(
        'modules/csv/invalid_header',
        'Problem in CSV header, missing columns: ('
        . join(',', map { $_->{name} } @invalid_cols)
        . ') / unknown columns: (' . join(',', @unknown_columns) . ')',
        [@invalid_cols, @unknown_columns]
    ) if (@invalid_cols || @unknown_columns);
}

sub _get_csv_and_fh {
    my $self        = shift;
    my $filestore   = shift;

    my $options = { binary => 1 };

    if(exists $self->parser_options->{sep_char}) {
        $options->{sep_char} = $self->parser_options->{sep_char};
    }

    my $csv = Text::CSV_XS->new($options);

    use File::BOM qw(:subs);
    use autodie;
    my $fh;
    my ($encoding, $spill) = open_bom($fh, $filestore->get_path);
    no autodie;

    return ($csv, $fh, $encoding, $spill);
}

sub _process_validate_input {
    my $self            = shift;
    my $transaction     = shift;

    ### We do not allow string CSV
    throw(
        'sysin/bagcsv/invalid_input_format',
        'Invalid input format, we only allow filestore references'
    ) if (
        $self->parser_options->{input_file_only} &&
        !$transaction->input_file
    );

    ### Check for valid CSV, checks header etc
    $self->_csv_validate_header($transaction->input_file)
        if $self->parser_options->{validate_header}
}

sub _process_get_next_row {
    my $self        = shift;

    $self->_csv_load_stash();

    my $csv         = $self->process_stash->{csv}->{parser};
    my $fh          = $self->process_stash->{csv}->{fh};

    return if $csv->eof();

    my $row         = $csv->getline_hr($fh);

    return unless $row;

    for my $key (keys %$row) {
        $row->{ $key } = try {
            decode('UTF-8',  $row->{ $key }, Encode::FB_CROAK);
        } catch {
            # "Windows 1252" is Microsoft's flavour of ISO-8859-1
            decode('cp1252', $row->{ $key }, Encode::FB_CROAK);
        };
    }

    my @string      = @_;
    for my $key ($csv->column_names) {
        push(@string, $row->{ $key });
    }

    return ($row, '"' . join('","', @string) . '"');
}

sub _csv_load_stash {
    my $self        = shift;

    return if $self->process_stash->{csv}->{fh};

    my $filestore   = $self
                    ->process_stash
                    ->{transaction}
                    ->input_file;

    my ($csv, $fh)  = $self->_get_csv_and_fh(
        $filestore
    );

    $self->_csv_load_header($csv, $fh);

    $self->process_stash->{csv} = {
        parser      => $csv,
        fh          => $fh
    };
}

sub _csv_load_header {
    my $self        = shift;
    my $csv         = shift;
    my $fh          = shift;

    return unless $self->attribute_list && scalar (@{ $self->attribute_list });

    $csv->column_names($csv->getline($fh));
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

