package Zaaksysteem::Backend::Sysin::Modules::Roles::StUF;

use Moose::Role;

use Data::Dumper;
use Zaaksysteem::StUF;
use Zaaksysteem::Tools;

sub stuf_throw {
    my $self                = shift;
    my ($object, $type, $message, $extra_options)    = @_;

    throw('stuf_throw', 'Missing required arguments') unless $object && $type && $message;

    my $stufmessage = $object->fout(
        {
            reference_id            => (
                $self->process_stash->{transaction}
                    ? $self->process_stash->{transaction}->id
                    : 'T' . time()
            ),
            date            => DateTime->now(),
            code            => 'StUF003',
            plek            => 'client',
            omschrijving    => $type .': ' . $message,
        }
    );

    $extra_options           ||= {};

    throw(
        $type,
        $message, {
            transaction_output  => $stufmessage,
            %{ $extra_options }
        }
    );
}

sub is_active {
    my $self            = shift;
    my $interface       = shift;

    my $conf_interface  = $self->get_config_interface($interface);

    return unless $conf_interface->active;

    return 1 if $interface->active;
}

sub get_config_from_config_iface {
    my $self            = shift;
    my $interface       = shift;

    my $conf_interface  = $self->get_config_interface($interface);

    return $conf_interface->get_interface_config;
}

=head2 $module->_get_stuf_version($interface)

Return value: $STUF_VERSION

Returns the configured version in the interface for this StUF module. Defaults to C<0204>. As
first parameter, please supply the current interface object, e.g. C<< transaction->interface_id >>

B<Possible return values>

=over 4

=item 0204

Version 0204

=item 0310

Version 0310 of BG (or 0301 StUF)

=back

=cut

sub _get_stuf_version {
    my $self            = shift;
    my $interface       = shift;

    my $version         = '0204';

    if ($interface->get_interface_config->{stuf_version}) {
        $version        = $interface->get_interface_config->{stuf_version};
    } elsif ($self->can('stuf_version')) {
        $version        = $self->stuf_version;
    }

    return $version;
}


=head2 _process_get_next_row

Single xml mode only

=cut

sub _process_get_next_row {
    my $self        = shift;

    my $xml;

    ### Via processor params
    if ($self->process_stash->{transaction}->get_processor_params) {
        return if ($self->process_stash->{once});

        $self->process_stash->{once} = 1;

        if (my $params = $self->process_stash->{transaction}->get_processor_params) {
            return ($params, Data::Dumper::Dumper($params));
        }

        return;
    }

    if ($self->process_stash->{transaction}->input_file) {
        if (!$self->process_stash->{stuf}->{entries}) {
            my $xmlobj  = XML::LibXML->load_xml(
                'location'  => $self->process_stash->{transaction}->input_file->get_path()
            );

            ### Find first kennisgevingsBericht
            my @nodes   = $xmlobj->findnodes("//*[local-name()='kennisgevingsBericht']");

            my @entries;
            for my $node (@nodes) {
                push(@entries, $node->toString());
            }

            $self->process_stash->{stuf}->{entries} = \@entries;
        }

        return unless $self->process_stash->{stuf}->{entries};

        my $pointer     = $self->process_stash->{stuf}->{pointer};

        if ($pointer) {
            $xml        = $self->process_stash->{stuf}->{entries}->[
                $pointer++
            ];
        } else {
            $pointer = 0;

            $xml        = $self->process_stash->{stuf}->{entries}->[
                $pointer++
            ];
        }

        $self->process_stash->{stuf}->{pointer} = $pointer;

    } else {

        return if $self->process_stash->{stuf}->{xml};

        return unless $self
                    ->process_stash
                    ->{transaction}
                    ->input_data;

        $xml            = $self->process_stash->{stuf}->{xml} = $self
                        ->process_stash
                        ->{transaction}
                        ->input_data;
    }

    return unless $xml;

    my $stuf_config = $self->get_config_from_config_iface(
        $self->get_config_interface($self->process_stash->{transaction})
    );

    my $interface   = $self
                    ->process_stash
                    ->{transaction}
                    ->interface_id;

    my $object      = Zaaksysteem::StUF->from_xml(
        $xml,
        {
            cache => ($self->process_stash->{transaction}->result_source->schema->default_resultset_attributes->{'stuf_cache'} ||= {}),
            version => $self->_get_stuf_version($interface)
        }
    );

    if ($object && $object->stuurgegevens->referentienummer) {
        $self->process_stash->{transaction}->external_transaction_id(
            $object->stuurgegevens->referentienummer
        );
        $self->process_stash->{transaction}->update;
    }

    $self->process_stash->{stuf}->{xml} = $xml || 1;

    return ($object, $xml);
}

sub get_config_interface {
    my $self            = shift;
    my $row             = shift;

    my $config          = $row->result_source->schema->resultset('Interface')->find_by_module_name(
        'stufconfig'
    );

    throw(
        'sysin/interface/stuf/get_config_interface/no_interface',
        'No configuration interface found, cannot handle'
    ) unless $config;

    return $config;
}

=head2 $module->_process_selector

Return value: subref to module processor

=cut

sub _process_selector {
    my $self            = shift;
    my $object          = shift;
    my $record          = shift;

    $self->stuf_throw(
        $object,
        'sysin/modules/stuf/process/inactive',
        'Interface is inactive, StUF messages cannot be processed',
    ) unless $self->is_active($record->transaction_id->interface_id);

    if (my $params = $record->transaction_id->get_processor_params) {
        if ($params->{processor}) {
            return $self->can($params->{processor});
        }
    }

    if (lc($object->stuurgegevens->berichtsoort) eq 'lk01') {
        return $self->can('_process_kennisgeving');
    }

    throw(
        'sysin/modules/prs/process_selector',
        'No support for this message built in zaaksysteem:' . burp($object->stuurgegevens)
    );
}

=head2 $module->_process_kennisgeving($transaction_record, $rowobject)

Return value: $STRING_RESPONSE_XML

Processes the StUF XML "row" (message).

B<Params>

=over 4

=item $transaction_record

The L<Zaaksysteem::Backend::Sysin::Transaction::Component> row. It will be filled
with object_preview, a preview string describing the data object changed.

=item $rowobject

The source of the data, in case of a CSV this would probably a HASHREF row, but in
this case, it is a L<Zaaksysteem::StUF::PRS> object.

=back

=cut

sub _process_kennisgeving {
    my $self            = shift;
    my $record          = shift;
    my $object          = shift;

    $self->_validate_stuf_message($object);

    $self->process_stash->{row}->{mutations} = [];

    try {
        $self->_stuf_process_mutations($record, $object);
    } catch {
        print STDERR "StUF Caught error\n";
        $record->is_error(1);

        if (
            UNIVERSAL::isa($_, 'Zaaksysteem::Exception::Base')
        ) {
            if ($_->message !~ /Validation of profile failed/) {
                $_->throw;
            }

            $record->transaction_id->error_message(
                $_->message . ".\n\n"
                . "Message could not be processed in zaaksysteem. "
                . "We DID send an acknowledge to make sure other messages will be processed."
            );
        } else {
            $self->stuf_throw(
                $object,
                'sysin/modules/stuf/process/error',
                $_
            );
        }

        $record->transaction_id->update;
    };

    ### We did not die yet, response with bevestiging
    $record->output(
        $object->acknowledge(
            {
                date            => $record->transaction_id->date_created,
                reference_id    => $record->transaction_id->id,
            }
        )->to_xml
    );
    $record->update;

    return 1;

}

sub _stuf_process_mutations {
    my $self            = shift;
    my $record          = shift;
    my $object          = shift;
    my $mutations;

    my $mutatiesoort    = $object->mutatiesoort;

    ### Create
    if ($mutatiesoort eq 'T') {
        ### Sleutel already exists?
        my $subscription;
        eval {
            $subscription = $self->get_entry_from_subscription(
                $record,
                $object,
                $self->stuf_subscription_table
            );
        };

        if ($@ && $@ !~ /no_entry_found/) {
            die($@);
        }

        if (!$subscription) {
            my $entry  = $self->stuf_create_entry($record, $object);
            
            ### XXX TODO
            ### OPTIONAL: when indicatorOvername = V (verplicht), we set a subscription
            ### when it is not already set
            $self->_add_subscription_for_entry($record, $object, $entry) if $entry;
        } else {
            $mutatiesoort = 'W';
        }
    }

    ### Update (or Correction)
    if ($mutatiesoort eq 'W' || $mutatiesoort eq 'C') {
        $self->stuf_update_entry($record, $object);
    }

    ### Delete
    if ($mutatiesoort eq 'V') {
        $self->get_entry_from_subscription(
            $record,
            $object,
            $self->stuf_subscription_table
        );

        $self->stuf_delete_entry($record, $object);
    }
}


=head2 OBJECT SUBSCRIPTIONS

=head2 $module->enable_subscription($natuurlijk_persoon)

Return value: $BOOL_SUCCESS

=cut

sub get_entry_from_subscription {
    my $self                                = shift;
    my ($record, $object, $local_table, $object_subscription)    = @_;

    my $config_module                       = $self->get_config_interface(
        $record->transaction_id->interface_id
    );

    unless ($object_subscription) {
        my $params              = $object->as_params;
        my $external_id         = $params->{ $config_module->module_object->get_primary_key($config_module) };

        $object_subscription    = $record
                                ->result_source
                                ->schema
                                ->resultset('ObjectSubscription')
                                ->search(
                                    {
                                        interface_id    => $record
                                                        ->transaction_id
                                                        ->interface_id
                                                        ->id,
                                        local_table     => $local_table,
                                        external_id     => $external_id,
                                    }
                                )->first;

        if (!$object_subscription && $params->{sleutelOntvangend}) {
            my $local_id            = $params->{sleutelOntvangend};

            ### System sends our local id, try to find it.
            $object_subscription    = $record
                                    ->result_source
                                    ->schema
                                    ->resultset('ObjectSubscription')
                                    ->search(
                                        {
                                            id              => $local_id,
                                            interface_id    => $record
                                                            ->transaction_id
                                                            ->interface_id
                                                            ->id,
                                            local_table     => $local_table,
                                        }
                                    )->first;

            ### Update external_id
            if ($object_subscription) {
                $object_subscription->external_id($external_id);
                $object_subscription->update;
            }
        }
    }

    $self->stuf_throw(
        $object,
        'sysin/modules/stuf/process/no_entry_found',
        'No existing entry found for this subject (' . $local_table . ') in our database'
    ) unless $object_subscription;

    my $entry               = $record
                            ->result_source
                            ->schema
                            ->resultset($local_table)
                            ->search(
                                {
                                    id      => $object_subscription->local_id,
                                }
                            )->first;

    $self->stuf_throw(
        $object,
        'sysin/modules/stuf/process/no_np_entry_found',
        'No existing ' . $local_table . ' found for this subject (' . $local_table . ') in our database'
    ) unless $entry;

    return $entry;
}


=head2 _process_disable_subscription

Arguments:

=cut

sub _process_disable_subscription {
    my $self                        = shift;
    my ($record,$object)            = @_;

    my $p_params                    = $record
                                    ->transaction_id
                                    ->get_processor_params->{params};

    my $interface                   = $record->transaction_id->interface_id;

    my $config_module               = $self->get_config_interface(
        $interface
    );

    ### First delete the record in our DB, when our call below fails,
    ### we can rollback this. It will not work the other way around
    my $subscription                = $interface->object_subscriptions->find($p_params->{subscription_id});

    my $module_cfg                  = $self->get_config_from_config_iface(
        $interface
    );

    $self->stuf_throw(
        $object,
        'sysin/modules/stufprs/_process_disable_subscription/subscription_not_found',
        'Could not find subscription'
    ) unless $subscription;

    my $return_output = '';
    if (
        $config_module->module_object->can_connect_to_other_sbus($config_module)
    ) {
        my $transaction = $record->transaction_id;

        my $call_options        = $self->_load_call_options_from_config(
            $transaction->interface_id,
            {
                reference           => $transaction->id,
                datetime            => DateTime->now(),
                follow_subscription => 1,
                calltype            => 'async',
                set_subscription    => 1,
            }
        );

        my ($rv, $trace)        = $self->stuf0204->disable_subscription(
            {
                external_id     => $p_params->{external_id},
                subscription_id => $p_params->{subscription_id},
                entity_type     => uc($self->stuf_object_type),
            },
            $call_options
        );

        $transaction->external_transaction_id($transaction->id);
        $transaction->direction('outgoing');
        $transaction->update;

        unless ($rv) {
            throw(
                'sysin/modules/stuf' . lc($self->stuf_object_type) . '/disable_natuurlijkpersoon_subscription/error',
                'Failed disabling natuurlijk_persoon_subscription: ' . Data::Dumper::Dumper($trace)
            );
        }

        $record->input($trace->request->content);
        $return_output = $trace->response->content;

    } else {
        $return_output = 'Subscription removed, but no interaction started with "StUF Makelaar"'
                .': synchronization-mode is not "hybrid" or "question". '
                . $p_params->{external_id} . '" must be manually removed';
    }


    ### Delete this person
    $self->stuf_delete_entry($record, $object, $subscription);

    $subscription->date_deleted(DateTime->now());
    $subscription->update;

    $record->output($return_output);

    return $return_output;
}


=head2 $module->_add_subscription_for_entry($transaction_record, $rowobject, $natuurlijk_persoon)

Return value: $BOOL_SUCCES

Generates a L<Zaaksysteem::Backend::Sysin::ObjectSubscription::Component> row, to
subscribe our data record with a datarecord from our interface

B<Params>

=over 4

=item $transaction_record

The L<Zaaksysteem::Backend::Sysin::Transaction::Component> row. It will be filled
with object_preview, a preview string describing the data object changed.

=item $rowobject

The source of the data, in case of a CSV this would probably a HASHREF row, but in
this case, it is a L<Zaaksysteem::StUF::PRS> object.

=item $natuurlijk_persoon

The L<Zaaksysteem::DB::Component::NatuurlijkPersoon> row to enable a subscription on.

=back

=cut

sub _add_subscription_for_entry {
    my $self                = shift;
    my ($record, $object, $entry)   = @_;

    my $config_module                       = $self->get_config_interface(
        $record->transaction_id->interface_id
    );

    ### NatuurlijkPersoon
    my $subscription        = $record
                            ->result_source
                            ->schema
                            ->resultset('ObjectSubscription')
                            ->search(
                                {
                                    interface_id    => $record
                                                    ->transaction_id
                                                    ->interface_id
                                                    ->id,
                                    local_table     => $entry->result_source->source_name,
                                    local_id        => $entry->id,
                                    external_id     => $object->as_params->{$config_module->module_object->get_primary_key($config_module)},
                                }
                            )->first;

    if (!$subscription) {

        $subscription       = $record
                            ->result_source
                            ->schema
                            ->resultset('ObjectSubscription')
                            ->create(
                                {
                                   interface_id     => $record
                                                    ->transaction_id
                                                    ->interface_id
                                                    ->id,
                                   local_table      => $entry->result_source->source_name,
                                   local_id         => $entry->id,
                                   external_id      => $object->as_params->{$config_module->module_object->get_primary_key($config_module)}
                                }
                            );
    }

    $subscription->object_preview(
        $record->preview_string
    );
    $subscription->update;
}

=head2 $module->_remove_subscription_from_entry($transaction_record, $rowobject, $natuurlijk_persoon)

Return value: $BOOL_SUCCES

Sets the C<date_deleted> on a L<Zaaksysteem::Backend::Sysin::ObjectSubscription::Component>
row, to unsubscribe our data record.

B<Params>

=over 4

=item $transaction_record

The L<Zaaksysteem::Backend::Sysin::Transaction::Component> row. It will be filled
with object_preview, a preview string describing the data object changed.

=item $rowobject

The source of the data, in case of a CSV this would probably a HASHREF row, but in
this case, it is a L<Zaaksysteem::StUF::PRS> object.

=item $natuurlijk_persoon

The L<Zaaksysteem::DB::Component::NatuurlijkPersoon> row to disable a subscription on.

=back

=cut

sub _remove_subscription_from_entry {
    my $self                = shift;
    my ($record, $object, $entry)   = @_;

    ### NatuurlijkPersoon
    my $subscription        = $record
                            ->result_source
                            ->schema
                            ->resultset('ObjectSubscription')
                            ->search(
                                {
                                    interface_id    => $record
                                                    ->transaction_id
                                                    ->interface_id
                                                    ->id,
                                    local_table     => $entry->result_source->source_name,
                                    local_id        => $entry->id,
                                }
                            )->first;

    if ($subscription) {
        $subscription->date_deleted(DateTime->now());
        $subscription->update;
    }
}

=head2 $module->_validate_stuf_message($transaction_record)

Return value: $BOOL_SUCCES

Validates the given StUF message for a correct C<entiteittype> and C<mutatiesoort>

B<Params>

=over 4

=item $transaction_record

The L<Zaaksysteem::Backend::Sysin::Transaction::Component> row. It will be filled
with object_preview, a preview string describing the data object changed.

=back

=cut

sub _validate_stuf_message {
    my $self            = shift;
    my $object          = shift;

    # $self->stuf_throw(
    #     $object,
    #     'sysin/modules/stuf' . lc($self->stuf_object_type) . '/process/wrong_entiteittype',
    #     'Invalid entiteittype for processor: ' . $object->entiteittype
    # ) unless ($object->entiteittype eq $self->stuf_object_type);

    $self->stuf_throw(
        $object,
        'sysin/modules/stuf' . lc($self->stuf_object_type) . '/process/unknown_mutatiesoort',
        'Cannot determine what action to run because of missing mutatiesoort'
    ) unless (
        $object->mutatiesoort
    );

}

=head2 $module->disable_subscription($object_subscription)

Return value: $BOOL_SUCCESS

=cut

sub disable_subscription {
    my $self            = shift;
    my $params          = shift;
    my $interface       = shift;

    my $subscription    = $params->{subscription_id};

    throw(
        'sysin/modules/stuf' . lc($self->stuf_object_type) . '/process/disable_subscription/no_subscription_id',
        'Impossible to disable subscription, subscription not given'
    ) unless $subscription;

    unless (UNIVERSAL::isa($subscription, 'DBIx::Class')) {
        $subscription   = $interface
                        ->object_subscriptions
                        ->find($subscription);
    }

    my $module_cfg                  = $self->get_config_from_config_iface(
        $interface
    );

    my $config_module               = $self->get_config_interface(
        $interface
    );

    throw(
        'sysin/modules/stuf' . lc($self->stuf_object_type) . '/process/disable_subscription',
        'Impossible to disable subscription, subscription not found in db'
    ) unless $subscription;

    $interface->process(
        {
            direction               => 'outgoing',
            external_transaction_id => 'unknown',
            input_data              => 'Disable subscription: ' . $subscription->external_id,
            processor_params        => {
                processor                       => '_process_disable_subscription',
                params                          => {
                    subscription_id         => $subscription->id,
                    external_id             => $subscription->external_id,
                }
            },
        }
    );
}


sub _load_call_options_from_config {
    my $self            = shift;
    my $interface       = shift;
    my $options         = shift || {};

    my $module_cfg      = $self->get_config_from_config_iface(
        $interface
    );

    my $rv              = {
        sender                  => $module_cfg->{mk_sender},
        receiver                => (
            $options->{via_gbav}
                ? $module_cfg->{gbav_applicatie}
                : $module_cfg->{mk_ontvanger}
        ),
        reference               => $options->{reference},
        datetime                => $options->{datetime}->strftime('%Y%m%d%H%M%S00'),
        dispatch                => {},
        key_type                => ($module_cfg->{stuf_supplier} eq 'pink' ? 'gegevens' : 'ontvangend'),
        gemeentecode            => $module_cfg->{gemeentecode}
    };

    ### We need another receiver voor "afnemerindicaties"
    $rv->{receiver}     = $module_cfg->{mk_ontvanger_afnemer} if $options->{set_subscription};
    $rv->{supplier}     = $module_cfg->{stuf_supplier};

    if ($module_cfg->{mk_spoof}) {
        $rv->{dispatch}->{transport_hook} = sub {
            my $request             = shift;
            my $stuf0204            = Zaaksysteem::XML::Compile
                                    ->xml_compile
                                    ->add_class('Zaaksysteem::StUF::0204::Instance')
                                    ->stuf0204;

            return $stuf0204->spoof_answer($request);

            # return HTTP::Response->new(200, 'answer manually created',
            #     [ 'Content-Type' => 'text/xml' ], '<xml><my /><custom /></xml>'
            # );
        };
    } else {
        my $config          = $interface->result_source->schema->catalyst_config;

        # $config = {
        #     services => {
        #         ssl_key    => '/vagrant/services.key',
        #         ssl_crt    => '/vagrant/services.crt',
        #     }
        # };

        my %transport_opts  = (
            timeout     => 15,
        );

        if ($options->{calltype} && $options->{calltype} eq 'async') {
            $transport_opts{address}    = $module_cfg->{mk_async_url};
        } elsif ($options->{calltype} && $options->{calltype} eq 'pink_gbav') {
            $transport_opts{address}    = $module_cfg->{gbav_pink_url};
        } else {
            $transport_opts{address}    = $module_cfg->{mk_sync_url};
        }

        my $transport   = XML::Compile::Transport::SOAPHTTP->new(%transport_opts);

        #$transport->userAgent->default_header('SOAPAction', $self->soap_action);

        if ($config->{services}) {
            $transport->userAgent->ssl_opts(
                'SSL_key_file'  => $config->{services}{ssl_key}
            );

            $transport->userAgent->ssl_opts(
                'SSL_cert_file' => $config->{services}{ssl_crt}
            );
        }

        $transport->userAgent->ssl_opts(
            'verify_hostname' => 0
        );

        $rv->{dispatch}->{transport} = $transport;
    }

    return $rv;
}



# sub enable_subscription {
#     my $self            = shift;
#     my $params          = shift;
#     my $interface       = shift;

#     my $subscription    = $params->{subscription_id};

#     throw(
#         'sysin/modules/stuf' . lc($self->stuf_object_type) . '/process/disable_subscription',
#         'Impossible to disable subscription, subscription not found'
#     ) unless $subscription;

#     my $stufmsg     = Zaaksysteem::StUF->new(
#         entiteittype    => $self->stuf_object_type,
#         stuurgegevens   => Zaaksysteem::StUF::Stuurgegevens->new(
#             zender              => {
#                 applicatie          => 'ZSNL',
#             },
#             ontvanger           => {
#                 applicatie          => 'CGM',
#             },
#         ),
#     )->set_afnemerindicatie(
#         {
#             reference_id            => 29237824,
#             date                    => DateTime->now(),
#             sleutelGegevensbeheer   => $subscription,
#             afnemerindicatie        => 1,
#         }
#     );

#     return $interface->process(
#         {
#             external_transaction_id => 'unknown',
#             input_data              => $stufmsg->to_xml,
#         }
#     );
# }

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 get_config_from_config_iface

TODO: Fix the POD

=cut

=head2 get_config_interface

TODO: Fix the POD

=cut

=head2 is_active

TODO: Fix the POD

=cut

=head2 stuf_throw

TODO: Fix the POD

=cut

