package Zaaksysteem::Backend::Sysin::Modules::STUFNNP;

use Moose;

use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::ZAPI::Form::Field;

use Zaaksysteem::Constants;
use Zaaksysteem::Tools;

use JSON;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::StUF
    MooseX::Log::Log4perl
/;

###
### Interface Properties
###
### Below a list of interface properties, see
### L<Zaaksysteem::Backend::Sysin::Modules> for details.

use constant INTERFACE_ID               => 'stufnnp';

use constant INTERFACE_CONFIG_FIELDS    => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_stuf_version',
        type        => 'select',
        label       => 'Versie van StUF',
        data        => {
            options     => [
                {
                    value    => '0204',
                    label    => '0204',
                },
                {
                    value    => '0310',
                    label    => '0310',
                }
            ],
        },
        required    => 1,
        description => 'Afhankelijk van de makelaar, kan er gekozen worden voor'
            .' StUF versie 0204 of 0310. Raadpleeg de leverancier van de makelaar'
            .' voor de te gebruiken versie.'
    ),
];

use constant MODULE_SETTINGS            => {
    name                            => INTERFACE_ID,
    label                           => 'StUF Koppeling NNP',
    interface_config                => INTERFACE_CONFIG_FIELDS,
    direction                       => 'incoming',
    manual_type                     => ['text', 'file'],
    is_multiple                     => 0,
    is_manual                       => 1,
    retry_on_error                  => 1,
    allow_multiple_configurations   => 0,
    is_casetype_interface           => 0,
    trigger_definition              => {
        disable_subscription   => {
            method  => 'disable_subscription',
            #update  => 1,
        },
        # search          => {
        #     method  => 'search_nnp',
        #     #update  => 1,
        # },
        # import      => {
        #     method  => 'import_nnp',
        #     #update  => 1,
        # },
    },
    # has_attributes                  => 1,
    # attribute_list                  => [
    #     {
    #         external_name   => 'PRS.a-nummer',
    #         internal_name   => 'a_nummer',
    #         object          => 'natuurlijk_persoon',
    #         attribute_type  => 'defined'
    #     },
    #     {
    #         external_name   => 'PRS.bsn-nummer',
    #         internal_name   => 'burgerservicenummer',
    #         attribute_type  => 'defined'
    #     },
    #     {
    #         external_name   => 'PRS.voornamen',
    #         internal_name   => 'voornamen',
    #         object          => 'natuurlijk_persoon',
    #         attribute_type  => 'defined'
    #     },
    #     {
    #         external_name   => 'PRS.voorletters',
    #         internal_name   => 'voorletters',
    #         object          => 'natuurlijk_persoon',
    #         attribute_type  => 'defined'
    #     },
    #     {
    #         external_name   => 'PRS.voorvoegselGeslachtsnaam',
    #         internal_name   => 'voorvoegsel',
    #         object          => 'natuurlijk_persoon',
    #         attribute_type  => 'defined'
    #     },
    #     {
    #         external_name   => 'PRS.geslachtsnaam',
    #         internal_name   => 'geslachtsnaam',
    #         object          => 'natuurlijk_persoon',
    #         attribute_type  => 'defined'
    #     },
    #     {
    #         external_name   => 'PRS.geboortedatum',
    #         internal_name   => 'geboortedatum',
    #         object          => 'natuurlijk_persoon',
    #         attribute_type  => 'defined'
    #     },
    #     {
    #         external_name   => 'PRS.geslachtsaanduiding',
    #         internal_name   => 'geslachtsaanduiding',
    #         object          => 'natuurlijk_persoon',
    #         attribute_type  => 'defined'
    #     },
    #     {
    #         external_name   => 'PRS.datumOverlijden',
    #         internal_name   => 'datum_overlijden',
    #         object          => 'natuurlijk_persoon',
    #         attribute_type  => 'defined'
    #     },
    #     {
    #         external_name   => 'PRS.indicatieGeheim',
    #         internal_name   => 'indicatie_geheim',
    #         object          => 'natuurlijk_persoon',
    #         attribute_type  => 'defined'
    #     },
    #     {
    #         external_name   => 'PRS.aanduidingNaamgebruik',
    #         internal_name   => 'aanduiding_naamgebruik',
    #         object          => 'natuurlijk_persoon',
    #         attribute_type  => 'defined'
    #     },
    # ]
};

has 'stuf_object_type' => (
    'is'        => 'ro',
    'default'   => 'NNP'
);

has 'stuf_subscription_table' => (
    'is'        => 'ro',
    'default'   => 'Bedrijf'
);

=head2 stuf0204

isa: Zaaksysteem::StUF::0204::Instance

Returns the xml_compile instance object for StUF 0204

=cut

has 'stuf0204'   => (
    'is'        => 'rw',
    'isa'       => 'Zaaksysteem::StUF::0204::Instance',
    'lazy'      => 1,
    'default'   => sub {
        return Zaaksysteem::XML::Compile->xml_compile->add_class('Zaaksysteem::StUF::0204::Instance')->stuf0204;
    }
);

###
### BUILDARGS
###

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig( %{ MODULE_SETTINGS() } );
};


=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::STUFPRS - STUFPRS engine for StUF PRS related queries

=head1 SYNOPSIS

    # See testfile:
    # t/431-sysin-modules-stufprs.t

=head1 DESCRIPTION

STUFPRS engine for StUF PRS related queries

=head1 TRIGGERS

=head2 search($params)

=cut

=head1 PROCESSORS

=head2 CREATE SUBJECT

=head2 $module->stuf_create_entry($transaction_record, $rowobject)

Return value: $ROW_NATUURLIJK_PERSOON

Creates a new L<Zaaksysteem::DB::Component::NatuurlijkPersoon> into our database,
and sets a subscription between our data record and theirs via C<ObjectSubscription>

B<Params>

=over 4

=item $transaction_record

The L<Zaaksysteem::Backend::Sysin::Transaction::Component> row. It will be filled
with object_preview, a preview string describing the data object changed.

=item $rowobject

The source of the data, in case of a CSV this would proba

=back

=cut

define_profile 'stuf_create_entry' => (
    %{ GEGEVENSMAGAZIJN_KVK_PROFILE() }
);

sub stuf_create_entry {
    my $self                = shift;
    my ($record, $object)   = @_;

    my $organization_params = $object->get_params_for_organization;

    my $org_rs = $record->result_source->schema->resultset('Bedrijf');

    ### Validation
    assert_profile($organization_params);

    ### BACKWARDS compatability
    my $entry = $org_rs->create(
        {
            %{$organization_params},
            authenticated   => 1,
            authenticatedby => 'kvk'
        }
    );

    my $mutation_record = Zaaksysteem::Backend::Sysin::Transaction::Mutation->new(
        table    => 'Bedrijf',
        table_id => $entry->id,
        create   => 1
    );

    $mutation_record->from_dbix($entry);

    push(
        @{ $self->process_stash->{row}->{mutations} },
        $mutation_record
    );

    $record->preview_string(
        $entry->handelsnaam
    );

    $self->_add_subscription_for_entry($record, $object, $entry);

    if ($organization_params->{deleted_on}) {
        $self->stuf_delete_entry($record, $object);
    } elsif ($entry->deleted_on) {
        ### Make sure entry comes back to live
        $entry->deleted_on(undef);

        my $subscription = $entry->subscription_id;
        $subscription->date_deleted(undef);

        $entry->update;
        $subscription->update;
    }

    return $entry;
}

=head2 UPDATE SUBJECT

=head2 $module->stuf_update_entry($record, $object)

Updates a PRS entry in our database

=cut

sub stuf_update_entry {
    my $self                = shift;
    my ($record, $object)   = @_;

    my $bedrijf;
    $bedrijf                = $self->get_entry_from_subscription(@_, 'Bedrijf');

    $self->_update_subject_bedrijf(
        @_, $bedrijf
    );

    $record->preview_string(
        $bedrijf->handelsnaam
    );

    return $bedrijf;
}

sub _update_subject_bedrijf {
    my $self                                = shift;
    my ($record, $object, $entry)           = @_;

    my $subject_params      = $object->get_params_for_organization;
    my %old_values          = $entry->get_columns;

    unless ($entry->update($subject_params)->discard_changes) {
        throw(
            'sysin/modules/stufnnp/process/np_update_error',
            'Impossible to update Bedrijf in our database, unknown error'
        );
    }

    my $mutation_record     = Zaaksysteem::Backend::Sysin::Transaction::Mutation
                            ->new(
                                table       => 'Bedrijf',
                                table_id    => $entry->id,
                                update      => 1
                            );

    ### Log mutation
    $mutation_record->from_dbix($entry, \%old_values);

    push(
        @{ $self->process_stash->{row}->{mutations} },
        $mutation_record
    );

    if ($subject_params->{deleted_on}) {
        $self->stuf_delete_entry($record, $object);
    } elsif ($entry->deleted_on) {
        ### Make sure entry comes back to live
        $entry->deleted_on(undef);

        my $subscription    = $entry->subscription_id;
        $subscription->date_deleted(undef);

        $entry->update;
        $subscription->update;
    }

    return $entry;
}


=head2 DELETE SUBJECT

=head2 stuf_delete_entry

=cut


sub stuf_delete_entry {
    my $self                = shift;
    my ($record, $object, $subscription)    = @_;


    my $bedrijf                             = $self->get_entry_from_subscription(
                                                $record,
                                                $object,
                                                'Bedrijf',
                                                $subscription
                                            );

    $record->preview_string(
        $bedrijf->handelsnaam
    );

    $bedrijf->deleted_on(DateTime->now());
    $bedrijf->update;

    $self->_remove_subscription_from_entry($record, $object, $bedrijf);
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 GEGEVENSMAGAZIJN_KVK_PROFILE

TODO: Fix the POD

=cut

=head2 INTERFACE_CONFIG_FIELDS

TODO: Fix the POD

=cut

=head2 INTERFACE_ID

TODO: Fix the POD

=cut

=head2 MODULE_SETTINGS

TODO: Fix the POD

=cut

