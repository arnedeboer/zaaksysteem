package Zaaksysteem::Backend::Sysin::Modules::STUFZKN;
use Moose;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::BaseURI
    Zaaksysteem::Backend::Sysin::Modules::Roles::ClientCertificate
    Zaaksysteem::Backend::Sysin::Modules::Roles::SOAPGeneric
/;

use Encode qw(encode_utf8);
use XML::LibXML;
use Zaaksysteem::Tools;
use Zaaksysteem::StUF::0310::Processor;
use Zaaksysteem::ZAPI::Form::Field;

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::STUFZKN - STUF DMS/STUF ZKN interface

=head1 DESCRIPTION

Interface module that implements the SOAP calls described in the STUF-DMS/STUF-ZKN.

This module implements the L<Zaaksysteem::Backend::Sysin::Modules::Roles::SOAPGeneric>
role, making the STUF-ZKN calls available through a SOAP interface.

=cut

my $INTERFACE_ID = 'stuf_zkn';

my @INTERFACE_CONFIG_FIELDS = (
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_allow_new_cases',
        type        => 'checkbox',
        required    => 0,
        label       => 'Registreren van nieuwe zaken accepteren',
        description => '',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_allow_phase_update',
        type        => 'checkbox',
        required    => 0,
        label       => 'Afronden fase accepteren',
        description => '',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_client_certificate',
        type        => 'file',
        label       => 'Certificaat (public key)',
        description => 'Upload hier het certificaat van de software die de SOAP-interface van deze koppeling zal aanroepen',
        required    => 1,
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_casetypes',
        type        => 'multiple',
        label       => 'Zaaktypes',
        required    => 0,
        data => {
            fields => [
                {
                    name => 'casetype',
                    type => 'spot-enlighter',
                    label => 'Zaaktype',
                    description => 'Let op! Zorg dat het geselecteerde zaaktype een unieke waarde in het veld "identificatie" heeft. Deze identificatiecode wordt door StUF-ZKN gebruikt om het zaaktype te selecteren.',
                    data => {
                        restrict => 'casetype/object',
                        placeholder => 'Type uw zoekterm',
                        label => 'values.name'
                    }
                },
            ],
        },
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_api_url',
        type        => 'display',
        label       => 'Endpoint URL (voor andere systemen)',
        description => '',
        required    => 0,
        data => { template => '<[field.value]>' },
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_test_url',
        type        => 'display',
        label       => 'Simulatietool',
        description => '',
        required    => 0,
        default     => '/stuf-zkn/simulator',
        data => { template => '<a href="<[field.value]>"><[field.value]></a>' },
    ),
);

my %MODULE_SETTINGS = (
    name                          => $INTERFACE_ID,
    label                         => 'StUF-ZKN',
    interface_config              => \@INTERFACE_CONFIG_FIELDS,
    direction                     => 'incoming',
    manual_type                   => ['text'],
    is_multiple                   => 1,
    is_manual                     => 0,
    allow_multiple_configurations => 1,
    is_casetype_interface         => 0,
    has_attributes                => 0,
    attribute_list                => [],
    retry_on_error                => 0,
    trigger_definition            => {},
);

has uri_values => (
    is => 'ro',
    isa => 'HashRef',
    lazy => 1,
    default => sub {
        {
            interface_test_url => '/stuf/simulator',
        }
    },
);

has services_uri_values => (
    is => 'ro',
    isa => 'HashRef',
    lazy => 1,
    default => sub {
        {
            interface_api_url  => '/sysin/interface/ID/soap',
        }
    },
);

=head2 BUILDARGS

Configures this interface module (configuration form fields, etc.).

=cut

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig(%MODULE_SETTINGS);
};

my $STUF_ZKN_NS = 'http://www.egem.nl/StUF/sector/zkn/0310';

my %DISPATCH_TABLE = (
    # Case calls
    "{$STUF_ZKN_NS}genereerZaakIdentificatie_Di02"     => 'generate_case_id',
    "{$STUF_ZKN_NS}zakLv01"                            => 'get_case_details',
    "{$STUF_ZKN_NS}zakLk01"                            => 'write_case',

    # Document calls
    "{$STUF_ZKN_NS}genereerDocumentIdentificatie_Di02" => 'generate_document_id',
    "{$STUF_ZKN_NS}edcLv01"                            => 'get_case_document',
    "{$STUF_ZKN_NS}edcLk01"                            => 'write_case_document',

    # Document calls, "locking"
    "{$STUF_ZKN_NS}geefZaakdocumentbewerken_Di02"      => 'get_case_document_lock',
    "{$STUF_ZKN_NS}updateZaakdocument_Di02"            => 'write_case_document_lock',
    "{$STUF_ZKN_NS}cancelCheckout_Di02"                => 'unlock_case_document',
);

sub _process_row {
    my $self = shift;
    my ($record, $row) = @_;

    my $xml = XML::LibXML->load_xml(string => $record->input);
    my $xc = XML::LibXML::XPathContext->new($xml);
    $xc->registerNs('SOAP',  'http://schemas.xmlsoap.org/soap/envelope/');
    $xc->registerNs('StUF',  'http://www.egem.nl/StUF/StUF0301');
    $xc->registerNs('ZKN',   'http://www.egem.nl/StUF/sector/zkn/0310');
    $xc->registerNs('BG',    'http://www.egem.nl/StUF/sector/bg/0310');
    $xc->registerNs('xlink', 'http://www.w3.org/1999/xlink');

    # Find the SOAP call by extracting the root element's name from the body
    # in {namespace}nodeName format
    my @nodes = $xc->findnodes('//SOAP:Body/*[1]');
    my @node_names = map {
        '{' . $_->namespaceURI . '}' . $_->localname
    } @nodes;

    # We don't care about the SOAP bits, so use the SOAP body's content as the
    # XPath context node.
    $xc->setContextNode($nodes[0]);

    my $processor = Zaaksysteem::StUF::0310::Processor->new(
        record => $record,
        schema => $record->result_source->schema,
        betrokkene_model => $record->result_source->schema->betrokkene_model,
        object_model => Zaaksysteem::Object::Model->new(
            schema => $record->result_source->schema,
        ),
    );

    my $response_xml;
    my $error = 0;
    try {
        if (exists $DISPATCH_TABLE{$node_names[0]}) {
            if (my $call = $processor->can($DISPATCH_TABLE{$node_names[0]})) {
                $response_xml = $call->(
                    $processor,
                    $xc,
                );
            }
            else {
                throw(
                    'stuf-zkn/unsupported_call',
                    "SOAP call '$node_names[0]' is not implemented",
                );
            }
        }
        else {
            throw(
                'stuf-zkn/unsupported_call',
                "SOAP call '$node_names[0]' is not supported",
            );
        }
    } catch {
        my %stuurgegevens = $processor->parse_stuurgegevens($xc, $xc->findnodes('ZKN:stuurgegevens'));
        $response_xml = $processor->generate_fo03($record->id, \%stuurgegevens, $_);
        $error = 1;
    };

    my $msg = $error ? "Fout opgetreden bij verwerken van verzoek $node_names[0]" : "Verzoek $node_names[0] afgehandeld";
    $response_xml = encode_utf8($response_xml);

    $self->set_record_output($record, $response_xml, $msg);

    die "$response_xml" if $error;

    return;
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
