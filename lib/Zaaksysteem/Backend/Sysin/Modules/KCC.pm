package Zaaksysteem::Backend::Sysin::Modules::KCC;

use Moose;

use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::ZAPI::Form::Field;

use JSON;
use Regexp::Common qw(net);

use Zaaksysteem::Exception;
use Zaaksysteem::Profile;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
/;

use constant INTERFACE_ID => 'kcc';

use constant INTERFACE_CONFIG_FIELDS => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_pbx_ip',
        type        => 'text',
        label       => 'IP-adres',
        required    => 1,
        description => 'IP-adres van de PBX (telefooncentrale)',
        # Note: this is a Javascript (client-side) check.
        data        => { pattern => '^([0-9]{1,3}\.){3}[0-9]{1,3}$' },
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_call_timeout',
        type        => 'text',
        label       => 'Timeout',
        required    => 0,
        default     => 300,
        description => 'Tijd (in seconden) dat een telefoon-melding actief blijft.',
        # Note: this is a Javascript (client-side) check.
        data        => { pattern => '^[0-9]+$' },
    ),
];

use constant MODULE_SETTINGS => {
    name                          => INTERFACE_ID,
    label                         => 'PBX voor KCC',
    interface_config              => INTERFACE_CONFIG_FIELDS,
    direction                     => 'incoming',
    manual_type                   => ['text'],
    is_manual                     => 0,
    is_multiple                   => 0,
    retry_on_error                => 0,
    allow_multiple_configurations => 0,
    is_casetype_interface         => 0,
    test_interface                => 1,
    test_definition               => {
        description => qq{
            Hiermee kunt u de KCC-functionaliteit proberen zonder de PBX aan te
            passen.
        },
        tests => [
            {
                id          => 1,
                label       => "Testgesprek",
                name        => "kcc_test_call",
                method      => "kcc_test_call",
                description => qq{
                    Deze test simuleert een telefoongesprek, zodat u kunt zien
                    hoe de KCC-meldingen eruit zien. Zorg dat er een
                    contactpersoon is met telefoonnummer "0123456789", dat uw
                    extension op "999" staat ingesteld en dat de telefoondienst
                    aan staat.
                },
            }
        ],
    },
    trigger_definition            => {
        incoming_call     => { method => 'incoming_call' },
        list_active_calls => { method => 'list_active_calls' },
        mark_call         => { method => 'mark_call' },
    },
};

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig( %{ MODULE_SETTINGS() } );
};

=head2 incoming_call

Create a transaction for an incoming call.

=cut

define_profile incoming_call => (
    required => [qw(client_ip phonenumber extension)],
    constraint_methods => {
        client_ip   => qr/^$RE{net}{IPv4}$/,
        phonenumber => qr/^[0-9]{10}$/,
    },
);

sub incoming_call {
    my $self      = shift;
    my $params    = assert_profile(shift||{})->valid;
    my $interface = shift;

    if($params->{client_ip} ne $interface->get_interface_config->{pbx_ip}) {
        throw(
            "zaaksysteem/access_denied",
            "Your IP ($params->{client_ip}) does not match that of the configured PBX."
        );
    }

    my $transaction = $interface->process(
        {
            processor_params => {
                processor => '_register_incoming_call',

                client_ip   => $params->{client_ip},
                phonenumber => $params->{phonenumber},
                extension   => $params->{extension},
            },
            input_data => "Gesprek van $params->{phonenumber} naar $params->{extension}",
        }
    );

    return $transaction;
}

sub _register_incoming_call {
    my $self = shift;
    my ($record, $params) = @_;

    $record->output("Undecided");
}

=head2 mark_call

Mark an incoming call as "accepted" or "rejected".

=head3 Arguments

=head3 Return

The transaction.

=cut

define_profile mark_call => (
    required => [qw(transaction_id mark)],
    constraint_methods => {
        mark => qr/^(accepted|rejected)$/,
    },
);

sub mark_call {
    my $self      = shift;
    my $params    = assert_profile(shift||{})->valid;
    my $interface = shift;

    my $transaction = $interface->transactions->search(
        { id => $params->{transaction_id} }
    )->single;
    if (!$transaction) {
        throw(
            "transaction/not_found",
            "Transaction not found in database.",
        );
    }

    my $processor_params = $transaction->processor_params();
    my $record           = $transaction->transaction_records->single;

    $record->update({ output => $params->{mark} });
    $transaction->processor_params(
        {
            %$processor_params, result => $params->{mark}
        }
    );
    $transaction->transaction_delete();

    return $transaction->apply_roles();
}

=head2 list_active_calls

Return the list of currently active calls for an extension.

=head2 Arguments

=over

=item * extension

Only return active calls for this extension.

=back

=head3 Returns

A list of L<Zaaksysteem::Schema::Transaction> instances, one for each active call.

=cut

define_profile list_active_calls => (
    required => [qw(extension)],
);

sub list_active_calls {
    my $self      = shift;
    my $params    = assert_profile(shift||{})->valid;
    my $interface = shift;

    my $cutoff = DateTime->now()->subtract(
        seconds => $interface->get_interface_config->{call_timeout},
    );

    my $call_transactions = $interface->transactions->search_active(
        {
            date_created => { ">=" => $cutoff },
        }
    );

    my @matching_calls;
    for my $call_transaction ($call_transactions->all) {
        my $processor_params = $call_transaction->processor_params();

        if($processor_params->{extension} eq $params->{extension}) {
            $call_transaction->apply_roles();
            push @matching_calls, $call_transaction;
        }
    }

    return \@matching_calls;
}

=head2 kcc_test_call

Create a test transaction ("fake a call").

=cut

sub kcc_test_call {
    my $self = shift;
    my $interface = shift;

    $self->incoming_call(
        {
            client_ip   => $interface->get_interface_config->{pbx_ip},
            phonenumber => '0123456789',
            extension   => '999',
        },
        $interface,
    );

    return 1;
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 INTERFACE_CONFIG_FIELDS

TODO: Fix the POD

=cut

=head2 INTERFACE_ID

TODO: Fix the POD

=cut

=head2 MODULE_SETTINGS

TODO: Fix the POD

=cut

