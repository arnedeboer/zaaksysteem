package Zaaksysteem::Backend::Sysin::Modules::GWS4ALL;
use Moose;

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::GWS4ALL - Interface module for GWS4all SOAP

=cut

use Data::UUID;
use String::CamelCase qw(decamelize);
use XML::Compile::WSDL11;
use XML::Compile::SOAP::WSA;
use XML::Compile::SOAP11;
use XML::Compile::Transport::SOAPHTTP;
use Zaaksysteem::Tools;
use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::ZAPI::Form::Field;

use Zaaksysteem::Constants qw/RGBZ_GEMEENTECODES/;


extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
    Zaaksysteem::Backend::Sysin::Modules::Roles::Tests
/;

use constant INTERFACE_ID => 'gws4all';

use constant INTERFACE_CONFIG_FIELDS => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_jaaropgave_sjabloon',
        type => 'spot-enlighter',
        label => 'Sjabloon voor jaaropgave',
        required => 1,
        description => 'Selecteer hier het sjabloon dat gebruikt wordt voor jaaropgaves.',
        data => {
            restrict => 'template',
            placeholder => 'Type uw zoekterm',
            label => 'naam',
        },
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_specificatie_sjabloon',
        type => 'spot-enlighter',
        label => 'Sjabloon voor uitkeringsspecificatie',
        required => 1,
        description => 'Selecteer hier het sjabloon dat gebruikt wordt voor uitkeringsspecificaties.',
        data => {
            restrict => 'template',
            placeholder => 'Type uw zoekterm',
            label => 'naam',
        },
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name    => 'interface_soap_timeout',
        type    => 'text',
        label   => 'Time-out voor GWS4all-aanroepen (in seconden)',
        default => 10,
        description => 'Beschouw een aanroep naar GWS4all als "niet geslaagd" na de ingestelde tijd.',
        data => { pattern => '/^[0-9]+$/' },
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_gemeentecode',
        type        => 'select',
        label       => 'Gemeente code',
        required    => 1,
        data        => {
            options     => [
                sort({ $a->{label} cmp $b->{label} } map({ { value => $_, label => RGBZ_GEMEENTECODES()->{$_} } } keys %{ RGBZ_GEMEENTECODES() })),
            ],
        },
        description => 'Selecteer de gemeente waarop deze koppeling van toepassing is',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_endpoint_jaaropgave',
        type => 'text',
        label => 'URL van de Jaaropgave-service',
        required => 1,
        description => 'Let op, dit moet een beveiligde (https) URL zijn',
        data => { pattern => '/^https:/' },
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_endpoint_specificatie',
        type => 'text',
        label => 'URL van de Uitkeringsspecificatie-service',
        required => 1,
        description => 'Let op, dit moet een beveiligde (https) URL zijn',
        data => { pattern => '/^https:/' },
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_spoofmode',
        type => 'checkbox',
        label => 'Spoof-modus',
        description => qq{
            Met deze "spoof"-modus worden geen berichten met GWS4all uitgewisseld,
            maar worden gesimuleerde antwoorden gebruikt, zodat de functionaliteit
            getest kan worden zonder afhankelijkheid van burger-gegevens of een werkende
            GWS4all-omgeving.<br>
            <b>LET OP</b>: Niet aanzetten op een productie-omgeving. Deze optie is alleen voor
            test-doeleinden en levert geen juiste/echte gegevens.
        },
    ),
];

use constant MODULE_SETTINGS => {
    name => INTERFACE_ID,
    label => 'GWS4all Uitkeringsspecificaties',
    interface_config => INTERFACE_CONFIG_FIELDS,
    direction => 'outgoing',
    is_multiple => 0,
    is_manual => 0,
    retry_on_error => 0,
    allow_multiple_configurations => 0,
    is_casetype_interface => 0,
    test_interface => 1,
    trigger_definition  => {
        get_jaaropgave => {
            method => 'get_jaaropgave',
        },
        get_specificatie => {
            method => 'get_specificatie',
        },
    },
    test_definition => {
        description => qq{
            Om te controleren of de applicatie goed geconfigureerd is, kunt u
            hieronder een aantal tests uitvoeren. Hiermee controleert u uw verbinding
            met de GWS4all-applicatie.
        },
        tests => [
            {
                id          => 1,
                label       => 'Test verbinding',
                name        => 'test_connection',
                method      => 'test_connection',
                description => 'Controleert of de beveiligde verbinding met GWS4all opgezet kan worden.'
            },
        ],
    },
};

use constant SPOOF_DATA => {
    jaaropgave => {
        '987654321' => {
            'Client' => {
                'Voornamen'             => 'Spoof',
                'AanduidingNaamgebruik' => '1',
                'Voorletters'           => 'S',
                'Achternaam'            => 'Nepheid',
                'Voorvoegsel'           => 'van',
                'BurgerServiceNr'       => '987654321',
                'Adres'     => {
                    'Straatnaam'     => 'Jaaropgavestraat',
                    'Gemeentenaam'   => 'Tholen',
                    'Woonplaatsnaam' => 'Testwoonplaats',
                    'Postcode'       => '1234XX',
                    'Huisnummer'     => '2'
                },
            },
            'JaarOpgave' => [
                {
                    'Inhoudingsplichtige' => {
                        'Gemeentecode'   => '0716',
                        'Bezoekadres'    => 'Hof van Tholen 2',
                        'Gemeentenaam'   => 'Tholen',
                        'Woonplaatsnaam' => 'Tholen',
                        'Postcode'       => '4691DZ'
                    },
                    'SpecificatieJaarOpgave' => [
                        {
                            'Loonheffingskorting' => [
                                {
                                    'CdLoonheffingskorting' => '1',
                                    'Ingangsdatum'          => '20130101'
                                }
                            ],
                            'CdPremieVolksverzekering' => '250',
                            'OntvangstenPremieloon'    => {
                                'WaardeBedrag'       => '0',
                                'CdPositiefNegatief' => '+'
                            },
                            'VergoedingPremieZVW' => {
                                'WaardeBedrag'       => '0',
                                'CdPositiefNegatief' => '+'
                            },
                            'OntvangstenVergoedingPremieZVW' => {
                                'WaardeBedrag'       => '0',
                                'CdPositiefNegatief' => '+'
                            },
                            'Dienstjaar'                 => 2013,
                            'WerkgeversheffingPremieZVW' => {
                                'WaardeBedrag'       => '1396',
                                'CdPositiefNegatief' => '+'
                            },
                            'AangiftePeriodeTot'  => '20131231',
                            'AangiftePeriodeVan'  => '20130101',
                            'IngehoudenPremieZVW' => {
                                'WaardeBedrag'       => '0',
                                'CdPositiefNegatief' => '+'
                            },
                            'Fiscaalloon' => {
                                'WaardeBedrag'       => '18002',
                                'CdPositiefNegatief' => '+'
                            },
                            'Loonheffing' => {
                                'WaardeBedrag'       => '4661',
                                'CdPositiefNegatief' => '+'
                            },
                            'Regeling'               => 'WET WERK EN BIJSTAND',
                            'IndicatieZVW'           => '1',
                            'OntvangstenFiscaalloon' => {
                                'WaardeBedrag'       => '0',
                                'CdPositiefNegatief' => '+'
                            },
                            'OntvangstenWerkgeversheffingPremieZVW' => {
                                'WaardeBedrag'       => '0',
                                'CdPositiefNegatief' => '+'
                            },
                            'OntvangstenIngehoudenPremieZVW' => {
                                'WaardeBedrag'       => '0',
                                'CdPositiefNegatief' => '+'
                            },
                        },
                    ],
                },
            ],
        },
    },
    specificatie => {
        '987654321' => {
            'Uitkeringsspecificatie' => [
                {
                    'Uitkeringsinstantie' => {
                        'Gemeentecode'   => '0716',
                        'Bezoekadres'    => 'Hof van Tholen 2',
                        'Gemeentenaam'   => 'Tholen',
                        'Woonplaatsnaam' => 'Tholen',
                        'Postcode'       => '4691DZ'
                    },
                    'Dossierhistorie' => [
                        {
                            'Betrekkingsperiode'        => '201410',
                            'Regeling'                  => 'IOAW/IOAZ',
                            'VakantiegeldOverInkomsten' => {
                                'WaardeBedrag'       => '0',
                                'CdPositiefNegatief' => '+'
                            },
                            'Iban'              => 'NL08INGB0000000555',
                            'Componenthistorie' => [
                                {
                                    'IndicatieKolom' => '3',
                                    'Bedrag'         => {
                                        'WaardeBedrag'       => '3100',
                                        'CdPositiefNegatief' => '+'
                                    },
                                    'Omschrijving' => 'Aantal dagen'
                                },
                                {
                                    'IndicatieKolom' => '1',
                                    'Bedrag'         => {
                                        'WaardeBedrag'       => '157990',
                                        'CdPositiefNegatief' => '+'
                                    },
                                    'Omschrijving' => 'Grondslag Volledig gezin'
                                },
                                {
                                    'IndicatieKolom' => '3',
                                    'Bedrag'         => {
                                        'WaardeBedrag'       => '0',
                                        'CdPositiefNegatief' => '+'
                                    },
                                    'Omschrijving' =>
                                        'Arbeid uit dienstbet. incl. VT'
                                },
                                {
                                    'IndicatieKolom' => '3',
                                    'Bedrag'         => {
                                        'WaardeBedrag'       => '28000',
                                        'CdPositiefNegatief' => '+'
                                    },
                                    'Omschrijving' =>
                                        'Arbeid uit dienstbet. incl. VT'
                                },
                                {
                                    'IndicatieKolom' => '2',
                                    'Bedrag'         => {
                                        'WaardeBedrag'       => '28000',
                                        'CdPositiefNegatief' => '+'
                                    },
                                    'Omschrijving' => 'TOTAAL TE KORTEN INKOMSTEN'
                                },
                                {
                                    'IndicatieKolom' => '1',
                                    'Bedrag'         => {
                                        'WaardeBedrag'       => '129990',
                                        'CdPositiefNegatief' => '+'
                                    },
                                    'Omschrijving' => 'BRUTO INCLUSIEF VT'
                                },
                                {
                                    'IndicatieKolom' => '2',
                                    'Bedrag'         => {
                                        'WaardeBedrag'       => '9629',
                                        'CdPositiefNegatief' => '+'
                                    },
                                    'Omschrijving' => 'RESERVERING VAKANTIEGELD'
                                },
                                {
                                    'IndicatieKolom' => '1',
                                    'Bedrag'         => {
                                        'WaardeBedrag'       => '120361',
                                        'CdPositiefNegatief' => '+'
                                    },
                                    'Omschrijving' => 'BRUTO UITKERING'
                                },
                                {
                                    'IndicatieKolom' => '2',
                                    'Bedrag'         => {
                                        'WaardeBedrag'       => '4166',
                                        'CdPositiefNegatief' => '+'
                                    },
                                    'Omschrijving' => 'LOONHEFFING PARTNER'
                                },
                                {
                                    'IndicatieKolom' => '2',
                                    'Bedrag'         => {
                                        'WaardeBedrag'       => '4166',
                                        'CdPositiefNegatief' => '+'
                                    },
                                    'Omschrijving' => 'LOONHEFFING CLIENT'
                                },
                                {
                                    'IndicatieKolom' => '1',
                                    'Bedrag'         => {
                                        'WaardeBedrag'       => '112029',
                                        'CdPositiefNegatief' => '+'
                                    },
                                    'Omschrijving' => 'NETTO UITKERING'
                                },
                                {
                                    'IndicatieKolom' => '1',
                                    'Bedrag'         => {
                                        'WaardeBedrag'       => '112029',
                                        'CdPositiefNegatief' => '+'
                                    },
                                    'Omschrijving' => 'UIT TE BETALEN BEDRAG'
                                }
                            ],
                            'TeVerrekenenInkomsten' => {
                                'WaardeBedrag'       => '28000',
                                'CdPositiefNegatief' => '+'
                            },
                            'UitbetaaldBedrag' => {
                                'WaardeBedrag'       => '112029',
                                'CdPositiefNegatief' => '+'
                            },
                            'Periodenummer'          => '201410',
                            'OverigeBijstandspartij' => [
                                {
                                    'Voornamen'             => 'Spoof Test',
                                    'AanduidingNaamgebruik' => '3',
                                    'Voorletters'           => 'ST',
                                    'Achternaam'            => 'TestAchternaam',
                                    'Voorvoegsel'           => 'van',
                                    'AchternaamEchtgenoot'  => 'TestAchternaamEchtgenoot',
                                    'BurgerServiceNr'       => '987654321'
                                }
                            ],
                            'GekorteInkomsten' => {
                                'WaardeBedrag'       => '28000',
                                'CdPositiefNegatief' => '+'
                            },
                            'InkomstenVrijlating' => {
                                'WaardeBedrag'       => '0',
                                'CdPositiefNegatief' => '+'
                            },
                            'InkomstenNaVrijlating' => {
                                'WaardeBedrag'       => '28000',
                                'CdPositiefNegatief' => '+'
                            },
                            'OpgegevenInkomsten' => {
                                'WaardeBedrag'       => '28000',
                                'CdPositiefNegatief' => '+'
                            },
                            'Dossiernummer' => '1337',
                            'Bic'           => 'RABONL2U'
                        }
                    ]
                }
            ],
            'Client' => {
                'Voornamen' => 'Jan',
                'Adres'     => {
                    'Straatnaam'     => 'Specificatieplein',
                    'Gemeentenaam'   => 'Tholen',
                    'Woonplaatsnaam' => 'Testwoonplaats',
                    'Postcode'       => '1234XX',
                    'Huisnummer'     => '2'
                },
                'AanduidingNaamgebruik' => '1',
                'Voorletters'           => 'J',
                'Achternaam'            => 'Tester',
                'Voorvoegsel'           => 'van den',
                'BurgerServiceNr'       => '987654321'
            }
        },
    },
};

around BUILDARGS => sub {
    my $orig = shift;
    my $class = shift;

    $class->$orig( %{ MODULE_SETTINGS() } );
};

=head1 ATTRIBUTES

A place to store the SOAP response data (that's generated deep in process-code,
but is needed at the first level).

=cut

has response => (is => 'rw', default => sub { undef });

=head1 METHODS

=head2 _compile_call

Internal method that compiles a specified SOAP call from its WSDL.

=cut

sub _compile_call {
    my ($self, %params) = @_;

    my $home = $params{interface}->result_source->schema->default_resultset_attributes->{config}->{home}
        or throw("internal/home", "Need home configuration");

    my $wsa = XML::Compile::SOAP::WSA->new(version => '1.0');
    my $wsdl = XML::Compile::WSDL11->new($home . "/share/wsdl/gws4all/$params{wsdl}");
    $wsdl->importDefinitions($_) for glob("$home/share/wsdl/gws4all/*.xsd");

    my $transport = XML::Compile::Transport::SOAPHTTP->new(
        timeout => $params{timeout} || 10,
        address => $params{endpoint},
    );

    return $wsdl->compileClient(
        operation => $params{action},
        transport => $transport->compileClient()
    );
}


=head2 compile_jaaropgave_call

Compile the "SendJaarOpgaveClient" SOAP call from its WSDL. Returns a code
reference that can be used to do the actual call.

=cut

sub compile_jaaropgave_call {
    my ($self, $interface) = @_;

    return $self->_compile_call(
        interface => $interface,
        action    => 'SendJaarOpgaveClient',
        wsdl      => 'JaarOpgaveClient-v0300-b01.wsdl',
        timeout   => $interface->jpath('$.soap_timeout'),
        endpoint  => $interface->jpath('$.endpoint_jaaropgave'),
    );
}

=head2 compile_specificatie_call

Compile the "SendUitkeringsSpecificatieClient" SOAP call from its WSDL. Returns
a code reference that can be used to do the actual call.

=cut

sub compile_specificatie_call {
    my ($self, $interface) = @_;

    return $self->_compile_call(
        interface => $interface,
        action    => 'SendUitkeringsSpecificatieClient',
        wsdl      => 'UitkeringsSpecificatieClient-v0300-b01.wsdl',
        timeout   => $interface->jpath('$.soap_timeout'),
        endpoint  => $interface->jpath('$.endpoint_specificatie'),
    );
}


=head2 get_jaaropgave

Retrieve a "Jaaropgave" for the specified year + BSN from the JaarOpgaveClient
service at GWS4all.

=cut

sub get_jaaropgave {
    my $self = shift;
    my ($params, $interface) = @_;

    if ($interface->jpath('$.spoofmode')) {
        $self->response( SPOOF_DATA->{jaaropgave}{$params->{bsn}} );
    }
    else {
        $interface->process({
            input_data => 'xml',
            processor_params => {
                processor => '_process_jaaropgave',
                year => $params->{year},
                bsn  => $params->{bsn},
            },
        });
    }

    return $self->response;
}

sub _process_jaaropgave {
    my $self   = shift;
    my $record = shift;
    my $transaction = $self->process_stash->{transaction};
    my $interface   = $transaction->interface;
    my $params      = $transaction->get_processor_params();

    my $soap_params = {
        $self->_gws_header($interface),
        Body => {
            Body => {
                Request => {
                    BurgerServiceNr => $params->{bsn},
                    Dienstjaar      => $params->{year},
                },
            }
        }
    };

    my $call = $self->compile_jaaropgave_call($interface);
    my ($answer, $trace) = $call->($soap_params);

    $transaction->input_data($record->input($trace->request->content));
    $record->output($trace->response->content);

    if (!$answer) {
        return;
    }

    # Yup. "Reponse". It's in the schemas and everything.
    my $result = $answer->{Reponse}{JaarOpgaveClient};
    $self->response($result);
}

=head2 get_specificatie

Retrieve an "uitkeringsspecificatie" from GWS4all for the specified BSN + period.

The period is a combination of year and month, in the format 'YYYYMM'.

=cut

sub get_specificatie {
    my $self = shift;
    my ($params, $interface) = @_;

    if ($interface->jpath('$.spoofmode')) {
        $self->response(SPOOF_DATA->{specificatie}{ $params->{bsn} });
    }
    else {
        $interface->process({
            input_data => 'xml',
            processor_params => {
                processor => '_process_specificatie',
                period => $params->{period},
                bsn  => $params->{bsn},
            },
        });
    }

    return $self->response;
}

sub _process_specificatie {
    my $self   = shift;
    my $record = shift;
    my $transaction = $self->process_stash->{transaction};
    my $interface   = $transaction->interface;
    my $params      = $transaction->get_processor_params();

    my $soap_params = {
        $self->_gws_header($interface),
        Body => {
            Body => {
                Request => {
                    BurgerServiceNr => $params->{bsn},
                    Periodenummer   => $params->{period},
                },
            }
        }
    };

    my $call = $self->compile_specificatie_call($interface);
    my ($answer, $trace) = $call->($soap_params);

    $transaction->input_data($record->input($trace->request->content));
    $record->output($trace->response->content);

    if ($answer) {
        my $result = $answer->{Response}{UitkeringsSpecificatieClient};
        $self->response($result);
    }
}

sub _gws_header {
    my $self = shift;
    my ($interface) = @_;

    return (
        wsa_MessageID => Data::UUID->new->create_b64,
        wsa_To => 'adres onbekend',    # If the demo XML says this..

        headerPart => {
            BerichtIdentificatie => {
                DatTijdAanmaakRequest => DateTime->now->iso8601 . 'Z',
                ApplicatieInformatie  => 'Zaaksysteem',
            },
            RouteInformatie => {
                Bron => {
                    Bedrijfsnaam   => "Mintlab",
                    ApplicatieNaam => "Zaaksysteem",
                },
                Bestemming =>
                    { Gemeentecode => $interface->jpath('$.gemeentecode'), },
            },
        },
    );
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
