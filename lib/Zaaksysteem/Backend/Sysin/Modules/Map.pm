package Zaaksysteem::Backend::Sysin::Modules::Map;
use Moose;
use namespace::autoclean;

use Zaaksysteem::Tools;

use URI;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
    Zaaksysteem::Backend::Sysin::Modules::Roles::Tests
    Zaaksysteem::DynClass
/;

use Zaaksysteem::Types qw/JSONBoolean/;

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::Map - Map settings and WMS layer module

=head1 TESTS

    ./zs_prove -v t/lib/TestFor/General/Backend/Sysin/Modules/Map.pm

=head1 DESCRIPTION

In Zaaksysteem.nl it is possible to have MAP functionality. This functionality enables users to find addresses,
place coordinates on a map and add extra WMS services to the map.

=cut

use constant INTERFACE_ID => 'map';

use constant INTERFACE_CONFIG_FIELDS => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_wms_layers',
        type        => 'multiple',
        label       => 'WMS Kaartlaag services',
        description => "De Publieke Dienstverlening op de Kaart (PDOK) staat het gebruik van WMS lagen toe om extra functionaliteit aan uw kaart toe te voegen, zoals gemeentegrenzen, bag locaties, etc. Geef de naam van de laag, een label voor de laag, en de url naar de WMS service. Overzicht: https://www.pdok.nl/nl/producten/pdok-services/overzicht-urls/a",
        data        => {
            fields      => [
                {
                    name    => 'label',
                    label   => 'Titel voor de kaart',
                    type    => 'text',
                    data    => {
                        placeholder => 'Mijn Kaartlaag'
                    }
                },
                {
                    name    => 'id',
                    label   => 'Kaartlaag-naam',
                    type    => 'text',
                    description => "Naam van de kaartlaag die gebruikt moet worden, zoals beschreven in de &gt;label&lt;&gt;name&lt; tag in de GetCapabilities XML.",
                },
                {
                    name    => 'url',
                    label   => 'WMS basis-URL',
                    type    => 'text',
                    description => "URL van de WMS-service. Let op! Zonder het 'GetCapabilities'-deel",
                    data => {
                        pattern => 'https:\/\/.+',
                        placeholder => 'https://map.datapunt.amsterdam.nl/maps/parkeervakken?VERSION=1.1.0&SERVICE=wms'
                    }
                },
                {
                    name    => 'feature_info_xpath',
                    type    => 'text',
                    label   => 'Locatieattribuut XPath',
                    description => 'XPath (1.0) expressie waarmee voor exacte locaties extra informatie opgehaald kan worden uit de kaartlaag.',
                    data => {
                        placeholder => q"/table[@class='featureInfo']/tr[2]/td[2]/text()",
                    }
                },
                {
                    name    => 'active',
                    label   => 'Actief',
                    type    => 'checkbox',
                }
            ]
        }
    ),
];

use constant MODULE_SETTINGS => {
    name                          => INTERFACE_ID,
    label                         => 'Kaartconfiguratie',
    interface_config              => INTERFACE_CONFIG_FIELDS,
    direction                     => 'incoming',
    manual_type                   => ['text'],
    is_multiple                   => 0,
    is_manual                     => 0,
    retry_on_error                => 0,
    allow_multiple_configurations => 0,
    is_casetype_interface         => 0,
    has_attributes                => 0,
    attribute_list => [
    ],
    trigger_definition => {
        get_ol_settings => {
            method  => 'get_ol_settings',
            api     => 1
        },
    },
    test_interface  => 1,
    test_definition => {
        description => qq{
            Om te controleren of de applicatie goed geconfigureerd is, kunt u
            hieronder een aantal tests uitvoeren. Hiermee controleert u de verbinding
            van uw profiel.
        },
        tests => [
            {
                id          => 1,
                label       => 'Test verbinding',
                name        => 'connection_test',
                method      => 'maps_test_connection',
                description => 'Test verbinding naar kaart-URLs'
            }
        ],
    },
};

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig( %{ MODULE_SETTINGS() } );
};

sub BUILD {
    my $self = shift;

    $self->_create_dynamic_classes;
}

has '+_dynamic_classes'    => (
    lazy => 1,
    default     => sub {
        my $self        = shift;

        return [
            {
                class       => __PACKAGE__ . '::OL::LayerWMS',
                attributes  => {
                    id          => {
                        is          => 'ro',
                        isa         => 'Str',
                        required    => 1,
                    },
                    label       => {
                        is          => 'ro',
                        isa         => 'Str',
                        required    => 1,
                    },
                    uri         => {
                        is          => 'ro',
                        isa         => 'URI::https',
                        required    => 1,
                    },
                    url         => {
                        is          => 'ro',
                        lazy        => 1,
                        default     => sub {
                            shift->uri->as_string;
                        }
                    },
                    active      => {
                        is          => 'ro',
                        isa         => JSONBoolean,
                        required    => 1,
                        coerce      => 1,
                    },
                    feature_info_xpath => {
                        is => 'ro',
                        isa => 'Str',
                    }
                },
                moose       => 1,
            },
            {
                class       => __PACKAGE__ . '::OL',
                attributes  => {
                    wms_layers  => {
                        is          => 'rw',
                        isa         => 'ArrayRef[' . __PACKAGE__ . '::OL::LayerWMS]',
                        lazy        => 1,
                        default     => sub { []; },
                        required    => 1,
                    },
                    map_center  => {
                        is          => 'rw',
                        isa         => 'Str',
                    },                   
                },
                moose       => 1,
            }
        ]
    }
);

=head1 METHODS

=head2 get_ol_settings

=over 4

=item Arguments: \%params [, $INTEFACE_ROW ]

=item Return Value: L<Zaaksysteem::Backend::Sysin::Modules::Map::OL>

=back

    $obj->get_ol_settings(undef, $interface)

    # Returns:
    bless( {
      'map_center' => '51.278,5.563',
      'wms_layers' => [
        bless( {
          'active' => bless( do{\(my $o = 1)}, 'JSON::XS::Boolean' ),
          'id' => 'hoogtenl',
          'label' => 'Hoogtebestand Nederland',
          'uri' => bless( do{\(my $o = 'https://geodata.nationaalgeoregister.nl/ahn1/wms')}, 'URI::https' )
        }, 'Zaaksysteem::Backend::Sysin::Modules::Map::OL::LayerWMS' ),
        bless( {
          'active' => bless( do{\(my $o = 0)}, 'JSON::XS::Boolean' ),
          'id' => 'adrnl',
          'label' => 'Adressen Nederland',
          'uri' => bless( do{\(my $o = 'https://geodata.nationaalgeoregister.nl/inspireadressen/wms')}, 'URI::https' )
        }, 'Zaaksysteem::Backend::Sysin::Modules::Map::OL::LayerWMS' )
      ]
    }, 'Zaaksysteem::Backend::Sysin::Modules::Map::OL' )

Returns an instance of L<Zaaksysteem::Backend::Sysin::Modules::Map::OL> for API v1

=cut

sub get_ol_settings {
    my ($self, $params, $interface) = @_;
    my %set_params;

    my $settings = Zaaksysteem::Backend::Sysin::Modules::Map::OL->new();
    
    if (my $wms_layers = $interface->jpath('$.wms_layers')) {
        my @list_of_wms_layers;
        for my $layer (@$wms_layers) {
            my $uri = URI->new($layer->{url});
            try {
                my %wms_layer_args = (uri => $uri);

                $wms_layer_args{ $_ } = $layer->{ $_ } for qw[
                    label active id
                ];

                if (defined $layer->{ feature_info_xpath }) {
                    $wms_layer_args{ feature_info_xpath } = $layer->{ feature_info_xpath };
                }

                push @list_of_wms_layers, Zaaksysteem::Backend::Sysin::Modules::Map::OL::LayerWMS->new(
                    %wms_layer_args
                );
            } catch {
                $self->log->error('sysin/modules/map: Could not instantiate a OL::LayerWMS: invalid config: ' . $_);
            };
        }

        $settings->wms_layers(\@list_of_wms_layers);
    }

    if (
        (my $lat  = $interface->result_source->schema->resultset('Config')->get('customer_info_latitude')) &&
        (my $lng  = $interface->result_source->schema->resultset('Config')->get('customer_info_longitude'))
    ) {
        $settings->map_center("$lat,$lng");
    }

    return $settings;
}

=head2 maps_test_connection

Runs a connection test on every configured map URL.

=cut

sub maps_test_connection {
    my ($self, $interface) = @_;

    for my $layer (@{ $interface->jpath('$.wms_layers') }) {
        # Not specifying a CA certificate means "use system CA store"
        $self->test_host_port_ssl($layer->{url});
    }

    return;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
