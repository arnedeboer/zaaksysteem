package Zaaksysteem::Backend::Sysin::Modules::Ogone;
use Moose;

use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::ZAPI::Form::Field;

extends 'Zaaksysteem::Backend::Sysin::Modules';

use constant INTERFACE_CONFIG_FIELDS => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_ogone_id',
        type        => 'text',
        label       => 'Ogone ID',
        description => '<p>Vul de username (PSPID) van de Ogone-gebruiker in</p>',
        default     => '',
        required    => 1,
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_shapass_in',
        type        => 'text',
        label       => 'SHA-IN versleuteling',
        description => '<p>Dit is te vinden op het tabblad &quot;Verificatie Data en Herkomst&quot; bij Ogone</p>',
        default     => '',
        required    => 1,
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_shapass_out',
        type        => 'text',
        label       => 'SHA-1-OUT Versleuteling',
        description => '<p>Dit te vinden op het tabblad &quot;Transactiefeedback&quot; bij Ogone</p>',
        default     => '',
        required    => 1,
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_hash_algorithm',
        type        => 'select',
        label       => 'Hash-algoritme',
        default     => 'sha512',
        required    => 1,
        description => '<p>Dit te vinden op het tabblad &quot;Algemene beveiligingsparameters&quot; bij Ogone</p>',
        data => {
            options => [
                {
                    label => 'sha512',
                    value => 'sha512',
                },
                {
                    label => 'sha1',
                    value => 'sha1',
                },
            ],
        },
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_return_url',
        type        => 'text',
        label       => 'Terugkeer-URL',
        description => '<p>Vul de URL in waarnaar teruggekeerd moet worden als er een fout optreedt bij Ogone.</p>',
        default     => '',
        required    => 1,
        data => {
            placeholder => 'http://',
        },
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_mode',
        type        => 'select',
        label       => 'Modus',
        default     => 'accept',
        required    => 1,
        description => '<p>Selecteer de modus waarin deze koppeling met Ogone communiceert (<b>let op</b>: op de acceptatie-omgeving zal de productie-modus nooit gebruikt worden, ook niet als hier &quot;Productie&quot; ingesteld is.</p>',
        data => {
            options => [
                {
                    label => 'Acceptatie',
                    value => 'accept',
                },
                {
                    label => 'Productie',
                    value => 'production',
                },
            ],
        },
    ),
];

use constant MODULE_SETTINGS => {
    name                          => 'ogone',
    label                         => 'Ogone-betalingen',
    interface_config              => INTERFACE_CONFIG_FIELDS,
    direction                     => 'outgoing',
    manual_type                   => ['text'],
    is_multiple                   => 0,
    is_manual                     => 0,
    retry_on_error                => 0,
    allow_multiple_configurations => 0,
    is_casetype_interface         => 0,
    test_interface                => 0,
    test_definition               => {},
    attribute_list                => [],
};

around BUILDARGS => sub {
    my $orig = shift;
    my $class = shift;

    return $class->$orig( %{ MODULE_SETTINGS() }, @_ );
};

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
