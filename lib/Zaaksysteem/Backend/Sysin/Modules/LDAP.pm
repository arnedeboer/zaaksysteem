package Zaaksysteem::Backend::Sysin::Modules::LDAP;

use Moose;

use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::ZAPI::Form::Field;

extends 'Zaaksysteem::Backend::Sysin::Modules';

=head1 Interface Properties

Below a list of interface properties, see
L<Zaaksysteem::Backend::Sysin::Modules> for details.

=cut

use constant INTERFACE_ID               => 'ldap';

use constant INTERFACE_CONFIG_FIELDS    => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_hostname',
        type => 'text',
        label => 'Server hostname',
        required => 1
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_realm',
        type => 'text',
        default => 'zaaksysteem',
        label => 'Domein',
        required => 1,
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_port',
        type => 'text',
        default => '389',
        label => 'Server port',
        required => 1
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_user_base_dn',
        type => 'text',
        default => 'dc=zaaksysteem,dc=nl',
        required => 1,
        label => 'Gebruiker base DN'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_user_filter',
        type => 'text',
        default => '(&(objectClass=posixAccount)(cn=%s))',
        required => 1,
        label => 'Gebruikers filter',
        description => 'Filter om te bepalen welke accounts relevant zijn voor de koppeling. Gebruik "%s" om aan te geven waar de username van de gebruiker ingevuld moet worden'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_role_base_dn',
        type => 'text',
        default => 'dc=zaaksysteem,dc=nl',
        required => 1,
        label => 'Rol base DN'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_role_filter',
        type => 'text',
        default => '(&(objectClass=posixGroup)(memberUid=%s))',
        required => 1,
        label => 'Rollen filter',
        description => 'Filter om te bepalen welke rollen relevant zijn voor de koppeling. Gebruik "%s" om aan te geven waar de rolnaam ingevuld moet worden'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_autocreate_user',
        type => 'checkbox',
        required => 1,
        label => 'Auto-aanmaak gebruikers',
        description => 'Indien het wenselijk is gebruikers te importeren bij de eerste keer dat ze zich aanmelden op het Zaaksysteem, vink dit aan'
    ),
];

use constant MODULE_SETTINGS => {
    name => INTERFACE_ID,
    label => 'Systeem LDAP',
    interface_config => INTERFACE_CONFIG_FIELDS,
    direction => 'incoming',
    manual_type => [],
    is_multiple => 1,
    is_manual => 1,
    retry_on_error => 0,
    allow_multiple_configurations => 0,
    is_casetype_interface => 0
};

around BUILDARGS => sub {
    my $orig = shift;
    my $class = shift;

    return $class->$orig( %{ MODULE_SETTINGS() }, @_ );
};

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 INTERFACE_CONFIG_FIELDS

TODO: Fix the POD

=cut

=head2 INTERFACE_ID

TODO: Fix the POD

=cut

=head2 MODULE_SETTINGS

TODO: Fix the POD

=cut

