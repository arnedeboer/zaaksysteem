package Zaaksysteem::Backend::Sysin::Modules::Key2Finance;

use Moose;
use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::ZAPI::Form::Field;
use Zaaksysteem::JSON;
use Zaaksysteem::Exception;
use Zaaksysteem::Constants;
use Zaaksysteem::Backend::Sysin::Transaction::Mutation;
use Zaaksysteem::Profile;
use Zaaksysteem::ZAPI::Response;
use Zaaksysteem::ZTT;
use Encode qw(encode_utf8);


BEGIN { extends 'Zaaksysteem::Backend::Sysin::Modules'; }

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::JSON
/;

=head1 Interface Properties

Below a list of interface properties, see
L<Zaaksysteem::Backend::Sysin::Modules> for details.

=cut

use constant INTERFACE_ID               => 'key2finance';

use constant INTERFACE_CONFIG_FIELDS    => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_api_key',
        type        => 'text',
        label       => 'API Key',
        required    => 1,
    )
];
has 'attribute_list_naw'     => (
    is          => 'ro',
    isa         => 'ArrayRef',
    default     => sub { return {}; },
    lazy        => 1,
);

use constant MODULE_SETTINGS            => {
    name                            => INTERFACE_ID,
    label                           => 'Key2Finance',
    interface_config                => INTERFACE_CONFIG_FIELDS,
    direction                       => 'outgoing',
    is_multiple                     => 0,
    is_manual                       => 1,
    allow_multiple_configurations   => 1,
    is_casetype_interface           => 1,
    has_attributes                  => 1,
    attribute_list                  => [
        {
            external_name   => 'STAMNUMMER_BELASTINGADM',
            attribute_type  => 'magic_string'
#            internal_name   => 'me.id', # hardcoded version, ugly in the interface
#            attribute_type  => 'defined'
        },
        {
            external_name   => 'OMSCHRIJVING',
            attribute_type  => 'magic_string'
        },
        {
            external_name   => 'BEDRAG',
            attribute_type  => 'magic_string',
        },
        {
            external_name   => 'OMSCHRIJVING_FOUTKODE',
            attribute_type  => 'magic_string'
        },
        {
            external_name   => 'AANSLAGNUMMER',
            attribute_type  => 'magic_string'
        },
        {
            external_name   => 'INCASSO_ACTIEF',
            attribute_type  => 'magic_string'
        },
        {
            external_name   => 'KODE_INK_UITG',
            attribute_type  => 'magic_string'
        },
        {
            external_name   => 'NOTAGEBONDEN_TEKST',
            attribute_type  => 'magic_string'
        },
        {
            external_name   => 'BEDRAG_BTW',
            attribute_type  => 'magic_string'
        },
        {
            external_name   => 'PERCENTAGE_BTW',
            attribute_type  => 'magic_string'
        },
        {
            external_name   => 'BTW_KODE',
            attribute_type  => 'magic_string'
        },
        {
            external_name   => 'TAAK',
            attribute_type  => 'magic_string'
        },
        {
            external_name   => 'AANVULLING_OMSCHRIJVING_TOT_50_POSTIES',
            attribute_type  => 'magic_string'
        },
        {
            external_name   => 'FCL_LANG',
            attribute_type  => 'magic_string'
        },
        {
            external_name   => 'PROJECTNUMMER_LANG',
            attribute_type  => 'magic_string'
        },
        {
            external_name   => 'AFDELING_LANG',
            attribute_type  => 'magic_string'
        },
        {
            external_name   => 'AKTIVITEIT_LANG',
            attribute_type  => 'magic_string'
        },
        {
            external_name   => 'ECL_LANG',
            attribute_type  => 'magic_string'
        },
        {
            external_name   => 'TAAK_LANG',
            attribute_type  => 'magic_string'
        },
        {
            external_name   => 'MACHTIGINGDATUM',
            attribute_type  => 'magic_string'
        },
        {
            external_name   => 'MACHTIGINGSKENMERK',
            attribute_type  => 'magic_string'
        },
        {
            external_name   => 'MACHTIGINGSTYPE',
            attribute_type  => 'magic_string'
        },
        {
            external_name   => 'BETAALKENMERK',
            attribute_type  => 'magic_string'
        },
        {
            external_name   => 'KREDIETBEHEERDER',
            attribute_type  => 'magic_string'
        },
        {
            external_name   => 'NOTANUMMER',
            attribute_type  => 'magic_string'
        },
        {
            external_name   => 'FCL',
            attribute_type  => 'magic_string'
        },
        {
            external_name   => 'ECL',
            attribute_type  => 'magic_string'
        },
        {
            external_name   => 'VERVAARDIGEN_NOTA',
            attribute_type  => 'magic_string'
        },
        {
            external_name   => 'NOTAGEBONDEN_TEKST_VOLLEDIG',
            attribute_type  => 'magic_string'
        }
    ],
    attribute_list_naw => [
        {
            external_name   => 'STAMNUMMER_BELASTINGEN',
            internal_name   => 'me.id',
            attribute_type  => 'defined'
        },
        {
            external_name   => 'NAAM',
            internal_name   => 'aanvrager_geslachtsnaam',
            attribute_type  => 'defined',
        },
        {
            external_name   => 'STRAATNAAM_VBL',
            internal_name   => 'aanvrager_straat',
            attribute_type  => 'defined'
        },
        {
            external_name   => 'HUISNUMMER_VBL',
            internal_name   => 'HUISNUMMER_VBL',
            attribute_type  => 'magic_string',
        },
        {
            external_name   => 'POSTKODE_NUMERIEK_VBL',
            internal_name   => 'aanvrager_postcode',
            attribute_type  => 'defined'
        },
        {
            external_name   => 'POSTKODE_ALFANUMERIEK_VBL',
            internal_name   => 'aanvrager_postcode',
            attribute_type  => 'defined'
        },
        {
            external_name   => 'NATUURLIJK_NIET_NATUURLIJK',
            internal_name   => 'aanvrager_type',
            attribute_type  => 'defined'
        },
        {
            external_name   => 'PLAATSNAAM_VBL',
            internal_name   => 'aanvrager_woonplaats',
            attribute_type  => 'defined'
        },
        {
            external_name   => 'VOORLETTERS_VBL',
            internal_name   => 'aanvrager_voorvoegsel',
            attribute_type  => 'defined'
        },
        {
            external_name   => 'VOORVOEGSEL_VBL',
            internal_name   => 'aanvrager_voorvoegsel',
            attribute_type  => 'defined'
        },
        {
            external_name   => 'HUISLETTER_VBL',
            internal_name   => 'HUISLETTER_VBL',
            attribute_type  => 'defined'
        },
        {
            external_name   => 'TOEVOEGING_HUISNUMMER_VBL',
            internal_name   => 'aanvrager_huisnummertoevoeging',
            attribute_type  => 'defined'
        },
        {
            external_name   => 'SOFINUMMER',
            internal_name   => 'aanvrager_burgerservicenummer',
            attribute_type  => 'defined'
        },
        {
            external_name   => 'A_NUMMER',
            internal_name   => 'aanvrager_anummer',
            attribute_type  => 'defined'
        },
        {
            external_name   => 'KVK_NUMMER_1',
            attribute_type  => 'magicstring',
            internal_name   => 'aanvrager_kvknummer'
        }
    ],
    trigger_definition  => {
        set_case_list   => {
            method  => 'key2fin_set_case_list',
            #update  => 1,
            public => 1,
        },
        close_cases     => {
            method  => 'key2fin_close_cases',
           # update  => 1,
            public => 1,
        },
        get_csv_naw     => {
            method  => 'key2fin_get_csv_naw',
            #update  => 1,
            public => 1,
        },
        get_csv_deb  => {
            method  => 'key2fin_get_csv_deb',
            #update  => 1,
            public => 1,
        }
    },
};


use constant ALL_NAW_FIELDS => [qw/
    RELATIE_NUMMER
    BEDRIJFSKODE
    BEDRIJFSKODE_FCL
    FCL_DEBITEUREN
    DEBITEURENNUMMER
    SOORT_DEBITEUR
    STAMNUMMER_BELASTINGEN
    NAAM
    VOORLETTERS_VBL
    VOORVOEGSEL_VBL
    STRAATNAAM_VBL
    HUISNUMMER_VBL
    HUISLETTER_VBL
    TOEVOEGING_HUISNUMMER_VBL
    AANDUIDING_HUISNUMMER_VBL
    POSTKODE_NUMERIEK_VBL
    POSTKODE_ALFANUMERIEK_VBL
    GEMEENTEKODE_VBL
    PLAATSNAAM_VBL
    LANDKODE_VBL
    LANDNAAM_VBL
    BANKNUMMER_RELATIE
    GIRONUMMER_RELATIE
    TELEFOONNUMMER_RELATIE
    TELEXNUMMER
    TELEFAX_NUMMER
    SOFINUMMER
    A_NUMMER
    BTW_ID_NUMMER
    NATUURLIJK_NIET_NATUURLIJK
    KODE_BLOKKERING_HERINN_AANM
    BETALINGSREGELING
    OPSCHONEN
    RENTE_BEREKENEN
    BTW_PLICHTIG
    KODE_BLOKKERING
    IND_AUT_INCASSO_TOEGESTAAN
    BEDRIJFSKODE_RC_NUMMER
    RC_NUMMER
    KODE_HERKOMST
    GEBOORTEDATUM
    POSTBUSNUMMER_CP
    ANTWOORDNUMMER_CA
    EMAILADRES
    BEDRIJFSKODE_GRK
    GROEPSKENMERK
    KODE_SUBADMIN_GRK
    BEDRIJFSKODE_OPT
    DEBNR_OUD
    BEDRAG_DEBET_DEBITEUR
    BEDRAG_CREDIT_DEBITEUR
    VALUTA
    EUR_BEDR_DB_DEBITEUR
    EUR_BEDR_CR_DEBITEUR
    BASIS_VALUTA
    BASIS_BEDR_DB_DEBITEUR
    BASIS_BEDR_CR_DEBITEUR
    BEDRIJFSKODE_VKM1
    VRIJ_KENMERKNUMMER1
    VRIJ_KENMERKWAARDE1
    BEDRIJFSKODE_VKM2
    VRIJ_KENMERKNUMMER2
    VRIJ_KENMERKWAARDE2
    BEDRIJFSKODE_VKM3
    VRIJ_KENMERKNUMMER3
    VRIJ_KENMERKWAARDE3
    BEDRIJFSKODE_VKM4
    VRIJ_KENMERKNUMMER4
    VRIJ_KENMERKWAARDE4
    BEDRIJFSKODE_VKM5
    VRIJ_KENMERKNUMMER5
    VRIJ_KENMERKWAARDE5
    IND_NAAMGEBRUIK
    IND_ADRESGEBRUIK
    IND_OVERLEDEN
    PARTNERNAAM
    VOORVOEGSEL_PARTNERNAAM
    HANDELSNAAM
    STAT_VENNOOTNAAM
    STRAATNAAM_CS
    HUISNUMMER_CS
    HUISLETTER_CS
    TOEVHUISNR_CS
    AANDHUISNR_CS
    POSTKODE_NUM_CS
    POSTKODE_ALFA_CS
    PLAATSNAAM_CS
    GEMEENTEKODE_CS
    LANDKODE_CS
    LANDNAAM_CS
    POSTKODE_NUM_CP
    POSTKODE_ALFA_CP
    PLAATSNAAM_CP
    GEMEENTEKODE_CP
    LANDKODE_CP
    LANDNAAM_CP
    POSTKODE_NUM_CA
    POSTKODE_ALFA_CA
    PLAATSNAAM_CA
    GEMEENTEKODE_CA
    LANDKODE_CA
    LANDNAAM_CA
    BUITENLANDS_ADRES1
    BUITENLANDS_ADRES2
    BUITENLANDS_ADRES3
    LANDKODE_BA
    LANDNAAM_BA
    NAW_DEB_LINK
    KVK_NUMMER_1
    SUB_DOSSIERNUMMER
    DDS_SLEUTEL
    RSIN
    VESTIGINGSNUMMER
    IBAN
    BIC
    EINDHEK
/];

use constant ALL_DEB_FIELDS => [qw/
    BEDRIJFSKODE
    NOTASOORT
    VOLGNUMMER
    DEBITEURENNUMMER
    OMSCHRIJVING
    KOPPELINGKODE
    INGANGSDATUM
    EINDDATUM
    VERVALDAG
    NOTAGEBONDEN_TEKST
    FOUTKODE
    KREDIETBEHEERDER
    NOTANUMMER
    BETALINGSKENMERK
    OMSCHRIJVING_FOUTKODE
    INVOERBESTAND
    KENMERK1
    KENMERK2
    KENMERK3
    KENMERK4
    KENMERK5
    BETAALKENMERK
    AANSLAGNUMMER
    STAMNUMMER_BELASTINGADM
    INCASSO_ACTIEF
    VALUTA
    VERZENDDATUM
    VERVALDATUM_TERMIJN
    VERVALDATUM_INCASSO
    IND_CORRECTIE
    AANTAL_TERMIJNEN
    AANTAL_INCASSO
    BEDRIJFSKODE
    NOTASOORT
    VOLGNUMMER
    REGELNUMMER
    BEDRIJFSKODE_FCL
    FCL
    PROJECTNUMMER
    AFDELING
    AKTIVITEIT
    ECL
    KODE_INK_UITG
    OMSCHRIJVING
    BEDRAG
    BEDRAG_PER_EENHEID
    KWANTITEIT
    TARIEFKODE
    BEDRAG_BTW
    PERCENTAGE_BTW
    FOUTKODE
    OMSCHRIJVING_FOUTKODE
    NOTAGEBONDEN_TEKST
    BEDRIJFSKODE_BTW_KODE
    BTW_KODE
    KODE_SUBADMIN_BTW
    TAAK
    BEDRIJFSKODE_TAAK
    VALUTA
    EUR_BEDR
    EUR_BEDR_BTW
    EUR_BEDR_PER_EENH
    BASIS_VALUTA
    BASIS_BEDR
    BASIS_BEDR_BTW
    BASIS_BEDR_PER_EENH
    AANVULLING_OMSCHRIJVING_TOT_50_POSTIES
    KOLOMNUMMER_RSV
    AANMANEN\/HERINNEREN
    VERVAARDIGEN_NOTA
    BELASTING_JAAR
    FCL_LANG
    PROJECTNUMMER_LANG
    AFDELING_LANG
    AKTIVITEIT_LANG
    ECL_LANG
    TAAK_LANG
    NAW_DEB_LINK
    MACHTIGINGDATUM
    MACHTIGINGSKENMERK
    MACHTIGINGSTYPE
    EINDHEK
    NOTAGEBONDEN_TEKST_VOLLEDIG
/];

=head2 BUILDARGS

Settings for module, see

=cut

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig( %{ MODULE_SETTINGS() } );
};

sub key2fin_set_case_list {
    my $self        = shift;
    my $params      = shift;
    my $interface   = shift;

    ### Collect case ids, in json, and create 1 record

    my $caselist = { results => [] };

    my $interface_config = $interface->get_interface_config;

    throw(
        'sysin/modules/key2finance/incorrect_api_key',
        "incorrect api key"
    ) unless $params->{api_key} eq $interface_config->{api_key};

    my $attributes = $interface->get_interface_config->{attribute_mapping};

    my $naw_attributes = MODULE_SETTINGS->{attribute_list_naw};

    my $casetype_id = $interface->case_type_id->id or die "case_type_id not specified, check configuration and enforce in frontend";

    my $cases = $interface->result_source->schema->resultset('Zaak')->search({
        status => 'open',
        'zaaktype_node_id.zaaktype_id' => $casetype_id,
    }, {
        join => 'zaaktype_node_id',
    });

    while(my $case = $cases->next()) {

        # only export this case if everything is ready for case closing, including result
        next unless
            $case->can_volgende_fase->can_advance() &&
            $case->volgende_fase->status eq $case->afhandel_fase->status;

        my $row = $self->export_case({
            attributes => $attributes,
            naw_attributes => $naw_attributes,
            case => $case
        });

        push @{
            $caselist->{results}
        }, $row;

    }

    my $json = JSON::encode_json($caselist);

    my $external_transaction_id = localtime();

    my $transaction = $interface
                    ->result_source
                    ->schema
                    ->resultset('Transaction')
                    ->transaction_create(
                        {
                            input_data      => $json,
                            interface_id    => $interface->id,
                            external_transaction_id => $external_transaction_id
                        }
                    );

    my $record = $transaction
                ->transaction_records
                ->transaction_record_create(
        {
            transaction_id  => $transaction->id,
            input           => $json,
            output          => ' ',
        }
    );

    return $transaction;
}


=head2 export_case

Harvest case data and return in a palatable format.

=cut

define_profile export_case => (
    required => [qw/attributes naw_attributes case/]
);
sub export_case {
    my $self = shift;
    my $input = shift;

    my $options = assert_profile($input)->valid;

    my $ztt_cache = {};

    my $attributes       = $options->{attributes};
    my $naw_attributes   = $options->{naw_attributes};
    my $case             = $options->{case};
    my $ztt              = Zaaksysteem::ZTT->new(cache => $ztt_cache);

    $ztt->add_context($case);

    my $stddeb_row = {};

    foreach my $attribute (@$attributes) {
        my $key           = $attribute->{external_name};
        my $internal_name = $attribute->{internal_name};

        unless ($internal_name) {
            $stddeb_row->{$key} = '';
            next;
        }

        # searchable objects are represented as hashref structures
        if(ref $internal_name) {
            my $searchable_object = $internal_name; # misnomer correction
            my $label = $searchable_object->{searchable_object_label};
            my $type  = $searchable_object->{searchable_object_type};

            next unless $label;

            my $template = sprintf('[[%s]]', $label);

            my $output = $ztt->process_template($template)->string || '';
            #warn "ord:" . join "-", map { $_ . ': ' . ord($_) } ($output =~ m|.|gis);

            # csv output don't like newlines
            # double newlines indicate field separation
            $output =~ s|\r\n|, |gis;

            # single newlines would be within fields, get rid.
            $output =~ s|[\n\r]||gis;

            $stddeb_row->{ $key } = $output;
        } else {
            $stddeb_row->{ $key } = $case->systeemkenmerk($internal_name);
        }
    }

    my $stdnaw_row = {};
    foreach my $attribute (@$naw_attributes) {
        my $key           = $attribute->{external_name};
        my $internal_name = $attribute->{internal_name};

        my $value;
        if($key eq 'HUISNUMMER_VBL') {
            $value = $case->aanvrager_object->huisnummer;
        } elsif($key eq 'HUISLETTER_VBL') {
            $value = $case->aanvrager_object->huisletter;
        } elsif($key eq 'TOEVOEGING_HUISNUMMER_VBL') {
            $value = $case->aanvrager_object->huisnummertoevoeging;
        } elsif($key eq 'VOORVOEGSEL_VBL') {
            $value = $case->systeemkenmerk('aanvrager_voorvoegsel');
        } elsif($key eq 'VOORLETTERS_VBL') {

            # in case of bedrijven or medewerkers, don't bother. the regex will not hurt
            $value = $case->aanvrager_object->type eq 'natuurlijk_persoon' ?
                $case->aanvrager_object->voorletters : '';

            # zaaksysteem generates voorletters with dots, key2finance
            # likes it without dots.
            $value =~ s|\.||gis;
        } elsif($key eq 'NAAM') {

            $value = $case->aanvrager_object->type eq 'natuurlijk_persoon' ?
                $case->aanvrager_object->geslachtsnaam :
                $case->aanvrager_object->handelsnaam;

        } else {
            $value = $case->systeemkenmerk($internal_name);
        }

        # these do some formatting after the ->systeemkenmerk call
        if($key eq 'POSTKODE_ALFANUMERIEK_VBL') {
            $value =~ s|\d||gis;
        } elsif($key eq 'POSTKODE_NUMERIEK_VBL') {
            $value =~ s|\D||gis;
        } elsif($key eq 'NATUURLIJK_NIET_NATUURLIJK') {
            $value = $value eq 'Natuurlijk persoon' ? 'N' : 'I';
        }

        $stdnaw_row->{$key} = $value;
    }

    # naw records are linked to deb record using these fields.
    # there are some use cases where this is hard to achieve otherwise.
    $stdnaw_row->{STAMNUMMER_BELASTINGEN} = $stddeb_row->{STAMNUMMER_BELASTINGADM};

    my $result = {
        stddeb_row => $stddeb_row,
        stdnaw_row => $stdnaw_row,
        case_id => $case->id
    };

    return $result;
}



sub key2fin_get_csv_deb {
    my $self        = shift;
    my $params      = shift;
    my $interface   = shift;

    my $interface_config = JSON::decode_json($interface->interface_config);

    throw(
        'sysin/modules/key2finance/incorrect_api_key',
        "incorrect api key"
    ) unless $params->{api_key} eq $interface_config->{api_key};

    my $transaction = $interface->transactions->find($params->{transaction_id});

    throw(
        'sysin/modules/key2finance/transaction_mismatch',
        "No transaction found for that id/interface combination"
    ) if not defined $transaction;

    my $row_data = $self->get_partial_row_data({
        transaction => $transaction,
        part => 'stddeb_row'
    });

    my $deb_fields = ALL_DEB_FIELDS;

    return Zaaksysteem::ZAPI::Response->new(
        unknown     => $row_data,
        uri_prefix  => URI->new('http://localhost/'),
        no_pager    => 1,
        options     => {
            'csv'   => {
                sep_char        => ';',
                column_order    => $deb_fields,
                no_header       => 1
            }
        }
    );
}

sub get_partial_row_data {
    my ($self, $options) = @_;

    my $transaction = $options->{transaction}   or die "need transaction";
    my $part        = $options->{part}          or die "need part";

    my $record_rs = $transaction->transaction_records->search();

    my $csv_result = [];

    while(my $record = $record_rs->next) {
        my $string = encode_utf8($record->input);
        my $input   = JSON::decode_json($string);

        my $results = $input->{results};

        foreach my $result (@$results) {
            my $partial_row_data = $result->{$part};
            push @$csv_result, $partial_row_data;
        }
    }

    return $csv_result;

}

sub key2fin_get_csv_naw {
    my $self        = shift;
    my $params      = shift;
    my $interface   = shift;

    my $interface_config = JSON::decode_json($interface->interface_config);

    throw(
        'sysin/modules/key2finance/no_transaction_id',
        'No transaction id given'
    ) unless $params->{transaction_id};

    throw(
        'sysin/modules/key2finance/incorrect_api_key',
        "incorrect api key"
    ) unless $params->{api_key} eq $interface_config->{api_key};

    my $transaction = $interface->transactions->find($params->{transaction_id});

    throw(
        'sysin/modules/key2finance/transaction_mismatch',
        "No transaction found for that id/interface combination"
    ) if not defined $transaction;

    my $row_data = $self->get_partial_row_data({
        transaction => $transaction,
        part => 'stdnaw_row'
    });

    my $naw_fields = ALL_NAW_FIELDS;
    return Zaaksysteem::ZAPI::Response->new(
        unknown     => $row_data,
        uri_prefix  => URI->new('http://localhost/'),
        no_pager    => 1,
        options     => {
            'csv'   => {
                sep_char        => ';',
                column_order    => $naw_fields,
                no_header       => 1
            }
        }
    );
}

sub key2fin_close_cases {
    my $self        = shift;
    my $params      = shift;
    my $interface   = shift;

    my $interface_config = JSON::decode_json($interface->interface_config);

    throw(
        'sysin/modules/key2finance/incorrect_api_key',
        "incorrect api key"
    ) unless $params->{api_key} eq $interface_config->{api_key};

    my $transaction = $interface
                    ->result_source
                    ->schema
                    ->resultset('Transaction')
                    ->find($params->{transaction_id});

    my $external_transaction_id = localtime();
    $self->process(
        {
            input_data              => $transaction->input_data,
            external_transaction_id => $external_transaction_id,
            interface               => $interface,
        }
    );
}

sub _process_row {
    my $self            = shift;
    my $record          = shift;
    my $params          = shift;

    ### Close case and throw error on failure, or success on success
    my $case_id = $params->{case_id};

    my $case        = $record
                    ->result_source
                    ->schema
                    ->resultset('Zaak')->find($case_id);

    my $oldvalue = $case->status;

    $case->set_gesloten(DateTime->now());

    $case->logging->trigger('case/early_settle', {
        component => 'zaak',
        data => {
            case_id => $case->id,
            reason => 'Gesloten ivm einde key2finance transactie',
            case_result => $case->resultaat
        }
    });

    my $mutation = Zaaksysteem::Backend::Sysin::Transaction::Mutation
                    ->new(
                        table       => 'Zaak',
                        table_id    => $case->id,
                        update      => 1
                    );

# HIER gaat iets mis, need logging:)
    # $mutation->add_mutation(
    #     column      => 'status',
    #     old_value   => $oldvalue,
    #     new_value   => $case->status
    # );

    # $self->process_stash->{row}->{mutations} = $mutation;

    return 1;
}


1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 ALL_DEB_FIELDS

TODO: Fix the POD

=cut

=head2 ALL_NAW_FIELDS

TODO: Fix the POD

=cut

=head2 INTERFACE_CONFIG_FIELDS

TODO: Fix the POD

=cut

=head2 INTERFACE_ID

TODO: Fix the POD

=cut

=head2 MODULE_SETTINGS

TODO: Fix the POD

=cut

=head2 get_partial_row_data

TODO: Fix the POD

=cut

=head2 key2fin_close_cases

TODO: Fix the POD

=cut

=head2 key2fin_get_csv_deb

TODO: Fix the POD

=cut

=head2 key2fin_get_csv_naw

TODO: Fix the POD

=cut

=head2 key2fin_set_case_list

TODO: Fix the POD

=cut

