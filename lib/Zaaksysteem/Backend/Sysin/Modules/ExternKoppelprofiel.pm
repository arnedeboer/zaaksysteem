package Zaaksysteem::Backend::Sysin::Modules::ExternKoppelprofiel;

use Moose;

use Zaaksysteem::Tools;
use Zaaksysteem::ZAPI::Form::Field;
use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::Types qw(UUID);

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
  Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
  Zaaksysteem::Backend::Sysin::Modules::Roles::Tests
  /;

use constant INTERFACE_ID => 'externe_koppeling';

use constant INTERFACE_CONFIG_FIELDS => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name     => 'interface_objecttypes',
        type     => 'spot-enlighter',
        label    => 'Objecttypes',
        required => 1,
        description => 'Bepaal hier welk objecttype bevraagd kan worden door de externe partij',
        data => {
            multi       => 500,
            restrict    => 'objecttypes',
            placeholder => 'Type uw zoekterm',
            label       => 'label',
        }
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_api_key',
        type        => 'text',
        label       => 'API key',
        required    => 1,
        description => 'Interface key voor externe koppeling',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name     => 'interface_medewerker',
        type     => 'spot-enlighter',
        label    => 'Medewerker',
        required => 1,
        description => 'Bepaal hier welke medewerkerrechten de externe partij krijgt.',
        data => {
            restrict    => 'contact/medewerker',
            placeholder => 'Type uw zoekterm',
            label       => 'naam',
        }
    ),
];

use constant MODULE_SETTINGS => {
    name                          => INTERFACE_ID,
    label                         => 'Extern koppelprofiel',
    interface_config              => INTERFACE_CONFIG_FIELDS,
    direction                     => 'incoming',
    manual_type                   => ['text'],
    is_multiple                   => 1,
    is_manual                     => 1,
    retry_on_error                => 0,
    allow_multiple_configurations => 1,
    is_casetype_interface         => 0,
    has_attributes                => 0,
    trigger_definition => {
        search_object => {
            method => 'search_object',
            update => 0
        },
    },
    case_hooks      => [],
    test_interface  => 0,
};

has exception => (is => 'rw');

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig(%{MODULE_SETTINGS()});
};

define_profile search_object => (
    required => {
        object_class => 'Str',
        model => 'Any',
    },
    optional => {
        uuid => UUID,
    },
);

sub search_object {
    my ($self, $params, $interface) = @_;
    $params = assert_profile($params)->valid();

    my $model = delete $params->{model};
    my $object_class = delete $params->{object_class};

    return [ $model->search($object_class, $params) ];
}

1;

__END__

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::ExternKoppelprofiel - Extern koppelprofiel

=head1 SYNOPISIS

=head1 METHODS

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 INTERFACE_CONFIG_FIELDS

TODO: Fix the POD

=cut

=head2 INTERFACE_ID

TODO: Fix the POD

=cut

=head2 MODULE_SETTINGS

TODO: Fix the POD

=cut

=head2 search_object

TODO: Fix the POD

=cut

