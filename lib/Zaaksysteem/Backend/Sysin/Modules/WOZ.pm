package Zaaksysteem::Backend::Sysin::Modules::WOZ;
use Moose;

use Try::Tiny;
use Zaaksysteem::Tools;
use Zaaksysteem::ZAPI::Form::Field;
use Zaaksysteem::ZAPI::Form;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
    /;


=head1 Interface Properties

Below a list of interface properties, see
L<Zaaksysteem::Backend::Sysin::Modules> for details.

=cut

use constant INTERFACE_ID            => 'woz';
use constant INTERFACE_CONFIG_FIELDS => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_username',
        type        => 'text',
        label       => 'Gebruikersnaam',
        description => 'Gebruikersnaam voor Cyclomedia',
        required    => 1,
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_password',
        type        => 'password',
        label       => 'Wachtwoord',
        required    => 1,
        description => 'Wachtwoord voor Cyclomedia',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_key',
        type        => 'text',
        label       => 'API key',
        description => 'API key voor Cyclomedia',
        required    => 1,
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_account_id',
        type        => 'text',
        label       => 'Account ID',
        description => "Account ID for Cyclomedia",
        required    => 1,
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_private_key',
        type        => 'textarea',
        label       => 'Private key voor Cyclomedia',
        required    => 0,
    ),
## is present in beheer/woz. I'm not changing that bit for now.
#    Zaaksysteem::ZAPI::Form::Field->new(
#        name        => 'interface_intro_txt',
#        type        => 'richtext',
#        label       => 'Introductietekst',
#        required    => 1,
#    ),
#    Zaaksysteem::ZAPI::Form::Field->new(
#        name        => 'interface_waardebepaling_txt',
#        type        => 'richtext',
#        label       => 'Waardebepaling',
#        required    => 1,
#    ),
#    Zaaksysteem::ZAPI::Form::Field->new(
#        name        => 'interface_object_txt',
#        type        => 'richtext',
#        label       => 'Objecttekst',
#        required    => 1,
#    ),
#   Zaaksysteem::ZAPI::Form::Field->new(
#        name        => 'interface_kadaster_txt',
#        type        => 'richtext',
#        label       => 'Kadastrale percelentekst',
#        required    => 1,
#    ),
#    Zaaksysteem::ZAPI::Form::Field->new(
#        name        => 'interface_sjabloon',
#        type        => 'file',
#        label       => 'WOZ sjabloon',
#        required    => 1,
#    ),
#    Zaaksysteem::ZAPI::Form::Field->new(
#        description => 'De URL voor plaatjes van het WOZ object. Formaat: basis-url/[[woz_object_id]].extensie, bijv. http://www.mijngemeente.nl/woz/[[woz_object_id]].jpg',
#        label       => 'URL',
#        name        => 'interface_url',
#        type        => 'text',
#        required    => 1,
#    ),
#    Zaaksysteem::ZAPI::Form::Field->new(
#        description => 'Geef mogelijkheid voor opmerkingen in PIP',
#        label       => 'Reactie op PIP toestaan',
#        name        => 'interface_pip_reactie',
#        type        => 'checkbox',
#        required    => 0,
#    ),
];

use constant MODULE_SETTINGS => {
    name                          => INTERFACE_ID,
    label                         => 'WOZ koppeling',
    interface_config              => INTERFACE_CONFIG_FIELDS,
    direction                     => 'incoming',
    manual_type                   => ['text'],
    is_multiple                   => 0,
    is_manual                     => 0,
    allow_multiple_configurations => 0,
    is_casetype_interface         => 0,
    has_attributes                => 0,
    attribute_list                => [],
    retry_on_error                => 0,
    trigger_definition            => {},
};

=head2 BUILDARGS

Settings for module, see

=cut

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig(%{ MODULE_SETTINGS() });
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
