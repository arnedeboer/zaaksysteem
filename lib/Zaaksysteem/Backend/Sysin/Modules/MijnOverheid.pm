package Zaaksysteem::Backend::Sysin::Modules::MijnOverheid;
use Moose;

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::MijnOverheid - Interface module for MijnOverheid/Lopende Zaken

=cut

use JSON;
use Zaaksysteem::Tools;
use Zaaksysteem::XML::MijnOverheid::HTTPClient;
use Zaaksysteem::XML::MijnOverheid::SpoofClient;
use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::ZAPI::Form::Field;

extends 'Zaaksysteem::Backend::Sysin::Modules';
with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
    Zaaksysteem::Backend::Sysin::Modules::Roles::Tests
/;

use constant INTERFACE_ID => 'mijnoverheid';

use constant INTERFACE_CONFIG_FIELDS => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_oin',
        type => 'text',
        label => 'Overheidsidentificatienummer',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_esb',
        type => 'select',
        label => 'ESB-provider',
        data => {
            options => [
                {
                    value => 'opentunnel',
                    label => 'OpenTunnel',
                }
            ],
        },
        default => 'opentunnel',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_endpoint_bb_check',
        type => 'text',
        label => 'Endpoint-URL van de "Berichtenbox Check"-service',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_endpoint_bb',
        type => 'text',
        label => 'Endpoint-URL van de "Berichtenbox"-service',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_endpoint_lz',
        type => 'text',
        label => 'Endpoint-URL van de "Lopende Zaken"-service',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_ca_cert',
        type => 'file',
        label => 'CA-certificaate voor de ESB',
        description => 'Upload hier het certificaat van de <abbr title="Certificate Authority (de partij die de verbinding met de ESB heeft gecertificeerd)">CA</abbr> die het certificaat gebruikt door de ESB heeft ondertekend.',
        required    => 0,
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_spoofmode',
        type => 'checkbox',
        label => 'Spoof-modus',
        description => qq{
            Met deze "spoof"-modus worden geen berichten naar de
            geconfigureerde ESB gestuurd,, zodat de functionaliteit getest kan
            worden zonder afhankelijkheid van de ESB.<br>
            <b>LET OP</b>: Niet aanzetten op een productie-omgeving. Deze optie is alleen voor
            test-doeleinden en levert geen juiste/echte gegevens.
        },
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_base_uri',
        type => 'display',
        label => 'Basis-URI',
        description => 'De genoteerde URI wordt gebruikt om de link naar de PIP in MijnOverheid-berichten samen te stellen',
        data => {
            template => '<[field.value]>'
        }
    )
];

use constant MODULE_SETTINGS => {
    name                          => INTERFACE_ID,
    label                         => 'MijnOverheid',
    interface_config              => INTERFACE_CONFIG_FIELDS,
    direction                     => 'outgoing',
    is_multiple                   => 0,
    is_manual                     => 0,
    retry_on_error                => 1,
    allow_multiple_configurations => 0,
    is_casetype_interface         => 0,
    has_attributes                => 0,
    test_interface                => 1,
    test_definition               => {
        description => qq{
            Hier kunt u de verbinding met MijnOverheid testen.
        },
        tests => [
            {
                id    => 1,
                label => 'Test',
                name  => 'test_secure_connection',
                method => 'test_secure_connection',
                description => qq{
                    Deze test probeert verbinding te maken met de server(s) van MijnOverheid.
                },
            },
        ],
    },
    trigger_definition => {
        PostStatusUpdate => {
            method => 'PostStatusUpdate',
            update => 1
        },
    },
};

around BUILDARGS => sub {
    my $orig = shift;
    my $class = shift;

    $class->$orig( %{ MODULE_SETTINGS() } );
};

=head2 _load_values_into_form_object

=cut

around _load_values_into_form_object => sub {
    my $orig = shift;
    my $self = shift;
    my $opts = $_[1]; # get options

    my $form = $self->$orig(@_);

    $form->load_values({
        interface_base_uri => $opts->{ base_url }
    });

    return $form;
};


=head2 PostStatusUpdate

=cut

define_profile PostStatusUpdate => (
    required => [qw/
        case_id
        kenmerken
        message
    /],
);

sub PostStatusUpdate {
    my $self      = shift;
    my $params    = assert_profile(shift)->valid;
    my $interface = shift;

    $interface->process({
        external_transaction_id => 'unknown',
        input_data => JSON->new->pretty->encode($params->{message}),
        processor_params => {
            %$params,
            processor => '_process_external_system_message',
        },
    });
}

=head2 _process_external_system_message

Perform the calls to OpenTunnel to handle "MijnOverheid" messaging.

=cut

sub _process_external_system_message {
    my $self   = shift;
    my $record = shift;

    my $transaction = $self->process_stash->{transaction};
    my $params      = $transaction->get_processor_params();
    my $interface   = $transaction->interface;

    my $base_uri    = $interface->jpath('$.base_uri');

    my $case = $interface->result_source->schema->resultset('Zaak')->find($params->{case_id});
    my $requestor = $case->aanvrager_object;

    try {
        if ($requestor->btype ne 'natuurlijk_persoon') {
            throw('mijnoverheid/requestor_type', "MijnOverheid wordt alleen ondersteund voor natuurlijke personen");
        }
        my $bsn = sprintf("%09d", $requestor->burgerservicenummer);

        my $api = $self->_mijnoverheid_client($interface);

        my $subject = "Bericht verstuurd";
        my $message = "";
        my ($result, $xml, $xml_out);
        if ($params->{message}{message_type} eq 'Lopende Zaken') {
            ($result, $xml, $xml_out) = $api->lopende_zaak(
                bsn     => $bsn,
                case_id => $case->id,

                date_of_registration => $case->registratiedatum,
                date_target          => $case->streefafhandeldatum,
                casetype_name        => $case->zaaktype_node_id->titel,
                explanation          => $case->onderwerp_extern,
                status               => $params->{message}{status},
                status_name          => $params->{message}{status_name},
                url                  => "${base_uri}pip/zaak/" . $case->id,
                (defined $case->afhandeldatum)
                    ? (date_of_completion => $case->afhandeldatum)
                    : (),
                (defined $case->resultaat)
                    ? (result             => $case->resultaat)
                    : (),
            );

            $self->log->trace("LopendeZaken XML:\n" . $xml);
        }
        elsif($params->{message}{message_type} eq 'Berichtenbox') {

            my ($check_result, $check_xml, $request_xml) = $api->has_berichtenbox(
                bsn => sprintf("%09d", $bsn)
            );

            $record->input($request_xml);
            $record->output($check_xml);

            if (!$check_result->{has_berichtenbox}) {
                throw('mijnoverheid/not_available', "Burger heeft geen MijnOverheid berichtenbox");
            }

            my $ztt = Zaaksysteem::ZTT->new();
            $ztt->add_context($case);

            $message = $ztt->process_template( $params->{message}{message} )->string // '';
            $subject = $ztt->process_template( $params->{message}{subject} )->string // '';

            ($result, $xml, $xml_out) = $api->berichtenbox_message(
                bsn => $bsn,
                subject   => $subject,
                content   => $message,
                reference => 'BERICHT1',
            );

            $self->log->trace("BerichtenBox XML:\n" . $xml);
        }
        else {
            throw("mijnoverheid/unknown_message_type", "Onbekend berichttype: " . $params->{message}{message_type});
        }

        $record->output($record->output . "\n\n" . $xml);
        $record->input($record->input . "\n\n" . $xml_out);

        if (!$result->{accepted}) {
            throw(
                "mijnoverheid/not_accepted",
                "Bericht niet geaccepteerd door MijnOverheid: " . $result->{message} // '[geen melding]',
            );
        }

        my $logging = $interface->result_source->schema->resultset('Logging');
        $logging->trigger(
            'case/send_mijnoverheid_message', {
            component    => 'case',
            component_id => $params->{case_id},
            zaak_id      => $params->{case_id},
            data         => {
                destination => $params->{message}{message_type},
                subject => $subject,
                message => $message,
            }
        });
    }
    catch {
        my $message = "Algemene fout";
        if (blessed($_)) {
            my $error_set;
            if ($_->can('object')) {
                my $object = $_->object;

                if (blessed($object) && $object->isa('HTTP::Message')) {
                    $error_set = 1;
                    $transaction->error_message($object->as_string);
                }
            }

            if ($_->can('message')) {
                $message = $_->message;
                $transaction->error_message($message) unless $error_set;
            }
        }

        # Log error $_ in case. *Then* fail the transaction.
        my $logging = $interface->result_source->schema->resultset('Logging');
        $logging->trigger(
            'case/send_mijnoverheid_message', {
            component    => 'case',
            component_id => $params->{case_id},
            zaak_id      => $params->{case_id},
            data         => {
                destination => $params->{message}{message_type},
                subject => 'Bericht kon niet verzonden worden',
                message => $message,
                error   => 1,
            }
        });
    };

    return;
}

sub _mijnoverheid_client {
    my $self = shift;
    my ($interface) = @_;

    if ($interface->jpath('$.spoofmode')) {
        return Zaaksysteem::XML::MijnOverheid::SpoofClient->new();
    }

    my $instance = Zaaksysteem::XML::Compile
        ->xml_compile
        ->add_class('Zaaksysteem::XML::MijnOverheid::Instance')
        ->mijnoverheid;

    my $schema = $interface->result_source->schema;
    my $ca_file = $schema->resultset('Filestore')->find($interface->jpath('$.ca_cert[0].id'));

    my $ua = LWP::UserAgent->new();
    if ($ca_file) {
        $ua->ssl_opts(
            verify_hostname => 1,
            SSL_ca_file     => $ca_file->get_path,
        );
    }

    my $client = Zaaksysteem::XML::MijnOverheid::HTTPClient->new(
        xml_instance => $instance,
        ua           => $ua,
        sender       => $interface->jpath('$.oin'),
        call_urls    => {
            has_berichtenbox     => $interface->jpath('$.endpoint_bb_check'),
            berichtenbox_message => $interface->jpath('$.endpoint_bb'),
            lopende_zaak         => $interface->jpath('$.endpoint_lz'),
        },
    );

    return $client;
}

=head2 test_secure_connection

Tests if a connection can be made to MijnOverheid.

=cut

sub test_secure_connection {
    my $self = shift;
    my $interface = shift;

    my $schema = $interface->result_source->schema;
    my $ca_file = $schema->resultset('Filestore')->find($interface->jpath('$.ca_cert[0].id'));
    if (!$ca_file) {
        throw(
            'sysin/modules/test/error',
            'CA certificate is not present.'
        );
    }

    $self->test_host_port_ssl(
        $interface->jpath('$.endpoint_bb_check'),
        $ca_file->get_path,
    );
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
