package Zaaksysteem::Backend::Sysin::Modules::STUFPRS;

use Moose;

use Zaaksysteem::Tools;
use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::ZAPI::Form::Field;

use Zaaksysteem::Profiles qw/PROFILE_NATUURLIJK_PERSOON/;

use JSON;

use Zaaksysteem::Exception;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::StUFPRSNPS
/;

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::STUFPRS - Handling of StUF 0204 entity PRS calls

=head1 SYNOPSIS


=head1 DESCRIPTION

This module handles all calls regarding StUF 0204 of entity PRS: Natuurlijk Persoon

=cut


###
### Interface Properties
###
### Below a list of interface properties, see
### L<Zaaksysteem::Backend::Sysin::Modules> for details.

use constant INTERFACE_ID               => 'stufprs';

use constant INTERFACE_CONFIG_FIELDS    => [];

use constant MODULE_SETTINGS            => {
    name                            => INTERFACE_ID,
    label                           => 'StUF Koppeling PRS',
    interface_config                => INTERFACE_CONFIG_FIELDS,
    direction                       => 'incoming',
    manual_type                     => ['text', 'file'],
    is_multiple                     => 0,
    is_manual                       => 1,
    retry_on_error                  => 1,
    allow_multiple_configurations   => 0,
    is_casetype_interface           => 0,
    trigger_definition              => {
        disable_subscription   => {
            method  => 'disable_subscription',
            #update  => 1,
        },
        search          => {
            method  => 'search_natuurlijkpersoon',
            #update  => 1,
        },
        import      => {
            method  => 'import_natuurlijkpersoon',
            #update  => 1,
        },
        search_for_single_result => {
            method  => 'search_for_single_result',
        }
    },
    attribute_list                  => [
        {
            external_name   => 'bsn-nummer',
            internal_name   => 'burgerservicenummer',
            attribute_type  => 'defined'
        },
    ]
};

has 'stuf_object_type' => (
    'is'        => 'ro',
    'default'   => 'PRS'
);

has 'stuf_subscription_table' => (
    'is'        => 'ro',
    'default'   => 'NatuurlijkPersoon'
);

###
### BUILDARGS
###

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig( %{ MODULE_SETTINGS() } );
};

=head1 ATTRIBUTES

=head2 stuf0204

isa: Zaaksysteem::StUF::0204::Instance

Returns the xml_compile instance object for StUF 0204

=cut

has 'stuf0204'   => (
    'is'        => 'rw',
    'isa'       => 'Zaaksysteem::StUF::0204::Instance',
    'lazy'      => 1,
    'default'   => sub {
        return Zaaksysteem::XML::Compile->xml_compile->add_class('Zaaksysteem::StUF::0204::Instance')->stuf0204;
    }
);

=head1 METHODS

=head2 search_natuurlijkpersoon_complete

Search on BSN, first search in local sbus, and when no results found, search in gba-v

=cut

use constant SINGLE_RESULT_PROFILE => {
    required            => [qw//],
    optional            => [qw/
        burgerservicenummer
        geboortedatum
        postcode
        huisnummer
        huisletter
    /],
    dependency_groups   => {
        vbl_set     => [qw/postcode huisnummer geboortedatum/],
    },
    require_some        => {
        vbl_or_bsn  => [1, qw/postcode burgerservicenummer/],
    },
};

has 'profile_single_result_search' => (
    is      => 'ro',
    lazy    => 1,
    default => sub { return SINGLE_RESULT_PROFILE; }
);

=head2 search_for_single_result

TODO: Fix me

=cut

define_profile 'search_for_single_result' => (
    %{ SINGLE_RESULT_PROFILE() }
);

sub search_for_single_result {
    my $self            = shift;
    my ($params)        = @_;

    assert_profile($params || {});

    ### When using pink, we could do an integrale zoekvraag.
    my $interface       = $_[1];
    my $config_iface    = $self->get_config_interface($interface);
    my $module_cfg      = $self->get_config_from_config_iface(
        $interface
    );

    unless ($config_iface->module_object->can_search_sbus($config_iface)) {
        throw(
            'sysin/modules/stuf' . lc($self->stuf_object_type) . '/search_natuurlijkpersoon_complete/no_permission',
            'Searching via SBUS not setup'
        )
    }

    if (
        $module_cfg->{stuf_supplier} eq 'pink' && $config_iface->module_object->can_search_gbav($config_iface)
    ) {
        $_[0]->{via_gbav} = 1;
    }

    ### Find results

    my $results         = $self->_handle_single_search(@_);

    if ($results && !@{ $results } && !$_[0]->{via_gbav} && $config_iface->module_object->can_search_gbav($config_iface)) {
        $_[0]->{via_gbav} = 1;
        $results = $self->_handle_single_search(@_);
    }

    return $results;
}

sub _handle_single_search {
    my $self            = shift;

    my $results         = $self->search_natuurlijkpersoon(@_);

    if ($results && @{ $results } > 1) {
        throw(
            'sysin/modules/stuf' . lc($self->stuf_object_type) . '/search_for_single_result/multiple',
            'Multiple results found, please refine your search'
        );
    }

    return $results;
}

=head2 search_natuurlijkpersoon

Arguments: \%params

Return value: $NP_HASH

    $search_results = $interface->process_trigger(
        'search',
        {
            burgerservicenummer => '987654321'
        }
    );

    ## ArrayRef with search results:
    # [
    #   {
    #     'a_nummer' => 4919938078,
    #     'aanduiding_naamgebruik' => undef,
    #     'burgerlijke_staat' => undef,
    #     'burgerservicenummer' => 987654321,
    #     'datum_overlijden' => undef,
    #     'external_transaction_id' => 'sleutel-172',
    #     'functie_adres' => 'W',
    #     'geboortedatum' => '19750815',
    #     'gegevensbeheer_id' => '172',
    #     'geslachtsaanduiding' => 'M',
    #     'geslachtsnaam' => 'Muler',
    #     'huisnummer' => 68,
    #     'huisnummertoevoeging' => 'MMXI',
    #     'indicatie_geheim' => undef,
    #     'postcode' => '8387KD',
    #     'straatnaam' => 'Mxyvwemaqpddplhmg',
    #     'verzendend_id' => undef,
    #     'voorletters' => 'XF',
    #     'voornamen' => 'Sam',
    #     'voorvoegsel' => undef,
    #     'woonplaats' => 'Lvxnrpuiy'
    #   },
    #   {
    #       [...]
    #   }
    # ]


Initiates a C<vraagBericht> StUF call to a ServiceBus. See the profile
L<Zaaksysteem::Profiles#PROFILE_NATUURLIJK_PERSOON> for allowed parameters (every
optional AND required parameter in this profile is optional with this call)

=cut


define_profile 'search_natuurlijkpersoon' => (
    required => [
    ],
    optional    => [
        @{ PROFILE_NATUURLIJK_PERSOON()->{required} },
        @{ PROFILE_NATUURLIJK_PERSOON()->{optional} },
        'via_gbav'
    ],
    # require_some => {
    #     'bsn_or_geslachtsnaam'  => [1, qw/burgerservicenummer geslachtsnaam/],
    # },

    ### Below constraints are from official StUF XSD
    constraint_methods  => PROFILE_NATUURLIJK_PERSOON()->{constraint_methods},
);

sub search_natuurlijkpersoon {
    my $self            = shift;

    my $params          = assert_profile(shift || {})->valid;
    my $interface       = shift;

    my $transaction = $interface->process(
        {
            processor_params        => {
                processor       => '_process_search_natuurlijkpersoon',
                request_params  => $params,
            },
            direction               => 'outgoing',
            external_transaction_id => 'unknown',
            input_data              => Data::Dumper::Dumper($params),
        }
    );

    unless (
        $transaction->processor_params &&
        $transaction->processor_params->{result}
    ) {
        throw(
            'sysin/modules/stuf' . lc($self->stuf_object_type) . '/search_natuurlijkpersoon/no_processed_result',
            'Failure in getting results',
        );
    }

    $interface->result_source->schema->resultset('Logging')->trigger(
        'stuf/search',
        {
            component       => 'transaction',
            component_id    => $transaction->id,
            data => {
                via_form => ($interface->result_source->schema->current_user ? 0 : 1),
                params  => (
                    (
                        $transaction->processor_params->{result} &&
                        $transaction->processor_params->{result}->[0]
                    )
                        ? $transaction->processor_params->{result}->[0]
                        : $params
                ),
            }
        }
    );

    return $transaction->processor_params->{result};
}

###
### Processor for the search_natuurlijkpersoon call
###

sub _process_search_natuurlijkpersoon {
    my $self            = shift;
    my $record          = shift;
    my $transaction     = $self->process_stash->{transaction};

    $self->process_stash->{error_fatal} = 1;

    my $processor_params = $transaction->get_processor_params();
    my $params           = $processor_params->{request_params};

    my $call_options    = $self->_load_call_options_from_config(
        $transaction->interface_id,
        {
            via_gbav    => $params->{via_gbav},
            reference   => $transaction->id,
            datetime    => DateTime->now()
        }
    );

    ### Integrale zoekvraag when gbav is selected and spoof mode is disabled (hence: blessed)
    if ($call_options->{supplier} eq 'pink' && $params->{via_gbav} && blessed($call_options->{dispatch}->{transport})) {
        $call_options->{dispatch}->{transport}->userAgent->default_header('SOAPAction', 'http://www.egem.nl/StUF/sector/bg/0204/beantwoordSynchroneVraagIntegraal');
    }

    my ($rv, $trace)    = $self->stuf0204->search_natuurlijkpersoon(
        $params,
        $call_options
    );

    $record->preview_string(
        ($params->{geslachtsnaam}
            ? 'Naam: ' . $params->{geslachtsnaam} . ' / '
            : ''
        ) .
        ($params->{burgerservicenummer}
            ? 'BSN: ' . $params->{burgerservicenummer}
            : ''
        )
    );

    if (!$rv) {
        throw(
            'sysin/modules/stuf' . lc($self->stuf_object_type) . '/search_natuurlijkpersoon/no_results',
            'Failure in calling: ' . $call_options->{dispatch}->{endpoint} . (
                ($trace && $trace->response) ? ', response: ' . $trace->response->content
                : ''
            ),
            {
                fatal => 1
            }
        );
    }

    $record->input($trace->request->content);

    $rv                 = $self->_prepare_identification(
        $rv,
        {
            via_gbav    => $params->{via_gbav}
        }
    );

    $transaction->processor_params(
        {
            %{ $transaction->processor_params },
            result  => $rv,
        }
    );

    $record->output($trace->response->content);
}

=head2 import_natuurlijkpersoon

Arguments: \%params, $INTERFACE_ROW

Return value: L<Zaaksysteem::Schema::NatuurlijkPersoon> row

    $np_row = $interface->process_trigger('import', \%params)

Imports a user in zaaksysteem, and will set a kennisgeving to the SBUS engine.

Use one of the results received from the search call:
L<Zaaksysteem::Backend::Sysin::Modules::STUFPRS#search_natuurlijkpersoon> as \%params.

See the profile
L<Zaaksysteem::Profiles#PROFILE_NATUURLIJK_PERSOON> for allowed parameters (every
optional AND required parameter in this profile is optional with this call)

B<Params>

See the profile
L<Zaaksysteem::Profiles#PROFILE_NATUURLIJK_PERSOON> for allowed parameters (every
optional AND required parameter in this profile is optional with this call). The following
parameters are required besides this list.

=over 4

=item external_transaction_id

    $id = 'sleutel-172'
    # or
    $id = 'bsn-987654321'

A unique id for identifiing a person in a SBUS engine. Always use the 'sleutel' construction

=item force

When setting this boolean value to true, it will not do a check in zaaksysteem for an existing

=back

=cut


define_profile 'import_natuurlijkpersoon' => (
    required            => [
        @{ PROFILE_NATUURLIJK_PERSOON()->{required} },
    ],
    optional            => [
        @{ PROFILE_NATUURLIJK_PERSOON()->{optional} },
        qw/
            via_gbav
            external_id
            force
        /
    ],
    constraint_methods  => PROFILE_NATUURLIJK_PERSOON()->{constraint_methods},
    require_some        => {
        external_id_or_gbav => [1, qw/via_gbav external_id/],
    },
    dependencies        => {
        via_gbav            => [qw/burgerservicenummer/]
    }
);

sub import_natuurlijkpersoon {
    my $self            = shift;
    my $rawparams       = shift || {};

    my $params          = assert_profile($rawparams)->valid;
    my $interface       = shift;

    unless ($params->{force}) {
        ### Check for existing entry in natuurlijk_persoon table
        my ($np) = $interface->result_source->schema->resultset('NatuurlijkPersoon')->search(
            {
                authenticated       => 1,
                burgerservicenummer => sprintf("%09d", $params->{burgerservicenummer}),
                deleted_on          => undef,  
            }
        )->first;

        return $np if $np;
    }

    my $transaction     = $interface->process(
        {
            processor_params        => {
                processor           => '_process_import_natuurlijkpersoon',
                request_params      => $params,
            },
            external_transaction_id => 'unknown',
            input_data              => Data::Dumper::Dumper($params),
            direction               => 'outgoing',
        }
    );

    unless (
        $transaction->processor_params &&
        $transaction->processor_params->{result}
    ) {
        throw(
            'sysin/modules/stuf' . lc($self->stuf_object_type) . '/import_natuurlijkpersoon/no_processed_result',
            'Failure in getting results: See transaction id: ' . $transaction->id,
        );
    }

    my $rv              = $transaction->processor_params->{result};
    my $subscription_id = $transaction->processor_params->{subscription_id};

    if ($params->{via_gbav}) {
        my $module_cfg      = $self->get_config_from_config_iface(
            $transaction->interface_id
        );

        ### Lets make sure we spoof a reply
        if ($module_cfg->{mk_spoof}) {
            my $stuf0204            = Zaaksysteem::XML::Compile
                                    ->xml_compile
                                    ->add_class('Zaaksysteem::StUF::0204::Instance')
                                    ->stuf0204;

            eval {
                $transaction->interface_id->process(
                    {
                        external_transaction_id => 'SPOOF-ANSWER-' . $transaction->id,
                        input_data              => $stuf0204->spoof_gba_v_kennisgeving(
                            {
                                %$params,
                                subscription_id => $subscription_id,
                            },
                            ### Hardcodes key_type, because it is pink
                            { key_type => 'gegevens', local_id => $subscription_id, local_table => 'NatuurlijkPersoon' }
                        ),
                        direction               => 'incoming',
                    }
                );
            };

            if ($@) {
                warn('Something went wrong in spoof channel: ' . $@);
            }
        }
    }

    $rv     = {
        betrokkene_identifier => 'betrokkene-natuurlijk_persoon-' . $rv
    };

    $interface->result_source->schema->resultset('Logging')->trigger(
        'stuf/import',
        {
            component       => 'transaction',
            component_id    => $transaction->id,
            data => {
                via_form => ($interface->result_source->schema->current_user ? 0 : 1),
                params  => $params,
            }
        }
    );

    return $rv;
}

###
### Processor for the import_natuurlijkpersoon call
###

sub _process_import_natuurlijkpersoon {
    my $self                = shift;
    my $record              = shift;

    my $transaction         = $self->process_stash->{transaction};
    my $interface           = $transaction->interface_id;

    my $processor_params    = $transaction->get_processor_params();
    my $params              = $processor_params->{request_params};

    ### First, generate this person into our database
    my $entry               = $self->_import_natuurlijkpersoon_in_zs($params, $transaction);

    $record->preview_string(
        $entry->voornamen . ' ' . $entry->achternaam
    );

    my $subscription        = $self->_notify_sbus_for_subscription($entry, $record, $transaction);

    $transaction->processor_params(
        {
            %{ $transaction->processor_params },
            result          => $entry->id,
            subscription_id => $subscription->id,
        }
    );
}

sub _notify_sbus_for_subscription {
    my $self                            = shift;
    my ($entry, $record, $transaction)  = @_;

    my $processor_params    = $transaction->get_processor_params();
    my $params              = $processor_params->{request_params};

    my $call_options        = $self->_load_call_options_from_config(
        $transaction->interface_id,
        {
            reference           => $transaction->id,
            datetime            => DateTime->now(),
            follow_subscription => 1,
            calltype            => 'async',
            set_subscription    => 1,
        }
    );

    my %set_natuurlijkpersoon_params = (
        subscription_id         => $entry->id,
        $params->{via_gbav}
            ? (map({ $_ => $params->{$_} } grep({ $params->{$_} } qw/burgerservicenummer a_nummer/)))
            : (external_id          => $params->{external_id}),
    );

    my $subscription                = $self->_set_subscription(
        {
            transaction     => $transaction,
            entry           => $entry,
            external_id     => ($params->{external_id} || undef),
        }
    );

    my ($rv, $trace)        = $self->stuf0204->set_natuurlijkpersoon(
        {
            %set_natuurlijkpersoon_params,
            subscription_id         => ($subscription->id || undef),
        },
        {
            %{ $call_options },
            follow_subscription     => 1, # Set kennisgeving
        }
    );

    $record->input($trace->request->content);

    if (!$rv) {
        throw(
            'sysin/modules/stuf' . lc($self->stuf_object_type) . '/search_natuurlijkpersoon/no_results',
            'Failure in calling: ' . $call_options->{dispatch}->{endpoint} . (
                ($trace && $trace->response) ? ', response: ' . $trace->response->content
                : ''
            ),
            {
                fatal => 1
            }
        );
    }

    $record->output($trace->response->content);

    ### GBA-V Handling
    if ($params->{via_gbav}) {
        my $valid_bsn       = sprintf("%09d", $params->{burgerservicenummer});
        my $transaction     = $transaction->interface_id->process(
            {
                processor_params        => {
                    processor   => '_process_set_pink_voa',
                    params      => {
                        burgerservicenummer => $valid_bsn,
                    }
                },
                external_transaction_id => 'unknown',
                input_data              => 'Burgerservicenummer: ' . $valid_bsn,
                direction               => 'outgoing',
            }
        );
    }

    return $subscription;
}


define_profile "_set_subscription"  => (
    required    => [qw/entry transaction/],
    optional    => ['external_id'],
);

sub _set_subscription {
    my $self                = shift;
    my $params              = assert_profile(shift || {})->valid;

    my $entry               = $params->{entry};
    my $sleutel             = $params->{external_id};
    my $transaction         = $params->{transaction};

    my %create_params       = (
        local_id        => $entry->id,
        interface_id    => $transaction->interface_id->id,
        local_table     => $entry->result_source->source_name,
        external_id     => ($sleutel || 'IN_PROGRESS'),
        object_preview  => $entry->geslachtsnaam . ' [BSN: ' . $entry->burgerservicenummer . ']',
    );

    return $entry->result_source->schema->resultset('ObjectSubscription')->create(\%create_params);
}

sub _process_set_pink_voa {
    my $self                = shift;
    my $record              = shift;

    my $transaction         = $self->process_stash->{transaction};
    my $interface           = $transaction->interface_id;

    my $processor_params    = $transaction->get_processor_params();
    my $params              = $processor_params->{params};

    my $call_options        = $self->_load_call_options_from_config(
        $transaction->interface_id,
        {
            reference           => $transaction->id,
            datetime            => DateTime->now(),
            follow_subscription => 1,
            calltype            => 'pink_gbav'
        }
    );

    ### GBA-V Handling
    my ($rv, $trace)        = $self->stuf0204->set_pink_voa(
        $params,
        $call_options
    );

    if (!$rv) {
        throw(
            'sysin/modules/stuf' . lc($self->stuf_object_type) . '/set_pink_voa/invalid_response',
            'Failure in calling: ' . $call_options->{dispatch}->{endpoint} . (
                ($trace && $trace->response) ? ', response: ' . $trace->response->content
                : ''
            ),
            {
                fatal => 1
            }
        );
    }

    $transaction->external_transaction_id($rv->{referentie});

    $record->input($trace->request->content);
    $record->output($trace->response->content);
}

=head2 _import_natuurlijkpersoon_in_zs

Arguments: \%params, $TRANSACTION_ROW

Return value: L<Zaaksysteem::Schema::NatuurlijkPersoon>

    my $np_entry = $module->_import_natuurlijkpersoon_in_zs(
        {
          'a_nummer' => 4919938078,
          'aanduiding_naamgebruik' => undef,
          'burgerlijke_staat' => undef,
          'burgerservicenummer' => 987654321,
          'datum_overlijden' => undef,
          'external_transaction_id' => 'sleutel-172',
          'functie_adres' => 'W',
          'geboortedatum' => '19750815',
          'gegevensbeheer_id' => '172',
          'geslachtsaanduiding' => 'M',
          'geslachtsnaam' => 'Muler',
          'huisnummer' => 68,
          'huisnummertoevoeging' => 'MMXI',
          'indicatie_geheim' => undef,
          'postcode' => '8387KD',
          'straatnaam' => 'Mxyvwemaqpddplhmg',
          'verzendend_id' => undef,
          'voorletters' => 'XF',
          'voornamen' => 'Sam',
          'voorvoegsel' => undef,
          'woonplaats' => 'Lvxnrpuiy'
        },
        $TRANSACTION_ROW
    );

Imports a natuurlijk persoon into ZS.

=cut

sub _import_natuurlijkpersoon_in_zs {
    my $self                        = shift;
    my ($params, $transaction)      = @_;

    my $entry = $transaction
            ->result_source
            ->schema
            ->resultset('NatuurlijkPersoon')
            ->create_natuurlijk_persoon(
                $params,
                {
                    authenticated => 'gba'
                }
            );

    my $mutation_record     = Zaaksysteem::Backend::Sysin::Transaction::Mutation
                            ->new(
                                table       => 'NatuurlijkPersoon',
                                table_id    => $entry->id,
                                create      => 1
                            );

    $mutation_record->from_dbix($entry);

    push(
        @{ $self->process_stash->{row}->{mutations} },
        $mutation_record
    );

    return $entry;
}


=head1 INTERNAL METHODS

=head2 _prepare_identification

Arguments: \%params

Return value: { %params, external_transaction_id => 'sleutel-234234'}

=cut

sub _prepare_identification {
    my $self            = shift;
    my $rv              = shift;
    my $opts            = shift;

    for my $np (@{ $rv }) {
        if (!$opts->{via_gbav} && !$np->{external_id}) {
            throw(
                '/modules/stufprs/prepare_identification/no_sleutel',
                'GBA-V Mode is unset, but no sleutel is retrieved'
            );
        }

        if ($opts->{via_gbav}) {
            $np->{via_gbav} = 1;
        }
    }

    return $rv;
}


=head1 INTERNAL METHODDS

=head2 $module->_is_verhuisd($object)

Return value: $TRUE_OR_FALSE

Returns whether this person has moved to another city or not. In case of a 0204 xml message,
this would mean the system will check if gemeente_code is different from our set gemeente_code
in config.

=cut

sub _binnen_gemeente {
    my $self                                = shift;
    my ($record, $object)                   = @_;

    my $gemeentecode = $self->get_config_from_config_iface($record->transaction_id->interface_id)->{gemeentecode};

    return $object->binnen_gemeente($gemeentecode);
}

# sub _handle_transaction_error {
#     my $self                = shift;
#     my ($transaction)       = @_;

#     my $message             = $self->next::method(@_);

#     # my $xml                 = $self->stuf0204->generate_foutbericht(

#     #     {
#     #         {
#     #             messagetype             => 'Lk01',
#     #             entitytype              => 'PRS',
#     #             reference               => 'ZS0000229154',
#     #             datetime                => '2014030209011458',
#     #             mutation_type           => 'create',
#     #             follow_subscription     => 1,
#     #     entitytype              => 'PRS',
#     #         messagetype             => 'create',
#     #         mutation_type           => 'Lv01',
#     #         sender                  => 'ZSNL',
#     #         receiver                => $module_cfg->{mk_ontvanger},
#     #         reference               => $transaction->id,
#     #         datetime                => $options->{datetime}->strftime('%Y%m%d%H%M%S00'),
#     #     }
#     # )''

#     return $message;
# }

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Install>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 INTERFACE_CONFIG_FIELDS

TODO: Fix the POD

=cut

=head2 INTERFACE_ID

TODO: Fix the POD

=cut

=head2 MODULE_SETTINGS

TODO: Fix the POD

=cut

=head2 PROFILE_NATUURLIJK_PERSOON

TODO: Fix the POD

=cut

