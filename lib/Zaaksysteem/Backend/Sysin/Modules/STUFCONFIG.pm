package Zaaksysteem::Backend::Sysin::Modules::STUFCONFIG;

use Moose;

use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::ZAPI::Form::Field;

use JSON;
use Crypt::OpenSSL::X509;

use Zaaksysteem::Exception;
use Zaaksysteem::Constants qw/RGBZ_GEMEENTECODES/;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::StUF
    Zaaksysteem::Backend::Sysin::Modules::Roles::Tests
/;

###
### Interface Properties
###
### Below a list of interface properties, see
### L<Zaaksysteem::Backend::Sysin::Modules> for details.

use constant INTERFACE_ID               => 'stufconfig';

use constant INTERFACE_CONFIG_FIELDS    => [
    # Zaaksysteem::ZAPI::Form::Field->new(
    #     name        => 'interface_zs_cert',
    #     type        => 'file',
    #     label       => 'Public key (zaaksysteem)',
    #     required    => 0,
    # ),
    # Zaaksysteem::ZAPI::Form::Field->new(
    #     name        => 'interface_zs_key',
    #     type        => 'file',
    #     label       => 'Private key (zaaksysteem)',
    #     required    => 0,
    # ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_stuf_supplier',
        type        => 'select',
        label       => 'Aanbieder van ServiceBus',
        data        => {
            options     => [
                {
                    value    => 'centric',
                    label    => 'Centric (0301)',
                },
                {
                    value    => 'pink',
                    label    => 'Pink (0204)',
                },
                {
                    value    => 'vicrea',
                    label    => 'Vicrea (0204)',
                }
            ],
        },
        required    => 1,
        description => 'Elke leverancier heeft haar eigen manier van interpretatie '
            .'van het StUF protocol. Om deze smaken uit elkaar te houden, vragen wij '
            .'u een keuze te maken voor een leverancier.'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_synchronization_type',
        type        => 'select',
        label       => 'Type koppeling',
        data        => {
            options     => [
                {
                    value    => 'mutation',
                    label    => 'Mutatieberichten',
                },
                {
                    value    => 'question',
                    label    => 'Vraagberichten',
                },
                {
                    value    => 'hybrid',
                    label    => 'Hybride (vraag + mutatieberichten)',
                },
            ],
        },
        required    => 1,
        description => 'De werking van de koppeling is afhankelijk van het type, '
            .'bij enkel vraagberichten of in een hybride functie, zal een koppeling personen '
            .'bij verhuizing niet verwijderen, terwijl dit bij mutatieberichten wel het geval is. Uiteraard '
            .'zullen personen niet bevraagd kunnen worden bij een keuze voor mutatieberichten.'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_gemeentecode',
        type        => 'select',
        label       => 'Gemeente code',
        required    => 1,
        data        => {
            options     => [
                sort({ $a->{label} cmp $b->{label} } map({ { value => $_, label => RGBZ_GEMEENTECODES()->{$_} } } keys %{ RGBZ_GEMEENTECODES() })),
                { value => 0, label => 'Ongedefinieerd' },
                { value => 9999, label => 'Test' }
            ],
        },
        description => 'Selecteer de gemeente waarop deze koppeling van toepassing is',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_mk_cert',
        type        => 'file',
        label       => 'Public key (Makelaar)',
        #required    => 1,
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_mk_sender',
        type        => 'text',
        label       => 'Naam verzender (e.g. ZSNL)',
        default     => 'ZSNL',
        required    => 1,
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_mk_ontvanger',
        type        => 'text',
        label       => 'Naam ontvanger (e.g. CGM)',
        required    => 1,
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_gbav_applicatie',
        type        => 'text',
        label       => 'GBA-V Ontvanger (e.g. CML)',
        description => 'Wanneer u gebruik maakt van GBA-V, geef hier de ontvanger voor het StUF bericht op.'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_mk_ontvanger_afnemer',
        type        => 'text',
        label       => 'Naam ontvanger afnemer (e.g. CMODIS)',
        required    => 1,
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_mk_sync_url',
        type        => 'text',
        label       => 'Makelaar url (SYNC)',
        description => 'Voorbeeld: http://MAKELAAR_URL/ListenerStUFBG204Synchroon)',
        required    => 0,
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_mk_async_url',
        type        => 'text',
        label       => 'Makelaar url (ASYNC)',
        description => 'Voorbeeld: http://MAKELAAR_URL/ListenerStUFBG204Asynchroon)',
        required    => 0,
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_gbav_pink_url',
        type        => 'text',
        label       => '(Pink only) GBA-V VOA webservice',
        description => 'Wanneer u gebruik maakt van Pink, dient u bij GBA-V verkeer een'
            .' webservice op te geven waar VOA indicaties geplaatst kunnen worden op GBA-V resultaten. '
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_gbav_search',
        type        => 'checkbox',
        label       => 'Vraagberichten naar GBA-V toestaan',
        description => 'Wanneer zaaksysteem.nl is gekoppeld met een makelaar voor'
            .' verkeer naar de makelaar toe, en zoeken via de GBA-V gewenst is. '
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_search_extern_webform_only',
        type        => 'checkbox',
        label       => 'Vraagberichten alleen toestaan via webformulier',
        description => 'Voorkom zoeken in de makelaar door medewerkers en '
            .' zoek enkel burgers geauthenticeerd via DigiD. '
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_mk_spoof',
        type        => 'checkbox',
        label       => 'Laat zaaksysteem een makelaar simuleren',
        description => 'Met onze zogenaamde "Spoof"-modus kan een servicebus gesimuleerd worden,'
            .' zodat getest kan worden of verschillende scenario\'s van de koppeling werken.'
            .' Voor een overzicht van scenario\'s: wiki.zaaksysteem.nl/TODO'
            .' LET OP: Niet aanzetten op een live omgeving, de koppeling stopt hiermee.'
    ),
];

use constant MODULE_SETTINGS            => {
    name                            => INTERFACE_ID,
    label                           => 'StUF Koppeling Configuratie',
    interface_config                => INTERFACE_CONFIG_FIELDS,
    direction                       => 'incoming',
    manual_type                     => ['text'],
    is_multiple                     => 0,
    is_manual                       => 1,
    retry_on_error                  => 1,
    allow_multiple_configurations   => 0,
    is_casetype_interface           => 0,
    test_interface                  => 1,
    test_definition                 => {
        description => qq{
            Om te controleren of de applicatie goed geconfigureerd is, kunt u
            hieronder een aantal tests uitvoeren. Hiermee controleert u uw verbinding
            met uw servicebus.
        },
        tests       => [
            {
                id          => 1,
                label       => 'Test verbinding',
                name        => 'connection_test',
                method      => 'stuf_test_connection',
                description => 'Test verbinding naar opgegeven Service Bus'
            }
        ],
    },
    # trigger_definition  => {
    #     disable_subscription   => {
    #         method  => 'disable_subscription',
    #         #update  => 1,
    #     },
    # },
};


###
### BUILDARGS
###

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig( %{ MODULE_SETTINGS() } );
};


=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::STUFCONFIG - StUF configuration

=head1 SYNOPSIS

=head2 stuf_test_connection

=cut

sub stuf_test_connection {
    my $self                        = shift;
    my $interface                   = shift;

    ### Connect with our StUF ServiceBus server.
    $self->test_host_port(
        $interface->get_interface_config->{mk_async_url}
    ) if $interface->get_interface_config->{mk_async_url};

    $self->test_host_port(
        $interface->get_interface_config->{mk_sync_url}
    ) if $interface->get_interface_config->{mk_sync_url};

    ### Success
    return 1;
}

=head2 can_search_sbus

Arguments: $ROW_CONFIG_INTERFACE

Return value: $ROW_PRS_OR_NPS_INTERFACE or FALSE

    my $prs_or_nps_interface = $stufconfig_module->can_search_sbus($stufconfig);

Returns the Natuurlijk Persoon interface according to the settings of the stufconfig
module.

Will return a NPS or PRS interface, Depending on the stuf_supplier (pink,vicrea,centric) and
the setting: interface_sbus_search and interface_bidirectional

=cut


sub can_search_sbus {
    my $self                        = shift;
    my $config_interface            = shift;

    throw(
        'modules/stufconfig/invalid_config_interface',
        'Please supply the config interface as first parameter'
    ) unless ($config_interface && $config_interface->module eq 'stufconfig');

    return unless $config_interface->active;

    my $prsnps_interface            = $self->get_natuurlijkpersoon_interface($config_interface);

    return unless ($prsnps_interface && $prsnps_interface->active);

    if (
        $config_interface->get_interface_config->{synchronization_type} =~ /^(?:question|hybrid)$/
    ) {
        return $prsnps_interface;
    }

    return;

    #$interface->result_source
}

=head2 can_connect_to_other_sbus

Arguments: $ROW_CONFIG_INTERFACE

Return value: $ROW_PRS_OR_NPS_INTERFACE or FALSE

    my $prs_or_nps_interface = $stufconfig_module->can_search_sbus($stufconfig);

Returns the Natuurlijk Persoon interface according to the settings of the stufconfig
module.

Will return a NPS or PRS interface, Depending on the stuf_supplier (pink,vicrea,centric) and
if the C<synchronization_type> equals "hybrid" or "question"

=cut


sub can_connect_to_other_sbus {
    my $self                        = shift;
    my $config_interface            = shift;

    throw(
        'modules/stufconfig/invalid_config_interface',
        'Please supply the config interface as first parameter'
    ) unless ($config_interface && $config_interface->module eq 'stufconfig');

    return unless $config_interface->active;

    my $prsnps_interface            = $self->get_natuurlijkpersoon_interface($config_interface);

    return unless ($prsnps_interface && $prsnps_interface->active);


    if (
        $config_interface->get_interface_config->{synchronization_type} &&
        $config_interface->get_interface_config->{synchronization_type} =~ /^(?:question|hybrid)$/
    ) {
        return $prsnps_interface;
    }

    return;

    #$interface->result_source
}

=head2 can_search_gbav

Arguments: $ROW_CONFIG_INTERFACE

Return value: $ROW_PRS_OR_NPS_INTERFACE or FALSE

    my $prs_or_nps_interface = $stufconfig_module->can_search_sbus($stufconfig);

Returns the Natuurlijk Persoon interface according to the settings of the stufconfig
module. Only when we are allowed to search the GBAV

Will return a NPS or PRS interface, Depending on the stuf_supplier (pink,vicrea,centric) and
the setting: interface_gbav_search and interface_bidirectional

=cut


sub can_search_gbav {
    my $self                        = shift;
    my $config_interface            = shift;

    throw(
        'modules/stufconfig/invalid_config_interface',
        'Please supply the config interface as first parameter'
    ) unless ($config_interface && $config_interface->module eq 'stufconfig');

    return unless $config_interface->active;

    my $prsnps_interface            = $self->get_natuurlijkpersoon_interface($config_interface);

    return unless ($prsnps_interface && $prsnps_interface->active);


    if (
        $config_interface->get_interface_config->{synchronization_type} =~ /^(?:question|hybrid)$/ &&
        $config_interface->get_interface_config->{gbav_search}
    ) {
        return $prsnps_interface;
    }

    return;

    #$interface->result_source
}

=head2 get_natuurlijkpersoon_interface

Arguments: $ROW_CONFIG_INTERFACE

Return value: $ROW_PRS_OR_NPS_INTERFACE or FALSE

    my $prs_or_nps_interface = $stufconfig_module->can_search_sbus($stufconfig);

Returns the active Natuurlijk Persoon interface according to the settings of the stufconfig
module "stuf_supplier".

=cut

sub get_natuurlijkpersoon_interface {
    my $self                        = shift;
    my $config_interface            = shift;

    throw(
        'modules/stufconfig/invalid_config_interface',
        'Please supply the config interface as first parameter'
    ) unless ($config_interface && $config_interface->module eq 'stufconfig');

    my $type    = 'prs';

    if ($config_interface->get_interface_config->{stuf_supplier} && lc($config_interface->get_interface_config->{stuf_supplier}) eq 'centric') {
        $type   = 'nps';
    }

    return $config_interface->result_source->schema->resultset('Interface')->search_active(
        {
            module => 'stuf' . $type
        }
    )->first;
}

=head2 get_primary_key

Arguments: $ROW_CONFIG_INTERFACE

Return value: $STRING_SLEUTEL_IDENTIFIER

    print $module_cfg->get_primary_key;

    # Prints either:
    #   sleutelGegevensbeheer
    # OR
    #   sleutelVerzendend

Depending on the manufacturer..pink or others

=cut

sub get_primary_key {
    my $self                        = shift;
    my $config_interface            = shift;

    if ($config_interface->get_interface_config->{stuf_supplier} && lc($config_interface->get_interface_config->{stuf_supplier}) eq 'centric') {
        return 'sleutelVerzendend';
    }

    return 'sleutelGegevensbeheer';
}

sub verify_ssl_hash {
    my $self                        = shift;
    my $config_interface            = shift;
    my $given_hash                  = shift;

    my $filestore                   = $config_interface->result_source->schema->resultset('Filestore')->find(
        $config_interface->jpath('$.mk_cert[0].id')
    );

    return unless $filestore;

    my $openssl_object              = Crypt::OpenSSL::X509
                                    ->new_from_file($filestore->get_path);


    return 1 if $openssl_object->hash eq $given_hash;
    return;
}

1;

__END__

=head1 DESCRIPTION

STUF Configuration for StUF related queries

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 INTERFACE_CONFIG_FIELDS

TODO: Fix the POD

=cut

=head2 INTERFACE_ID

TODO: Fix the POD

=cut

=head2 MODULE_SETTINGS

TODO: Fix the POD

=cut

=head2 verify_ssl_hash

TODO: Fix the POD

=cut

