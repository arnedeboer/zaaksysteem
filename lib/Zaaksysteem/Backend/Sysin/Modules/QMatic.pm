package Zaaksysteem::Backend::Sysin::Modules::QMatic;

use Moose;
use Time::HiRes qw/tv_interval gettimeofday/;

use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::ZAPI::Form::Field;

use XML::Compile::WSDL11;
use XML::Compile::SOAP11;
use XML::Compile::Transport::SOAPHTTP;

use Zaaksysteem::Constants;

use Zaaksysteem::Exception;
use Data::Dumper;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
    Zaaksysteem::Backend::Sysin::Modules::Roles::Tests
/;


use constant INTERFACE_ID               => 'qmatic';

use constant INTERFACE_CONFIG_FIELDS    => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_endpoint',
        type        => 'text',
        label       => 'Url QMatic',
        required    => 1,
        description => 'QMatic SOAP server'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_spoofmode',
        type        => 'checkbox',
        label       => 'Spoofmode',
        required    => 1,
        description => 'Zet de spoofmode aan, alle afspraakkenmerken krijgen nu fake data vanuit Zaaksysteem. NIET aanzetten op productieomgevingen',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_spoofdays',
        type        => 'text',
        label       => 'Spoofmode: Available dates',
        default     => '7',
        required    => 0,
        description => 'Definieer het aantal dagen wat getoond moet worden indien spoofmode aan staat',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_spoofslots',
        type        => 'text',
        default     => '20',
        label       => 'Spoofmode: Available slots',
        required    => 0,
        description => 'Definieer het aantal tijdsloten wat getoond moet worden indien spoofmode aan staat',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_spoofinterval',
        type        => 'text',
        default     => '15',
        label       => 'Spoofmode: Interval',
        required    => 0,
        description => 'Definieer de tijdsinterval (in minuten) wat getoond moet worden indien spoofmode aan staat',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_offset',
        type        => 'text',
        label       => 'QMatic offset',
        required    => 0,
        description => "Door een bug in QMatic software waardoor ze lokale tijd als UTC tijd aanduiden is het mogelijk dat afspraken niet op het juiste tijdstip worden ingepland. Deze settings zorgt ervoor dat de juiste offset gebruikt kan worden waardoor er geen codewijzigingen hoeven plaats te vinden bij overgangen van zomer naar wintertijd. Als de offset niet gedefinieerd is gaat Zaaksysteem ervanuit dat de tijd zoals QMatic deze doorstuurt UTC is, of een correcte tijdzone meegeeft. De offset moet worden aangegeven als: +uumm of -uumm. Bijvoorbeeld: +0100, +0200, -0100"
    ),
];

use constant MODULE_SETTINGS            => {
    name                            => INTERFACE_ID,
    label                           => 'QMatic',
    interface_config                => INTERFACE_CONFIG_FIELDS,
    direction                       => 'incoming',
    manual_type                     => ['text'],
    is_multiple                     => 0,
    is_manual                       => 1,
    retry_on_error                  => 0,
    allow_multiple_configurations   => 0,
    is_casetype_interface           => 0,
    has_attributes                  => 1,
    trigger_definition  => {
        getProduct => {
            method => 'getProduct',
            update => 1
        },
        getAvailableProducts => {
            method => 'getAvailableProducts',
            update => 1
        },
        getAvailableAppointmentDays => {
            method => 'getAvailableAppointmentDays',
            update => 1
        },
        getAvailableAppointmentTimes => {
            method => 'getAvailableAppointmentTimes',
            update => 1
        },
        deleteAppointment => {
            method => 'deleteAppointment',
            update => 1
        },
        bookAppointment => {
            method => 'bookAppointment',
            update => 1
        },
    },
    case_hooks                  => [],
    test_interface                  => 1,
    test_definition                 => {
        description => qq{
            Om te controleren of de applicatie goed geconfigureerd is, kunt u
            hieronder een aantal tests uitvoeren. Hiermee controleert u de verbinding
            van uw profiel.
        },
        tests       => [
            {
                id          => 1,
                label       => 'Test verbinding',
                name        => 'connection_test',
                method      => 'qmatic_test_connection',
                description => 'Test verbinding naar profiel URL'
            }
        ],
    },
};

has response => (is => 'rw', default => sub { undef });

sub dynamic_attribute_list {
    my ($self, $interface) = @_;

    my $products = $self->getAvailableProducts({}, $interface);

    throw ('sysin/qmatic/no_products', 'Kon productenlijst niet ophalen')
        unless $products && @$products;

    return [ map {
        {
            attribute_type => 'magic_string',
            all_casetypes  => 1,
            checked        => 1,
            linkId         => $_->{linkId},
            appointLength  => $_->{appointLength},
            external_name  => $_->{name} . ' (' . $_->{appointLength} . ' minuten)'
        }
    } @$products ];
}


around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig( %{ MODULE_SETTINGS() } );
};


sub qmatic_test_connection {
    my ($self, $interface) = @_;

    $self->test_host_port($interface->get_interface_config->{endpoint});
}


=head2 getProduct

Reverse lookup of QMatic product using the magic_string

This mapping is manually configured in the interface

=cut

sub getProduct {
    my ($self, $params, $interface) = @_;

    my $magic_string = $params->{magic_string};
    my $mapping = $interface->get_interface_config->{attribute_mapping};

    foreach my $item (@$mapping) {
        my $object_id = $item->{internal_name}->{searchable_object_id};

        if ($object_id && $object_id eq $magic_string) {
            return $item;
        }
    }
}


sub getAvailableProducts {
    my ($self, $params, $interface) = @_;

    return $self->callSOAP($interface, 'getAvailableProducts', {});
}


sub getAvailableAppointmentDays {
    my ($self, $params, $interface) = @_;

    my $days = $self->callSOAP($interface, 'getAvailableAppointmentDays', {
        productLinkID => $params->{productLinkID}
    });

    return [$self->addTimezones($interface, @$days)];
}


sub getAvailableAppointmentTimes {
    my ($self, $params, $interface) = @_;

    my $times = $self->callSOAP($interface, 'getAvailableAppointmentTimes', {
        productLinkID => $params->{productLinkID},
        appDate => $self->removeTimezone($interface, $params->{appDate})
    });

    return [$self->addTimezones($interface, @$times)];
}


sub deleteAppointment {
    my ($self, $params, $interface) = @_;

    my $result = $self->callSOAP($interface, 'deleteAppointment', {
        appointmentId => $params->{appointmentId}
    });

    # zapi likes arrays
    return [$result];
}


sub bookAppointment {
    my ($self, $params, $interface) = @_;

    if ($params->{previous_appointment_id}) {

        my $deleted_result = $self->deleteAppointment({
            appointmentId => $params->{previous_appointment_id},
        }, $interface);

        die "error deleting appointment" unless $deleted_result->[0]->{appointmentStatus} eq '1';
    }

    my $aanvrager = $self->retrieve_aanvrager($interface, $params->{aanvrager});

    my $appointment_info = {
        productLinkID => $params->{productLinkID},
        appDate => $self->removeTimezone($interface, $params->{appDate}),
        appTime => $self->removeTimezone($interface, $params->{appTime}),
        name => $aanvrager->naam,
        address => $aanvrager->street_address,
        town => $aanvrager->city,
        state => '',
        zipCode => $aanvrager->postal_code,
        country => 'Nederland',
        phone => $aanvrager->telefoonnummer || '',
        phone2 => $aanvrager->mobiel || '',
        email => $aanvrager->email || '',
        piNo => $aanvrager->btype eq 'bedrijf' ? $aanvrager->dossiernummer : $aanvrager->burgerservicenummer,
        accountNo => '',
        cardNum => '',
        title => '',
        born => '', #$aanvrager->btype eq 'natuurlijk_persoon' ? $aanvrager->geboortedatum : '-',
        vipLevel => '0',
        gender => $aanvrager->btype eq 'natuurlijk_persoon' ? $aanvrager->geslachtsnaam : '-',
        social => '',
        family => '',
        customerClass => '',
        comments => ''
    };

    my $result =  $self->callSOAP($interface, 'bookAppointment', $appointment_info);

    # zapi likes arrays.
    return [$result];
}

sub callSOAP {
    my ($self, $interface, $action, $params) = @_;

    if ($interface->get_interface_config->{spoofmode}) {
        return $self->spoof($interface, $action, $params);
    }

    $interface->process({
        external_transaction_id => 'unknown',
        input_data => 'xml',
        processor_params => {
            processor => '_process_qmatic',
            action => $action,
            params => $params,
        },
    });

    return $self->response;
}

=head2 offset

QMatic sends us localtime masked as UTC. This could be problematic when our localtime changes due to CEST to CET and back. The offset configuration in the interface allows us to change the CEST to CET without code changes and/or releases.

=cut

sub offset {
    my ($self, $interface) = @_;
    my $offset = $interface->get_interface_config->{offset};
    if (defined $offset && $offset =~ /^[\-\+]\d{4}$/) {
        return $offset;
    }
    return "+0000";
}

sub _process_qmatic {
    my $self             = shift;
    my $record           = shift;

    my $transaction      = $self->process_stash->{transaction};
    my $processor_params = $transaction->get_processor_params;
    my $interface        = $transaction->interface;

    my $action = $processor_params->{action};
    my $params = $processor_params->{params};

    my $call = $self->compile_call($interface, $action);

    my $t0 = [gettimeofday];

    my ($answer, $trace) = $call->($params);
    $transaction->input_data($record->input($trace->request->content));
    $record->output($trace->response->content);

    $interface->result_source->schema->log->info("QMatic '$action' request took " . tv_interval($t0) . ' ms.');

    my $result = $answer->{parameters}->{$action . 'Return'};

    throw('sysin/qmatic/no_response', 'Geen antwoord ontvangen van QMatic', {fatal => 1})
        unless $result;

    $self->response($result);
}



sub compile_call {
    my ($self, $interface, $action) = @_;

    my $home = $interface->result_source->schema->default_resultset_attributes->{config}->{home}
        or die "need home config";

    my $wsdl = XML::Compile::WSDL11->new($home . '/share/wsdl/qmatic/calendar-service.wsdl') or die $!;

    my $transport = XML::Compile::Transport::SOAPHTTP->new(
        timeout => 10,
        address => $interface->get_interface_config->{endpoint}
    );

    $transport->userAgent->default_header('SOAPAction', '');

    return $wsdl->compileClient(
        operation => $action,
        transport => $transport->compileClient()
    );
}


sub spoof {
    my ($self, $interface, $action, $params) = @_;

    my $available_days  = $interface->get_interface_config->{spoofdays}     || 2;
    my $available_slots = $interface->get_interface_config->{spoofslots}    || 20;
    my $interval        = $interface->get_interface_config->{spoofinterval} || 30;

    my $rs = $interface->result_source->schema->resultset('BibliotheekKenmerken')->search_rs({
        value_type => 'calendar',
        deleted    => undef,
    });

    my @available_products;
    while (my $p = $rs->next) {
        push(@available_products, { linkId => $p->magic_string, name => $p->magic_string, appointLength => 34 });
    }

    my $spoof = {
        deleteAppointment            => { appointmentStatus => '1' },
        bookAppointment              => { appointmentId => time()  },
        getAvailableAppointmentDays  => [ ],
        getAvailableAppointmentTimes => [ ],
        getAvailableProducts         => \@available_products,
    };

    my $now = DateTime->today();
    for (my $d = 0; $d < $available_days; $d++) {
        push(@{$spoof->{getAvailableAppointmentDays}}, $now->strftime('%FT%TZ'));
        my $dt = $now->clone()->add(hours => 8);
        for (my $s = 0; $s < $available_slots; $s++) {
            push(@{$spoof->{getAvailableAppointmentTimes}}, $dt->strftime('%FT%TZ'));
            $dt->add(minutes => $interval);
        }
        $now->add(days => 1);
    }

    return $spoof->{$action};

}
=head2 retrieve_aanvrager

boilerplate to get to the aanvrager object

=cut

sub retrieve_aanvrager {
    my ($self, $interface, $betrokkene_string) = @_;

    my $betrokkene_model = $interface->result_source->schema->resultset('Zaak')->betrokkene_model;

    return $betrokkene_model->get_by_string($betrokkene_string);
}

sub _generate_interface_form_attribute_mapping {
    my $self    = shift;
    my $f       = shift;

    return unless $self->has_attributes;

    my $title = "Kenmerken koppelen";

    my $fieldsets = $f->fieldsets;
    push @$fieldsets,
        Zaaksysteem::ZAPI::Form::FieldSet->new(
            name        => 'fieldset-mapping',
            title       => $title,
            description => 'Uw koppeling maakt gebruik van attributen, welke weer'
                . ' gekoppeld kunnen worden aan velden binnen het zaaksysteem.'
                . ' Druk op de knop "' . $title . '" om deze velden aan elkaar te koppelen'
        );

    my $actions = $fieldsets->[-1]->actions;

    push $actions,
        {
            "name"          => "attribute_mapping",
            "label"         => $title,
            "type"          => "popup",
            "importance"    => "secondary",
            "disabled"      => "!isFormValid()",
            "data"          => {
                "template_url"  => "/html/sysin/links/mapping.html",
                "title"         => $title
            }
        };
}


=head2 addTimezones

The given dates are in UTC format which the frontend automatically
localizes. Since the dates are really in Dutch time, this gives
an undesired output. Here, we make the timezone explicit, causing
the frontend to render the right timezone difference (which is none).
This needs be reversed before communicating back to QMatic.

=cut

sub addTimezones {
    my ($self, $interface, @dates) = @_;

    my $offset = $self->offset($interface);;
    $_ =~ s/Z$/$offset/ for @dates;

    return @dates;
}


=head2 removeTimezone

Because the times are really in Dutch local time, we need to make
the timezone explicit. However, since QMatic expects no timezone
information, this needs to be reverted.

=cut

sub removeTimezone {
    my ($self, $interface, $date) = @_;
    my $offset = $self->offset($interface);;

    $date =~ s/\Q$offset\E$/Z/;

    return $date;
}


1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 INTERFACE_CONFIG_FIELDS

TODO: Fix the POD

=cut

=head2 INTERFACE_ID

TODO: Fix the POD

=cut

=head2 MODULE_SETTINGS

TODO: Fix the POD

=cut

=head2 bookAppointment

TODO: Fix the POD

=cut

=head2 callSOAP

TODO: Fix the POD

=cut

=head2 compile_call

TODO: Fix the POD

=cut

=head2 deleteAppointment

TODO: Fix the POD

=cut

=head2 dynamic_attribute_list

TODO: Fix the POD

=cut

=head2 getAvailableAppointmentDays

TODO: Fix the POD

=cut

=head2 getAvailableAppointmentTimes

TODO: Fix the POD

=cut

=head2 getAvailableProducts

TODO: Fix the POD

=cut

=head2 qmatic_test_connection

TODO: Fix the POD

=cut

=head2 retrieve_aanvrager

TODO: Fix the POD

=cut

=head2 spoof

TODO: Fix the POD

=cut
