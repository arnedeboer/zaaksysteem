package Zaaksysteem::Backend::Sysin::Modules::ControlPanel;
use Moose;

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::ControlPanel - ControlPanel interface

=head1 SYNOPISIS

=cut

use Zaaksysteem::Tools;
use Zaaksysteem::ZAPI::Form::Field;
use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::Types qw(UUID);

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
    Zaaksysteem::Backend::Sysin::Modules::Roles::Tests
    /;

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig(
        name             => 'controlpanel',
        label            => 'Control Panel',
        interface_config => [
            Zaaksysteem::ZAPI::Form::Field->new(
                name        => 'interface_api_key',
                type        => 'text',
                label       => 'API key',
                required    => 1,
                description => 'Interface key voor controlpanel',
            ),
            Zaaksysteem::ZAPI::Form::Field->new(
                name     => 'interface_medewerker',
                type     => 'spot-enlighter',
                label    => 'Medewerker',
                required => 1,
                description =>
                    'Bepaal hier welke medewerkerrechten de externe partij krijgt.',
                data => {
                    restrict    => 'contact/medewerker',
                    placeholder => 'Type uw zoekterm',
                    label       => 'naam',
                }
            ),
        ],
        direction                     => 'incoming',
        manual_type                   => ['text'],
        is_multiple                   => 0,
        is_manual                     => 0,
        retry_on_error                => 0,
        allow_multiple_configurations => 0,
        is_casetype_interface         => 0,
        has_attributes                => 0,
        trigger_definition            => {
            search_object => {
                method => 'search_object',
                update => 0,
            },
            releases => {
                method => 'get_available_releases',
                update => 0
            },
            templates => {
                method => 'get_available_templates',
                update => 0
            },
            clouds => {
                method => 'get_available_clouds',
                update => 0
            },
            loadbalancers => {
                method => 'get_available_loadbalancers',
                update => 0
            },
        },
        case_hooks     => [],
        test_interface => 0,

    );
};

=head1 METHODS

=head2 get_available_releases

Get the available releases. Currently this is hardcoded,
this should be moved to a configurable item, so we can create branches
for a customer where they can test specific things

=cut

sub get_available_releases {
    my ($self, $interface) = @_;

    return {
        master    => "Productie",
        accept    => "Acceptatie",
        hotfix    => "Patch release",
        quarterly => "Testing",
        sprint    => "Development",
    };
}

=head2 get_available_templates

Get the available releases. Currently this is hardcoded,
this should be moved to a configurable item, in the interface config.

=cut

sub get_available_templates {
    my ($self, $interface) = @_;

    return {
        training   => 'Training template',
        production => 'A copy of your production template',
        accept     => 'A copy of your accept template',
        initial    => 'An empty Zaaksysteem with predefined case types',
        clean      => 'An empty Zaaksysteem, no case types',
    };
}

=head2 get_available_clouds

Get the available clouds.

=cut

sub get_available_clouds {
    my ($self, $interface) = @_;

    return {
        gov        => 'Overheidscloud',
        'gov-acc'  => 'Overheidscloud - accept',
        commercial => "Commerciele cloud",
        training   => "Trainigscloud",
        ontwikkel  => "Ontwikkelingscloud",
        dev        => "Mintlab development cloud",
    };
}

=head2 get_available_loadbalancers

Get the available loadbalancers.

=cut

sub get_available_loadbalancers {
    my ($self, $interface) = @_;

    # Because our mapping is cloud => loadbalancers we do this. Might change in
    # the future
    return $self->get_available_clouds($interface);
}

=head2 search_object

Search objects.

=cut

define_profile search_object => (
    required => {
        object_class => 'Str',
        model        => 'Zaaksysteem::Object::Model',
    },
    optional => {
        uuid   => UUID,
    },
);

sub search_object {
    my ($self, $params, $interface) = @_;
    my $dv = assert_profile($params);
    $params =  { %{$dv->valid }, %{$dv->unknown} };

    my $model        = delete $params->{model};
    my $object_class = delete $params->{object_class};

    return [$model->search($object_class, $params)];
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
