package Zaaksysteem::Backend::Sysin::Modules::API;

use Moose;

use Zaaksysteem::Tools;
use Zaaksysteem::ZAPI::Form::Field;
use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::Types qw(UUID);

extends 'Zaaksysteem::Backend::Sysin::Modules';

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::API

=cut

use constant INTERFACE_ID => 'api';

use constant INTERFACE_CONFIG_FIELDS => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_api_verion',
        type        => 'select',
        label       => 'Versie',
        required    => 1,
        description => 'Selecteer de gewenste API versie.',
        data        => {
            options => [
                { value => 'v1', label => 'API v1' }
            ]
        }
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_api_key',
        type        => 'text',
        label       => 'API sleutel',
        required    => 1,
        description => 'Interface key voor externe koppeling',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name     => 'interface_medewerker',
        type     => 'spot-enlighter',
        label    => 'Medewerker',
        required => 1,
        description => 'Bepaal hier welke medewerkerrechten de externe partij krijgt.',
        data => {
            restrict    => 'contact/medewerker',
            placeholder => 'Type uw zoekterm',
            label       => 'naam',
        }
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_access',
        type => 'select',
        label => 'Toegangsniveau',
        required => 1,
        description => 'Het toegangsniveau bepaalt of de API gebruiker alleen gegevens mag raadplegen, of mogelijk ook mag wijzigen. Deze instelling werkt globaal over de API, dus zelfs als de ingestelde medewerker schrijfrechten heeft op een bepaald object, maar deze instelling op "Raadplegen" staat, zal de API gebruiker niet kunnen schrijven naar het Zaaksysteem.',
        data => {
            options => [
                { value => 'ro', label => 'Raadplegen' },
                { value => 'rw', label => 'Behandelen' }
            ]
        }
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_query_constraint',
        type => 'spot-enlighter',
        label => 'Zoekopdracht',
        required => 0,
        description => 'Door een hier een opgeslagen zoekopdracht te specificeren kan bepaald worden welke objecten de externe partij kan zien en/of gebruiken',
        data => {
            restrict => 'objects',
            placeholder => 'Type uw zoekterm',
            label => 'label',
            params => { object_type => 'saved_search' }
        }
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_api_uri',
        type => 'display',
        label => 'API URI',
        description => 'De genoteerde URI is de locatie waar de externe partij met het Zaaksysteem kan communiceren',
        required => 0,
        data => {
            template => '<a href="<[field.value]>" target="_blank"><[field.value]></a>'
        }
    )
];

use constant MODULE_SETTINGS => {
    name                          => INTERFACE_ID,
    label                         => 'Zaaksysteem API',
    interface_config              => INTERFACE_CONFIG_FIELDS,
    direction                     => 'incoming',
    manual_type                   => ['text'],
    is_multiple                   => 1,
    is_manual                     => 1,
    retry_on_error                => 0,
    allow_multiple_configurations => 1,
    is_casetype_interface         => 0,
    has_attributes                => 0,
    case_hooks      => [],
    test_interface  => 0,
    trigger_definition  => {
        log_mutation              => {
            method  => 'log_mutation',
        },
    },
};

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig(%{MODULE_SETTINGS()});
};

=head2 _load_values_into_form_object

=cut

around _load_values_into_form_object => sub {
    my $orig = shift;
    my $self = shift;
    my $opts = $_[1]; # get options

    my $form = $self->$orig(@_);

    $form->load_values({
        interface_api_uri => $opts->{ base_url } . 'api/v1/'
    });

    return $form;
};

sub log_mutation {
    my ($self, $params, $interface) = @_;

    my $transaction = $interface->process({
            external_transaction_id => $params->{request_id},
            input_data              => $self->create_input_data($params),
            processor_params        => {
                processor       => '_process_log_mutation',
                request_body    => $params->{request_body},
                request_method  => $params->{request_method},
                request_call    => $params->{request_call},
                client_ip       => $params->{client_ip}
            },
        },
    );

    return $transaction;
}

sub create_input_data {
    my $self        = shift;
    my $params      = shift;

    return 'Client IP: ' . $params->{client_ip} . "\n"
        .'Request method: ' . $params->{request_method} . "\n"
        .'Request API call: ' . $params->{request_call} . "\n"
        ."Success: UNKNOWN\n"
        ."Request body:\n" . $params->{request_body};

}

sub _process_log_mutation {
    my ($self, $record) = @_;

    my $transaction = $self->process_stash->{transaction};
    my $params      = $transaction->get_processor_params();


    my $interface   = $transaction->interface;
    my $config      = $interface->get_interface_config;

    $record->input($self->create_input_data($params));
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 log_mutation

TODO: Fix the POD

=head2 create_input_data

TODO: Fix the POD

=cut
