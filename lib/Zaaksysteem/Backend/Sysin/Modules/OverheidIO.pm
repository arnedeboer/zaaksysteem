package Zaaksysteem::Backend::Sysin::Modules::OverheidIO;
use Moose;

use Encode qw(encode_utf8);
use JSON;
use LWP::UserAgent;
use URI;

use Zaaksysteem::Tools;
use Zaaksysteem::Backend::Sysin::OverheidIO::Model;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    MooseX::Log::Log4perl
    Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
/;

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::OverheidIO - OverheidIO Koppelingen

=head1 DESCRIPTION

This koppeling will handle the different modules from OverheidIO. For now support is limited
to OpenKVK.

=head1 SYNOPIS

    # Most of this module is used by third party modules, like L<Zaaksysteem::BR::Subject>

=head1 TESTS

Most of the tests can be found in L<TestFor::General::Backend::Sysin::Modules::OverheidIO>

=head1 CONSTANTS

=head2 INTERFACE_ID

Contains the interface id: C<stufnps>

=cut

use constant INTERFACE_ID               => 'overheidio';

=head2 INTERFACE_CONFIG_FIELDS

Config fields for this interface, empty for now

=cut

use constant INTERFACE_CONFIG_FIELDS    => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_overheid_io_key',
        type        => 'text',
        label       => 'Overheid.io API key',
        required    => 1,
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_overheid_io_module_kvk',
        type        => 'checkbox',
        label       => 'Activeer Overheid IO voor KvK bevragingen',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        when => 'interface_overheid_io_module_kvk === true',
        name  => 'interface_overheid_io_eherkenning_request',
        type  => 'checkbox',
        label => 'Activeren voor eHerkenning',
        description =>
            qq{Bevraag Overheid.io na het inloggen met eHerkenning zodat de meest recente gegevens in Zaaksysteem opgeslagen worden.},
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_overheid_io_module_bag',
        type        => 'checkbox',
        label       => 'Activeer Overheid IO voor BAG bevragingen',
    ),
];

=head2 MODULE_SETTINGS

Configuration for this module

=cut


use constant MODULE_SETTINGS            => {
    name                            => INTERFACE_ID,
    label                           => 'Overheid IO Koppelingen',
    interface_config                => INTERFACE_CONFIG_FIELDS,
    direction                       => 'outgoing',
    manual_type                     => [],
    is_multiple                     => 0,
    is_manual                       => 1,
    retry_on_error                  => 0,
    sensitive_config_fields         => [qw(overheid_io_key)],
    allow_multiple_configurations   => 0,
    is_casetype_interface           => 0,
    trigger_definition              => {
        search_companies      => {
            method  => 'search_companies',
        },
    },
    interface_update_callback => sub {
        return shift->_update_interface_config(@_);
    },

    description => qq{
<p>
Deze koppeling is verantwoordelijke voor de integratie tussen de
verschillende API's bij OverheidIO en Zaaksysteem. Er dient een
abbonement aangevraagd te worden bij <a href="https://overheid.io/abonnementen">OverheidIO</a>
om gebruik te kunnen maken van deze interface.
</p>
<p>
    Voor meer informatie kunt u terecht op de
    <a href="http://wiki.zaaksysteem.nl/Koppelprofiel_OverheidIO">
        Zaaksysteem Wiki
    </a>
</p>
},
};

=head2 _update_interface_config

Update the interface config to respect the new config items

=cut

sub _update_interface_config {
    my ($module, $interface, $model) = @_;

    my $config = $interface->get_interface_config;

    my $legacy_config = delete $config->{overheid_io_module};
    return 0 unless $legacy_config;

    if ($legacy_config eq 'kvk') {
        $config->{overheid_io_module_kvk} = 1;
        $interface->update_interface_config($config);
        return 1;
    }
    return 0;
}

=head2 OVERHEIDIO_HOST

B<Default>: overheid.io

Host of overheid.io

=cut

use constant OVERHEIDIO_HOST    => 'overheid.io';


=head2 OVERHEIDIO_PATHS

The different overheid.io modules and there releative URL paths for the API

=cut

use constant OVERHEIDIO_PATHS   => {
    'kvk'   => '/api/kvk',
};

=head2 OVERHEID_IO_KVK_FIELDS

An C<ArrayRef> of fieldnames to retrieve from overheid.io (kvk).

=cut

use constant OVERHEID_IO_KVK_FIELDS => [qw/dossiernummer handelsnaam huisnummer huisnummertoevoeging plaats postcode straat vestigingsnummer/];


###
### BUILDARGS
###

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig( %{ MODULE_SETTINGS() } );
};

=head1 METHODS

=head2 search_companies

    my $transaction = $interface->process_trigger('search_companies', { company => 'Mintlab' });

    my @result      = @{ $transaction->get_processor_params->{result} }

Searches for company entities matching the given params in remote systems

=cut

define_profile search_companies => (
    required => {},
    constraint_methods  => {
        'address_residence.zipcode' => sub {
            my $val     = pop;

            return 1 if $val =~ /^[1-9][0-9]{3}[A-Z]{2}$/;
            return;
        },
        'address_residence.street_number' => sub {
            my $val     = pop;

            return 1 if $val =~ /^[0-9]+$/;
            return;
        }
    },
    field_filters   => {
        'address_residence.zipcode' => sub {
            my $val = shift;

            $val =~ s/\s//;

            return uc($val);
        }
    },
    require_some    => {
        coc_or_company_or_zipcode => [1, qw/coc_number address_residence.zipcode company/],
    },
    dependency_groups => {
        zipcode_group       => [qw/address_residence.zipcode address_residence.street_number/],
    }
);

sub search_companies {
    my $self            = shift;
    my $params          = shift;
    my $interface       = shift;

    ### Error when required params are missing, but do not override $params
    assert_profile($params);

    my $transaction     = $interface->process(
        {
            processor_params        => {
                processor       => '_process_search_companies',
                request_params  => $params,
            },
            direction               => 'outgoing',
            external_transaction_id => 'unknown',
            input_data              => Data::Dumper::Dumper($params),
        }
    );

    unless (
        $transaction->processor_params &&
        $transaction->processor_params->{result}
    ) {
        throw(
            'sysin/modules/overheidio/search_companies/no_results',
            'Failure in getting results',
        );
    }

    return $transaction;
}

=head2 _process_search_companies

    $interface->process(
        {
            processor_params        => {
                processor       => '_process_search_companies',
                request_params  => { company => 'Mintlab' }
            },
            direction               => 'outgoing',
            external_transaction_id => 'unknown',
            input_data              => Data::Dumper::Dumper($params),
        }
    );

Actual processor for searching in other party for companies.

=cut

my $map = {
    coc_number                          => 'dossiernummer',
    coc_location_number                 => 'vestigingsnummer',
    company                             => 'handelsnaam',
    'address_residence.street'          => 'straat',
    'address_residence.zipcode'         => 'postcode',
    'address_residence.street_number'   => 'huisnummer',
};

sub _process_search_companies {
    my $self            = shift;
    my $record          = shift;

    ### Every error is by default fatal (do not retry)
    $self->process_stash->{error_fatal} = 1;

    my $transaction     = $self->process_stash->{transaction};
    my $params          = $transaction->get_processor_params()->{request_params};
    my $key             = $transaction->interface->get_interface_config->{overheid_io_key};

    my $model = Zaaksysteem::Backend::Sysin::OverheidIO::Model->new(
        key  => $key,
        type => 'kvk',
    );

    ### Map query company => handelsnaam, coc_number => dossiernummer etc
    my %query           = map {; $map->{ $_ } => $params->{ $_ } } grep({ $map->{$_} } keys %$params);

    $record->preview_string(
        "Zoek (kvk): " .
        join(', ', values %query)
    );


    # If we have a dossiernummer, use that as the search term
    # otherwise pick the handelsnaam
    my $search_term = delete $query{dossiernummer} // delete $query{handelsnaam};

    my $res = $model->search(
        $search_term,
        filter => \%query,
    );

    $transaction->input_data($record->input);

    unless ($res) {
        $transaction->error_fatal(1);
        $transaction->error_count(1);

        return;
    }

    ### Map result to objects
    my $companies = $self->_map_to_companies($res);

    $transaction->processor_params(
        {
            %{ $transaction->processor_params },
            result  => ($companies || []),
        }
    );

    return $record->output();
}

=head2 _get_json_from_overheid

    $self->_get_json_from_overheid(
        {
            module      => 'kvk',
            filters     => { 'handelsnaam' => 'mintlab' },
            api_key     => 'a8bab86ab6a9b86a98b6a98b6a9b86a986b9a86b9ab',
        }
    )

Calls the Overheid.IO API with the given API key. Will define a URL containing the filters given.

=cut

define_profile '_get_json_from_overheid' => (
    required     => {
        module          => 'Str',
        api_key         => 'Str',
        filters         => 'HashRef',
    },
    optional    => {
        record          => 'Zaaksysteem::Backend::Sysin::TransactionRecord::Component'
    }
);

sub _get_json_from_overheid {
    my $self    = shift;
    my $opts    = assert_profile(shift || {})->valid;

    my %filters = $opts->{filters} ? %{ $opts->{filters} } : ();
    my $key     = $opts->{api_key};

    throw(
        'sysin/module/overheidio/unknown_overheidio_module',
        "Unknown overheid.io module given: " . $opts->{module}
    ) unless OVERHEIDIO_PATHS->{$opts->{module}};

    ### Only active
    $filters{'actief'} = 'true';
    my %query   = map {; "filters[$_]" => $filters{$_} } keys %filters;

    ### Max 30 results
    $query{size} = 30;

    ### Generate URL
    my $uri     = URI->new("https://" . OVERHEIDIO_HOST . OVERHEIDIO_PATHS->{$opts->{module}});
    $uri->query_form(%query, 'fields[]' => OVERHEID_IO_KVK_FIELDS);


    ### Load UserAgent:
    my $ua      = LWP::UserAgent->new(
        ssl_opts => {
           verify_hostname => 0,
           SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
        },
        timeout => 5,
    );
    $ua->default_header('ovio-api-key', $key);

    ### Send HTTP Request:
    my $res     = $ua->get($uri->as_string);
    $self->log->debug('Send Overheid.IO request: ' . $uri->as_string);

    if ($opts->{record}) {
        my $record = $opts->{record};

        $record->input(
            'Calling ' . $uri->host . $uri->path . " with query: \n"
            . join("\n", map({ $_ . ': ' . $filters{$_} } keys %filters))
        );
        $record->output(substr($res->decoded_content, 0, 2048));

        unless ($res->is_success) {
            $record->is_error(1);
        }
    }

    return ($res->is_success ? JSON->new->decode($res->content) : undef);
}

=head2 _map_to_companies

    $self->_map_to_companies(
        {
            '_embedded' => {
                'rechtspersoon' => [
                    {
                        '_links' => {
                            'self' => {
                                'href' => '/api/kvk/51902672/0000'
                            }
                        },
                        'dossiernummer' => '51902672',
                        'handelsnaam' => 'Mintlab B.V.',
                        'huisnummer' => '7',
                        'huisnummertoevoeging' => 'UNIT 521-522',
                        'plaats' => 'Amsterdam',
                        'postcode' => '1051JL',
                        'straat' => 'Donker Curtiusstraat',
                        'subdossiernummer' => '0000',
                        'vestigingsnummer' => '21881022'
                    },
                    {
                        '_links' => {
                            'self' => {
                                'href' => '/api/kvk/51902672/0000'
                            }
                        },
                        'dossiernummer' => '51902672',
                        'handelsnaam' => 'Mintlab B.V.',
                        'huisnummer' => '7',
                        'huisnummertoevoeging' => 'UNIT 521-522',
                        'plaats' => 'Amsterdam',
                        'postcode' => '1051JL',
                        'straat' => 'Donker Curtiusstraat',
                        'subdossiernummer' => '0000',
                        'vestigingsnummer' => '21881022'
                    }
                ]
            },
            '_links' => {
                'self' => {
                    'href' => '/api/kvk?filters%5Bdossiernummer%5D=51902672&ovio-api-key=59bc9ccb54a0125984b596fe192840ea960772e010d920a0f20b6d73c62d037c&fields[]=dossiernummer&fields[]=handelsnaam&fields[]=huisnummer&fields[]=huisnummertoevoeging&fields[]=plaats&fields[]=postcode&fields[]=straat&fields[]=vestigingsnummer'
                }
            },
            'pageCount' => 2,
            'size' => 100,
            'totalItemCount' => 2s
        };
    );

Will map the given Overheid.IO companies to objects for L<Zaaksysteem::BR::Subject>

=cut

sub _map_to_companies {
    my $self        = shift;
    my $json        = shift or return [];

    my $rechtspersonen = $json->{_embedded}->{rechtspersoon};

    return [] unless $rechtspersonen;

    my @rv;
    for my $r (@$rechtspersonen) {
        my $href    = $r->{_links}->{self}->{href};
        my ($ident) = $href =~ /(\d+\/\d+)$/;
        push(
            @rv,
            {
                subject_type    => 'company',
                subject         => {
                    company     => $r->{handelsnaam},
                    coc_number  => $r->{dossiernummer},
                    coc_location_number => $r->{vestigingsnummer},
                    address_residence => {
                        street => $r->{straat},
                        street_number => $r->{huisnummer},
                        street_number_suffix => $r->{huisnummertoevoeging},
                        city => $r->{plaats},
                        zipcode => $r->{postcode},
                        country => {
                            dutch_code => '6030',
                        }
                    }
                },
                external_subscription => {
                    external_identifier => $ident
                }
            }
        )
    }

    return \@rv;
}


1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

