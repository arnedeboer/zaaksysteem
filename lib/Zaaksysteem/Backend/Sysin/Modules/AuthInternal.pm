package Zaaksysteem::Backend::Sysin::Modules::AuthInternal;

use Moose;

use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::ZAPI::Form::Field;

use Zaaksysteem::Backend::Sysin::Transaction::Mutation;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
/;


=head1 Interface Properties

Below a list of interface properties, see
L<Zaaksysteem::Backend::Sysin::Modules> for details.

=cut

use constant INTERFACE_ID               => 'authldap';

use constant INTERFACE_CONFIG_FIELDS    => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_autocreate_user',
        type => 'checkbox',
        required => 1,
        label => 'Auto-aanmaak gebruikers',
        description => 'Indien het wenselijk is gebruikers te importeren bij de eerste keer dat ze zich aanmelden op het Zaaksysteem, vink dit aan'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_can_create_user',
        type => 'checkbox',
        required => 1,
        label => 'Handmatig gebruikers kunnen aanmaken',
        description => "Via gebruikersbeheer handmatig gebruikers kunnen aanmaken. Let op: dit werkt niet wanneer u gebruikt maakt op gebruikerssynchronisatie d.m.v. een beheerbox"
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_messaging_user',
        type        => 'spot-enlighter',
        label       => 'Gebruikersbeheerder',
        description => 'Voor het verzenden van notificaties, geef hier de persoon op welke belast is met het toewijzen van gebruikers aan een afdeling',
        data => {
            restrict => 'contact/medewerker',
            placeholder => 'Type uw zoekterm',
            label => 'naam'
        }
    ),
];

use constant MODULE_SETTINGS => {
    name => INTERFACE_ID,
    label => 'Interne authenticatie',
    essential => 1,
    interface_config => INTERFACE_CONFIG_FIELDS,
    direction => 'incoming',
    manual_type => [],
    is_multiple => 1,
    is_manual => 1,
    retry_on_error => 0,
    allow_multiple_configurations => 1,
    is_casetype_interface => 0,
    trigger_definition  => {
        register_changes   => {
            method  => 'register_changes',
        },
    },
};

around BUILDARGS => sub {
    my $orig = shift;
    my $class = shift;

    return $class->$orig( %{ MODULE_SETTINGS() }, @_ );
};

has 'subject_type' => (
    is      => 'ro',
    default => 'employee',
);

sub register_changes {
    my $self                        = shift;
    my $raw_params                  = shift || {};
    my $interface                   = shift;

    my $transaction = $interface->process(
        {
            processor_params        => {
                processor       => '_process_changes',
                changes         => $raw_params->{changes},
            },
            external_transaction_id => 'unknown',
            input_data              => 'json',
            #direct                  => 1,
        }
    );

    return $transaction;
}

sub _process_changes {
    my $self            = shift;
    my $record          = shift;
    my $mutations;

    my $transaction     = $self->process_stash->{transaction};

    my $params          = $transaction->get_processor_params();

    my $changes         = $params->{changes};

    my $mutation        = Zaaksysteem::Backend::Sysin::Transaction::Mutation->new(
        table               => 'Subject',
        table_id            => $changes->{_subject_id}
    );

    my $subject         = $transaction
                        ->result_source
                        ->schema
                        ->resultset('Subject')
                        ->search({ id => $changes->{_subject_id} })
                        ->first;

    throw(
        'sysin/modules/authldap',
        'Cannot find subject by id: ' . $changes->{_subject_id}
    ) unless $subject;

    if ($changes->{_create}) {
        $mutation->create(1);
    }
    else {
        $mutation->update(1);
    }


    my $human_readable_changes = sprintf("User %s changed\n", $subject->username);
    for my $column (keys %{ $changes }) {
        my $columndata = $changes->{ $column };
        next unless UNIVERSAL::isa($changes->{$column}, 'HASH');

        $mutation->add_mutation(
            {
                old_value   => $columndata->{old},
                new_value   => $columndata->{new},
                column      => $column,
            }
        );

        $human_readable_changes .= 'Attribuut "' . $column
                                . '" gewijzigd naar "'
                                . $columndata->{new} . "\"\n";
    }

    $self->process_stash->{row}->{mutations} = [ $mutation ];

    $record->input($human_readable_changes);

    $record->output('SUCCESFULL');
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 INTERFACE_CONFIG_FIELDS

TODO: Fix the POD

=cut

=head2 INTERFACE_ID

TODO: Fix the POD

=cut

=head2 MODULE_SETTINGS

TODO: Fix the POD

=cut

=head2 register_changes

TODO: Fix the POD

=cut

