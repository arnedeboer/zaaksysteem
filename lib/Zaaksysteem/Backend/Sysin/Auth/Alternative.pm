package Zaaksysteem::Backend::Sysin::Auth::Alternative;
use Moose;

use LWP::UserAgent;
use Number::Phone;
use XML::LibXML;
use Zaaksysteem::Tools;
use Zaaksysteem::Types qw(MobileNumber EmailAddress BSN NonEmptyStr);
use Crypt::OpenSSL::Random qw(random_pseudo_bytes);
use Math::Base36 qw(encode_base36);
use Authen::Passphrase;

with 'MooseX::Log::Log4perl';

=head1 NAME

Zaaksysteem::Auth::Alternative - Alternative authentication module for Zaaksystem

=head1 DESCRIPTION

Model for Zaaksysteem Alternative Authentication or Two Factor authentication

=head1 SYNOPSIS

    use Zaaksysteem::Auth::Alternative;

    my $alt = Zaaksysteem::Auth::Alternative->new(
        schema => $cli->schema,
    );

    my $number = $alt->assert_mobile_phone_number($number);
    my $xml = $alt->create_sms_xml(
        phone_number => $number,
        message => "You win the lottery",
    );
    $alt->send_sms(
        xml => $xml,
        record => $transaction_record,
    );


=head1 ATTRIBUTES

=head2 schema

An L<Zaaksysteem::Schema> object. Required.

=cut

has schema => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Schema',
    required => 1,
);

=head2 interface

A L<Zaaksysteem::Model::DB::Interface> object. Required.

=cut

has interface => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Model::DB::Interface',
    required => 1,
);

=head2 sms_sender

The SMS sender name. Required.

=cut

has sms_sender => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

=head2 sms_product_token

The SMS product token. Required.

=cut

has sms_product_token => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

=head2 sms_endpoint. Required.

The SMS HTTP endpoint

=cut

has sms_endpoint => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

=head2 ua

An L<LWP::UserAgent> object, override when needed

=cut

has ua => (
    is      => 'ro',
    isa     => 'LWP::UserAgent',
    lazy    => 1,
    default => sub {
        my $ua = LWP::UserAgent->new;
        $ua->timeout(10);
        $ua->ssl_opts(verify_hostname => 1,);
        return $ua;
    },
);

=head2 password_reset_timeout

Waiting period (in seconds) for a password reset

=cut

has password_reset_timeout => (
    is      => 'ro',
    isa     => 'Int',
    default => 30,
);

=head2 assert_mobile_phone_number

Checks if the number used is a Dutch mobile phone and/or Dutch pager
number. Dies in case of non-compliancy, returns a number which can be
used for subsequent calls.

=cut

sig assert_mobile_phone_number  => 'Str';

sub assert_mobile_phone_number {
    my ($self, $number) = @_;

    if (substr($number, 0,2) eq '00') {
        $number = "+" . substr($number, 2);
    }

    my $ok_number = Number::Phone->new($number) // Number::Phone->new('NL', $number);

    if (!$ok_number) {
        throw('auth/alternative/phone_number/invalid', "Not a valid phonenumber: $number", { fatal => 1});
    }
    elsif (uc($ok_number->country) ne 'NL') {
        throw('auth/alternative/phone_number/country_code', "Not a supported country", { fatal => 1 });
    }
    # Pagers and mobile phones can receive SMS?
    elsif (!$ok_number->is_mobile() && !$ok_number->is_pager) {
        throw('auth/alternative/phone_number/non_mobile', "Not a mobile phone number", { fatal => 1} );
    }

    return '0031' . $ok_number->format_using('Raw');

}

=head2 send_sms

    $self->send_sms(
        xml => $xml,
        record => "Zaaksysteem::Schema::TransactionRecord",
    );

Sends the XML to the endpoint and logs the output in the transaction records.

=cut

define_profile send_sms => (
    required => {
        xml    => 'Str',
        record => 'Zaaksysteem::Schema::TransactionRecord',
    },
);

sub send_sms {
    my $self = shift;
    my $params = assert_profile({@_})->valid;

    my $req = HTTP::Request->new(
        'POST',
        $self->sms_endpoint,
        [ "Content-Type" => "application/xml" ],
        $params->{xml},
    );

    my $res = $self->ua->request($req);

    $params->{record}->input($req->dump(maxlength => 0));
    $params->{record}->output($res->dump(maxlength => 0));

    if (!$res->is_success) {
        throw(
            "twofactor/sms_sending_failed",
            $res->status_line,
            { fatal => 1 }, # Don't retry SMS sending. That's expensive.
        );
    }

    return 1;
}

=head2 create_sms_xml

    $self->create_sms_xml(
        phone_number => $number,
        message      => "When you read this, you've received the SMS",
        reference    => "Reference to something",
    );

Generate the XML which is used to send to the endpoint. You will want to
reference to be a transaction ID but it can be anything.

=cut

define_profile create_sms_xml => (
    required => {
        phone_number => 'Str',
        message      => 'Str',
        reference    => 'Str',
    },
);

sub create_sms_xml {
    my $self = shift;
    my $params = assert_profile({@_})->valid;

    my $number = $self->assert_mobile_phone_number($params->{phone_number});

    my $doc = XML::LibXML->createDocument;

    my %nodes;

    $nodes{root} = $doc->createElement("MESSAGES");
    $doc->setDocumentElement($nodes{root});

    $nodes{authentication}  = $nodes{root}->addNewChild(undef, "AUTHENTICATION");
    $nodes{token}           = $nodes{authentication}->addNewChild(undef, "PRODUCTTOKEN");

    $nodes{msg} = $nodes{root}->addNewChild(undef, "MSG");

    $nodes{from}      = $nodes{msg}->addNewChild(undef, "FROM");
    $nodes{to}        = $nodes{msg}->addNewChild(undef, "TO");
    $nodes{body}      = $nodes{msg}->addNewChild(undef, "BODY");
    $nodes{reference} = $nodes{msg}->addNewChild(undef, "REFERENCE");

    $nodes{token}->appendTextNode($self->sms_product_token);
    $nodes{from}->appendTextNode(substr($self->sms_sender, 0, 11));
    $nodes{to}->appendTextNode($number);
    $nodes{body}->appendTextNode(substr($params->{message}, 0, 160));
    $nodes{reference}->appendTextNode($params->{reference});

    my $xml = $doc->toString(1);

    $self->log->trace("SMS XML: " . $xml);

    return $xml;
}

=head2 assert_username

Check if the username is available, dies when the username cannot be found.

=cut

sig assert_username => 'Str';

sub assert_username {
    my ($self, $username) = @_;

    my $ue = $self->find_user_entity($username);
    if ($ue) {
        throw('auth/alternative/username/exists', "Username $username already exists");
    }
    return 0;
}

=head2 find_user_entity

Get the L<Zaaksysteem:Schema::UserEntity> object from the database via the username.
Returns undef if nothing can be found.

=cut

sig find_user_entity => 'Str';

sub find_user_entity {
    my ($self, $username) = @_;

    return $self->schema->resultset('UserEntity')->search({
        'LOWER(source_identifier)' => lc($username),
        source_interface_id        => $self->interface->id,
    })->first;
}

=head2 get_user_entity

Get the L<Zaaksysteem:Schema::UserEntity> object from the database via the username.
Dies if no user entity can be found.

=cut

sig get_user_entity => 'Str';

sub get_user_entity {
    my ($self, $username) = @_;

    my $ue = $self->find_user_entity($username);
    return $ue if $ue;

    throw('auth/alternative/user/not_found',
        "Unable to find user with username $username");
}

=head2 register_account

Register an account. Returns a L<Zaaksysteem::Model::DB::Subject> object.

    my $subject = $self->register_account(
        # Required:
        username       => 'username',
        password       => 'password',
        password_check => 'password',
        email          => 'foo@example.com',
        phone          => '+31612345678',
        subject_type   => 'person', # Or company

        # In case of a person BSN is required
        bsn => 123456789,

        # In case of a company KVK is required
        # vestigingsnummer is optional
        kvk => 1234567,
        vestigingsnummer => 12345679,
    );

=cut

define_profile register_account => (
    required => {
        username       => NonEmptyStr,
        password       => NonEmptyStr,
        password_check => NonEmptyStr,
        email          => EmailAddress,
        phone          => MobileNumber,
        subject_type   => 'Str',

    },
    optional => {
        bsn => BSN,
        kvknummer        => 'Num',
        vestigingsnummer => 'Num',
    },
);

sub register_account {
    my $self = shift;
    my $params = assert_profile({@_})->valid;

    $self->assert_password(%$params);

    my $ue      = $self->create_account(%$params);
    my $subject = $ue->subject_id;

    $self->update_password(
        subject     => $subject,
        user_entity => $ue,
        password    => $params->{password},
    );

    return $subject;
}

=head2 create_account

Create an account. Returns a L<Zaaksysteem::Schema::UserEntity> object.

    my $subject = $self->create_account(
        # Required:
        username       => 'username',
        email          => 'foo@example.com',
        phone          => '+31612345678',
        subject_type   => 'person', # Or company

        # In case of a person BSN is required
        bsn => 123456789,

        # In case of a company KVK is required
        # vestigingsnummer is optional
        kvk => 1234567,
        vestigingsnummer => 12345679,
    );

=cut

define_profile create_account => (
    required => {
        username     => NonEmptyStr,
        email        => EmailAddress,
        phone        => MobileNumber,
        subject_type => 'Str',
    },
    optional => {
        bsn              => BSN,
        kvknummer        => 'Num',
        vestigingsnummer => 'Num',
    }
);
sub create_account {
    my $self = shift;
    my $params = assert_profile({@_})->valid;

    $self->assert_username($params->{username});

    my $subject = $self->create_subject(@_);
    my $ue = $self->create_user_entity_from_subject($subject);

    $self->trigger_logging(
        type => "account",
        data => {
            action => 'create',
            %$params,
        },
        subject => $subject,
    );
    return $ue;
}

=head2 create_subject

Create a L<Zaaksysteem::Model::DB::Subject>

    my $subject = $self->create_subject(
        # Required:
        username       => 'username',
        email          => 'foo@example.com',
        phone          => '+31612345678',
        subject_type   => 'person', # Or company

        # In case of a person BSN is required
        bsn => 123456789,

        # In case of a company KVK is required
        # vestigingsnummer is optional
        kvk => 1234567,
        vestigingsnummer => 12345679,
    );

=cut

define_profile create_subject => (
    required => {
        username       => NonEmptyStr,
        email          => EmailAddress,
        phone          => MobileNumber,
        subject_type   => 'Str',

    },
    optional => {
        bsn => BSN,
        kvknummer        => 'Int',
        vestigingsnummer => 'Int',
    },
    dependencies => {
        subject_type => {
            'person'  => [ 'bsn' ],
            'company' => [ 'kvknummer' ],
        }
    },
);

sub create_subject {
    my $self = shift;
    my $params = assert_profile({@_})->valid;

    my $subject = $self->schema->resultset('Subject')->create(
        {
            subject_type => $params->{subject_type},
            username     => $params->{username},
            properties   => {
                phone_number  => $params->{phone},
                email_address => $params->{email},

                ($params->{subject_type} eq 'person')
                    ? (bsn => $params->{bsn})
                    : (
                        kvknummer        => $params->{kvknummer},
                        vestigingsnummer => $params->{vestigingsnummer},
                    ),
            },
        }
    );
    $subject->discard_changes;
    return $subject;
}

=head2 create_user_entity_from_subject

Create a L<Zaaksysteem::Schema::UserEntity> object from a L<Zaaksysteem::Model::DB::Subject>

    $self->create_user_entity_from_subject($subject);

=cut

sig create_user_entity_from_subject => "Zaaksysteem::Model::DB::Subject";

sub create_user_entity_from_subject {
    my $self = shift;
    my $subject = shift;

    return $subject->create_related(
        'user_entities',
        {
            source_interface_id => $self->interface->id,
            source_identifier   => $subject->username,

            # User entities are created with a "date deleted". When SMS
            # verification is done, the "date deleted" will be removed.
            date_deleted        => $self->schema->format_datetime_object(DateTime->now()),

            # Change this behaviour, add 'active' to user entity?
            #active => 0,
        }
    );
}

=head2 trigger_logging

Create logging entry in the C<auth/alternative> namespace.

    Sself->trigger_logging(
        type => 'foo', # creates auth/alternative/foo event logging entries
        data => { data => 'you want to store' },
        subject => $subject,
    );

=cut

define_profile trigger_logging => (
    required => {
        type    => 'Str',
        data    => 'HashRef',
        subject => 'Zaaksysteem::Model::DB::Subject',
    },
    optional => {
        data => 'HashRef',
    },
    defaults => {
        data => sub { return {} },
    },
);

sub trigger_logging {
    my $self  = shift;
    my $params = assert_profile({@_})->valid;

    return try {
        my $subject = $params->{subject};

        my $betrokkene = $self->find_betrokkene_by_subject(
            $subject
        );

        my $logging = $self->schema->resultset('Logging');

        $logging->trigger(
            'auth/alternative/' . $params->{type},
            {
                component    => 'authentication',
                component_id => $subject->id,

                data         => { %{$params->{data}}, username => $subject->username },

                $betrokkene ?
                (
                    betrokkene_id => $betrokkene->bid,
                    created_for => $betrokkene->bid,
                ) :
                (),
            }
        );
        return 1;
    }
    catch {
        $self->log->error("Unable to add logging entry: $_");
        return 0;
    };
}

=head2 create_activation_link

Create an activation link for a given subject. Deletes all other activation links.

    my $link = $self->create_activation_link($subject);

    printf("Username %s has token %s and expires on %s", 
        $link->subject_uuid->username,
        $link->token,
        $link->expires
    );

=cut

sig create_activation_link => 'Zaaksysteem::Model::DB::Subject';

sub create_activation_link {
    my ($self, $subject) = @_;

    my $rs = $self->schema->resultset('AlternativeAuthenticationActivationLink');
    my $activation_link = $rs->search({
            subject_id => $subject->uuid ,
    });

    if ($activation_link->count) {
        $activation_link->delete;
    }

    $self->trigger_logging(
        type => 'token',
        data => { },
        subject => $subject,
    );

    return $rs->create(
        {
            subject_id => $subject->uuid,
            token      => $self->activation_link_token,
            expires    => $self->schema->format_datetime_object(
                DateTime->now->add(hours => 72)
            ),
        }
    );
}

=head2 activation_link_token

Create a activation link token

=cut

sub activation_link_token {
    my $self = shift;
    return unpack 'H*', Crypt::OpenSSL::Random::random_pseudo_bytes(32);
}

=head2 get_activation_subject

Get the subject for an active activation link.

    my $subject = $self->get_activation_subject($token);

=cut

sig get_activation_subject => 'Str';

sub get_activation_subject {
    my $self = shift;
    my $code = shift;

    my $rs = $self->schema->resultset('AlternativeAuthenticationActivationLink');
    my $activation_link = $rs->search({
        token   => $code,
        expires => { '>=' => $self->schema->format_datetime_object(DateTime->now()) },
    })->first;

    if ($activation_link) {
        return $activation_link->subject_id;
    }
    throw('auth/alternative/activation/expired', "Your activation code does not exist or is expired");
}

=head2 delete_activation_link

Delete the activation link for a given token

    $self->delete_activation_link($token);

=cut

sig delete_activation_link => 'Str';

sub delete_activation_link {
    my $self = shift;
    my $code = shift;

    my $rs = $self->schema->resultset('AlternativeAuthenticationActivationLink');
    $rs->search({
        token   => $code,
    })->delete;
    return 1;
}

=head2 change_username

Change the username of a subject

    $self->change_username($subject, $username);

=cut

sig change_username => 'Zaaksysteem::Model::DB::Subject,Str';

sub change_username {
    my $self = shift;
    my $subject = shift;
    my $username = shift;

    my $ue = $subject->user_entities->search_rs({source_interface_id => $self->interface->id });

    my $old = $subject->username;

    $subject->update({username => $username});
    $ue->update({source_identifier => $username});

    $self->trigger_logging(
        type    => 'username',
        data    => { old => $old, new => $username },
        subject => $subject,
    );

    return 1;
}

=head2 update_password

Update the password for a subject. You should call assert_password prior
to calling this function to check if the password matches.

    $self->update_password(
        subject => $subject,
        password => $password,

        # Optional
        user_entity => $user_entity
    );

=cut

define_profile update_password => (
    required => {
        subject     => "Zaaksysteem::Model::DB::Subject",
        password    => 'Str',
    },
    optional => {
        user_entity => "Zaaksysteem::Schema::UserEntity",
    }
);

sub update_password {
    my $self = shift;
    my $params = assert_profile({@_})->valid;

    my $ue = $params->{user_entity} // $params->{subject}->user_entities->search_rs({source_interface_id => $self->interface->id})->first;

    $params->{subject}->update_password(
        $ue,
        { password => $params->{password} }
    );

    return 1;
}

=head2 assert_password

Check if the password is correct, also checks if the username is present in the password.
Could be expanded to force password policies to the user.

    $self->assert_password(
        username => 'myusername',
        password => 'password',
        password_check => 'password',
    );

=cut

define_profile assert_password => (
    required => {
        username       => NonEmptyStr,
        password       => NonEmptyStr,
        password_check => NonEmptyStr,
    },
);

sub assert_password {
    my $self = shift;
    my $params = assert_profile({@_})->valid;

    my $password = $params->{password};
    if ($password ne $params->{password_check}) {
        throw("auth/alternative/password/invalid", "Password does not match check");
    }

    my $username = $params->{username};
    my $qr = qr/$username/;
    if ($password =~ /\Q$qr\E/i) {
        throw("auth/alternative/password/username", "Password matches username");
    }
    return 1;
}

=head2 create_auth_challenge

Create a 2FA token

=cut

sub create_auth_challenge {
    my $self = shift;

    my $prand = random_pseudo_bytes(4);
    my $code = unpack("N", $prand);
    my $factor = encode_base36($code);

    $self->log->trace("Generated second factor: $factor");
    return $factor;
}

=head2 verify_second_factor

Just a simple verify function

=cut

sig verify_second_factor => 'Str,Str';

sub verify_second_factor {
    my ($self, $want, $is) = @_;
    return lc($want) eq lc($is);
}


=head2 activate_account

Activate an account. Sends an activation email

    $self->activate_account($user_entity);

=cut

sig activate_account => 'Zaaksysteem::Schema::UserEntity';

sub activate_account {
    my ($self, $ue) = @_;

    $ue->update({
        date_deleted => undef
    });

    $self->trigger_logging(
        type => 'activate',
        data => { },
        subject => $ue->subject_id,
    );

    $self->send_account_confirmation_email($ue->subject_id);
}

=head2 find_subject_by_betrokkene

Finds you a subject for a given betrokkene

=cut

sub find_subject_by_betrokkene {
    my ($self, $betrokkene) = @_;

    my $gm = $betrokkene->gm_object;

    my $record = $self->schema->resultset('GegevensmagazijnSubjecten')->search_rs(
        {
            $betrokkene->btype eq 'natuurlijk_persoon' ?
                ( np_uuid => $gm->uuid ) : ( nnp_uuid => $gm->uuid ),
        }
    )->first;
    if ($record) {
        return $record->subject_uuid;
    }
    return undef;
}

=head2 find_betrokken_by_subject

Finds you a betrokken for a given subject

=cut

sig find_betrokkene_by_subject => 'Zaaksysteem::Model::DB::Subject';

sub find_betrokkene_by_subject {
    my ($self, $subject) = @_;

    if (my $record = $self->get_subject_link($subject)) {
        if ($subject->subject_type eq 'person') {
            return $record->np_uuid;
        }
        else {
            return $record->nnp_uuid;
        }
    }
    # If we cannot find the link, search for the betrokkene in the
    # actual tables
    if ($subject->subject_type eq 'person') {
        return $self->schema->resultset('NatuurlijkPersoon')->find_by_bsn(
            $subject->properties->{bsn}, { authenticated => [0, 1, undef] }
        );
    }
    else {
        return $self->schema->resultset('Bedrijf')->find_by_kvk_and_vestiging(
            {
                kvk              => $subject->properties->{kvknummer},
                vestigingsnummer => $subject->properties->{vestigingsnummer},
                authenticated    => [0, 1, undef]
            }
        );
    }
}

=head2 get_subject_link

Get a link between the subject and the betrokkene

=cut

sig get_subject_link => 'Zaaksysteem::Model::DB::Subject';

sub get_subject_link {
    my ($self, $subject) = @_;

    return $self->schema->resultset('GegevensmagazijnSubjecten')->search_rs(
        {
            subject_uuid => $subject->uuid,
        }
    )->first;
}

=head2 set_subject_link

Set a link between the subject and the betrokkene

    $self->set_subject_link($subject, $person);

    $self->set_subject_link($subject, $company);

=cut

sig set_subject_link => 'Zaaksysteem::Model::DB::Subject,Defined';

sub set_subject_link {
    my ($self, $subject, $gm) = @_;

    my $rs = $self->schema->resultset('GegevensmagazijnSubjecten')->create(
        {
            subject_uuid => $subject->uuid,
            $subject->subject_type eq 'person' ?
                ( np_uuid => $gm->uuid ) : ( nnp_uuid => $gm->uuid ),
        }
    );

    return 1;
}

=head2 send_activation_link

Send the activation link to a subject

    $self->send_activation_link($subject, $uri);

=cut

sig send_activation_link => 'Zaaksysteem::Model::DB::Subject,Defined';

sub send_activation_link {
    my $self = shift;
    my $subject = shift;
    my $activation_link = shift;

    return $self->_send_email('$.account_activation_template.id',
        $subject, { activation_link => "$activation_link", });
}

=head2 send_account_confirmation_email

Send the account confirmation email to a subject

    $self->send_account_confirmation_email($subject);

=cut

sig send_account_confirmation_email => 'Zaaksysteem::Model::DB::Subject';

sub send_account_confirmation_email {
    my $self = shift;
    my $subject = shift;
    return $self->_send_email('$.account_confirmation_template.id', $subject);
}

=head2 send_password_request_email

Send a password request e-mail. Currently not in use.

    $self->send_password_request_email($subject);

=cut

sig send_password_request_email => 'Zaaksysteem::Model::DB::Subject';

sub send_password_request_email {
    my $self = shift;
    my $subject = shift;
    return $self->_send_email('$.password_request_template.id', $subject);
}

=head2 send_password_reset_email

Send a password reset e-mail. Currently not in use.

    $self->send_password_request_email($subject);

=cut

sig send_password_reset_email => 'Zaaksysteem::Model::DB::Subject';

sub send_password_reset_email {
    my $self = shift;
    my $subject = shift;
    return $self->_send_email('$.password_reset_template.id', $subject);
}

=head2 assert_password_reset

Assert if the password reset may happen

=cut

sig assert_password_reset => 'Zaaksysteem::Schema::UserEntity';

sub assert_password_reset {
    my ($self, $ue) = @_;

    my $properties = $ue->properties;
    my $last_reset = $properties->{password_reset};
    if ($self->password_reset_timeout) {
        if ($last_reset && (time - $last_reset <= $self->password_reset_timeout)) {
            throw('auth/alternative/password_reset/timeout', "Password reset not triggered, minimum time interval not passed");
        }
    }

    $properties->{password_reset} = time;
    $ue->update({ properties => $properties });
}

=head2 check_password

Check the password for a L<Zaaksysteem::Schema::UserEntity>

=cut

sig check_password => 'Zaaksysteem::Schema::UserEntity,Str';

sub check_password {
    my ($self, $ue, $want) = @_;

    if (!$ue->active) {
        throw("auth/alternative/user/inactive", "User is not active");
    }

    my $ppr = Authen::Passphrase->from_rfc2307($ue->password);

    if (!$ppr->match($want)) {
        throw("auth/alternative/password", "Password does not match");
    }
    return 1;
}

=head1 PRIVATE METHODS

=head2 _get_email_template

Get an email template by a jpath query.

=cut

sub _get_email_template {
    my $self = shift;
    my $interface_jpath = shift;

    if (my $id = $self->interface->jpath($interface_jpath)) {
        my $template = $self->schema->resultset('BibliotheekNotificaties')->find($id);
        return $template if $template;
        throw('auth/alternate/email/template', "No email template found in database with ID $id");
    }
    throw('auth/alternate/email/jpath', "No email configured in interface configuration for $interface_jpath");

}

=head2 _send_email

Send an email to someone

=cut

sub _send_email {
    my $self    = shift;
    my $jpath   = shift;
    my $subject = shift;
    my $context = shift // {};

    return try {
        my $template = $self->_get_email_template($jpath);
        my $subject  = $subject;

        my $email = $subject->properties->{email_address} // $subject->properties->{initial_email_address};
        my $phone = $subject->properties->{phone_number}  // $subject->properties->{initial_phone_number};

        $context = {
            username => $subject->username,
            email    => $email,
            mobile   => $phone,
            %$context,
        };

        my $msg = $template->send_mail(
            {
                to => $email,
                ztt_context => $context,
            }
        );

        $self->log->trace("Sending msg as " . $msg->as_string);

        $self->trigger_logging(
            type => 'mail',
            data => { template => $template->label, context => $context },
            subject => $subject,
        );

        return $msg;
    }
    catch {
        $self->log->fatal($_);
        $self->trigger_logging(
            type => 'mail',
            data => { template => $jpath, error => "$_" },
            subject => $subject,
        );
        return undef;
    }
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
