package Zaaksysteem::Backend::Sysin::Zorginstituut::Model;
use Moose;

extends 'Zaaksysteem::Backend::Sysin';

=head1 NAME

Zaaksysteem::Backend::Sysin::Zorginstituut::Model

=head1 SYNOPSIS

    use Zaaksysteem::Backend::Sysin::Zorginstituut::Model;

    # Any module that is enabled with wmo
    my $interface = $zs->schema->resultset('Interface')
        ->search_active({ module => 'c2go' })->first;

    my $wmo = Zaaksysteem::Backend::Sysin::iWMO::Model->new(
        interface        => $interface,
        # Optional, defaults to:
        berichtversie    => 2,
        berichtsubversie => 0,
    );

    $wmo->encode_301_message(
        case          => $case,
        berichtcode   => '432',
    );



=cut

use DateTime;
use Zaaksysteem::Tools;
use Zaaksysteem::XML::Compile;

has schema => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Schema',
    required => 1,
);

has interface => (
    is       => 'ro',
    required => 1,
);

has instance => (
    is      => 'ro',
    builder => '_build_instance',
    lazy    => 1,
);

has berichtversie => (
    is      => 'ro',
    isa     => 'Int',
    default => 2,
);

has berichtsubversie => (
    is      => 'ro',
    isa     => 'Int',
    default => 00,
);

=head2 generate_headers

    my %headers = $self->generate_headers(
        # required
        berichtcode => '132',
        ontvanger => '1234',
        afzender => '2345',

        # optional
        date => DateTime->now(),
    );

Returns a hash in list context

=cut

define_profile generate_headers => (
    required => {
        berichtcode   => 'Int',
        ontvanger     => 'Int',
        afzender      => 'Int',
    },
    optional => { date => 'DateTime', },
    defaults => { date => sub { return DateTime->now() } },
);

sub generate_headers {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;
    return (
        Header => {
            Berichtspecificatie => {
                Code      => $opts->{berichtcode},
                Versie    => $self->berichtversie,
                SubVersie => $self->berichtsubversie,
            },
            Afzender             => $opts->{afzender},
            Ontvanger            => $opts->{ontvanger},
            Berichtidentificatie => {
                Identificatie => $self->generate_identification,
                Dagtekening   => $opts->{date},
                Tekenset      => 1,
            },
        },
    );
}

=head2 generate_identification

Creates a new serial ID.

=cut

sub generate_identification {
    my $self = shift;

    return $self->schema->storage->dbh_do(
        sub {
            my ($storage, $dbh, @args) = @_;

            my $sth = $dbh->prepare(q{
                SELECT nextval('zorginstituut_identificatie_seq');
            });
            $sth->execute();
            my @result = $sth->fetchrow_array;
            return $result[0];
        }
    );
}


=head2 encode_301_message

    $self->encode_301_message(
        case          => $case,

        # Optional, defaults to 414.
        berichtcode => 414,

        # Optional, only use this for testing
        data => {
            # .. See the code for what you want to put into this
        },
    );

B<Arguments>: L<case|Zaaksysteem::Schema::Zaak>, L<berichtcode|Int>, L<data|HashRef>
B<Returns>: The generated XML


The data that is shown is the data that is used to create the XML.

=cut

define_profile encode_301_message => (
    required => {
        case        => 'Zaaksysteem::Schema::Zaak',
        berichtcode => 'Int',
    },
    optional => {
        data        => 'HashRef',
    },
);

sub encode_301_message {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    my $data = $opts->{data} // $self->interface->get_mapped_attributes_from_case($opts->{case});

    $self->log->debug("Encoding 301 message with the following data" . dump_terse $data);

    my $np = $opts->{case}->aanvrager_object;

    my @producten;
    push(@producten,
        {
            aanbieder        => $data->{aanbieder},
            ingangsdatum     => $data->{toegewezen_product_ingangsdatum},
            einddatum        => $data->{toegewezen_product_einddatum},
            toewijzingsdatum => $data->{toegewezen_product_toewijzingsdatum},

            omvang => {
                volume     => int($data->{toegewezen_product_omvang_volume}),
                eenheid    => $data->{toegewezen_product_omvang_eenheid},
                frequentie => $data->{toegewezen_product_omvang_frequentie},
            },

            commentaar      => $data->{toegewezen_product_commentaar},
            categorie       => $data->{toegewezen_product_categorie},
            code            => $data->{toegewezen_product_code},
            reden_intrekking => $data->{toegewezen_product_reden_intrekking},
        }
    );

    foreach (qw(1 2 3)) {
        # Assume that when there is no date set, we don't have a product
        next unless exists $data->{"toegewezen_product_ingangsdatum_$_"};
        push(
            @producten,
            {
                aanbieder    => $data->{aanbieder},
                ingangsdatum => $data->{"toegewezen_product_ingangsdatum_$_"},
                einddatum    => $data->{"toegewezen_product_einddatum_$_"},
                toewijzingsdatum =>
                    $data->{toegewezen_product_toewijzingsdatum},
                omvang => {
                    volume => $data->{"toegewezen_product_omvang_volume_$_"}
                    ? int($data->{"toegewezen_product_omvang_volume_$_"})
                    : undef,
                    eenheid =>
                        $data->{"toegewezen_product_omvang_eenheid_$_"},
                    frequentie =>
                        $data->{"toegewezen_product_omvang_frequentie_$_"},
                },
                commentaar => $data->{"toegewezen_product_commentaar_$_"},
                categorie  => $data->{"toegewezen_product_categorie_$_"},
                code       => $data->{"toegewezen_product_code_$_"},
                reden_intrekking => $data->{"toegewezen_product_reden_intrekking_$_"},
            }
        );
    }

    my $gemeentecode = $self->gemeentecode;

    my %data = (
        $self->generate_headers(
            berichtcode   => $opts->{berichtcode},
            ontvanger     => $data->{aanbieder},
            afzender      => $gemeentecode,
        ),
        Client      => $np,
        Beschikking => {
            nummer        => $opts->{case}->id,
            afgiftedatum  => $data->{beschikkingsafgiftedatum},
            ingangsdatum  => $data->{beschikkingsingangsdatum},
            einddatum     => $data->{beschikkingseinddatum},
        },
        gemeentecode => $gemeentecode,
        producten    => {
            beschikt => \@producten,
            toegewezen => \@producten,
        },
        commentaar => {
            general              => $data->{algemeen_commentaar},
            toegewezen_producten => $data->{toegewezen_product_commentaar},
        },
    );

    $self->create_object_subscription(
        bericht_id =>
            $data{Header}{Berichtidentificatie}{Identificatie},

        case => $opts->{case}
    );

    return $self->instance->build_301(writer => \%data);
}

=head2 create_object_subscription

Create an object subscription for Zorginstituut

=cut

define_profile create_object_subscription => (
    required => {
        bericht_id => 'Int',
        case       => 'Any',
    }
);

sub create_object_subscription {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    $self->schema->resultset('ObjectSubscription')->create(
        {
            interface_id => $self->interface->id,
            external_id  => $opts->{bericht_id},
            local_table  => 'Zaak',
            local_id     => $opts->{case}->id,
        }
    );
}

=head2 gemeentecode

Returns the gemeentecode as 4 digits, as used by the Dutch CBS.

=cut

sub gemeentecode {
    my $self = shift;
    return sprintf("%04d", $self->interface->get_interface_config->{gemeentecode});
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
