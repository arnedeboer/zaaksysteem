package Zaaksysteem::Backend::Sysin::Acknowledge::Model;
use Moose;

extends 'Zaaksysteem::Backend::Sysin';
with 'Zaaksysteem::Backend::Sysin::Roles::UA';
with 'Zaaksysteem::Backend::Sysin::Roles::Zorginstituut';


=head1 NAME

Zaaksysteem::Backend::Sysin::Acknowledge::Model

=head1 SYNOPSIS

    use Zaaksysteem::Backend::Sysin::Acknowledge::Model;

    my $interface = $zs->schema->resultset('Interface')
        ->search_active({ module => 'c2go' })->first;

    my $c2go = Zaaksysteem::Backend::Sysin::Acknowledge::Model->new_from_interface($interface);

    # Send iWMO 2.0 message to Acknowledge
    $c2go->get_oauth_token;
    $c2go->send_wmo_301_message($case);



=cut

use HTTP::Headers;
use HTTP::Request;
use JSON::XS qw(decode_json encode_json);

use Zaaksysteem::Tools;
use Zaaksysteem::XML::Compile;

define_profile _send_301_message => (
    required => {
        endpoint => 'Str',
        xml      => 'Str',
    },
    optional => {
        soapaction => 'Str',
        soapns     => 'Str',
    },
    defaults => {
        soapaction => 'TestBericht301',
        soapns     => 'http://wup.swvo.nl/',
    }
);

sub _send_301_message {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    return $self->call(
        endpoint   => $opts->{endpoint},
        message    => $opts->{xml},
        soapaction => $opts->{soapaction},
        soapns     => $opts->{soapns},
    );
}

=head2 call

Make an API call to Acknowledge

=cut

define_profile call => (
    required => {
        endpoint   => 'Str',
        message    => 'Defined',
        soapaction => 'Str',
        soapns     => 'Str',
    },
);

sub call {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    $self->assert_endpoint($opts->{endpoint});

    my $SOAP_ENVELOPE = q{<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"
  xmlns:acknowledge="http://wup.swvo.nl">
<SOAP-ENV:Body>
<acknowledge:%s>
    <acknowledge:bericht><![CDATA[%s]]></acknowledge:bericht>
</acknowledge:%s>
</SOAP-ENV:Body>
</SOAP-ENV:Envelope>};

    my $req = HTTP::Request->new(
        POST => $opts->{endpoint},
        HTTP::Headers->new(
            Content_Type => 'text/xml; charset=UTF-8',
            SOAPAction   => sprintf('%s%s', $opts->{soapns}, $opts->{soapaction}),
        ),
        sprintf($SOAP_ENVELOPE, $opts->{soapaction}, $opts->{message}, $opts->{soapaction}),
    );

    if ($self->log->is_trace) {
        $self->log->trace(
            "Calling Acknowledge $opts->{endpoint} with data: " . $req->as_string
        );
    }

    return $self->assert_http_response($self->ua->request($req));
}


=head1 BUILDERS

=head2 _build_endpoints

Build the endpoints for oath and message type based on the interface configuration.

=cut

sub _build_endpoints {
    my $self = shift;
    return { message => $self->interface->get_interface_config->{endpoint_message}, };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
