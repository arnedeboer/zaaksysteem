package Zaaksysteem::Backend::Sysin::iJW::Model;
use Moose;

extends 'Zaaksysteem::Backend::Sysin::Zorginstituut::Model';

=head1 NAME

Zaaksysteem::Backend::Sysin::iJW::Model

=head1 SYNOPSIS

    use Zaaksysteem::Backend::Sysin::iJW::Model;

    # Any module that is enabled with wmo
    my $interface = $zs->schema->resultset('Interface')
        ->search_active({ module => 'c2go' })->first;

    my $wmo = Zaaksysteem::Backend::Sysin::iJW::Model->new(
        interface        => $interface,
        berichtversie    => 4,
        berichtsubversie => 0,
    );

    $wmo->encode_301_message(
        case        => $case,
        berichtcode => 414,
    );



=cut

use Zaaksysteem::XML::Compile;

sub _build_instance {
    my $self     = shift;
    return Zaaksysteem::XML::Compile->xml_compile->add_class(
        'Zaaksysteem::XML::Generator::iJW::2_0')
    ->ijw2_0;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
