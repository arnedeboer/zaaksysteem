package Zaaksysteem::Backend::Rules::CasetypeRule;

use Moose;

extends 'Zaaksysteem::Backend::Rules::Rule';

use Zaaksysteem::Constants qw/ZAAKSYSTEEM_CONSTANTS/;

=head1 NAME

Zaaksysteem::Backend::Rule::CasetypeRule - Casetype specific rule handling

=head1 SYNOPSIS

=head1 DESCRIPTION

Extension of L<Zaaksysteem::Backend::Rules::Rule>, in charge of abstracting a
specific zaaktype rule to a normal "rule"

=head1 ATTRIBUTES

=head2 attribute_map

isa: HashRef ({ 44 => 'attribute.kenteken', 55 => 'attribute.omschrijving'})

Attribute mapping, to map bibliotheek_kenmerken_id to new style: C<attribute.kenmerk>
notation.

=cut

has 'attribute_map' => (
    is      => 'rw',
    isa     => 'HashRef',
);

has '_schema' => (
    is      => 'rw',
    lazy    => 1,
    default => sub {
        return shift->casetype_node->result_source->schema;
    }
);

=head2 has_parent_case

Boolean value, indicates whether the case has a parent case (used to
determine whether rules referring to referential attributes should be
skipped).

=cut

has has_parent_case => (
    is       => 'ro',
    isa      => 'Bool',
    required => 0,
    default  => 0,
);

=head2 status

isa: Int

The status of this case, that is: milestone number. e.g: 1 or 2

=cut

has 'status'    => (
    'is'        => 'rw',
    'isa'       => 'Int'
);

=head2 casetype_node

isa: Zaaksysteem::Model::DB::ZaaktypeNod

Contains the casetype_node where we load the rule from

=cut

has 'casetype_node'  => (
    is          => 'rw',
    isa         => 'Zaaksysteem::Model::DB::ZaaktypeNode',
    required    => 1,
);

has 'old_case_attributes'  => (
    is          => 'rw',
    isa         => 'ArrayRef',
    required    => 1,
);

=head2 zt_definition

    {
      'actie_1' => 'verberg_kenmerk',
      'actie_1_kenmerk' => 147,
      'actie_1_value' => 'hide Value',
      'acties' => '1',
      'naam' => 'Rule basic check: hide',
      'voorwaarde_1_kenmerk' => 144,
      'voorwaarde_1_value' => 'hide',
      'voorwaarde_1_value_checkbox' => '1',
      'voorwaarde_2_kenmerk' => 145,
      'voorwaarde_2_value' => 'show',
      'voorwaarde_2_value_checkbox' => '1',
      'voorwaarden' => '2'
    };

ZaaktypeRegel style definition of a rule, which we will need to convert to a
new style rule

=cut

has 'zt_definition' => (
    is      => 'rw',
);

=head1 CONSTRUCTION

'''_build_rule'''

Will build this rule, by loading conditions and the C<then> and C<else> actions

=cut

after '_build_rule' => sub {
    my $self        = shift;

    # print STDERR Data::Dumper::Dumper($self->zt_definition);

    $self->_load_conditions;
    $self->_load_actions('then');
    $self->_load_actions('else');
};

=head2 INTERNAL METHODS

=head2 _load_conditions

Turns the data from a L<ZaaktypeRegel> JSON object into Rule::Condition objects.

=cut


sub _load_conditions {
    my $self            = shift;

    my $zt_conditions   = $self->_convert_casetype_rule_to_array('voorwaarde');

    if (
        $self->zt_definition->{condition_type} &&
        lc($self->zt_definition->{condition_type}) eq 'or'
    ) {
        $self->condition_type('or');
    }

    my @conditions;
    for my $zt_condition (@$zt_conditions) {

        ### Sometimes, our zaaktype beheer "fscked" up by inserting an empty
        ### condition
        next unless $zt_condition;

        # print STDERR Data::Dumper::Dumper($zt_condition);
        $self->add_condition({
            attribute   => (
                $self->attribute_map->{$zt_condition->{kenmerk}}
                    ? 'attribute.' . $self->attribute_map->{$zt_condition->{kenmerk}}
                    : $zt_condition->{kenmerk}
            ),
            values      => (ref $zt_condition->{value} eq 'ARRAY' ? $zt_condition->{value} : [$zt_condition->{value}]),

            casetype_properties  => $self->casetype_node->properties,
            rules_params         => $self->rules_params,
            _schema              => $self->_schema,
            attribute_map        => $self->attribute_map,

            $zt_condition->{value_postcode_from}
                ?   (
                        value_zipcode_from => $zt_condition->{value_postcode_from},
                        value_zipcode_to   => $zt_condition->{value_postcode_to},
                    )
                : ()
        })
    }
}

=head2 _load_actions

Turns the data from a L<ZaaktypeRegel> JSON object into Rule::Action objects.

=cut

sub _load_actions {
    my $self            = shift;
    my $actiontype      = shift;

    my $zt_actiontype = 'actie';
    if ($actiontype eq 'else') {
        $zt_actiontype = 'ander';
    }

    my $zt_actions      = $self->_convert_casetype_rule_to_array($zt_actiontype);

    my @actions;
    for my $zt_action (@$zt_actions) {
        $zt_action  = $self->_translate_zaaktype_action_into_action($zt_action);
        next unless $zt_action;

        $self->add_action($actiontype, $zt_action->{type}, {
            attribute_name  => $zt_action->{kenmerk},
            value           => $zt_action->{value},
            $zt_action->{attribute_type} ? (attribute_type => $zt_action->{attribute_type}) :(),
            $zt_action->{related_attributes} ? (related_attributes => $zt_action->{related_attributes}) : (),
            $zt_action->{can_change} ? (can_change => 1) : (),
            $zt_action->{message}   ? (message  => $zt_action->{message}) : (),
            ($zt_action->{copy_properties} && $zt_action->{copy_properties} eq 'on') ? (copy_attributes  => 1) : (),
            ($zt_action->{startzaak} && $zt_action->{startzaak} eq 'on') ? (want_start_case => 1) : (),
            $zt_action->{zaaktype_id}
                ?
                    (
                        casetype_id => $zt_action->{zaaktype_id},
                        casetype_title => $zt_action->{zaaktype},
                    )
                    : (),
        });
    }
}

sub _translate_zaaktype_action_into_action {
    my $self                = shift;
    my $action              = shift;

    return unless $action->{type};

    ### Map type to internal type
    my $action_map          = {
        verberg_kenmerk     => 'hide_attribute',
        toon_kenmerk        => 'show_attribute',
        vul_waarde_in       => 'set_value',
        pauzeer_aanvraag    => 'pause_application',
        hide_group          => 'hide_group',
        show_group          => 'show_group',
    };

    $action->{type}         = $action_map->{ $action->{type} } or return;

    my $casetype_kenmerk;
    ### Convert kenmerk to useful kenmerk
    if ($action->{kenmerk}) {
        if ($action->{kenmerk} =~ /^\d+$/) {
            ($casetype_kenmerk)       = grep {
                $_->bibliotheek_kenmerken_id &&
                $_->bibliotheek_kenmerken_id->id eq $action->{kenmerk}
            } @{ $self->old_case_attributes };
        }
        

        $action->{kenmerk}      = $self->_get_attribute_for_old_case_kenmerk($action->{kenmerk})
            or return;
    }

    if ($casetype_kenmerk) {
        $action->{attribute_type}   = $casetype_kenmerk->bibliotheek_kenmerken_id->value_type;

        ### Translate type to object_type
        my $veldoptie               = ZAAKSYSTEEM_CONSTANTS->{veld_opties}->{$action->{attribute_type}};
        $action->{attribute_type}   = $veldoptie->{object_search_type};

        if ($veldoptie->{object_search_filter}) {
            $action->{value} = $veldoptie->{object_search_filter}->(undef, $action->{value});
        }

        ### Don't allow setting referential attributes
        if (
            $action->{type} eq 'set_value' &&
            $self->has_parent_case &&
            $casetype_kenmerk->referential
        ) {
            return;
        }
    }

    ### Convert value to real value
    if (
        $action->{type} eq 'set_value' &&
        $action->{kenmerk} &&
        $action->{kenmerk} eq 'case_result'
    ) {
        my $result = $self->casetype_node->zaaktype_resultaten->search(
            {},
            {
                order_by    => 'id',
                rows        => 1,
                offset      => ($action->{value} - 1)
            }
        )->first or return;

        $action->{value} = $result->resultaat or return;
    }

    if (
        ($action->{type} eq 'hide_group' || $action->{type} eq 'show_group') &&
        $action->{kenmerk}
    ) {
        # Loop over attributes
        my ($group_id, $current_group, @related_attributes);
        for my $attr (@{ $self->old_case_attributes }) {
            ### Make sure it only works for the current phase
            next unless $attr->zaak_status_id->status eq $self->status;

            if ($attr->is_group) {
                $current_group = $attr;

                if ($current_group->label eq $action->{kenmerk}) {
                    $group_id = $current_group->id;
                }
                next;
            }

            next unless $current_group->label eq $action->{kenmerk};

            ### Attribute belongs to current group, add this kenmerk to related_attributes
            my $related_attr = $self->_get_attribute_for_old_case_kenmerk($attr->bibliotheek_kenmerken_id->id)
                if $attr->bibliotheek_kenmerken_id;

            next unless $related_attr;

            push(
                @related_attributes,
                $related_attr
            );
        }

        $action->{kenmerk}              = $group_id;
        $action->{related_attributes}   = \@related_attributes;
    }

    return $action;
}

sub _get_attribute_for_old_case_kenmerk {
    my ($self, $kenmerk) = @_;

    return unless $kenmerk;

    my $attr = (
        $self->attribute_map->{$kenmerk}
            ? 'attribute.' . $self->attribute_map->{$kenmerk}
            : $kenmerk
    );

    if ($attr =~ /^\d+$/) {
        print STDERR 'Failed loading action in new rule engine, attribute not found: '
            . $attr;
        return;
    }

    return $attr;
}


=head2 _convert_casetype_rule_to_array

Arguments: $STRING_PREFIX

    $self->zt_definition({
      'actie_1' => 'verberg_kenmerk',
      'actie_1_kenmerk' => 147,
      'actie_1_value' => 'hide Value',
      'acties' => '1',
      'naam' => 'Rule basic check: hide',
      'voorwaarde_1_kenmerk' => 144,
      'voorwaarde_1_value' => 'hide',
      'voorwaarde_1_value_checkbox' => '1',
      'voorwaarde_2_kenmerk' => 145,
      'voorwaarde_2_value' => 'show',
      'voorwaarde_2_value_checkbox' => '1',
      'voorwaarden' => '2'
    });

    my $hash = $self->_convert_casetype_rule_to_array('voorwaarde');

    ## Returns


Converts a zaaktype style rule to hash.

=cut

sub _convert_casetype_rule_to_array {
    my $self            = shift;
    my $prefix          = shift;
    my (@list);

    for my $rawkey (grep(/^${prefix}_.*/, sort keys %{ $self->zt_definition })) {
        my ($id, $var)  = $rawkey =~ /^${prefix}_(\d+)_?(.*)$/;
        $var = 'type' unless $var;

        my $index       = ($id - 1);
        unless ($list[$index]) { $list[$index] = {}; }

        $list[$index]->{$var}  = $self->zt_definition->{ $rawkey };
    }

    return \@list;
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Backend::Rules::Rule> L<Zaaksysteem::Backend::Rules> L<Zaaksysteem::Manual>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
