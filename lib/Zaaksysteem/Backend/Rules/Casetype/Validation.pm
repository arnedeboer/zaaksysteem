package Zaaksysteem::Backend::Rules::Casetype::Validation;
use Moose::Role;

=head1 NAME

Zaaksysteem::Backend::Rules::Casetype::Validation - Validation role for casetypes

=head1 SYNOPSIS

=cut

use Zaaksysteem::Tools;
use JSON::Path;

with 'MooseX::Log::Log4perl';

=head1 METHODS

=head2 _validate_prepare_params

Will prepare the given params for the validate system. In this case, will transform old style
kenmerk_ids to new_style C<attribute.KENMERKNAAM> form

=cut

around '_validate_prepare_params' => sub {
    my $method              = shift;
    my $self                = shift;
    my ($params)            = @_;

    ### Return params when we do not have to interfere or params is not set
    return $self->$method(@_) unless ($self->source_type eq 'casetype' && $params);

    for my $key (keys %$params) {
        next unless $key =~ /^\d+$/;

        if ($self->_attribute_mapping->{ $key }) {
            $params->{ 'attribute.' . $self->_attribute_mapping->{ $key } } = $params->{$key};
        } else {
            $self->log->warn("rules/validation/prepare_params: could not find attribute for id: $key");
        }

        delete($params->{$key});
    }

    return $self->$method($params);
};

=head2 validate_from_case

Arguments: $case

    my $validation = $rules->validate_from_case($schema->resultset('Zaak')->search->first);

Validate


=cut

sub validate_from_case {
    my $self                = shift;
    my $case                = shift;
    my $options             = shift || {};

    my $engine              = $self;
    unless (ref $engine) {
        my $params          = $self->generate_object_params(
            {
                'case'                      => $case,
                $options->{'case.number_status'} ? ( 'case.number_status' => $options->{'case.number_status'}) : ()
            },
            {
                engine => 1,
            }
        );

        $engine             = $params->{engine};
    }

    my $kenmerken           = $case->field_values;

    return $engine->validate(
        {
            %{ $engine->rules_params },
            %{ $kenmerken },
            $options->{'case.number_status'} ? ( 'case.number_status' => $options->{'case.number_status'}) : ()
        }
    );
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Backend::Rules> L<Zaaksysteem::Manual>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
