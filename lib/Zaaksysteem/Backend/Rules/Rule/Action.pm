package Zaaksysteem::Backend::Rules::Rule::Action;

use Moose::Role;
use namespace::autoclean;

use Moose::Util::TypeConstraints;

use Zaaksysteem::Tools;

subtype 'ValidAttribute',
    as 'Str',
    where { $_ =~ /^attribute\.\w+$/ };

=head1 NAME

Zaaksysteem::Backend::Rules::Rule::Action - A single action

=head1 SYNOPSIS

=head1 DESCRIPTION

Role containing the basics for an action in the rules engine. Actions are defined below
the then and else blocks, and are instantiated by L<Zaaksysteem::Backend::Rules::Rule#add_action>

=head1 ATTRIBUTES

=head2 type

isa: Str

    $self->type('show_attribute');

The action name, e.g.: "show_attribute" or "hide_attribute"

=cut

has 'type'      => (
    is      => 'rw',
    isa     => 'Str',
);

=head2 attribute_type

isa: Str

    print $self->attribute_type;

    # number

The attribute type: "number" or "text"

=cut

has 'attribute_type'      => (
    is      => 'rw',
    isa     => 'Str',
);

=head1 METHODS

=head2 data

Transforms action attributes to a data hash. Set C<_data_attributes> attribute to the keys to
use when loading the data hash in TO_JSON

=cut

sub data {
    my $self        = shift;

    my %rv;
    for my $attr (@{ $self->_data_attributes }) {
        $rv{$attr} = $self->$attr;
    }

    return \%rv;
}

=head2 CONSTRUCTION

During construction, we delete the data attribute before loading this class. The elements within
this hash are put one level higher, e.g.:

    {
        type => 'show_attribute',
        data => {
            casetype_id => 44
        },
    }

    # Becomes

    {
        type => 'show_attribute',
        casetype_id => 44
    }

=cut

around BUILDARGS => sub {
    my $orig    = shift;
    my $class   = shift;
    my %params  = @_;

    if ($params{data}) {
        $params{$_} = $params{data}->{$_} for keys %{ $params{data} };
        delete $params{data};
    }

    return $class->$orig(%params);
};


=head2 TO_JSON

    $var = $action_object->TO_JSON

    # $var contains:
    {
        type    => 'show_attribute',
        data    => {
            casetype_id =. 44,
        }
    }

Returns a JSON hash containing the type and data for this object.

=cut

sub TO_JSON {
    my $self    = shift;

    my $json    = {
        type    => $self->type,
        data    => $self->data,
    };

    return $json;
}

=head1 REQUIRED METHODS

=head2 integrity_verified

Makes sure the integrity of this action object is verified, so this action can be loaded in the rule object

=cut

requires 'integrity_verified';

sub _populate_validation_results {
    shift;
    return shift;
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Backend::Rules::Rule> L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Install>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
