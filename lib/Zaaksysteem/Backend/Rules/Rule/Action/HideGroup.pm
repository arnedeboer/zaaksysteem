package Zaaksysteem::Backend::Rules::Rule::Action::HideGroup;

use Moose;
use namespace::autoclean;

use Moose::Util::TypeConstraints;
use Zaaksysteem::Tools;

with 'Zaaksysteem::Backend::Rules::Rule::Action';

=head1 NAME

Z::B::Rules::Rule::Action::HideGroup - Hide an entire group of fields

attribute.bjsdf_asdla
price
case_result

=head1 SYNOPSIS

=head1 DESCRIPTION

This specific action set a value on an attribute

=head1 ATTRIBUTES

=head2 attribute_name

=cut

has 'attribute_name'    => (
    is          => 'rw',
    isa         => 'Str',
    required    => 1,
);

# has 'group_name'        => (
#     is          => 'rw',
#     isa         => 'Str',
#     lazy        => 1,
#     default     => sub {
#         return shift->attribute_name;
#     }
# );

=head2 related_attributes

=cut

has 'value'             => (
    is          => 'rw',
);

has 'related_attributes'    => (
    is      => 'rw',
    isa     => 'ArrayRef',
    default => sub { [] },
);

=head2 related_text_blocks

Array containing a list of text blocks in this group.

=cut

has 'related_text_blocks'    => (
    is      => 'rw',
    isa     => 'ArrayRef',
    default => sub { [] },
);


=head2 _data_attributes

isa: Array

List of attributes to show in data

=cut

has '_data_attributes' => (
    is      => 'rw',
    isa     => 'ArrayRef',
    default => sub {
        my $self = shift;

        return [qw/attribute_name related_attributes related_text_blocks/];
    }
);

sub integrity_verified { return 1; }

sub _populate_validation_results {
    my $self            = shift;
    my $result_object   = shift;
    my $params          = shift;

    if ($self->type eq 'hide_group') {
        ### In our rule engine, it is possible to hide an attribute, and show it in another rule. With one
        ### exception: when it is hidden by a group. Make sure the ShowHide module (for hiding single attributes) know
        ### that it got hidden by the group method
        $result_object->_hidden_attributes_metadata->{'hide_group'}->{$_} = 1 for @{ $self->related_attributes };
        $result_object->_hidden_text_blocks_metadata->{'hide_group'}->{$_} = 1 for @{ $self->related_text_blocks };
        $result_object->hidden_groups([ grep({ $_ ne $self->attribute_name } @{ $result_object->hidden_groups }), $self->attribute_name ]);

        $result_object->_remove_active_attributes(@{ $self->related_attributes });
        $result_object->_remove_active_text_blocks(@{ $self->related_text_blocks });

        $result_object->_add_hidden_attributes(@{ $self->related_attributes });
        $result_object->_add_hidden_text_blocks(@{ $self->related_text_blocks });
    }

    if ($self->type eq 'show_group') {
        delete $result_object->_hidden_attributes_metadata->{'hide_group'}->{$_} for @{ $self->related_attributes };
        delete $result_object->_hidden_text_blocks_metadata->{'hide_group'}->{$_} for @{ $self->related_text_blocks };

        $result_object->hidden_groups([ grep({ $_ ne $self->attribute_name } @{ $result_object->hidden_groups }) ]);

        ### In our rule engine, it is possible to hide an attribute, and show it in another rule. With one
        ### exception: when it is hidden by a group. Make sure we do not show kenmerken when it is hidden by a rule
        ### (for hiding single attributes)
        my @related_attr;
        for (@{ $self->related_attributes }) {
            next if $result_object->_hidden_attributes_metadata->{'hide'}->{$_};

            push(@related_attr, $_);
        }
        $result_object->_add_active_attributes(@related_attr);

        my @related_text_blocks;
        for (@{ $self->related_text_blocks }) {
            next if $result_object->_hidden_text_blocks_metadata->{'hide'}->{$_};

            push(@related_text_blocks, $_);
        }

        $result_object->_add_active_text_blocks(@related_text_blocks);
    }
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Backend::Rules> L<Zaaksysteem::Manual>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 integrity_verified

TODO: Fix the POD

=cut

