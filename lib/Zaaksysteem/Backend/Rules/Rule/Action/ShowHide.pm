package Zaaksysteem::Backend::Rules::Rule::Action::ShowHide;

use Moose;
use namespace::autoclean;

use Zaaksysteem::Tools;

with 'Zaaksysteem::Backend::Rules::Rule::Action';

=head1 NAME

Z::B::Rules::Rule::Action::ShowHide - ShowHide functionality of rules

=head1 SYNOPSIS

=head1 DESCRIPTION

This specific action will show or hide rules.

=head1 ATTRIBUTES

=head2 attribute_name

=cut

has 'attribute_name'    => (
    is          => 'rw',
    isa         => 'ValidAttribute',
    required    => 1
);

=head2 _data_attributes

isa: Array

List of attributes to show in data

=cut

has '_data_attributes' => (
    is      => 'rw',
    isa     => 'ArrayRef',
    default => sub { return [qw/attribute_name/]; }
);

=head2 integrity_verified

Validate if the rule is valid

=cut

sub integrity_verified { return 1; }

sub _populate_validation_results {
    my $self            = shift;
    my $result_object   = shift;
    my $params          = shift;

    if ($self->type eq 'hide_attribute') {
        ### Make sure the show_group function does not show this element again
        $result_object->_hidden_attributes_metadata->{'hide'}->{$self->attribute_name} = 1;

        $result_object->_remove_active_attributes($self->attribute_name);

        $result_object->_add_hidden_attributes($self->attribute_name);
    }
    elsif ($self->type eq 'show_attribute') {
        delete $result_object->_hidden_attributes_metadata->{'hide'}->{$self->attribute_name};

        ### Do not show an attribute when it is already hidden by a group hide
        unless ($result_object->_hidden_attributes_metadata->{'hide_group'}->{$self->attribute_name}) {
            $result_object->_add_active_attributes($self->attribute_name);
        }
    }
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Backend::Rules> L<Zaaksysteem::Manual>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
