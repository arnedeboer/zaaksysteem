package Zaaksysteem::Backend::Rules::Rule::Action::SetValue;
use Moose;
use namespace::autoclean;

use Moose::Util::TypeConstraints;
use Zaaksysteem::Tools;
use Zaaksysteem::Types qw/Boolean/;

with qw(
    MooseX::Log::Log4perl
    Zaaksysteem::Backend::Rules::Rule::Action
    Zaaksysteem::Backend::Rules::Rule::AttributesValidation
);

subtype 'ValidExtendedAttribute',
    as 'Str',
    where { $_ =~ /^(?:attribute\.\w+|price|case_result|case\.price|case\.result)$/ };

=head1 NAME

Z::B::Rules::Rule::Action::SetValue - Set a value on an attribute

attribute.bjsdf_asdla
price
case_result

=head1 SYNOPSIS

=head1 DESCRIPTION

This specific action set a value on an attribute

=head1 ATTRIBUTES

=head2 attribute_name

=cut

has 'attribute_name'    => (
    is          => 'rw',
    isa         => 'ValidExtendedAttribute',
    required    => 1,
);


=head2 value

=cut

has 'value'             => (
    is          => 'rw',
);

has 'can_change'        => (
    is          => 'rw',
    isa         => Boolean,
    coerce      => 1,
);

has 'price'             => (
    is          => 'rw',
);

has 'set_case_result'   => (
    is          => 'rw',
);

=head2 _data_attributes

isa: Array

List of attributes to show in data

=cut

has '_data_attributes' => (
    is      => 'rw',
    isa     => 'ArrayRef',
    lazy    => 1,
    default => sub {
        my $self = shift;

        if ($self->attribute_name =~ /^(?:attribute\.\w+|case.result|(?:case\.)?price)$/) {
            return [qw/attribute_name value can_change attribute_type/];
        }
    }
);

sub BUILD {
    my $self            = shift;

    $self->_validate_values;
}

sub integrity_verified { return 1; }

sub _validate_values {
    my $self        = shift;

    if ($self->attribute_name =~ /^(?:case\.)?price$/) {
        $self->validate_price($self->value);
    } elsif (
        $self->attribute_name =~ /^attribute\.$/
    ) {
        throw(
            'rules/rule/action/set_value/invalide_values',
            'No values set in attribute "value"'
        ) unless $self->value;
    } elsif ($self->attribute_name eq 'case_result' || $self->attribute_name eq 'case.result') {
        $self->attribute_name('case.result');
    }
}

sub _populate_validation_results {
    my $self            = shift;
    my $result_object   = shift;
    my $params          = shift;


    $params->{$self->attribute_name} =
        $result_object->changes->{$self->attribute_name} = $self->value;

    $result_object->changeable_attributes->{$self->attribute_name} = $self->can_change;

    if ($self->log->is_trace) {
        $self->log->trace(
            sprintf(
                "Setting %s to %s see changes here: %s",
                $self->attribute_name, $self->value,
                dump_terse($result_object->changes)
            )
        );
    }


    return $result_object;
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Backend::Rules> L<Zaaksysteem::Manual>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 BUILD

TODO: Fix the POD

=cut

=head2 integrity_verified

TODO: Fix the POD

=cut

