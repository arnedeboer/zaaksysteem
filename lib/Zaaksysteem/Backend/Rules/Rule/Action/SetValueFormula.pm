package Zaaksysteem::Backend::Rules::Rule::Action::SetValueFormula;

use Moose;
use namespace::autoclean;

use Moose::Util::TypeConstraints;

use Zaaksysteem::Tools;

with qw[
    Zaaksysteem::Backend::Rules::Rule::Action
    Zaaksysteem::Backend::Rules::Rule::AttributesValidation
    MooseX::Log::Log4perl
];

=head1 NAME

Zaaksysteem::Backend::Rules::Rule::Action::SetValueFormula - Dynamic value setting via rules

=head1 SYNOPSIS

=head1 DESCRIPTION

This specific action set a dynamically calculated value on an attribute

=head1 TYPES

=head2 ValidExtendedFormulaAttribute

Validates a pseudo-attributename so we don't set values on unsupported aspects
of a case.

=cut

subtype 'ValidExtendedFormulaAttribute',
    as 'Str',
    where { $_ =~ /^(?:attribute\.\w+|price|case\.price)$/ };

=head1 ATTRIBUTES

=head2 attribute_name

Holds a symbolic reference (magic string) to the target attribute for which
this action should set a value.

=cut

has attribute_name => (
    is => 'rw',
    isa => 'ValidExtendedFormulaAttribute',
    required => 1
);

=head2 value

State container for the replacement value of attributes on a case.

=cut

has formula => (
    is => 'rw',
);

=head2 _data_attributes

Array of attributes to show in data.

=cut

has _data_attributes => (
    is      => 'rw',
    isa     => 'ArrayRef',
    default => sub {
        return [qw[attribute_name attribute_type formula]],
    }
);

=head1 METHODS

=cut

sub BUILD {
    my $self = shift;

    $self->_validate_values;
}

=head2 integrity_verified

Hook for the rule verification checks. See
L<Zaaksysteem::Backend::Rules::Rule::Action> for the interface description.

=cut

sub integrity_verified { my $self = shift;  return $self->formula ? 1 : 0 };

=head2 _validate_values

Private method that does the actual validation for values. Executed at during
object construction.

=head3 Exceptions

=over 4

=item rules/rule/action/set_value/invalid_values

Thrown when validation is unable to find a value to validate.

=back

=cut

sub _validate_values {
    my $self = shift;

    unless ($self->formula) {
        throw(
            'rules/rule/action/set_value/invalid_formula',
            'No values set in attribute "formula"'
        );
    }
}

=head2 _populate_validation_results

Populates a validation result object with the local value.

=cut

sig _populate_validation_results => 'Object, HashRef';

sub _populate_validation_results {
    my ($self, $result_object, $params) = @_;

    my $ztt = Zaaksysteem::ZTT->new()->add_context($params);

    my $rv = $ztt->process_template(sprintf(
        '[[ %s ]]',
        $self->formula,
    ))->string;

    $self->validate_price($rv);

    if ($self->log->is_trace) {
        $self->log->trace(
            sprintf(
                'Regel "%s" met formule "%s" heeft als uitkomst "%s" voor attribuut "%s" met params %s',
                $self->label, $self->formula,
                $rv,          $self->attribute_name,
                dump_terse($params)
            )
        );
    }

    $params->{ $self->attribute_name } = $rv;
    $result_object->changes->{ $self->attribute_name } = $rv;

    $result_object->changeable_attributes->{$self->attribute_name} = 0;

    return $result_object;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Backend::Rules>, L<Zaaksysteem::Manual>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
