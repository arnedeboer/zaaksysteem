package Zaaksysteem::Backend::Rules::Rule::Condition::RequestorType;

use Moose::Role;
use Zaaksysteem::Tools;


=head1 NAME

Zaaksysteem::Backend::Rules::Rule::Condition::RequestorType - Handles customer properties of casetype

=head1 SYNOPSIS

=head1 DESCRIPTION

=cut

after 'BUILD' => sub {
    my $self        = shift;

    if ($self->attribute eq 'aanvrager') {
        my $inverse_betrokkenemap = {
            'Natuurlijk persoon' => 'natuurlijk_persoon',
            'Niet natuurlijk persoon' => 'bedrijf',
            'Behandelaar' => 'medewerker',
            'Organisatorische eenheid' => 'org_eenheid',
        };

        for (my $i=0; $i < @{ $self->values }; $i++) {
            if ($self->values->[$i]) {
                $self->values->[$i] = $inverse_betrokkenemap->{ $self->values->[$i] };
            }

            ### Delete null values in array
            if (!$self->values->[$i]) {
                delete $self->values->[$i];
            }
        }


        ### Ok, special case: when we got a value "bedrijf", then also set "medewerker"
        if (grep({ $_ eq 'bedrijf' } @{ $self->values }) && !grep({ $_ eq 'medewerker' }  @{ $self->values })) {
            push(@{ $self->values }, 'medewerker');
        }


        ### Ok, ZS-4494: when there is no array, always validate true. Backwards compatible with
        ### Tubular and Documentairly.
        if (!@{ $self->values }) {
            $self->validates_true(1);
        }

        $self->attribute('case.requestor.subject_type');
    }
};

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Backend::Rules> L<Zaaksysteem::Manual>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
