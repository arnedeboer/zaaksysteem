package Zaaksysteem::Backend::Auth::UserEntity::Component;

use Moose;

use JSON;

BEGIN { extends 'DBIx::Class' }

sub TO_JSON {
    my $self = shift;

    return {
        id => $self->id,
        source_interface_id => $self->get_column('source_interface_id'),
        source_identifier => $self->source_identifier,
        username => $self->username
    };
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 TO_JSON

TODO: Fix the POD

=cut

