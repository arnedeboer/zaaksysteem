package Zaaksysteem::Backend::Auth::UserEntity::Component;
use Moose;

=head1 NAME

Zaaksysteem::Backend::Auth::UserEntity::Component

=cut

use JSON;

BEGIN { extends 'DBIx::Class' }

=head2 TO_JSON

TODO: Fix the POD

=cut

sub TO_JSON {
    my $self = shift;

    return {
        id                  => $self->id,
        source_interface_id => $self->get_column('source_interface_id'),
        source_identifier   => $self->source_identifier,
        username            => $self->username
    };
}

=head2 active

Checks whether the user entity is active or not

=cut

sub active {
    my $self = shift;

    if ($self->date_deleted && $self->date_deleted <= DateTime->now()) {
        return 0;
    }
    return 1;
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

