package Zaaksysteem::Backend::Rules;

use Moose;
use Zaaksysteem::Tools;
use Clone qw/clone/;

use Zaaksysteem::Backend::Rules::ValidationResults;

with qw/
    MooseX::Log::Log4perl
    Zaaksysteem::Backend::Rules::Casetype
/;

=head1 NAME

Zaaksysteem::Backend::Rules - Zaaksysteem Rules engine

=head1 SYNOPSIS

=head1 DESCRIPTION

Zaaksysteem Rules engine, in charge of generating decision trees, and manipulating
contexts (like a case) according to anwers on these decissions.

For now, this object has support for generating a decision tree which can be used
with our mintjs frontend framework.

=head2 MODULE LAYOUT

    Zaaksysteem::Backend::Rules;    # contains one or more rules:
        Zaaksysteem::Backend::Rule  # contains conditions and actions

            Zaaksysteem::Backend::Rules::Rule::Condition   # contains different kind of conditions
                Zaaksysteem::Backend::Rules::Rule::Condition::Confidentiality
                Zaaksysteem::Backend::Rules::Rule::Condition::Contactchannel
                Zaaksysteem::Backend::Rules::Rule::Condition::PaymentStatus
                Zaaksysteem::Backend::Rules::Rule::Condition::Properties
                Zaaksysteem::Backend::Rules::Rule::Condition::RequestorType
                Zaaksysteem::Backend::Rules::Rule::Condition::Zipcode

            Zaaksysteem::Backend::Rules::Rule::Action      # Contains different type of actions
                Zaaksysteem::Backend::Rules::Rule::Action::Pause
                Zaaksysteem::Backend::Rules::Rule::Action::SetValue
                Zaaksysteem::Backend::Rules::Rule::Action::ShowHide

=head1 REQUIRED ATTRIBUTES

=head2 source_type

Returns the source type of this rule, mostly this would be 'casetype'

=cut

has 'source_type' => (
    is      => 'rw',
    lazy    => 1,
    builder => '_build_source_type'
);

sub _build_source_type {}

=head2 source

The source from where we generate our rules from, for now, this would be L<Zaaksysteem::DB::ZaaktypeRegels>

=cut

has 'source'    => (
    is      => 'rw',
);

=head2 source_params

Specific source parameters

    $self->source_params( { milestone_number => 4 });

B<Options>

=over 4

=item milestone_number

The phase number the rules should be generated for

=back

=cut

has 'source_params'     => (
    is      => 'rw',
    isa     => 'HashRef',
    default => sub { {}; },
);

=head2 rules_params

=cut

has 'rules_params'     => (
    is      => 'rw',
    isa     => 'HashRef',
    default => sub { {}; },
);

=head2 rules

The actual list of rules.

=cut

has 'rules'     => (
    is      => 'rw',
    isa     => 'ArrayRef',
    lazy    => 1,
    default => sub { shift->_build_tree; },
);

sub has_rules {
    my $self = shift;
    return @{$self->rules} ? 1 : 0;
}

=head1 CONSTRUCTION

After construction, call $self->rules once, to get his object inflated

=cut

sub BUILD {
    my $self        = shift;

    if ($self->source) {
        $self->rules;
    }
}

=head1 METHODS

=head2 validate

Arguments: [ \%PARAMS ]

    my $validation_result = $rules->validate(
        {
            'case.casetype.designation_of_confidentiality' => '-',
            'case.casetype.extension' => 'Nee',
            'case.casetype.lex_silencio_positivo' => 'Nee',
            'case.casetype.node.id' => 626,
            'case.casetype.penalty' => 'Nee',
            'case.casetype.publication' => 'Nee',
            'case.casetype.registration_bag' => 'Nee',
            'case.casetype.suspension' => 'Nee',
            'case.casetype.wkpb' => 'Nee',
            'case.casetype.objection_and_appeal' => 'Nee',
            'case.channel_of_contact' => 'email',
            'case.confidentiality' => 'public',
            'case.number_status' => 1,
            'case.payment_status' => undef,
            'case.requestor.house_number' => 42,
            'case.requestor.subject_type' => 'natuurlijk_persoon',
            'case.requestor.zipcode' => '1011PZ',
            'attribute.aantal_kentekens' => '1',
            'attribute.kenteken' => '63-SPG-4'
            'attribute.extra_kenteken1' => '',
            'attribute.extra_kenteken2' => '123-aa-bb',
        }
    );

    print "Visible: " . join(', ',  @{ $validation_result->visible_attributes });
    print "Hidden: " . join(', ',  @{ $validation_result->hidden_attributes });

    ### Prints
    # Visible: attribute.aantal_kentekens, attribute.kenteken
    # Hidden: attribute.extra_kenteken1, attribute.extra_kenteken2

Will return the validation results according to the given parameters, or returns
the last known validation profile.

=cut

has '_validate' => (
    is      => 'rw',
    isa     => 'Maybe[Zaaksysteem::Backend::Rules::ValidationResults]',
);

sub validate {
    my $self            = shift;
    my ($params)        = @_;

    return $self->_validate unless $params;

    return $self->_validate(
        $self->_generate_validation(
            $self->_validate_prepare_params($params)
        )
    );
}

sub _validate_prepare_params    { shift; return shift; }

=head2 _generate_validation

Ask every rule if it validates, and when true, give information about the
actions

=cut

sub _generate_validation {
    my $self            = shift;
    my $params          = shift;

    my ($visible_attrs, $visible_text_blocks);
    if ($params->{'case.number_status'}) {
        $visible_attrs  = $self->_get_list_of_visible_attributes_for_number_status(
            $params->{'case.number_status'}
        );
        $visible_text_blocks  = $self->_get_list_of_visible_text_blocks_for_number_status(
            $params->{'case.number_status'}
        );
    } else {
        $visible_attrs = $self->_get_list_of_visible_attributes;
        $visible_text_blocks = $self->_get_list_of_visible_text_blocks;
    }

    ### Define result object to work on
    my $result_object = Zaaksysteem::Backend::Rules::ValidationResults->new(
        'active_attributes'  => $visible_attrs,
        'active_text_blocks' => $visible_text_blocks,
    );

    return $result_object if !$self->has_rules;

    ### Clone params, we change these on our way down the actions, when a set value
    ### changes something, we want to make sure the original params stay intact
    my $working_params  = clone($params);

    ### Loop over rules and validate each and everyone of them
    for my $rule (@{ $self->rules }) {
        $rule->validate($result_object, $working_params);
    }

    return $result_object;
}

=head2 integrity

Arguments: @RULES

    $integrity = Zaaksysteem::Backend::Rules->integrity;

    if ($integrity->success) {
        print "NO PROBLEMS"
    } else {
        for my $problem (@{ $integrity->problems }) {
            print "Rule number: " . $problem->{rule_number} . ', id:' . $problem->{id}
                . ', has problem: ' . $problem->{message};
        }
    }

Checks a list of given rules for problems, when a problem is found, an array will be filled with
extra information about this problem.

=cut

has '_integrity' => (
    'is'        => 'rw',
    'isa'       => 'Zaaksysteem::Backend::Rules::Integrity',
    'predicate' => 'has_integrity',
);

sub integrity {}

=head2 TO_JSON

Returns the JSON representation of these rules

=cut

sub TO_JSON {
    my $self        = shift;

    return $self->rules;
}

=head1 INTERNAL METHODS

=head2 _build_tree

Arguments: none

Return value: $BOOL_TRUE

    $self->_build_tree;

Builds the decision tree from the C<source> parameters.

=cut

sub _build_tree {}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Backend::Rules> L<Zaaksysteem::Manual>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 BUILD

TODO: Fix the POD

=cut

