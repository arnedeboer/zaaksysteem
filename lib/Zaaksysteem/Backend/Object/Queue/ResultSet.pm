package Zaaksysteem::Backend::Object::Queue::ResultSet;

use Moose;

use Zaaksysteem::Tools;
use Zaaksysteem::Types qw[UUID];

extends 'Zaaksysteem::Backend::ResultSet';

=head1 NAME

Zaaksysteem::Backend::Object::Queue::ResultSet - Additional behavior for the
C<queue> table

=head1 DESCRIPTION

=head1 METHODS

=head2 create_item

Wrapper for L<DBIx::Class::ResultSet/create>. This method mostly exists to
provide a stable API for the module should the underlying storage change.

    my $item = $queue_rs->create_item('my_queued_item_type_name', {
        object_id => UUID,
        label => 'Str',
        data => { ... }
    });

=cut

define_profile create_item => (
    required => {
        label => 'Str'
    },
    optional => {
        object_id => UUID,
        data => 'HashRef',
        priority => 'Int',
    },
    defaults => {
        priority => 1000,
    },
);

sig create_item => 'Str, ?HashRef';

sub create_item {
    my $self = shift;
    my $type = shift;
    my $opts = assert_profile(shift || {})->valid;

    $opts->{ type } = $type;

    $self->create($opts)->discard_changes;
}

=head2 create_items

Wrapper for L<DBIx::Class::ResultSet/populate>. This method mostly exists to
provide a stable API for the module should the underlying storage change.

    my $item = $queue_rs->create_items(
        'my_queued_item_type_name',
        [
            {
                object_id => UUID,
                label     => 'Str',
                data      => {...}
            }
        ]
    );

=cut

=head2 _deflate_column_value

Deflates a value as described by the schema file

It is a similar, simplified, function as
L<DBIx::Class::InflateColumn/_deflate_column>. So we
don't need to maintain two seperate decoding/deflating mechanisms.

=cut

sub _deflate_column_value {
    my $self = shift;
    my $col = shift;
    my $val = shift;

    my $info = $self->result_source->column_info($col);

    return $val unless $info;
    return $val unless exists $info->{_inflate_info}{deflate};

    return $info->{_inflate_info}{deflate}->($val, $self);
}

=head2 create_items

    my @items = ( { label => "foo", data => { foo => bar }, object_id => UUID }, {} );
    $resultset->create_items($type, @items);

Creates queue items in bulk (think thousands).

=cut

define_profile create_items => (
    required => {
        label => 'Str'
    },
    optional => {
        object_id => UUID,
        data => 'HashRef',
        priority => 'Int',
    },
    defaults => {
        priority => 1000,
    },
);

sig create_items => 'Str, @HashRef';

sub create_items {
    my $self = shift;
    my $type = shift;

    # Force to scalar, otherwise you get the keys from the valid hashref
    my @rows = map { scalar assert_profile($_)->valid } @_;

    @rows = map { [$type, $_->{object_id}, $self->_deflate_column_value('data', $_->{data}), $_->{label}, $_->{priority}] } @rows;
    $self->populate([
        [qw(type object_id data label priority)],
        @rows,
    ]);
    return;

}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
