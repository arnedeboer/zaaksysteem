package Zaaksysteem::Backend::Object::Queue::Component;

use Moose;

use JSON;

use Zaaksysteem::Tools;

extends 'DBIx::Class';

=head1 NAME

Zaaksysteem::Backend::Object::Queue::Component - Behaviors for queue item
instances

=head1 DESCRIPTION

=head1 METHODS

=head2 broadcast

Sends a C<zs_action_queued> notification to the connected PostgreSQL database,
alerting any listening zs-queue daemons.

=cut

sig broadcast => 'URI';

sub broadcast {
    my $self = shift;
    my $base_uri = shift;

    if ($self->is_changed) {
        throw('queue/broadcast/item_not_saved', sprintf(
            'Queue item "%" cannot be broadcast, record has changed fetch',
            $self->simplify
        ));
    }

    my $url = sprintf('%sapi/queue/%s/run', $base_uri->as_string, $self->id);

    $self->result_source->schema->storage->dbh_do(sub {
        my ($storage, $dbh) = @_;

        $dbh->do(sprintf(
            "NOTIFY zs_action_queued, %s",
            $dbh->quote(JSON->new->encode({ url => $url }))
        ));
    });
}

=head2 is_finished

Returns true if the item has succesfully been run.

=cut

sub is_finished {
    return shift->status eq 'finished';
}

=head2 is_failed

Returns true if the item has met a failure while being run.

=cut

sub is_failed {
    return shift->status eq 'failed';
}

=head2 is_pending

Returns true if the item is still pending to be run.

=cut

sub is_pending {
    return shift->status eq 'pending';
}

=head2 is_running

Returns true if the item is currently being run.

=cut

sub is_running {
    return shift->status eq 'running';
}

=head2 is_waiting

Returns true if the item is currently waiting to be run. This implies that
another queue item or process is governing the item, and it should not be run
independently.

=cut

sub is_waiting {
    return shift->status eq 'waiting';
}

=head2 is_ready

Returns true if the item is ready to be run. (C<pending> or C<waiting>).

=cut

sub is_ready {
    my $self = shift;

    return $self->is_pending || $self->is_waiting;
}

=head2 object_data

Convenience method for retrieving a
L<Zaaksysteem::Backend::Object::Data::Component> instance.

=head3 Exceptions

=over 4

=item queue/run/object_data_required

Thrown when queue item has no related object_data row

=back

=cut

sub object_data {
    my $self = shift;

    unless ($self->get_column('object_id')) {
        throw('queue/run/object_data_required', sprintf(
            'Running queue item "%s" requires a related object_data instance',
            $self->stringify
        ));
    }

    return $self->object_id;
}

=head2 subject

Convenience method for retrieving a
L<Zaaksysteem::Backend::Subject::Component> instance.

=cut

sub subject {
    my $self = shift;

    return unless exists $self->data->{ _subject_id };

    return $self->result_source->schema->resultset('Subject')->find(
        $self->data->{ _subject_id }
    );
}

=head2 stringify

Returns a human readable(-ish) string with most identifiying information of
the queued item, formatted like C<type(id): label>.

=cut

sub stringify {
    my $self = shift;

    return sprintf(
        '%s(%s): %s',
        $self->type,
        $self->id // '<not synced>',
        $self->label
    );
}

=head2 TO_JSON

JSON Serialization helper for the Queue ZAPI controller.

=cut

sub TO_JSON {
    my $self = shift;

    my %fields = map { $_ => $self->get_column($_) } qw[
        id
        label
        object_id
        status
        type
    ];

    $fields{ data } = $self->data;

    return \%fields;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
