package Zaaksysteem::Backend::Object::Data::Roles::Case;

use Moose::Role;

use Zaaksysteem::Tools;

=head1 NAME

Zaaksysteem::Backend::Object::Data::Roles::Case - Object behaviors for the
'case' object_class instances

=head1 SYNOPSIS

Implements specific code behaviors exclusive to object instances with class
'case'.

=head1 CONSTANTS

=head2 ATTRIBUTES

This constant is an ARRAYREF containing a listing of magic string names that
should be included in the free-text vector.

=cut

use constant ATTRIBUTES => [qw[
    case.requestor.full_name
    case.requestor.family_name
    case.requestor.bsn
    case.requestor.email
    case.requestor.name
    case.recipient.full_name
    case.recipient.family_name
    case.recipient.bsn
    case.recipient.email
    case.assignee
    case.assignee.email
    case.assignee.phone_number
    case.coordinator

    case.number
    case.subject
    case.date_of_registration
    case.date_of_completion

    case.casetype.name
]];

=head2 ACTIONS

This constant defines an array of actions that can be performed on cases.

Each action is encoded as a hashref, like the following:

    { slug => '...' }

=cut

use constant ACTIONS => [
    { slug => 'allocate' },
    { slug => 'acquire' },
    { slug => 'suspend' },
    { slug => 'resume' },
    { slug => 'prolong' },
    { slug => 'relate' },
    { slug => 'manage' },
    { slug => 'destroy' },
    { slug => 'publish' },
    { slug => 'export' }
];

=head1 METHODS

=head2 text_vector_terms

This method overrides L<Zaaksysteem::Backend::Object::Data::Component>'s
C<text_vector_terms> method, since case-like objects should not have a
generic free-text vector. See the C<ATTRIBUTES> constant for a listing of
magic strings we want to include in the vector.

=cut

override text_vector_terms => sub {
    my $self = shift;
    my @terms;

    for my $attr (@{ $self->object_attributes }) {
        # Yeah, this is a bit nasty, but we can't call
        # $object->get_attribute_by_name yet, so it'll have to do.
        # TODO refactor when ZS-2178 lands in quarterly.
        next unless grep { $attr->name eq $_ } @{ ATTRIBUTES() };

        my $term = $attr->vectorize_value;
        push @terms, $term if defined $term;
    }

    return @terms;
};

=head2 get_source_object

This method overrides L<Zaaksysteem::Backend::Object::Data::Component>'s
C<get_source_object> to retrieve a C<Zaak> object based on the C<object_id> of
this row.

=cut

override get_source_object => sub {
    my $self = shift;

    my $object_id = $self->object_id;
    my $zaak = $self->result_source->schema->resultset('Zaak')->find($object_id);

    return $zaak;
};

=head2 documents

Returns a resultset with all accepted, non-destroyed/deleted files currently
associated with the case this method is invoked on.

    my $files_rs = $case_object->documents;

The search parameters for finding the files can be configured by passing
this method a hashlist of additional or overriding parameters;

    my $unaccepted_files = $case_object->documents(accepted => 0);

=cut

sig documents => '%Any';

sub documents {
    my $self = shift;

    my %conditions = (
        case_id => $self->object_id,
        accepted => 1,
        date_deleted => undef,
        destroyed => 0,
        @_
    );

    return $self->result_source->schema->resultset('File')->search_rs(
        \%conditions,
        { prefetch => [qw[filestore_id metadata_id]] }
    );
}

=head2 TO_JSON

This method wraps L<Zaaksysteem::Backend::Object::Data::Component/TO_JSON> and
adds a key, C<actions>, to the original method's return value. The values for
the added key represent possible actions that can be performed on objects of
this type.

=cut

around TO_JSON => sub {
    my $orig = shift;
    my $self = shift;

    my $retval = $self->$orig(@_);

    $retval->{ actions } = [ map { $_->{ slug } } @{ ACTIONS() } ];

    return $retval;
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
