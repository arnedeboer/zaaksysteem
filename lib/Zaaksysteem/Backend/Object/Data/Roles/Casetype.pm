package Zaaksysteem::Backend::Object::Data::Roles::Casetype;

use Moose::Role;

=head1 NAME

Zaaksysteem::Backend::Object::Data::Roles::Casetype

=head1 SYNOPSIS

    my $object = $model->retrieve(uuid => $uuid_of_casetype);

    my $zaaktype = $object->get_source_object;

=head1 DESCRIPTION

This class merely exists so other bits of code don't need to muck about
with databases and resultsets too much.

=head1 METHODS

=head2 get_source_object

This method overrides
L<Zaaksysteem::Backend::Object::Data::Component/get_source_object> with a
concrete implementation.

=cut

override get_source_object => sub {
    my $self = shift;
    
    return $self->result_source->schema->resultset('Zaaktype')->find(
        $self->object_id
    );
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
