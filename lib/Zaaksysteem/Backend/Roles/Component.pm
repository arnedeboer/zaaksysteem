package Zaaksysteem::Backend::Roles::Component;

use Moose;

use Zaaksysteem::Tools;

BEGIN { extends qw/DBIx::Class/; }

=head1 NAME

Zaaksysteem::Backend::Roles::Component - Implement specific behaviors for
Roles rows.

=head1 DESCRIPTION

=head1 BACKWARDS COMPATIBILITY METHODS

Do not use the below methods, these are for backwards compatibilty only

=head2 ldapid

Returns the primary id for this group

=cut

sub ldapid {
    my $self = shift;
    warn ('bwcompat call to ldapid');

    return $self->id;
}

=head2 omschrijving

Backwards compatible call to "name"

=cut

sub omschrijving {
    my $self    = shift;
    warn ('bwcompat call to omschrijving');

    return $self->name;
}

has 'parent_id' => (
    is      => 'ro',
    lazy    => 1,
    default => sub {
        return shift->parent_group_id;
    }
);

sub TO_JSON {
    my $self    = shift;

    return {
        $self->get_columns,
        table     => 'Roles',
        $self->parent_id ? (parent_id => $self->parent_id->id) : (),
    };
}

=head1 ATTRIBUTES

=head2 depth_list

=cut

has 'parent_groups' => (
    is          => 'ro',
    lazy        => 1,
    default     => sub {
        my $self            = shift;

        my $parent_group    = $self->parent_group_id;

        ### Query all parent groups
        return [ $self->result_source->schema->resultset('Groups')->search(
            {
                id      => { in     => $parent_group->path }
            },
            {
                order_by    => { '-asc' => [\"array_length(me.path, 1)"] },
            }
        ) ];
    }

);


=head2 subjects

Arguments: none

    my $subjects_rs     = $groups->subjects;

Will retrieve subjects who belong to this role

=cut

has 'subjects' => (
    is          => 'ro',
    lazy        => 1,
    default     => sub {
        my $self        = shift;

        $self->result_source->schema->resultset('Subject')->search(
            {
                $self->id => \'= ANY (role_ids)',
            },
        );
    }
);

=head2 set_deleted

Arguments: none

    $role->set_deleted;

    ### Not needed:
    # $role->update

Only way to proper delete a role

=cut

sub set_deleted {
    my $self        = shift;

    ### Loop over subjects to remove role_ids
    $self->result_source->schema->txn_do(
        sub {
            for my $subject ($self->subjects->all) {
                next unless grep({ $_ == $self->id } @{ $subject->role_ids });

                my @role_ids = grep({ $_ != $self->id } @{ $subject->role_ids });
                $subject->role_ids(\@role_ids);
                $subject->update;
            }

            $self->delete;
        }
    );
}

=head2 update_role

Argumants: \%params

Return value: $UPDATED_ROW

    $newrole   = $newrole->update_role(
        {
            name            => 'Bestandelaar',
            description     => 'Bestandelaar',
        }
    );

=cut

define_profile 'update_role' => (
    required            => [],
    optional            => [qw/name description change_systemrole/],
    constraint_methods  => {
        name                => qr/^[\w\s_-]+$/,
        description         => qr/^[\w\s_-]+$/,
    }
);

sub update_role {
    my $self            = shift;
    my $params          = assert_profile(shift || {})->valid;

    throw(
        'roles/update_role/cannot_change_system_role',
        'Not authorized to alter systemrole, unless change_systemrole=1'
    ) unless ($params->{change_systemrole} || !$self->system_role);

    for my $key (qw/name description/) {
        $self->$key($params->{$key});
    }

    $self->update;
    return $self->discard_changes();
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

