package Zaaksysteem::Backend::File::Component;

use Moose;

use File::Basename;
use File::Temp qw[tempfile];
use File::Spec::Functions;
use File::MimeInfo::Magic;

use Encode qw[encode_utf8];

use Params::Profile;
use Zaaksysteem::Constants qw(MIMETYPES_ALLOWED DOCUMENT_STATUS);
use Zaaksysteem::Exception;
use Zaaksysteem::Tools;

use Zaaksysteem::Tools::Text;
use Zaaksysteem::Tools::Text::PDFHack;
use Zaaksysteem::Tools::Text::OpenDocument;
use Zaaksysteem::Tools::Text::HTML;

extends 'DBIx::Class';

with 'Zaaksysteem::Backend::File::MailDissector';

use Exception::Class (
    'Zaaksysteem::Backend::File::Component::Exception' => { fields => 'code' },
    'Zaaksysteem::Backend::File::Component::Exception::General' => {
        isa         => 'Zaaksysteem::Backend::File::Component::Exception',
        description => 'General exception',
        alias       => 'throw_general_exception',
    },
    'Zaaksysteem::Backend::File::Component::Exception::Parameter' => {
        isa         => 'Zaaksysteem::Backend::File::Component::Exception',
        description => 'Parameter exception',
        alias       => 'throw_parameter_exception',
    },
    'Zaaksysteem::Backend::File::Component::Exception::Logic' => {
        isa         => 'Zaaksysteem::Backend::File::Component::Exception',
        description => 'Logic error',
        alias       => 'throw_logic_exception',
    },
);

=head2 $file->update_properties

Updates the properties of a File database entry.

=head3 Arguments

=over

=item subject [required]

The subject executing this call.

=item deleted [optional]

Boolean. When set to true it will set date_deleted to now() and deleted_by to
the given subject. The opposite, setting to false, will 'undelete' the file.

Undeleted files will always get placed in the root. Any directories set before
deletion are no longer present.

=item metadata [optional]

Update or create a file's metadata. See update_metadata POD for columns.

=item directory_id [optional]

Place this file in a directory.

=item accepted [optional]

Boolean. When set to false a rejection_reason is required detailing why this
file is being sent back to the queue. Setting to false will remove any case or
subject that is currently set.

=item rejection_reason [required if accepted set to false]

String detailing the reason why a document was not accepted.

=item publish_pip [optional]

Boolean, decides whether this file gets shown on the PIP or not.

=item publish_website [optional]

Boolean, decides whether this file may be exported outside of Zaaksysteem.

=item case_document_ids

List of zaaktype_kenmerken IDs

=back

=head3 Returns

Updated version of the File object.

=cut

Params::Profile->register_profile(
    method  => 'update_properties',
    profile => {
        required => [qw/
        /],
        optional => [qw/
            name
            case_id
            publish_pip
            publish_website
            accepted
            rejection_reason
            directory_id
            version
            root_file_id
            destroyed
            pip_owner
            user_has_zaak_beheer_permissions
            document_status
        /],
        constraint_methods => {
            document_status => DOCUMENT_STATUS,
            case_id         => qr/^[\d]+$/,
            accepted        => qr/^(0|1)$/,
        },
    }
);

sub update_properties {
    my $self = shift;
    my $opts = $_[0];

    my $dv = Params::Profile->check(
        params  => $opts,
    );

    # Various parameter contraints/checks
    my $valid = $dv->valid;

    my $user_has_zaak_beheer_permissions = delete $valid->{user_has_zaak_beheer_permissions};

    if ($dv->has_invalid) {
        my @invalid = join ',',$dv->invalid;
        throw_parameter_exception(
            code  => '/file/update_properties/invalid_parameters',
            error => "Invalid options given: @invalid",
        );
    }

    # Check subject seperately, as it would pollute the valid hash.
    if (!$opts->{subject}) {
        throw_parameter_exception(
            code  => '/file/update_properties/missing_parameters',
            error => "Missing options: subject",
        );
    }

    # Forbid altering deleted files unless the file is being restored or destroyed
    if ($self->date_deleted && !exists $opts->{deleted} && !exists $opts->{destroyed}) {
        throw_parameter_exception(
            code  => '/file/update_properties/cannot_alter_deleted',
            error => "Cannot alter a deleted file",
        );
    }

    # case_document_ids is expected to always be a list
    if ($opts->{case_document_ids} && !UNIVERSAL::isa($opts->{case_document_ids}, 'ARRAY')) {
        throw_parameter_exception(
            code  => '/file/update_properties/not_an_array',
            error => "case_document_ids can only be set as an array",
        );
    }

    # When a file is being destroyed, make sure any other existing versions are destroyed as well
    if (exists $opts->{destroyed} && $opts->{destroyed}) {

        throw('case/file/destroy', "Gebruiker heeft geen rechten om de prullenbak te legen")
            unless $user_has_zaak_beheer_permissions;

        my $root = $self->get_root_file;

        $root->files->update({destroyed => $opts->{destroyed}});
        $root->update({destroyed => $opts->{destroyed}});

        if($self->case) {
            $self->case->logging->trigger('case/document/remove', {
                component => 'documenten',
                data => {
                    case_id   => $self->case->id,
                    file_id   => $self->id,
                    file_name => sanitize_filename($self->name),
                }
            });
        } else {
            $self->result_source->schema->resultset('Logging')->trigger('file/remove', {
                component => 'documenten',
                data => {
                    file_id   => $self->id,
                    file_name => sanitize_filename($self->name),
                }
            });
        }
    }

    # As some things depend on a case being present we set it first if it is present
    # in the options. (As opposed to various if statements)
    if ($opts->{case_id} && !$self->case_id) {
        $self->update({case_id => $opts->{case_id}});
        $self->trigger('assign');
        delete $opts->{case_id};
    }

    # Update metadata
    if ($opts->{metadata}) {
        $self->update_metadata($opts->{metadata});
    }

    # Always add modified info
    $valid->{date_modified} = DateTime->now;
    $valid->{modified_by}   = $opts->{subject};

    # D::FV does not handle undef well. Check if a value is set in
    # valid, if not (but they key is set in the opts hash) set it
    # anyway.
    if (exists $opts->{directory_id} && !$valid->{directory_id}) {
        $valid->{directory_id} = $opts->{directory_id};
    }
    if (exists $opts->{publish_website} && !$valid->{publish_website}) {
        $valid->{publish_website} = 0;
    }
    if (exists $opts->{publish_pip} && !$valid->{publish_pip}) {
        $valid->{publish_pip} = 0;
    }

    # Restore or delete a file
    if (exists $opts->{deleted} && !$opts->{deleted}) {
        $valid->{date_deleted} = undef;
        $valid->{deleted_by}   = undef;

        if($self->case) {
            $self->case->logging->trigger('case/document/restore', {
                component => 'documenten',
                data => {
                    case_id => $self->case->id,
                    file_id => $self->id
                }
            });
        } else {
            $self->result_source->schema->resultset('Logging')->trigger('file/restore', {
                component => 'documenten',
                data => { file_id => $self->id }
            });
        }
    }
    elsif (exists $opts->{deleted} && $opts->{deleted}) {
        $valid->{date_deleted} = DateTime->now;
        $valid->{deleted_by}   = $opts->{subject};
        $valid->{directory_id} = undef;

        $self->case_documents->delete;

        if($self->case) {
            $self->case->logging->trigger('case/document/trash', {
                component => 'documenten',
                data => {
                    case_id => $self->case->id,
                    file_id => $self->id
                }
            });
        } else {
            $self->result_source->schema->resultset('Logging')->trigger('file/trash', {
                component => 'documenten',
                data => { file_id => $self->id }
            });
        }
    }

    # Reject a file; unset case_id and set accepted to false.
    if (exists $opts->{accepted} && !$opts->{accepted}) {
        ### ZS-1863: Make sure we do not "deny" a file which is already accepted...
        if ($self->accepted) {
            throw_logic_exception(
                code  => '/file/update_properties/cannot_unaccept',
                error => 'File is already accepted, cannot "unaccept"',
            );
        }


        $self->reject;

        $self->trigger('reject', {
            rejection_reason => $opts->{rejection_reason},
            reject_to_queue => $self->reject_to_queue,
            filename => $self->filename,
        });
        # When rejecting a PIP document, handle it as a deletion. There is no
        # requeueing for these types. Same goes for when 'reject_to_queue' is false.
        if (!$self->reject_to_queue) {
            $valid->{date_deleted} = DateTime->now;
            $valid->{deleted_by}   = $opts->{subject};
            $valid->{directory_id} = undef;
            $self->case_documents->delete;
        }
        # All other documents go back to the queue.
        else {
            if (!$valid->{rejection_reason}) {
                throw_logic_exception(
                    code  => '/file/update_properties/rejection_reason_required',
                    error => 'Rejection reason required when not accepting a file',
                );
            }
            $valid->{case_id}    = undef;
            $valid->{subject_id} = undef;
            $valid->{accepted}   = 'f';
        }
    }

    # Accept a file, unset any rejection_reason that might exist.
    elsif (exists $opts->{accepted}) {
        if (!$self->case_id) {
            throw_logic_exception(
                code  => '/file/update_properties/cannot_accept_unassigned',
                error => 'Cannot accept a document if it is not assigned to a case',
            );
        }
        $valid->{rejection_reason}  = undef;
        $valid->{is_duplicate_name} = 0;
        $valid->{is_duplicate_of}   = undef;

        $self->handle_accepted({properties => $opts});
    }

    # If a file is already accepted and is getting a name change OR
    # it is currently being accepted: automatically rename the file if there is
    # a duplicate. If a root_file_id is being set it would implicate a new version in
    # a series. Ignore duplicate name if so.
    if ($self->accepted && $valid->{name} || ($valid->{accepted} and !$valid->{root_file_id})) {
        my $name = $valid->{name} ? sanitize_filename($valid->{name}) : $self->name;
        $valid->{name} = $self->get_valid_filename($name);
    }

    if (!$self->accepted && !$valid->{accepted}) {
        $valid->{active_version} = 1;
    }

    # Assign a case
    if ($opts->{case_id}) {
        if ($self->case) {
            throw_logic_exception(
                code  => '/file/update_properties/case_already_defined',
                error => 'Cannot assign a new case when a case is already defined',
            );
        }
        # When assigning a case_id a previous rejection_reason should be cleared.
        $valid->{rejection_reason} = undef;
    }

    # If a case document is being assigned, set the defaults belonging to it.
    # This is done before the actual update so overrides can be passed right
    # away.
    if ($opts->{case_document_ids}) {
        $self->set_or_replace_case_documents(@{$opts->{case_document_ids}});

        # Only set defaults if a single document is being assigned.
        $self->apply_case_document_defaults;
    }

    my $result = $self->update($valid)->discard_changes;

    if($valid->{ accepted }) {
        $self->trigger('accept');
    }

    if($valid->{ name }) {
        $self->trigger('rename', {
            renamed_to => $valid->{ name }
        });

        ### Check if references files are now "in the clear"
        if ($valid->{name} ne $self->name) {
            my $duplicate_files = $self->file_is_duplicate_ofs;

            while (my $duplicate_file = $duplicate_files->next) {
                $duplicate_file->is_duplicate_of(undef);
                $duplicate_file->is_duplicate_name(0);
                $duplicate_file->update;
            }
        }
    }

    if(exists $valid->{ publish_website }) {
        $self->trigger($valid->{ publish_website } ? 'publish' : 'unpublish', {
            publish_to => 'website'
        });
    }

    if(exists $valid->{ publish_pip }) {
        $self->trigger($valid->{ publish_pip } ? 'publish' : 'unpublish', {
            publish_to => 'pip'
        });
    }

    # Update search_term name
    $result->update({search_term => $result->name});

    return $result;
}

=head2 $file->update_metadata

Update a File's metadata.

=head3 Parameters

All parameters are optional.

=over

=item description

=item document_category

=item trust_level

Valid: 'Openbaar', 'Beperkt openbaar', 'Intern', 'Zaakvertrouwelijk', 'Vertrouwelijk', 'Confidentieel', 'Geheim', 'Zeer geheim'

=item origin

Valid: 'Inkomend', 'Uitgaand', 'Intern'

=back

=cut

Params::Profile->register_profile(
    method  => 'update_metadata',
    profile => {
        required => [],
        optional => [qw/
            description
            document_category_parent
            document_category_child
            trust_level
            origin
        /],
    }
);

sub update_metadata {
    my $self = shift;
    my $opts = $_[0];

    my $valid = Params::Profile->check(
        params  => $opts,
    );
    if (!$valid->success) {
        throw_parameter_exception(
            code  => '/file/update_metadata/invalid_options',
            error => 'Invalid options given',
        );
    }

    # Check whether to create or update
    if (!$self->metadata_id) {
        my $md = $self->result_source->schema->resultset('FileMetadata')->create(
            $_[0]
        );
        $self->update({metadata => $md});
    }
    else {
        $self->metadata->update($_[0]);
    }
}

=head2 $file->update_file

Replace a file with a newer version.

=head3 Parameters

=over

=item new_file_path [required]

The new file's location.

=item original_name [required]

The name that will be set in the Filestore for this file. Not to be confused
with the File's name. This will be copied from the current File. This is required
due to not accidently setting the name to the UUID.

=item is_restore

Indicate whether or not this is a file being restored from a previous version.
When set, the version check (if older than latest, throw exception) is bypassed.

=back

=cut

Params::Profile->register_profile(
    method  => 'update_file',
    profile => {
        required => [qr/subject new_file_path original_name/],
        optional => [qr/is_restore/],
    }
);

sub update_file {
    my $self = shift;
    my $opts = $_[0];

    my $dv = Params::Profile->check(
        params  => $opts,
    );
    if ($dv->has_invalid) {
        my @invalid = join ',',$dv->invalid;
        throw_parameter_exception(
            code  => '/file/update_file/invalid_options',
            error => 'Invalid options given: @invalid',
        );
    }

    # Check if there is already a newer parent than the file being updated
    my $newest = $self->get_last_version;
    if ($newest->version != $self->version && !$opts->{is_restore}) {
        throw_parameter_exception(
            code  => '/file/update_file/not_last_version',
            error => sprintf (
                "File %s is at version %d, can't modify given version %d",
                ($self->name, $newest->version, $self->version)
            ),
        );
    }

    # Create new filestore entry
    my $file_path = $opts->{new_file_path};

    my $fs = $self->result_source->schema->resultset('Filestore')->filestore_create({
        original_name => $opts->{original_name},
        file_path     => $file_path,
    });

    my $file_properties = get_file_properties($opts->{original_name});

    # First copy the metadata, then copy the rest
    my $metadata;
    if ($self->metadata_id) {
        $metadata = $self->metadata->copy();
    }

    my $copy = $self->copy({
        name          => $file_properties->{name},
        extension     => $file_properties->{ext},
        metadata_id   => $metadata,
        directory_id  => $newest->directory_id,
        filestore_id  => $fs,
        root_file_id  => $self->root_file_id || $self->id, # The first file doesn't point to itself
        version       => $newest->version+1,
        date_created  => DateTime->now,
        created_by    => $opts->{subject},
        date_modified => DateTime->now,
        modified_by   => $opts->{subject},
    });

    my $creation_reason =  sprintf(
        "Document '%s' (versie %d) vervangen met document '%s' (versie %d)",
        (
            $self->name.$self->extension,
            $self->version,
            $fs->original_name,
            $copy->version,
        )
    );

    # Unset any foreign keys that may disappear on the old entry. (Directories, for example)
    $self->update({directory_id => undef});

    # Same for restoring files, except $self isn't always the last active file
    if ($opts->{is_restore}) {
        $newest->update({directory_id => undef});

        # Also update the creation_reason string
        $creation_reason = sprintf(
            "Document '%s' (versie %d) vervangen",
            ($newest->name, $newest->version, $self->name, $self->version, $opts->{subject})
        );
    }

    $copy->update({creation_reason => $creation_reason });

    ### Search all other versions, and unset root_file_id
    $copy->make_active_version;

    return $copy->discard_changes;
}

sub make_active_version {
    my $self                = shift;

    my $root_file_id        = $self->root_file_id;


    if ($root_file_id) {
        my $previous_versions   = $self->result_source->resultset->search(
            {
                '-or'   => [
                    { id => $root_file_id->id },
                    {
                        root_file_id    => $root_file_id->id,
                        id              => { '!=' => $self->id },
                    },
                ]
            }
        );

        while (my $old_version = $previous_versions->next) {
            $old_version->active_version(0);
            $old_version->update;
        }
    }

    $self->active_version(1);
    $self->update;

    return $self;
}

Params::Profile->register_profile(
    method  => 'update_existing',
    profile => {
        required => [qr/subject existing_file_id/],
    }
);

sub update_existing {
    my $self = shift;
    my $opts = $_[0];

    my $dv = Params::Profile->check(
        params  => $opts,
    );

    if ($dv->has_invalid) {
        my @invalid = join ',',$dv->invalid;
        throw_parameter_exception(
            code  => '/file/update_existing/invalid_options',
            error => 'Invalid options given: @invalid',
        );
    }

    my ($existing) = $self->result_source->resultset->search({id => $opts->{existing_file_id}});
    if (!$existing) {
        throw_parameter_exception(
            code  => '/file/update_existing/file_not_found',
            error => printf('Existing file with id %d not found', $opts->{existing_file_id}),
        );
    }
    if ($existing->accepted) {
        throw_logic_exception(
            code  => '/file/update_existing/existing_already_accepted',
            error => 'When using an existing file, the file cannot already be accepted',
        );
    }

    my $last_version = $self->get_last_version;

    my $result = $existing->update_properties({
        subject         => $opts->{subject},
        version         => $last_version->version+1,
        root_file_id    => $last_version->root_file_id || $last_version->id,
        accepted        => 1,
        publish_pip     => $last_version->publish_pip || $existing->publish_pip,
        publish_website => $last_version->publish_website || $existing->publish_website,
        directory_id    => $last_version->directory_id,
    });

    # Set new file-id on file case documents. History not required.
    if ($last_version->case_documents) {
        $last_version->case_documents->search({ case_document_id => {'not in' => $result->case_documents->get_column('case_document_id')->as_query } })->update({
            file_id => $result->id
        });
    }

    # Unset any foreign keys that may disappear on the old entry.
    $last_version->update({directory_id => undef});

    $existing->make_active_version;

    return $result->discard_changes;
}

=head2 $file->get_last_version

Returns the last version of a file.

=cut

sub get_last_version {
    my $self      = shift;
    my $root_file = $self->get_root_file;

    my $res = $self->result_source->schema->resultset('File')->search_rs(
        { root_file_id => $root_file->id, },
        { order_by => { -desc => 'version' }, rows => 1 },
    )->first;
    return $res ? $res : $self;
}

=head2 $self->get_root_file

Gets the root file of a file. The root file is the first document in a series
of versions. When no root_file_id is set in the file, it is assumed that this
is in fact the first file in the series.

=cut

sub get_root_file {
    my $self = shift;
    my $root_file;
    if ($self->root_file_id) {
        $root_file = $self->root_file_id;
    }
    else {
        $root_file = $self;
    }
    return $root_file;
}

=head2 $self->make_leading

Makes this file the leading one by duplicating it into the last version +1.

=cut

sub make_leading {
    my $self = shift;
    my ($subject) = @_;

    # Simply use update_file to achieve this. The functionality is exactly the
    # same. The only 'bad' part of it is that it will readd the file to the
    # filestore.
    my $file_path = $self->filestore->ustore->getPath($self->filestore->uuid);

    return $self->update_file({
        subject       => $subject,
        original_name => $self->filestore->original_name,
        new_file_path => $file_path,
        is_restore    => 1,
    })->discard_changes;
}

=head2 $self->get_pip_data

Fetches the data that is approved to show on the PIP.

=cut

sub get_pip_data {
    my $self = shift;

    no strict 'refs';
    *DateTime = sub {shift->iso8601};
    use strict;

    (my $dotless = $self->extension) =~ s/\.//g;
    return {
        extension_dotless => $dotless,
        id                => $self->id,
        destroyed         => $self->destroyed,
        extension         => $self->extension,
        pip_thumbnail_url => '/pip/file/thumbnail/file_id/'.$self->id,
        version           => $self->version,
        name              => $self->name,
        accepted          => $self->accepted,
        publish_pip       => $self->publish_pip,
        date_created      => $self->date_created,
        date_modified     => $self->date_modified,
        date_deleted      => $self->date_deleted,
        deleted_by        => $self->deleted_by,
        created_by        => $self->created_by,
        modified_by       => $self->modified_by,
        filestore_id => {
           mimetype => $self->filestore->mimetype,
        },
        case_id => {
           id => $self->case->id,
        },
    }
}

=head2 TO_JSON

TO_JSON extension. Pretty much only to convert DateTime objects. Consider
moving this to a more global space at some point. Or even better; fix it
properly without the monkey patch.

=cut

sub TO_JSON {
    my $self = shift;

    {
        no warnings 'redefine';
        no strict 'refs';
        *DateTime::TO_JSON = sub {shift->iso8601};
        use strict;
    }

    my %options;

    ### XXX TODO (different call from frontend)
    # $options{log} = [$self->loglines->all];

    # Intake owner
    $options{intake_owner} = $self->intake_owner;

    ### XXX TODO
    # if ($self->is_duplicate_name && $self->accepted == 0 && $self->case) {
    #     my $result = $self->result_source->resultset->search({
    #         id        => {'!=' => $self->id},
    #         name      => $self->name,
    #         case_id   => $self->case->id,
    #         extension => $self->extension,
    #     },
    #     {
    #         prefetch    => [
    #             { case_id => "zaaktype_node_id" },
    #             { case_id => "zaaktype_id" },
    #             { case_id => "aanvrager" },
    #             { case_id => "coordinator" },
    #             { case_id => "behandelaar" },
    #             'filestore_id',
    #             'root_file_id'
    #         ]
    #     }
    #     );
    #     # In case the duplicate vanished in the mean time we simply skip this part.
    #     if ($result->count) {
    #         $options{is_duplicate_of} = $result->first->get_last_version->id;
    #     }
    # }

    $options{thumbnail_url}     = '/file/thumbnail/file_id/'.$self->id;
    $options{pip_thumbnail_url} = '/pip/file/thumbnail/file_id/'.$self->id;
    ($options{extension_dotless} = $self->extension) =~ s/\.//g;

    $options{case_documents} = undef;


    ### XXX TODO (different call from frontend)
    if (
        $self->has_column_loaded('case_document_id_list') &&
        ref $self->get_column('case_document_id_list') eq 'ARRAY'
    ) {
        #my @cds = map {$_->case_document_id} $self->case_documents->search({}, { prefetch => ['case_document_id', 'case_document']});
        #$options{case_documents} = \@cds;
        $options{case_documents} = $self->get_column('case_document_id_list');
    } else {
        $options{case_documents} = [ map { $_->case_document_id->id } $self->case_documents->search({}, { prefetch => ['case_document_id', 'case_document']}) ];
    }

    ### Make sure we at least sent a hashref
    $options{case_documents} = [] unless $options{case_documents};

    ### Done, annotation count from subquery
    if ($self->has_column_loaded('annotation_count')) {
        $options{annotation_count} = $self->get_column('annotation_count');
    } else {
        $options{annotation_count} = $self->annotation_count;
    }

    %options = $self->_load_plain_columns(
        %options
    );

    %options = $self->_append_custom_columns(
        %options
    );

    my $rv = {
        %options,
    };

    return $rv;
}

sub _load_plain_columns {
    my $self            = shift;
    my %options         = @_;

    for my $column ($self->result_source->columns) {
        my $info = $self->result_source->column_info($column);

        # If there's a DateTime inflator, use it. Others (like relationships)
        # should stay un-inflated for performance.
        if (exists $info->{_ic_dt_method}) {
            if (defined $self->$column) {
                $options{$column} = $self->$column->clone->set_time_zone('UTC')->iso8601 . 'Z';
            }
            else {
                $options{$column} = undef;
            }
            next;
        }

        $options{$column} = $self->get_column($column);
    }

    return %options;
}

sub _append_custom_columns {
    my $self            = shift;
    my %options         = @_;

    $options{case_id}       = ($self->case_id ? $self->case_id->id : undef);
    $options{filestore_id}  = $self->filestore_id;
    $options{directory_id}  = $self->directory_id;
    $options{metadata_id}   = ($self->metadata_id ? { $self->metadata_id->get_columns } : undef);

    $options{$_}            = $self->$_ for qw/modified_by deleted_by created_by/;
    # root_file_id

    ### Get relationships
    # my @relationships   = $self->result_source->relationships;

    # for my $column ($self->result_source->columns) {
    #     next if grep { $column eq $_ } @relationships;

    #     $options{$column} = $self->get_column($column);
    # }

    return %options;
}


sub annotation_count {
    my ($self) = @_;

    my $schema = $self->result_source->schema;
    my $user_id = $schema->default_resultset_attributes->{ current_user }->uidnumber;

    my $rs = $schema->resultset('FileAnnotation')->search({ file_id => $self->id });

    return $schema->resultset('Config')->get('pdf_annotations_public') ?
        $rs->count :
        $rs->search({subject => 'betrokkene-medewerker-'. $user_id})->count;
}

=head2 get_download_info

Easy access to oft required fields when serving a static file.

Returns the path to the file in scalar context, in list context returns path,
mimetype, size and filename (composed from the name on the file record
and the extention that is the request format).

Example:

    $c->serve_static_file($file->get_download_info);

Or:

    my ($path, $mime, $size, $name) = $file->get_download_info;

Or:

    my ($path, $mime, $size, $name) = $file->get_download_info('pdf');

=cut

sub get_download_info {
    my $self = shift;
    my $target_fmt = shift;

    unless($target_fmt) {
        if(wantarray) {
            return (
                $self->filestore->get_path,
                $self->filestore->mimetype,
                $self->filestore->size,
                $self->name . $self->extension
            );
        } else {
            return $self->filestore->get_path;
        }
    }

    my ($handle, $tmpfile) = tempfile();

    my %optional;

    if($self->case_id) {
        $optional{ case_id } = $self->get_column('case_id');
    }

    my $filepath = $self->filestore->convert({
        target_format => $target_fmt,
        target_file => $tmpfile,
        %optional
    });

    # TODO refactor; wantarray cruft is confusing
    # don't forget unless($target_fmt) above.
    if(wantarray) {
        return (
            $filepath,
            mimetype($filepath),
            -s $filepath,
            sprintf('%s.%s', $self->name, $target_fmt)
        );
    } else {
        return $filepath;
    }
}

=head2 get_subject_name

Get the display name for any given subject.

=cut

sub get_subject_name {
    my ($self, $subject) = @_;
    return $self->result_source->schema->betrokkene_model->get({}, $subject)->display_name;
}

# Should probably just use the ResultSet one. Fix later if there's some time left.
sub get_file_properties {
    my ($file_path) = @_;
    my $M_A = MIMETYPES_ALLOWED;
    my @valid_extensions = keys %$M_A;

    my ($name, $dir, $extension) = fileparse($file_path, @valid_extensions);

    return  {
        name => $name,
        dir  => $dir,
        ext  => $extension,
    };
}

=head2 $self->get_valid_filename($name, $extension, $case_id)

Returns a valid filename to prevent duplicates.

Example: file.txt already exists for a given case, it then appends (1) to the
name. Every future occurence results in a +1 in this version suffix.

=cut


sub get_valid_filename {
    my $self = shift;
    my ($name) = @_;

    # If there is no case, we cannot say there are duplicate files (yet).
    return $name if !$self->case;

    my $files = $self->result_source->resultset->search({
        id        => {'!=' => $self->id},
        name      => { '~*' => $name.'(\ \(\d+\))?' },
        case_id   => $self->case->id,
        extension => $self->extension,
    });

    # No existing files found, return the name as is
    if ($files->count == 0) {
        return $name;
    }

    # Suffix in this context means the 'version' a file has.
    my $suffix;
    while (my $f = $files->next) {
        # Extract the suffix from the current file
        my ($f_suffix) = ($f->name =~ m/.*\((\d+)\)/);

        # If the current suffix + 1 is greater than the highest registered, it becomes leading.
        if ($f_suffix && $f_suffix+1 > $suffix) {
            $suffix = $f_suffix+1;
        }
        # First file with a suffix
        elsif (!$f_suffix && !$suffix) {
            $suffix = 1;
        }
    }
    return sprintf "%s (%d)", ($name, $suffix);
}

sub trigger {
    my ($self, $subtype, $opts) = @_;

    # Logging outside of the case context is nonsensical,
    # no one will read those events
    return unless $self->case_id;

    my $event_fields = {
        component => 'document',
        component_id => $self->id,
        zaak_id => $self->case_id->id,
        data => $opts
    };

    $self->result_source->schema->resultset('Logging')->trigger(
        sprintf('case/document/%s', $subtype),
        $event_fields
    );
}

sub log {
    my ($self, $opts) = @_;
    my $message    = $opts->{message};
    my $properties = $opts->{updated_properties};
    my $trigger    = $opts->{trigger};

    # Delete ID keys
    delete $properties->{id};

    my $logline;
    if ($message && !$properties) {
        $logline = $message;
    }
    elsif ($message && $properties) {
        # Fix up the DateTime fields
        my @dt_fields = ('date_modified', 'dated_deleted', 'date_created');
        for my $dt_f (@dt_fields) {
            if ($properties->{$dt_f}) {
                $properties->{$dt_f}->set_time_zone('Europe/Amsterdam');
                $properties->{$dt_f} = sprintf "%s %d:%d", (
                    $properties->{$dt_f}->dmy,
                    $properties->{$dt_f}->hour,
                    $properties->{$dt_f}->minute
                );
            }
        }
        my @need_logging;
        my $name_mapping = {
            accepted              => 'Document geaccepteerd',
            case_id               => 'Zaaknummer',
            case_document_ids     => 'Zaakdocumenten',
            date_deleted          => 'Datum verwijdering',
            date_modified         => 'Datum wijziging',
            deleted_by            => 'Verwijderd door',
            description           => 'Omschrijving',
            directory_id          => 'Directory',
            is_duplicate_name     => 'Bestand deelt naam met bestaand bestand',
            modified_by           => 'Gewijzigd door',
            name                  => 'Bestandsnaam',
            origin                => 'Richting',
            publish_pip           => 'Weergeven op PIP',
            publish_website       => 'Weergeven op website',
            rejection_reason      => 'Afwijzingsreden',
            root_file_id          => 'Eerste versie',
            subject_id            => 'Betrokkene',
            trust_level           => 'Vertrouwelijkheid',
            version               => 'Versie',
            document_category_parent  => 'Document hoofdcategorie',
            document_category_child   => 'Document subcategorie',
        };

        for my $p (keys %$properties) {
            my $value = $properties->{$p} || '<leeg>';
            push @need_logging, sprintf(
                "%s -> %s", ($name_mapping->{$p}, $value)
            );
        }
        my $string = join "\n", @need_logging;

        $logline = sprintf "%s:\n%s", ($message, $string);
    }

    my $case_id = $self->case_id->id if $self->case_id;

    my $modifier = $properties->{ modified_by };

    my $log_arguments   = {
        component       => 'document',
        component_id    => $self->id,
        created_for     => $modifier,
        zaak_id         => $case_id,
        data            => {
            case_id       => $case_id,
            subject_id    => $modifier,
            file_id       => $self->id,
            change        => $logline,
            filename      => $self->filename,
        },
    };

    $log_arguments->{zaak_id} = $case_id;

    $self->result_source->schema->resultset('Logging')->trigger(
        $trigger, $log_arguments
    );
}

=head2 $self->filename

Returns the concatenation of name and extension separated by a comma, e.g
klaymen.gif

=cut

sub filename {
    my $self    = shift;

    return $self->name . $self->extension;
}

=head2 filepath

Returns the full 'virtual' path of the file representation in Zaaksysteem. Within a case you can put files in (sub)directories.
This is NOT the path of the file on disk, use L<Zaaksysteem::Backend::Filestore::Component::get_path> for this purpose.

=head3 SYNOPSIS

    my $path_in_case = $file->filepath;
    my $path_on_disk = $file->filestore->get_path;

=cut

sub filepath {
    my $self = shift;

    my $dir = $self->directory;
    return $dir ? catfile($dir->name, $self->filename) : $self->filename;
}

=head2 $self->loglines

Returns a DBIx::Class::Resultset of all loglines related to this file.

=cut

sub loglines {
    my $self    = shift;

    my $search = $self->result_source->schema->resultset('Logging')->search({
        component    => 'document',
        component_id => $self->id,
    });
    return $search;
}

=head2 apply_case_document_defaults

Applies the defaults belonging to the case type document. This method can only
function if there is a single case document set.

=cut

sub apply_case_document_defaults {
    my $self = shift;
    my @cds  = $self->case_documents;

    return unless scalar @cds == 1;

    my $cd = $cds[0]->case_document_id;

    my $library_property = $cd->bibliotheek_kenmerken_id;
    if ($library_property && $library_property->file_metadata_id) {
        my $md = $library_property->file_metadata_id;
        my %opts;
        my @columns = $md->result_source->columns;
        for my $c (@columns) {
            next if $c eq 'id';
            if ($md->$c) {
                $opts{$c} = $md->$c;
            }
        }
        if ($library_property->value_default) {
            $opts{description} = $library_property->value_default;
        }
        $self->update_metadata({%opts});
    }

    $self->update({
        publish_pip => $cd->pip || 0,
        publish_website => $cd->publish_public || 0
    });

    return $self->discard_changes;
}


sub has_label {
    my $self = shift;
    my $label = shift;

    unless($label) {
        return $self->case_documents->count > 0 ? 1 : 0;
    }

    return grep
        { $_->case_document->library_attribute->magic_string eq $label }
        $self->case_documents;
}

sub labels {
    my $self = shift;

    return map { $_->case_document->label } $self->case_documents;
}

=head2 set_or_replace_case_documents

Method to set new case documents for a file. Requires a list of case
document IDs to be given. All existing entries will be deleted.

=cut

sub set_or_replace_case_documents {
    my ($self, @case_document_ids) = @_;

    # Strip any possible duplicate IDs
    my %dupe = ();
    @case_document_ids = grep { !$dupe{$_}++ } @case_document_ids;

    # Delete existing
    $self->case_documents->delete;

    # Create new entries
    my $fcd = $self->result_source->schema->resultset('FileCaseDocument');
    for my $cid (map { ref $_ eq 'ARRAY' ? @$_ : $_ } @case_document_ids) {
        $fcd->create({
            case_document_id => $cid,
            file_id          => $self->id,
        });
    }

    return $self;
}

=head2 move_case_document

Method to easily move a case document assignment from one document to another.

This should be called on the target file, passing the 'from' file as a parameter.

=cut

Params::Profile->register_profile(
    method  => 'move_case_document',
    profile => {
        required => [qr/subject from case_document_id/],
    }
);

sub move_case_document {
    my $self = shift;
    my $opts = $_[0];

    my $dv = Params::Profile->check(
        params  => $opts,
    );
    my $valid = $dv->valid;

    $self->result_source->schema->resultset('FileCaseDocument')->search({
        file_id => $opts->{from},
        case_document_id => $opts->{case_document_id},
    })->delete;

    $self->result_source->schema->resultset('FileCaseDocument')->find_or_create({
        file_id => $self->id,
        case_document_id => $opts->{case_document_id},
    });

    $self->apply_case_document_defaults;

    return $self->discard_changes;
}


=head2 reject

This called when the case owner (behandelaar) doesn't allow a file to go in.
If this file is linked to a scheduled job, we give the job a chance to do cleanup

=cut

sub reject {
    my ($self) = @_;

    if (my $scheduled_job = $self->scheduled_jobs_id) {
        $scheduled_job->apply_roles->reject;
    }
}

=head2 condense_content

=cut

sub condense_content {
    my $self = shift;

    return eval {
        my $text;

        if($self->filestore->mimetype eq 'text/plain') {
            $text = Zaaksysteem::Tools::Text->new(source => $self->filestore->content);
        }

        if($self->filestore->mimetype eq 'text/html') {
            my $html = Zaaksysteem::Tools::Text::HTML->new(file => $self->filestore->get_path);

            $text = Zaaksysteem::Tools::Text->new(source => $html);
        }

        if($self->filestore->mimetype eq 'application/pdf') {
            my $pdf = Zaaksysteem::Tools::Text::PDFHack->new(file => $self->filestore->get_path);

            $text = Zaaksysteem::Tools::Text->new(source => $pdf);
        }

        if($self->filestore->mimetype eq 'application/vnd.oasis.opendocument.text') {
            my $odt = Zaaksysteem::Tools::Text::OpenDocument->new(file => $self->filestore->get_path);

            $text = Zaaksysteem::Tools::Text->new(source => $odt);
        }

        my @convertables = qw[
            application/vnd.openxmlformats-officedocument.wordprocessingml.document
            application/vnd.ms-word
        ];

        if(grep { $self->filestore->mimetype eq $_ } @convertables) {
            my ($ext) = $self->filestore->original_name =~ m[(\.\w+)$];

            my $tmpfile = File::Temp->new(
                DIR => $self->result_source->schema->tmp_path,
            );

            my $tmp_pdf = $self->filestore->convert_to_pdf($ext, $tmpfile);
            my $pdf = Zaaksysteem::Tools::Text::PDFHack->new(file => $tmp_pdf);

            $text = Zaaksysteem::Tools::Text->new(source => $pdf);
        }

        return undef unless blessed $text;

        my @keywords = $text->stemmed_words;

        return undef unless scalar @keywords;

        return encode_utf8(join " ", @keywords);
    };
}

sub insert {
    my $self    = shift;

    $self->search_index($self->condense_content);

    my $rv      = $self->next::method(@_);

    if (ref($self->case_id)) {
        $self->case_id->touch;
    }

    return $rv;
};

sub update {
    my $self    = shift;

    $self->search_index($self->condense_content);

    my $rv      = $self->next::method(@_);

    if (ref($self->case_id)) {
        $self->case_id->touch;
    }

    return $rv;
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 MIMETYPES_ALLOWED

TODO: Fix the POD

=cut

=head2 annotation_count

TODO: Fix the POD

=cut

=head2 get_file_properties

TODO: Fix the POD

=cut

=head2 has_label

TODO: Fix the POD

=cut

=head2 insert

TODO: Fix the POD

=cut

=head2 labels

TODO: Fix the POD

=cut

=head2 log

TODO: Fix the POD

=cut

=head2 make_active_version

TODO: Fix the POD

=cut

=head2 trigger

TODO: Fix the POD

=cut

=head2 update

TODO: Fix the POD

=cut

=head2 update_existing

TODO: Fix the POD

=cut

