package Zaaksysteem::Backend::File::MailDissector;

use Moose::Role;
use Zaaksysteem::OutlookMailParser;
use Zaaksysteem::Email;

=head2 handle_accepted

Outlook emails will be converted to MIME.

MIME mails will be dissected into parts, these parts
will be added to the case in a folder.

=cut

sub handle_accepted {
    my ($self, $arguments) = @_;

    my $properties = $arguments->{properties} or die "need properties";

    my $id   = $self->id;
    my $name = $self->filestore_id->original_name();
    my $path = $self->filestore_id->get_path();

    my $outlook = Zaaksysteem::OutlookMailParser->new();
    my $msg     = $outlook->process_outlook_message(
        { path => $path, original_name => $name });

    return if !$msg;

    my $email = Zaaksysteem::Email->new(
        message => $msg,
        schema  => $self->result_source->schema
    );
    if($email->add_to_case($self->case, accept => 1)) {
        $self->update_properties({
                deleted => 1,
                subject => $properties->{subject},
        });
    }
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
