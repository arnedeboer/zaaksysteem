package Zaaksysteem::Scheduler::Job::SDUConnectSync;

use Moose::Role;
use namespace::autoclean;

=head1 NAME

Zaaksysteem::Scheduler::Job::SDUConnectSync - Job runner for SDU Connect based
object synchronisation

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head1 METHODS

=head2 run

Handler for L<Zaaksysteem::Scheduler/run_pending>. This will poll the
configured SDU Connect machine for new changes in the collection.

=cut

sub run {
    my $self = shift;
    my $c = shift;

    my $interface = $c->model('DB::Interface')->find($self->interface_id);

    $interface->process_trigger('delta_sync', {
        object_model => $c->model('Object')
    });

    # Update last_run field on the interface. We could build this into the
    # scheduled job itself, and perhaps that would be useful too, but the
    # interface then depends on the existance of a job to get the last run
    # timestamp.
    my $config = $interface->get_interface_config;

    $config->{ last_run } = DateTime->now->iso8601 . 'Z';

    $interface->update_interface_config($config);

    return 1;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
