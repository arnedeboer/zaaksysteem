package Zaaksysteem::Betrokkene::Object;

use strict;
use warnings;

use Zaaksysteem::Betrokkene::ResultSet;
use Zaaksysteem::Constants qw/RGBZ_LANDCODES/;

use Moose;

with 'MooseX::Log::Log4perl';

my $BETROKKENE_TYPES = {
    'medewerker'            => 'Medewerker',
    'natuurlijk_persoon'    => 'NatuurlijkPersoon',
};

has 'types'     => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        return $BETROKKENE_TYPES;
    },
);

### Direct catalyst access
#has 'c'         => (
#    'is'        => 'rw',
#    'weak_ref'  => 1,
#);

has [qw/prod dbic stash config customer/] => (
    'weak_ref'  => 1,
    'is'        => 'ro',
);

has gm_object => (
    is      => 'rw',
    isa     => 'Defined',
    builder => '_build_gm_object',
    lazy    => 1,
);

sub _build_gm_object {
    my $self = shift;
    if ($self->can('gm_np')) {
        return $self->gm_np;
    }
    else {
        return $self->gm_bedrijf;
    }
}

has '_dispatch_options' => (
    'is'    => 'ro',
    'lazy'  => 1,
    'default'   => sub {
        my $self    = shift;

        my $dispatch = {
            prod        => $self->prod,
            dbic        => $self->dbic,
            stash       => $self->stash,
            config      => $self->config,
            customer    => $self->customer,
        };

        Scalar::Util::weaken($dispatch->{stash});

        return $dispatch;
    }
);


### What was the trigger of the call, get, search etc
has 'trigger'      => (
    'is'    => 'rw'
);

### Type, well, should always be 'natuurlijk_persoon'
has 'type'      => (
    'is'    => 'rw'
);

### TODO ah why not, see other TODO
has 'bo'      => (
    'is'    => 'rw'
);

### There is always a betrokkene id
has 'id'        => (
    'is'    => 'rw',
);

### There is always a betrokkene_type
has 'btype'        => (
    'is'    => 'rw',
);

### And the identifier, when internal
has 'identifier' => (
    'is'    => 'rw',
);

### Authenticated by: can be: medewerker, kvk, gba
### or UNDEF (webform)
has 'authenticated_by' => (
    'is'    => 'rw',
);

### Only with GBA or KVK
has 'authenticated'    => (
    'is'    => 'rw',
);

has 'can_verwijderen'    => (
    'is'    => 'rw',
);

### BAG koppeling, dynamic
has 'verblijfsobject'   => (
    'is'    => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my ($self) = @_;

        return unless $self->can('verblijfsobject_id');

        return $self->verblijfsobject_id if $self->verblijfsobject_id;

        my $na = $self->dbic->resultset('BagNummeraanduiding')->search({
            'postcode'              => uc($self->postcode || ''),
            'huisnummer'            => $self->huisnummer,
            'huisletter'            => (uc($self->huisletter || '') || undef),
            'huisnummertoevoeging'  => (uc($self->huisnummertoevoeging || '') || undef),
            'einddatum'             => undef,
        });

        if ($na->count && $na->count == 1) {
            $na     = $na->first;

            my $vbo = $na->verblijfsobjecten->search({
                'einddatum' => undef,
            });

            if ($vbo->count) {
                return $vbo->first;
            }
        }

        return;
    }
);

has 'verblijfsobject_koppeling' => (
    'is'    => 'rw',
    'lazy'  => 1,
    'default'   => sub {
        my ($self) = @_;

        return unless $self->can('verblijfsobject_id');

        return unless $self->verblijfsobject_id;

        return 1;
    }
);

sub rt_identifier {
    my ($self) = @_;

    return $self->btype . '-'
        . $self->identifier;
}

sub rt_setup_identifier {
    my ($self) = @_;

    return 'betrokkene-' . $self->btype
        . '-' . $self->ex_id;
}

has 'betrokkene_identifier' => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        return shift->rt_setup_identifier;
    }
);

has 'ex_id' => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my ($self) = @_;

        if ($self->can('ldapid')) {
            return $self->ldapid;
        } elsif ($self->can('gmid')) {
            return $self->gmid;
        }
    }
);

has 'human_type' => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my ($self) = @_;

        if( $self->btype eq 'natuurlijk_persoon' ) {
            return 'Natuurlijk persoon';
        }

        if( $self->btype eq 'bedrijf' ) {
            return 'Niet natuurlijk persoon';
        }

        if( $self->btype eq 'medewerker' ) {
            return 'Behandelaar';
        }
    }
);

has [qw/is_overleden is_briefadres in_onderzoek/]  => (
    'is'    => 'ro',
);

has 'record'    => (
    'is'        => 'rw',
    'isa'       => 'DBIx::Class::Row'
);

has 'country'      => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my $self = shift;

        my $landcode;
        if ($self->btype eq 'natuurlijk_persoon') {
            $landcode = $self->landcode;
        } elsif ($self->btype eq 'bedrijf') {
            $landcode = $self->vestiging_landcode;
        }
        return RGBZ_LANDCODES->{$landcode} if $landcode;
    }
);

sub TO_JSON {
    my $self = shift;

    return {
        id => $self->id,
        betrokkene_type => $self->type,
        gegevens_magazijn_id => $self->ex_id,
        naam => $self->naam,
        subject_identifier => $self->betrokkene_identifier,

        betrokkene_id => undef,
        deleted => undef,
        uuid => undef,
        verificatie => undef,
        rol => undef,
        magic_string_prefix => undef
    };
}

=head2 as_hashref

Returns nothing.

Subclasses use this to return a representation of themselves in plain hash form.

=cut

sub as_hashref {
    return;
}


sub log_view {
    my $self        = shift;
    my $user_id     = shift;

    die('Invalid USER given') unless $user_id;

    return 1 unless $self->btype eq 'natuurlijk_persoon';

    return $self->dbic->resultset('Logging')->trigger(
        'subject/view',
        {
            component => 'betrokkene',
            betrokkene_id => $user_id,
            data => {
                name        => $self->naam,
                identifier  => $self->burgerservicenummer,
            }
        }
    );
}


sub street_address {
    my ($self) = @_;

    # for other btypes, die of natural causes. let nature be.
    return $self->btype eq 'bedrijf' ? (join ' ', grep { $_ } (
            $self->vestiging_straatnaam,
            $self->vestiging_huisnummer,
            $self->vestiging_huisnummertoevoeging
        )
    ) : join ' ', grep { $_ } (
        $self->straatnaam,
        $self->huisnummer,
        $self->huisnummertoevoeging
    );
}

sub city {
    my ($self) = @_;

    return $self->btype eq 'bedrijf' ? $self->vestiging_woonplaats : $self->woonplaats;
}

sub postal_code {
    my ($self) = @_;

    return $self->btype eq 'bedrijf' ? $self->vestiging_postcode : $self->postcode;
}

=head2 has_valid_address

Arguments: none

Return value: $TRUE_ON_SUCCESS

    if (!$betrokkene->has_address) {
        die('Betrokkene has no address');
    }

Checks whether this betrokkene has a valid address.

=cut

sub has_valid_address {
    my $self        = shift;

    return 1 if $self->straatnaam;

    return;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 RGBZ_LANDCODES

TODO: Fix the POD

=cut

=head2 TO_JSON

TODO: Fix the POD

=cut

=head2 city

TODO: Fix the POD

=cut

=head2 log_view

TODO: Fix the POD

=cut

=head2 postal_code

TODO: Fix the POD

=cut

=head2 rt_identifier

TODO: Fix the POD

=cut

=head2 rt_setup_identifier

TODO: Fix the POD

=cut

=head2 street_address

TODO: Fix the POD

=cut

