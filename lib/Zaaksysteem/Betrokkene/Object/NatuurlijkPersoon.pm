package Zaaksysteem::Betrokkene::Object::NatuurlijkPersoon;

use strict;
use warnings;
use Moose;

use Zaaksysteem::Backend::Subject::Naamgebruik qw/naamgebruik/;
use Zaaksysteem::Constants qw/FRIENDLY_BETROKKENE_MESSAGES/;
use Zaaksysteem::Exception;
use Zaaksysteem::Profile;

use constant BOBJECT    => 'Zaaksysteem::Betrokkene::Object';
use constant BRSOBJECT  => 'Zaaksysteem::Betrokkene::ResultSet';

extends BOBJECT;

my $CLONE_MAP = [qw/
    burgerservicenummer
    a_nummer
    voornamen
    geslachtsnaam
    voorvoegsel
    geslachtsaanduiding
    geboorteplaats
    geboorteland
    geboortedatum
    aanhef_aanschrijving
    voorletters_aanschrijving
    voornamen_aanschrijving
    naam_aanschrijving
    voorvoegsel_aanschrijving
    burgerlijke_staat

    indicatie_geheim
    authenticatedby
    import_datum
    verblijfsobject_id
    datum_huwelijk
    datum_huwelijk_ontbinding

    aanduiding_naamgebruik

    partner_voorvoegsel
    partner_geslachtsnaam
    partner_burgerservicenummer
/ ];

my $CONTACT_MAP = {
    telefoonnummer  => 1,
    mobiel          => 1,
    email           => 1,
};

my $ADRES_CLONE_MAP = [qw/
    straatnaam
    huisnummer
    woonplaats
    postcode
    huisletter
    huisnummertoevoeging
    functie_adres
    adres_buitenland1
    adres_buitenland2
    adres_buitenland3
    landcode
/];

my $SEARCH_MAP = {
    'burgerservicenummer' => 'natuurlijk_persoons.burgerservicenummer',
    'a_nummer' => 'natuurlijk_persoons.a_nummer',
    'voornamen' => 'natuurlijk_persoons.voornamen',
    'geslachtsnaam' => 'natuurlijk_persoons.geslachtsnaam',
    'partner_geslachtsnaam' => 'natuurlijk_persoons.partner_geslachtsnaam',
    'voorvoegsel' => 'natuurlijk_persoons.voorvoegsel',
    'partner_voorvoegsel' => 'natuurlijk_persoons.partner_voorvoegsel',
    'geslachtsaanduiding' => 'natuurlijk_persoons.geslachtsaanduiding',
    'geboorteplaats' => 'natuurlijk_persoons.geboorteplaats',
    'geboorteland' => 'natuurlijk_persoons.geboorteland',
    'geboortedatum' => 'natuurlijk_persoons.geboortedatum',
    'aanhef_aanschrijving' => 'natuurlijk_persoons.aanhef_aanschrijving',
    'voorletters_aanschrijving' => 'natuurlijk_persoons.voorletters_aanschrijving',
    'voornamen_aanschrijving' => 'natuurlijk_persoons.voornamen_aanschrijving',
    'naam_aanschrijving' => 'natuurlijk_persoons.naam_aanschrijving',
    'voorvoegsel_aanschrijving' => 'natuurlijk_persoons.voorvoegsel_aanschrijving',
    'burgerlijke_staat' => 'natuurlijk_persoons.burgerlijke_staat',
    'indicatie_geheim' => 'natuurlijk_persoons.indicatie_geheim',
    'straatnaam'    => 'straatnaam',
    'huisnummer' => 'huisnummer',
    'woonplaats' => 'woonplaats',
    'postcode' => 'postcode',
    'huisletter' => 'huisletter',
    'huisnummertoevoeging' => 'huisnummertoevoeging',
    'datum_overlijden' => 'natuurlijk_persoons.datum_overlijden',
};

=head2 in_onderzoek

Retrieve db field

=cut

sub in_onderzoek {
    my ($self) = @_;

    return $self->gm_extern_np->in_onderzoek;
}

has 'is_briefadres'  => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my $self        = shift;

        if (uc($self->functie_adres || '') eq 'B') {
            return 1;
        }
    }
);

has 'subscription_id'   => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        return shift->gm_extern_np->subscription_id
    }
);


has 'gm_adres'  => (
    'is'    => 'rw',
);

has 'gm_np'     => (
    'is'    => 'rw',
);

has 'gm_extern_np'     => (
    'is'    => 'rw',
);

has 'is_overleden' => (
    'is'    => 'rw',
    'lazy'  => 1,
    'default'   => sub {
        my ($self) = @_;

        return $self->gm_extern_np->is_overleden(@_);
    }
);


sub is_verhuisd {
    my $self    = shift;

    return 1 if (
        $self->woonplaats ne $self
                                ->gm_extern_np
                                ->adres_id
                                ->woonplaats
    );

    return;
}

=head2 datum_overlijden

Retrieve potential passing date

=cut

sub datum_overlijden {
    my ($self) = @_;

    return $self->gm_extern_np->datum_overlijden;
}


has 'gmid'      => (
    'is'    => 'rw',
);

has 'intern'    => (
    'is'    => 'rw',
);

has 'in_zaaksysteem'    => (
    'is'    => 'rw',
);

### Convenience method containing some sort of display_name
### Ai, this one is different from the one we punt in the database as
### display_name. We create a new method display_name to get it even
has 'naam' => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my ($self) = @_;

        ### Depends on aanduiding naamgebruik.
        return $self->volledige_naam;
    },
);

### Make sure this is the same as regel 10000.
has 'display_name' => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my ($self) = @_;

        ### Depends on aanduiding naamgebruik.
        return $self->voornamen . ' ' . $self->achternaam,
    },
);

has 'geslacht'      => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my ($self) = @_;

        return 'man' if uc($self->geslachtsaanduiding) eq 'M';
        return 'vrouw' if uc($self->geslachtsaanduiding) eq 'V';

        return;
    }
);

has 'aanhef'      => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my ($self) = @_;

        return 'meneer' if uc($self->geslachtsaanduiding) eq 'M';
        return 'mevrouw' if uc($self->geslachtsaanduiding) eq 'V';

        return;
    }
);

has 'aanhef1'      => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my ($self) = @_;

        return 'heer' if uc($self->geslachtsaanduiding) eq 'M';
        return 'mevrouw' if uc($self->geslachtsaanduiding) eq 'V';

        return;
    }
);

has 'aanhef2'      => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my ($self) = @_;

        return 'de heer' if uc($self->geslachtsaanduiding) eq 'M';
        return 'mevrouw' if uc($self->geslachtsaanduiding) eq 'V';

        return;
    }
);

has 'volledige_naam' => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my ($self) = @_;

        ### Depends on aanduiding naamgebruik.
        return $self->voorletters . ' '
            . $self->achternaam;
    },
);

has 'achternaam'  => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my ($self) = @_;

        return naamgebruik({
            aanduiding => $self->aanduiding_naamgebruik || '',
            partner_voorvoegsel => $self->partner_voorvoegsel,
            partner_geslachtsnaam => $self->partner_geslachtsnaam,
            voorvoegsel => $self->voorvoegsel,
            geslachtsnaam => $self->geslachtsnaam,
        });
    }
);

has 'volledig_huisnummer'   => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my ($self) = @_;
        return $self->huisnummer
            . (
                $self->huisletter
                    ? ' ' . $self->huisletter
                    : ''
            ) . (
                $self->huisnummertoevoeging
                    ? ' - ' . $self->huisnummertoevoeging
                    : ''
            ) if $self;
    }
);

has 'messages'    => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my $self                = shift;
        my %rv;

        $rv{'briefadres'}   = FRIENDLY_BETROKKENE_MESSAGES->{'briefadres'}
            if $self->is_briefadres;
        $rv{'onderzoek'}    = FRIENDLY_BETROKKENE_MESSAGES->{'onderzoek'}
            if $self->in_onderzoek;
        $rv{'deceased'}     = FRIENDLY_BETROKKENE_MESSAGES->{'deceased'}
            if $self->is_overleden;
        $rv{'secret'}       = FRIENDLY_BETROKKENE_MESSAGES->{'secret'}
            if $self->indicatie_geheim;
        $rv{'moved'}        = FRIENDLY_BETROKKENE_MESSAGES->{'moved'}
            if $self->is_verhuisd;

        return \%rv;
    }
);

has 'messages_by_urgence' => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my $self                = shift;
        my (@info, @error);

        my $messages            = $self->messages;

        push(@info, $messages->{briefadres})    if $messages->{briefadres};

        push(@error, $messages->{onderzoek})    if $messages->{onderzoek};
        push(@error, $messages->{deceased})     if $messages->{deceased};
        push(@error, $messages->{secret})       if $messages->{secret};

        push(@info, $messages->{moved})         if $messages->{moved};


        return {
            error   => \@error,
            info    => \@info,
        };
    }
);

has 'messages_as_flash_messages' => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my $self                = shift;
        my @messages;

        my $aanvrager_messages  = $self->messages_by_urgence;

        push (
            @messages,
            {
                'type'      => 'error',
                'message'   => $_,
            }
        ) for @{ $aanvrager_messages->{error} };

        push (
            @messages,
            {
                'type'      => 'info',
                'message'   => $_,
            }
        ) for @{ $aanvrager_messages->{info} };

        return \@messages;
    }
);

has 'voorletters'   => (
    'is'        => 'ro',
    'lazy'      => 1,
    'builder'   => '_build_voorletters',
);

sub _build_voorletters {
    my $self        = shift;
    my $val         = shift;

    my $voornamen   = defined($val) ? $val : $self->voornamen;

    my ($firstchar) = $voornamen =~ m/^(\w{1})/;
    my @other_chars = $voornamen =~ m/ (\w{1})/g;

    return join(". ", $firstchar, @other_chars) . (
        ($firstchar || @other_chars) ?
        '.' : ''
    );

}

has 'can_verwijderen'   => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my $self    = shift;

        return 1 unless $self->gm_extern_np->authenticated;

        return;
    },
);



#sub search {
#    my $self    = shift;
#    my $searchr = shift;
#
#    my ($search);
#
#    for my $key (keys %{ $searchr }) {
#        my $searchkey = $key;
#        if ($key =~ /^gm-/) {
#            $key =~ s/^gm-//;
#
#            $search->{'natuurlijk_persoons.' . $key} = $searchr->{$searchkey};
#        } elsif ($key =~ /^adres-/) {
#            $key =~ s/^adres-//;
#
#            $search->{$key} = $searchr->{$searchkey};
#        }
#    }
#
#    ### Replace gm with tablename, adres with tablename
#    $self->log->debug('Searching for NP, credentials: ' .
#        Dumper($search)
#    );
#
#
#    return $self->c->model('DBG::Adres')->search(
#        $search,
#        {
#            'join'  => 'natuurlijk_persoons',
#        }
#    );
#}


my $ORDER_MAP = {
    'geslachtsnaam'     => 'geslachtsnaam',
    'voornamen'         => 'voornamen',
    'voorvoegsel'       => 'voorvoegsel',
    'bsn'               => 'burgerservicenummer',
    'geboortedatum'     => 'geboortedatum',
    'straatnaam'        => 'straatnaam',
    'huisnummer'        => 'huisnummer',
    'authenticated'     => 'authenticated',
};
my $ORDER_DIR = {
    'ASC'       => 'asc',
    'DESC'      => 'desc',
};
sub search {
    my $self        = shift;
    my $dispatch_options = shift;
    my $opts        = shift;
    my ($searchr)   = @_;
    my ($roworder, $roworderdir);

    ### We will return a resultset layer over DBIx::Class containing
    ### data, we will use to populate this class. That's why we cannot
    ### be called when we are the object itself. Should be a future
    ### feature
    die('M::B::NP->search() only possible call = class based')
        unless !ref($self);

    return unless defined($opts->{'intern'});


    ### SOME NOT complicated ORDERING
    if (
        $dispatch_options->{stash}->{order} &&
        defined($ORDER_MAP->{ $dispatch_options->{stash}->{order} })

    ) {
        $roworder = $ORDER_MAP->{ $dispatch_options->{stash}->{order} };
    } else {
        $roworder = 'geslachtsnaam';
    }

    if (
        $dispatch_options->{stash}->{order_direction} &&
        defined($ORDER_DIR->{ $dispatch_options->{stash}->{order_direction} })
    ) {
        $roworderdir = $ORDER_DIR->{ $dispatch_options->{stash}->{order_direction} };
    } else {
        $roworderdir = 'asc'
    }

    ### Ok, we got the information we need, now we have to search the
    ### database GM or our internal version, but first, define some variables
    ### with our variable map.
    my %search;
    for (keys %{ $SEARCH_MAP }) {
        next unless defined($searchr->{ $_ });

        if ($_ eq 'geboortedatum') {
            push @{ $search{"-and"} }, ($SEARCH_MAP->{ $_ } => $searchr->{ $_ });
        } elsif ($_ eq 'huisnummer') {
            push @{ $search{"-and"} }, ($SEARCH_MAP->{ $_ } => $searchr->{ $_ });
        } elsif ($_ eq 'burgerservicenummer') {
            push @{ $search{"-and"} }, ('NULLIF(' . $SEARCH_MAP->{ $_ } . ",'')::integer" => int($searchr->{ $_ }));
        } elsif ($_ eq 'voorvoegsel' || $_ eq 'geslachtsnaam') {
            push @{ $search{"-and"} }, (
                -or => [
                    $SEARCH_MAP->{ $_ }           => { 'ilike' => '%' . $searchr->{ $_ } . '%' },
                    $SEARCH_MAP->{ "partner_$_" } => { 'ilike' => '%' . $searchr->{ $_ } . '%' },
                ],
            );
        } elsif ($_ eq 'postcode') {
            ## XXX POSTCODE, Uppercase without spaces
            my $postcode = $searchr->{ $_ };
            $postcode = uc($postcode);
            $postcode =~ s/\s*//g;
            push @{ $search{"-and"} }, (
                $SEARCH_MAP->{ $_ } => { 'ilike' => "\%$postcode\%" }
            );
        } elsif (!$searchr->{'EXACT'}) {
            push @{ $search{"-and"} }, (
                $SEARCH_MAP->{ $_ } => { 'ilike' => '%' . $searchr->{ $_ } . '%'}
            );
        } else {
            push @{ $search{"-and"} }, ($SEARCH_MAP->{ $_ } => $searchr->{ $_ });
        }
    }

    ### Define correct db
    ### TODO: Constants would be fine
    my ($model);
    if ($opts->{intern}) {
        $model = 'DB::GmAdres';
    } else {
        $model = 'DB::Adres';
        push @{ $search{"-and"} }, ('natuurlijk_persoons.deleted_on' => undef);
    }

    ### Paging
    my $rowlimit = $dispatch_options->{stash}->{paging_rows} =
        $dispatch_options->{stash}->{paging_rows} || 40;
    my $rowpage = $dispatch_options->{stash}->{paging_page} =
        $dispatch_options->{stash}->{paging_page} || 1;

    if($opts->{rows_per_page}) {
        $rowlimit = $opts->{rows_per_page};
    }
    ### Ask internal or external model about this search
    #
    my @dbopts = (
        #['lower(natuurlijk_persoons.geslachtsnaam)', qw/kip/ ],
        \%search,
        {
            'join'      => 'natuurlijk_persoons',
            'page'      => $rowpage,
            'rows'      => $rowlimit,
            'order_by'  => { '-' . $roworderdir => $roworder }
        }

    );
    my $resultset;
    if ($opts->{intern}) {
        $resultset = $dispatch_options->{dbic}->resultset('GmAdres')->search(@dbopts);
    } else {
        $resultset = $dispatch_options->{dbic}->resultset('Adres')->search(@dbopts);
    }

    return unless $resultset;

    ### Paging info
    $dispatch_options->{stash}->{paging_total}       = $resultset->pager->total_entries;
    $dispatch_options->{stash}->{paging_lastpage}    = $resultset->pager->last_page;

    return BRSOBJECT->new(
        'class'     => __PACKAGE__,
        'dbic_rs'   => $resultset,
        'opts'      => $opts,
        %{ $dispatch_options },
    );
}


sub BUILD {
    my ($self) = @_;

    ### Nothing to do if we do not know which way we came in
    return unless ($self->trigger eq 'get' && $self->id);

    ### It depends on the 'intern' option, weather we retrieve
    ### our data from our our snapshot DB, or GM. When there is
    ### no intern defined, we will look at the id for a special string
    if ($self->id =~ /\-/) {
        $self->log->debug('XXX Found special string');

        ### Special string, no intern defined, go to intern default
        if (!defined($self->{intern})) {
            $self->log->debug('XXX Found internal request');
            $self->{intern} = 1;
        }

        my ($gmid, $id) = $self->id =~ /^(\d+)\-(\d+)$/;

        $self->id($id);
        $self->gmid($gmid);
    }

    if (!$self->intern) {
#        $self->log->debug('XXX Found external request');

        ### Get id is probably gmid, it is an external request, unless it is
        ### already set of course
        if (!$self->gmid) {
            $self->gmid($self->id);
            $self->id(undef);
        }
    }

    ### All set, let's rock and rolla. Depending on where we have to get the
    ### data from, fill in the blanks
    if ($self->{intern}) {
#        $self->log->debug('XXX Load internal id: ' . $self->id);
        $self->_load_intern or die('Failed loading internal M::B::NP Object');
    } else {
#        $self->log->debug('XXX Load external id:' . $self->gmid);
        $self->_load_extern or die('Failed loading external M::B::NP Object');
    }

    ### Some defaults, should move to Object
    $self->btype('natuurlijk_persoon');

}

sub _load_contact_data {
    my ($self, $gm_id)  = @_;

    return unless $gm_id;

    my $contactdata = $self->dbic->resultset('ContactData')->search(
        {
            gegevens_magazijn_id    => $gm_id,
            betrokkene_type         => 1,
        }
    );

    return unless $contactdata->count;
    $contactdata = $contactdata->first;

    for my $key (keys %{ $CONTACT_MAP }) {
        $self->{$key} = $contactdata->$key;
    }
}

sub _load_extern {
    my ($self) = @_;
    my ($gm);

    (
        $self->log->debug(
            'M::B::NP->load: Could not find external GM by id: ' . $self->gmid
        ),
        return
    ) unless $gm = $self->dbic->resultset('NatuurlijkPersoon')->find($self->gmid);

    $self->gm_np(    $gm );
    $self->gm_adres( $self->gm_np->adres_id );

    if ($gm->authenticated) {
        $self->log->debug('Found authenticated user');
        $self->authenticated(1);
    }

    if ($gm->authenticatedby) {
        $self->authenticated_by(
            $gm->authenticatedby
        );
    }

    ### We are loaded external, now let's set up some triggers and attributes
    $self->_load_attributes;

    ### Try to find this person loaded within betrokkenen
    my $intern_loaded = $self->dbic->resultset('GmNatuurlijkPersoon')->search(
        {
            gegevens_magazijn_id    => $self->gmid
        }
    );

    #if ($intern_loaded->count) {
        ### Load contact
        $self->_load_contact_data($self->gmid);
        #}

    $self->gm_extern_np( $self->gm_np );

    return 1;
}

sub _load_intern {
    my ($self) = @_;
    my ($bo);

    (
        $self->log->warn(
            'M::B::NP->load: Could not find internal betrokkene by id ' . $self->id
        ),
        return
    ) unless $bo = $self->dbic->resultset('ZaakBetrokkenen')->find($self->id);

    ### TODO : NO idea yet if I really need this object
    $self->bo($bo);

    ### Retrieve data from internal GM
    return unless $bo->natuurlijk_persoon;

    ### Make sure we have these data for back reference
    $self->gm_np(    $bo->natuurlijk_persoon );
    $self->gm_adres( $self->gm_np->adres_id );

    $self->identifier($bo->natuurlijk_persoon->id . '-' . $self->id);

    ### Define some authenticated info
    ### Search for source (DBG)
    my $dbg = $self->dbic->resultset('NatuurlijkPersoon')->find(
        $bo->natuurlijk_persoon->gegevens_magazijn_id
    );

    if ($dbg->authenticated) {
        $self->log->debug('Found authenticated user');
        $self->authenticated(1);
    }

    if ($dbg->authenticatedby) {
        $self->authenticated_by(
            $dbg->authenticatedby
        );
    }

    ### We are loaded internal, now let's set up some triggers and attributes
    $self->_load_attributes;

    $self->_load_contact_data($bo->natuurlijk_persoon->gegevens_magazijn_id)
        if $bo->natuurlijk_persoon->gegevens_magazijn_id;

    $self->gmid($bo->natuurlijk_persoon->gegevens_magazijn_id);

    $self->gm_extern_np( $dbg );

    return 1;
}

sub _load_attributes {
    my ($self) = @_;

    for my $meth (keys %{ $CONTACT_MAP }) {
        $self->meta->add_attribute($meth,
            'is'        => 'rw',
            ### On update, add custom field back to RT
            'trigger'   => sub {
                my ($self, $new, $old) = @_;
                my ($external_id);

                $self->log->debug('Trigger called for contactupdate: ' .
                    $meth);

                ## Do not update anything when new is the same
                if ($old && $new eq $old) { return $new; }

                if ($self->gmid) {
                    $external_id = $self->gmid;
                } else {
                    $external_id =
                        $self->bo->natuurlijk_persoon->gegevens_magazijn_id;
                }

                my $contactdata = $self->dbic->resultset('ContactData')->search(
                    {
                        gegevens_magazijn_id    => $external_id,
                        betrokkene_type         => 1,
                    }
                );

                if ($contactdata->count) {
                    $contactdata = $contactdata->first;
                    $contactdata->$meth($new);
                    $contactdata->update;
                } else {
                    $contactdata = $self->dbic->resultset('ContactData')->create(
                        {
                            'gegevens_magazijn_id'  => $external_id,
                            'betrokkene_type'       => 1,
                            $meth   => $new,
                        }
                    );
                }
            },
        );
    }

    for my $meth (@{ $CLONE_MAP }, 'adres_id', 'in_gemeente') {
        $self->meta->add_attribute($meth,
            'is'        => 'rw',
            'lazy'      => 1,
            ### On update, add custom field back to RT
            'trigger'   => sub {
                my ($self, $new) = @_;

                # And definetly do not update the adres_id
                if ($meth eq 'adres_id') { return; }

                ### Update object
                $self->gm_np->$meth($new);
                $self->gm_np->update;
            },
            ### Load custom fields from RT
            'default'   => sub {
                my ($self) = @_;

                return $self->gm_np->$meth;
            }
        );
    }

    ### Adres?
    for my $meth (@{ $ADRES_CLONE_MAP }) {
        $self->meta->add_attribute($meth,
            'is'        => 'rw',
            'lazy'      => 1,
            ### On update, add custom field back to RT
            'trigger'   => sub {
                my ($self, $new, $old) = @_;

                ### Update object
                $self->gm_adres->$meth(($new || undef));
                $self->gm_adres->update;

                ### Remove verblijfsobject
                $self->gm_np->verblijfsobject_id(undef);
                $self->gm_np->update;
            },
            ### Load custom fields from RT
            'default'   => sub {
                my ($self) = @_;

                return $self->gm_adres->$meth;
            }
        );
    }
}

sub _make_intern {
    my ($self, $dispatch_options, $gmo) = @_;

    $self->log->debug('M::B::NP->_make_intern called with object: ' . ref($gmo));
    return unless ref($gmo) eq __PACKAGE__;

    (
        $self->log->error('M::B::NP->set: Not an external GM object'),
        return
    ) if $gmo->intern;

    ### Create the copy
    my %np_values           = map { $_ => $gmo->gm_np->$_ }
        @{ $CLONE_MAP };
    my %np_adres_values     = map { $_ => $gmo->gm_adres->$_ }
        @{ $ADRES_CLONE_MAP };

    ### Create the link
    $np_values{'gegevens_magazijn_id'}  = $gmo->gmid;

    my $create = {
        'adres' => \%np_adres_values,
        'gm'    => \%np_values,
    };

    my $res = $self->_create_intern(
        $dispatch_options,
        {},
        $create
    );

    return $res;
}

sub create {
    my ($self, $dispatch_options, $params) = @_;

    my %profile = (
        required => [
            'np-voornamen',
            'np-geslachtsnaam',
            'np-geslachtsaanduiding',
        ],
        dependencies => {
            'np-landcode' => sub {
                my $dfv     = shift;
                my $code    = shift;

                if ($code eq '6030') {
                    return ['np-postcode','np-huisnummer','np-straatnaam','np-woonplaats'];
                } else {
                    return ['np-adres_buitenland1'];
                }
            }
        },
        optional        => [qw/
            authenticated_by
            np-voorletters
            np-huisnummer
            np-postcode
            np-straatnaam
            np-woonplaats
        /],
        optional_regexp => qr/npc?-.*/,
        defaults => { 'np-voorletters' => '' },
    );
    $params = assert_profile($params, profile => \%profile)->valid;

    my ($create) = ({});

    ### generate data
    $create->{gm} = {
        map {
            my $label = $_;
            $label =~ s/^np-//g;
            $label => $params->{ $_ }
        } grep(/^np-/, keys %{$params})
    };

    $create->{gmc} = {
        map {
            my $label = $_;
            $label =~ s/^npc-//g;
            $label => $params->{ $_ }
        } grep(/^npc-/, keys %{ $params })
    };

    $create->{adres} = {};
    for my $adresid (@{ $ADRES_CLONE_MAP }) {
        delete($create->{gm}->{$adresid});

        $create->{adres}->{$adresid} =
            $params->{'np-' . $adresid};
    }

    ### Ongeauthoriseerde gebruiker, geen GBA
    $create->{gm}->{authenticated} =
        (
            $create->{authenticated} ? 1 : undef
        );

    ### Type given?
    if ($params->{authenticated_by}) {
        $create->{gm}->{authenticatedby} =
            $params->{authenticated_by};
    }

    ### Forward to make-intern
    my $boid = $self->_create_extern($dispatch_options,undef,$create);

    $self->log->debug('BO-ID: ' . $boid);

    return $boid;
}

sub _create_extern {
    my ($self, $dispatch_options, $opts, $create) = @_;

    my $npaoo = $dispatch_options->{dbic}->resultset('Adres')->create(
        $create->{'adres'}
    );

    return unless $npaoo;
    $self->log->debug(
        'M::B::NP->_create_extern created adres with id ' .
        $npaoo->id
    );

    ### Copy this ID to our GM
    my $npoo = $dispatch_options->{dbic}->resultset('NatuurlijkPersoon')->create(
        {
            'adres_id'      => $npaoo->id,
            %{ $create->{gm} }
        }
    );

    return unless $npoo;

    $self->log->debug('M::B::NP->_create_extern created gm with id: ' . $npoo->id);

    ### Register contact_data
    if (
        $create->{gmc} &&
        %{ $create->{gmc} }
    ) {
        my $npco = $dispatch_options->{dbic}->resultset('ContactData')->create(
            {
                'gegevens_magazijn_id'  => $npoo->id,
                'betrokkene_type'       => 1,
                %{ $create->{gmc} }
            }
        );
    }

    return $npoo->id;
}

sub _create_intern {
    my ($self, $dispatch_options, $opts, $create) = @_;

    my $npaoo = $dispatch_options->{dbic}->resultset('GmAdres')->create(
        $create->{adres}
    );

    if (!$npaoo) {
        throw("ZS/B/O/NP", "Unable to create GmAdress");
    }

    $self->log->debug(
        'M::B::NP->_create_intern created adres with id ' . $npaoo->id
    );

    $self->log->debug('Create GmNatuurlijkPersoon');
    my $npoo = $dispatch_options->{dbic}->resultset('GmNatuurlijkPersoon')->create(
        {
            adres_id => $npaoo->id,
            %{$create->{gm}}
        }
    );

    my $naam = $self->_build_voorletters($npoo->voornamen || '') . ' ' . naamgebruik({
        aanduiding => $npoo->aanduiding_naamgebruik || '',
        partner_voorvoegsel => $npoo->partner_voorvoegsel,
        partner_geslachtsnaam => $npoo->partner_geslachtsnaam,
        voorvoegsel => $npoo->voorvoegsel,
        geslachtsnaam => $npoo->geslachtsnaam,
    });

    $self->log->debug("Created GmNatuurlijkPersoon: $naam");

    if (!$npoo) {
        throw("ZS/B/O/NP", "Unable to create GmNatuurlijkPersoon");
    }
    $self->log->debug('M::B::NP->_create_intern created gm with id: ' . $npoo->id);

    ### Register contact_data
    if (ref $create->{gmc} eq 'HASH' && $create->{gm}{gegevens_magazijn_id} && scalar keys %{$create->{gmc}}) {
        $self->log->debug('Create Contactdata');
        my $npco = $dispatch_options->{dbic}->resultset('ContactData')->create(
            {
                gegevens_magazijn_id  => $create->{gm}->{gegevens_magazijn_id},
                betrokkene_type       => 1,
                %{ $create->{gmc} }
            }
        );
        if (!$npco) {
            throw("ZS/B/O/NP", "Unable to create ContactData");
        }
        $self->log->debug('M::B::NP->_create_intern created ContactData with id: ' . $npco->id);
    }


    ### Create betrokkene
    my $args = {
        betrokkene_type      => 'natuurlijk_persoon',
        betrokkene_id        => $npoo->id,
        gegevens_magazijn_id => $create->{gm}{gegevens_magazijn_id},
        naam                 => $naam,
    };
    my $bo = $dispatch_options->{dbic}->resultset('ZaakBetrokkenen')->create($args);

    return unless $bo;
    $self->log->debug('M::B::NP->_create_intern created BO:' . $bo->id);

    return $bo->id;
}

sub set {
    my ($self, $dispatch_options, $external_id) = @_;

    ### We assume id is a GM id, because we cannot set an old betrokkene
    ### 'again'. So, we will load __PACKAGE__ with trigger get and as an
    ### external object. Feed it to our internal baker, and return a classy
    ### string with information;
    my $identifier = $external_id . '-';

    $self->log->debug('M::B::NP->set called with identifier: ' . $identifier);

    # Load external id
    my $gmo = __PACKAGE__->new(
        'trigger'       => 'get',
        'id'            => $external_id,
        'intern'        => 0,
        %{ $dispatch_options },
    );

    if (!$gmo) {
        throw("Betrokkene/Object/NP", "Unable to create Natuurlijk persoon met ID $external_id");
    }

    # Feed it to our baker
    my $bid = $self->_make_intern($dispatch_options, $gmo);
    if (!$bid) {
        $self->log->error('M::B::NP->set no bid for gmo: ' . $gmo->id);
        return;
    }
    $identifier .= $bid;

    $self->log->debug('M::B::NP->set create identifier ' . $identifier);

    return 'natuurlijk_persoon-' . $identifier;
}


#sub _set {
#    my ($self, $id) = @_;
#    my ($copy, $adrescopy) = {};
#
#    ### Found this id?
#    my $npo = $self->dbicg->resultset('NatuurlijkPersoon')->find($id);
#    return unless $npo;
#
#    $copy->{ $_ } = $npo->$_ for @{ $CLONE_MAP };
#
#    my $npadreso = $npo->adres_id;
#
#    $adrescopy->{ $_ } = $npadreso->$_ for @{ $ADRES_CLONE_MAP };
#
#    my $npaoo = $self->dbic->resultset('GmAdres')->create(
#        {
#            %$adrescopy
#        }
#    );
#
#    ### Copy this ID to our GM
#    my $npoo = $self->dbic->resultset('GmNatuurlijkPersoon')->create(
#        {
#            'gegevens_magazijn_id'  => $id,
#            'adres_id' => $npaoo->id,
#            %$copy
#        }
#    );
#
#    #$self->log->debug('Gaatie? ');
#
#    ### Set this id
#    my $bo = $self->dbic->resultset('ZaakBetrokkenen')->create({
#        'betrokkene_type'           => 'natuurlijk_persoon',
#        'betrokkene_id'             => $npoo->id,
#        'gegevens_magazijn_id'      => $id,
#        'naam'                      => $npoo->naam,
#    });
#
#    return $bo->id;
#}

sub verwijder {
    my ($self) = @_;

    $self->gm_extern_np->deleted_on(DateTime->now);
    return $self->gm_extern_np->update;
}

=head2 as_hashref

Returns the betrokkene data as a plain hashref.

=cut

sub as_hashref {
    my $self = shift;

    return {
        id                => $self->betrokkene_identifier,
        name              => $self->display_name,
        street            => join(
            ' ',
            grep { $_ } (
                $self->straatnaam,
                $self->huisnummer,
                $self->huisnummertoevoeging
            )
        ),
        city              => $self->woonplaats,
        postal_code       => $self->postcode,
        telephone_numbers => [ grep { $_ } ($self->telefoonnummer, $self->mobiel) ],
        email_addresses   => [ grep { $_ } ($self->email) ],
        type              => 'natuurlijk_persoon',
        gmid              => $self->gmid,
    };
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 BOBJECT

TODO: Fix the POD

=cut

=head2 BRSOBJECT

TODO: Fix the POD

=cut

=head2 BUILD

TODO: Fix the POD

=cut

=head2 FRIENDLY_BETROKKENE_MESSAGES

TODO: Fix the POD

=cut

=head2 create

TODO: Fix the POD

=cut

=head2 is_verhuisd

TODO: Fix the POD

=cut

=head2 search

TODO: Fix the POD

=cut

=head2 set

TODO: Fix the POD

=cut

=head2 verwijder

TODO: Fix the POD

=cut

