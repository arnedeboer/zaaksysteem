package Zaaksysteem::View::TT;

use strict;
use base 'Catalyst::View::TT';
use DateTime;
use Zaaksysteem::Constants;
use Data::Dumper;

__PACKAGE__->mk_accessors(qw/_app_root_dirs/);

__PACKAGE__->config({
    PLUGIN_BASE => 'Zaaksysteem::Template::Plugin',
    RELATIVE    => 1,
    WRAPPER     => 'tpl/zaak_v1/nl_NL/layouts/wrapper.tt',
    RECURSION   => 1,
    PRE_CHOMP   => 2,
    POST_CHOMP  => 2,
});




sub render {
    my $self    = shift;
    my ($c, $main_template)     = @_;

    $c->log->debug("main_template: " . $main_template);
    $c->stash->{template_root}  = $c->config->{root} . '/tpl/zaak_v1/nl_NL';
    $c->stash->{template_site}  = '/tpl/zaak_v1/nl_NL';

    $c->stash->{constants}      =
        $c->stash->{ZCONSTANTS} = ZAAKSYSTEEM_CONSTANTS;
    $c->stash->{ZNAMING}        = ZAAKSYSTEEM_NAMING;
    $c->stash->{ZOPTIONS}       = ZAAKSYSTEEM_OPTIONS;
    $c->stash->{helpers}        = {
        'date'      => DateTime->now()
    };

    $c->stash->{ENV}            = \%ENV;

    $c->stash->{invoke_assets_minified} = 1 if
        $c->config->{invoke_assets_minified};

    $c->stash->{additional_template_paths} = [
        $c->stash->{template_root}
    ];

    $c->stash->{cache_javascript} = $c->config->{cache_javascript} if $c->config->{cache_javascript};

    return $self->NEXT::render( @_ );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 ZAAKSYSTEEM_CONSTANTS

TODO: Fix the POD

=cut

=head2 ZAAKSYSTEEM_NAMING

TODO: Fix the POD

=cut

=head2 ZAAKSYSTEEM_OPTIONS

TODO: Fix the POD

=cut

=head2 render

TODO: Fix the POD

=cut

