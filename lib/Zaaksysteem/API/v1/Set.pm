package Zaaksysteem::API::v1::Set;

use Moose;
use namespace::autoclean -except => 'meta';

use Zaaksysteem::Tools;

=head1 NAME

Zaaksysteem::API::v1::Set - Marry the concepts of an API view of data with
L<Zasksyteem::Object::Iterator>.

=head1 SYNOPSIS

    my $set = Zaaksysteem::API::v1::Set->new(
        iterator => $c->model('Object')->search(...),
    );

    $set->init_paging($c->request);

    # Serialize the dataview with a page.
    $c->stash->{ result } = $set;

=head1 DESCRIPTION

This class exists for the purpose of marrying the concepts of a 'dataview', as
experienced by an APIv1 consumer and the underlying iterator/resultset. This
object has all the metadata required for the API's serializer to produce
representations of a paged dataset.

The idea is that instead of stitching together page state data from query
parameters (like most actions do), instances of this object abstract that
need away in a consistant way.

=head1 ATTRIBUTES

=head2 iterator

Holds an L<Zaaksysteem::Object::Iterator> object that is to be used as a base
resultset of the resource.

=cut

has iterator => (
    is => 'rw',
    isa => 'Zaaksysteem::Object::Iterator',
    required => 1
);

=head2 page

State field that determines the current page within the dataview is to be
used. Defaults to page C<1>.

=cut

has page => (
    is => 'rw',
    isa => 'Int',
    required => 1,
    default => 1
);

=head2 rows_per_page

Limit on the maximum amount of entries/items/rows on a single page. Defaults
to C<10>.

=cut

has rows_per_page => (
    is => 'rw',
    isa => 'Int',
    required => 1,
    default => 10
);

=head2 uri

Holds an L<URI> object that is used as a base to provide paged access to the
resource.

=cut

has uri => (
    is => 'rw',
    isa => 'URI'
);

=head1 METHODS

=head2 init_paging

This method initializes the set with the current 'view', according to the
request being made.

=head3 Signature

C<< Catalyst::Request => Zaaksysteem::API::v1::Set >>

=cut

sig init_paging => 'Catalyst::Request => Zaaksysteem::API::v1::Set';

sub init_paging {
    my $self = shift;
    my $res = shift;
    my $params = $res->params;

    $self->uri($res->uri->clone);

    if (exists $params->{ page }) {
        $self->page($params->{ page });
    }

    return $self;
}

=head2 mangle_uri

Produces a string representation of an URI that can be passed to the frontend
to retrieve a view of the object's data set to a specific page.

=head3 Signature

C<< Maybe[Int] => Maybe[Str] >>

=head3 Example call

    my $uri = $set->mangle_uri($next_page);

=cut

sub mangle_uri {
    my $self = shift;
    my $page = shift;

    return undef unless defined $page;

    my $uri = $self->uri->clone;

    $uri->query_param(page => $page);

    return $uri->as_string;
}

=head2 build_iterator

This method produces a new L<iterator|Zaaksysteem::Object::Iterator>, using
L</iterator> as a base, with L</page> and L</rows_per_page> mangled in the
underlying L<DBIx::Class::ResultSet>.

=head3 Signature

C<< => Zaaksysteem::Object::Iterator >>

=head3 Example call

    my $iter = $set->build_iterator;

=cut

sub build_iterator {
    my $self = shift;

    # We're breaking domain here, we shouldn't know anything about the
    # internals of Iterator, but it only abstracts a set of results, not the
    # capacity to reconfigure the query.
    ### Welp, it looks like Iterator *can* reconfigure the search
    ### (set_pager_attributes). But it shouldn't. So I won't depend on it here.
    return $self->iterator->clone(
        rs => $self->iterator->rs->search_rs(undef, {
            rows => $self->rows_per_page,
            page => $self->page
        })
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
