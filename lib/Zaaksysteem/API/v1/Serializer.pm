package Zaaksysteem::API::v1::Serializer;

use Moose;

use Zaaksysteem::Tools;
use List::Util qw[first];

use Module::Pluggable
    search_path => __PACKAGE__ . '::Reader',
    instantiate => 'new',
    sub_name    => 'readers';

use Module::Pluggable
    search_path => __PACKAGE__ . '::Encoder',
    instantiate => 'new',
    sub_name    => 'encoders';

=head1 NAME

Zaaksysteem::API::v1::Serializer - API v1 Serializer

=head1 SYNOPSIS

    my $serializer = Zaaksysteem::API::v1::Serializer->new(
        encoding_format => 'JSON'
    );

    my $data = $serializer->serialize($object);

    # OR

    my $data = $serializer->encode($serializer->read($object));

=head1 DESCRIPTION

This serializer works in a 2-step fashion, reading an object into a plain Perl
datastructure, followed by an encoding run based on the configured
L</encoding_format>.

Every object reader is implemented as a plugin found in the
L<Zaaksysteem::API::v1::Serializer::Reader> namespace.

Every encoder is implemented as a plugin found in the
L<Zaaksysteem::API::v1::Serializer::Encoder> namespace.

=head1 ATTRIBUTES

=head2 encoding_format

Attribute that holds a format type used to find the correct encoder plugin.

=cut

has encoding_format => (
    is => 'rw',
    default => 'JSON'
);

=head1 METHODS

=head2 serialize

This method attempts to fully serialize an object of arbitrairy type by
chaining the L</read> and L</encode> methods.

    my $data = $serializer->serialize($object);

=cut

sub serialize {
    my $self = shift;

    return $self->encode($self->read(shift));
}

=head2 encode

This method attempts to encode a plain Perl datastructure into a serialized
format.

A specific encoder will be selected based on the configured
L</encoding_format> by iterating over all available encoder plugins and asking
if the plugin handles the format.

B<Caveat>: a plugin is selected on first-come-first-serve basis, where the
order of plugins is undefined. Basically, don't build encoders with
overlapping support for one or more formats.

=cut

sub encode {
    my $self = shift;
    my $data = shift;

    # Find the first encoder that claims to encode in $fmt
    my $encoder = first {
        $_->grok($self->encoding_format)
    } $self->encoders(serializer => $self);

    unless (defined $encoder) {
        throw('api/v1/serializer', sprintf(
            'Configured to serialize to "%s", no such encoder.',
            $self->encoding_format
        ));
    }

    return $encoder->encode($self, $data);
}

=head2 read

This method attempts to read an arbitrairy object, returning a plain
(non-blessed) Perl datastructure which can be fed to an encoder.

A specific reader will be selected by iterating over all available
reader plugins and selecting the first one to claim support for the provided
object.

B<Caveat>: a plugin is selected on first-come-first-serve basis, where the
order of plugins is undefined. Basically, don't build readers with overlapping
support for one or more objecttypes.

=cut

sub read {
    my $self = shift;
    my $data = shift;

    my $reader = first { defined }
                   map { $_->grok($data) }
                       $self->readers;

    unless (defined $reader) {
        throw('api/v1/serializer', sprintf(
            'No reader found for "%s"',
            $data // '<undef>'
        ));
    }

    return $reader->($self, $data, @_);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
