package Zaaksysteem::API::v1::Message;

use Moose;

=head1 NAME

Zaaksysteem::API::v1::Message

=head1 DESCRIPTION

This class functions as a base class for API messages. By itself it doesn't
do anything but reserve the namespace and implement the required interface for
the API serializer.

=head1 METHODS

=head2 field_hash

Placeholder method for inheriting classes.

=cut

sub field_hash {
    return undef;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
