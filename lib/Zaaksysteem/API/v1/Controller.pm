package Zaaksysteem::API::v1::Controller;

use Moose;
use namespace::autoclean;

use Zaaksysteem::Tools;

BEGIN { extends 'Catalyst::Controller::ActionRole' }

__PACKAGE__->config(
    action_roles => [qw[+Zaaksysteem::API::v1::ActionRole]]
);

=head1 NAME

Zaaksysteem::API::v1::Controller - Base controller class for APIv1 controllers

=head1 SYNOPSIS

    package Zaaksysteem::Controller::MyController;

    use Moose

    BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

    sub my_action : Chained(...) : PathPart(...) : Args(0) {
        ...
    }

=head1 DESCRIPTION

This base controller uses L<Catalyst::Controller::ActionRole> to apply
L<Zaaksysteem::API::v1::ActionRole> to all actions defined in child class
controllers.

=head1 ACTIONS

=head2 end

This default C<end>-action simply detaches to L<Zaaksysteem::View::API::v1>.

=cut

sub end : Private {
    my ($self, $c) = @_;

    return if $c->stash->{ digest_response };

    $c->detach($c->view('API::v1'));
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
