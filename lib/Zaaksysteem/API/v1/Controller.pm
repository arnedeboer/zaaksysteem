package Zaaksysteem::API::v1::Controller;
use Moose;
use namespace::autoclean;

use Zaaksysteem::Tools;

# Beware:
# As of version 5.90013, Catalyst has merged this functionality into the
# core Catalyst::Controller. You should no longer use it for new development
# and we recommend switching to the core controller as soon as practical.
BEGIN { extends 'Catalyst::Controller::ActionRole' }

with 'MooseX::Log::Log4perl';

__PACKAGE__->config(
    action_roles => [qw[+Zaaksysteem::API::v1::ActionRole]]
);


=head1 NAME

Zaaksysteem::API::v1::Controller - Base controller class for APIv1 controllers

=head1 SYNOPSIS

    package Zaaksysteem::Controller::MyController;

    use Moose

    BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

    sub my_action : Chained(...) : PathPart(...) : Args(0) {
        ...
    }

=head1 DESCRIPTION

This base controller uses L<Catalyst::Controller::ActionRole> to apply
L<Zaaksysteem::API::v1::ActionRole> to all actions defined in child class
controllers.

=head1 ATTRIBUTES

=head2 api_control_module_types

TODO: Document me

=cut

has api_control_module_types => (
    is          => 'rw',
    default     => sub { ['api'] },
    isa         => 'ArrayRef',
);

=head2 namespace

The namespace in which the set will be placed

=cut

has namespace => (
    is       => 'ro',
    isa      => 'Str',
    required => 0, # For now it is not required, many modules do not have it.
);

=head1 ACTIONS

=head2 end

This default C<end>-action simply detaches to L<Zaaksysteem::View::API::v1>.

=cut

sub end : Private {
    my ($self, $c) = @_;

    return if $c->stash->{ api_unauthorized_response };

    $c->detach($c->view('API::v1'));
}

=head1 METHODS

=head2 get_set_from_zql

    my $set = $self->_get_set_from_zql(
        $c,
        "ZQL statement"
    );

Dies on error.

=cut

sub get_set_from_zql {
    my ($self, $c, $zql) = @_;

    my $set = try {
        return Zaaksysteem::API::v1::Set->new(
            iterator => $c->model('Object')->zql_search($zql)
        );
    } catch {
        $self->log->fatal("get_set_from_zql with '$zql' generated the following error: '$_'");
        throw(
            'api/v1/get_set_from_zql',
            'Internal API error, please report a bug'
        );
    };
    $c->stash->{set}  = $set;
    $c->stash->{$self->namespace} = $set->build_iterator->rs;
    return $set;
}

=head2 list_set

Return a list of found objects.
If pagination is allowed, callers may use paging as a param for the iterator.

=cut

sub list_set {
    my ($self, $c) = @_;
    $c->stash->{result} = $c->stash->{set}->init_paging($c->request);
}

=head2 get_object

Return the object.

=cut

sub get_object {
    my ($self, $c) = @_;
    $c->stash->{result} = $c->stash->{ $self->namespace };
    return $c->stash->{ $self->namespace };
}

=head2 save_object

Save and return the object.

=cut

sub save_object {
    my ($self, $c, $object) = @_;
    $object = $c->model('Object')->save(object => $object);
    $c->stash->{ $self->namespace } = $object;
    return $object;
}

=head2 assert_post

Assert if a request is a HTTP post method.

=cut

sub assert_post {
    my ($self, $c) = @_;

    my $m = uc($c->req->method);
    return 1 if $m eq 'POST';
    throw('api/v1/request_method/not_allowed', "HTTP request method $m is not allowed: POST only");
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
