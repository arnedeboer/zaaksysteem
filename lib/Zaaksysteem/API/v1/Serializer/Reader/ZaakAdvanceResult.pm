package Zaaksysteem::API::v1::Serializer::Reader::ZaakAdvanceResult;

use Moose;

with 'Zaaksysteem::API::v1::Serializer::ReaderRole';

sub class { 'Zaaksysteem::Zaken::AdvanceResult' }

sub read {
    my ($class, $serializer, $result) = @_;

    my $transitionable = $result->can_advance;

    my %retval = (
        transitionable => $transitionable ? \1 : \0
    );

    unless ($transitionable) {
        $retval{ reasons } = [ $result->reasons ];
    }

    return \%retval;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
