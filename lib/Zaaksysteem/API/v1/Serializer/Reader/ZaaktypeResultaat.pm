package Zaaksysteem::API::v1::Serializer::Reader::ZaaktypeResultaat;

use Moose;

with 'Zaaksysteem::API::v1::Serializer::ReaderRole';

=head1 NAME

=head1 SYNOPSIS

=head1 METHODS

=head2 class

=cut

sub class { 'Zaaksysteem::Model::DB::ZaaktypeResultaten' }

=head2 read

=cut

sub read {
    my ($class, $serializer, $resultaat) = @_;

    return {
        id => $resultaat->id,
        type => $resultaat->resultaat,
        label => $resultaat->label
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
