package Zaaksysteem::API::v1::Serializer::Reader::File;

use Moose;

with 'Zaaksysteem::API::v1::Serializer::ReaderRole';

=head1 NAME

Zaaksysteem::API::v1::Serializer::Reader::File - Read document objects

=head1 DESCRIPTION

=head1 METHODS

=head2 class

Implements interface required by
L<Zaaksysteem::API::v1::Serializer::ReaderRole>.

=cut

sub class { 'Zaaksysteem::Backend::File::Component' }

=head2 read

Implements interface required by
L<Zaaksysteem::API::v1::Serializer::ReaderRole>.

=cut

sub read {
    my ($class, $serializer, $file) = @_;

    return {
        type => 'file',
        reference => $file->filestore_id->uuid,
        instance => {
            id => $file->filestore_id->uuid,
            created => $serializer->read($file->date_created),
            last_modified => $serializer->read($file->date_modified),
            title => $file->name,
            name => $file->filename,
            mimetype => $file->filestore_id->mimetype,
            checksum => sprintf('md5:%s', $file->filestore_id->md5),
            size => $file->filestore_id->size,
            metadata => (!$file->metadata_id ? {} :
                { map { $_ => $file->metadata_id->$_ } qw/origin document_category trust_level description/ }
            ),
        }
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
