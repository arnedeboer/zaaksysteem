package Zaaksysteem::API::v1::Serializer::Reader::QueueItem;

use Moose;

with 'Zaaksysteem::API::v1::Serializer::ReaderRole';

=head1 NAME

Zaaksysteem::API::v1::Serializer::Reader::QueueItem - Read queue item objects

=head1 DESCRIPTION

=head1 METHODS

=head2 class

Implements interface required by
L<Zaaksysteem::API::v1::Serializer::ReaderRole>.

=cut

sub class { 'Zaaksysteem::Model::DB::Queue' }

=head2 read

Implements interface required by
L<Zaaksysteem::API::v1::Serializer::ReaderRole>.

=cut

sub read {
    my ($class, $serializer, $item) = @_;

    return {
        type => 'queue_item',
        reference => $item->id,
        instance => {
            data => $item->data,
            status => $item->status,
            type => $item->type,
            label => $item->label,
            parent_id => $item->get_column('parent_id'),
            object_id => $item->get_column('object_id')
        }
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
