package Zaaksysteem::API::v1::Serializer::Reader::PreparedFileBag;

use Moose;

with 'Zaaksysteem::API::v1::Serializer::ReaderRole';

sub class { 'Zaaksysteem::API::v1::PreparedFileBag' }

sub read {
    my ($class, $serializer, $bag) = @_;

    return {
        type => 'filebag',
        reference => undef,
        instance => {
            references => $bag->file_references
        }
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
