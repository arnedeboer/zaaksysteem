package Zaaksysteem::API::v1::Serializer::Reader::LegacyJob;
use Moose;

use Zaaksysteem::Tools;

with 'Zaaksysteem::API::v1::Serializer::ReaderRole';

=head1 NAME

Zaaksysteem::API::v1::Serializer::Reader::LegacyJob - Reader for legacy non-objectdata Scheduled Jobs

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 METHODS

=head2 class

=cut

sub class { 'Zaaksysteem::DB::Component::ScheduledJobs' }

=head2 read

=cut

sig read => 'Zaaksysteem::API::v1::Serializer, Object => HashRef';

sub read {
    my ($self, $serializer, $object) = @_;

    my %job = map { $_ => $object->$_ } qw(
        task
        scheduled_for
        parameters
        created
        last_modified
        deleted
        schedule_type
        uuid
    );

    my $uuid = ($object->get_column('case_id') && $object->case_id->object_data)
        ? $object->case_id->object_data->id
        : undef;

    $job{case} = {
        reference => $uuid,
        type      => 'case',
    };

    return {
        type => 'scheduled_job',
        reference => $object->uuid,
        instance => \%job,
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
