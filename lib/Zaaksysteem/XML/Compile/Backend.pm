package Zaaksysteem::XML::Compile::Backend;

use Moose;
use Zaaksysteem::Exception;

with 'Zaaksysteem::Log::LoggerRole';

=head1 NAME

Zaaksysteem::XML::Compile::Backend - Backend object, to provide accessors to different XSD schema instances

=head1 SYNOPSIS

    ### Within a different module
    my $backend     = Zaaksysteem::XML::Compile->xml_compile;

    $backend->add_class('Zaaksysteem::StUF::0204::Instance');

    ### Load StUF 0204 instance
    my $stuf0204    = $backend->stuf0204

    ### Get schema from it
    my $schema      = $stuf0204->schema;

=head1 DESCRIPTION

This object holds different XSD schema instances. Look at L<Zaaksysteem::XML::Compile::Instance>
for more information about the setup of such instance. It makes sure XML::Compile is only needed
once in an application.

=head1 ATTRIBUTES

=head2 home

isa: Str

Home directory of the application, will be given to the instances so these can find where
their XSD's are located (relatively)

=cut

has 'home'      => (
    'is'        => 'rw',
);

=head2 objects

isa: HashRef( name => isa:Zaaksysteem::XML::Compile::Instance )

    print Data::Dumper::Dumper($objects);

    VAR1 = {
        stuf0204 => bless({}, 'Zaaksysteem::StUF::0204::Instance'),
        stuf0301 => bless({}, 'Zaaksysteem::StUF::0301::Instance'),
    };

Contains the different instances which inherit from L<Zaaksysteem::XML::Compile::Instance>. Named
by name retrieved from instance object.

=cut

has 'objects' => (
    is      => 'rw',
    isa     => 'HashRef',
    default => sub { {} },
);

=head1 METHODS

=head2 add_class

Arguments: Object inherited from L<Zaaksysteem::XML::Compile::Instance>

Return value: $self

    ### Add single instance
    $backend->add_class(Zaaksysteem::StUF::0204::Instance);

    ### Multiple

    $backend->add_class([
        qw/
            Zaaksysteem::StUF::0204::Instance
            Zaaksysteem::StUF::0301::Instance
        /
    ]);

Adds a class instance to this backend object. When a class already exists, it will silently
skip.

This instance creates an accessor on this C< $backend > object, and will make sure all the
elements defined in this instance are loaded.

At the end, it will fill C<< $backend->objects >> with this class by setting the key to
C<< $instance->name >>

=cut

sub add_class {
    my $self        = shift;
    my $class       = shift;

    my @classes     = (
        ref $class eq 'ARRAY'
            ? @{ $class }
            : $class
    );
    #my $opts        = assert_profile(shift || {})->valid;

    for my $class (@classes) {
        next if grep { ref($_) eq $class } values %{ $self->objects };

        Class::MOP::load_class($class);

        throw(
            'xml/compile/backend/add_class/invalid_isa',
            "Class '$class' does not implement Zaaksysteem::XML::Compile::Instance"
        ) unless $class->does('Zaaksysteem::XML::Compile::Instance');

        my $object      = $class->new(home => $self->home);
        my $name        = $object->name;

        $self->objects->{ $name } = $object;

        $self->meta->add_attribute(
            $object->name,
            is          => 'rw',
            lazy        => 1,
            default     => sub {
                my $self        = shift;

                return $self->objects->{ $name };
            }
        );
    }

    return $self;
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Install>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
