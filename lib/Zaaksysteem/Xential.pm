package Zaaksysteem::Xential;
use Moose;

=head1 NAME

Zaaksysteem::Xential

=head1 SYNOPSIS

    use Zaaksysteem::Xential;

    my $interface = $zs->schema->resultset('Interface')
        ->search_active({ module => 'xential' })->first;

    my $xential = Zaaksysteem::Xential->new_from_interface($interface);

    # Starting a document
    $xential->prepare_preregistered_with_options;
    $xential->start_document;
    $xential->build_document;

    # Getting the details
    $xential->get_build_status;

    # Get the version of Xential
    # (primairly used to check if they are up).
    $xential->get_version
    $xential->get_version_info

=cut

use Zaaksysteem::Tools;
use Zaaksysteem::Types qw[UUID NonEmptyStr];
use Zaaksysteem::XML::Compile;
use Zaaksysteem::XML::Zaaksysteem::Serializer;

with 'MooseX::Log::Log4perl';

=head1 ATTRIBUTES

=head2 interface

=cut

has interface => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Model::DB::Interface',
    required => 1,
);

has schema => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Schema',
    lazy     => 1,
    default  => sub {
        my $self = shift;
        return $self->interface->result_source->schema;
    }
);

=head2 xential

The Xential SOAP implementation

=cut

has xential => (
    is      => 'ro',
    lazy    => 1,
    default => sub {
        my $self     = shift;
        my $instance = Zaaksysteem::XML::Compile->xml_compile->add_class(
            'Zaaksysteem::XML::Xential::Instance')->xential;
        return $instance;
    }
);

has endpoints => (
    is      => 'ro',
    isa     => 'HashRef',
    lazy    => 1,
    default => sub {
        my $self = shift;
        my $config = $self->interface->get_interface_config;
        my %endpoints = (
            ContextService   => $config->{contextservice}  // undef,
            BuildService     => $config->{buildservice}    // undef,
            DocumentsService => $config->{documentservice} // undef,
            InfoService      => $config->{infoservice}     // undef,
            UtilService      => $config->{utilservice}     // undef,
        );
        return \%endpoints;
    },
);

=head2 new_from_interface

Create a new object of L<Zaaksysteem::Xential> based on the interface.

=cut

sub new_from_interface {
    my ($self, $interface) = @_;

    return $self->new(
        interface => $interface
    );
}

=head2 list_bases

Call listBases at Xential

=cut

sub list_bases {
    my ($self, $opts) = @_;

    $opts->{endpoint} //= $self->endpoints->{ContextService};

    return $self->_do_call('list_bases', undef, $opts);
}

=head2 create_context

Call createContext at Xential

=cut

define_profile create_context => (
    required => {
        baseId   => 'Str',
        userName => 'Str',
        password => 'Str',
    },
);

sub create_context {
    my ($self, $params, $opts) = @_;

    $opts->{endpoint} //= $self->endpoints->{ContextService};

    return $self->_do_call('create_context', $params, $opts);
}

=head2 get_version

Get the version from Xential, returns a scalar value;

=cut

sub get_version {
    my ($self, $opts) = @_;

    $opts->{endpoint} //= $self->endpoints->{InfoService};

    return $self->_do_call('get_version', undef, $opts)->{'return'}
}

=head2 get_version_info

Get the version from Xential, returns a scalar value;

=cut

sub get_version_info {
    my ($self, $opts) = @_;

    $opts->{endpoint} //= $self->endpoints->{InfoService};

    return $self->_do_call('get_version_info', undef, $opts)
}

=head2 get_templates

Get templates from Xential

=cut

sub get_templates {
    my ($self, $opts) = @_;

    $opts->{endpoint} //= $self->endpoints->{InfoService};

    return $self->_do_call('get_templates', undef, $opts)
}

=head2 get_registration_info

    $xential->get_registration_info(
        { xentialRegistrationUuid => $uuid}
    );

=cut

define_profile get_registration_info => (
    required => {
        registrationUuid => UUID,
    },
    optional => {
        context => 'HashRef',
    },
);

sub get_registration_info {
    my ($self, $params, $opts) = @_;

    $opts->{endpoint} //= $self->endpoints->{InfoService};

    return $self->_do_call('get_registration_info', $params, $opts)
}

=head2 prepare_preregistered_with_options

Call Xential with a preparePreregisteredWithOptions statement

=cut

define_profile prepare_preregistered_with_options => (
    required => {
        xml             => 'Str',
        selectionOption => 'HashRef',
        storageOptions  => 'HashRef',
    },
    optional => {
        statusUpdateOption => 'HashRef',
        context => 'HashRef',
    }
);

sub prepare_preregistered_with_options {
    my ($self, $params, $opts) = @_;

    $opts->{endpoint} //= $self->endpoints->{BuildService};

    return $self->_do_call('prepare_preregistered_with_options', $params, $opts);
}

=head2 start_document

Call Xential with a startDocument statement

=cut

define_profile start_document => (
    required => {
        ticketUuid    => UUID,
        documentTitle => NonEmptyStr,
    },
    optional => {
        userName => NonEmptyStr,
        context  => 'HashRef',
    },
);

sub start_document {
    my ($self, $params, $opts) = @_;
    $params = assert_profile($params)->valid;

    $opts->{endpoint} //= $self->endpoints->{BuildService};

    return $self->_do_call('start_document', $params, $opts);
}

=head2 build_document

Call Xential with a buildDocument statement

=cut

define_profile build_document => (
    required => {
        documentId    => NonEmptyStr,
    },
    optional => {
        priority => NonEmptyStr,
        context  => 'HashRef',
    },
    defaults => {
        priority   => 'NORMAL',
    },
    constraint_methods => {
        priority => qr/^(?:NORMAL|LOW)$/,
    },
);

sub build_document {
    my ($self, $params, $opts) = @_;
    $params = assert_profile($params)->valid;

    $opts->{endpoint} //= $self->endpoints->{BuildService};

    return $self->_do_call('build_document', $params, $opts);
}

=head2 get_build_status

Call Xential with a getBuildStatus statement

=cut

define_profile get_build_status => (
    required => {
        documentId    => 'Int',
    },
);

sub get_build_status {
    my ($self, $params, $opts) = @_;

    $opts->{endpoint} //= $self->endpoints->{BuildService};

    return $self->_do_call('get_build_status', $params, $opts);
}

define_profile create_document_from_template => (
    required => {
        case           => 'Zaaksysteem::Schema::Zaak',
        template_uuid  => 'Str',
        document_title => 'Str',
    },
    optional => {
        username    => 'Str',
        transaction => 'Zaaksysteem::Model::DB::Transaction',

        # The following options should only be used for testing purposes.
        xml    => 'Str',
        target => 'Str',
    },
    defaults => { target => 'ZAAKSYSTEEM', },
    constraint_methods => {
        target => qr/^(?:ZAAKSYSTEEM|NONE)$/,
    }
);

=head2 create_document_from_template

The function which you will want to use if you want to create a document from Xential.

=head3 ARGUMENTS

=over

=item * case

A L<Zaaksysteem::Schema::Zaak> object, required.

=item * template_uuid

A UUID of the template found at Xential, required.

=item * username

A username of the user, optional.

=item * transaction

A transaction of Zaaksystem, optional.

=item * xml

XML, should only be used for testing!

=item * target

A target, currently accepted: 'ZAAKSYSTEEM' and 'NONE', should
also only be used for testing!

=back

=head3 RETURNS

This functions returns the following data structure

    {
        case          => '42',
        template_uuid => '71ca37f6-432e-446d-8161-07da9b108397',
        document      => {
            id => '666',
            title => 'Some title',
            ticket_uuid => 'b3357d4c-f07f-4555-b06f-72a0c16f6d4d',
            xential_uuid => '88d337ff-69f2-41c8-8329-9b3b403c2b18',
            start_url => 'https://some.url.example.com/start',
            resume_url => 'https://some.url.example.com/resume',
        },
        status   => 'done',
        build_id => '999',
    };

The status can be:

=over

=item prepare

=item start

=item complex_build

=item done

=item build_fail

=back

=cut

sub create_document_from_template {
    my $self = shift;
    my $options = assert_profile({@_})->valid;

    # The context should *ALWAYS* be created, Xential advises this.
    my $context = $self->create_context({
        baseId   => $self->interface->get_interface_config->{contextservice_base_id},
        userName => $self->interface->get_interface_config->{contextservice_username},
        password => $self->interface->get_interface_config->{contextservice_password},
    })->{createContext};

    my $case        = delete $options->{case};
    my $uuid        = delete $options->{template_uuid};
    my $username    = delete $options->{username};
    my $transaction = delete $options->{transaction};

    # Make sure we retreive the transaction from the DB to get a UUID
    $transaction->discard_changes if $transaction;

    # Be able to override XML is testcase scenario's
    my $xml = delete $options->{xml};
    if (!defined $xml) {
        my $s = Zaaksysteem::XML::Zaaksysteem::Serializer->new(
            schema => $self->schema,
        );
        $xml = $s->case_to_xml($case);
    }

    # Define relatedid as an URL
    my %related_params = (
        transaction_uuid => $transaction->uuid,
        case_uuid        => $case->object_data->uuid,
        file_uuid        => undef,
        interface_uuid   => $transaction->interface_id->uuid,
    );

    my $related_id     = join('&',
        map ({ $_ . '=' . ($related_params{$_} || '') } keys %related_params)
    );


    my %arguments = (
        xml             => $xml,
        selectionOption => { selectionByUuid => $uuid, },

        storageOption => {
            fixed      => 1,
            target     => delete $options->{target},
            properties => { entry => [] },
            $transaction
            ? (documentRelationSettings => { relatedid => $related_id })
            : (),
        },

        context => $context,
    );

    my %answer = (
        case          => $case->id,
        template_uuid => $uuid,
        document      => {
            id          => undef,
            title       => delete $options->{document_title},
            ticket_uuid => undef,

            xential_uuid => undef,
            start_url    => undef,
            resume_url   => undef,
        },
        context  => $context,
        status   => 'prepare',
        build_id => undef,
        $username ? (username => $username) : (),
        $transaction ? (transaction_uuid => $transaction->uuid) : (),
    );

    my $prepare = $self->prepare_preregistered_with_options(\%arguments, $options)->{preparePreregisteredResult};

    my %startargs = (
        ticketUuid    => $prepare->{ticketUuid},
        documentTitle => $answer{document}->{title},
        defined $username ? (userName => $username) : (),
        context => $context,
    );

    $answer{status}                 = 'start';
    $answer{document}{ticket_uuid}  = $prepare->{ticketUuid};
    $answer{document}{xential_uuid} = $prepare->{xentialRegistrationUuid};
    $answer{document}{start_url}    = $prepare->{startDocumentUrl};

    my $start = $self->start_document(\%startargs, $options)->{startDocumentResult};

    if ($start->{status} ne 'VALID') {
        $answer{document}{resume_url} = $start->{resumeDocumentUrl};
        $answer{status} = 'complex_build';

        ### Fix url, add afterOpenAction
        my $uri = URI->new($answer{document}{resume_url});
        $uri->query_form($uri->query_form, afterOpenAction => 'close');
        $answer{document}{resume_url} = $uri->as_string;

        return \%answer;
    }

    $answer{document}{resume_url} = $start->{resumeDocumentUrl};
    $answer{document}{id}         = $start->{documentId};
    $answer{status}               = 'build';

    my %build_args = (
        documentId => $start->{documentId},
        priority   => 'NORMAL',
        context    => $context,
    );

    my $build = $self->build_document(\%build_args, $options)->{buildResult};
    if ($build->{id}) {
        $answer{build_id} = $build->{id};
        $answer{status} = 'done';
    }
    else {
        $answer{status} = 'build_fail';
    }

    return \%answer;

}

=head1 PRIVATE METHODS

You may use them, but if you break it, you buy it

=head2 _assert_endpoint

Asserts if the endpoint is defined. If there is a transport_hook defined in the options we will grant you calling priviledges. Dies (and logs) on failure.

=head3 SYNOPSIS

    my $ok = $self->_assert_endpoint(
        {
            endpoint       => undef,
            transport_hook => sub {
                return HTTP::Response->new(
                    200,
                    'Mocking the answers',
                    ['Content-Type' => 'text/xml'],
                    $zs->slurp(RESPONSE_XML_PATH, lc($service), $name),
                );
                }
        }
    );

    my $ok = $self->_assert_endpoint(
        {
            endpoint       => 'https://foo.bar/Baz',
        }
    );

=cut

sub _assert_endpoint {
    my ($self, $opts) = @_;

    if (defined $opts->{transport_hook}) {
        $self->log->warn("Not checking if we have an endpoint, transport hook in place");
        return 1;
    }
    elsif (!defined $opts->{endpoint}) {
        $self->log->error("No endpoint defined in the call");
        throw("Xential/SOAP/endpoint/undefined", "Undefined endpoint in the call, not submitting anything");
    }
    $self->log->trace("Endpoint is defined: $opts->{endpoint}");
    return 1;
}

sub _do_call {
    my ($self, $call, $params, $opts) = @_;

    $self->_assert_endpoint($opts);

    my ($answer, $trace) = $self->xential->call($call, $params, $opts);

    if (!$answer || $trace->error) {
        my $msg = sprintf(
            "Error while calling '%s' with params '%s' on endpoint '%s'%s:%s",
            $call, dump_terse($params, 5), $opts->{endpoint},
            $opts->{transport_hook} ? " [Transport hook in place]" : "",
            $trace->error
        );
        $self->log->error($msg);
        throw("Xential/SOAP/trace/error", $msg);
    }

    $self->log->trace('Xential answered: ' . dump_terse($answer, 4));

    if (my $f = $answer->{Fault}) {
        my $errname = $f->{_NAME};
        my $error   = $answer->{$errname};

        my $details = $error->{detail};
        if (!$details) {
            throw("xential/system_error", $error->{reason});
        }
        else {
            throw("xential/$errname", "Xential error: $errname: have a look at the XSD");
        }
    }
    return $answer->{parameters};
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
