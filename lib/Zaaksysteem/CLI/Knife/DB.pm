package Zaaksysteem::CLI::Knife::DB;

use Moose::Role;
use Zaaksysteem::CLI::Knife::Action;

use Zaaksysteem::Tools qw[try catch];

use DateTime;
use DBI;
use IO::File;
use IO::Pipe;

=head1 NAME

Zaaksysteem::CLI::Knife::DB - Database content utilities

=head1 SYNOPSIS

    /path/to/app $ ./bin/zsknife --config <path> --hostname <hostname> db ...

=head1 DESCRIPTION

This module declares command-line utilities that work on the referenced
customer's database. These utilities are mainly intended to be used during
the development and maintenance of Zaaksysteem code, B<and should not be used
in production environments>.

=head1 UTILITIES

=cut

register_knife db => (
    description => 'Database management utils'
);

=head2 LOAD

Thie knife category is the set of actions that can load data into the
database of a customer instance.

Consumes some source data which will replace the current database contents.
The current database will be backed up to C<original_name-unix_timestamp>
using a switch-in renaming procedure.

=cut

register_category load => (
    knife => 'db',
    description => 'Load database data and/or schema'
);

=head3 FILE

Load raw SQL data from a file. assumes schema included.

    /path/to/app $ ./bin/zsknife ... db load file <path_to_sql>

=cut

register_action file => (
    knife => 'db',
    category => 'load',
    description => 'Load data/schema from SQL file',
    run => sub {
        my $self = shift;
        my $file = shift;

        unless (-f $file && -r $file) {
            $self->log->error(sprintf(
                'File "%s" does not exist or cannot be read',
                $file
            ));

            return 1;
        }
        
        my ($dbname) = $self->schema->storage->dbh->{ Name } =~ m[dbname\=(\w+)];

        my $new_db = $self->_new_database($dbname);

        $self->_load_database_from_file($new_db, IO::File->new($file, 'r'));

        $self->_switch_database($dbname, $new_db);
    }
);

=head2 DUMP

This knife category is the set of actions that can dump data from the
database of the customer instance.

=cut

register_category dump => (
    knife => 'db',
    description => 'Dump database data and/or schema'
);

=head3 FILE

Dumps raw SQL data to a file. Includes schema.

    /path/to/app $ ./bin/zsknife ... db dump file <path_to_file>

This action will refuse to overwrite existing files.

=cut

register_action file => (
    knife => 'db',
    category => 'dump',
    description => 'Dump data/schema to SQL file',
    run => sub {
        my $self = shift;
        my $file = shift;

        if (-e $file) {
            $self->log->error(sprintf('File "%s" already exists', $file));

            return 1;
        }

        my ($dbname) = $self->schema->storage->dbh->{ Name } =~ m[dbname\=(\w+)];
        
        $self->_dump_database($dbname, IO::File->new($file, 'w'));
    }
);

=head1 ATTRIBUTES

=head2 management_connect_info

This attribute is used to store a modified copy of the customer instance
database connect information. By default, the C<dbname> part of the C<DSN> is
set to C<template1>, so database administered.

=cut

has management_connect_info => (
    is => 'rw',
    lazy => 1,
    isa => 'HashRef',
    builder => '_build_management_connect_info'
);

sub _build_management_connect_info {
    my $self = shift;

    my $config = $self->zs_config->get_customer_config($self->hostname);

    # Clone relevant info just in case we overwrite some sane state somewhere
    my $connect_info = { %{ $config->{ 'Model::DB' }{ connect_info } } };

    # Rewrite to management dbname, this could be configurable some day
    $connect_info->{ dsn } =~ s/dbname\=\w+/dbname=template1/;

    return $connect_info;
}

=head2 management_dbh

This attribute references a fresh L<DBI> database handle, connected to using
the L</management_connect_info> data.

=cut

has management_dbh => (
    is => 'rw',
    lazy => 1,
    builder => '_build_management_dbh',
);

sub _build_management_dbh {
    my $self = shift;

    # Tempting as it may be, calling 'clone' on the existing dbh for a new
    # connection doesn't work here. The new dbh instance will still be
    # connected to the original database for some reason.

    my $connect_info = $self->management_connect_info;

    return DBI->connect(
        $connect_info->{ dsn },
        $connect_info->{ username },
        $connect_info->{ password }
    );
}

=head1 METHODS

=head2 _load_database_from_file

Loads raw SQL into the database. Assumes the supplied database name points to
an empty database.

=cut

sub _load_database_from_file {
    my $self = shift;
    my $dbname = shift;
    my $file = shift;

    # psql sub-process is nasty, but works. Bare perl was used in the initial
    # implementation of this procedure, but proved significantly slower
    # (probably poor buffering on my part).
    my $psql_pipe = IO::Pipe->new;

    $psql_pipe->writer('psql', '--quiet', $dbname);

    while (my $line = <$file>) {
        $psql_pipe->print($line);
    }
}

=head2 _dump_database

Dumps raw SQL to a file.

=cut

sub _dump_database {
    my $self = shift;
    my $dbname = shift;
    my $file = shift;

    my $sql_pipe = IO::Pipe->new;

    # pg_dump is, for now, the only reasonable utility that takes care of
    # all aspects of a proper dump.
    $sql_pipe->reader('pg_dump', $dbname);

    $self->log->info(sprintf('Dumping database "%s" as raw SQL to file', $dbname));

    while (my $line = <$sql_pipe>) {
        $file->print($line);
    }

    $file->close;
    $sql_pipe->close;
}

=head2 _new_database

Creates a new temporary database based on an existing database's name.

Returns the new database's name.

=cut

sub _new_database {
    my $self = shift;
    my $dbname = shift;

    # Kill the wrapper transaction we're in, postgresql doesn't like creating
    # databases from within a transaction
    $self->schema->txn_rollback;

    # First create the new database
    my $new_name = sprintf('_%s_%s', $dbname, DateTime->now->epoch);

    my $create = sprintf('CREATE DATABASE "%s"', $new_name);

    $self->log->info(sprintf('Creating new database "%s"', $new_name));

    $self->schema->storage->dbh->do($create);

    return $new_name;
}

=head2 _switch_database

Provided with an old and a new database name, this method will attempt to
do an in-place switch for two databases. When provided with (old) database A
and (new) database B it will rename A to A_<unix timestamp>, and B to A.

=cut

sub _switch_database {
    my $self = shift;
    my $dbname = shift;
    my $new_name = shift;

    # Cleanup existing connection, we can't ALTER unless no one is connected
    $self->schema->storage->disconnect;
    
    my $dbh = $self->management_dbh;

    my $bak_name = sprintf('%s_%s', $dbname, DateTime->now->epoch);
    
    # Raw SQL because meta db statements cannot be prepared in the usual ways
    my $rename_fmt = 'ALTER DATABASE "%s" RENAME TO "%s"';
    
    $self->log->info(sprintf(
        'Renaming existing database "%s" to backup db "%s"',
        $dbname,
        $bak_name
    ));

    $dbh->do(sprintf($rename_fmt, $dbname, $bak_name));

    $self->log->info('Switching new database in place');

    $dbh->do(sprintf($rename_fmt, $new_name, $dbname));
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
