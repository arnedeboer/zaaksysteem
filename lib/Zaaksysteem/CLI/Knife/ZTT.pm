package Zaaksysteem::CLI::Knife::ZTT;

use Moose::Role;

# import knife action keywords (register_knife, register_action, etc)
use Zaaksysteem::CLI::Knife::Action;
use Zaaksysteem::Tools;
use Zaaksysteem::ZTT;
use Zaaksysteem::ZTT::MagicDirective;

=head1 NAME

Zaaksysteem::CLI::Knife::ZTT - ZTT CLI utilities

=head1 SYNOPSIS

Process a simple template:

    /path/to/app $ ./bin/zsknife ztt eval template tpl="1 + 1 = [[ 1 + 1 ]]"

Process a template using a case context:

    /path/to/app $ ./bin/zsknife ztt eval template tpl="[[ zaaknummer ]]" case=<ID>

Process a template using an object context:

    /path/to/app $ ./bin/zsknife ztt eval template tpl="[[ my_attr ]]" object=<UUID>

=cut

register_knife ztt => (
    description => 'ZTT utilty fuctions'
);

register_category eval => (
    knife => 'ztt',
    description => 'Run one-off ZTT instances'
);

register_action template => (
    knife => 'ztt',
    category => 'eval',
    description => 'Evaluate a plaintext ZTT template',
    run => sub {
        my $self = shift;
        my $params = $self->get_knife_params;

        unless (defined $params) {
            throw('cli/knife/ztt/parameters_required', 'This action requires parameters');
        }

        unless (defined $params->{ tpl }) {
            throw('cli/knife/ztt/tpl_required', 'Missing tpl parameters');
        }

        my $ztt = Zaaksysteem::ZTT->new;

        if ($params->{ case }) {
            my $case = $self->schema->resultset('Zaak')->find($params->{ case });

            unless (defined $case) {
                throw('cli/knife/ztt/case_not_found', sprintf(
                    'Could not find case by ID "%s"',
                    $params->{ case }
                ));
            }

            $self->log->info(sprintf('Added case \'%s\' to ZTT context', $case->onderwerp));
            $ztt->add_context($case);
        }

        if ($params->{ object }) {
            my $object = $self->objectmodel->retrieve(uuid => $params->{ object });

            unless (defined $object) {
                throw('cli/knife/ztt/object_not_found', sprintf(
                    'Could not find object by UUID "%s"',
                    $params->{ object }
                ));
            }

            $self->log->info(sprintf('Added object \'%s\' to ZTT context', $object));
            $ztt->add_context($object);
        }

        my $tpl = $ztt->process_template($params->{ tpl });

        $self->log->info(sprintf('Result: \'%s\'', $tpl->string));
    }
);

register_category deparse => (
    knife => 'ztt',
    description => 'Parse input and dump AST'
);

register_action magic_directive => (
    knife => 'ztt',
    category => 'deparse',
    description => 'Parse magic directive and dump the AST',
    run => sub {
        my $self = shift;
        my $params = $self->get_knife_params;

        unless (defined $params) {
            throw('cli/knife/ztt/parameters_required', 'This action requires parameters');
        }

        unless (defined $params->{ script }) {
            throw('cli/knife/ztt/script_required', 'Missing script parameters');
        }

        my $parser = Zaaksysteem::ZTT::MagicDirective->new;
        my $ztt = Zaaksysteem::ZTT->new;

        my $expr = $parser->parse($params->{ script });

        unless (defined $expr) {
            throw('cli/knife/ztt/parser_failure', 'Parser returned undefined');
        }

        $self->log->info(sprintf('Deparse: \'%s\'', $ztt->eval_expression({
            call => 'deparse', args => [ $expr->{ expression } ]
        })));
    }
);

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
