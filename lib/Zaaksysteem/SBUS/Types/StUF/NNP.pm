package Zaaksysteem::SBUS::Types::StUF::NNP;

use Data::Dumper;
use Moose;

use Zaaksysteem::SBUS::Constants;

extends 'Zaaksysteem::SBUS::Types::StUF::GenericAdapter';

use constant ADRES_MAPPING => {
    'huisnummer'            => 'vestiging_huisnummer',
    'huisnummertoevoeging'  => 'vestiging_huisnummertoevoeging',
    'postcode'              => 'vestiging_postcode',
    'straatnaam'            => 'vestiging_straatnaam',
    'woonplaats'            => 'vestiging_woonplaats',
};

sub _stuf_to_params {
    my ($self, $nnp_xml, $stuf_options) = @_;

    my $nnp_mapping     = STUF_NNP_MAPPING;

    my $create_params   = $self->_convert_stuf_to_hash(
        $nnp_xml,
        {
            %{ $nnp_mapping },
        }
    );

    return $create_params;
}

sub _stuf_relaties {
    my ($self, $nnp_xml, $stuf_options, $create_options) = @_;

    if ($nnp_xml->{extraElementen}) {
        my $extra = $self->_convert_stuf_extra_elementen_to_hash(
            $nnp_xml->{extraElementen}
        );

        if (length($extra->{handelsRegisterVolgnummer})) {
            $create_options->{subdossiernummer} =
                sprintf("%04d", $extra->{handelsRegisterVolgnummer});
        }
    }

    my $address = $self->handle_adr_relations($nnp_xml, 'NNP');

    $create_options->{ $_ } = $address->{ $_ }
        for keys %{ $address };

    my $address_map = ADRES_MAPPING;
    for my $key (keys %{ $address_map }) {
        $create_options->{
            $address_map->{ $key }
        } = $create_options->{ $key };

        delete($create_options->{$key});
    }

    return $create_options;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
