package Zaaksysteem::SBUS::Objecten;

use Moose;
use Data::Dumper;

has [qw/schema config log die_on_error fake_transport/]   => (
    'is'    => 'rw',
);

### options contains
### {
###     traffic_object
### }
sub handle_response {
    my ($self, $params, $options) = @_;

    $self->_verify_capability;

    my $operation   = $params->{operation};
    my $stufxml     = $params->{input};

    my ($adapter, $return_value);
    eval {
        ### Do stuff
        my $prepared_params;

        ### Check if we need to use an adapter to get the required arguments
        ### from the parameters
        if ($params->{sbus_type}) {
            my $adapter_package = 'Zaaksysteem::SBUS::Types::'
                . $params->{sbus_type} . '::'
                . $params->{object};

            $adapter            = $adapter_package->new(
                config  => $self->config,
                schema  => $self->schema,
                log     => $self->log,
                fake_transport => $self->fake_transport,
            );

            $prepared_params    = $adapter->prepare_response_parameters(
                $params,
                $options
            );

            die(
                'Servicebus requested object: ' . $params->{object}
                . ', but the sbus_type did not return any parameters: '
                . $params->{sbus_type}
            ) unless $prepared_params;
        } else {
            $prepared_params    = $params->{input};
        }

        ### options to options
        $options->{mutatie_type}    = $prepared_params->{mutatie_type};
        delete($prepared_params->{mutatie_type});

        my $result                  = $self->_commit_to_database(
            $prepared_params,
            $options
        );

        $self->_flush_logobject(
            $result, $options->{traffic_object}
        ) if $result;

        if ($adapter) {
            $return_value           = $adapter->generate_response_return(
                $params,
                $prepared_params,
                $options,
                $result
            );
        }
    };

    if ($@) {
        $options->{traffic_object}->error(1);
        $options->{traffic_object}->error_message(
            'Error handling SBUS message: ' . $@
        );

        $self->log->error('Error handling stuf XML: ' . $@);

        if ($adapter) {
            $return_value = $adapter->generate_error(
                $options,
                $params,
                {
                    errormessage    => $@
                }
            );
        } else {
            die('Error handling SBUS message: ' . $@);
        }

        die($options->{traffic_object}->error_message)
            if $self->die_on_error;

    }

    return $return_value;
}

sub handle_request {
    my ($self, $params, $options) = @_;

    $self->_verify_capability;

    my $operation   = $params->{operation};
    my $stufxml     = $params->{input};

    my ($return_value);
    eval {
        ### Do stuff
        my $prepared_params;

        ### Check if we need to use an adapter to get the required arguments
        ### from the parameters
        my ($adapter);
        if ($params->{sbus_type}) {
            my $adapter_package = 'Zaaksysteem::SBUS::Types::'
                . $params->{sbus_type} . '::'
                . $params->{object};

            $adapter            = $adapter_package->new(
                config  => $self->config,
                schema  => $self->schema,
                log     => $self->log,
                fake_transport => $self->fake_transport,
                die_on_error => $self->die_on_error,
            );

            $prepared_params    = $adapter->prepare_request_parameters(
                $params,
                $options
            );

            die(
                'Servicebus requested object: ' . $params->{object}
                . ', but the sbus_type did not return any parameters: '
                . $params->{sbus_type}
            ) unless $prepared_params;
        } else {
            $prepared_params    = $params->{input};
        }

        ### options to options
        $options->{mutatie_type}    = $prepared_params->{mutatie_type};
        delete($prepared_params->{mutatie_type});

        if ($options->{dispatch_type} && $adapter) {
            my $dispatcher  = '_dispatch_' . $options->{dispatch_type};

            $self->log->info(
                'Try to dispatch to: ' . $dispatcher . ': ' .
                $adapter->can($dispatcher)
            );

            if (my $coderef = $adapter->can($dispatcher)) {
                $return_value = $coderef->(
                    $adapter,
                    $options->{dispatch_method},
                    $prepared_params,
                    $options
                );
            }
        }
    };

    if ($@) {
        $options->{traffic_object}->error(1);
        $options->{traffic_object}->error_message(
            'Error handling stuf XML: ' . $@
        );
        $self->log->error('Error handling stuf XML: ' . $@);
        die($options->{traffic_object}->error_message)
            if $self->die_on_error;
    }

    return $return_value;
}

sub _flush_logobject {
    my ($self, $logobject, $to) = @_;

    ### Got log object, flush to DB after given traffic object
    if ($logobject) {
        $logobject->traffic_object($to);
        $logobject->flush($self->schema);
    }
}


sub _verify_capability {
    my ($self, $params) = @_;

    return 1 unless $params->{sbus_type};

    unless ($self->_capability->{ $params->{sbus_type} }) {
        die(
            'Servicebus requested object: ' . $params->{object}
            . ', but the sbus_type given not exist: ' . $params->{sbus_type}
        );
    }

    return 1;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 handle_request

TODO: Fix the POD

=cut

=head2 handle_response

TODO: Fix the POD

=cut

