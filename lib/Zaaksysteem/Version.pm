package Zaaksysteem::Version;
use strict;
use warnings;

require Exporter;
our @ISA    = qw(Exporter);
our @EXPORT = qw($VERSION);

# Changed the version to adhere Perl best practises, using rc notation could break stuff
# This means that for release "v1.23.4rc1" $VERSION should be "v1.23.4.1"
our $VERSION = 'v3.25.43';

1;

__END__

=head1 NAME

Zaaksysteem::Version - A package for version bumping

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
