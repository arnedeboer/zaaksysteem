package Zaaksysteem::Search::Term::Column;

use Moose;

extends 'Zaaksysteem::Search::Term';

use Moose::Util qw(ensure_all_roles);

=head1 NAME

Zaaksysteem::Search::Term::Column - "Column" term of a conditional

=head1 ATTRIBUTES

=head2 value

The columnname of this term.

=head1 METHODS

=head2 evaluate

Evaluate the term. Returns a list containing only the value as a string

=cut

has value => (
    is  => 'ro',
    isa => 'Str',
);

override 'evaluate' => sub {
    my $self = shift;
    my ($resultset, $conditional) = @_;

    my $rtype = '';
    if($conditional->rterm->can('guess_type')) {
        $rtype = $conditional->rterm->guess_type;
    }

    # This is a bit of a hack to make hstore queries faster.
    #
    # SELECT hstore_field @> "key = value"; is *way* faster than
    # SELECT hstore_field->'key' = 'value';
    if (   $conditional->operator eq '='
        && $rtype ne 'DATE' ) {
        ensure_all_roles($self, 'Zaaksysteem::Search::Term::HStoreColumn');

        $self->column($self->value);
        $conditional->operator("@>");

        return ($resultset->hstore_column);
    }
    elsif ($conditional->operator eq 'contains') {
        # PostgreSQL syntax:
        # ... WHERE string_to_array($column, '\x1E') @> '{"val1", "val2"}'

        # Make sure the rterm is passed to the database as an arrayref
        ensure_all_roles($conditional->rterm, 'Zaaksysteem::Search::Term::ArrayRole');

        $conditional->operator("@>");

        return sprintf(
            "string_to_array(%s, '\x1E')",
            $resultset->map_hstore_key($self->value),
        );
    }

    my $column = $resultset->map_hstore_key($self->value);

    if($conditional->rterm->isa('Zaaksysteem::Search::Term::Literal')) {
        if ($rtype eq 'DATE') {
            $column = "CAST(hstore_to_timestamp($column) AS $rtype)";
        }
        elsif ($rtype ne 'TEXT') {
            $column = "CAST($column AS $rtype)";
        }
    }

    return ($column);
};

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

