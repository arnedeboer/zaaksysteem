package Zaaksysteem::Search::Acl;
use Moose::Role;

use Time::HiRes qw/tv_interval gettimeofday/;
use List::MoreUtils qw[uniq];
use Zaaksysteem::Tools qw(dump_terse);

with 'MooseX::Log::Log4perl';

=head1 NAME

Zaaksysteem::Search::Acl - ACL Functions for Object::Data::ResultSet

=head1 SYNOPSIS

=head1 DESCRIPTION

This Role defines a route from resultset to component, containing the ACL capabilities of an object
without doing an additional query every time we hit a row.

Route:

    ResultSet->search: no changes
      ->_construct_row:
        * Trigger $resultset->_acl_table to get all ACL entries for this user
        * Collect the capabilities belonging to this $row from $resultset->_acl_table
          and insert this in Component->acl_capabilities

=head1 ATTRIBUTES

=head2 _acl_table

The table, hashref, containing all the acl entries for the currently logged in user.

Example:

=cut

has '_acl_table' => (
    is  => 'rw',
    lazy => 1,
    builder => '_load_acl_table'
);

=head2 _load_acl_table

    $self->_load_acl_table

Queries the ObjectAclEntry table, and collects every uuid for the currently loggedin user

=cut

sub _load_acl_table {
    my $self            = shift;

    my $acl_scope_where = $self->_get_acl_query;

    my @tests = map {
        { scope => $_, -or => $acl_scope_where->{ $_ } }
    } keys %{ $acl_scope_where };

    my $acl_table_query = $self->result_source->schema->resultset('ObjectAclEntry')->search_rs(
        { -or           => \@tests },
        {
            select      => [qw/groupname object_uuid scope/, { array_agg => 'distinct capability'}],
            as          => [qw/groupname object_uuid scope capabilities/],
            group_by    => [qw/groupname object_uuid scope/],
        }
    );

    my $acls_for_query = {};

    while (my $acls = $acl_table_query->next) {
        my ($scope, $uuid, $capabilities, $groupname) = map {
            $acls->get_column($_)
        } qw[scope object_uuid capabilities groupname];

        $acls_for_query->{ $scope }{ $uuid }{ $groupname || 'undefined' } = $capabilities;
    }

    return $acls_for_query;
}

=head2 _load_acl_capabilitis_on_row

Loads the acl capabilities from _acl_table on the given row

=cut

sub _load_acl_capabilities_on_row {
    my $self    = shift;
    my $row     = shift;

    if ($self->result_class eq 'DBIx::Class::ResultClass::HashRefInflator') {
        return;
    }

    my $table = $self->_acl_table;
    my $class_uuid = $row->get_column('class_uuid');

    my @capabilities;

    # The loops below could be generalised one more step, combining type and instance
    # deref strategies, but the map/grep/map code becomes unreadable in the process.

    # The inclusion of the 'undefined' group in every loop is by design, as it
    # provides for the meta-group of ACLs that are *always* in effect,
    # ignoring the acl_groupname column of rows. This is used for conceptual
    # 'shared set permissions' that should change for every group.

    if ($class_uuid && $table->{ type }{ $class_uuid }) {
        # Unwrap all matching capabilities in type scope
        push @capabilities, map  { @{ $_ } } # Unwrap aggregate arrayref of capabilities
                            grep { defined } # Ignore undefined groups
                            map  { $table->{ type }{ $class_uuid }{ $_ // '' } } # Deref possible groups
                                 ( 'undefined', $row->acl_groupname );
    }

    my $uuid = $row->get_column('uuid');

    if ($uuid && $table->{ type }{ $uuid }) {
        # Unwrap all matching capabilities in instance scope
        push @capabilities, map  { @{ $_ } }
                            grep { defined }
                            map  { $table->{ instance }{ $uuid }{ $_ // '' } }
                                 ( 'undefined', $row->acl_groupname );
    }

    $row->acl_capabilities([ uniq @capabilities ]);
}

=head2 acl_items

This method returns a HASH reference that L<SQL::Abstract> will interpret as
an 'or' disjunction. It builds this OR query by looking at the current user,
his/her group (=ou) and roles. Each returned item in the OR is a normal
L<SQL::Abstract> condition.

=cut

sub _get_groupnames {
    my $self = shift;

    ### Retrieve all possible groupnames from the object_acl table
    ###
    ### Althought the RECURSIVE query below is nothing more than:
    ### SELECT me.groupname FROM object_acl_entry me GROUP BY me.groupname;
    ### It will act a lot faster: 1ms vs 330ms on a table with 74623 rows

    ### Watch it: this query below does NOT come through the DBIC_TRACE logger
    my @groupnames = $self->result_source->schema->storage->dbh_do(sub{
        my ($storage, $dbh, @args) = @_;

        my $sth = $dbh->prepare(q{
            WITH RECURSIVE t(n) AS (
                SELECT MIN(groupname) FROM object_acl_entry
              UNION
                SELECT (SELECT groupname FROM object_acl_entry WHERE groupname > n ORDER BY groupname LIMIT 1)
                FROM t WHERE n IS NOT NULL
            )
            SELECT n FROM t;
        });

        my @data;

        $sth->execute();

        while( my $row = $sth->fetchrow_hashref){
            # Skip undef, we supply that as a default, only makes the query
            # bigger
            push @data, $row->{ n } if defined $row->{n};
        }

        $self->log->trace("Found the following groups: " . dump_terse(\@data));
        return @data;
    });
    return \@groupnames;
}

sub acl_items {
    my ($self, $alias, %conditions) = @_;

    my $acl_scope_where = $self->_get_acl_query;

    my $groupnames = $self->_get_groupnames();

    # Map acl scope names to object_data columns
    my %col_map = (
        instance => 'uuid',
        type     => 'class_uuid',
    );

    my @acl_subqueries;

    for my $groupname (@$groupnames, undef) {
        for my $scope (keys %{ $acl_scope_where }) {
            my $acl_query = $self->result_source->schema->resultset('ObjectAclEntry')->search_rs(
                {
                    scope       => $scope,
                    groupname   => $groupname,
                    capability  => [qw[read search]],
                    -or         => $acl_scope_where->{ $scope }
                },
                {
                    alias       => sprintf('perm_%s_%s', $groupname || 'undef', $scope)
                }
            );

            # This is the meat of our production, this builds a WHERE clause
            # where either uuid or class_uuid is checked to be IN the
            # object_uuid# column of a scoped and grouped ACL subquery result.
            my $scoped_grouped_acl_subquery = {
                $col_map{ $scope } => { -in => $acl_query->get_column('object_uuid')->as_query }
            };

            # Only set a conditional for the acl_groupname column if the
            # groupname is really defined, IS NULL checking would also be
            # wrong here because undefined groupnames in the ACL table apply
            # to all groupnames on the object_data table.
            if (defined $groupname) {
                $scoped_grouped_acl_subquery->{ acl_groupname } = $groupname;
            }

            push @acl_subqueries, $scoped_grouped_acl_subquery;
        }
    }

    return @acl_subqueries;
}

=head2 _get_acl_query

Gets the ACL query for the user

=cut

sub _get_acl_query {
    my $self = shift;

    my $user = $self->{attrs}{zaaksysteem}{user};

    if (!$user) {
        $self->log->trace("Implied admin on _get_acl_query");
        return {},
    }

    my @entities;

    my @roles = map { $_->id } @{ $user->roles };
    my @groups = map { $_->id } @{ $user->groups };

    if (!@roles) {
        $self->log->error("User with ID %d has no roles", $user->id);
    }
    if (!@groups) {
        $self->log->error("User with ID %d has no groups", $user->id);
    }

    # All possible positions for the current user, which is the cartesian product of
    # groups over roles, a rule may exist for any item in that product.
    for my $group (@groups) {
        for my $role (@roles) {
            push @entities, sprintf('%s|%s', $group, $role);
        }
    }

    $self->log->trace(sprintf("User with ID %d has is in the following entities: %s", $user->id, dump_terse(\@entities)));

    ### Define the two different queries. One for "instance" and one for "type"
    return {
        instance => [
            { entity_type => 'tag',      entity_id => 'all_users' },

            # This unit checks for object-level rules based on the current user's name
            { entity_type => 'user',     entity_id => $user->username },

            # This unit checks for object and object_class level rules based on the current user's group/role combinations
            { entity_type => 'position', entity_id => { -in => \@entities } },

            # This unit checks for object and object_class level rules based on the current user's roles
            { entity_type => 'role',     entity_id => { -in => \@roles } },

            # This unit checks for object and object_class level rules based on the current user's group lineage
            { entity_type => 'group',    entity_id => { -in => \@groups } }
        ],

        type => [
            # Match globally readable objects
            { entity_type => 'tag',      entity_id => 'all_users' },

            # This unit checks for object and object_class level rules based on the current user's group/role combinations
            { entity_type => 'position', entity_id => { -in => \@entities } },

            # This unit checks for object and object_class level rules based on the current user's roles
            { entity_type => 'role',     entity_id => { -in => \@roles } },

            # This unit checks for object and object_class level rules based on the current user's group lineage
            { entity_type => 'group',    entity_id => { -in => \@groups } }
        ]
    };
}


1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
