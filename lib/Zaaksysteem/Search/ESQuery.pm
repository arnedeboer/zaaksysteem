package Zaaksysteem::Search::ESQuery;

use Moose;

use Zaaksysteem::Tools;

use List::Util qw[reduce];
use Moose::Util qw[ensure_all_roles];

use Zaaksysteem::Search::Term;
use Zaaksysteem::Search::Term::Column;
use Zaaksysteem::Search::Term::Function;
use Zaaksysteem::Search::Term::Literal;
use Zaaksysteem::Search::Term::Set;
use Zaaksysteem::Search::Conditional;
use Zaaksysteem::Search::Conditional::Keyword;

=head1 NAME

Zaaksysteem::Search::ESQuery - Parse ElasticSearch 2.3 Query DSL queries

=head1 DESCRIPTION

This class can be used to parse and use ElasticSearch-style queries in
Zaaksysteem. On the highest level, it receives a hashref with decoded query
data and can apply the resulting conditions to a
L<Zaaksysteem::Backend::Object::Data::ResultSet>.

    # Simple keyword search, matches on all records which contain the
    # keywords 'my keywords'.
    my $query = { query => { match => { _all => 'my keywords' } } };

    my $parser = Zaaksysteem::Search::ESQuery->new(
        query => $query
        object_type => 'case'
    );

    my $resultset = $parser->apply_to_resultset(get_object_data_rs());

    for my $case ($resultset->all) {
        ...
    }

=head1 ATTRIBUTES

=head2 query

Holds a reference to a ElasticSearch query, decoded from it's JSON form into
a plain perl hashref.

Defaults to an empty hashref, which implies a 'match everything' query.

=cut

has query => (
    is => 'ro',
    isa => 'HashRef',
    default => sub { return {} }
);

=head2 object_type

Holds the object type for which the query should execute.

Defaults to C<object> (which should match anything in production).

=cut

has object_type => (
    is => 'ro',
    isa => 'Str',
    default => sub { 'object' }
);

=head1 METHODS

=head2 apply_to_resultset

This method applies the provided L</query> to a given instance of
L<Zaaksysteem::Backend::Object::Data::ResultSet>, resulting in a new
resultset.

    my $rs = $esq->apply_to_resultset(get_rs());

    for my $entry ($rs->all) {
        ...
    }

=cut

sig apply_to_resultset => 'Zaaksysteem::Backend::Object::Data::ResultSet';

sub apply_to_resultset {
    my $self = shift;
    my $rs = shift;

    ensure_all_roles($rs, 'Zaaksysteem::Backend::Object::Roles::ObjectResultSet');

    my $cond = $self->parse($self->query);

    $rs = $rs->search({ object_class => $self->object_type });

    return $rs->search_rs(
        $cond->evaluate($rs)
    );
}

=head2 parse

High-level entry to the query parser. Does some basic sanity checks on the
provided L</query>, and passes it to L</unroll_expression>.

=head3 Exceptions

=over 4

=item search/es_query/unsupported/feature

This exception will be thrown if the usage of a feature the object
infrastructure does not yet support is found.

=back

=cut

sig parse => 'HashRef';

sub parse {
    my $self = shift;
    my $query = shift;

    if (exists $self->query->{ from } || exists $self->query->{ size }) {
        throw('search/es_query/unsupported_feature', sprintf(
            'Paging using the ElasticSearch API is not yet supported'
        ));
    }

    return $self->unroll_expression($query);
}

=head2 unroll_expression

Unroll a ElasticSearch query. The datastructure is walked over, and a
L<Zaaksysteem::Search::Conditional> tree is built from it. This tree can be
used to generate SQL for the actual query to the sotrage engine.

    my $conditional = $esq->unroll_expression({
        query => {
            match => { _all => 'my keywords' }
        }
    });

=cut

sub unroll_expression {
    my $self = shift;
    my $expression = shift;

    return unless defined $expression;

    unless (ref $expression eq 'HASH') {
        throw('search/es_query/invalid_expression', sprintf(
            'Expected a hash'
        ));
    }

    return if scalar(keys %{ $expression }) == 0;

    unless (exists $expression->{ query }) {
        throw('search/es_query/expected_query', sprintf(
            'No query context found in top-level, are the conditions prefixed with "query."?'
        ));
    }

    return $self->_unroll_query($expression->{ query });
}

sub _unroll_query {
    my $self = shift;
    my $query = shift;

    # Empty query implies match_all, no filters whatsoever
    if (exists $query->{ match_all } || scalar(keys %{ $query }) == 0) {
        return;
    }

    if (exists $query->{ term }) {
        return $self->_unroll_term($query->{ term });
    }

    if (exists $query->{ bool }) {
        return $self->_unroll_bool($query->{ bool });
    }

    if (exists $query->{ range }) {
        return $self->_unroll_range($query->{ range });
    }

    if (exists $query->{ match }) {
        return $self->_unroll_match($query->{ match });
    }

    throw('search/es_query/unsupported_query(s)', sprintf(
        '"%s" queries are not supported',
        join(', ', sort keys %{ $query })
    ));
}

sub _unroll_term {
    my $self = shift;
    my $term = shift;

    unless (ref $term eq 'HASH') {
        throw('search/es_query/term/invalid', sprintf(
            '"term" query invalid, not an object'
        ));
    }

    my @terms;

    for my $key (sort keys %{ $term }) {
        my $val = $term->{ $key };
        my $ref = ref $val;
        my $column = sprintf('case.%s', $key);

        if ($ref eq 'HASH') {
            push @terms, $self->_unroll_term_query($key, $val) if ref $val eq 'HASH';
        } elsif(!$ref) {
            push @terms, Zaaksysteem::Search::Conditional->new(
                operator => '=',
                lterm => Zaaksysteem::Search::Term::Column->new(value => $column),
                rterm => Zaaksysteem::Search::Term::Literal->new(value => $val)
            );
        }
    }

    unless (scalar @terms) {
        throw('search/es_query/term/invalid', sprintf(
            '"term" query invalid, conditions to match on required'
        ));
    }

    return reduce {
        Zaaksysteem::Search::Conditional->new(
            operator => 'and',
            lterm => $a,
            rterm => $b
        );
    } @terms
}

sub _unroll_bool {
    my $self = shift;
    my $bool = shift;

    my @conditions;

    unless (ref $bool eq 'HASH') {
        throw('search/es_query/bool/invalid', sprintf(
            '"bool" query invalid, specification not an object'
        ));
    }

    if ($bool->{ must }) {
        push @conditions, map {
            $self->_unroll_query({ $_ => $bool->{ must }{ $_ }})
        } sort keys %{ $bool->{ must }};
    }

    if ($bool->{ filter }) {
        push @conditions, map {
            $self->_unroll_query({ $_ => $bool->{ filter }{ $_ }})
        } sort keys %{ $bool->{ filter } };
    }

    if ($bool->{ must_not }) {
        my @sub_conds = map {
            $self->_unroll_query({ $_ => $bool->{ must_not }{ $_ }})
        } sort keys %{ $bool->{ must_not } };

        my $condition = reduce {
            Zaaksysteem::Search::Conditional->new(
                operator => 'and',
                lterm => $a,
                rterm => $b
            )
        } @sub_conds;

        $condition->invert(1);

        push @conditions, $condition;
    }

    unless (scalar @conditions) {
        throw('search/es_query/bool/invalid', sprintf(
            '"bool" query invalid, conditions to match on required'
        ));
    }

    return reduce {
        Zaaksysteem::Search::Conditional->new(
            operator => 'and',
            lterm => $a,
            rterm => $b
        )
    } @conditions;
}

sub _unroll_term_query {
    my $self = shift;
    my $column = shift;
    my $term = shift;

    unless (exists $term->{ query }) {
        throw('search/es_query/term/invalid', sprintf(
            '"term" query invalid, no "query" key defined in { "term": { ... } } form'
        ));
    }

    return Zaaksysteem::Search::Conditional->new(
        operator => '=',
        lterm => Zaaksysteem::Search::Term::Column->new(value => $column),
        rterm => Zaaksysteem::Search::Term::Literal->new(value => $term->{ query })
    );
}

sub _unroll_range {
    my $self = shift;
    my $range_expr= shift;

    unless (ref $range_expr eq 'HASH') {
        throw('search/es_query/range/invalid', sprintf(
            '"range" query invalid, not an object'
        ));
    }

    my @ranges;

    for my $key (sort keys %{ $range_expr }) {
        my $range = $range_expr->{ $key };
        my $column = sprintf('case.%s', $key);

        unless (ref $range eq 'HASH') {
            throw('search/es_query/range/invalid', sprintf(
                '"range" query invalid, column range mapping not an object'
            ));
        }

        unless (exists $range->{ gte } && exists $range->{ lte }) {
            throw('search/es_query/range/invalid', sprintf(
                '"range" query invalid, column range must use "gte" and "lte" comparators'
            ));
        }

        push @ranges, Zaaksysteem::Search::Conditional->new(
            operator => 'and',

            lterm => Zaaksysteem::Search::Conditional->new(
                operator => '>=',
                lterm => Zaaksysteem::Search::Term::Column->new(value => $column),
                rterm => Zaaksysteem::Search::Term::Literal->new(value => $range->{ gte })
            ),

            rterm => Zaaksysteem::Search::Conditional->new(
                operator => '<=',
                lterm => Zaaksysteem::Search::Term::Column->new(value => $column),
                rterm => Zaaksysteem::Search::Term::Literal->new(value => $range->{ lte })
            )
        );
    }

    unless (scalar @ranges) {
        throw('search/es_query/range/invalid', sprintf(
            '"range" query invalid, ranges to match on required'
        ));
    }

    return reduce {
        Zaaksysteem::Search::Conditional->new(
            operator => 'and',
            lterm => $a,
            rterm => $b
        )
    } @ranges;
}

sub _unroll_match {
    my $self = shift;
    my $match = shift;

    unless (ref $match eq 'HASH') {
        throw('search/es_query/match/invalid', sprintf(
            '"match" query invalid, not an object'
        ));
    }

    unless (exists $match->{ _all }) {
        throw('search/es_query/match/not_supported', sprintf(
            '"match" queries can currently only be used with the "_all" field'
        ));
    }

    my $query;
    my $fieldspec = $match->{ _all };
    my $ref = ref $fieldspec;

    if ($ref eq 'HASH' && exists $fieldspec->{ query }) {
        $query = $fieldspec->{ query };
    } elsif (!$ref) {
        $query = $fieldspec;
    } else {
        throw('search/es_query/match/invalid', sprintf(
            '"match" query expects an object or scalar matchterm'
        ));
    }

    return Zaaksysteem::Search::Conditional::Keyword->new(
        value => $query
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
