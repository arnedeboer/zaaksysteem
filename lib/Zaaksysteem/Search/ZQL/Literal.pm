package Zaaksysteem::Search::ZQL::Literal;

use Moose;

use Zaaksysteem::Search::Term::Literal;

has 'value' => ( is => 'ro', isa => 'Defined', required => 1 );

sub new_from_production {
    my $class = shift;

    return $class->new(value => shift);
}

sub dbixify {
    return Zaaksysteem::Search::Term::Literal->new(value => shift->value);
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 dbixify

TODO: Fix the POD

=cut

=head2 new_from_production

TODO: Fix the POD

=cut

