package Zaaksysteem::Search::ZQL;

use Moose;
use Moose::Util qw[ensure_all_roles];

use Parse::RecDescent;

use Zaaksysteem::Exception;
use Zaaksysteem::Profile;

=head1 NAME

Zaaksysteem::Search::ZQL - Parse ZQL queries

=cut

=head1 GRAMMAR

ZQL is an SQL inspired query language. It can be used to interact with
the Zaaksysteem instance by way of convenient and humand-readable syntax.

What's that thing they say? Lead by example? Aight.

=head2 Base case

    SELECT {} FROM case

This is the simplest SELECT query. Let's break it down

C<SELECT> defines the command of this query

C<{}> is a SELECT query probe. A probe is a thing that makes a value show up
in the resultset. This probe is special, it is the equivalent of SQLs C<*>,
which selects all columns.

C<FROM case> defines the object type being queried.

=head2 Fancier

    SELECT case.id FROM case WHERE case.id > 1

Quite similar to the previous example, but with a constrained probelisting
and a condition, the breakdown:

C<SELECT case.id> this probelisting defines the resultset to only contain
values from the C<case.id> column.

C<WHERE case.id > 1> defines a condition imposed on the resultset, with one
specific condition (the integer value of the C<case.id> column must be greater
than 1).

=head2 Datatypes

Supported datatypes in ZQL:

=over 4

=item null

Plain old undefined, 'unset' or non-existing value.

    case.name = NULL

=item integer

Plain old intergers, supported in bareword fashion, no no sign, no decimal points.

    case.id = 4

=item string

Double-quoted strings of UTF-8 chars.

    casetype.title = "Casetype"

=item boolean

Plain old, case-insensitive C<true>/C<false> values

=item set

A simple set implementation, denoted by C<( ... )>. Mostly used by C<IN>
queries. Elements can be any datatype, seperated by a C<,>.

    WHERE case.id IN ( 1, 2, 3, 4 )

=item datetime

Full test parsing of ISO-8601 dates in bareword form. The format is strict.

    WHERE case.registration_date = 2014-01-01T12:00:00Z

All dates are interpreted in Universal Coordinated Time (UTC), hence the
required Z after the time part of the datetime string.

=back

=head2 Operators

=over 4

=item equality

The equality check consists of a single C<=> between operands.

    WHERE case.id = 4

=item greater than

C<< > >>

=item lesser than

C<< < >>

=item greater than or equal

C<< >= >>

=item lesser than or equal

C<< <= >>

=item logical and

C<< AND >>

=item logical or

C<< OR >>

=item stringified like comparison

This operator allows fuzzy string comparisons to be done

    WHERE case.summary ~ "Parkeergebied"

Will perform a case-insensitive 'contains' comparison.

=item member-of test

This operator behaves like SQLs IN queries, syntactically they are akin as well

    WHERE case.id IN ( 1, 2, 3, ... )

=item multi-value property

This operator checks if ALL of the values on its right-hand-side are contained
in the attribute's value.

    WHERE attribute.something CONTAINS ( "a", "b" )

=item between ternary test

This operator is a user-friendly variant of a complexer greater-than-or-equals
X AND lesser-than-or-equals Y query.

    WHERE case.registration_date BETWEEN 2014-01-01T12:00:00Z AND 2014-02-02T12:00:00Z

Above example is functionally equivalent to

    WHERE ( case.registration_date >= 2014-01-01T12:00:00Z ) AND ( case.registration_date <= 2014-02-02T12:00:00Z )

=back

=head2 Con/dis-junctions

    SELECT case.id FROM case WHERE ( case.id > 1 ) AND ( case.id < 10 )

Current, lacking precedence parsing, the conjunction must be written as above.

=head2 n-ary conjunctions

    SELECT case.id FROM case WHERE ( case.id > 1 ) AND ( case.id < 10 ) OR ( case.id > 20 )

The above query will produce a resultset that lists case id's that must be
greater than 1, but lower than 10, or higher than 20. Ambiguous isn't it? It
gets unrolled like this

    { -and => [
        case.id => { '=' => 1 },
        -or => [
            { case.id => { '<' => 10 } },
            { case.id => { '>' => 20 } }
        ]
    ] }

Or, more simply:

    case.id > 1 AND ( case.id < 10 OR ( case.id > 20 ) )

Since there is no precedence parsing, the the order of unrolling the
con/dis-junctions is always right to left. This means that the right most
con/dis-junction will be most deeply nested.

=head2 Logical inversion

    SELECT case.id FROM case WHERE NOT ( case.id = 2 )

The above query will build a resultset containing only objects not matching
the sub-condition C<case.id = 2>.

Complexer queries could use some more syntactical sugar, but you can express
arbitrarily complex conditions with this prefix operator.

    SELECT case.id FROM case WHERE ( case.id > 1 ) AND ( NOT ( case.id > 20 ) )

This example will return cases with an id higher than one, but not higher than
20.

=head2 Resultset sorting

ZQL supports a SQL inspired C<ORDER BY> syntax where it's possible to sort the
output of the query on a column. Extending on the ISO SQL standard, it offers
the capability to choose the sorting style (numeric, or default, alphanumeric).

=head3 Example - simplest form

    SELECT {} FROM case ORDER BY case.number

The above query will sort the resultset by the HStore key C<case.number> in a
ASCENDING direction, using alphanumeric ordering. Even though C<case.number> is
an integer field, that information is lost at time of query execution, so the
default is to sort alpha-numerically.

=head3 Example - specify direction

    SELECT {} FROM case ORDER BY case.number DESC

This is the same query as above, but will sort alphanumerically with a
descending direction.

=head3 Example - specify sort comparison

    SELECT {} FROM case NUMERIC ORDER BY case.number

This query will attempt to interpret the field C<case.number> as an integer.
This will affect the resultset such that the sequential ordering of integers
is preserved. (You get 1, 2, 3, 4, ... ordering, instead of 1, 10, 11, 2, ... )

Possible sort style modifiers are C<NUMERIC> and C<ALPHANUMERIC>, although the
latter is implied when not specified.

=head2 Freetext searches

ZQL comes with a full-text index on every 'textlike' attribute field by
default (this can be overridden bij a specific object type to include or
exclude attributes, or inject completely different data if so desired).

The main syntax for this querying rows while doing a fulltext seach is

    SELECT {} FROM case MATCHING "keyword1 keyword2 ..."

The above query will return only objects of type C<case> that contain the
strings "keyword1" and "keyword2". The implication here is that both keywords
are matched to any text-like attribute, and the row only shows up in the
resultset if B<both> keywords match.

=head2 Distinct selects

The ability to do a DISTINCT SELECT on Zaaksysteem is mainly used on a few
categories, but is generically supported.

    SELECT DISTINCT case.type.title FROM case

This will produce a resultset with with values based on the case.type.title
'column'. For every unique value found in the database, 1 row is returned.

=head3 Example

    SELECT DISTINCT casetype.id, casetype.name FROM case

    {
        "next" : null,
        "status_code" : "200",
        "prev" : null,
        "num_rows" : 20,
        "rows" : 20,
        "comment" : null,
        "at" : null,
        "result" : [
            {
                "casetype.name" : "Incident",
                "count" : 2144,
                "casetype.id" : "10"
            },
            {
                "casetype.name" : "Vergadering",
                "count" : 600,
                "casetype.id" : "15"
            },
            .
            .
            .
        ]
    }

=head2 Descriptive selects

The SELECT command comes with the ability to describe the values of returned
in the resultset hydration. This can be triggered with the keyword chain
C<WITH DESCRIPTION> directly after the C<SELECT> (or possibly, after
C<SELECT DISTINCT>).

    SELECT WITH DESCRIPTION {} FROM case

Which hydrates as follows:

    {
       "next" : null,
       "status_code" : "200",
       "prev" : null,
       "num_rows" : "1",
       "rows" : 1,
       "comment" : null,
       "at" : null,
       "result" : [
          {
             "object_type" : "case",
             "id" : 2,
             "values" : {
                "case.number" : 2,
                .
                .
                .
             },
             "describe" : {
                "label" : "case",
                "class" : "case",
                "describe" : "Zaaksysteem.nl case",
                "attributes" : [
                   {
                      "attribute_type" : "text",
                      "name" : "case.number"
                   },
                   .
                   .
                   .
                ]
             }
          }
       ]
    }

=head1 ATTRIBUTES

=head2 query

String representation of the query being parsed

=cut

has query => ( is => 'ro', isa => 'Str' );

=head2 cmd

Accessor to the parsed L<Zaaksysteem::Search::ZQL::Command> object

=cut

has cmd => ( is => 'rw', isa => 'Zaaksysteem::Search::ZQL::Command' );

=head1 CONSTRUCTOR

=head2 new

This method provides main access to the ZQL parser, the usual
incantations suffice

    my $zql = Zaaksysteem::Search::ZQL->new('SELECT {} FROM ...');

=cut

around BUILDARGS => sub {
    my $orig = shift;
    my $class = shift;

    $class->$orig(query => shift);
};

sub BUILD {
    my $self = shift;

    #$::RD_HINT = 1;
    #$::RD_WARN = 1;
    #$::RD_TRACE = 1;

    my $parser = Parse::RecDescent->new(
        $self->grammar
    );

    unless($parser) {
        throw('search/zql', 'Unable to construct parser, is the grammar ok?');
    }

    my $cmd = try { $parser->command($self->query) } catch {
        throw('search/zql/parse', sprintf(
            'Unable to parse command "%s": %s',
            $self->query,
            $_
        ));
    };

    unless (defined $cmd) {
        throw('search/zql/parse', sprintf(
            'Unable to parse command "%s"',
            $self->query
        ));
    }

    $self->cmd($cmd);

    return;
}

=head1 METHODS

=head2 apply_to_resultset

=cut

sub apply_to_resultset {
    my $self = shift;
    my $rs = shift;

    ensure_all_roles($rs, 'Zaaksysteem::Backend::Object::Roles::ObjectResultSet');

    return $rs unless $self->cmd->can('apply_to_resultset');

    return $self->cmd->apply_to_resultset($rs);
}

=head2 object_type

Passthru / convenience method to the C<object_type> of the C<cmd> attribute.

=cut

sub object_type {
    return shift->cmd->object_type;
}

=head2 grammar

This sub produces a multi-line L<Parse::RecDescent> grammar implementing
the ZQL syntax on a parsing level.

See the section on ZQL grammar.

=cut

sub grammar {
    return <<'EOG';
{ use Zaaksysteem::Search::ZQL::Syntax }

query : command(s /;/)
      | <error: Expected command>

command : cmd_select
        | cmd_describe
        | cmd_analyze
        | cmd_count

cmd_analyze : /analyze/i <commit> cmd_select /\Z/
    { $return = zql_command 'Analyze', $item{ cmd_select } }

cmd_describe : /describe/i <commit> object_type /\Z/
    { $return = zql_command 'Describe', %item }

cmd_select : /select/i <commit> distinct(?) describe(?) probe(s /,/) from intake(?) matching(?) where(?) query_opt(s?) /\Z/
    { $return = zql_command 'Select', %item }

cmd_count : /count/i <commit> object_type matching(?) where(?) query_opt(s?) /\Z/
    { $return = zql_command 'Count', %item }

distinct : /distinct/i

probe : "{}" { $return = zql_literal('JPath', '{}') }
      | column { $return = $item{ column } }
      | jpath { $return = $item{ jpath } }
      | <error: Expecting {}, column, or jpath probe, got "$text">

from : /from/i <commit> object_type { $return = $item{ object_type } }

intake : /intake/i

where : /where/i <commit> expr { $return = $item{ expr } }

matching : /matching/i string { $return = $item{ string } }

query_opt : limit { $return = $item{ limit } }
          | group_by { $return = $item{ group_by } }
          | order_by { $return = $item{ order_by } }
          | <error: Expecting query option, got "$text">

describe : /with/i /description/i { $return = 1 }

limit : /limit/i number { $return = { limit => $item{ number } } }

group_by : /group/i /by/i <commit> column { $return = { group_by => $item{ column }->value } }

direction : /asc|desc/i { $return = '-' . lc($item[1]) }
          | { $return = '-asc' }

order_by : sort_style(?) /order by/i <commit> column direction
    { $return = { order_by => { $item{ direction } => $item{ column }->value }, sort_style => $item{ 'sort_style(?)' }[0] } }

sort_style : /numeric/i { $return = 'numeric' }
           | /alphanumeric/i { $return = 'alphanumeric' }
           | /natural/i { $return = 'natural' }

expr : infix_expression { $return = $item{ infix_expression } }
     | prefix_expression { $return = $item{ prefix_expression } }
     | "(" <commit> expr ")" { $return = $item{ expr } }
     | <error: Expecting expression>

prefix_expression : prefix_operator <commit> term
    { $return = zql_expr 'Prefix', %item }

infix_expression : term infix_expression_between { $return = zql_expr 'Infix::Between', %item }
                 | term infix_operator <commit> infix_expression_tail(1) { $return = zql_expr 'Infix', %item }

infix_expression_tail : infix_expression
                      | term
                      | <error: Expecting term at end of infix expression>

infix_expression_between : /between/i <commit> literal "AND" literal
    { $return = [ @item[3, 5] ] }

term : "(" expr ")" { $return = $item[2] }
     | function
     | literal
     | column

function : bareword set { $return = zql_literal('Function', $item{ bareword }, $item{ set }) }

bareword : /[\w\.]+/

object_type : bareword { $return = zql_literal('ObjectType', $item{ bareword }) }

column : bareword { $return = zql_literal('Column', $item{ bareword }) }

literal : date
        | set
        | jpath
        | null
        | boolean
        | string
        | number

null : /null/i { $return = zql_literal('Null') }

boolean : /true/i { $return = zql_literal('Boolean', 1) }
        | /false/i { $return = zql_literal('Boolean', 0) }

number : /\d+(\.\d+)?(?!\w)/ { $return = zql_literal('Number', $item[1]) }

# Non-greedily match all chars between 2 " chars, skip over escaped \"
string : /"(.*?)(?<!\\)"/
    { $return = zql_literal('String', $item[1]) }

set : "(" literal(s? /,/) ")"
    { $return = zql_literal('Set', $item[2]) }

date : /(\d{4}\-\d{2}\-\d{2}T\d{2}\:\d{2}\:\d{2}(\.\d{1,3})?Z)/
    { $return = zql_literal('DateTime', $item[1]) }

jpath : /\$.([\w\.\[\]]+)/
    { $return = zql_literal('JPath', $item[1]) }

prefix_operator : prefix_operators { $return = zql_op 'Prefix', $item[1] }

prefix_operators : "!" { $return = $item[1] }
                 | /not/i { $return = $item[1] }

infix_operator : infix_operators { $return = zql_op 'Infix', $item[1] }

infix_operator_between : /between/ literal "AND" literal
    { $return = zql_op 'Infix::Between', $item[2], $item[4] }

infix_operators : "^" { $return = $item[1] }
               | "%" { $return = $item[1] }
               | "*" { $return = $item[1] }
               | "/" { $return = $item[1] }
               | "-" { $return = $item[1] }
               | "+" { $return = $item[1] }
               | ">=" { $return = $item[1] }
               | "<=" { $return = $item[1] }
               | ">" { $return = $item[1] }
               | "<" { $return = $item[1] }
               | "=" { $return = $item[1] }
               | "~" { $return = $item[1] }
               | "." { $return = $item[1] }
               | "#" { $return = $item[1] }
               | /contains\s/i { $return = 'contains' }
               | /in\s/i { $return = 'in' }
               | /and\s/i { $return = 'and' }
               | /or\s/i { $return = 'or' }
EOG
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 BUILD

TODO: Fix the POD

=cut

