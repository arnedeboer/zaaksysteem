package Zaaksysteem::Model::Object;

use Moose;
use namespace::autoclean;

use Zaaksysteem::Tools;

use Catalyst::Model::Factory::PerRequest;
use Catalyst::Model::Factory;

extends 'Catalyst::Model::Factory::PerRequest';

=head1 NAME

Zaaksysteem::Model::Object - Catalyst glue for L<Zaaksysteem::Object::Model>
instances.

=head1 DESCRIPTION

=head1 PARAMETERS

    $c->model('Object', list => 'of', stuff => 1);

=head2 user


=cut

__PACKAGE__->config(
    class       => 'Zaaksysteem::Object::Model',
    constructor => 'new',
);

=head2 ACCEPT_CONTEXT

=cut

sub ACCEPT_CONTEXT {
    my ($self, $c, @args) = @_;

    $self->{ args } = \@args;

    # Prevent caching of instances when arguments are provided
    if (scalar @args) {
        return Catalyst::Model::Factory::ACCEPT_CONTEXT($self, $c);
    } else {
        return Catalyst::Model::Factory::PerRequest::ACCEPT_CONTEXT($self, $c);
    }
}

=head2 prepare_arguments

This method implements the interface required by
L<Catalyst::Model::Factory::PerRequest>.

=cut

sub prepare_arguments {
    my $self = shift;
    my $c = shift;

    my %init_args = @{ $self->{ args } };

    my %args = (
        table              => 'ObjectData',

        schema             => $c->model('DB')->schema,

        # Oh boy, yeah "this breaks" the model, but it's a lot cleaner than
        # having 'deep_relations' passthru patterns when inflating objects via
        # the model, as we did previous to this code.
        prefetch_relations => $c->req->param('deep_relations') ? 1 : 0
    );

    # Use parameterized user when available
    if (exists $init_args{ user }) {
        $args{ user } = $init_args{ user };

        return \%args;
    }

    return \%args if $c->check_any_user_permission('admin') ||
                     $c->session->{ pip } ||
                     grep { $_ eq 'DisableACL' } keys %{ $c->action->attributes };

    unless($c->user_exists) {
        throw('model/object/disabled', sprintf(
            'Refusing to instantiate Object model without user or DisableACL flag'
        ));
    }

    $args{ user } = $c->user;

    return \%args;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 prepare_arguments

TODO: Fix the POD

=cut

