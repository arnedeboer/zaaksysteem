package Zaaksysteem::Model::Object;

use Moose;
use namespace::autoclean;

use Zaaksysteem::Tools;

extends 'Catalyst::Model::Factory::PerRequest';

__PACKAGE__->config(
    class       => 'Zaaksysteem::Object::Model',
    constructor => 'new',
);

sub prepare_arguments {
    my ($self, $c) = @_;

    my %args = (
        table              => 'ObjectData',

        schema             => $c->model('DB')->schema,
        type_model         => $c->model('ObjectType'),  # Need to pre-load all known objectypes here

        # Oh boy, yeah "this breaks" the model, but it's a lot cleaner than
        # having 'deep_relations' passthru patterns when inflating objects via
        # the model, as we did previous to this code.
        prefetch_relations => $c->req->param('deep_relations') ? 1 : 0
    );

    return \%args if $c->check_any_user_permission('admin') ||
                     $c->session->{ pip } ||
                     grep { $_ eq 'DisableACL' } keys %{ $c->action->attributes };

    unless($c->user_exists) {
        throw('model/object/disabled', sprintf(
            'Refusing to instantiate Object model without user or DisableACL flag'
        ));
    }

    $args{ user } = $c->user;

    return \%args;
}

1;


__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 prepare_arguments

TODO: Fix the POD

=cut

