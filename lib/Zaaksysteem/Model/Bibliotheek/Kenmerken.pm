package Zaaksysteem::Model::Bibliotheek::Kenmerken;

use strict;
use warnings;
use Zaaksysteem::Constants qw/
    ZAAKSYSTEEM_CONSTANTS
    PARAMS_PROFILE_DEFAULT_MSGS
/;

use parent 'Catalyst::Model';

use Data::Dumper;
use JSON::XS ();

use constant KENMERKEN              => 'kenmerken';
use constant KENMERKEN_DB           => 'DB::BibliotheekKenmerken';
use constant KENMERKEN_OPTIONS_DB   => 'DB::BibliotheekKenmerkenValues';

use constant KENMERKEN_DB_MAP       => {


};

use Moose;

has 'c' => (
    is  => 'rw',
);


{
    Zaaksysteem->register_profile(
        method  => 'bewerken',
        profile => {
            required => [ qw/
                kenmerk_naam
                kenmerk_type
                bibliotheek_categorie_id
                commit_message
            /],
            optional => [ qw/
                id
                kenmerk_naam_public
                kenmerk_help
                kenmerk_value_default
                kenmerk_magic_string
                kenmerk_options
                kenmerk_document_categorie
                kenmerk_type_multiple
            /],
            missing_optional_valid => 1,
            dependencies        => {
                'kenmerk_type'   => sub {
                    my ($dfv, $value) = @_;

                    if (
                        $value &&
                        exists(ZAAKSYSTEEM_CONSTANTS->{veld_opties}->{
                                $value
                        }->{multiple}) &&
                        ZAAKSYSTEEM_CONSTANTS->{veld_opties}->{
                            $value
                        }->{multiple}
                    ) {
                        return [ 'kenmerk_options' ];
                    }

                    return [];
                },
            },
            constraint_methods  => {
                kenmerk_magic_string    => qr/^[\w0-9_]+$/,
                kenmerk_naam            => qr/^.{2,64}$/,
            },
            field_filters       => {
                kenmerk_type_multiple => sub {
                    my ($field) = @_;

                    return $field ? 1 : 0;
                },
            },
            msgs                => PARAMS_PROFILE_DEFAULT_MSGS,
        }
    );

    sub bewerken {
        my ($self, $params) = @_;

        my $dv = $self->c->check(
            params  => $params,
        );
        return unless $dv->success;

        ### Magic string check
        return if (
            !$params->{id} &&
            (
                !$params->{kenmerk_magic_string} ||
                $params->{kenmerk_magic_string} ne
                $self->c->model('DB::BibliotheekKenmerken')->generate_magic_string(
                    $params->{kenmerk_magic_string}
                )
            )
        );

        $self->c->log->debug('Trying to create kenmerk');

        my $valid_options = $dv->valid;

        delete $valid_options->{commit_message};
        ### Rewrite some values
        my %options = map {
            my $key = $_;
            $key =~ s/^kenmerk_//;
            $key => $valid_options->{ $_ }
        } keys %{ $valid_options };

        $options{value_type} = $options{type};
        delete($options{type});

        ### Delete options from options
        my $kenmerk_options;
        if (
            $options{options} &&
            exists(ZAAKSYSTEEM_CONSTANTS->{veld_opties}->{
                    $options{value_type}
            }->{multiple})
        ) {
            $kenmerk_options = $options{options};
        }

        delete($options{options});
        delete($options{id}) unless $options{id};
        delete($options{kenmerken});

        my $kenmerk = $self->c->model(KENMERKEN_DB)->update_or_create(\%options);
        return unless $kenmerk;

        $kenmerk->bump_version;

        my $options = JSON::XS->new->decode($dv->valid->{kenmerk_options});
        if ($options && ref $options eq 'ARRAY' && @$options) {
            $kenmerk->save_options({options => $options});
        }

        $self->_kenmerk_file($kenmerk);
        $self->_kenmerk_date($kenmerk);

        $self->_touch_casetypes($kenmerk);

        return $kenmerk;
    }
}

sub _kenmerk_file {
    my ($self, $kenmerk) = @_;

    if ($kenmerk->value_type eq 'file') {
        # Create or update a file_metadata entry if any values are given for it.
        my $orig_params = $self->c->req->params;
        my %md_create;
        my @md_keys = grep {$_ =~ /^metadata_/} keys %{ $orig_params };
        for my $md_key (@md_keys) {
            my $value = $orig_params->{$md_key};

            if ($value && $value ne '') {
                $md_key =~ s/metadata_//;
                $md_create{$md_key} = $value;
            }
        }
        if (%md_create && $kenmerk->file_metadata_id) {
            $kenmerk->file_metadata_id->update(\%md_create);
        }
        elsif (%md_create) {
            my $file_md = $self->c->model('DB::FileMetadata')->create(\%md_create);
            $kenmerk->update({file_metadata_id => $file_md->id});
        }
        return 1;
    }
    elsif (my $md = $kenmerk->file_metadata_id) {
        # If a kenmerk isn't a file, it is not logical to store metadata
        # about that file... and we delete it.
        $kenmerk->update({file_metadata_id => undef});
        $md->delete;
        return 0;
    }
}

sub _kenmerk_date {
    my ($self, $kenmerk) = @_;

    if ($kenmerk->value_type eq 'date') {
        my $params = $self->c->req->params;

        my $opts = $kenmerk->properties;

        my $qr = /^date_limit_/;
        for my $k (grep {$_ =~ /$qr/} keys %{ $params }) {
            my $v = $params->{$k};
            $k =~ s/$qr//;
            if (defined $v) {
                $opts->{date_limit}{$k} = $v;
            }
            else {
                delete $opts->{date_limit}{$k};
            }
        }

        $kenmerk->update({properties => $opts});
        return 1;
    }
    return 0;
}

sub _touch_casetypes {
    my ($self, $kenmerk) = @_;

    my $zaaktype_kenmerken = $kenmerk->zaaktype_kenmerkens->search(
        { },
        {
            prefetch => { 'zaaktype_node_id' => 'zaaktype_id' }
        },
    );

    my %casetypes_seen;
    while (my $ztk = $zaaktype_kenmerken->next) {
        my $casetype = $ztk->zaaktype_node_id->zaaktype_id;

        # Only touch each case type once, not once for every node
        next if $casetypes_seen{$casetype->id};
        $casetypes_seen{$casetype->id} = 1;

        $casetype->_sync_object($self->c->model('Object'));
    }

    return;
}

sub get {
    my ($self, %opt) = @_;
    my %rv;

    unless (defined $opt{id} && $opt{id}) {
        $self->c->log->warn('No ID supplied for kenmerk retrieval, bailing out.');

        return;
    }

    my $kenmerk         = $self->c->model(KENMERKEN_DB)->find($opt{id});

    unless ($kenmerk) {
        $self->c->log->error(sprintf 'Kenmerk not found by supplied ID (%s).', $opt{id});

        return;
    }

    my @rv_map;
    {
        my $edit_profile    = $self->c->get_profile(
            'method'=> 'bewerken',
            'caller' => __PACKAGE__
        ) or return;

        @rv_map = grep {$_ ne 'commit_message'} @{ $edit_profile->{optional} };

        if ($edit_profile->{required}) {
            push(@rv_map, @{ $edit_profile->{required} });
        }
    }


    $rv{kenmerk_options} = $kenmerk->options;

    if ($kenmerk->file_metadata_id) {
        $rv{file_metadata_id} = $kenmerk->file_metadata_id;
    }

    for my $key (@rv_map) {
        if ($key eq 'kenmerk_options') { next; }
        if ($key eq 'commit_message')  { next; }
        my $dbkey   = $key;

        $dbkey =~ s/^kenmerk_//g;
        if ($dbkey eq 'type') { $dbkey = 'value_type'; }
        $rv{$key} = $kenmerk->$dbkey;
    }

    return \%rv;
}


sub ACCEPT_CONTEXT {
    my ($self, $c) = @_;

    $self->{c} = $c;

    return $self;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 ACCEPT_CONTEXT

TODO: Fix the POD

=cut

=head2 KENMERKEN

TODO: Fix the POD

=cut

=head2 KENMERKEN_DB

TODO: Fix the POD

=cut

=head2 KENMERKEN_DB_MAP

TODO: Fix the POD

=cut

=head2 KENMERKEN_OPTIONS_DB

TODO: Fix the POD

=cut

=head2 PARAMS_PROFILE_DEFAULT_MSGS

TODO: Fix the POD

=cut

=head2 ZAAKSYSTEEM_CONSTANTS

TODO: Fix the POD

=cut

=head2 bewerken

TODO: Fix the POD

=cut

=head2 get

TODO: Fix the POD

=cut

