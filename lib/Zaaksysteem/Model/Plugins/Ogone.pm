package Zaaksysteem::Model::Plugins::Ogone;
use strict;
use warnings;
use base 'Catalyst::Model::Factory::PerRequest';

__PACKAGE__->config(
    class       => 'Zaaksysteem::Payment::Ogone',
    constructor => 'new',
);

sub prepare_arguments {
    my ($self, $c) = @_;

    my $interface = $c->model('DB::Interface')->search_active({ module => 'ogone' })->first;
    if(!$interface) {
        throw(
            "not_configured/ogone",
            "Geen Ogone-configuratie gevonden.",
        );
    }

    my $prod         = 0;
    my $start_config = $c->customer_instance->{start_config};

    if (   $interface->jpath('$.mode') eq 'production'
        && $c->config->{otap} eq 'prod'
    ) {
        $prod = 1;
    }

    return {
        'c'          => $c,
        'log'        => $c->log,
        'session'    => $c->session,
        'baseurl'    => $c->uri_for('/'),
        'prod'       => $prod,
        'pspid'      => $interface->jpath('$.ogone_id'),
        'backurl'    => $interface->jpath('$.return_url'),
        'shapass'    => $interface->jpath('$.shapass_in'),
        'shapassout' => $interface->jpath('$.shapass_out'),
        'hash_algorithm' => $interface->jpath('$.hash_algorithm'),
    };
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 prepare_arguments

TODO: Fix the POD

=cut

