package Zaaksysteem::Model::Gegevens::BAG;
use strict;
use warnings;
use base 'Catalyst::Model::Factory::PerRequest';

__PACKAGE__->config(
    class       => 'Zaaksysteem::Gegevens::BAG',
    constructor => 'new',
);

sub prepare_arguments {
    my ($self, $c) = @_;

    return {
        'log'       => $c->log,
        'prod'      => (
            $c->config->{otap} eq 'prod'
                ? 1
                : undef
        ),
        'dbic'      => $c->model('DB'),
        'config'    => $c->customer_instance
            ->{start_config}
            ->{'Plugin::Import'}
            ->{'BAG'},
    };
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 prepare_arguments

TODO: Fix the POD

=cut

