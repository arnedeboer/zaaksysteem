package Zaaksysteem::Tools;

use warnings;
use strict;

use autodie;
use v5.14;

use feature ();

use Data::Dump qw(dumpf);
use Devel::StackTrace;
use Scalar::Util qw[blessed];
use Exporter ();
use Zaaksysteem::Exception;
use Zaaksysteem::Profile;

use Moose::Util::TypeConstraints qw[find_type_constraint class_type];

=head1 NAME

Zaaksysteem::Tools - Basic utility functions and imports for Zaaksysteem code

=head1 SYNOPSIS

    package Zaaksysteem::SomePart;
    use Zaaksysteem::Tools;

    # We now have: warnings, strict, feature(:5.14), autodie
    # and everything from Zaaksysteem::Exception and Zaaksysteem::Profile

    define_profile foo => ( etc. );

    sub foo {
        throw("etc", "etc.");
    }

=cut

our @EXPORT = (
    @Zaaksysteem::Exception::EXPORT,
    @Zaaksysteem::Profile::EXPORT,

    qw(
        burp
        barf

        blessed
        sanitize_filename

        sig
    ),
);

sub import {
    warnings->import();
    strict->import();
    feature->import(':5.14');
    autodie->import(':all');

    Zaaksysteem::Exception->import();
    Zaaksysteem::Profile->import();

    goto &Exporter::import;
}

=head1 EXPORTED FUNCTIONS

=head2 sig

This procedure adds the ability to do runtime checks on the types of arguments
and returnvalues of Moose method calls.

    package My::Package;

    use Moose;

    # This signature defines a method that takes at least 2 arguments, both
    # string, a 0+ element array of strings and returns a stringish value.
    sig test => 'Num, Num, @Str => Str';

    sub test { ... }

    # This signature only enforces the first argument to be string. Staying in
    # line with perl's lenient nature, additional arguments are allowed.
    sig simple => 'Str';

    sub simple { ... }

    # Only enforce the returnvalue to be stringish.
    sig retcheck => '=> Str';

    sub retcheck { return 'abc' }

Note that due to the runtime nature of checks that enforce the return value
type may be undesired, as the method is still executed, and may have had
side-effects (db, global state, static class stuff).

=cut

sub sig ($$) {
    my $method = shift;
    my $signature = shift;
    my $package = caller;

    die 'needs moose' unless $package->can('meta');

    my ($in, $out) = split m[\s*=>\s*], $signature;

    return unless $in || $out;

    my @ins = $in ? check_chain(split(m[\s*,\s*], $in)) : ();
    my @outs = $out ? check_chain(split(m[\s*,\s*], $out)) : ();

    $package->meta->add_around_method_modifier($method, sub {
        my $orig = shift;
        my $self = shift;

        my @args = @_;

        map { $_->(\@args) } @ins;

        my @rets = $self->$orig(@_);
        my @temp = @rets;

        map { $_->(\@temp) } @outs;

        unless (defined wantarray) {
            warn sprintf(
                "Explicitly defined return value ignored by caller %s:%d",
                (caller 2)[1, 2]
            ) if scalar @outs;
        }

        return wantarray ? @rets : $rets[0];
    });
}

=head2 check_chain

This function supports L</sig>. It consumes a string with a type-signature
definition and produces a list of coderefs that validate the provided values.

=cut

sub check_chain {
    my @chain;

    for my $type (@_) {
        my ($prefix, $name) = $type =~ m[^([\@\%\?]?)(.*)];

        my $constraint = find_type_constraint($name) || class_type($name);

        unless ($prefix) {
            push @chain, sub {
                $constraint->assert_valid(shift(@{ $_[0] }))
            };
        }

        if ($prefix eq '?') {
            push @chain, sub {
                if ($constraint->check($_[0][0])) {
                    shift(@{ $_[0] });
                }
            };
        }

        if ($prefix eq '@') {
            push @chain, sub {
                map { $constraint->assert_valid($_) } @{ $_[0] };
            };

            last;
        }

        if ($prefix eq '%') {
            push @chain, sub {
                my %hash = @{ $_[0] };

                map { $constraint->assert_valid($hash{ $_ }) } keys %hash;
            };

            last;
        }
    }

    return @chain;
}

=head2 burp

Print a warning.

This is smart about its arguments: if there is only one argument, and it's a
reference, L<Data::Dump::dump> is called on it. In other cases, it's passed to
sprintf().

This function is exported by default.

=cut

sub burp {
    my $msg = _generate_message(@_);

    return _log($msg);
}

=head2 barf

Print a warning, like L<burp>, but include a stack trace.

This function is exported by default.

=cut

sub barf {
    my $msg = _generate_message(@_);
    my $stack = Devel::StackTrace->new();

    return _log($msg, $stack->as_string());
}

=head2 set_logger

Set the logger object that C<barf> and C<burp> use. This object should provide
a "debug" method.

By default, or when the logger is set to C<undef>, the built-in C<warn>
function will be used.

=cut

my $logger;

sub set_logger {
    $logger = shift;
    return;
}

=head1 INTERNAL FUNCTIONS

=head2 _log

Internal function that doest the actual logging. If called in void context,
the built-in C<warn> is used if no logger is set, and C<< $logger->debug() >>
if it is.

In other contexts (scalar, list), the value to be logged is returned instead.

=cut

sub _log {
    return join($/, @_)
        if (defined wantarray);

    (defined $logger)
        ? $logger->debug(join($/, @_))
        : warn(scalar(localtime), " ", join($/, @_), $/);
}

=head2 _generate_message

Generate the "real" error message. If one argument is passed, and it's a
reference, L<Data::Dumper::dump()> will be called on it.

In all other cases, the argument(s) will be passed to C<sprintf>.

=cut

sub _generate_message {
    my $msg;

    if (@_ == 1 && ref($_[0])) {
        $msg = dumpf($_[0], \&_dump_filter);
    }
    else {
        # sprintf()'s prototype expects a scalar as the first argument, so we
        # have to split it out manually first.
        $msg = sprintf(shift, @_);
    }

    return $msg . $/;
}

=head2 _dump_filter

Filter for L<Data::Dumper::dumpf> that collapses known huge data structures
down to manageable size.

The following classes are made a bit easier on the eyes:

=over

=item * L<DBIx::Class::Schema>

=back

=cut

sub _dump_filter {
    my ($ctx, $object_ref) = @_;

    if ($ctx->is_blessed && $object_ref->isa('DBIx::Class::Schema')) {
        my $storage = $object_ref->storage;
        return {
            dump => sprintf(
                "<%s database handle connected to %s>",
                $ctx->class,
                $storage
                    ? $storage->connect_info->[0]{dsn}
                    : 'nothing'
            ),
        };
    }

    return;
}

=head2 sanitize_filename

Replace all "scary" characters in a user-supplied filename with "_".

=cut

sub sanitize_filename {
    my $filename = shift;

    throw('sanitize_filename', "Need a filename") unless defined $filename;

    # <, >, " and & for html/xml
    # ":" for weird path effects

    $filename =~ s{[<>&":]}{_}sg;

    # Strip file path (DOS or UNIX)
    return (split m{[\\/]}, $filename)[-1];
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

