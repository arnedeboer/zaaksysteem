package Zaaksysteem::Geo::BAG;
use Moose;
use Zaaksysteem::Geo;

with qw(MooseX::Log::Log4perl);

use constant    SOURCE_TABLES   => {
    nummeraanduiding    => 'BagNummeraanduiding',
    openbareruimte      => 'BagOpenbareruimte',
    woonplaats          => 'BagWoonplaats'
    # Rest still unsupported
};

use constant    ESSENTIAL_DATA  => {
    BagNummeraanduiding => {
        parent                  => {
            table       => 'BagOpenbareruimte',
            key         => 'openbareruimte'
        },
        fields                  => {
            postcode                => 'postcode',
            huisnummer              => 'huisnummer',
            huisnummertoevoeging    => 'huisnummertoevoeging',
            huisletter              => 'huisletter',
            gps_lat_lon             => 'gps_lat_lon'
        }
    },
    BagOpenbareruimte => {
        parent                  => {
            table       => 'BagWoonplaats',
            key         => 'woonplaats'
        },
        fields                  => {
            naam                    => 'straat',
            gps_lat_lon             => 'gps_lat_lon'
        }
    },
    BagWoonplaats => {
        fields                  => {
            naam                    => 'woonplaats',
        }
    }
};

has 'apikey'    => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my $self    = shift;
        my $schema  = $self->result_source->schema;

        return $schema
                ->default_resultset_attributes
                ->{config}
                ->{google_api_key}
    }
);

=head2 $component->geocode_term

Return value: $geocode_address

Generate a term for geocoding this location with a googlemaps or other
geocoder.

    e.g:
        Nederland, Amsterdam, Donker Curtiusstraat 7
        Nederland, Amsterdam, Donker Curtiusstraat

=cut

sub geocode_term {}

sub set_wsg_coordinates {
    my $self        = shift;

    ### Only on component
    return unless UNIVERSAL::isa($self, 'DBIx::Class');

    return unless $self->can('coordinates_wsg');

    my $location = $self->get_wsg_location;

    if ($location) {
        $self->coordinates_wsg(
            join(',', @{ $location->{coordinates} })
        );
        $self->update;
    }
}

sub get_wsg_location {
    my $self        = shift;
    my $term        = shift;

    my $geocoder    = Zaaksysteem::Geo->new(
        key     => $self->apikey
    );

    my $location;

    eval {
        $geocoder->query($term || $self->geocode_term);
        $geocoder->geocode;

        if ($geocoder->success) {
            my $locationes   = $geocoder->results;
            $location        = shift( @{ $locationes });
        }
    };

    if ($@) {
        $self->log->error(
            'Error calling geocoder: ' . $@
        );
    }

    return $location;
}

=head2 $resultset->lookup_nearest_bag_object(geo_address)

Return value: $BAG_ROW

Given an arrayref of coordinates:

    $resultset->lookup_nearest_bag_object(
        'Amsterdam, Donker Curtiusstraat 7'
    );

Returns the nearest bag location

=cut

sub lookup_nearest_bag_object {
    my $self        = shift;
    my $address     = shift;

    return unless UNIVERSAL::isa($self, 'DBIx::Class::ResultSet');
    my $schema      = $self->result_source->schema;

    my $location    = $self->get_wsg_location($address);

    return unless $location;

    ### Find city
    my $city    =  $schema->resultset('BagWoonplaats')->search(
        {
            'LOWER(naam)'   => lc($location->city),
        }
    )->first or return;

    $address                         = lc($location->address);

    my ($streetname, $streetnumber)     = $address =~ /^(.*?)\s+(\d.*)$/;
    unless ($streetname) {
        $streetname = $address;
    }

    my ($numberletter)   = $streetnumber
        =~ /^\d+([a-z]+)/;
    my ($numbersuffix)   = $streetnumber
        =~ /^\d+[a-z]+[\s-]+(.*)/i;

    $streetnumber     =~ s/^(\d+).*/$1/;

    return unless $streetname;

    ### Find street
    my $street  = $city->openbareruimten->search(
        {
            'LOWER(naam)'   => lc($streetname),
        }
    )->first or return;

    unless ($streetnumber) {
        return $street;
    }

    my $filter = $street->hoofdadressen->search(
        {
            'huisnummer'    => lc($streetnumber),
        }
    );

    return $street unless $filter->count;
    my $newfilter;


    if (
        $numberletter &&
        ($newfilter = $filter->search({'LOWER(huisletter)' => lc($numberletter)})) &&
        $newfilter->count
    ) {
        $filter = $newfilter;
    }

    if (
        $numbersuffix &&
        ($newfilter = $filter->search({'LOWER(huisnummertoevoeging)' => lc($numbersuffix)})) &&
        $newfilter->count
    ) {
        $filter = $newfilter;
    }

    return $filter->first;
}

=head2 $self->get_record_by_source_identifier($source_identifier)

Return value: $DB_RECORD

 $self->get_record_by_source_identifier('nummeraanduiding-637372832372323')

=cut

sub get_record_by_source_identifier {
    my ($self, $source_identifier) = @_;

    die('Only call this method from a resultset object')
        unless UNIVERSAL::isa($self, 'DBIx::Class::ResultSet');

    my ($type, $id)     = $source_identifier =~ /^(.+)\-(.+)$/;

    die "Need valid source_identifier (got: '$source_identifier')" unless $type && $id;
    die('Type: ' . $type . ' unsupported') unless SOURCE_TABLES()->{$type};

    return $self
        ->result_source
        ->schema->resultset(
            SOURCE_TABLES()->{$type}
        )->search(
            {
                identificatie   => $id
            }
        )->first;
}

=head2 $self->get_address_data_by_source_identifier($source_identifier)

Return value: \%ADDRESS_DATA

 print Dumper($self->get_address_data_by_source_identifier('nummeraanduiding-9876543218375842'));

 $VAR1 = {
           'huisletter' => 'A',
           'huisnummer' => 23,
           'huisnummertoevoeging' => '1rec',
           'postcode' => '1051JL',
           'straat' => 'Donker Curtiusstraat',
           'woonplaats' => 'Amsterdam'
         };
=cut

sub get_address_data_by_source_identifier {
    my ($self, $source_identifier) = @_;

    die('Only call this method from a resultset object')
        unless UNIVERSAL::isa($self, 'DBIx::Class::ResultSet');

    my $record      = $self->get_record_by_source_identifier($source_identifier)
        or return;
    
    my $rv = {};
    while ($record) {
        my ($record_name) = ref($record) =~ /::([^:]+)$/;

        my $structure   = ESSENTIAL_DATA()->{ $record_name };

        for my $field (keys %{ $structure->{fields} }) {
            my $value           = $structure->{fields}->{ $field };

            # Skip (more specific) values that already exist in the return-data set.
            next if exists($rv->{$value}) && $rv->{$value};

            $rv->{ $value }     = $record->$field;
        }

        if (defined($structure->{parent}) && $structure->{parent}) {
            my $parent  = $structure->{parent}->{key};

            $record     = $record->$parent;
            next;
        }

        $record = undef;
    }

    return $rv;
}

=head2 $self->get_human_identifier_by_source_identifier($source_identifier [,\%options ])

Return value: \%ADDRESS_DATA

 print $self->get_human_identifier_by_source_identifier('nummeraanduiding-9876543218375842');

 # Returns: "Donker Curtiusstraat 23A-1rec"

Gives a human readable string of the given BAG source identifier, you can
influence the output by using one of the identifiers below

B<Options>

=over 4

=item prefix_with_city BOOLEAN

 print $self->get_human_identifier_by_source_identifier(
    'nummeraanduiding-9876543218375842'
    {
        prefix_with_city => 1,
    }
 );

 # Returns: "Amsterdam - Donker Curtiusstraat 23A-1rec"

Prefix the output with the city name

=back

=cut

sub get_human_identifier_by_source_identifier {
    my ($self, $source_identifier, $options) = @_;
    $options ||= {};

    die('Only call this method from a resultset object')
        unless UNIVERSAL::isa($self, 'DBIx::Class::ResultSet');

    my $record      = $self->get_record_by_source_identifier($source_identifier)
        or return;

    my ($bagtype)   = $source_identifier =~ /^(\w+)-\d+/;

    if (lc($bagtype) eq 'nummeraanduiding') {
        return (
            (
                defined($options->{prefix_with_city}) &&
                $options->{prefix_with_city}
            )   ? $record->openbareruimte->woonplaats->naam . ' - '
                : ''
            ) .
            $record->openbareruimte->naam . ' ' .  $record->nummeraanduiding;
    }

    if (lc($bagtype) eq 'pand') {
        return 'Bouwjaar: ' . $record->bouwjaar;
    }

    if (lc($bagtype) eq 'verblijfsobject') {
        return 'Oppervlakte: ' . $record->oppervlakte;
    }

    if (lc($bagtype) eq 'standplaats') {
        return '-';
    }

    if (lc($bagtype) eq 'ligplaats') {
        return '-';
    }

    if (lc($bagtype) eq 'openbareruimte') {
        return $record->naam .
            (
                $options->{prefix_with_city}
                ? ', ' . $record->woonplaats->naam
                : ''
            );
    }

    return '';
}

sub TO_JSON {
    my $self        = shift;

    if ($self->can('get_columns')) {
        return { $self->get_columns };
    }

    return [ $self->all ];
}


=head2 $comp->load_gps_coordinates

Return value: $BOOL_SUCCESS

    my $success = $component->load_gps_coordinates

(Re)loads the GPS coordinates from google into this nummeraanduiding object by constructing
a country, city, street and number and send it through the Google API. The retrieved GPS
coordinates will be set on the table in column C<gps_lat_lon>

=cut

sub load_gps_coordinates {
    my ($self, $params) = @_;
    $params //= {};

    if ($params->{skip_update} || $ENV{DISABLE_ZAAKSYSTEEM_GEO_CODE}) {
        $self->log->trace(sprintf("Skip loading GPS location on request for BAG %s [%d]", $self->identificatie, $self->id));
        return 0;
    }

    my $location = $self->get_wsg_location;

    unless ($location && $location->coordinates->{lat} && $location->coordinates->{lng}) {
        $self->log->trace("Did not get GPS coordinates from the Geo module");
        return 0;
    }

    my $lat_lon_as_string   = join(',', $location->coordinates->{lat}, $location->coordinates->{lng});

    if ($self->gps_lat_lon && $self->gps_lat_lon eq $lat_lon_as_string) {
        $self->log->trace("GPS coordinates in database are the same, skipping DB update");
        return 1;
    }

    $self->log->debug(
        sprintf(
            'Updating lat/lon for location: %s to %s',
            $self->geocode_term, $lat_lon_as_string
        )
    );

    $self->gps_lat_lon($lat_lon_as_string);
    $self->update;

    return 1;
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 ESSENTIAL_DATA

TODO: Fix the POD

=cut

=head2 SOURCE_TABLES

TODO: Fix the POD

=cut

=head2 TO_JSON

TODO: Fix the POD

=cut

=head2 get_wsg_location

TODO: Fix the POD

=cut

=head2 set_wsg_coordinates

TODO: Fix the POD

=cut

