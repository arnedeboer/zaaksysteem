package Zaaksysteem::Geo::Location;

use Moose;

use constant LOCATION_ATTRIBUTES => [qw/
    address
    street
    province
    city
    country
    coordinates
    zipcode
    identification
/];

my $attrs = LOCATION_ATTRIBUTES;
has [@{ $attrs }] => (
    'is'    => 'ro'
);

sub TO_JSON {
    my $self    = shift;

    my $attrs   = LOCATION_ATTRIBUTES;

    my %result;
    $result{ $_ } = $self->$_ for @{ $attrs };

    return \%result;
}



1;




__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 LOCATION_ATTRIBUTES

TODO: Fix the POD

=cut

=head2 TO_JSON

TODO: Fix the POD

=cut

