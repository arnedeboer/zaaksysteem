package Zaaksysteem::Geo::Model;

use Moose;
use namespace::autoclean;

use Zaaksysteem::Tools;

=head1 NAME

Zaaksysteem::Geo::Model - Common Zaaksysteem library for geocoding.

=head1 DESCRIPTION

=head1 SYNOPSIS

    my $geo = $c->model('Geo');

    my $result = $geo->geocode('Donker Curtiusstraat 7, Amsterdam');

    my $result = $geo->reverse_geocode()

    my $result = $geo->impute(Zaaksysteem::Object::Types::Location->new(...));

=head1 ATTRIBUTES

=head2 interfaces

Set of L<Zaaksysteem::Backend::Sysin::Interface::Component> instances that
provide geocoding services.

=cut

has interfaces => (
    is => 'rw',
    isa => 'ArrayRef[Zaaksysteem::Backend::Sysin::Interface::Component]',
    required => 1
);

=head2 module_opts

Stores per-module options

=cut

has module_opts => (
    is => 'rw',
    isa => 'HashRef',
    default => sub { return {}; }
);

=head1 METHODS

=head2 geocode

Lookup the coordinates of a given L<Zaaksysteem::Object::Types::Address>.

    my $result = $model->geocode(Zaaksysteem::Object::Types::Address->new(...));

=cut

sig geocode => 'Zaaksysteem::Object::Types::Address => Zaaksysteem::Object::Types::Location';

sub geocode {
    my $self = shift;
    my $address = shift;

    my $provider = $self->provider;

    my $opts = $self->module_opts->{ $provider->module } || {};
    my $process_args = { %{ $opts }, address => $address };

    return $self->provider->process_trigger(geocode => $process_args);
}

=head2 reverse_geocode

Reverse lookup of an L<Zaaksysteem::Object::Types::Address> given a set of
coordinates.

    my $address = $model->reverse_geocode({
        latitude => 52.324346,
        longitude => 4.955232
    });

=cut

sig reverse_geocode => 'Zaaksysteem::Object::Types::Location => Zaaksysteem::Object::Types::Address';

sub reverse_geocode {
    my $self = shift;
    my $location = shift;

    my $provider = $self->provider;

    my $opts = $self->module_opts->{ $provider->module } || {};
    my $process_args = { %{ $opts }, location => $location };

    return $self->provider->process_trigger(reverse_geocode => $process_args);
}

=head2 impute

Impute the given L<Zaaksysteem::Object::Types::Location|location> instance.
The model will attempt to use all available geocoding providers and local data
to 'fill in' missing data about the location.

=cut

sig impute => 'Zaaksysteem::Object::Types::Location';

sub impute {
    my $self = shift;
    my $location = shift;

    # Empty for now, nothing to impute

    return $location;
}

=head2 provider

From the set of L</interfaces>, select the preferred provider.

=cut

sub provider {
    my $self = shift;

    # No heuristic available yet, only one provider implemented (opencage).
    # This sub must be extended when more providers are added to select the
    # preferred one.
    my $provider = $self->interfaces->[0];

    unless (defined $provider) {
        throw('model/geo/provider_not_found', sprintf(
            'Geocoding provider requested, but none could be found.'
        ));
    }

    return $provider;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
