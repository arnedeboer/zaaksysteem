package Zaaksysteem::Config;
use Moose;
use namespace::autoclean;

=head1 NAME

Zaaksysteem::Config - Zaaksysteem configuration loader

=head1 SYNOPSIS

    my $configs = Zaaksysteem::Config->new(
        config_root => '/etc/zaaksysteem',
    );

    my @customers = $configs->get_all_customers();

    foreach (@customers) {
        my $config = $configs->get_customer_config($_);

        my $schema = $configs->get_customer_instance($_);

        $schema->resultset("Zaak")->search_rs({}, {rows => 10});
    }

=head1 ATTRIBUTES

=cut

use File::Spec::Functions;
use Config::Any;
use Zaaksysteem::Tools;
use Zaaksysteem::Cache;
use Zaaksysteem::Users;
use Zaaksysteem::Model::DB;
use List::MoreUtils qw(uniq);

with 'MooseX::Log::Log4perl';

=head2 zs_customer_d

The path to the customer_d directory, eg, /etc/zaaksysteem/customer.d. Required.

=cut

has zs_customer_d => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

=head2 zs_conf

The path to the Zaaksysteem main config, etc /etc/zaaksysteem/zaaksysteem.conf. Not required.

=cut

has zs_conf => (
    is       => 'rw',
    isa      => 'Str',
    required => 0,
);

=head2 config

The configuration in a HashRef, loaded by the zs_conf file.

=cut

has config => (
    is      => 'ro',
    isa     => 'HashRef',
    lazy    => 1,
    builder => '_load_zs_conf',
);

=head2 customers

Customers configuration which are found in the customer.d directory.

=cut

has customers => (
    is      => 'ro',
    isa     => 'HashRef',
    builder => '_load_customer_d',
    lazy    => 1,
);

=head1 PUBLIC METHODS

=head2 get_all_customers

Returns an array with all the customer domains found in the customer.d config files.

=cut

sub get_all_customers {
    my $self = shift;

    my $customers = $self->customers;
    return keys %$customers;
}

=head2 get_customer_config

Returns a customer configuration for a specific domain/customer.
Dies if none can be found/exists.

=cut

sub get_customer_config {
    my ($self, $host) = @_;

    my $customers = $self->customers;
    if (exists $customers->{$host}) {
        $customers->{$host}->{start_config} = $customers->{$host};
        return $customers->{$host};
    }
    throw(
        "customerd/no_customer_config/$host",
        "No customer found with hostname '$host'"
    );
}

=head2 get_customer_schema

Get the schema for the specific customer/domain.
Dies in case the customer cannot be found.

=head3 ARGUMENTS

=over

=item * host

The host of the customer/customer name. Required.

=item * with_defaults

If you want the schema to be loaded with defaults. This is something you want to have if you have migration scripts and such. Defaults to false

=back

=cut

sub get_customer_schema {
    my ($self, $host, $options) = @_;
    my $ref = ref $options;
    if (!$ref && $options) {
        $options = { with_defaults => 1 }
    }
    elsif(!$ref) {
        $options = {};
    }

    my $customer_config = $self->get_customer_config($host);
    if (!$options->{with_defaults}) {
        return $self->_get_schema($customer_config);
    }

    my $cache  = Zaaksysteem::Cache->new(storage => {});

    my $schema = $self->_get_schema($customer_config);
    my $logger = Log::Log4perl->get_logger(ref $schema);

    $schema->storage->debugobj(Zaaksysteem::DB::Profile->new());
    $schema->cache($cache);
    $schema->customer_config($customer_config);

    $schema->default_resultset_attributes->{betrokkene_model} = Zaaksysteem::Betrokkene->new(
        dbic     => $schema,
        stash    => {},
        config   => $self->config,
        customer => $customer_config,
    );

    $schema->default_resultset_attributes->{cache}  = $cache;
    $schema->default_resultset_attributes->{log}    = $logger;
    $schema->default_resultset_attributes->{config} = $self->config;

    return $schema;
}

=head1 PRIVATE METHODS

=head2 _load_zs_conf

Loads a Zaaksysteem config and flattens it to a Hash.

=head3 RETURNS

Returns an HashRef on succes, dies on failure.
Returns an empty HashRef if zs_conf is not set.

=cut

sub _load_zs_conf {
    my $self = shift;

    return {} if !defined $self->zs_conf;

    if (!-f $self->zs_conf) {
        throw("zaaksysteem/conf/missing",
            'No Zaaksysteem configuration file found');
    }

    return Config::Any->load_files(
        {
            files           => [$self->zs_conf],
            use_ext         => 1,
            driver_args     => {},
            flatten_to_hash => 1,
        }
    )->{ $self->zs_conf };
}

=head2 _load_customer_d

Loads all the configuration files found in the customer.d directory.

=head3 RETURNS

HashRef on succes, dies on failure or if no customers are found.

=cut

sub _load_customer_d {
    my $self = shift;

    my $customerd = $self->zs_customer_d;

    if (!-d $customerd) {
        throw("customerd/missing", 'No customer.d directory found!');
    }

    my $cfgs = Config::Any->load_files(
        {
            files           => [glob(catfile($customerd, '*.conf'))],
            use_ext         => 1,
            flatten_to_hash => 1,
            driver_args     => {
                General => {
                    -ForceArray => 1
                },
            },
        }
    );

    my %domains;
    for my $configfile (values %{$cfgs}) {
        for my $customer (keys %{$configfile}) {

            if ($configfile->{$customer}{disabled}) {
                $self->log->info("config/customer/disabled: Customer instance '$customer' is disabled, skipping.");
                next;
            }

            my $aliases = delete $configfile->{$customer}{aliases};
            my @hosts = $aliases ? split(/[\,\;]\s*/, $aliases) : ();
            @hosts = uniq(@hosts, $customer);
            foreach my $host (@hosts) {
                if (exists $domains{$host}) {
                    my $msg = "config/duplicate/config: Host $host already found!";
                    $self->log->warn($msg);
                    next;
                    throw('config/duplicate/config', "Host $host already found!");
                }
                $domains{$host} = $configfile->{$customer};
            }
        }
    }

    return \%domains if %domains;

    throw("customerd/no_customers", 'No customers configured!');
}

=head2 _get_schema

Get a ZS schema for a specific configuration.

=head3 RETURNS

An L<Zaaksysteem::Model::DB> instance

=cut

sub _get_schema {
    my ($self, $config) = @_;

    Zaaksysteem::Model::DB->config(
        schema_class => 'Zaaksysteem::Schema',
        connect_info => $config->{'Model::DB'}{connect_info},
    );

    return Zaaksysteem::Model::DB->new->schema;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
