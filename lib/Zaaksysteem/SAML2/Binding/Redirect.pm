package Zaaksysteem::SAML2::Binding::Redirect;

use Moose;
use MooseX::Types::Moose qw/ Str ArrayRef /;
use MooseX::Types::URI qw/ Uri /;

use Zaaksysteem::Profile;
use Zaaksysteem::Exception;

use MIME::Base64 qw/ encode_base64 decode_base64 /;
use IO::Compress::RawDeflate qw/ rawdeflate /;
use IO::Uncompress::RawInflate qw/ rawinflate /;
use URI;
use URI::QueryParam;
use Crypt::OpenSSL::RSA;
use Crypt::OpenSSL::X509;
use File::Slurp qw/ read_file /;

=head1 NAME

Zaaksysteem::SAML2::Binding::Redirect

=head1 SYNOPSIS

  my $redirect = Zaaksysteem::SAML2::Binding::Redirect->new(
    key => 'sign-nopw-cert.pem',
    url => $sso_url
  );

  my $url = $redirect->sign(
    request => 'message content'
  );

  # or

  my $redirect = Zaaksysteem::SAML2::Binding::Redirect->new(
    cert => $idp_cert,
    param => 'SAMLResponse',
  );

  my $ret = $redirect->verify($url);

=head1 CONSTRUCTORS

=head2 new

Plain old Moose constructor

(* = required)

=head3 Options

=over 4

=item key *

The signing key (for creating Redirect URLs)

=item cert *

The IdP's signing cert (for verifying Redirect URLs)

=item url *

The IdP's SSO service url for the Redirect binding

=item param

The query param name to use (C<SAMLRequest>, C<SAMLResponse>) (defaults to C<SAMLRequest>)

=item signature_algorithm

URI identifying the required signature algorithm (defaults to L<http://www.w3.org/2000/09/xmldsig#rsa-sha1>)

=back

=cut

has key => ( isa => Str, is => 'ro' );
has cert => ( isa => ArrayRef[Str], is => 'ro', default => sub { [] });
has url => ( isa => Uri, is => 'ro', required => 1, coerce => 1 );

has param => (
    isa => Str,
    is => 'ro',
    required => 1,
    default => 'SAMLRequest'
);

has signature_algorithm => (
    isa => Uri,
    is => 'ro',
    coerce => 1,
    default => 'http://www.w3.org/2000/09/xmldsig#rsa-sha1'
);

=head1 METHODS

=head2 sign

Build a redirect URI and sign accordingly.

=head3 Arguments

=over 4

=item request

Stringable message to sign

=item relaystate

Optional parameter to pass to the IdP. Must be stringable.

=back

=cut

define_profile sign => (
    required => [qw[request]],
    optional => [qw[relaystate]],
    typed => {
        request => 'Str',
        relaystate => 'HashRef'
    }
);

sub sign {
    my ($self, %params) = @_;

    my $opts = assert_profile(\%params)->valid;
    my $relaystate = $opts->{ relaystate };

    my $input = "$opts->{ request }";
    my $output = '';

    rawdeflate \$input => \$output;
    my $req = encode_base64($output, '');

    my $u = URI->new($self->url);
    $u->query_param($self->param, $req);
    $u->query_param('RelayState', $relaystate) if defined $relaystate;
    $u->query_param('SigAlg', $self->signature_algorithm);

    my $key_string = read_file($self->key);
    my $rsa_priv = Crypt::OpenSSL::RSA->new_private_key($key_string);

    my %dispatch = (
        'http://www.w3.org/2000/09/xmldsig#rsa-sha1' => sub {
            $rsa_priv->use_sha1_hash;

            return $rsa_priv->sign(shift);
        },

        'http://www.w3.org/2001/04/xmldsig-more#rsa-sha256' => sub {
            $rsa_priv->use_sha256_hash;

            return $rsa_priv->sign(shift);
        }
    );

    unless(exists $dispatch{ $self->signature_algorithm }) {
        throw('saml2/redirect/sign', 'Unsupported signature algorithm: '. $self->signature_algorithm);
    }

    $u->query_param('Signature', encode_base64($dispatch{ $self->signature_algorithm }->($u->query), ''));

    return $u;
}

=head2 verify($url)

Decode a Redirect binding URL.

Verifies the signature on the response.

=cut

sub verify {
    my ($self, $url) = @_;
    my $u = URI->new($url);

    # verify the response
    my $sigalg = $u->query_param('SigAlg');
    die "can't verify '$sigalg' signatures"
         unless $sigalg eq 'http://www.w3.org/2000/09/xmldsig#rsa-sha1';

    for my $cert (@{ $self->cert }) {
        my @retval = eval {
            $self->_verify($u->clone, Crypt::OpenSSL::X509->new_from_string($cert))
        };

        next if $@;

        return @retval;
    }

    die $@;
}

sub _verify {
    my ($self, $u, $cert) = @_;

    my $rsa_pub = Crypt::OpenSSL::RSA->new_public_key($cert->pubkey);

    my $sig = decode_base64($u->query_param_delete('Signature'));
    my $signed = $u->query;
    die "bad sig" unless $rsa_pub->verify($signed, $sig);

    # unpack the SAML request
    my $deflated = decode_base64($u->query_param($self->param));
    my $request = '';
    rawinflate \$deflated => \$request;

    # unpack the relaystate
    my $relaystate = $u->query_param('RelayState');

    return ($request, $relaystate);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
