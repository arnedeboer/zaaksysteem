package Zaaksysteem::ZAPI::Form::Builder;

use Moose;

use List::MoreUtils qw[firstidx];

use Zaaksysteem::Tools;

use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::ZAPI::Form::Field;
use Zaaksysteem::ZAPI::Form::FieldSet;

=head1 NAME

Zaaksysteem::ZAPI::Form::Builder - Construct forms from object meta-data.

=head1 DESCRIPTION

This package consumes objects with attributes that do
L<Zaaksysteem::Metarole::FormAttribute> and produces
L<Zaaksysteem::ZAPI::Form>s.

=cut

has layout => (
    traits => [qw[Array]],

    is => 'ro',
    isa => 'ArrayRef[HashRef]',
    default => sub { []; },

    handles => {
        add_fieldset => 'push',
        fieldsets => 'elements'
    }
);

=head1 METHODS

=head2 group

=cut

define_profile group => (
    required => {
        title => 'Str',
        fields => 'Str' # Actually ArrayRef[Str], but Data::FormValidator flattens arrayrefs
    },
    optional => {
        name => 'Str',
    }
);

sub group {
    my ($self, %params) = @_;

    my $opts = assert_profile(\%params)->valid;

    $self->add_fieldset($opts);

    # Return self for method chaining
    return $self;
}

=head2 formulate

Build a L<form|Zaaksysteem::ZAPI::Form> from an L<object|Zaaksysteem::Object>.

    my $form = $builder->formulate($object);

=cut

sub formulate {
    my $self = shift;
    my $object = shift;

    unless(eval { $object->can('meta') }) {
        throw('zapi/form/builder', 'Unable to build a form from anything that does not implement the "meta" method.');
    }

    my $meta = $object->meta;

    my $form = Zaaksysteem::ZAPI::Form->new(
        name => $object->can('type') ? $object->type : "$object"
    );

    my %fields;

    for my $attr ( $meta->get_all_attributes ) {
        next unless $attr->does('Zaaksysteem::Metarole::ObjectAttribute');

        my $reader = $attr->get_read_method;

        my %opts = (
            name => $attr->name,
            label => $attr->label,
            type => $attr->type,
            required => $attr->is_required ? 1 : 0 ,
            value => $object->$reader(),
        );

        my $default = $attr->default($object);

        if ($default) {
            $opts{ default } = $default;
        }

        if ($attr->documentation) {
            $opts{ description } = $attr->documentation
        }

        if ($attr->options) {
            $opts{ options } = $attr->options;
        }

        if ($attr->data) {
            $opts{ data } = $attr->data;
        }

        $fields{ $attr->name } = Zaaksysteem::ZAPI::Form::Field->new(%opts);
    }

    for my $fieldset_spec ($self->fieldsets) {
        $fieldset_spec->{ fields } = [
            map { $fields{ $_} } @{ $fieldset_spec->{ fields }}
        ];

        $form->add_fieldset(Zaaksysteem::ZAPI::Form::FieldSet->new(
            %{ $fieldset_spec }
        ));
    }

    return $form;
}


__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

