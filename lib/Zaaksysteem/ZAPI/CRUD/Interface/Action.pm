package Zaaksysteem::ZAPI::CRUD::Interface::Action;

use Moose;

use constant ACTION_TYPES       => ['submit','popup'];

use constant OBJECT_PROFILE     => {
    required            => [qw/
        id
        label
    /],
    optional            => [qw/
        type
        data
        when

    /],
    constraint_methods  => {
        id              => qr/^[\w_-]+/,
        # type            => sub {
        #     my $val     = pop;

        #     return unless $val;

        #     return 1 if grep { $_ eq $val } @{ ACTION_TYPES() };
        #     return;
        # },
        data            => sub {
            my ($dfv)   = @_;

            my $val     = $dfv->get_filtered_data->{
                $dfv->get_current_constraint_field
            };

            return unless UNIVERSAL::isa($val, 'HASH');

            return 1;
        },
    },
};

=head1 NAME

Zaaksysteem::ZAPI::CRUD::Interface::Action - Single action definition for this interface

=head1 SYNOPSIS

=head1 DESCRIPTION

The CRUD Interface Action description for Angular

=head1 ATTRIBUTES

=head2 id [required]

ISA: Str

Internal id for this action, make sure the id is unique within a CRUD::Interface

=cut

has 'id'    => (
    is          => 'ro',
    isa         => 'Str',
    required    => 1,
);

=head2 label [required]

ISA: Str

Internal id for this action, make sure the id is unique within a CRUD::Interface

=cut

has 'label'    => (
    is          => 'ro',
    isa         => 'Str',
    required    => 1,
);

=head2 type [optional]

ISA: Str

The type of this action. This will define the behavior for the action, you could
be showing a popup, or trigger a delete action

B<types>

=over

=item delete

Triggers the delete action for this row

=item popup

Displays a popup with content from C<data.url> (see data property below).

=back

=cut

has 'type'    => (
    is          => 'ro',
    isa         => 'Str',
);

=head2 data [optional]

ISA: HashRef

 $action->data({ url => '/form/edit-case.html'});

A hashref containing custom data for the given C<type>. This could be a C<url>
for a popup.

=cut

has 'data'    => (
    is          => 'ro',
    isa         => 'HashRef',
);

=head2 when [optional]

ISA: Str

 $action->when("selectedItems.length==1");

A conditional attribute defining when this action could be used. For instance,
you only want to be able to trigger this action on a single selected row (popup).

=cut

has 'when'    => (
    is          => 'ro',
    isa         => 'Str',
);

=head1 METHODS

=head2 TO_JSON

Send data in json format

=cut

sub TO_JSON {
    my $self        = shift;

    $self->_validate;

    return $self->_object_params;
}

=head1 INTERNAL METHODS

=head2 _object_params

Returns a HASHRef containing this object parameters according to this object
profile

=cut

sub _object_params {
    my $self        = shift;

    return { map {
        $_ => $self->$_
    } (
        @{ OBJECT_PROFILE->{required} },
        @{ OBJECT_PROFILE->{optional} }
    ) };
}

=head2 _validate

Validates this object, to prove it is complete and contains valid attributes

=cut

sub _validate {
    my $self            = shift;

    my $dv              = Data::FormValidator->check(
        $self->_object_params,
        OBJECT_PROFILE
    );

    die(
        'Cannot validate form field, invalid or missing: '
        . join(',', $dv->invalid, $dv->missing)
    ) unless $dv->success;

    return 1;
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::ZAPI::CRUD::Interface> L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 ACTION_TYPES

TODO: Fix the POD

=cut

=head2 OBJECT_PROFILE

TODO: Fix the POD

=cut

