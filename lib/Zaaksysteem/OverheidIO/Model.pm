package Zaaksysteem::OverheidIO::Model;
use Moose;


=head1 NAME

Zaaksysteem::OverheidIO::Model - An Zaaksysteem OverheidIO bridge model

=head1 DESCRIPTION

=head1 SYNOPSIS

    use Zaaksysteem::OverheidIO::Model;
    my $model = Zaaksysteem::OverheidIO::Model->new(
        schema => $schema
    );

=cut

use Zaaksysteem::BR::Subject;
use Zaaksysteem::Tools;

with 'MooseX::Log::Log4perl';

=head1 ATTRIBUTES

=head2 schema

A L<Zaaksysteem::Schema> object

=head2 _bridge

A L<Zaaksysteem::BR::Subject> object. Not to be called directly, use
C<get_bridge_for_overheid_io> function.

=cut

has schema => (
    is       => 'ro',
    isa      => "Zaaksysteem::Schema",
    required => 1,
);

has _bridge => (
    is => 'ro',
    isa => 'Zaaksysteem::BR::Subject',
    default => sub {
        my $self = shift;
        return  Zaaksysteem::BR::Subject->new(
            schema        => $self->schema,
            remote_search => 'openkvk',
        );
    },
    lazy => 1,
);

=head2 get_overheid_io_interface

Get the OverheidIO interface

=cut

sub get_overheid_io_interface {
    my $self = shift;

    my $interface = $self->schema->resultset("Interface")->search_active({module => "overheidio"})->first;
    if ($interface) {
        return $interface;
    }
    $self->log->trace("No active OverheidIO interface");
    return undef;
}

=head2 get_bridge_for_overheid_io

Get the L<Zaaksysteem::BR::Subject> object for OverheidIO
Returns undef if there is no eHerkenning/OverheidIO link

=cut

sub get_bridge_for_overheid_io {
    my $self = shift;

    return undef unless $self->has_eherkenning_overheid_io;
    return $self->_bridge;

}

=head2 has_eherkenning_overheid_io

Check if you can query overheidIO from eHerkenning requests.

=cut

sub has_eherkenning_overheid_io {
    my $self = shift;

    if (my $interface = $self->get_overheid_io_interface) {
        if ($interface->get_interface_config->{overheid_io_eherkenning_request}) {
            return 1
        }
    }
    return 0;
}

=head2 get_company_from_overheid_io

Search for a company at OverheidIO

=cut

sub get_company_from_overheid_io {
    my $self = shift;
    my %opts = @_;

    my $br = $self->get_bridge_for_overheid_io;
    return undef unless $br;

    my @objects = $br->search(
        {
            subject_type         => 'company',
            'subject.coc_number' => $opts{kvknummer},
            $opts{vestigingsnummer}
            ? ('subject.coc_location_number' => int($opts{vestigingsnummer}))
            : (),
        }
    );
    if (!@objects) {
        throw("openkvk/company_not_found", "Unable to find company");
    }

    if (@objects == 1) {
        $br->remote_import($objects[0]);
        return \@objects;
    }
    else {
        return \@objects;
    }
}

=head2 update_company_from_overheid_io

Update the betrokkene with information from Overheid.IO.
Do not update the betrokkene if or when
C<vestigingsnummer_override_used> has been set in the session
or when the Overheid.IO interface isn't active
or when the boolean bit is not set to actually update the information
or when the betrokkene isn't found

=cut

sub update_company_from_overheid_io {
    my ($self, $session, $betrokkene) = @_;

    if (!$betrokkene) {
        $self->log->debug("No betrokkene, not updating overheid.io");
        return 0;
    }

    if ($session->{vestigingsnummer_override_used}) {
        $self->log->debug("Override used, not updating overheid.io");
        return 0;
    }

    my $br = $self->get_bridge_for_overheid_io;
    return undef unless $br;

    my $b = $betrokkene->gm_bedrijf;
    my @objects = $br->search(
        {
            subject_type                  => 'company',
            'subject.coc_number'          => $b->dossiernummer,
            'subject.coc_location_number' => $b->vestigingsnummer,
        }
    );

    if (@objects == 1) {
        $br->remote_import($objects[0]);
    }
    return 0;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
