package Zaaksysteem::Schema::BeheerImportLog;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'Zaaksysteem::Result';


=head1 NAME

Zaaksysteem::Schema::BeheerImportLog

=cut

__PACKAGE__->table("beheer_import_log");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'beheer_import_log_id_seq'

=head2 import_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 old_data

  data_type: 'text'
  is_nullable: 1

=head2 new_data

  data_type: 'text'
  is_nullable: 1

=head2 created

  data_type: 'timestamp'
  is_nullable: 1

=head2 last_modified

  data_type: 'timestamp'
  is_nullable: 1

=head2 kolom

  data_type: 'text'
  is_nullable: 1

=head2 identifier

  data_type: 'text'
  is_nullable: 1

=head2 action

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "beheer_import_log_id_seq",
  },
  "import_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "old_data",
  { data_type => "text", is_nullable => 1 },
  "new_data",
  { data_type => "text", is_nullable => 1 },
  "created",
  { data_type => "timestamp", is_nullable => 1 },
  "last_modified",
  { data_type => "timestamp", is_nullable => 1 },
  "kolom",
  { data_type => "text", is_nullable => 1 },
  "identifier",
  { data_type => "text", is_nullable => 1 },
  "action",
  { data_type => "varchar", is_nullable => 1, size => 255 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 import_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::BeheerImport>

=cut

__PACKAGE__->belongs_to(
  "import_id",
  "Zaaksysteem::Schema::BeheerImport",
  { id => "import_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2014-03-18 11:06:59
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:KaPw6ZJkq5lBijCzATyn/w





# You can replace this text with custom content, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

