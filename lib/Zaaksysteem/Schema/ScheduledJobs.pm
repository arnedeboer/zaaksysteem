package Zaaksysteem::Schema::ScheduledJobs;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'Zaaksysteem::Result';


=head1 NAME

Zaaksysteem::Schema::ScheduledJobs

=cut

__PACKAGE__->table("scheduled_jobs");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'scheduled_jobs_id_seq'

=head2 task

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 scheduled_for

  data_type: 'timestamp'
  is_nullable: 1

=head2 parameters

  data_type: 'text'
  is_nullable: 1

=head2 created

  data_type: 'timestamp'
  is_nullable: 1

=head2 last_modified

  data_type: 'timestamp'
  is_nullable: 1

=head2 deleted

  data_type: 'timestamp'
  is_nullable: 1

=head2 schedule_type

  data_type: 'varchar'
  is_nullable: 1
  size: 16

=head2 case_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 uuid

  data_type: 'uuid'
  default_value: uuid_generate_v4()
  is_nullable: 1
  size: 16

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "scheduled_jobs_id_seq",
  },
  "task",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "scheduled_for",
  { data_type => "timestamp", is_nullable => 1 },
  "parameters",
  { data_type => "text", is_nullable => 1 },
  "created",
  { data_type => "timestamp", is_nullable => 1 },
  "last_modified",
  { data_type => "timestamp", is_nullable => 1 },
  "deleted",
  { data_type => "timestamp", is_nullable => 1 },
  "schedule_type",
  { data_type => "varchar", is_nullable => 1, size => 16 },
  "case_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "uuid",
  {
    data_type => "uuid",
    default_value => \"uuid_generate_v4()",
    is_nullable => 1,
    size => 16,
  },
);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->add_unique_constraint("scheduled_jobs_uuid_key", ["uuid"]);

=head1 RELATIONS

=head2 files

Type: has_many

Related object: L<Zaaksysteem::Schema::File>

=cut

__PACKAGE__->has_many(
  "files",
  "Zaaksysteem::Schema::File",
  { "foreign.scheduled_jobs_id" => "self.id" },
  {},
);

=head2 case_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Zaak>

=cut

__PACKAGE__->belongs_to("case_id", "Zaaksysteem::Schema::Zaak", { id => "case_id" });


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2016-02-03 15:30:23
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:c+pdzOCX8dutPXK9xM8ZSw

__PACKAGE__->resultset_class('Zaaksysteem::DB::ResultSet::ScheduledJobs');

__PACKAGE__->load_components(
    "+Zaaksysteem::DB::Component::ScheduledJobs",
    __PACKAGE__->load_components()
);

__PACKAGE__->add_columns('last_modified',
    { %{ __PACKAGE__->column_info('last_modified') },
    set_on_update => 1,
    set_on_create => 1,
});

__PACKAGE__->add_columns('created',
    { %{ __PACKAGE__->column_info('created') },
    set_on_create => 1,
});

use JSON;
use Data::Dumper;

__PACKAGE__->inflate_column('parameters', {
    inflate => sub { JSON->new->canonical->utf8(0)->decode(shift || '{}') },
    deflate => sub { JSON->new->canonical->utf8(1)->encode(shift || {}) },

});



# You can replace this text with custom content, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

