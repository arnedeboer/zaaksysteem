package Zaaksysteem::Schema::Directory;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'Zaaksysteem::Result';


=head1 NAME

Zaaksysteem::Schema::Directory

=cut

__PACKAGE__->table("directory");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'directory_id_seq'

=head2 name

  data_type: 'text'
  is_nullable: 0

=head2 case_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 original_name

  data_type: 'text'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "directory_id_seq",
  },
  "name",
  { data_type => "text", is_nullable => 0 },
  "case_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "original_name",
  { data_type => "text", is_nullable => 0 },
);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->add_unique_constraint("name_case_id", ["name", "case_id"]);

=head1 RELATIONS

=head2 case_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Zaak>

=cut

__PACKAGE__->belongs_to("case_id", "Zaaksysteem::Schema::Zaak", { id => "case_id" });

=head2 files

Type: has_many

Related object: L<Zaaksysteem::Schema::File>

=cut

__PACKAGE__->has_many(
  "files",
  "Zaaksysteem::Schema::File",
  { "foreign.directory_id" => "self.id" },
  {},
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2015-01-06 15:10:36
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:MsvEr9pkIWeKP4z8nFEZ9Q

__PACKAGE__->resultset_class('Zaaksysteem::Backend::Directory::ResultSet');

__PACKAGE__->load_components(qw/
    +Zaaksysteem::Backend::Directory::Component
    +DBIx::Class::Helper::Row::ToJSON
/);

__PACKAGE__->belongs_to("case", "Zaaksysteem::Schema::Zaak", { id => "case_id" });

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

