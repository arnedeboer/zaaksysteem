package Zaaksysteem::Schema::Queue;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'Zaaksysteem::Result';


=head1 NAME

Zaaksysteem::Schema::Queue

=cut

__PACKAGE__->table("queue");

=head1 ACCESSORS

=head2 id

  data_type: 'uuid'
  default_value: uuid_generate_v4()
  is_nullable: 0
  size: 16

=head2 object_id

  data_type: 'uuid'
  is_foreign_key: 1
  is_nullable: 1
  size: 16

=head2 status

  data_type: 'text'
  default_value: 'pending'
  is_nullable: 0

=head2 type

  data_type: 'text'
  is_nullable: 0

=head2 label

  data_type: 'text'
  is_nullable: 0

=head2 data

  data_type: 'text'
  default_value: '{}'
  is_nullable: 0

=head2 date_created

  data_type: 'timestamp'
  default_value: statement_timestamp()
  is_nullable: 0

=head2 date_started

  data_type: 'timestamp'
  is_nullable: 1

=head2 date_finished

  data_type: 'timestamp'
  is_nullable: 1

=head2 parent_id

  data_type: 'uuid'
  is_foreign_key: 1
  is_nullable: 1
  size: 16

=head2 priority

  data_type: 'integer'
  default_value: 1000
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type => "uuid",
    default_value => \"uuid_generate_v4()",
    is_nullable => 0,
    size => 16,
  },
  "object_id",
  { data_type => "uuid", is_foreign_key => 1, is_nullable => 1, size => 16 },
  "status",
  { data_type => "text", default_value => "pending", is_nullable => 0 },
  "type",
  { data_type => "text", is_nullable => 0 },
  "label",
  { data_type => "text", is_nullable => 0 },
  "data",
  { data_type => "text", default_value => "{}", is_nullable => 0 },
  "date_created",
  {
    data_type     => "timestamp",
    default_value => \"statement_timestamp()",
    is_nullable   => 0,
  },
  "date_started",
  { data_type => "timestamp", is_nullable => 1 },
  "date_finished",
  { data_type => "timestamp", is_nullable => 1 },
  "parent_id",
  { data_type => "uuid", is_foreign_key => 1, is_nullable => 1, size => 16 },
  "priority",
  { data_type => "integer", default_value => 1000, is_nullable => 0 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 parent_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Queue>

=cut

__PACKAGE__->belongs_to(
  "parent_id",
  "Zaaksysteem::Schema::Queue",
  { id => "parent_id" },
);

=head2 queues

Type: has_many

Related object: L<Zaaksysteem::Schema::Queue>

=cut

__PACKAGE__->has_many(
  "queues",
  "Zaaksysteem::Schema::Queue",
  { "foreign.parent_id" => "self.id" },
  {},
);

=head2 object_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ObjectData>

=cut

__PACKAGE__->belongs_to(
  "object_id",
  "Zaaksysteem::Schema::ObjectData",
  { uuid => "object_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2016-11-28 12:27:39
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:vEkP36MrXtFz85hypXCRjA

use JSON;

__PACKAGE__->resultset_class('Zaaksysteem::Backend::Object::Queue::ResultSet');

__PACKAGE__->load_components(
    '+Zaaksysteem::Backend::Object::Queue::Component',
    __PACKAGE__->load_components()
);

__PACKAGE__->inflate_column(data => {
    inflate => sub { JSON->new->utf8(0)->decode(shift || '{}') },
    deflate => sub { JSON->new->canonical(1)->utf8(1)->encode(shift || {}) }
});

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
