package Zaaksysteem::Schema::Subject;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'Zaaksysteem::Result';


=head1 NAME

Zaaksysteem::Schema::Subject

=cut

__PACKAGE__->table("subject");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'subject_id_seq'

=head2 uuid

  data_type: 'uuid'
  default_value: uuid_generate_v4()
  is_nullable: 1
  size: 16

=head2 subject_type

  data_type: 'text'
  is_nullable: 0

=head2 properties

  data_type: 'text'
  default_value: '{}'
  is_nullable: 0

=head2 settings

  data_type: 'text'
  default_value: '{}'
  is_nullable: 0

=head2 username

  data_type: 'text'
  is_nullable: 0
  original: {data_type => "varchar"}

=head2 last_modified

  data_type: 'timestamp'
  default_value: current_timestamp
  is_nullable: 1
  original: {default_value => \"now()"}

=head2 role_ids

  data_type: 'integer[]'
  is_nullable: 1

=head2 group_ids

  data_type: 'integer[]'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "subject_id_seq",
  },
  "uuid",
  {
    data_type => "uuid",
    default_value => \"uuid_generate_v4()",
    is_nullable => 1,
    size => 16,
  },
  "subject_type",
  { data_type => "text", is_nullable => 0 },
  "properties",
  { data_type => "text", default_value => "{}", is_nullable => 0 },
  "settings",
  { data_type => "text", default_value => "{}", is_nullable => 0 },
  "username",
  {
    data_type   => "text",
    is_nullable => 0,
    original    => { data_type => "varchar" },
  },
  "last_modified",
  {
    data_type     => "timestamp",
    default_value => \"current_timestamp",
    is_nullable   => 1,
    original      => { default_value => \"now()" },
  },
  "role_ids",
  { data_type => "integer[]", is_nullable => 1 },
  "group_ids",
  { data_type => "integer[]", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->add_unique_constraint("subject_uuid_key", ["uuid"]);

=head1 RELATIONS

=head2 object_mutations

Type: has_many

Related object: L<Zaaksysteem::Schema::ObjectMutation>

=cut

__PACKAGE__->has_many(
  "object_mutations",
  "Zaaksysteem::Schema::ObjectMutation",
  { "foreign.subject_id" => "self.id" },
  {},
);

=head2 user_entities

Type: has_many

Related object: L<Zaaksysteem::Schema::UserEntity>

=cut

__PACKAGE__->has_many(
  "user_entities",
  "Zaaksysteem::Schema::UserEntity",
  { "foreign.subject_id" => "self.id" },
  {},
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2014-10-24 11:04:51
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:2TitL4s/SDnsPBMjRpslMg

__PACKAGE__->resultset_class('Zaaksysteem::Backend::Subject::ResultSet');

use JSON;

__PACKAGE__->load_components(
    '+Zaaksysteem::Backend::Subject::Component',
    __PACKAGE__->load_components()
);

__PACKAGE__->inflate_column('properties', {
    inflate => sub { JSON->new->utf8(0)->decode(shift // '{}') },
    deflate => sub { JSON->new->utf8(1)->encode(shift // {}) },
});

__PACKAGE__->inflate_column('settings', {
    inflate => sub { JSON->new->utf8(0)->decode(shift // '{}') },
    deflate => sub { JSON->new->utf8(1)->encode(shift // {}) },
});

__PACKAGE__->add_columns('last_modified',
    { %{ __PACKAGE__->column_info('last_modified') },
    set_on_update => 1,
    set_on_create => 1,
});

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

