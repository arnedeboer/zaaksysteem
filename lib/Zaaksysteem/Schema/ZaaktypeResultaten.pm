package Zaaksysteem::Schema::ZaaktypeResultaten;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'Zaaksysteem::Result';


=head1 NAME

Zaaksysteem::Schema::ZaaktypeResultaten

=cut

__PACKAGE__->table("zaaktype_resultaten");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'zaaktype_resultaten_id_seq'

=head2 zaaktype_node_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 zaaktype_status_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 resultaat

  data_type: 'text'
  is_nullable: 1

=head2 ingang

  data_type: 'text'
  is_nullable: 1

=head2 bewaartermijn

  data_type: 'integer'
  is_nullable: 1

=head2 created

  data_type: 'timestamp'
  is_nullable: 1

=head2 last_modified

  data_type: 'timestamp'
  is_nullable: 1

=head2 dossiertype

  data_type: 'varchar'
  is_nullable: 1
  size: 50

=head2 label

  data_type: 'text'
  is_nullable: 1

=head2 selectielijst

  data_type: 'text'
  is_nullable: 1

=head2 archiefnominatie

  data_type: 'text'
  is_nullable: 1

=head2 comments

  data_type: 'text'
  is_nullable: 1

=head2 external_reference

  data_type: 'text'
  is_nullable: 1

=head2 trigger_archival

  data_type: 'boolean'
  default_value: true
  is_nullable: 0

=head2 selectielijst_brondatum

  data_type: 'date'
  is_nullable: 1

=head2 selectielijst_einddatum

  data_type: 'date'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "zaaktype_resultaten_id_seq",
  },
  "zaaktype_node_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "zaaktype_status_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "resultaat",
  { data_type => "text", is_nullable => 1 },
  "ingang",
  { data_type => "text", is_nullable => 1 },
  "bewaartermijn",
  { data_type => "integer", is_nullable => 1 },
  "created",
  { data_type => "timestamp", is_nullable => 1 },
  "last_modified",
  { data_type => "timestamp", is_nullable => 1 },
  "dossiertype",
  { data_type => "varchar", is_nullable => 1, size => 50 },
  "label",
  { data_type => "text", is_nullable => 1 },
  "selectielijst",
  { data_type => "text", is_nullable => 1 },
  "archiefnominatie",
  { data_type => "text", is_nullable => 1 },
  "comments",
  { data_type => "text", is_nullable => 1 },
  "external_reference",
  { data_type => "text", is_nullable => 1 },
  "trigger_archival",
  { data_type => "boolean", default_value => \"true", is_nullable => 0 },
  "selectielijst_brondatum",
  { data_type => "date", is_nullable => 1 },
  "selectielijst_einddatum",
  { data_type => "date", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 zaaktype_status_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ZaaktypeStatus>

=cut

__PACKAGE__->belongs_to(
  "zaaktype_status_id",
  "Zaaksysteem::Schema::ZaaktypeStatus",
  { id => "zaaktype_status_id" },
);

=head2 zaaktype_node_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ZaaktypeNode>

=cut

__PACKAGE__->belongs_to(
  "zaaktype_node_id",
  "Zaaksysteem::Schema::ZaaktypeNode",
  { id => "zaaktype_node_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2016-03-16 14:31:36
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:G551fHQOVQMgYWbM3ymB1Q

__PACKAGE__->resultset_class('Zaaksysteem::DB::ResultSet::ZaaktypeResultaten');

__PACKAGE__->add_columns('last_modified',
    { %{ __PACKAGE__->column_info('last_modified') },
    set_on_update => 1,
    set_on_create => 1,
});

__PACKAGE__->add_columns('created',
    { %{ __PACKAGE__->column_info('created') },
    set_on_create => 1,
});




# You can replace this text with custom content, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

