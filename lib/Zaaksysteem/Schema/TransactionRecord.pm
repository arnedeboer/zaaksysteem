package Zaaksysteem::Schema::TransactionRecord;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'Zaaksysteem::Result';


=head1 NAME

Zaaksysteem::Schema::TransactionRecord

=cut

__PACKAGE__->table("transaction_record");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'transaction_record_id_seq'

=head2 transaction_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 input

  data_type: 'text'
  is_nullable: 0

=head2 output

  data_type: 'text'
  is_nullable: 0

=head2 is_error

  data_type: 'boolean'
  default_value: false
  is_nullable: 0

=head2 date_executed

  data_type: 'timestamp'
  default_value: current_timestamp
  is_nullable: 0
  original: {default_value => \"now()"}

=head2 date_deleted

  data_type: 'timestamp'
  is_nullable: 1

=head2 preview_string

  data_type: 'varchar'
  is_nullable: 1
  size: 200

=head2 last_error

  data_type: 'text'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "transaction_record_id_seq",
  },
  "transaction_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "input",
  { data_type => "text", is_nullable => 0 },
  "output",
  { data_type => "text", is_nullable => 0 },
  "is_error",
  { data_type => "boolean", default_value => \"false", is_nullable => 0 },
  "date_executed",
  {
    data_type     => "timestamp",
    default_value => \"current_timestamp",
    is_nullable   => 0,
    original      => { default_value => \"now()" },
  },
  "date_deleted",
  { data_type => "timestamp", is_nullable => 1 },
  "preview_string",
  { data_type => "varchar", is_nullable => 1, size => 200 },
  "last_error",
  { data_type => "text", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 transaction_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Transaction>

=cut

__PACKAGE__->belongs_to(
  "transaction_id",
  "Zaaksysteem::Schema::Transaction",
  { id => "transaction_id" },
);

=head2 transaction_record_to_objects

Type: has_many

Related object: L<Zaaksysteem::Schema::TransactionRecordToObject>

=cut

__PACKAGE__->has_many(
  "transaction_record_to_objects",
  "Zaaksysteem::Schema::TransactionRecordToObject",
  { "foreign.transaction_record_id" => "self.id" },
  {},
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2014-03-18 11:07:01
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:5a88rtgAQsQA5l3gAKfhqA

__PACKAGE__->belongs_to(
  "transaction",
  "Zaaksysteem::Schema::Transaction",
  { id => "transaction_id" },
);

__PACKAGE__->resultset_class('Zaaksysteem::Backend::Sysin::TransactionRecord::ResultSet');

__PACKAGE__->load_components(qw/
    +Zaaksysteem::Backend::Sysin::TransactionRecord::Component
    +DBIx::Class::Helper::Row::ToJSON
/);


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

