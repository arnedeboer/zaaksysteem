package Zaaksysteem::Schema::Adres;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'Zaaksysteem::Result';


=head1 NAME

Zaaksysteem::Schema::Adres

=cut

__PACKAGE__->table("adres");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'adres_id_seq'

=head2 straatnaam

  data_type: 'varchar'
  is_nullable: 1
  size: 80

=head2 huisnummer

  data_type: 'smallint'
  is_nullable: 1

=head2 huisletter

  data_type: 'char'
  is_nullable: 1
  size: 1

=head2 huisnummertoevoeging

  data_type: 'varchar'
  is_nullable: 1
  size: 4

=head2 nadere_aanduiding

  data_type: 'varchar'
  is_nullable: 1
  size: 35

=head2 postcode

  data_type: 'varchar'
  is_nullable: 1
  size: 6

=head2 woonplaats

  data_type: 'varchar'
  is_nullable: 1
  size: 75

=head2 gemeentedeel

  data_type: 'varchar'
  is_nullable: 1
  size: 75

=head2 functie_adres

  data_type: 'char'
  is_nullable: 1
  size: 1

=head2 datum_aanvang_bewoning

  data_type: 'date'
  is_nullable: 1

=head2 woonplaats_id

  data_type: 'varchar'
  is_nullable: 1
  size: 32

=head2 gemeente_code

  data_type: 'smallint'
  is_nullable: 1

=head2 hash

  data_type: 'varchar'
  is_nullable: 1
  size: 32

=head2 import_datum

  data_type: 'timestamp'
  is_nullable: 1

=head2 deleted_on

  data_type: 'timestamp'
  is_nullable: 1

=head2 adres_buitenland1

  data_type: 'text'
  is_nullable: 1

=head2 adres_buitenland2

  data_type: 'text'
  is_nullable: 1

=head2 adres_buitenland3

  data_type: 'text'
  is_nullable: 1

=head2 landcode

  data_type: 'integer'
  default_value: 6030
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "adres_id_seq",
  },
  "straatnaam",
  { data_type => "varchar", is_nullable => 1, size => 80 },
  "huisnummer",
  { data_type => "smallint", is_nullable => 1 },
  "huisletter",
  { data_type => "char", is_nullable => 1, size => 1 },
  "huisnummertoevoeging",
  { data_type => "varchar", is_nullable => 1, size => 4 },
  "nadere_aanduiding",
  { data_type => "varchar", is_nullable => 1, size => 35 },
  "postcode",
  { data_type => "varchar", is_nullable => 1, size => 6 },
  "woonplaats",
  { data_type => "varchar", is_nullable => 1, size => 75 },
  "gemeentedeel",
  { data_type => "varchar", is_nullable => 1, size => 75 },
  "functie_adres",
  { data_type => "char", is_nullable => 1, size => 1 },
  "datum_aanvang_bewoning",
  { data_type => "date", is_nullable => 1 },
  "woonplaats_id",
  { data_type => "varchar", is_nullable => 1, size => 32 },
  "gemeente_code",
  { data_type => "smallint", is_nullable => 1 },
  "hash",
  { data_type => "varchar", is_nullable => 1, size => 32 },
  "import_datum",
  { data_type => "timestamp", is_nullable => 1 },
  "deleted_on",
  { data_type => "timestamp", is_nullable => 1 },
  "adres_buitenland1",
  { data_type => "text", is_nullable => 1 },
  "adres_buitenland2",
  { data_type => "text", is_nullable => 1 },
  "adres_buitenland3",
  { data_type => "text", is_nullable => 1 },
  "landcode",
  { data_type => "integer", default_value => 6030, is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 natuurlijk_persoons

Type: has_many

Related object: L<Zaaksysteem::Schema::NatuurlijkPersoon>

=cut

__PACKAGE__->has_many(
  "natuurlijk_persoons",
  "Zaaksysteem::Schema::NatuurlijkPersoon",
  { "foreign.adres_id" => "self.id" },
  {},
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2014-08-20 14:41:02
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:mTEbZUYYa6lOrMKf6jV8kw

__PACKAGE__->add_columns('import_datum',
    { %{ __PACKAGE__->column_info('import_datum') },
    set_on_create => 1,
});

__PACKAGE__->load_components(
    "+DBIx::Class::Helper::Row::ToJSON",
    "+Zaaksysteem::Backend::Component",
    __PACKAGE__->load_components()
);


# You can replace this text with custom content, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

