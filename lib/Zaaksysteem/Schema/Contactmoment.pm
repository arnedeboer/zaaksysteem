package Zaaksysteem::Schema::Contactmoment;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'Zaaksysteem::Result';


=head1 NAME

Zaaksysteem::Schema::Contactmoment

=cut

__PACKAGE__->table("contactmoment");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'contactmoment_id_seq'

=head2 subject_id

  data_type: 'varchar'
  is_nullable: 1
  size: 100

=head2 case_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 type

  data_type: 'enum'
  extra: {custom_type_name => "contactmoment_type",list => ["email","note"]}
  is_nullable: 0

=head2 medium

  data_type: 'enum'
  extra: {custom_type_name => "contactmoment_medium",list => ["behandelaar","balie","telefoon","post","email","webformulier"]}
  is_nullable: 0

=head2 date_created

  data_type: 'timestamp'
  default_value: current_timestamp
  is_nullable: 0
  original: {default_value => \"now()"}

=head2 created_by

  data_type: 'text'
  is_nullable: 0

=head2 uuid

  data_type: 'uuid'
  default_value: uuid_generate_v4()
  is_nullable: 0
  size: 16

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "contactmoment_id_seq",
  },
  "subject_id",
  { data_type => "varchar", is_nullable => 1, size => 100 },
  "case_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "type",
  {
    data_type => "enum",
    extra => { custom_type_name => "contactmoment_type", list => ["email", "note"] },
    is_nullable => 0,
  },
  "medium",
  {
    data_type => "enum",
    extra => {
      custom_type_name => "contactmoment_medium",
      list => [
        "behandelaar",
        "balie",
        "telefoon",
        "post",
        "email",
        "webformulier",
      ],
    },
    is_nullable => 0,
  },
  "date_created",
  {
    data_type     => "timestamp",
    default_value => \"current_timestamp",
    is_nullable   => 0,
    original      => { default_value => \"now()" },
  },
  "created_by",
  { data_type => "text", is_nullable => 0 },
  "uuid",
  {
    data_type => "uuid",
    default_value => \"uuid_generate_v4()",
    is_nullable => 0,
    size => 16,
  },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 case_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Zaak>

=cut

__PACKAGE__->belongs_to("case_id", "Zaaksysteem::Schema::Zaak", { id => "case_id" });

=head2 contactmoment_emails

Type: has_many

Related object: L<Zaaksysteem::Schema::ContactmomentEmail>

=cut

__PACKAGE__->has_many(
  "contactmoment_emails",
  "Zaaksysteem::Schema::ContactmomentEmail",
  { "foreign.contactmoment_id" => "self.id" },
  {},
);

=head2 contactmoment_notes

Type: has_many

Related object: L<Zaaksysteem::Schema::ContactmomentNote>

=cut

__PACKAGE__->has_many(
  "contactmoment_notes",
  "Zaaksysteem::Schema::ContactmomentNote",
  { "foreign.contactmoment_id" => "self.id" },
  {},
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2015-12-01 14:24:49
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:FnLareSppXgXsxX7uuOB7Q

__PACKAGE__->resultset_class('Zaaksysteem::Backend::Contactmoment::ResultSet');

__PACKAGE__->load_components(
    "+Zaaksysteem::Backend::Contactmoment::Component",
    __PACKAGE__->load_components()
);

=head2 contactmoment_emails

Type: has_many

Related object: L<Zaaksysteem::Schema::ContactmomentEmail>

=cut

__PACKAGE__->belongs_to(
  "contactmoment_email",
  "Zaaksysteem::Schema::ContactmomentEmail",
  { "foreign.contactmoment_id" => "self.id" },
  {},
);

=head2 contactmoment_notes

Type: has_many

Related object: L<Zaaksysteem::Schema::ContactmomentNote>

=cut

__PACKAGE__->belongs_to(
  "contactmoment_note",
  "Zaaksysteem::Schema::ContactmomentNote",
  { "foreign.contactmoment_id" => "self.id" },
  {},
);

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

