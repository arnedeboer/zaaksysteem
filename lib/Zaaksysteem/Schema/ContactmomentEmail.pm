package Zaaksysteem::Schema::ContactmomentEmail;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'Zaaksysteem::Result';


=head1 NAME

Zaaksysteem::Schema::ContactmomentEmail

=cut

__PACKAGE__->table("contactmoment_email");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'contactmoment_email_id_seq'

=head2 filestore_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 contactmoment_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 body

  data_type: 'text'
  is_nullable: 0

=head2 subject

  data_type: 'varchar'
  is_nullable: 0
  size: 150

=head2 recipient

  data_type: 'varchar'
  is_nullable: 0
  size: 256

=head2 cc

  data_type: 'text'
  is_nullable: 1

=head2 bcc

  data_type: 'text'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "contactmoment_email_id_seq",
  },
  "filestore_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "contactmoment_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "body",
  { data_type => "text", is_nullable => 0 },
  "subject",
  { data_type => "varchar", is_nullable => 0, size => 150 },
  "recipient",
  { data_type => "varchar", is_nullable => 0, size => 256 },
  "cc",
  { data_type => "text", is_nullable => 1 },
  "bcc",
  { data_type => "text", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 contactmoment_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Contactmoment>

=cut

__PACKAGE__->belongs_to(
  "contactmoment_id",
  "Zaaksysteem::Schema::Contactmoment",
  { id => "contactmoment_id" },
);

=head2 filestore_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Filestore>

=cut

__PACKAGE__->belongs_to(
  "filestore_id",
  "Zaaksysteem::Schema::Filestore",
  { id => "filestore_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2014-10-15 16:02:23
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:4iNfCJRcnVVKyOxdg+63mw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

