package Zaaksysteem::Schema::Filestore;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'Zaaksysteem::Result';


=head1 NAME

Zaaksysteem::Schema::Filestore

=cut

__PACKAGE__->table("filestore");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'filestore_id_seq'

=head2 uuid

  data_type: 'uuid'
  is_nullable: 0
  size: 16

=head2 thumbnail_uuid

  data_type: 'uuid'
  is_nullable: 1
  size: 16

=head2 original_name

  data_type: 'varchar'
  is_nullable: 0
  size: 250

=head2 size

  data_type: 'integer'
  is_nullable: 0

=head2 mimetype

  data_type: 'varchar'
  is_nullable: 0
  size: 160

=head2 md5

  data_type: 'varchar'
  is_nullable: 0
  size: 100

=head2 date_created

  data_type: 'timestamp'
  default_value: current_timestamp
  is_nullable: 0
  original: {default_value => \"now()"}

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "filestore_id_seq",
  },
  "uuid",
  { data_type => "uuid", is_nullable => 0, size => 16 },
  "thumbnail_uuid",
  { data_type => "uuid", is_nullable => 1, size => 16 },
  "original_name",
  { data_type => "varchar", is_nullable => 0, size => 250 },
  "size",
  { data_type => "integer", is_nullable => 0 },
  "mimetype",
  { data_type => "varchar", is_nullable => 0, size => 160 },
  "md5",
  { data_type => "varchar", is_nullable => 0, size => 100 },
  "date_created",
  {
    data_type     => "timestamp",
    default_value => \"current_timestamp",
    is_nullable   => 0,
    original      => { default_value => \"now()" },
  },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 bibliotheek_sjablonens

Type: has_many

Related object: L<Zaaksysteem::Schema::BibliotheekSjablonen>

=cut

__PACKAGE__->has_many(
  "bibliotheek_sjablonens",
  "Zaaksysteem::Schema::BibliotheekSjablonen",
  { "foreign.filestore_id" => "self.id" },
  {},
);

=head2 contactmoment_emails

Type: has_many

Related object: L<Zaaksysteem::Schema::ContactmomentEmail>

=cut

__PACKAGE__->has_many(
  "contactmoment_emails",
  "Zaaksysteem::Schema::ContactmomentEmail",
  { "foreign.filestore_id" => "self.id" },
  {},
);

=head2 files

Type: has_many

Related object: L<Zaaksysteem::Schema::File>

=cut

__PACKAGE__->has_many(
  "files",
  "Zaaksysteem::Schema::File",
  { "foreign.filestore_id" => "self.id" },
  {},
);

=head2 transactions

Type: has_many

Related object: L<Zaaksysteem::Schema::Transaction>

=cut

__PACKAGE__->has_many(
  "transactions",
  "Zaaksysteem::Schema::Transaction",
  { "foreign.input_file" => "self.id" },
  {},
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2014-03-18 11:07:00
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:w3IZNB0zwxdIJ1RjppZW4A

__PACKAGE__->resultset_class('Zaaksysteem::Backend::Filestore::ResultSet');
__PACKAGE__->load_components(
    '+DBIx::Class::Helper::Row::ToJSON',
    '+Zaaksysteem::Backend::Filestore::Component',
    __PACKAGE__->load_components()
);

# You can replace this text with custom content, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

