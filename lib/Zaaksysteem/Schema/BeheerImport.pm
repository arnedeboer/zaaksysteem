package Zaaksysteem::Schema::BeheerImport;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'Zaaksysteem::Result';


=head1 NAME

Zaaksysteem::Schema::BeheerImport

=cut

__PACKAGE__->table("beheer_import");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'beheer_import_id_seq'

=head2 importtype

  data_type: 'varchar'
  is_nullable: 1
  size: 256

=head2 succesvol

  data_type: 'integer'
  is_nullable: 1

=head2 finished

  data_type: 'timestamp'
  is_nullable: 1

=head2 import_create

  data_type: 'integer'
  is_nullable: 1

=head2 import_update

  data_type: 'integer'
  is_nullable: 1

=head2 error

  data_type: 'integer'
  is_nullable: 1

=head2 error_message

  data_type: 'text'
  is_nullable: 1

=head2 entries

  data_type: 'integer'
  is_nullable: 1

=head2 created

  data_type: 'timestamp'
  is_nullable: 1

=head2 last_modified

  data_type: 'timestamp'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "beheer_import_id_seq",
  },
  "importtype",
  { data_type => "varchar", is_nullable => 1, size => 256 },
  "succesvol",
  { data_type => "integer", is_nullable => 1 },
  "finished",
  { data_type => "timestamp", is_nullable => 1 },
  "import_create",
  { data_type => "integer", is_nullable => 1 },
  "import_update",
  { data_type => "integer", is_nullable => 1 },
  "error",
  { data_type => "integer", is_nullable => 1 },
  "error_message",
  { data_type => "text", is_nullable => 1 },
  "entries",
  { data_type => "integer", is_nullable => 1 },
  "created",
  { data_type => "timestamp", is_nullable => 1 },
  "last_modified",
  { data_type => "timestamp", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 beheer_import_logs

Type: has_many

Related object: L<Zaaksysteem::Schema::BeheerImportLog>

=cut

__PACKAGE__->has_many(
  "beheer_import_logs",
  "Zaaksysteem::Schema::BeheerImportLog",
  { "foreign.import_id" => "self.id" },
  {},
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2014-03-18 11:06:59
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:0Ubahok7yyEMtmohyakCzw

__PACKAGE__->add_columns('last_modified',
    { %{ __PACKAGE__->column_info('last_modified') },
    set_on_update => 1,
    set_on_create => 1,
});

__PACKAGE__->add_columns('created',
    { %{ __PACKAGE__->column_info('created') },
    set_on_create => 1,
});




# You can replace this text with custom content, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

