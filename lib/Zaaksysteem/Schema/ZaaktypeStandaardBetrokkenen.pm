package Zaaksysteem::Schema::ZaaktypeStandaardBetrokkenen;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'Zaaksysteem::Result';


=head1 NAME

Zaaksysteem::Schema::ZaaktypeStandaardBetrokkenen

=cut

__PACKAGE__->table("zaaktype_standaard_betrokkenen");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'zaaktype_standaard_betrokkenen_id_seq'

=head2 zaaktype_node_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 zaak_status_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 betrokkene_type

  data_type: 'text'
  is_nullable: 0

=head2 betrokkene_identifier

  data_type: 'text'
  is_nullable: 0

=head2 naam

  data_type: 'text'
  is_nullable: 0

=head2 rol

  data_type: 'text'
  is_nullable: 0

=head2 magic_string_prefix

  data_type: 'text'
  is_nullable: 1

=head2 gemachtigd

  data_type: 'boolean'
  default_value: false
  is_nullable: 0

=head2 notify

  data_type: 'boolean'
  default_value: false
  is_nullable: 0

=head2 uuid

  data_type: 'uuid'
  default_value: uuid_generate_v4()
  is_nullable: 1
  size: 16

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "zaaktype_standaard_betrokkenen_id_seq",
  },
  "zaaktype_node_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "zaak_status_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "betrokkene_type",
  { data_type => "text", is_nullable => 0 },
  "betrokkene_identifier",
  { data_type => "text", is_nullable => 0 },
  "naam",
  { data_type => "text", is_nullable => 0 },
  "rol",
  { data_type => "text", is_nullable => 0 },
  "magic_string_prefix",
  { data_type => "text", is_nullable => 1 },
  "gemachtigd",
  { data_type => "boolean", default_value => \"false", is_nullable => 0 },
  "notify",
  { data_type => "boolean", default_value => \"false", is_nullable => 0 },
  "uuid",
  {
    data_type => "uuid",
    default_value => \"uuid_generate_v4()",
    is_nullable => 1,
    size => 16,
  },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 zaak_status_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ZaaktypeStatus>

=cut

__PACKAGE__->belongs_to(
  "zaak_status_id",
  "Zaaksysteem::Schema::ZaaktypeStatus",
  { id => "zaak_status_id" },
);

=head2 zaaktype_node_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ZaaktypeNode>

=cut

__PACKAGE__->belongs_to(
  "zaaktype_node_id",
  "Zaaksysteem::Schema::ZaaktypeNode",
  { id => "zaaktype_node_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2016-10-06 17:13:12
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:mukv4YvsfgoBKxF+8d3r5Q

__PACKAGE__->resultset_class('Zaaksysteem::DB::ResultSet::ZaaktypeStandaardBetrokkenen');

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
