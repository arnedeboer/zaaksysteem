package Zaaksysteem::Schema::FileMetadata;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'Zaaksysteem::Result';


=head1 NAME

Zaaksysteem::Schema::FileMetadata

=cut

__PACKAGE__->table("file_metadata");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'file_metadata_id_seq'

=head2 description

  data_type: 'text'
  is_nullable: 1

=head2 trust_level

  data_type: 'varchar'
  default_value: 'Zaakvertrouwelijk'
  is_nullable: 0
  size: 100

=head2 origin

  data_type: 'varchar'
  is_nullable: 1
  size: 100

=head2 document_category

  data_type: 'varchar'
  is_nullable: 1
  size: 150

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "file_metadata_id_seq",
  },
  "description",
  { data_type => "text", is_nullable => 1 },
  "trust_level",
  {
    data_type => "varchar",
    default_value => "Zaakvertrouwelijk",
    is_nullable => 0,
    size => 100,
  },
  "origin",
  { data_type => "varchar", is_nullable => 1, size => 100 },
  "document_category",
  { data_type => "varchar", is_nullable => 1, size => 150 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 bibliotheek_kenmerkens

Type: has_many

Related object: L<Zaaksysteem::Schema::BibliotheekKenmerken>

=cut

__PACKAGE__->has_many(
  "bibliotheek_kenmerkens",
  "Zaaksysteem::Schema::BibliotheekKenmerken",
  { "foreign.file_metadata_id" => "self.id" },
  {},
);

=head2 files

Type: has_many

Related object: L<Zaaksysteem::Schema::File>

=cut

__PACKAGE__->has_many(
  "files",
  "Zaaksysteem::Schema::File",
  { "foreign.metadata_id" => "self.id" },
  {},
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2014-03-18 11:07:00
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:ysl20jtADEeY9AoFz1eMOA

__PACKAGE__->load_components(qw/
    +DBIx::Class::Helper::Row::ToJSON
/);

# Prevent making an entirely new file row when copying metadata
__PACKAGE__->has_many(
  "+files",
  "Zaaksysteem::Schema::File",
  { "foreign.metadata_id" => "self.id" },
  {cascade_copy => 0},
);

__PACKAGE__->add_columns("+description", { is_serializable => 1});

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

