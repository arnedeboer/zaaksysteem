package Zaaksysteem::Betrokkene;

use strict;
use warnings;

use Scalar::Util;
use Data::Dumper;
use Params::Profile;

use Moose;

with 'MooseX::Log::Log4perl';

use Zaaksysteem::Betrokkene::Object::NatuurlijkPersoon;
use Zaaksysteem::Betrokkene::Object::Bedrijf;
use Zaaksysteem::Betrokkene::Object::Medewerker;
use Zaaksysteem::Exception;

use constant BOBJECT => __PACKAGE__ . '::Object';

my $BETROKKENE_TYPES = {
    'medewerker'            => 'Medewerker',
    'natuurlijk_persoon'    => 'NatuurlijkPersoon',
    'bedrijf'               => 'Bedrijf',
};


has [qw/prod stash config customer/] => (
    'weak_ref' => 1,
    'is'    => 'rw',
);

has [qw/dbic/] => (
    #'weak_ref' => 1,
    'is'    => 'rw',
);

has '_dispatch_options' => (
    'is'    => 'ro',
    'lazy'  => 1,
    'default'   => sub {
        my $self    = shift;

        my $dispatch = {
            prod    => $self->prod,
            dbic    => $self->dbic,
            stash   => $self->stash,
            config  => $self->config,
            customer => $self->customer
        };

        Scalar::Util::weaken($dispatch->{stash});

        return $dispatch;
    }
);

has 'c' => (
    'is'    => 'ro',
    'weak_ref'  => 1,
);

has 'types'     => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        return $BETROKKENE_TYPES;
    },
);


Params::Profile->register_profile(
    'method'        => 'search',
    'profile'       => {
        required    => [qw/
            type
        /],
        optional    => [qw/
            intern
        /],
        defaults    => {
            'intern'    => 1,
        },
        constraint_methods => {
            'type'      => sub {
                my ($dfv, $val) = @_;

                return (exists($BETROKKENE_TYPES->{$val}) ? 1 : undef);
            },
            'intern'    => qr/^\d*$/,
        }
    }
);

sub search {
    my $self        = shift;
    my $opts        = shift;
    my ($search)    = @_;

    return unless Params::Profile->validate('params' => $opts);

    (
        $self->log->debug( 'M::B->search(): No search parameters given' ),
        return
    ) unless ($search && UNIVERSAL::isa($search, 'HASH'));

    ### Because we do not strictly use the Data::FormValidator object, we have
    ### redefine the defaults here. Not a problem, we keep the information for
    ### the reader in above profile, and also for use in other controllers
    ###
    ### For get, this will be true: we search intern by default, with one but:
    ### it could be a special string, but that is something for the designated
    ### Betrokkene Class.
    $opts->{intern} = (exists($opts->{intern}) ? $opts->{intern} : 1);

    my $bclass = $self->_get_bclass($opts->{type});

    ### The following eval is not really needed, but because we do want to
    ### kill the object when we cannot make sure it is valid, we will die in
    ### the Object::{TYPE} class. Catch it here, and return undef
    my ($bo);

    eval {
        $bo = $bclass->search(
            $self->_dispatch_options,
            $opts,
            $search,
        );
    };

    ### This logging should be extra.
    (
        $self->log->debug('M::B->search() DIE: ' . $bclass . ':' . $@),
        return
    ) if $@;

    return $bo;
}


Params::Profile->register_profile(
    'method'        => 'get',
    'profile'       => {
        required    => [qw/
        /],
        optional    => [qw/
            type
            intern
        /],
        defaults    => {
            'intern'    => undef,
        },
        constraint_methods => {
            'type'      => sub {
                my ($dfv, $val) = @_;

                return (exists($BETROKKENE_TYPES->{$val}) ? 1 : undef);
            },
            'intern'    => qr/^\d*$/,
        }
    }
);


=head2 get($opts, $id)

Retrieve a 'betrokkene' ('involved') object. This can be a citizen, organisation
or staff member.

=cut

sub get {
    my $self    = shift;
    my $opts    = shift;
    my $id      = shift;

    ### Validation and id?
    Params::Profile->validate('params' => $opts) or return;

    unless ($id && $id =~ /^[\d\-\w]+$/) {
        $self->log->error(sprintf(
            'M::B->get(): Invalid ID, "%s" is not an integer',
            $id // '<undef>',
        ));
        return;
    }

    ### We simply cannot continue, when we do not have a type, there is no
    ### pointer given about what kind of betrokkene this is and it is not a
    ### magic string
    unless (
        exists($opts->{type}) && $opts->{type}
    ) {
        my ($bid, $betrokkene_type);

        ### No internal pointer, we assume this is a 'magic' id or plain id
        if (!$id && (!exists($opts->{intern}) || !$opts->{intern})) {
            ### No magic string, and id is not a betrokkene id
            $self->log->debug(
                'M::B->get(): Not any idea where you want to '
                . ' get this information from, make sure you provide a'
                . ' betrokkene id (intern = 1) || at least an id'
            );
            return;
        }

        if ($id =~ /-/) {
            my ($req_type, $orig_id);

            if ($id =~ /^betrokkene-/) {
                $opts->{intern} = 0;
                ($opts->{type}, $id) = $id =~ /(\w+)-(\d+)$/;
            } else {
                $opts->{intern} = 1;
                ($req_type, $orig_id, $bid) = $id =~ /^([\w\_]*)-?(\d+)\-(\d+)$/;

                $self->log->debug('TRY Found REGTYPE');
                if ($req_type) {
                    $self->log->debug('Found REGTYPE');
                    $opts->{type} = $req_type;
                    $id =~ s/^[\w\_]+\-//g;
                }

            }
        }

        if ($opts->{intern} && !$bid) {
            $bid = $id;
        }

        ### Ok, fallback, id is probably a betrokkeneid
        if (!$bid && !$opts->{type}) {
            $bid = $id;
            $opts->{intern} = 1;
        }

        ### Get type from betrokkene database
        if (!$opts->{type}) {
            my $bdb = $self->dbic->resultset('ZaakBetrokkenen')->find($bid) or (
                $self->log->debug(
                    'M::B->get(): No betrokkene found by id ' .
                    $bid
                ),
                return
            );

            $opts->{type} = $bdb->betrokkene_type;
        }
    }

    #$self->log->debug('LETS SEE: ' . $opts->{type} . ':' . $id);

    ### Because we do not strictly use the Data::FormValidator object, we have
    ### redefine the defaults here. Not a problem, we keep the information for
    ### the reader in above profile, and also for use in other controllers
    ###
    ### For get, this will be true: we search intern by default, with one but:
    ### it could be a special string, but that is something for the designated
    ### Betrokkene Class.
    $opts->{intern} = (exists($opts->{intern}) ? $opts->{intern} : undef);

    my $bclass = $self->_get_bclass($opts->{type});

    ### The following eval is not really needed, but because we do want to
    ### kill the object when we cannot make sure it is valid, we will die in
    ### the Object::{TYPE} class. Catch it here, and return undef
    my $bo;

    my $bo_opts = {
        'trigger'       => 'get',
        'id'            => $id,
        %{ $self->_dispatch_options },
        %{ $opts }
    };

    if (my $cached = $self->from_cache($bo_opts)) {
        return $cached;
    }

    eval {
        $bo = $bclass->new(%{ $bo_opts });
    };

    if($@) {
        $self->log->trace($@);

        return;
    }

    $self->set_cache($bo_opts, $bo);

    return $bo;
}


=head2 generate_cache_key

A unique key is generated based on the fields intern, extern, type and id.

=cut

sub generate_cache_key {
    my ($self, $options) = @_;

    return join '', map { $options->{$_} || 0 } qw/intern extern type id/;
}



=head2 get_cache

Since two different sources are used for the cache, figure out
where to store objects in the given situation.

=cut

sub get_cache {
    my ($self, $bo_opts) = @_;

    ### XXX Disabled caching in betrokkene, because of memory leak. The reason for
    ### XXX this cache was to optimize speed of Documents, which is now solved
    ### XXX by getting betrokkene data from a different table.
    return;

    my $cache = $bo_opts->{stash} || $self->dbic && $self->dbic->cache;

    return $cache->{__Betrokkene_Cache} ||= {} if $cache;
}


=head2 set_cache

Store a Betrokkene object on a given cache. This can be either
a supplied stash parameter, or the cache on the schema.

=cut

sub set_cache {
    my ($self, $options, $object) = @_;

    if (my $cache = $self->get_cache($options)) {
        my $key = $self->generate_cache_key($options);
        $cache->{$key} = $object;
    }
}


=head2 _bo_from_cache

Find a Betrokkene object in the cache.

=cut

sub from_cache {
    my ($self, $options) = @_;

    if (my $cache = $self->get_cache($options)) {
        my $key = $self->generate_cache_key($options);

        return $cache->{$key};
    }

    return;
}


sub set {
    my ($self, $ident) = @_;
    my ($type, $id);

    return unless (($type, $id) = $ident =~ /betrokkene-(\w+)-(\d+)/);

    my $bclass = $self->_get_bclass($type);
    my $bobj = $bclass->new();
    my $identifier = eval { $bobj->set($self->_dispatch_options, $id); };

    if ($@) {
        $self->log->debug('M::B->set() DIE: ' . $@);
        return;
    }

    return $identifier;
}


sub create {
    my ($self, $type, $params) = @_;

    my $bclass = $self->_get_bclass($type);
    my $bobj = $bclass->new();
    my $identifier = eval { $bobj->create($self->_dispatch_options, $params); };

    if ($@) {
        $self->log->debug('M::B->create() DIE: ' . $@);
        return;
    }
    return $identifier;

}

=head2 _get_bclass

Get the correct subclass

=cut

sub _get_bclass {
    my ($self, $type) = @_;
    my $bclass = sprintf('%s::Object::%s', __PACKAGE__, $BETROKKENE_TYPES->{$type});
#    $self->log->debug($bclass);
    return $bclass;
}


=head2 get_by_string

avoid boilerplate

=cut

sub get_by_string {
    my ($self, $betrokkene_string) = @_;

    my ($betrokkene_type, $betrokkene_id) = $betrokkene_string =~ m|^betrokkene-(\w+)-(\d+)$|;

    die "incorrect betrokkene_string, we need betrokkene-type-id"
        unless $betrokkene_type && $betrokkene_id;

    return $self->get({ type => $betrokkene_type }, $betrokkene_id);
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 BOBJECT

TODO: Fix the POD

=cut

=head2 create

TODO: Fix the POD

=cut

=head2 from_cache

TODO: Fix the POD

=cut

=head2 search

TODO: Fix the POD

=cut

=head2 set

TODO: Fix the POD

=cut

