package Zaaksysteem::UserSettings;

use Moose;
use Zaaksysteem::Tools;
use Module::Load;

use constant INFLATABLES => {
    favourites  => 'Zaaksysteem::UserSettings::Favourites',
    lab         => 'Zaaksysteem::UserSettings::Labs',
};

=head1 NAME

Zaaksysteem::UserSettings - Prevent the use of anonymous hashing in Zaaksysteem, by using this object

=head1 SYNOPSIS

    my $subject_row = $s->resultset('Subject')->first;

    my $settings    = Zaaksysteem::UserSettings->new_from_settings($subject_row->settings);

    ### Unconverted hashes
    print $settings->{inavigator};
    # prints HASH(0x23a23)

    ### Converted hashes
    print $settings->favorite_saved_searches   # or print $settings->{favorite_saved_searches}
    # prints bless({ [...] }, Zaaksysteem::UserSettings::Favourites)

=head1 DESCRIPTION

Before we know it, we got a lot of settings in anonymous JSON hashes in our L<Zaaksysteem::DB::Subject> table.

This namespace makes sure hashed inflate to objects when defined, and keeps a backwards compatible fashion for
"unconverter" hashes

=head1 ATTRIBUTES

=head2 setting_keys

=cut

has 'setting_keys'  => (
    is          => 'ro',
    isa         => 'ArrayRef',
    default     => sub { return []; }
);

=head2 schema

DBIx::Class schema

=cut

has 'schema'    => (
    is          => 'rw',
    isa         => 'Zaaksysteem::Schema',
    required    => 1,
);

=head2 new_from_settings

Arguments: \%SETTINGS [, %optional_constructor_params ]

    my $settings = Zaaksysteem::UserSettings->new_from_settings(\%settings, schema => $schema);

Generates

=cut

sub new_from_settings {
    my $class       = shift;
    my $settings    = shift;
    my $self        = $class->new(@_);

    throw('usersettings/invalid_settings', 'Settings need to be of type "HASH"') unless ref $settings eq 'HASH';

    for my $key (keys %$settings) {
        push(@{ $self->setting_keys }, $key);

        if (INFLATABLES->{$key}) {
            $self->$key->_inflate_from_settings($settings->{$key});
            next;
        }

        $self->{$key} = $settings->{$key};
    }

    return $self;
}

=head2 to_settings 

Arguments: None

Return value: Hash of keys

=cut

sub to_settings {
    my $self        = shift;

    my $rv = {};

    ### Smart trick to pick unique keys
    for my $key (keys %{ { map({ $_ => 1 } (@{ $self->setting_keys }, keys %{ INFLATABLES() })) } }) {
        if ($self->can($key)) {
            $rv->{$key} = $self->$key->_deflate_to_settings;
        } else {
            $rv->{$key} = $self->{$key};
        }
    }

    return $rv;
}



=head1 PRIVATE METHODS

=head1 CLASS LOADING

Loads every inflatable from the constant C<inflatables>, and creates an accessor.

=cut

for my $inflatable ( keys %{ INFLATABLES() }) {
    Module::Load::load(INFLATABLES->{$inflatable});

    __PACKAGE__->meta->add_attribute(
        $inflatable,
        is      => 'ro',
        lazy    => 1,
        default => sub {
            my $self = shift;

            return INFLATABLES->{$inflatable}->new(schema => $self->schema);
        }
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
