package Zaaksysteem::DB::Profile;

use strict;

use base 'DBIx::Class::Storage::Statistics';

use Time::HiRes qw(time);
use Data::Dumper qw[Dumper];

my ($start, $queries) = (undef, []);

my %query_count;

sub query_start {
    my ($self, $sql, @params) = @_;

    $query_count{ $sql }++;

    if ($ENV{DBIC_TRACE}) {
        $self->print("Query : $sql\n");

        if(scalar @params) {
            $self->print("Params: " . join(', ', @params) . "\n");
        }

        $self->print("Seen  : " . ($query_count{ $sql } || 'never') . "\n");

        my $iter = 10;
        my $caller_package = "";
        my $caller_line;

        while (not $caller_package =~ m[^Zaaksysteem]) {
            $iter++;

            ($caller_package, $caller_line) = (caller $iter)[0, 2];
        }

        $self->print("Caller: $caller_package at $caller_line\n");
    }

    $start = time;

    return;
}

sub query_end {
    my $self = shift();
    my $sql = shift();
    my @params = @_;

    my $elapsed = sprintf("%0.4f", time() - $start);

    if ($ENV{DBIC_TRACE}) {
        $self->print("Time  : " . $elapsed . " seconds\n\n");
    }

    push @$queries, {
        took    => $elapsed,
        query   => $sql,
    };

    return;
}

sub reset_query_count {
    $queries = [];
    $start = 0;

    return;
}

sub queries {
    return $queries;
}

#sub interval {return sprintf("%0.4f", time() - $start); }

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 queries

TODO: Fix the POD

=cut

=head2 query_end

TODO: Fix the POD

=cut

=head2 query_start

TODO: Fix the POD

=cut

=head2 reset_query_count

TODO: Fix the POD

=cut
