package Zaaksysteem::DB::Component::BagOpenbareruimte;

use strict;
use warnings;

use base qw/Zaaksysteem::DB::Component::BagGeneral Zaaksysteem::Geo::BAG/;


=head2 geocode_term

Return value: $geocode_address

Generate a term for geocoding this location with a googlemaps or other
geocoder.

=cut

sub geocode_term {
    my $self    = shift;

    my @terms   = ('Nederland');

    if ($self->woonplaats) {
        push(@terms, $self->woonplaats->naam, $self->naam);
    }
    else {
        push(@terms, $self->naam);
    }

    return join(',', @terms);
}

sub to_string {
    my $self = shift;

    return sprintf(
        '%s, %s',
        $self->naam,
        $self->woonplaats->naam
    );
}

sub TO_JSON {
    my $self = shift;

    return {
        $self->get_columns,
        id => 'openbareruimte-' . $self->identificatie,
        type => 'street',
        streetname => $self->naam,
        city => $self->woonplaats->naam
    };
}

=head2 insert

Return value: $row

Extends L<DBIx::Class> component insert by retrieving coordinates for this BAG record

=cut

sub insert {
    my $self    = shift;

    my $row     = $self->next::method(@_);

    $row->load_gps_coordinates;

    return $row;
}


=head2 update

Return value: $row

Extends L<DBIx::Class> component update by retrieving coordinates for this BAG record

=cut

sub update {
    my $self    = shift;

    $self->load_gps_coordinates({ skip_update => 1});

    $self->next::method(@_);

    return $self;
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 TO_JSON

TODO: Fix the POD

=cut

=head2 to_string

TODO: Fix the POD

=cut

