package Zaaksysteem::DB::Component::BibliotheekKenmerken;

use List::MoreUtils qw/all/;
use Moose;
use Moose::Util qw/apply_all_roles/;
use Data::Dumper;

use Zaaksysteem::Constants qw/ZAAKSYSTEEM_CONSTANTS/;
use Zaaksysteem::Exception;
use Zaaksysteem::Profile;

extends 'Zaaksysteem::Backend::Component';


=head2 apply_roles

To format fields different roles can be applied, as a plugin system.
Currently only qmatic is implemented.

=cut

sub apply_roles {
    my ($self) = @_;

    if ($self->value_type eq 'calendar') {
        apply_all_roles($self, __PACKAGE__ . '::Qmatic');
    }

    return $self;
}


=head2 options

Fields with multiple possible values (checkbox, radio, select box, custom) can have entries
in bibliotheek_kenmerken_values.

Return a list with these values, including all the attribute per value, e.g. disabled.

=cut

has 'options' => (
    'is'        => 'rw',
    'isa'       => 'ArrayRef',
    'lazy'      => 1,
    'default'   => sub {
        my $self = shift;

        throw('kenmerken/value_type/missing', 'Een kenmerk zonder type is niet toegestaan')
            unless $self->value_type;

        return [] unless ZAAKSYSTEEM_CONSTANTS->{veld_opties}->{$self->value_type}->{multiple};

        my $rs = $self->bibliotheek_kenmerken_values;

        return [
            map {{
                id      => $_->id,
                value   => $_->value,
                active  => $_->active,
                sort_order   => $_->sort_order
            }}
            $rs->search({}, {order_by => { -asc => 'sort_order' } })->all
        ];
    }
);


define_profile save_options => (
    required => [qw/options/],
    optional => [qw/reason/],
    typed => {
        reason => 'Str',
    },
);
sub save_options {
    my $self = shift;
    my $params = assert_profile(shift)->valid;
    my $options = $params->{options};

    # i tried to get this into the profile, no luck. good example anyone?
    throw('kenmerken/input/invalid', 'Een optie zonder waarde is niet toegestaan')
        unless ref $options && ref $options eq 'ARRAY' && all { defined $_->{value}} @$options;

    my $reason = $params->{reason};

    my $rs = $self->bibliotheek_kenmerken_values;

    $self->result_source->schema->txn_do(sub {
        my $sort_order = 0;

        $rs->delete;

        foreach my $option (@$options) {

            $rs->create({
                bibliotheek_kenmerken_id => $self->id,
                value       => $option->{value},
                sort_order  => $sort_order,
                active      => $option->{active}
            });

            $sort_order += 1;
        }

        $self->result_source->schema->resultset('Logging')->trigger('attribute/update', {
            component => 'kenmerk',
            component_id => $self->id,
            data => {
                attribute_id => $self->id,
                reason => $reason || '',
                # upon create, the default value of 1 is not set in the retrieved model
                version => $self->version || 1,
                options => $options
            }
        });

    });
}



sub can_have_multiple_values {
    my $self = shift;

    return ZAAKSYSTEEM_CONSTANTS->{ veld_opties }{ $self->value_type }{ multiple } || $self->type_multiple;
}


sub insert {
    my ($self) = @_;

    # we need the database to generate the new searchable_id param.
    # if we don't supply it, it will take its chance. so get rid
    # of whatever the app thinks it should pass.
    # is there a way to put this in the Schema? that be less hacky.
    delete $self->{_column_data}->{searchable_id};

    $self->_set_search_term();

    return $self->next::method(@_);
}

sub update {
    my ($self) = shift;

    $self->_set_search_term();


    return $self->next::method(@_);
}

sub bump_version {
    my $self = shift;

    my $version = $self->version || 0; # prevent undef warnings

    $self->version($version + 1);

    $self->update;
}

sub _set_search_term {
    my ($self) = @_;
    my $search_term = '';
    if($self->naam) {
        $search_term .= $self->naam;
    }
    $self->search_term($search_term);
}



sub uses_options {
    my ($self) = @_;

    return $self->value_type =~ m/^(option|checkbox|select)$/;
}

=head2 Attributes for L<Zaaksysteem::Backend::Component::Searchable>

=cut

has '_searchable_object_id' => (
    is      => 'ro',
    lazy    => 1,
    default => sub { return shift->id; }
);


has '_searchable_object_label' => (
    is      => 'ro',
    lazy    => 1,
    default => sub { return shift->naam; }
);


has '_searchable_object_description' => (
    is      => 'ro',
    lazy    => 1,
    default => sub { return shift->naam; }
);

=head2 JSON Extension for L<Zaaksysteem::Backend::Component>

=cut


before 'TO_JSON' => sub {
    my $self                    = shift;

    $self->_json_data(
        {
            %{ $self->_json_data },
            $self->get_columns,
        }
    );
};


=head2 format_as_string

This component is most equipped to format a value, since it knows
its settings. The values come from elsewhere, doesn't matter, all
we need to know here is what kind of field we are and then spit
out some representation.

=cut

sub format_as_string {
    my ($self, $values) = @_;

    return $values ? @$values : ();
}


sub reject_pip_change_request {
    # nop just here to prevent errors
}

=head2 filter

The different bibliotheek_kenmerken value_type have filters
that define their range. This applies the filter. The purpose
of these filters is to enforce strict types.

    my $bibliotheek_kenmerk = $c->model('DB::BibliotheekKenmerken')->find($id);
    my $value = 'abcd';

    my $filtered = $bibliotheek_kenmerk->filter($value);

Input can be a scalar or arrayref. For the latter, the filter will be
applied to every element in the array.

=cut

sub filter {
    my ($self, $values) = @_;

    my $filter_function = ZAAKSYSTEEM_CONSTANTS
        ->{veld_opties}{ $self->value_type }{object_search_filter}
            or return $values;

    my $schema = $self->result_source->schema;

    return ref $values eq 'ARRAY' ?
        [ map { $filter_function->($schema, $_) } @$values ] :
        $filter_function->($schema, $values);
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 ZAAKSYSTEEM_CONSTANTS

TODO: Fix the POD

=cut

=head2 bump_version

TODO: Fix the POD

=cut

=head2 can_have_multiple_values

TODO: Fix the POD

=cut

=head2 insert

TODO: Fix the POD

=cut

=head2 reject_pip_change_request

TODO: Fix the POD

=cut

=head2 save_options

TODO: Fix the POD

=cut

=head2 update

TODO: Fix the POD

=cut

=head2 uses_options

TODO: Fix the POD

=cut

