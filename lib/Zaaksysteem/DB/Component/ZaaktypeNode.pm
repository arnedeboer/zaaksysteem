package Zaaksysteem::DB::Component::ZaaktypeNode;
use Moose;

BEGIN { extends 'DBIx::Class::Row'; }

use Zaaksysteem::Tools;

sub is_huidige_versie {
    my $self    = shift;

    if (
        $self->zaaktype_id &&
        $self->zaaktype_id->zaaktype_node_id->id eq $self->id
    ) {
        return 1;
    }

    return;
}

=head2 find_casetype_result

    $casetype_node->find_casetype_result(
        result => 'Foo',  # required
        label  => 'Bar',  # optional
    );

Find a result for a casetype based on the result type and/or label.
If the label is supplied it will try to get an exact match, if it doesn't
exist it will fall back to the first result with the matching result type.

=cut

define_profile find_casetype_result => (
    required => {
        result => 'Str',
    },
    optional => {
        label => 'Str',
    },
);

sub find_casetype_result {
    my $self = shift;
    my $opt = assert_profile({@_})->valid;

    if ($opt->{label}) {
        my $resultaat = $self->zaaktype_resultaten->search(
            {
                resultaat => $opt->{result},
                label     => $opt->{label},
            }
        )->first;
        return $resultaat if $resultaat;
    }

    # The label may have changed, fall back to whatever has t
    my $resultaat = $self->zaaktype_resultaten->search(
        { resultaat => $opt->{result} },
        {
            order_by => { -asc => 'id' },
            rows     => 1,
        }
    )->first;
    return $resultaat;
}

sub url_title {
    my ($self, $c)  = @_;

    my $title = lc($self->titel);

    $title =~ s/^\s+|\s+$//g;   ## Trim whitespace from front and back
    $title =~ s/\s+/-/;         ## Replace whitespace with dash

    return $title;
}

define_profile rules => (
    optional => ['status'],
    constraint_methods => {
        status => qr/^\d+$/
    }
);

sub rules {
    my ($self, $args) = @_;

    assert_profile($args);

    my $rules_rs = $self->zaaktype_regels->search(
        {
            $args->{status} ? ('zaak_status_id.status' => $args->{status}) : (),
            '-or'  => [
                { 'is_group' => undef },
                { 'is_group' => { '!=' => 1 } },
            ],
            'active'    => 1,
        },
        {
            order_by => 'me.id',
            prefetch => 'zaak_status_id'
        }
    );

    return $rules_rs;
}

sub get_steps {
    my $self = shift;
    my $stepnumber = shift;

    my $stati = $self->zaaktype_statuses->search({}, {
        order_by => { -asc => 'id' }
    });

    if($stepnumber) {
        $stati = $stati->search({ status => $stepnumber });
    }

    return map { $_->get_steps } $stati->all;
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 get_steps

TODO: Fix the POD

=cut

=head2 is_huidige_versie

TODO: Fix the POD

=cut

=head2 rules

TODO: Fix the POD

=cut

=head2 url_title

TODO: Fix the POD

=cut

