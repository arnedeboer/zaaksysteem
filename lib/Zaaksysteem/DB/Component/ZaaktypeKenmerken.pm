package Zaaksysteem::DB::Component::ZaaktypeKenmerken;

use strict;
use warnings;
use Data::Dumper;
use JSON;
use Zaaksysteem::Constants qw/ZAAKSYSTEEM_CONSTANTS/;

#use Moose;

use base qw/DBIx::Class/;

sub added_columns {
    return [qw/
        naam
        type
        options
    /];
}

sub options {
    my ($self) = @_;

    if ($self->bibliotheek_kenmerken_id) {
        return $self->bibliotheek_kenmerken_id->options;
    }
}

sub naam {
    my $self    = shift;

    if ($self->bibliotheek_kenmerken_id) {
        return $self->bibliotheek_kenmerken_id->naam;
    }
}

sub type {
    my $self    = shift;

    if ($self->bibliotheek_kenmerken_id) {
        return $self->bibliotheek_kenmerken_id->value_type;
    }
    elsif ($self->object_id) {
        return "objecttype";
    }
}

sub label {
    my $self    = shift;

    ### Make sure we write
    return $self->next::method(@_) if @_;

    ### Show label when exists
    return $self->next::method
        if $self->next::method;

    if ($self->bibliotheek_kenmerken_id) {
        return $self->bibliotheek_kenmerken_id->label;
    }
}

sub magic_string {
    my $self    = shift;

    if ($self->bibliotheek_kenmerken_id) {
        return $self->bibliotheek_kenmerken_id->magic_string;
    }
}

sub description {
    my $self    = shift;

    if ($self->bibliotheek_kenmerken_id) {
        return $self->bibliotheek_kenmerken_id->description;
    }
}

sub speciaal_kenmerk {
    my $self    = shift;

    if ($self->bibliotheek_kenmerken_id) {
        return $self->bibliotheek_kenmerken_id->speciaal_kenmerk;
    }
}

sub help {
    my $self    = shift;

    ### Make sure we write
    return $self->next::method(@_) if @_;

    ### Show label when exists
    return $self->next::method
        if $self->next::method;

    if ($self->bibliotheek_kenmerken_id) {
        return $self->bibliotheek_kenmerken_id->help;
    }
}

sub kenmerken_categorie {
    my $self    = shift;

    if ($self->bibliotheek_kenmerken_id) {
        return $self->bibliotheek_kenmerken_id->document_categorie;
    }

    return;
}

sub rtkey {
    my $self    = shift;

    return unless (
        $self->bibliotheek_kenmerken_id &&
        $self->bibliotheek_kenmerken_id->id
    );

    return 'kenmerk_id_' . $self->bibliotheek_kenmerken_id->id;
}

sub verplicht {
    my $self    = shift;

    return 1 if $self->value_mandatory;

    return;
}


sub default_value {
    my $self = shift;

    # If there is an update to the column, we'll let the original accessor
    # deal with it.
    return $self->value_default(@_) if @_;

    # Fetch the column value.
    my $value_default = $self->value_default;

    # If there's something in the description field, then just return that.
    return $value_default if defined $value_default && length $value_default;

    # Otherwise, get the default value from the bibliotheek_kenmerken table
    my $bib_row = $self->bibliotheek_kenmerken_id();

    if (defined $bib_row) {
       return $bib_row->value_default;
    }
}

=head2 required_permissions_decoded

Stored as json, and edited by the frontend without needing decoding. To suit
the backoffice code, default behaviour is not decode/encode.

This is an accessor for backend code that will actually use the information.

=cut

sub required_permissions_decoded {
    my ($self) = @_;

    # handle empty which is legacy - which will be around for a long time
    my $required = $self->required_permissions ? JSON::decode_json($self->required_permissions) : {};

    # legacy code - only necessary in dev environments, but never hurts.
    return [] unless $required && ref $required && ref $required eq 'HASH';

    return $required->{selectedUnits} || [];
}

sub TO_JSON {
    my $self            = shift;

    my $cols            = { $self->get_columns };

    $cols->{bibliotheek_kenmerken_id}   = { $self->bibliotheek_kenmerken_id->get_columns };

    $cols->{zaaktype_node_id}           = { $self->zaaktype_node_id->get_columns };

    return $cols;
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 TO_JSON

TODO: Fix the POD

=cut

=head2 added_columns

TODO: Fix the POD

=cut

=head2 default_value

TODO: Fix the POD

=cut

=head2 description

TODO: Fix the POD

=cut

=head2 help

TODO: Fix the POD

=cut

=head2 kenmerken_categorie

TODO: Fix the POD

=cut

=head2 label

TODO: Fix the POD

=cut

=head2 magic_string

TODO: Fix the POD

=cut

=head2 naam

TODO: Fix the POD

=cut

=head2 options

TODO: Fix the POD

=cut

=head2 rtkey

TODO: Fix the POD

=cut

=head2 speciaal_kenmerk

TODO: Fix the POD

=cut

=head2 type

TODO: Fix the POD

=cut

=head2 verplicht

TODO: Fix the POD

=cut

