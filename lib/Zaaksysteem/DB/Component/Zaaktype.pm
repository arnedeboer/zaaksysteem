package Zaaksysteem::DB::Component::Zaaktype;
use Moose;

use DateTime;

use Zaaksysteem::Tools;
use Zaaksysteem::Constants qw[ZAAKSYSTEEM_CONSTANTS];
use Zaaksysteem::Types qw(UUID);

use Zaaksysteem::Object::Types::Casetype;
use Zaaksysteem::Object::Types::CasetypePhase;
use Zaaksysteem::Object::Types::CasetypeResult;
use Zaaksysteem::Object::Types::Group;
use Zaaksysteem::Object::Types::Role;

use Zaaksysteem::API::v1::Serializer;

extends 'Zaaksysteem::Backend::Component';

has uuid => (
    is      => 'ro',
    isa     => UUID,
    lazy    => 1,
    default => sub {
        my $self = shift;
        return $self->_object->uuid;
    }
);

sub insert {
    my $self    = shift;

    # we need the database to generate the new searchable_id param.
    # if we don't supply it, it will take it's chance. so get rid
    # of whatever the app thinks it should pass.
    # is there a way to put this in the Schema? that's be less hacky.
    delete $self->{_column_data}->{searchable_id};

    $self->_set_search_string({insert => 1});

    return $self->next::method(@_);
}

sub update {
    my $self    = shift;
    my $columns = shift;

    $self->_set_search_string;

    return $self->next::method(@_);
}

sub title {
    my $self = shift;

    if($self->zaaktype_node_id) {
        return $self->zaaktype_node_id->titel;
    }

    return $self->id;
}

=head2 delete

Remove the zaaktype by setting its "deleted" date and try to remove it for real.

=cut

sub delete {
    my $self = shift;

    $self->set_delete;
    $self->next::method(@_);
}

=head2 set_deleted

Remove the zaaktype by setting its "deleted" date.

=cut

sub set_deleted {
    my $self = shift;

    $self->assert_delete;
    $self->deleted(DateTime->now());
    $self->update();
    return 1;
}

=head2 assert_delete

Arguments: none

Return value: $TRUE_ON_SUCCESS otherwise exception

    $zt->assert_delete()

Checks whether it is allowed to remove this casetype. True on success, exception when not possible

=cut

sub assert_delete {
    my $self = shift;

    my $zaak = $self->result_source->schema->resultset('Zaak')
        ->search_rs({ zaaktype_id => $self->id, deleted => undef },
            { limit => 1 })->first;

    if ($zaak) {
        throw('zs/casetype/delete/nen',
            'Unable to delete case type, cases still depend on me');
    }
    return 1;
}

sub _set_search_string {
    my ($self) = @_;

    my $search_string = ($self->id || '') . ' ';

    if($self->zaaktype_node_id) {

        if($self->zaaktype_node_id->titel) {
            $search_string .= $self->zaaktype_node_id->titel;
        }

        if($self->zaaktype_node_id->zaaktype_trefwoorden) {
            $search_string .= ' ' . $self->zaaktype_node_id->zaaktype_trefwoorden;
        }

        if($self->zaaktype_node_id->zaaktype_omschrijving) {
            $search_string .= ' ' . $self->zaaktype_node_id->zaaktype_omschrijving;
        }
    }

    $self->search_term($search_string);
    $self->search_order($search_string);
}

=head2 Attributes for L<Zaaksysteem::Backend::Component::Searchable>

=cut

has '_searchable_object_id' => (
    is      => 'ro',
    lazy    => 1,
    default => sub { return shift->id; }
);


has '_searchable_object_label' => (
    is      => 'ro',
    lazy    => 1,
    default => sub { return shift->zaaktype_node_id->titel; }
);


has '_searchable_object_description' => (
    is      => 'ro',
    lazy    => 1,
    default => sub { return shift->zaaktype_node_id->zaaktype_omschrijving; }
);

=head2 _object

Retrieves the ObjectData object for this Zaaktype.

Optionally passes thru an owner L<Zaaksysteem::Object::SecurityIdentity> to
L<Zaaksysteem::Backend::Object::Data::ResultSet/find_or_create_by_object_id>.

=cut

sub _object {
    my $self = shift;
    my $owner = shift;

    my $rs = $self->result_source->schema->resultset('ObjectData');

    return $rs->find_or_create_by_object_id(
        'casetype',
        $self->id,
        $owner
    );
}

=head2 _sync_object

Synchronizes the row and related rows to L<Zaaksysteem::Object> instances.

=cut

sig _sync_object => 'Zaaksysteem::Object::Model';

sub _sync_object {
    my $self = shift;
    my $model = shift;

    my $security_identity;

    if ($model->has_user) {
        my ($type, $id) = $model->user->security_identity;

        $security_identity = Zaaksysteem::Object::SecurityIdentity->new(
            entity_type => $type,
            entity_id => $id
        );
    }

    my $object = $self->_object($security_identity);

    my %map = (
        zaak_beheer => 'manage',
        zaak_edit => 'write',
        zaak_read => 'read',
        zaak_search => 'search'
    );

    $self->result_source->schema->txn_do(sub {
        if ($self->deleted) {
            my $uuid  = $object->uuid;
            my $count = $self->result_source->schema->resultset('ObjectData')
                ->search_rs({ class_uuid => $uuid })->count;

            # Some kind of foreign key constraint which buggers us.
            if ($count == 0) {
                $object->delete;
            }
            else {
                # warn because we want to let our testsuite know it is not good,
                # but we want the application to progress: a soft fail if you
                # will
                warn sprintf(
                    "Unable to delete object with UUID %s, multiple objects (%d) still depend on it\n",
                    $uuid, $count,);
            }
            return;
        }

        my $casetype = $self->build_casetype_object;

        $casetype->id($object->uuid);

        $object = $model->update_object_data($casetype);

        $model->refresh_object_relations($casetype, $object);

        $object->object_acl_entries->delete_all;

        for my $auth ($self->zaaktype_authorisations) {
            next unless $auth->recht && exists $map{ $auth->recht };

            $object->grant($auth,
                groupname => $auth->confidential ? 'confidential' : 'public',
                scope => 'type',
                capabilities => [ $map{ $auth->recht } ]
            );
        }

        $object->grant({ tag => 'all_users' },
            scope => 'instance',
            capabilities => [qw[read]]
        );
    });

    return $object;
}

=head2 build_casetype_object

Builds a L<Zaaksysteem::Object::Types::Casetype> object instance.

=cut

define_profile build_casetype_object => (
    optional => {
        zaaktype_node_id => 'Int',
        version => 'Int'
    }
);

sub build_casetype_object {
    my ($self, %params) = @_;

    my $opts = assert_profile(\%params)->valid;

    my $serializer = Zaaksysteem::API::v1::Serializer->new(
        has_full_access => 1
    );

    my $node;
    my $nodes = $self->zaaktype_nodes;

    if (exists $opts->{ zaaktype_node_id }) {
        $node = $nodes->find($opts->{ zaaktype_node_id });
    } elsif (exists $opts->{ version }) {
        $node = $nodes->search({ version => $opts->{ version } })->first;
    } else {
        $node = $self->zaaktype_node_id;
    }

    return unless defined $node;

    my $definition = $node->zaaktype_definitie_id;
    my $properties = $node->properties;

    return unless defined $definition && defined $properties;

    my %casetype_args = (
        casetype_id => $self->id,
        casetype_node_id => $node->id,
        offline => $self->active ? 0 : 1,
        name => $self->title,
        public_url_path => { $self->public_uri_paths },

        identification => $node->code,
        version => $node->version,
        keywords => $node->zaaktype_trefwoorden,
        description => $node->zaaktype_omschrijving,
        trigger => $node->trigger,
        subject_types => [
            $node->zaaktype_betrokkenen->search->get_column('betrokkene_type')->all
        ],

        public_registration_form => $node->webform_toegang ? 1 : 0,
        online_payment           => $node->online_betaling ? 1 : 0,
        allow_take_on_create     => $node->automatisch_behandelen ? 1 : 0,
        allow_assign_on_create   => $node->toewijzing_zaakintake ? 1 : 0,
        allow_subjects_on_create => $node->extra_relaties_in_aanvraag ? 1 : 0,
        show_confidentiality_on_create => $properties->{confidentiality} ? 1 : 0,

        contact_info_email_required => $node->contact_info_email_required ? 1 : 0,
        contact_info_phone_required => $node->contact_info_phone_required ? 1 : 0,
        contact_info_mobile_phone_required => $node->contact_info_mobile_phone_required ? 1 : 0,

        # "Contactgegevens tonen bij intake"
        intake_show_contact_info => $node->contact_info_intake ? 1 : 0,

        # zaaktype_node has a "negated" boolean
        visible_on_pip           => $node->prevent_pip ? 0 : 1,

        initiator_action => $definition->handelingsinitiator,
        legal_basis => $definition->grondslag,
        process_description => $definition->procesbeschrijving,
        selection_list => $definition->selectielijst,
        sources => ZAAKSYSTEEM_CONSTANTS->{ contactkanalen },
        lead_time_service => {
            $definition->afhandeltermijn_type => $definition->afhandeltermijn
        },
        lead_time_legal => {
            $definition->servicenorm_type => $definition->servicenorm
        },

        extendable => ($properties->{ verlenging_mogelijk } || '') eq 'Ja' ? 1 : 0,
        lex_silencio_positivo => ($properties->{ lex_silencio_positivo } || '') eq 'Ja' ? 1 : 0,
        local_basis => $properties->{ lokale_grondslag },
        supervisor_relation => $properties->{ verantwoordingsrelatie },
        publication_text => $properties->{ publicatietekst },
        suspendable => ($properties->{ opschorten_mogelijk } || '') eq 'Ja' ? 1 : 0,
        appealable => ($properties->{ beroep_mogelijk } || '') eq 'Ja' ? 1 : 0,
        publishable => ($properties->{ publicatie } || '') eq 'Ja' ? 1 : 0,
        confidentiality => $properties->{ vertrouwelijkheidsaanduiding },
        wkpb => ($properties->{ wkpb } || '') eq 'Ja' ? 1 : 0,
        purpose => $properties->{ doel },
        records_classification => $properties->{ archiefclassificatiecode },
        eform => $properties->{ e_formulier },
        supervisor => $properties->{ verantwoordelijke },
        motivation => $properties->{ aanleiding },
        penalty => $properties->{ wet_dwangsom },
        lock_registration_phase => $properties->{ lock_registration_phase } ? 1 : 0,
        queue_coworker_changes => $properties->{ queue_coworker_changes } ? 1 : 0,
    );

    my $preset_subject = $definition->preset_subject;

    if ($preset_subject) {
        $casetype_args{ preset_client } = $preset_subject;
    }

    if (defined $properties->{ verlengingstermijn }) {
        $casetype_args{ extension_period } = int($properties->{ verlengingstermijn } || 0);
    }

    if (defined $properties->{ verdagingstermijn }) {
        $casetype_args{ adjournment_period } = int($properties->{ verdagingstermijn } || 0);
    }

    delete $casetype_args{ $_ } for grep {
        not defined $casetype_args{ $_ }
    } keys %casetype_args;

    my $casetype = Zaaksysteem::Object::Types::Casetype->new(%casetype_args);

    my @phases;
    my @results;

    my $zaaktype_statuses = $node->zaaktype_statuses->search(undef, {
        order_by => { -asc => 'status' }
    });

    for my $status ($zaaktype_statuses->all) {
        my $zaaktype_kenmerken = $status->zaaktype_kenmerken->search(undef, {
            order_by => 'me.id',
            prefetch => {
                bibliotheek_kenmerken_id => 'bibliotheek_kenmerken_values'
            }
        });

        my %route = (
            set => $status->role_set ? JSON::true : JSON::false,
            group => undef,
            role => undef,
        );

        if ($status->ou_id) {
            my $group = $status->result_source->schema->resultset('Groups')->find($status->ou_id);

            if ($group) {
                $route{ group } = $group->object;
            }
        }

        if ($status->role_id) {
            my $role = $status->result_source->schema->resultset('Roles')->find($status->role_id);

            if ($role) {
                $route{ role } = $role->object;
            }
        }

        push @phases, Zaaksysteem::Object::Types::CasetypePhase->new(
            casetype_phase_id => $status->id,
            name => $status->naam,
            label => $status->fase,
            sequence => $status->status,
            attributes => [ map { $serializer->read($_) } $zaaktype_kenmerken->all ],
            route => \%route,
        );
    }

    my $zaaktype_resultaten = $node->zaaktype_resultatens->search(undef, {
        order_by => { -asc => 'id' }
    });

    while (my $result = $zaaktype_resultaten->next) {
        my %result_args = (
            casetype_result_id => $result->id,
            label => $result->label,
            generic_result_type => $result->resultaat,
            retention_period => $result->bewaartermijn,
            retention_period_source_date => $result->ingang,
            trigger_archival => $result->trigger_archival,
            archival_type => $result->archiefnominatie,
            dossier_type => $result->dossiertype,
            selection_list => $result->selectielijst,
            # Broken, waiting on ZS-11457
            #selection_list_start_date => $result->selectielijst_brondatum,
            #selection_list_end_date => $result->selectielijst_einddatum,
            external_reference => $result->external_reference,
            explanation => $result->comments
        );

        delete $result_args{ $_ } for grep {
            not defined $result_args{ $_ }
        } keys %result_args;

        push @results, Zaaksysteem::Object::Types::CasetypeResult->new(
            %result_args
        );
    }

    $casetype->instance_phases(\@phases);
    $casetype->instance_results(\@results);

    return $casetype;
}

=head2 JSON Extension for L<Zaaksysteem::Backend::Component>

=cut

before TO_JSON => sub {
    my $self = shift;
    my $node = $self->zaaktype_node_id;

    # Nastiness, completely rewriting the JSON data content for an object in
    # a method modifier that won't show up in a stacktrace...
    # Also, simply dumping all db columns? Seems overkill
    $self->_json_data({
        %{ $self->_json_data },
        $self->get_columns,
    });

    # Again with the column dump!
    $self->_json_data->{ zaaktype_node_id } = { $node->get_columns };
    $self->_json_data->{ public_url } = { $self->public_uri_paths };
};

=head2 public_uri_paths

=cut

sub public_uri_paths {
    my $self = shift;
    my $node = $self->zaaktype_node_id;
    my @retval;

    return unless defined $node;
    return if $node->trigger eq 'intern';

    my $format = sprintf(
        '/aanvragen/%d/%%s/%s',
        $self->id,
        $self->seo_friendly_title
    );

    my %map = (
        # $dbname => { $json_key => $path_part }
        natuurlijk_persoon         => { person       => 'persoon' },
        natuurlijk_persoon_na      => { person       => 'persoon' },
        niet_natuurlijk_persoon    => { organisation => 'organisatie' },
        niet_natuurlijk_persoon_na => { organisation => 'organisatie' },
        preset_client              => { unknown      => 'onbekend' }
    );

    my @subjects = map { $_->betrokkene_type } $node->zaaktype_betrokkenen->all;

    for my $subject (@subjects) {
        my $json_keys = $map{ $subject };
        next unless defined $json_keys;

        push @retval, map {
            $_ => sprintf($format, $json_keys->{ $_ })
        } keys %{ $json_keys };
    }

    # Return list instead of explicit map, since technically we can have
    # doubled keys (by virtue of conflating (niet_)natuurlijk_persoon(_na)
    # subjecttypes.
    return @retval;
}

=head2 seo_friendly_title

Format the title with only a-z, 0-9 and substitute a dash for anything else

=cut

sub seo_friendly_title {
    my $self = shift;
    my $title = lc $self->zaaktype_node_id->titel;

    $title =~ s/[^a-z0-9]+/-/gi;

    return $title;
}

=head2 set_active_node

Every zaaktype links to one zaaktype_node record. This system is used to allow multiple versions
of the zaaktype to exist. This method changes the active zaaktype_node. Every other node
must be disabled.

    my $zaaktype = $c->model('DB::Zaaktype')->find($zaaktype_id);

    $zaaktype->set_active_node($zaaktype_node_id);

=cut

sub set_active_node {
    my ($self, $zaaktype_node_id) = @_;

    die "need zaaktype_node_id" unless $zaaktype_node_id;

    my $schema = $self->result_source->schema;

    my $zaaktype_node_rs = $schema->resultset('ZaaktypeNode');
    die "new zaaktype_node_id does not exist" unless $zaaktype_node_rs->find($zaaktype_node_id);

    eval {
        $schema->txn_do(sub {
            # set currently node to deleted
            $self->zaaktype_node_id->deleted(DateTime->now());
            $self->zaaktype_node_id->update();

            $self->zaaktype_node_id($zaaktype_node_id);
            $self->update();

            # le roi est mort, vive le roi!
            $self->zaaktype_node_id->deleted(undef);
            $self->zaaktype_node_id->update();
        });
    };

    if ($@) {
        die "could update version: $@";
    }
}


=head2 attributes_by_magic_string

Get a lookup structure of the custom attributes in this casetype.
Say we have a text field with magic string [[my_text]] and
a number field [[my_number]], something like this will be returned:

    {
        my_text   => bless { value_type => 'text', id => 123, label => 'My text' },
        my_number => bless { value_type => 'number', id => 124, label => 'My number'}
    }

=cut

sub attributes_by_magic_string {
    my $self = shift;

    my @bibliotheek_kenmerken = map {
        $_->bibliotheek_kenmerken_id
    } $self->zaaktype_node_id->zaaktype_kenmerken->search({
        bibliotheek_kenmerken_id => {
            -not => undef
        }
    })->all;

    return {
        map { $_->magic_string => $_ } @bibliotheek_kenmerken
    };

}

1; #__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 insert

TODO: Fix the POD

=cut

=head2 title

TODO: Fix the POD

=cut

=head2 update

TODO: Fix the POD

=cut

