package Zaaksysteem::DB::Component::Zaaktype;

use strict;
use warnings;

use Moose;

use Data::Dumper;
use DateTime;

extends 'Zaaksysteem::Backend::Component';

sub insert {
    my $self    = shift;

    # we need the database to generate the new searchable_id param.
    # if we don't supply it, it will take it's chance. so get rid
    # of whatever the app thinks it should pass.
    # is there a way to put this in the Schema? that's be less hacky.
    delete $self->{_column_data}->{searchable_id};

    $self->_set_search_string({insert => 1});
    my $ret = $self->next::method(@_);

    $self->_sync_object;

    return $ret;
}

sub update {
    my $self    = shift;
    my $columns = shift;

    $self->_set_search_string;
    my $ret = $self->next::method(@_);

    $self->_sync_object;

    return $ret;
}

sub title {
    my $self = shift;

    if($self->zaaktype_node_id) {
        return $self->zaaktype_node_id->titel;
    }

    return $self->id;
}

=head2 delete

Remove the zaaktype by setting its "deleted" date.

=cut

sub delete {
    my $self = shift;

    $self->deleted(DateTime->now());
    $self->_sync_object;
    $self->next::method(@_);

}

sub _set_search_string {
    my ($self) = @_;

    my $search_string = ($self->id || '') . ' ';

    if($self->zaaktype_node_id) {

        if($self->zaaktype_node_id->titel) {
            $search_string .= $self->zaaktype_node_id->titel;
        }

        if($self->zaaktype_node_id->zaaktype_trefwoorden) {
            $search_string .= ' ' . $self->zaaktype_node_id->zaaktype_trefwoorden;
        }

        if($self->zaaktype_node_id->zaaktype_omschrijving) {
            $search_string .= ' ' . $self->zaaktype_node_id->zaaktype_omschrijving;
        }
    }

    $self->search_term($search_string);
}

=head2 Attributes for L<Zaaksysteem::Backend::Component::Searchable>

=cut

has '_searchable_object_id' => (
    is      => 'ro',
    lazy    => 1,
    default => sub { return shift->id; }
);


has '_searchable_object_label' => (
    is      => 'ro',
    lazy    => 1,
    default => sub { return shift->zaaktype_node_id->titel; }
);


has '_searchable_object_description' => (
    is      => 'ro',
    lazy    => 1,
    default => sub { return shift->zaaktype_node_id->zaaktype_omschrijving; }
);

=head2 _object

Retrieves the ObjectData object for this Zaaktype.

=cut

sub _object {
    my $self = shift;

    return $self->result_source->schema->resultset('ObjectData')->find_or_create_by_object_id(
        'casetype', $self->id
    );
}

=head2 _sync_object

=cut

sub _sync_object {
    my $self = shift;

    my $object = $self->_object;

    my %map = (
        zaak_edit => 'write',
        zaak_read => 'read'
    );

    $self->result_source->schema->txn_do(sub {
        $object->object_acl_entries->delete_all;

        if ($self->deleted) {
            my $uuid  = $object->uuid;
            my $count = $self->result_source->schema->resultset('ObjectData')
                ->search_rs({ class_uuid => $uuid })->count;

            # Some kind of foreign key constraint which buggers us.
            if ($count == 0) {
                $object->delete;
            }
            else {
                # warn because we want to let our testsuite know it is not good,
                # but we want the application to progress: a soft fail if you
                # will
                warn sprintf(
                    "Unable to delete object with UUID %s, multiple objects (%d) still depend on it\n",
                    $uuid, $count,);
            }
            return;
        }

        for my $auth ($self->zaaktype_authorisations) {
            next unless $auth->recht && exists $map{ $auth->recht };

            $object->grant($auth,
                groupname => $auth->confidential ? 'confidential' : 'public',
                scope => 'type',
                capabilities => [ $map{ $auth->recht } ]
            );
        }

        # XXX TODO Hardcoded nastiness, this implies the 'Behandelaar' role,
        # but will probably break a release after LDAP migration has landed
        # when IDs start diverging. We *should* have something in the
        # user/group model that returns a security identity every user falls
        # under.
        $object->grant({ role => '20002' },
            scope => 'instance',
            capabilities => [qw[read]]
        );

        my @hardcoded_attributes = (
            Zaaksysteem::Object::Attribute->new(
                name           => 'casetype.id',
                label          => 'Zaaktypenummer',
                attribute_type => 'integer',
                value          => $self->id,
            ),

            Zaaksysteem::Object::Attribute->new(
                name           => 'casetype.name',
                label          => 'Zaaktypetitel',
                attribute_type => 'text',
                value          => $self->title,
            ),

            Zaaksysteem::Object::Attribute->new(
                name           => 'casetype.public_url_path',
                label          => 'Public URL paths',
                attribute_type => 'text', # yeah.... it's not :(
                value          => { $self->public_uri_paths }
            )
        );

        $object->replace_object_attributes(@hardcoded_attributes);
        $object->update();
    });

    return $object;
}

=head2 JSON Extension for L<Zaaksysteem::Backend::Component>

=cut

before TO_JSON => sub {
    my $self = shift;
    my $node = $self->zaaktype_node_id;

    # Nastiness, completely rewriting the JSON data content for an object in
    # a method modifier that won't show up in a stacktrace...
    # Also, simply dumping all db columns? Seems overkill
    $self->_json_data({
        %{ $self->_json_data },
        $self->get_columns,
    });

    # Again with the column dump!
    $self->_json_data->{ zaaktype_node_id } = { $node->get_columns };
    $self->_json_data->{ public_url } = { $self->public_uri_paths };
};

=head2 public_uri_paths

=cut

sub public_uri_paths {
    my $self = shift;
    my $node = $self->zaaktype_node_id;
    my @retval;

    return unless defined $node;
    return if $node->trigger eq 'intern';

    my $format = sprintf(
        '/aanvragen/%d/%%s/%s',
        $self->id,
        $self->seo_friendly_title
    );

    my %map = (
        # $dbname => { $json_key => $path_part }
        natuurlijk_persoon         => { person       => 'persoon' },
        natuurlijk_persoon_na      => { person       => 'persoon' },
        niet_natuurlijk_persoon    => { organisation => 'organisatie' },
        niet_natuurlijk_persoon_na => { organisation => 'organisatie' },
        preset_client              => { unknown      => 'onbekend' }
    );

    my @subjects = map { $_->betrokkene_type } $node->zaaktype_betrokkenen->all;

    for my $subject (@subjects) {
        my $json_keys = $map{ $subject };
        next unless defined $json_keys;

        push @retval, map {
            $_ => sprintf($format, $json_keys->{ $_ })
        } keys %{ $json_keys };
    }

    # Return list instead of explicit map, since technically we can have
    # doubled keys (by virtue of conflating (niet_)natuurlijk_persoon(_na)
    # subjecttypes.
    return @retval;
}

=head2 seo_friendly_title

Format the title with only a-z, 0-9 and substitute a dash for anything else

=cut

sub seo_friendly_title {
    my $self = shift;
    my $title = lc $self->zaaktype_node_id->titel;

    $title =~ s/[^a-z0-9]+/-/gi;

    return $title;
}

=head2 set_active_node

Every zaaktype links to one zaaktype_node record. This system is used to allow multiple versions
of the zaaktype to exist. This method changes the active zaaktype_node. Every other node
must be disabled.

    my $zaaktype = $c->model('DB::Zaaktype')->find($zaaktype_id);

    $zaaktype->set_active_node($zaaktype_node_id);

=cut

sub set_active_node {
    my ($self, $zaaktype_node_id) = @_;

    die "need zaaktype_node_id" unless $zaaktype_node_id;

    my $schema = $self->result_source->schema;

    my $zaaktype_node_rs = $schema->resultset('ZaaktypeNode');
    die "new zaaktype_node_id does not exist" unless $zaaktype_node_rs->find($zaaktype_node_id);

    eval {
        $schema->txn_do(sub {
            # set currently node to deleted
            $self->zaaktype_node_id->deleted(DateTime->now());
            $self->zaaktype_node_id->update();

            $self->zaaktype_node_id($zaaktype_node_id);
            $self->update();

            # le roi est mort, vive le roi!
            $self->zaaktype_node_id->deleted(undef);
            $self->zaaktype_node_id->update();
        });
    };

    if ($@) {
        die "could update version: $@";
    }
}


=head2 attributes_by_magic_string

Get a lookup structure of the custom attributes in this casetype.
Say we have a text field with magic string [[my_text]] and
a number field [[my_number]], something like this will be returned:

    {
        my_text   => bless { value_type => 'text', id => 123, label => 'My text' },
        my_number => bless { value_type => 'number', id => 124, label => 'My number'}
    }

=cut

sub attributes_by_magic_string {
    my $self = shift;

    my @bibliotheek_kenmerken = map {
        $_->bibliotheek_kenmerken_id
    } $self->zaaktype_node_id->zaaktype_kenmerken->search({
        bibliotheek_kenmerken_id => {
            -not => undef
        }
    })->all;

    return {
        map { $_->magic_string => $_ } @bibliotheek_kenmerken
    };

}

1; #__PACKAGE__->meta->make_immutable;




__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 insert

TODO: Fix the POD

=cut

=head2 title

TODO: Fix the POD

=cut

=head2 update

TODO: Fix the POD

=cut

