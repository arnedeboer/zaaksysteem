package Zaaksysteem::DB::Component::BagNummeraanduiding;

use strict;
use warnings;

use base qw/Zaaksysteem::DB::Component::BagGeneral Zaaksysteem::Geo::BAG/;

sub nummeraanduiding {
    my $self    = shift;

    return $self->huisnummer . ($self->huisletter || '').
        ($self->huisnummertoevoeging  ? '-' . $self->huisnummertoevoeging : '');
}

=head2 geocode_term

Return value: $geocode_address

Generate a term for geocoding this location with a googlemaps or other
geocoder.

=cut

sub geocode_term {
    my $self    = shift;

    my @terms   = ('Nederland');

    my $woonplaats = $self->openbareruimte->woonplaats;
    if ($woonplaats) {
        push(@terms, $woonplaats->naam);
    }


    my $address = $self->openbareruimte->naam;
    $address    .= ' ' . $self->huisnummer;
    $address    .= $self->huisletter if $self->huisletter;
    $address    .= ' - ' . $self->huisnummertoevoeging
        if $self->huisnummertoevoeging;

    push(
        @terms,
        $address
    );

    return join(',', @terms);
}

sub to_string {
    my $self = shift;

    return sprintf(
        '%s %s, %s %s',
        $self->openbareruimte->naam,
        $self->nummeraanduiding // '',
        $self->postcode // '',
        $self->openbareruimte->woonplaats->naam
    );
}

sub TO_JSON {
    my $self = shift;

    my $json = {
        $self->get_columns,
        id => 'nummeraanduiding-' . $self->identificatie,
        type => 'address',
        number => $self->nummeraanduiding,
        street => $self->openbareruimte->naam,
        city => $self->openbareruimte->woonplaats->naam,
        postal_code => $self->postcode
    };

    $json->{object_subscription}    = undef;

    if ($self->can('subscription_id') && $self->subscription_id) {
        $json->{object_subscription} = {
            'id' => $self->subscription_id->id,
            'external_id' => $self->subscription_id->external_id,
        };
    }

    return $json;
}

=head2 insert

Return value: $row

Extends L<DBIx::Class> component insert by retrieving coordinates for this BAG record

=cut

sub insert {
    my $self    = shift;

    my $row     = $self->next::method(@_);

    $row->load_gps_coordinates;

    return $row;
}

=head2 update

Return value: $row

Extends L<DBIx::Class> component update by retrieving coordinates for this BAG record

=cut

sub update {
    my $self    = shift;

    $self->load_gps_coordinates({ skip_update => 1});

    $self->next::method(@_);

    return $self;
}


1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 TO_JSON

TODO: Fix the POD

=cut

=head2 nummeraanduiding

TODO: Fix the POD

=cut

=head2 to_string

TODO: Fix the POD

=cut

