package Zaaksysteem::DB::Component::ContactData;

use strict;
use warnings;

use base qw/DBIx::Class/;

sub TO_JSON {
    my $self = shift;

    my $data = {
        id => $self->id,
        email_addresses => [
            $self->email
        ],
        #created => $self->created->datetime,
        #last_modified => $self->last_modified->datetime,
    };

    my @phonenumbers = ();

    for my $key ('mobiel', 'telefoonnummer') {
        if($self->$key) {
            push(@phonenumbers, {
                type => $key eq 'mobiel' ? 'mobile' : 'landline',
                number => $self->$key
            });
        }
    }

    $data->{ phonenumbers } = [ @phonenumbers ];
    $data->{ identifier } = $self->identifier;

    if($self->natural_person) {
        $data->{ natural_person } = $self->natural_person;
    }

    if($self->non_natural_person) {
        $data->{ non_natural_person } = $self->non_natural_person;
    }

    return $data;
}

sub identifier {
    my $self = shift;

    if($self->natural_person) {
        return sprintf('betrokkene-natuurlijk_persoon-%d', $self->gegevens_magazijn_id);
    }

    if($self->non_natural_person) {
        return sprintf('betrokkene-bedrijf-%d', $self->gegevens_magazijn_id);
    }

    return;
}

sub natural_person {
    my $self = shift;

    if($self->betrokkene_type == 1) {
        return $self->result_source->schema->resultset('NatuurlijkPersoon')->find($self->gegevens_magazijn_id);
    }

    return;
}

sub non_natural_person {
    my $self = shift;

    if($self->betrokkene_type == 2) {
        return $self->result_source->schema->resultset('Bedrijf')->find($self->gegevens_magazijn_id);
    }

    return;
}

sub update {
    my $self = shift;

    my $result = $self->next::method(@_);
    $self->update_betrokkene;
    return $result;
}


sub insert {
    my $self = shift;

    my $result = $self->next::method(@_);
    $self->update_betrokkene;
    return $result;
}

sub update_betrokkene {
    my $self = shift;

    if (my $betrokkene = $self->natural_person || $self->non_natural_person) {
        $betrokkene->update;
    }
}


1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 TO_JSON

TODO: Fix the POD

=cut

=head2 identifier

TODO: Fix the POD

=cut

=head2 insert

TODO: Fix the POD

=cut

=head2 natural_person

TODO: Fix the POD

=cut

=head2 non_natural_person

TODO: Fix the POD

=cut

=head2 update

TODO: Fix the POD

=cut

=head2 update_betrokkene

TODO: Fix the POD

=cut

