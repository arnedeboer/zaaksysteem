package Zaaksysteem::DB::Component::BibliotheekSjablonen;

use strict;
use warnings;


use base qw/DBIx::Class/;


sub insert {
    my ($self) = @_;

    # we need the database to generate the new searchable_id param.
    # if we don't supply it, it will take its chance. so get rid
    # of whatever the app thinks it should pass.
    # is there a way to put this in the Schema? that's be less hacky.
    delete $self->{_column_data}->{searchable_id};

    $self->_set_search_term();
    return $self->next::method(@_);}

sub update {
    my ($self) = shift;

    $self->_set_search_term();
    return $self->next::method(@_);
}

sub _set_search_term {
    my ($self) = @_;
    my $search_term = '';
    if($self->naam) {
        $search_term .= $self->naam;
    }
    $self->search_term($search_term);
}


1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 insert

TODO: Fix the POD

=cut

=head2 update

TODO: Fix the POD

=cut

