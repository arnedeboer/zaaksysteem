package Zaaksysteem::DB::Component::BibliotheekSjablonen;

use strict;
use warnings;


use base qw/DBIx::Class/;


sub insert {
    my ($self) = @_;

    # we need the database to generate the new searchable_id param.
    # if we don't supply it, it will take its chance. so get rid
    # of whatever the app thinks it should pass.
    # is there a way to put this in the Schema? that's be less hacky.
    delete $self->{_column_data}->{searchable_id};

    $self->_set_search_term();
    return $self->next::method(@_);}

sub update {
    my ($self) = shift;

    $self->_set_search_term();
    return $self->next::method(@_);
}

sub _set_search_term {
    my ($self) = @_;
    my $search_term = '';
    if($self->naam) {
        $search_term .= $self->naam;
    }
    $self->search_term($search_term);
    $self->search_order($search_term);
}

=head2 used_by_casetypes

Returns a HashRef with the casetypes which are using the Catalogue Templates.

    { id => $casetype_node },

the casetype ID as the key and the L<Zaaksysteem::Model::DB::ZaaktypeNode> as the value.

=cut

sub used_by_casetypes {
    my $self = shift;

    my $sjablonen = $self->result_source->schema->resultset('ZaaktypeSjablonen')
        ->search({ bibliotheek_sjablonen_id => $self->id });
    my ($result, $ztn, $zt);
    while (my $f = $sjablonen->next) {
        $ztn = $f->zaaktype_node_id;
        $zt = $ztn->zaaktype_id->id;
        if ($ztn->is_huidige_versie) {
            $result->{$zt} = $ztn;
        }
        $result->{$zt} //= $ztn;
    }
    return $result;
}


1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 insert

TODO: Fix the POD

=cut

=head2 update

TODO: Fix the POD

=cut

