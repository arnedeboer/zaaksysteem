package Zaaksysteem::DB::Component::Logging::Object::Update;

use Moose::Role;

=head1 NAME

Zaaksysteem::DB::Component::Logging::Object::Update - Event subject
for object update mutations.

=head1 METHODS

=head2 onderwerp

Overrides L<Zaaksysteem::Schema::Logging/onderwerp> and provides a
contextualized summary of the event.

=cut

sub onderwerp {
    my $self = shift;

    return sprintf(
        '%s "%s" aangepast',
        $self->object_description,
        $self->data->{ object_label },
    );
}

around TO_JSON => sub {
    my $orig = shift;
    my $self = shift;

    my $data = $self->$orig(@_);

    my @value_rows = map {
        my $old_value = ref($_->{old_value}) eq 'ARRAY'
            ? join(", ", @{ $_->{old_value} })
            : $_->{old_value};
        my $new_value = ref($_->{new_value}) eq 'ARRAY'
            ? join(", ", @{ $_->{new_value} })
            : $_->{new_value};

        sprintf(
            "%s: %s -> %s",
            $_->{field_label},
            $old_value // "geen waarde",
            $new_value // "geen waarde",
        ),
    } @{ $self->data->{changes} };

    $data->{ content } = join("\n", @value_rows);
    $data->{ expanded } = JSON::false;

    return $data;
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
