package Zaaksysteem::DB::Component::Logging::Object::View;

use Moose::Role;

=head1 NAME

Zaaksysteem::DB::Component::Logging::Object::Note - Event handler/mangler for
views on objects.

=head1 METHODS

=head2 onderwerp

This method constructs a human-readable summary of the event.

=cut

sub onderwerp {
    my $self = shift;

    return sprintf(
        '<a href="/object/%s">%s</a> opgevraagd door "%s"',
        $self->get_column('object_uuid'),
        $self->data->{ object_label },
        $self->data->{ subject_label }
    );
}

=head2 event_category

Hardwired to the string C<object>.

=cut

sub event_category { 'object' }

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
