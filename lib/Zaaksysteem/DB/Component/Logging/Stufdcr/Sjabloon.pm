package Zaaksysteem::DB::Component::Logging::Stufdcr::Sjabloon;
use Moose::Role;

=head1 NAME

Zaaksysteem::DB::Component::Logging::Stufdcr::Sjabloon - Log line for "StUF DCR Sjabloon added"

=head1 METHODS

=head2 onderwerp

Returns a nicely formatted (for humans) subject line for the log line.

=cut

sub onderwerp {
    my $self = shift;

    sprintf(
        "%s toegevoegd aan zaak %s: %s",
        (@{ $self->data->{ ids } || [] } > 1 ? 'Sjablonen' : 'Sjabloon'),
        $self->get_column('zaak_id'),
        $self->data->{filename},
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
