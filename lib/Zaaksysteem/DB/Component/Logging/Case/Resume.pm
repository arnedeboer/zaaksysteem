package Zaaksysteem::DB::Component::Logging::Case::Resume;

use Moose::Role;

sub onderwerp {
    my $self = shift;

    my $onderwerp = sprintf(
        'Zaak %d hervat: %s',
        $self->data->{ case_id },
        $self->data->{ reason },
    );

    if($self->data->{ stalled_since } && $self->data->{ stalled_until }) {
        $onderwerp .= ". Opgeschort van " . $self->data->{ stalled_since } .
            " tot " . $self->data->{ stalled_until };
    }

    return $onderwerp;
}

sub event_category { 'case-mutation'; }

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 event_category

TODO: Fix the POD

=cut

=head2 onderwerp

TODO: Fix the POD

=cut

