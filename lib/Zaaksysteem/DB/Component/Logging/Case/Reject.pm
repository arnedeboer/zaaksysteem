package Zaaksysteem::DB::Component::Logging::Case::Reject;

use Moose::Role;

=head2 onderwerp

    Zaak 1336 is geweigerd door "Don Juan"

=cut

sub onderwerp {
    my $self = shift;

    sprintf('Zaak %s is geweigerd door "%s"', $self->get_column('zaak_id'), $self->data->{current_user}->{display_name});
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
