package Zaaksysteem::DB::Component::Logging::Case::Document;

use Moose::Role;

has file => ( is => 'ro', lazy => 1, default => sub {
    my $self = shift;

    $self->rs('File')->find($self->data->{ file_id } // $self->component_id);
});

around TO_JSON => sub {
    my $orig = shift;
    my $self = shift;

    my $data = $self->$orig(@_);

    my $file = $self->file;

    return $data unless $file;

    $data->{ file_id } = $file->id;
    $data->{ file_name } = $file->name;
    $data->{ mimetype } = $file->filestore_id->mimetype;

    return $data;
};

sub event_category { 'document'; }

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 event_category

TODO: Fix the POD

=cut

