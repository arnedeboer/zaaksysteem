package Zaaksysteem::DB::Component::Logging::Case::Document::Reject;

use Moose::Role;
use Data::Dumper;
use HTML::Entities qw(encode_entities);

sub onderwerp {
    my $self = shift;

    my $message = $self->data->{reject_to_queue} ?
        'Document "'. encode_entities($self->data->{filename}) .'" geweigerd en teruggeplaatst naar de documentintake.' :
        'Document "' . encode_entities($self->data->{filename}) . '" geweigerd en permanent verwijderd."';

    return $message . ($self->data->{rejection_reason} ? " Reden: " . $self->data->{rejection_reason} : '');
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 onderwerp

TODO: Fix the POD

=cut

