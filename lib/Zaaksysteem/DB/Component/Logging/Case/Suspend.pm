package Zaaksysteem::DB::Component::Logging::Case::Suspend;

use Moose::Role;

use DateTime::Format::Strptime;
use Zaaksysteem::Tools;

sub onderwerp {
    my $self = shift;

    my $data = $self->data;

    if ($data->{stalled_until}) {
        my $strp = DateTime::Format::Strptime->new(
            pattern  => "%Y-%m-%d",
            locale   => 'nl_NL',
            time_zone => "Europe/Amsterdam",
        );
        my $dt = $strp->parse_datetime($data->{stalled_until});
        $strp->pattern("%d-%m-%Y");

        return sprintf(
            'Zaak %d opgeschort tot %s: %s',
            $data->{case_id},
            $dt ? $strp->format_datetime($dt): $data->{stalled_until},
            $data->{reason}
        );
    }
    return sprintf('Zaak %s opgeschort voor onbepaalde tijd: %s', $data->{case_id}, $data->{reason});

}

sub event_category { 'case-mutation'; }

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 event_category

TODO: Fix the POD

=cut

=head2 onderwerp

TODO: Fix the POD

=cut

