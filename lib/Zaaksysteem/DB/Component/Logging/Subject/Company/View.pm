package Zaaksysteem::DB::Component::Logging::Subject::Company::View;

use Moose::Role;

=head2 onderwerp

    my $text_onderwerp = $self->onderwerp;

    # Returns:
    # Bedrijf "Mintlab" opgevraagd, KVK nummer: "51902672", via openkvk.

=cut

sub onderwerp {
    my $self    = shift;

    sprintf(
        'Bedrijf "%s" opgevraagd, KVK nummer: "%s", via %s.',
        $self->data->{company},
        $self->data->{coc_number},
        ($self->data->{module} || 'zaaksysteem')
    );
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
