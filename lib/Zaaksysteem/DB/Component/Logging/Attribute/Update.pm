package Zaaksysteem::DB::Component::Logging::Attribute::Update;

use Moose::Role;
use Data::Dumper;

sub onderwerp {
    my $self = shift;

    return $self->get_column('onderwerp') unless $self->attribute;

    my $onderwerp = sprintf(
        'Kenmerk "%s" opgeslagen, versie %s',
        $self->attribute->naam,
        $self->data->{ version }
    );

    if ($self->data->{reason}) {
        $onderwerp .= " (" . $self->data->{reason} . ") ";
    }

    my $options = $self->data->{options};

    if ($options) {
        $onderwerp .= ', Opties: ' . join ", ", map {
            $_->{active} ? $_->{value} : '(<strike>' . $_->{value} . '</strike>)'
        } @$options;
    }

    return length $onderwerp > 255 ? substr ($onderwerp, 0, 250) . '...' : $onderwerp;
}

sub _add_magic_attributes {
    my $self = shift;

    $self->meta->add_attribute('attribute' => (is => 'ro', lazy => 1, default => sub {
        return $self->result_source->schema->resultset('BibliotheekKenmerken')->find($self->data->{ attribute_id });
    }));
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 onderwerp

TODO: Fix the POD

=cut

