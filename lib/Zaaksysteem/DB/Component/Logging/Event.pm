package Zaaksysteem::DB::Component::Logging::Event;

use strict;
use warnings;

use Moose::Role;

use Data::Dumper;
use Encode qw(encode_utf8);
use JSON;

has data => (
    is => 'rw',
    default => sub {
        JSON->new->utf8(0)->decode(shift->event_data // '{}')
    }
);

sub encode {
    my $self = shift;

    no warnings 'redefine';
    local *DateTime::TO_JSON = sub { shift->iso8601 };

    return JSON->new->utf8(1)->canonical->convert_blessed->encode(shift // {});
}

before [qw[update insert]] => sub {
    my $self = shift;

    # Automagic for a sort-of required field. At least all events will show up in the logbook now
    unless(defined $self->component) {
        $self->component('event');
    }

    $self->set_column(onderwerp => encode_utf8($self->onderwerp));

    $self->event_data($self->encode($self->data));
};

# Placeholder to be overwritten by event roles that actually
# define some additional attributes
sub _add_magic_attributes { }

# Most generic kind of subjectline available for events
sub onderwerp {
    my ($self) = @_;

    return sprintf('Event "%s": %s', $self->event_type, $self->encode($self->data));
}

# Allow inflection for role package names derived from the event type of the auditlog event bound to this role.
# in goes 'kcc/call', out comes 'Kcc::Call'. Supports many levels of nesting.
sub inflect_event_type {
    my ($self, $event_type) = @_;

    return unless defined $event_type;

    return sprintf(
        'Zaaksysteem::DB::Component::Logging::%s',
        join('::', map { join('', map(ucfirst, split('_', $_))) } split('/', $event_type))
    );
}

sub event_category {
    my @type_split = split m[/], shift->event_type;

    return shift @type_split;
}

around TO_JSON => sub {
    my $orig = shift;
    my $self = shift;

    my $data = $self->$orig(@_);

    for my $key (keys %{ $self->data }) {
        $data->{ $key } = $self->data->{ $key };
    }

    return $data;
};

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 encode

TODO: Fix the POD

=cut

=head2 event_category

TODO: Fix the POD

=cut

=head2 inflect_event_type

TODO: Fix the POD

=cut

=head2 onderwerp

TODO: Fix the POD

=cut

