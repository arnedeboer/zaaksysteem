package Zaaksysteem::DB::Component::GenericNatuurlijkPersoon;

use strict;
use warnings;
use Data::Dumper;

use base qw/DBIx::Class/;

sub TO_JSON {
    my $self                = shift;

    my $json                = $self->next::method(@_);

    if ($self->subscription_id) {
        $json->{object_subscription} = {
            'id' => $self->subscription_id->id,
            'external_id' => $self->subscription_id->external_id,
        };
    }

    return $json;
}

sub in_onderzoek {
    my $self    = shift;

    my @checks = grep( { $_ =~ /^onderzoek_.*?/ } $self->columns);

    my @failures;
    for my $check (@checks) {
        my $categorie   = $check;
        $categorie      =~ s/onderzoek_(.*?)/$1/;

        if ($self->$check) {
            push(@failures, $categorie);
        }
    }

    return \@failures if scalar(@failures);
    return;
}

sub is_overleden {
    my $self    = shift;

    return 1 if $self->datum_overlijden;
    return;
}


1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 TO_JSON

TODO: Fix the POD

=cut

=head2 in_onderzoek

TODO: Fix the POD

=cut

=head2 is_overleden

TODO: Fix the POD

=cut

