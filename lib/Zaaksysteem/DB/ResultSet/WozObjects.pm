package Zaaksysteem::DB::ResultSet::WozObjects;

use Moose;
use namespace::autoclean;
use MooseX::NonMoose;
use Zaaksysteem::Constants;
use Data::Dumper;
use Parse::FixedLength;
use Archive::Zip qw( :ERROR_CODES :CONSTANTS );
use Zaaksysteem::JSON;
use File::Path qw/remove_tree/;
use Number::Format;
use Encode;
use Text::CSV;
use Time::HiRes qw/gettimeofday tv_interval/;
extends 'DBIx::Class::ResultSet';

sub BUILDARGS { $_[2] }


has 'old_values' => (
    'is'        => 'rw',
);


has 'recordtypes' => (
    'is'        => 'rw',
    'default'   => sub { {} },
);

has 'parsers' => (
    'is'        => 'rw',
    'default'   => sub { {} },
);


has 'old_values_date' => (
    'is'         => 'rw',
);


has 'vergelijkbare_objecten' => (
    'is'        => 'rw',
    'default'   => sub { {} },
);


has 'id_parser' => (
    'is'        => 'rw',
    'default'   => sub {Parse::FixedLength->new([record_id => 2 ]) },
);


has 'bedrijven' => (
    'is'        => 'rw',
    'default'   => sub { {} },
);

has 'marktinformatie' => (
    'is'        => 'rw',
    'default'   => sub { {} },
);


has 'woz_objects' => (
    'is'        => 'rw',
    'default'   => sub { {} },
);

has 'woz_owners' => (
    'is'        => 'rw',
    'default'   => sub { {} },
);


# only these record types are parsed right now
use constant ACTIVE_RECORD_IDS => {
    20 => 'woz_object',
#    30 => 'natuurlijk_persoon',
    31 => 'niet_natuurlijk_persoon',
    40 => 'kadastrale_identificatie',
    21 => 'onderbouwing_taxatie',
    22 => 'onderbouwing_taxatie_onderdeel',
    23 => 'identificatie_markinformatie',
    #24 => 'tussentijdse_taxatie',
    #25 => 'procesmatige_controle',
    #35 => 'extra_adressen',
    #41 => 'sluimerende_objecten',
    #51 => 'kadastrale_objecten',
    52 => 'marktinformatie',
    #53 => 'marktgegevens_objecten',
    #54 => 'kadastrale_identificatie',
    #80 => 'beschikkingen',
    60 => 'identificatie_eigenaar_gebruiker',
    #91 => 'codering_soort_object',
    #92 => 'codering_onderdeel',
};

use constant RECORDTYPES_MULTIPLE_LOOKUP => {
  'kadastrale_objecten' => 1,
  'tussentijdse_taxatie' => 1,
  'identificatie_markinformatie' => 1,
  'beschikkingen' => 1,
  'procesmatige_controle' => 1,
  'marktgegevens_objecten' => 1,
  'sluimerende_objecten' => 1,
  'onderbouwing_taxatie_onderdeel' => 1,
  'kadastrale_identificatie' => 1
};


=head2 search_betrokkene_objects

Currently WOZ is only aware of nstuurlijk_persoon and bedrijf type
betrokkenen, so no point in searching for others. In these cases, just
return nothing. No objects will be shwon and no-one gets hurt but the
taxman.

=cut

sub search_betrokkene_objects {
    my ($self, $options) = @_;

    my $betrokkene = $options->{betrokkene} or die "need betrokkene";

    my $btype = $betrokkene->btype;

    my $dispatch = {
        natuurlijk_persoon => sub { shift->burgerservicenummer },
        bedrijf            => sub { shift->dossiernummer }
    };

    return $self->search({
        owner => $betrokkene->btype . '-' . $dispatch->{$btype}->($betrokkene)
    }) if $dispatch->{$btype};
}


=head2 import_woz

upload a zip file with one or two files in it.

STUF_OUD{date}.csv: old values for WOZ
STUF_NIEUW: new values, STUF tax format

=cut

sub import_woz {
    my ($self, $opts) = @_;

    my $t0 = [gettimeofday];
    my $file_path   = $opts->{file_path}    or die "need file_path";
    my $tmp_dir     = $opts->{tmp_dir}      or die "need tmp_dir";


    # Read a Zip file
    my $zip = Archive::Zip->new();
    unless ( $zip->read( $file_path ) == AZ_OK ) {
        die "Uploaded file $file_path is not a zip file";
    }

    warn "unpacked zip " . tv_interval($t0);

    if(my ($old_values_member) = grep { $_->fileName =~ m/^STUF_OUD(.*).csv/ } $zip->members) {

        my ($date) = $old_values_member->fileName =~ m|^STUF_OUD(.*).csv|;
        $self->old_values_date($date);

        $self->_importMember({
            zip     => $zip,
            member  => $old_values_member,
            type    => 'old',
            tmp_dir => $tmp_dir,
        });
        warn "imported old values " . tv_interval($t0);
    }


    my $stuf_new_filename = 'STUF_NIEUW';
    my ($new_values_member) = grep { $_->fileName =~ m/^$stuf_new_filename/ } $zip->members;

    die "need $stuf_new_filename"
        unless $new_values_member;

    $self->_importMember({
        zip     => $zip,
        member  => $new_values_member,
        type    => 'new',
        tmp_dir => $tmp_dir,
    });

    warn "imported new values " . tv_interval($t0);
}


sub _importMember {
    my ($self, $opts) = @_;

    my $member      = $opts->{member}   or die 'need member';
    my $type        = $opts->{type}     or die 'need type';
    my $tmp_dir     = $opts->{tmp_dir}  or die "need tmp_dir";
    my $zip         = $opts->{zip}      or die "need zip";

    my $extracted_file_path = $tmp_dir . $member->fileName;
    $zip->extractMember($member, $extracted_file_path);


    if($type eq 'old') {

        $self->_read_old_values({
            extracted_file_path => $extracted_file_path
        });

    } elsif($type eq 'new') {

        $self->result_source->schema->txn_do(sub {

            # clean list
            $self->delete;

            $self->_import_file({
                file_path => $extracted_file_path
            });
        });
    }
    # exception is caught by Catalyst and shown to user.
    remove_tree($extracted_file_path);
}


sub _read_old_values {
    my ($self, $opts) = @_;

    my $extracted_file_path = $opts->{extracted_file_path} or die "need extracted_file_path";


    my $csv = Text::CSV->new({ binary => 1 })
        or die "Cannot use CSV: ".Text::CSV->error_diag ();

    open my $fh, "<:encoding(utf8)", $extracted_file_path
        or die "could not open $extracted_file_path: $!";

    my $columns = $csv->getline($fh); # just remove the header

    my $old_values = {};

    while ( my $row = $csv->getline( $fh ) ) {

        my $values = {};

        @$values{@$columns} = @$row;

        $old_values->{
            $values->{WOZOBJEKTNR}
        } = $values->{WAARDE};

    }

    # make generously available for the whole object!
    $self->old_values($old_values);

    $csv->eof or $csv->error_diag();
    close $fh;
}


sub _import_file {
    my ($self, $opts) = @_;

    # get the file format, compiled to be processe by Parse::FixedLength
    my $recordtypes = $self->initialize_recordtypes;

    my $file_path = $opts->{file_path} or die "need file_path";

    my $count = 0;
    my $line;

    my $t0 = [gettimeofday];

    # read through the file
    open my $fh, '<', $file_path or die "Cannot open $file_path: $!";

    while(sysread $fh, $line, 256) {

        $self->parse_line({ line => $line });

        $count++;

        warn "count: $count" unless $count && $count % 10000;
    }
    close $fh;


    warn "parsed file " . tv_interval($t0);

    $self->_import_new();
    warn "imported records " . tv_interval($t0);
}


sub removeFiller {
    my ($self, $record) = @_;

    foreach my $key (keys %$record) {
        if($key =~ m|^Filler|) {
            delete $record->{$key};
        }
    }
}




sub _import_new {
    my ($self, $opts) = @_;

    my $woz_owners = $self->woz_owners;
    my $woz_objects = $self->woz_objects;

    my $json = new Zaaksysteem::JSON;

    my $count = 0;
    foreach my $owner (keys %$woz_owners ) {

        # every woz object record is bluntly encoded in one JSON string
        # and linked to an owner.
        my $woz_object_list = $woz_owners->{$owner};

        my $combined_list = [];

        for my $aanduiding_eigenaar_gebruiker (qw/E G/) {
            if(exists $woz_object_list->{$aanduiding_eigenaar_gebruiker}) {
                my $list = $woz_object_list->{$aanduiding_eigenaar_gebruiker};
                foreach my $woz_object (@$list) {
                    $woz_object->{aanduiding_eigenaar_gebruiker} = $aanduiding_eigenaar_gebruiker;

                    my $woz_object_id = $woz_object->{'WOZ-objectnummer'} or die "need woz object id";

                    unless($woz_object_id) {
                        die Dumper $woz_object;
                    }

                    if($self->old_values && $self->old_values->{$woz_object_id}) {
                        $woz_object->{'Vorige waarde'} = {
                            waarde      => $self->old_values->{$woz_object_id},
                            peildatum   => $self->old_values_date,
                        };
                    }

                    $self->find_vergelijkbare_objecten({
                        woz_object => $woz_object,
                    });

                    $self->create({
                        owner           => $owner,
                        object_id       => $woz_object_id,
                        object_data     => $woz_object,
                    });

                    warn "import count: $count" unless ++$count && $count % 1000;
                }
            }
        }

    }
}


sub find_vergelijkbare_objecten {
    my ($self, $arguments) = @_;

    my $woz_object  = $arguments->{woz_object}  or die "need woz_object";

    my $onderbouwing_taxatie = $woz_object->{onderbouwing_taxatie};

    my $groepaanduiding = $onderbouwing_taxatie->{'Groepaanduiding vergelijkbare objecten'};
    return unless $groepaanduiding;

    my $vergelijkbare_objecten = $self->vergelijkbare_objecten->{$groepaanduiding};
    return unless $vergelijkbare_objecten;

    # sort by a numeric key
    my $indicatie_key = 'Indicatie vermelding op taxatieverslag';

    $vergelijkbare_objecten = [
        sort {
            $a->{$indicatie_key} <=> $b->{$indicatie_key}
        } @$vergelijkbare_objecten
    ];

    # not found in import, ignoring for now
    #if($vergelijkbare_objecten->[0]->{$indicatie_key} eq '0') {
    #
    #    warn "vergelijkbare_objecten: " . Dumper $vergelijkbare_objecten;
    #}

    my $found = [];
    foreach my $vergelijkbaar_object (@$vergelijkbare_objecten) {

        my $indicatie_vermelding = $vergelijkbaar_object->{'Indicatie vermelding op taxatieverslag'};
        my $volgnummer_marktgegeven = $vergelijkbaar_object->{'Volgnummer marktgegeven'};

        next unless
            $indicatie_vermelding &&
            $indicatie_vermelding =~ m|^\d$| &&
            $indicatie_vermelding >= 1 &&
            $indicatie_vermelding <= 7 &&
            $volgnummer_marktgegeven;


        my $woz_object_id = int($vergelijkbaar_object->{'WOZ-objectnummer referentie-object'});
        my $ref_woz_object = $self->woz_objects->{$woz_object_id};

        if($ref_woz_object) {

            my $ref_woz_data = {};
            foreach my $key (keys %$ref_woz_object) {
                next if
                    ref $ref_woz_object->{$key} &&
                    $key ne 'onderbouwing_taxatie' &&
                    $key ne 'onderbouwing_taxatie_onderdeel';

                $ref_woz_data->{$key} = $ref_woz_object->{$key};
            }
            $vergelijkbaar_object->{woz_object} = $ref_woz_data;
        }
        if(exists $self->marktinformatie->{$volgnummer_marktgegeven} ) {
            $vergelijkbaar_object->{marktinformatie} = $self->marktinformatie->{$volgnummer_marktgegeven};
        }
        push @$found, $vergelijkbaar_object;
    }

    $woz_object->{'vergelijkbare_objecten'} = $found;
}

#
# cache parser objects
#
sub _parser {
    my ($self, $arguments) = @_;

    my $record_id = $arguments->{record_id} or die "need record_id";

    unless(
        exists $self->parsers->{$record_id}
    ) {
        my $format = [];
        my $columns = $self->recordtypes->{$record_id};
        foreach my $column (@$columns) {
            push @$format, $column->{label};
            push @$format, $column->{size};
        }

        my $parser = Parse::FixedLength->new($format);

        $self->parsers->{$record_id} = $parser;
    }

    return $self->parsers->{$record_id};
}


sub initialize_recordtypes {
    my ($self) = @_;

    my $header_regexp = '.*positie lengte type nummer naam gegeven';
    my $record_id_column_regexp = 'Recordidentificatie.*?(\d\d)';
    my $column_regexp = '(\d+)\s-\s(\d+)\s(\d+)\s(\w+)\s(\d+\.\d+)\s(.*)';
    my $filler_column_regexp = '(\d+)\s-\s(\d+)\s(\d+)\s(\w+)\s(.*)Filler\s?';
    my $totaltelling_regexp = '(\d+)\s-\s(\d+)\s(\d+)\s(\w+)\sTotaaltelling.*(\w\.\w)';
    my $totaal_regexp = '(\d+)\s-\s(\d+)\s(\d+)\s(\w+)\s(Totaal.*)';

    my $current_record_id = '';
    my $current_recordtype;
    my $fillercount = 0; # multiple filler fields per record possible

    my $recordlayout_lines = $self->recordlayout_lines();

    foreach my $line (@$recordlayout_lines) {
        last if($line =~ m|^Sluitrecord|);
        next if($line =~ m|^positie lengte type nummer naam gegeven|);
        if($line =~ m|Record|) {
            my ($record_id) = $line =~ m|$record_id_column_regexp|;

            if($current_recordtype) {
                $self->recordtypes->{$current_record_id} = $current_recordtype;
                $current_recordtype = [];
            }
            $current_record_id = $record_id;

            push @$current_recordtype, {
                size => 2,
                label => 'record_id',
            };
        } elsif($line =~ m|$column_regexp|) {
            my ($start, $end, $size, $type, $number, $label) = $line =~ m|$column_regexp|;

            my $column = {
                start => $start,
                end => $end,
                size => $size,
                type => $type,
                number => $number,
                label => $label,
            };

            push @$current_recordtype, $column;

        } elsif($line =~ m|$filler_column_regexp|) {

            my ($start, $end, $size, $type, $label) = $line =~ m|$filler_column_regexp|;

            $fillercount += 1;

            my $column = {
                start => $start,
                end => $end,
                size => $size,
                type => $type,
                label => 'Filler' . $fillercount,
            };
            push @$current_recordtype, $column;
        }
    }
}


sub recordlayout_lines {
    my ($self) = @_;

    my $input = <<END_OF_RECORD_LAYOUT;
1. Voorlooprecord
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 00)
3 - 6 4 N 09.10 Gemeentecode
7 - 46 40 A 09.11 Gemeentenaam
47 - 86 40 A 91.10 Contactpersoon
87 - 106 20 A 91.20 Telefoonnummer contactpersoon
107 - 110 4 A Filler
111 - 118 8 D 92.11 Aanmaakdatum bestand
119 - 120 2 N 92.20 Bijgewerkt tot en met maand
121 - 128 8 D 92.31 Datum vorige aanlevering bestand
129 - 129 1 A 93.31 Aard leveringsbestand Stuf-TAX
130 - 149 20 A 91.31 Softwareleverancier Stuf-TAX
150 - 151 2 N 91.41 Versie Stuf-TAX
152 - 256 105 A Filler

2. Indien records met WOZ-objecten worden geleverd volgt hierna (anders
vervolg bij 3):
Stuurrecord
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 10)
3 - 4 2 N 93.21 Deelbestandsidentificatie Stuf-TAX (= 20)
5 - 256 252 A Filler
Gegevensrecords
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 20)
3 - 14 12 N 01.01 WOZ-objectnummer
15 - 54 40 A 10.20 Woonplaatsnaam
55 - 78 24 A 11.10 Straatnaam
79 - 83 5 N 11.20 Huisnummer
84 - 84 1 A 11.30 Huisletter
85 - 88 4 A 11.40 Huisnummertoevoeging
89 - 90 2 A 11.50 Aanduiding bij huisnummer
91 - 96 6 A 11.60 Postcode
97 - 136 40 A 11.70 Lokatieomschrijving
137 - 144 8 N 12.10 Grondoppervlakte
145 - 146 2 N 12.20 Gebruikscode
Gegevensrecords
positie lengte type nummer naam gegeven
147 - 147 1 A 14.10 Code gebouwd/ongebouwd
148 - 155 8 N 14.20 Meegetaxeerde oppervlakte gebouwd
156 - 166 11 N 14.35 Aandeel getaxeerde waarde gebouwd
167 - 177 11 N 15.10 Vastgestelde waarde
178 - 185 8 D 15.20 Waardepeildatum
186 - 188 3 N 15.30 Bijzondere-waarderingscode
189 - 189 1 A 81.10 Mutatiecode
190 - 197 8 D 81.20 Ingangsdatum
198 - 205 8 D 81.30 Einddatum
206 - 210 5 N 11.11 Straatcode
211 213 3 A 15.40 Aanduiding valutasoort
214 215 2 N 15.55 Code blokkeren taxatie WOZ-object
216 - 256 41 A Filler
Telrecord
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 90)
3 - 11 9 N Totaal aantal gegevensrecords deelbestand
12 - 21 10 N Totaaltelling 12.10
22 - 31 10 N Totaaltelling 14.20
32 - 44 13 N Totaaltelling 14.35
45 - 57 13 N Totaaltelling 15.10
58 - 256 199 A Filler
3. Indien records met subjecten worden geleverd volgt hierna (anders vervolg
bij 4):
Stuurrecord
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 10)
3 - 4 2 N 93.21 Deelbestandsidentificatie Stuf-TAX (= 30)
5 - 256 252 A Filler
Gegevensrecords
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 30)
3 - 12 10 N 01.10 A-nummer natuurlijk persoon
13 - 21 9 N 01.20 SoFi-nummer
22 - 31 10 A 02.11 Voorletters
32 - 41 10 A 02.30 Voorvoegsels
42 - 176 135 A 02.40 Geslachtsnaam-/bedrijfsnaam
177 - 231 55 A 02.41 Partnernaam/bedrijfsnaam verkort
232 - 241 10 A 02.31 Voorvoegsels behorend bij partnernaam
242 - 251 10 N 01.21 Aanvulling SoFi-nummer
252 - 251 1 A 04.05 Aanduiding naamgebruik
253 - 256 4 A Filler
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 31)
3 - 11 9 N 01.20 SoFi-nummer
12 - 35 24 A 11.10 Straatnaam
36 - 40 5 N 11.20 Huisnummer
41 - 41 1 A 11.30 Huisletter
42 - 45 4 A 11.40 Huisnummertoevoeging
46 - 47 2 A 11.50 Aanduiding bij huisnummer
48 - 53 6 A 11.60 Postcode
54 - 93 40 A 10.20 Woonplaatsnaam
94 - 133 40 A 13.10 Landnaam
134 - 134 1 A 81.10 Mutatiecode
135 - 142 8 D 81.20 Ingangsdatum
143 - 150 8 D 81.30 Einddatum
151 - 190 40 A 11.70 Lokatieomschrijving
191 - 200 10 N 01.21 Aanvulling SoFi-nummer
201 - 208 8 N 01.30 Handelsregisternummer
209 - 212 4 A Filler
213 - 222 10 N 01.40 Subjectnummer AKR
223 - 224 2 A 02.20 Adelijke titel of predikaat
225 - 225 1 A Filler
226 - 226 1 A 04.10 Geslachtsaanduiding
227 - 234 8 D 03.10 Geboortedatum natuurlijk persoon
235 - 242 8 D 08.10 Datum overlijden natuurlijk persoon
243 - 243 1 A 08.11 Status subject
244 - 248 5 A Filler
249 - 249 1 A 10.10 Functie adres
250 - 256 7 A Filler
Telrecord
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 90)
3 - 11 9 N Totaal aantal gegevensrecords deelbestand
12 - 20 9 N Totaal aantal gegevensrecords recordidentificatiecode
= 30
21 - 29 9 N Totaal aantal gegevensrecords recordidentificatiecode
= 31
30 - 256 227 A Filler
4. Indien records met kadastrale identificaties WOZ-objecten worden geleverd
volgt hierna (anders vervolg bij 5):
Stuurrecord
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 10)
3 - 4 2 N 93.21 Deelbestandsidentificatie Stuf-TAX (= 40)
5 - 256 252 A Filler
Gegevensrecord
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 40)
3 - 14 12 N 01.01 WOZ-objectnummer
15 - 19 5 A 51.10 Kadastrale gemeentecode
20 - 21 2 A 51.20 Sectie
22 - 26 5 N 51.30 Perceelnummer
27 - 27 1 A 51.40 Perceel-index-letter
28 - 31 4 N 51.50 Perceel-index-nummer
32 - 39 8 N 52.10 Toegekende oppervlakte
40 - 47 8 N 52.20 Meegetaxeerde oppervlakte gebouwd per
kadastraal object
48 - 48 1 A 81.10 Mutatiecode
49 - 56 8 D 81.20 Ingangsdatum
57 - 64 8 D 81.30 Einddatum
65 - 256 192 A Filler
Telrecord
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 90)
3 - 11 9 N Totaal aantal gegevensrecords deelbestand
12 - 24 13 N Totaaltelling 52.10
25 - 37 13 N Totaaltelling 52.20
38 - 256 219 A Filler
5. Indien records met identificaties eigenaar/gebruiker worden geleverd volgt
hierna (anders vervolg bij 5a):
Stuurrecord
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 10)
3 - 4 2 N 93.21 Deelbestandsidentificatie Stuf-TAX (= 60)
5 - 256 252 A Filler
Gegevensrecords
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 60)
3 - 14 12 N 01.01 WOZ-objectnummer
15 - 24 10 N 01.10 A-nummer natuurlijk persoon
25 - 33 9 N 01.20 SoFi-nummer
34 - 34 1 A 41.10 Aanduiding eigenaar/gebruiker
35 - 40 6 A 41.20 Zakelijk-rechtcode
41 - 42 2 A 41.30 c.s.-code
43 - 43 1 A 81.10 Mutatiecode
44 - 51 8 D 81.20 Ingangsdatum
52 - 59 8 D 81.30 Einddatum
60 - 69 10 N 01.21 Aanvulling SoFi-nummer
70 - 79 10 N 01.40 Subjectnummer AKR
80 - 256 177 A Filler
Telrecord
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 90)
3 - 11 9 N Totaal aantal gegevensrecords deelbestand
12 - 256 245 A Filler
5a. Indien records met statusgegevens van beschikkingen worden geleverd volgt
hierna (anders vervolg bij 6):
Stuurrecord
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 10)
3 - 4 2 N 93.21 Deelbestandsidentificatie Stuf-TAX (= 80)
5 - 256 252 A Filler
Gegevensrecords
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 80)
3 - 14 12 N 01.01 WOZ-objectnummer
15 - 22 8 D 15.20 Waardepeildatum
23 - 24 2 A Filler
25 - 26 2 N 22.10 Code status beschikking
27 - 27 1 A 81.10 Mutatiecode
28 - 35 8 D 81.20 Ingangsdatum
36 - 43 8 D 81.30 Einddatum
44 - 53 10 N 01.10 A-nummer natuurlijk persoon
Gegevensrecords
positie lengte type nummer naam gegeven
54 - 62 9 N 01.20 SoFi-nummer
63 - 72 10 N 01.21 Aanvulling SoFi-nummer
73 - 80 8 D 22.20 Datum status
81 - 256 176 A Filler
Telrecord
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 90)
3 - 11 9 N Totaal aantal gegevensrecords deelbestand
12 - 256 245 A Filler
6. Indien records met onderbouwingen taxaties WOZ-objecten worden geleverd
volgt hierna (anders vervolg bij 7):
Stuurrecord
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 10)
3 - 4 2 N 93.21 Deelbestandsidentificatie Stuf-TAX (= 21)
5 - 256 252 A Filler
Gegevensrecords
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 21)
3 - 14 12 N 01.01 WOZ-objectnummer
15 - 17 3 N 11.61 Wijkcode
18 - 20 3 N 11.63 Buurtcode
21 - 31 11 N 15.11 Waarde onroerende-zaakbelastingen
32 - 32 1 A 15.12 Reden verschil vastgestelde waarde en waarde
onroerende-zaakbelastingen
33 - 43 11 N 15.15 Getaxeerde waarde
44 - 54 11 N 15.13 Heffingsmaatstaf onroerendezaak-belasting
gebruikers
55 - 72 18 A Filler
73 - 73 1 A 15.31 Gehanteerd waarderingsvoorschrift
74 - 74 1 A 15.32 Monumentaanduiding
75 - 75 1 A 15.33 Code omzetbelasting
76 - 83 8 A 15.41 Groepaanduiding vergelijkbare objecten
84 - 89 6 A 15.42 Type-aanduiding
90 - 93 4 A 61.10 Soort-object-code
94 - 105 12 A Filler
106 - 106 1 A 61.22 Aanwezigheid lift
107 - 113 7 A Filler
Gegevensrecords
positie lengte type nummer naam gegeven
114 - 115 2 A 61.31 Indicatie ligging
116 - 117 2 A Filler
118 - 120 3 A 61.37 Code ontbreken nutsvoorzieningen
121 - 152 32 A Filler
153 - 160 8 N 65.06 Foto-indexnummer
161 - 162 2 A 61.70 Financieringsvorm
163 - 212 50 A 68.10 Aantekening
213 - 220 8 D 69.10 Taxatiedatum
221 - 224 4 A 69.11 Taxateur
225 - 225 1 A 69.12 Inpandige opname
226 - 226 1 A 69.13 Stijlletter
227 - 229 3 N 69.15 Percentage gereed
230 - 230 1 A 81.10 Mutatiecode
231 - 238 8 D 81.20 Ingangsdatum
239 - 246 8 D 81.30 Einddatum
247 - 254 8 D 15.21 Toestandspeildatum
255 - 256 2 A Filler
Telrecord
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 90)
3 - 11 9 N Totaal aantal gegevensrecords deelbestand
12 - 24 13 N Totaaltelling 15.11
25 - 37 13 N Totaaltelling 15.15
38 - 256 219 A Filler
7. Indien records met onderdelen voor onderbouwingen taxaties WOZ-objecten
worden geleverd volgt hierna (anders vervolg bij 8):
Stuurrecord
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 10)
3 - 4 2 N 93.21 Deelbestandsidentificatie Stuf-TAX (= 22)
5 - 256 252 A Filler
Gegevensrecords
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 22)
3 - 14 12 N 01.01 WOZ-objectnummer
15 - 20 6 N 11.71 Nummer onderdeel
21 - 21 1 A 15.31 Gehanteerd waarderingsvoorschrift
Gegevensrecords
positie lengte type nummer naam gegeven
22 - 22 1 A 15.33 Code omzetbelasting
23 - 30 8 A 15.41 Groepaanduiding vergelijkbare objecten en/of
objectdelen
31 - 40 10 N 15.60 Bepaalde waarde onderdeel
41 - 41 1 A 15.51 Code vrijstelling OZB
42 - 45 4 A 61.15 Code onderdeel WOZ-object
46 - 54 9 A 61.20 Bouwjaar
55 - 57 3 N 61.21 Bouwlaag
58 - 60 3 A 61.23 Ontsluiting verdieping
61 - 62 2 N 61.25 Aantal kamers
63 - 66 4 N 61.28 Renovatiejaar
67 - 68 2 N 61.29 Renovatiepercentage
69 - 69 1 A 61.32 Kwaliteit/luxe
70 - 70 1 A 61.33 Onderhoudstoestand
71 - 71 1 A 61.34 Uitstraling
72 - 72 1 A 61.35 Doelmatigheid
73 - 76 4 A 61.36 Voorzieningen
77 - 82 6 N 61.41 Inhoud
83 - 83 1 A 61.42 Code bruto/netto inhoud
84 - 89 6 N 61.43 Oppervlakte
90 - 90 1 A 61.44 Code bruto/netto oppervlakte
91 - 94 4 N 61.45 Lengte
95 - 98 4 N 61.46 Breedte
99 - 102 4 N 61.47 Hoogte
103 - 106 4 N 61.49 Frontbreedte
107 - 110 4 N 62.11 Aantal stuks/eenheden
111 - 117 7 N 62.12 Waarde per stuk/eenheid
118 - 124 7 N 62.21 Huurwaarde per vierkante meter
125 - 134 10 N 62.22 Huurwaarde
135 - 137 3 N 62.23 Kapitalisatiefactor
138 - 144 7 N 62.30 Vervangingskosten per kubieke meter/ stuk/
eenheid
145 - 155 11 N 62.31 Ongecorrigeerde vervangingswaarde
156 - 158 3 N 62.32 Verwachte levensduur
159 - 161 3 N 62.33 Restwaarde
162 - 165 4 N 62.34 Factor voor technische veroudering
166 - 169 4 N 62.35 Invloed economische veroudering
170 - 173 4 N 62.36 Invloed verandering bouwwijze
174 - 177 4 N 62.37 Invloed doelmatigheid
178 - 181 4 N 62.38 Invloed excessieve gebruikskosten
182 - 185 4 N 62.39 Factor voor functionele veroudering
186 - 215 30 A 68.11 Aantekening onderdeel
216 - 216 1 A 69.14 Code taxatiemethodiek
217 - 217 1 A 81.10 Mutatiecode
Gegevensrecords
positie lengte type nummer naam gegeven
218 - 225 8 D 81.20 Ingangsdatum
226 - 233 8 D 81.30 Einddatum
234 - 241 8 N 65.06 Foto-indexnummer
242 - 256 15 A Filler
Telrecord
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 90)
3 - 11 9 N Totaal aantal gegevensrecords deelbestand
12 - 24 13 N Totaaltelling 15.60
25 - 34 10 N Totaaltelling 61.41
35 - 256 222 A Filler
8. Indien records met identificaties marktinformatie worden geleverd worden
geleverd volgt hierna (anders vervolg bij 9):
Stuurrecord
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 10)
3 - 4 2 N 93.21 Deelbestandsidentificatie Stuf-TAX (= 23)
5 - 256 252 A Filler
Gegevensrecords
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 23)
3 - 10 8 A 15.41 Groepaanduiding vergelijkbare objecten en/of
objectdelen
11 - 11 1 A 63.10 Indicatie vermelding op taxatieverslag
12 - 18 7 A Filler
19 - 30 12 N 63.11 WOZ-objectnummer referentie-object
31 - 38 8 N 65.01 Volgnummer marktgegeven
39 - 39 1 A 81.10 Mutatiecode
40 - 47 8 D 81.20 Ingangsdatum
48 - 55 8 D 81.30 Einddatum
56 - 256 201 A Filler
Telrecord
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 90)
3 - 11 9 N Totaal aantal gegevensrecords deelbestand
12 - 256 245 A Filler
9. Indien records met tussentijdse taxaties worden geleverd volgt hierna
(anders vervolg bij 10):
Stuurrecord
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 10)
3 - 4 2 N 93.21 Deelbestandsidentificatie Stuf-TAX (= 24)
5 - 256 252 A Filler
Gegevensrecords
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 24)
3 - 14 12 N 01.01 WOZ-objectnummer
15 - 17 3 N 69.40 Volgnummer tussentijdse taxatie
18 - 27 10 N 15.19 Waardeverandering van de mutatie
28 - 35 8 D 15.20 Waardepeildatum
36 - 36 1 A 69.50 Reden tussentijdse taxatie
37 - 38 2 A 69.51 Resultaat tussentijdse taxatie
39 - 48 10 A 69.60 Nummer bezwaarschrift
49 - 49 1 A 69.61 Indiener bezwaarschrift
50 - 79 30 A 69.62 Gemachtigde
80 - 89 10 A 69.70 Nummer bouwvergunning
90 - 100 11 N 69.71 Opgegeven kosten verbouwing
101 - 101 1 A 81.10 Mutatiecode
102 - 109 8 D 81.20 Ingangsdatum
110 - 117 8 D 81.30 Einddatum
118 - 120 3 A 15.40 Aanduiding valutasoort
121 - 123 3 A 69.73 Valuta kosten verbouwing
124 - 163 40 A 69.72 Omschrijving vergunning
164 - 256 93 A Filler
Telrecord
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 90)
3 - 11 9 N Totaal aantal gegevensrecords deelbestand
12 - 24 13 N Totaaltelling 15.19
25 - 37 13 N Totaaltelling 69.71
38 - 256 219 A Filler
10. Indien records met gegevens ten behoeve van procesmatige controle
objectkenmerken worden geleverd volgt hierna (anders vervolg bij 11):
Stuurrecord
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 10)
3 - 4 2 N 93.21 Deelbestandsidentificatie Stuf-TAX (= 25)
5 - 256 252 A Filler
Gegevensrecords
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 25)
3 - 14 12 N 01.01 WOZ-objectnummer
15 - 64 50 A 68.10 Aantekening
65 - 68 4 A 69.11 Taxateur
69 - 76 8 D 70.10 Datum controle
77 - 78 2 N 70.11 Reden controle
79 - 79 1 N 70.12 Gecontroleerde onderdelen
80 - 80 1 N 70.13 Gecontroleerde objectkenmerken
81 - 81 1 A 70.14 Identificatie uitvoerder
82 - 83 2 N 70.15 Methodiek controle
84 - 84 1 A 81.10 Mutatiecode
85 - 92 8 D 81.20 Ingangsdatum
93 - 100 8 D 81.30 Einddatum
101 - 256 156 A Filler
Telrecord
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 90)
3 - 11 9 N Totaal aantal gegevensrecords deelbestand
12 - 256 245 A Filler
11. Indien records met identificaties extra adressen worden geleverd volgt hierna
(anders vervolg bij 12):
Stuurrecord
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 10)
3 - 4 2 N 93.21 Deelbestandsidentificatie Stuf-TAX (= 35)
5 - 256 252 A Filler
Gegevensrecords
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 35)
3 - 14 12 N 01.01 WOZ-objectnummer
15 - 54 40 A 10.20 Woonplaatsnaam
55 - 78 24 A 11.10 Straatnaam
79 - 83 5 N 11.20 Huisnummer
84 - 84 1 A 11.30 Huisletter
85 - 88 4 A 11.40 Huisnummertoevoeging
89 - 90 2 A 11.50 Aanduiding bij huisnummer
91 - 96 6 A 11.60 Postcode
97 - 136 40 A 11.70 Lokatieomschrijving
137 - 137 1 A 81.10 Mutatiecode
138 - 145 8 D 81.20 Ingangsdatum
146 - 153 8 D 81.30 Einddatum
154 - 158 5 N 11.11 Straatcode
159 - 256 98 A Filler
Telrecord
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 90)
3 - 11 9 N Totaal aantal gegevensrecords deelbestand
12 - 256 245 A Filler
12. Indien records met identificaties sluimerende WOZ-objecten worden
geleverd volgt hierna (anders vervolg bij 13):
Stuurrecord
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 10)
3 - 4 2 N 93.21 Deelbestandsidentificatie Stuf-TAX (= 41)
5 - 256 252 A Filler
Gegevensrecords
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 41)
3 - 14 12 N 01.01 WOZ-objectnummer
15 - 26 12 N 53.01 WOZ-objectnummer sluimerend WOZ-object
27 - 27 1 A 81.10 Mutatiecode
28 - 35 8 D 81.20 Ingangsdatum
36 - 43 8 D 81.30 Einddatum
44 - 256 213 A Filler
Telrecord
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 90)
3 - 11 9 N Totaal aantal gegevensrecords deelbestand
12 - 256 245 A Filler
13 Indien records met kadastrale objecten worden geleverd volgt hierna (anders
vervolg bij 14):
Stuurrecord
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 10)
3 - 4 2 N 93.21 Deelbestandsidentificatie Stuf-TAX (= 51)
5 - 256 252 A Filler
Gegevensrecords
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 51)
3 - 7 5 A 51.10 Kadastrale gemeentecode
8 - 9 2 A 51.20 Sectie
10 - 14 5 N 51.30 Perceelnummer
15 - 15 1 A 51.40 Perceel-index-letter
16 - 19 4 N 51.50 Perceel-index-nummer
20 - 27 8 N 53.10 Kadastrale oppervlakte
28 - 28 1 N 53.20 Bebouwingscode
29 - 31 3 N 54.10 Kaartbladnummer
32 - 32 1 N 54.11 Kaartbladvolgnummer
33 - 33 1 A 54.12 Ruitletter
34 - 35 2 N 54.13 Ruitnummer
36 - 41 6 N 54.15 X-coördinaat
42 - 47 6 N 54.16 Y-coördinaat
48 - 50 3 A 54.20 Registercode
51 - 55 5 A 54.21 Stuknummer
56 - 56 1 A 81.10 Mutatiecode
57 - 64 8 D 81.20 Ingangsdatum
65 - 72 8 D 81.30 Einddatum
73 - 256 184 A Filler
Telrecord
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 90)
3 - 11 9 N Totaal aantal gegevensrecords deelbestand
12 - 22 11 N Totaaltelling 53.10
23 - 256 234 A Filler
14. Indien records met marktinformatie worden geleverd volgt hierna (anders
vervolg bij 15):
Stuurrecord
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 10)
3 - 4 2 N 93.21 Deelbestandsidentificatie Stuf-TAX (= 52)
5 - 256 252 A Filler
Gegevensrecords
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 52)
3 - 10 8 N 65.01 Volgnummer marktgegeven
11 - 50 40 A 65.02 Omschrijving transactie
51 - 51 1 A 65.03 Aard marktinformatie
52 - 52 1 A 65.04 Soort transactie
53 - 54 2 A 65.05 Aanduiding bruikbaarheid marktgegeven
55 - 62 8 D 65.10 Datum transactie
63 - 72 10 N 65.11 Transactieprijs
73 - 82 10 N 65.20 Transactieprijs geïndexeerd naar
waardepeildatum
83 - 85 3 N 65.30 Looptijd huurcontract
86 - 86 1 A 65.31 Indexatie huurprijs
87 - 87 1 A 65.32 Indicatie omzetbelasting op huurprijs
88 - 97 10 N 65.33 Servicekosten
98 - 103 6 N 65.34 Totale bruto vloeroppervlakte
104 - 109 6 N 65.35 Verhuurbare vloeroppervlakte primaire ruimte
110 - 115 6 N 65.36 Huurprijs per vierkante meter primaire ruimte
116 - 121 6 N 65.37 Geschatte huurprijs per vierkante meter geïndexeerd
naar waardepeildatum
122 - 130 9 N 65.40 Grondkosten
131 - 139 9 N 65.41 Kosten ruwbouw
140 - 148 9 N 65.42 Kosten afbouw/inrichting
149 - 157 9 N 65.43 Kosten installaties
158 - 166 9 N 65.44 Kosten werktuigen
167 - 175 9 N 65.45 Kosten infrastructuur
176 - 184 9 N 65.46 Overige kosten
185 - 192 8 N 65.47 Totale bruto inhoud object
193 - 193 1 A 65.50 Indicatie omzetbelasting op grondprijs
194 - 201 8 N 65.51 Totale oppervlakte grond
202 - 207 6 N 65.52 Gronduitgifteprijs per vierkante meter
208 - 208 1 A 81.10 Mutatiecode
Gegevensrecords
positie lengte type nummer naam gegeven
209 - 216 8 D 81.20 Ingangsdatum
217 - 224 8 D 81.30 Einddatum
225 - 227 3 A 15.40 Aanduiding valutasoort
228 - 256 29 A Filler
Telrecord
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 90)
3 - 11 9 N Totaal aantal gegevensrecords deelbestand
12 - 24 13 N Totaaltelling 65.11
25 - 256 232 A Filler
15. Indien records met identificaties WOZ-objecten betrokken bij marktgegevens
worden geleverd volgt hierna (anders vervolg bij 16):
Stuurrecord
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 10)
3 - 4 2 N 93.21 Deelbestandsidentificatie Stuf-TAX (= 53)
5 - 256 252 A Filler
Gegevensrecords
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 53)
3 - 14 12 N 01.01 WOZ-objectnummer
15 - 22 8 N 65.01 Volgnummer marktgegeven
23 - 23 1 A 81.10 Mutatiecode
24 - 31 8 D 81.20 Ingangsdatum
32 - 39 8 D 81.30 Einddatum
40 - 47 8 N 65.06 Foto-indexnummer
48 - 58 11 N 15.14 Vastgestelde waarde op datum marktgegeven
59 - 66 8 D 65.15 Koopdatum
67 - 70 4 N 65.21 Verwachte wijziging vastgestelde waarde
71 - 72 2 N 65.22 Reden afwijking tussen marktgegeven en op
WOZ-gegevens gebaseerde verwachting
73 - 73 1 N 65.23 Relevantie reden afwijking
74 - 83 10 N 65.24 Kwantificering verschil verkoopprijs t.o.v.
marktwaarde
84 - 93 10 N 65.25 Kwantificering fout oude vastgestelde waarde
94 - 103 10 N 65.26 Kwantificering gevolg wijziging WOZ-object
104 - 256 153 A Filler
Gegevensrecords
positie lengte type nummer naam gegeven
Telrecord
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 90)
3 - 11 9 N Totaal aantal gegevensrecords deelbestand
12 - 24 13 N Totaaltelling 15.14
25 - 256 232 A Filler
16. Indien records met kadastrale identificaties marktgegevens worden geleverd
volgt hierna (anders vervolg bij 17):
Stuurrecord
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 10)
3 - 4 2 N 93.21 Deelbestandsidentificatie Stuf-TAX (= 54)
5 - 256 252 A Filler
Gegevensrecords
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 54)
3 - 10 8 N 65.01 Volgnummer marktgegeven
11 - 15 5 A 51.10 Kadastrale gemeentecode
16 - 17 2 A 51.20 Sectie
18 - 22 5 N 51.30 Perceelnummer
23 - 23 1 A 51.40 Perceel-index-letter
24 - 27 4 N 51.50 Perceel-index-nummer
28 - 28 1 A 81.10 Mutatiecode
29 - 36 8 D 81.20 Ingangsdatum
37 - 44 8 D 81.30 Einddatum
45 - 256 212 A Filler
Telrecord
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 90)
3 - 11 9 N Totaal aantal gegevensrecords deelbestand
12 - 256 245 A Filler
17. Indien records met codering soort object worden geleverd volgt hierna
(anders vervolg bij 18):
Stuurrecord
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 10)
3 - 4 2 N 93.21 Deelbestandsidentificatie Stuf-TAX (= 91)
5 - 256 252 A Filler
Gegevensrecords
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 91)
3 - 6 4 A 61.10 Soort-object-code
7 - 56 50 A 61.11 Omschrijving soort object
57 - 68 12 A 61.12 Verkorte omschrijving soort object
69 - 69 1 A 81.10 Mutatiecode
70 - 77 8 D 81.20 Ingangsdatum
78 - 85 8 D 81.30 Einddatum
86 - 256 171 A Filler
Telrecord
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 90)
3 - 11 9 N Totaal aantal gegevensrecords deelbestand
12 - 256 245 A Filler
18. Indien records met codering onderdelen WOZ-object worden geleverd volgt
hierna (anders vervolg bij 19):
Stuurrecord
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 10)
3 - 4 2 N 93.21 Deelbestandsidentificatie Stuf-TAX (= 92)
5 - 256 252 A Filler
Gegevensrecords
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 92)
3 - 6 4 A 61.15 Code onderdeel WOZ-object
7 - 56 50 A 61.16 Omschrijving onderdeel WOZ-object
57 - 68 12 A 61.17 Verkorte omschrijving onderdeel WOZ-object
69 - 69 1 A 81.10 Mutatiecode
70 - 77 8 D 81.20 Ingangsdatum
78 - 85 8 D 81.30 Einddatum
86 - 256 171 A Filler
Telrecord
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode Stuf-TAX (= 90)
3 - 11 9 N Totaal aantal gegevensrecords deelbestand
12 - 256 245 A Filler
19. Leveringsbestand eindigt altijd met:
Sluitrecord
positie lengte type nummer naam gegeven
1 - 2 2 N 93.11 Recordidentificatiecode (= 98)
3 - 11 9 N Totaal aantal records code = 10
12 - 20 9 N Totaal aantal records code = 20
21 - 29 9 N Totaal aantal records code = 21
30 - 38 9 N Totaal aantal records code = 22
39 - 47 9 N Totaal aantal records code = 23
48 - 56 9 N Totaal aantal records code = 24
57 - 65 9 N Totaal aantal records code = 30
66 - 74 9 N Totaal aantal records code = 31
75 - 83 9 N Totaal aantal records code = 35
84 - 92 9 N Totaal aantal records code = 40
93 - 101 9 N Totaal aantal records code = 41
102 - 110 9 N Totaal aantal records code = 51
111 - 119 9 N Totaal aantal records code = 52
120 - 128 9 N Totaal aantal records code = 53
129 - 137 9 N Totaal aantal records code = 54
138 - 146 9 N Totaal aantal records code = 60
147 - 155 9 N Totaal aantal records code = 80
156 - 164 9 A Filler
165 - 173 9 N Totaal aantal records code = 90
174 - 182 9 N Totaal aantal records code = 91
183 - 191 9 N Totaal aantal records code = 92
192 - 209 18 A Filler
210 - 218 9 N Totaal aantal records code = 25
219 - 256 38 A Filler
END_OF_RECORD_LAYOUT

    my @lines = split /\n/, $input;
    return \@lines;
}



sub parse_line {
    my ($self, $arguments) = @_;

    my $line = $arguments->{line} or die "need line";

    my $record_id = $self->id_parser->parse($line)->{record_id};

    return unless exists ACTIVE_RECORD_IDS->{$record_id};

    # obtain the parser object for this line type
    my $parser = $self->_parser({ record_id => $record_id }) or die "need parser";

    #warn "parser: " . Dumper $parser;
    my $record = $parser->parse_newref($line) or die "need parsed record";

    #warn "record: " . Dumper $record;
    $self->removeFiller($record);

    if($record_id eq '23') {
        my $groepaanduiding = $record->{'Groepaanduiding vergelijkbare objecten en/of'};

        push @{ $self->vergelijkbare_objecten->{$groepaanduiding} }, $record;
     }
    # bedrijf/organisatie
    # other objects are assumed to be owned by a natuurlijk_persoon
    elsif($record_id eq '31') {
        my $sofi_nummer             = int $record->{'SoFi-nummer'};
        my $handelsregisternummer   = int $record->{'Handelsregisternummer'};

        if($sofi_nummer && $handelsregisternummer) {
            warn "sofi: $sofi_nummer, handelsregisternummer: $handelsregisternummer";
            $self->bedrijven->{$sofi_nummer} = $handelsregisternummer;
        }
    }
    elsif($record_id eq '52') {
        my $volgnummer_marktgegeven = $record->{'Volgnummer marktgegeven'};
        $self->marktinformatie->{$volgnummer_marktgegeven} = $record;
    }
    elsif($record_id eq '60') {
        $self->parse_identificatie({ record => $record });
    } else {
        $self->parse_general({ record => $record });
    }
}


sub parse_general {
    my ($self, $arguments) = @_;

    my $record = $arguments->{record} or die "need record";

    my $record_id = $record->{record_id} or die Dumper $record;

    my $woz_object_id = int $record->{'WOZ-objectnummer'}
        or die "woz_id empty" . Dumper($record);

    if($record_id eq '20') {
        $record->{Huisnummer} = int $record->{Huisnummer};
        $self->woz_objects->{$woz_object_id} = $record;
        return;
    }

    my $type = ACTIVE_RECORD_IDS->{$record_id} or die "need type for ID = " . $record_id;

    if(RECORDTYPES_MULTIPLE_LOOKUP->{$type}) {
        my $raw = $self->woz_objects->{$woz_object_id}->{$type} ||= [];
        push @$raw, $record;
    } else {
        warn "supposed to be one on one: " . $type if $self->woz_objects->{$woz_object_id}->{$type};
        $self->woz_objects->{$woz_object_id}->{$type} = $record;
    }
}

# these records link WOZ objects to Sofi nummers.
# if the sofi nummer appears in the bedrijven hash, we assume it's a bedrijf
# a record is made in woz_owners with key:
# owner = bedrijf-[handelsregisternummer] or
#         natuurlijk_persoon-[sofinummer]
# the record is a list format, since it can consist of multiple records
sub parse_identificatie {
    my ($self, $arguments) = @_;

    my $record = $arguments->{record} or die "need record";

    my $sofi_nummer  = int $record->{'SoFi-nummer'};

    return unless $sofi_nummer;

    my $woz_object_id = int $record->{'WOZ-objectnummer'}
        or die "woz_id empty" . Dumper($record);

    my $woz_object = $self->woz_objects->{$woz_object_id};
    return unless $woz_object;

    my $owner;

    if(exists $self->bedrijven->{$sofi_nummer}) {
        my $handelsregisternummer = $self->bedrijven->{$sofi_nummer};
        $owner = 'bedrijf-' . $handelsregisternummer;
    } else {
        $owner = 'natuurlijk_persoon-' . $sofi_nummer;
    }

    my $aanduiding_eigenaar_gebruiker = $record->{'Aanduiding eigenaar/gebruiker'};
    my $list = $self->woz_owners->{$owner}->{$aanduiding_eigenaar_gebruiker} ||= [];
    push @$list, $woz_object;
}


__PACKAGE__->meta->make_immutable;

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 ACTIVE_RECORD_IDS

TODO: Fix the POD

=cut

=head2 BUILDARGS

TODO: Fix the POD

=cut

=head2 RECORDTYPES_MULTIPLE_LOOKUP

TODO: Fix the POD

=cut

=head2 find_vergelijkbare_objecten

TODO: Fix the POD

=cut

=head2 initialize_recordtypes

TODO: Fix the POD

=cut

=head2 parse_general

TODO: Fix the POD

=cut

=head2 parse_identificatie

TODO: Fix the POD

=cut

=head2 parse_line

TODO: Fix the POD

=cut

=head2 recordlayout_lines

TODO: Fix the POD

=cut

=head2 removeFiller

TODO: Fix the POD

=cut

