package Zaaksysteem::DB::ResultSet::ZaaktypeKenmerken;

use strict;
use warnings;

use Data::Dumper;
use Moose;
use Data::Dumper;

extends 'DBIx::Class::ResultSet', 'Zaaksysteem::Zaaktypen::BaseResultSet';

use Zaaksysteem::Constants qw/
    DEFAULT_KENMERKEN_GROUP_DATA
/;

use constant    PROFILE => {
    required        => [qw/
        bibliotheek_kenmerken_id
        label
    /],
    optional        => [qw/
        description
        help
        created
        pip
        publish_public
        zaakinformatie_view
        document_categorie
        value_mandatory
        value_default
        referential
        required_permissions
    /],
};





sub _validate_session {
    my $self            = shift;
    my $profile         = PROFILE;
    my $rv              = {};

    $self->__validate_session(@_, $profile);
}

sub search {
    my ($self, $search, @options) = @_;

    $search  ||= {};

    my $rv;
    if (!ref $search) {
        $rv = $self->next::method({$search, @options});
    }
    else {
        $rv = $self->next::method($search, @options);
    }
    return $rv;
}

sub _commit_session {
    my $self                    = shift;

    ### Remove old authorisations
    my $node                    = shift;
    my $element_session_data    = shift;

    my $rs = $self->result_source->schema->resultset('BibliotheekKenmerken');

    while (my ($key, $data) = each %{ $element_session_data }) {
        if (!$data->{object_metadata}) {
            $data->{object_metadata} = {};
        }

        if ($data->{type} && $data->{type} eq 'objecttype') {
            # Object types are special snowflakes
            if ($data->{bibliotheek_kenmerken_id}) {
                $data->{object_id} = delete $data->{bibliotheek_kenmerken_id};
            }

            if (!$data->{label} || ($data->{naam} && $data->{naam} ne $data->{label})) {
                $data->{label} = delete $data->{naam};
            }
        }
        else {
            unless (
                (
                    $data->{naam} &&
                    $data->{bibliotheek_kenmerken_id}
                ) ||
                $data->{is_group}
            ) {
                delete($element_session_data->{$key});
                next;
            }

            # save the actual version of the bibliotheek_kenmerk, so that using
            # the logging table, administrator can figure out the state of this
            # moment. This is due to a requirement of casetype administration and
            # maintenance.
            if ($data->{bibliotheek_kenmerken_id}) {

                # this line purposely dies when the item is not found. the database
                # has constraints that should prevent this situation from happening.
                # a 'die' on this line means that the database needs to be sanitized.
                $data->{version} = $rs->find($data->{bibliotheek_kenmerken_id})->version;
            }
        }
    }

    $self->next::method( $node, $element_session_data );
}


sub _retrieve_as_session {
    my $self            = shift;

    my $rv              = $self->next::method({
        search  => {
            is_group    => [ 1, undef ],
        }
    });

    return $rv unless UNIVERSAL::isa($rv, 'HASH');

    # The first non-object kenmerk should have a group header. This adds one if
    # it's missing.
    if (keys %{ $rv }) {
        my $obj_counter = 1;

        while ($rv->{$obj_counter}->{object_id}) {
            $obj_counter++;
        }

        if(!$rv->{$obj_counter}->{is_group}) {
            my $group_info  = DEFAULT_KENMERKEN_GROUP_DATA;

            my $newrv       = {};

            for (my $counter = 1; $counter < $obj_counter; $counter++) {
                $newrv->{ $counter } = $rv->{ $counter };
            }

            $newrv->{$obj_counter} = $self->_get_session_template;
            $newrv->{$obj_counter}->{zaaktype_node_id} = $rv->{$obj_counter}->{zaaktype_node_id};
            $newrv->{$obj_counter}->{zaak_status_id}   = $rv->{$obj_counter}->{zaak_status_id};

            $newrv->{$obj_counter}->{is_group}         = 1;
            $newrv->{$obj_counter}->{help}             = $group_info->{help};
            $newrv->{$obj_counter}->{label}            = $group_info->{label};

            for (my $counter = $obj_counter + 1; $counter <= (scalar( keys %{ $rv }) + 1); $counter++) {
                $newrv->{ $counter } = $rv->{($counter - 1)};
            }

            $rv = $newrv;
        }
    }

    return $rv;
}

sub search_fase_kenmerken {
    my $self        = shift;
    my $fase        = shift;

    return $self->search(
        {
            zaak_status_id     => $fase->id,
        },
        {
            'order_by'  => { '-asc' => 'me.id' },
            'prefetch'  => 'bibliotheek_kenmerken_id'
        }
    );
}


=head2 search_update_field_tasks

Find out how many open update_field requests are outstanding
for this phase.

In the PIP, a citizen will request a change to a field. This will
end up into the 'scheduled_job' table where it is queued until the case owner
(behandelaar) will handle it - approve or deny.

This indicates to the owner how many requests are present for this phase.

=cut

sub search_update_field_tasks {
    my ($self, $arguments) = @_;

    my $phase_id = $arguments->{phase_id} or die "need phase_id";
    my $case     = $arguments->{case}     or die "need case";

    my @kenmerken = $self->search({
        zaak_status_id => $phase_id,
        bibliotheek_kenmerken_id => { -not => undef }
    })->get_column('bibliotheek_kenmerken_id')->all;

    # no point pretending, if there's no fields we have no business here
    # since we're all about fields, bitch.
    return [] unless @kenmerken;

    my $rs = $self->result_source->schema->resultset('ScheduledJobs');

    my @tasks = $rs->search_update_field_tasks({
        case_id   => $case->id,
        kenmerken => \@kenmerken
    })->only_most_recent_field_update;

    # supply the caller with a map of the affected fields, and an id to the scheduled_jobs
    # row, as to allow for speedy lookup.
    return { map { $_->parameters->{bibliotheek_kenmerken_id} => $_->id } @tasks };
}




=head2 mangle_defaults(\@given_properties) || mangle_defaults(\%given_properties)

Returns a array-/hashref of parameters stemming from the current given
properties plus the ones having a default property according to our library.

B<Examples>

  # Given an ARRAYREF
    my $props = $rs->mangle_defaults([
        { 1     => 'User overwritten beer brew: Amstel' },
        { 3     => 'Hertog' }
    ]);

    print Dumper($props);

    #returns:
    #    $VAR1 = [
    #        { 1     => 'User overwritten beer brew: Amstel' },
    #        { 3     => 'Hertog' },
    #        { 2     => 'Amstel' }
    #    ];

  # Given an HASHREF
    my $props = $rs->mangle_defaults(
         1     => 'User overwritten beer brew: Amstel',
         3     => 'Hertog'
    ]);

    print Dumper($props);

    #returns:
    #    $VAR1 = {
    #         1     => 'User overwritten beer brew: Amstel',
    #         3     => 'Hertog',
    #         2     => 'Amstel'
    #    };

=cut

sub mangle_defaults {
    my ($self, $user_properties) = @_;

    die('User_properties undefined or not an ARRAYREF/HASHREF')
        unless (
            UNIVERSAL::isa($user_properties, 'ARRAY') ||
            UNIVERSAL::isa($user_properties, 'HASH')
        );

    my $return_properties   = {};
    if (UNIVERSAL::isa($user_properties, 'ARRAY')) {
        ### Make this set of data machine readable
        for my $user_prop (@{ $user_properties }) {
            my ($bibliotheek_kenmerken_id, $values) = each %$user_prop;
            $return_properties->{$bibliotheek_kenmerken_id} = $values;
        }
    } else {
        $return_properties  = { %{ $user_properties } };

    }

    # TODO - it looks like this query also gets defaults from later phases. restrict?
    # or is there a good reason to do this right away?
    my $properties          = $self->search(
        {
            is_group                                    => undef,
            'bibliotheek_kenmerken_id.id'               => { '!='   => undef },
            'bibliotheek_kenmerken_id.value_type'       => { '!='   => 'file' },
            'bibliotheek_kenmerken_id.value_default'    => { '!='   => undef },
            'bibliotheek_kenmerken_id.id'           => {
                'NOT IN' => [ keys %{ $return_properties } ]
            }
        },
        {
            prefetch    => 'bibliotheek_kenmerken_id'
        }
    );

    while(my $property = $properties->next) {
        my $value_default   = $property
                            ->bibliotheek_kenmerken_id
                            ->value_default;

        ### Value maybe defined, but could be empty. In our context: this
        ### means still 'defined'
        next unless $value_default;

        my $bib_id          = $property
                            ->bibliotheek_kenmerken_id
                            ->id;

        ### Overdone test below, because the IN query above should sort this
        ### out...but by the the concept of not having tests
        next if defined($return_properties->{ $bib_id });

        $return_properties->{ $bib_id } = $value_default;
    }

    ### Make this set of data back to zaak readable
    if (UNIVERSAL::isa($user_properties, 'ARRAY')) {
        my @mangled_params;
        for my $key (keys %{ $return_properties }) {
            push(
                @mangled_params,
                { $key  => $return_properties->{ $key } }
            );
        }
        return \@mangled_params;
    } else {
        return $return_properties;
    }

    return;
}

=head2 search_by_magic_strings

This convenience method returns a
L<Zaaksysteem::DB::ResultSet::BibliotheekKenmerken>-joined resultset. The
resultset will be constraint based on the BibliotheekKenmerken's
C<magic_string> field.

    my $rs = $kenmerken->search_by_magic_strings(qw[title description etc]);

=cut

sub search_by_magic_strings {
    my ($self, @magic_strings) = @_;

    return $self->search(
        {
            'bibliotheek_kenmerken_id.magic_string' => {
                -in => \@magic_strings
            }
        },
        {
            prefetch => 'bibliotheek_kenmerken_id'
        }
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 search

TODO: Fix the POD

=cut

=head2 search_fase_kenmerken

TODO: Fix the POD

=cut

