package Zaaksysteem::Log::LoggerRole;
use Moose::Role;

use Log::Log4perl;

=head1 NAME

Zaaksysteem::Log::LoggerRole - Role to embed loggability into random objects

=head1 DESCRIPTION

The idea behind this role is not that the logic for adding logging to a random
object is hard to do, but that it should be there implicitly always. Also,
checking for the definedness of a log object before logging is cumbersome, so
when this role is applied it's always safe to call log methods on the L</log>
method.

=head1 ATTRIBUTES

=head2 log

This method returns a reference to a L<Log::Log4perl::Logger> (or
derivative) object.

=cut

sub log {
    my $self = shift;
    return Log::Log4perl->get_logger(ref($self) || $self);
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
