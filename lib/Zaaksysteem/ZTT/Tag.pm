package Zaaksysteem::ZTT::Tag;

use Moose;

=head1 NAME

Zaaksysteem::ZTT::Tag - Package for wrapping data regarding a tag found in
templates

=head1 ATTRIBUTES

=head2 name

=cut

has name => (
    is => 'ro',
    isa => 'Str', required => 1
);

=head2 formatter

=cut

has formatter => (
    is => 'ro',
    isa => 'Maybe[HashRef]'
);

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
