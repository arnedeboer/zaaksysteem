package Zaaksysteem::ZTT::MagicDirective;

use Moose;

use Parse::RecDescent;

use Zaaksysteem::Tools;

=head1 NAME

Zaaksysteem::ZTT::MagicDirective - Parse augmented magic-string syntax

=head1 DESCRIPTION

This package abstracts the syntax used in ZTT templates to render
'magic strings'.

    my $str1 = 'case.id';
    my $str2 = 'case.id | link_to_case';
    my $str3 = 'case.startdate | date';
    my $str4 = 'case.id | link_to(case, "Zaak %d")'

    my $directive = Zaaksysteem::ZTT::MagicDirective->parse($str2);

    warn $directive->{ attribute };    # Will print 'case.id'

For a better description of the return value of this call, see L</parse>.

=head1 ATTRIBUTES

=head2 parser

This attribute holds a reference to the L<Parse::RecDescent> object used to
parse directives.

=cut

has parser => (
    is => 'rw',
    isa => 'Parse::RecDescent',
    default => sub {
        # $::RD_HINT = 1;
        # $::RD_WARN = 1;
        # $::RD_TRACE = 1;

        return Parse::RecDescent->new(MAGIC_DIRECTIVE_GRAMMAR->());
    }
);

=head1 CONSTANTS

=head2 MAGIC_DIRECTIVE_GRAMMAR

This constant is the grammar that will be fed to L<Parse::RecDescent>

=cut

use constant MAGIC_DIRECTIVE_GRAMMAR => <<'EOG';
# Main production of magic directive grammar
magic_directive : attribute filter(?) /\Z/ {
    $return = {
        attribute => lc($item{ attribute }{ name }),
        iterate_context => $item{ attribute }{ iterate_context },
        filter => $item{ 'filter(?)' }[0],
    };
}

# An attribute can be of the iterate-inline variant, or a plain bareword
attribute : /itereer\:/i bareword /\:/ bareword { $return = { name => $item[4], iterate_context => lc($item[2]) } }
          | bareword { $return = { name => $item{ bareword } } }

bareword : /[\w\.]+/

# A filter begins with a pipe, followed by a filtername and perhaps arguments
filter : "|" bareword filter_args(?) {
    $return = {
        name => lc($item{ bareword }),
        args => $item{ 'filter_args(?)'}[0] || []
    }
}

# Arguments appear between the '(' and ')' grouping chars, returns an
# arrayref of the arguments
filter_args : '(' arg(s? /,/) ')' {
    $return = $item[2]
}

# An argument can be bareword, or a quoted string (for space inclusion)
# Arguments are positional, no key-value-pair crud here pls
arg : bareword | string

# Non-greedily match all chars between two '"' chars, skip over escaped '\"'
# return the matched string sans quote chars
string : /"(.*?)(?<!\\)"/ {
    $return = $1
}
EOG

=head1 METHODS

=head2 parse

This method takes a string and attempts to parse the string as a magic
directive. The return-value is a hashref with the sanitized data.

    my $directive = Zaaksysteem::ZTT::MagicDirective->parse('...');

The returned hashref will look something like this:

    {
        attribute => '...',

        # Other keys are optional
        iterate_context => '...',

        filter => {
            name => '...'
            args => [       # Args will always be strings
                '...',
                '...'
            ]
        }
    }

=cut

sub parse {
    my $self = shift;
    my $text = shift;

    my $data;

    # No need to catch, on failure return implicit undef.
    try {
        $data = $self->parser->magic_directive($text);
    };

    # Early silent return on parse-errors
    unless (defined $data && $data->{ attribute }) {
        return;
    }

    # Strip empty fields for API compliance
    for my $key (keys %{ $data }) {
        delete $data->{ $key } unless $data->{ $key };
    }

    return $data;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 KNOWN BUGS

May summon the Dark Lord, Cthulhu

     Ph'nglui mglw'nafh Cthulhu R'lyeh wgah'nagl fhtagn

                            .....
                       .d$$$$*$$$$$$bc
                    .d$P"    d$$    "*$$.
                   d$"      4$"$$      "$$.
                 4$P        $F ^$F       "$c
                z$%        d$   3$        ^$L
               4$$$$$$$$$$$$$$$$$$$$$$$$$$$$$F
               $$$F"""""""$F""""""$F"""""C$$*$
              .$%"$$e    d$       3$   z$$"  $F
              4$    *$$.4$"        $$d$P"    $$
              4$      ^*$$.       .d$F       $$
              4$       d$"$$c   z$$"3$       $F
               $L     4$"  ^*$$$P"   $$     4$"
               3$     $F   .d$P$$e   ^$F    $P
                $$   d$  .$$"    "$$c 3$   d$
                 *$.4$"z$$"        ^*$$$$ $$
                  "$$$$P"             "$$$P
                    *$b.             .d$P"
                      "$$$ec.....ze$$$"
                          "**$$$**""

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
