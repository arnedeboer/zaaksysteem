package Zaaksysteem::ZTT::Selection;

use Moose;

=head1 NAME

Zaaksysteem::ZTT::Selection - Represent an abstract selection of text in a
template

=head1 ATTRIBUTES

=head2 tag

This attribute holds a reference to a L<Zaaksysteem::ZTT::Tag> object that was
detected in the selection.

=cut

has tag => (
    is => 'ro',
    isa => 'Zaaksysteem::ZTT::Tag'
);

=head2 selection

This attribute holds a string representation of the complete selection.

=cut

has selection => (
    is => 'ro',
    isa => 'Str'
);

=head2 iterate

=cut

has iterate => (
    is => 'ro',
    isa => 'Str'
);

=head2 subtemplate

This attribute holds a flattened string representing the whole of a
subtemplate.

=cut

has subtemplate => (
    is => 'ro',
    isa => 'Str'
);

=head2 name

This attribute may hold a string representing the name of the selection

=cut

has name => (
    is => 'ro',
    isa => 'Maybe[Str]'
);

__PACKAGE__->meta->make_immutable;
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
