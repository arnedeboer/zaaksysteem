package Zaaksysteem::ZTT::Constants;
use warnings;
use strict;
use utf8;

use Exporter qw[import];

use List::MoreUtils qw[any];
use Zaaksysteem::Constants qw();

our @EXPORT = qw[
    ZTT_DEFAULT_OPERATORS
    ZTT_DEFAULT_FUNCTIONS
    ZTT_DEFAULT_CONSTANTS
];

our @EXPORT_OK = qw[
    ZTT_DEFAULT_OPERATORS
    ZTT_DEFAULT_FUNCTIONS
    ZTT_DEFAULT_CONSTANTS
];

=head1 NAME

Zaaksysteem::ZTT::Constants - Constants for the ZTT infrastructure

=head1 CONSTANTS

=head2 ZTT_DEFAULT_OPERATORS

A collection of operators supported by ZTT.

=cut

use constant ZTT_DEFAULT_OPERATORS => {
    '+' => sub {
        my $x = _scalarify(shift) || 0;
        my $y = _scalarify(shift) || 0;

        return $x + $y;
    },

    '-' => sub {
        my $x = _scalarify(shift) || 0;
        my $y = _scalarify(shift) || 0;

        return $x - $y;
    },

    '*' => sub {
        my $x = _scalarify(shift) || 0;
        my $y = _scalarify(shift) || 0;

        return $x * $y;
    },

    '/' => sub {
        my $x = _scalarify(shift) || 0;
        my $y = _scalarify(shift) || 0;

        return undef if $y == 0;

        return $x / $y;
    },

    '==' => sub {
        my ($x, $y) = scalarify(shift, shift);

        return $x eq $y;
    },

    '!=' => sub {
        my ($x, $y) = scalarify(shift, shift);

        return $x ne $y;
    },

    '~=' => sub {
        my ($x, $y) = scalarify(shift, shift);

        return index(lc $x, lc $y) >= 0;
    },

    '>' => sub {
        my $x = _scalarify(shift) || 0;
        my $y = _scalarify(shift) || 0;

        return $x > $y;
    },

    '<' => sub {
        my $x = _scalarify(shift) || 0;
        my $y = _scalarify(shift) || 0;

        return $x < $y;
    },

    '>=' => sub {
        my $x = _scalarify(shift) || 0;
        my $y = _scalarify(shift) || 0;

        return $x >= $y;
    },

    '<=' => sub {
        my $x = _scalarify(shift) || 0;
        my $y = _scalarify(shift) || 0;

        return $x <= $y;
    },

    'in' => sub {
        my $x = _scalarify(shift);
        my $y = shift;

        return undef unless ref $y eq 'ARRAY';
        return any { $_ eq $x } @{ $y };
    }
};

=head2 ZTT_DEFAULT_CONSTANTS

A named list of constants available in all ZTT contexts.

=cut

use constant ZTT_DEFAULT_CONSTANTS => {
    'pi' => 3.14159265359
};

=head2 ZTT_DEFAULT_FUNCTIONS

A named list of functions available in all ZTT contexts.

=cut

use constant ZTT_DEFAULT_FUNCTIONS => {
    constant => sub {
        my $name = shift;

        return undef unless defined $name;

        return ZTT_DEFAULT_CONSTANTS->{ lc $name };
    },

    array => sub {
        return [
            map { ref $_ eq 'ARRAY' ? @{ $_ } : $_ } @_
        ];
    }
};

=head1 HELPER FUNCTIONS

=head2 scalarify

Helper function that converts a ZTT value from any type to a scalar-like
type. Currently only converts array values to a string join of the elements.

=cut

sub scalarify {
    return map { _scalarify($_) } @_;
}

sub _scalarify {
    my $arg = shift;

    return '' unless defined $arg;
    return join(', ', scalarify(@{ $arg })) if ref $arg eq 'ARRAY';

    # Ugly hack for ZS-13819 where people want to do math with text
    my $copy = Zaaksysteem::Constants::_numeric_fix_filter(undef, $arg);
    if (defined $copy) {
        return "$copy"
    }
    # End the ugly hack of ZS-13819

    return "$arg";
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

