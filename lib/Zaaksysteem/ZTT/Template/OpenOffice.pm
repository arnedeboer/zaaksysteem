package Zaaksysteem::ZTT::Template::OpenOffice;

use Moose;

use Zaaksysteem::ZTT;
use Zaaksysteem::ZTT::Tag;
use Zaaksysteem::ZTT::Selection::OpenOffice;

use URI;
use OpenOffice::OODoc;

extends 'Zaaksysteem::ZTT::Template';

=head1 NAME

Zaaksysteem::ZTT::Template::OpenOffice - Extend template class with specific
implementation details for OpenOffice documents.

=head1 ATTRIBUTES

=head2 document

This attribute holds a reference to an L<OpenOffice::OODoc::Document> object
which will be used as the source of template data.

=cut

has document => (
    is => 'ro',
    isa => 'OpenOffice::OODoc::Document',
    required => 1
);

=head2 manifest

The L<manifest|OpenOffice::OODoc::Manifest> for the loaded Document.

=cut

has manifest => (
    is => 'rw',
    isa => 'OpenOffice::OODoc::Manifest',
    init_arg => undef,
    default => sub {
        my $self = shift;
        return odfManifest(file => $self->document);
    },
);

has styles => (
    is => 'rw',
    isa => 'OpenOffice::OODoc::Styles',
    default => sub {
        return odfStyles(file => shift->document);
    }
);

has style_paragraph_selectors => (
    is => 'rw',
    isa => 'ArrayRef[Str]',
    traits => [qw[Array]],
    default => sub {
        return [qw[
            /office:master-styles/style:master-page/style:header/text:p
            /office:master-styles/style:master-page/style:footer/text:p
        ]];
    },
    handles => {
        paragraph_selectors => 'elements'
    }
);

=head1 METHODS

=head2 post_process

=cut

sub post_process {
    my $self = shift;

    my $ztt = Zaaksysteem::ZTT->new;

    $ztt->add_string_fetcher(@_);

    for my $paragraph (map { $self->styles->getNodesByXPath($_) } $self->paragraph_selectors) {
        my $subtemplate = $ztt->process_template($paragraph->text);

        # No modifications, no need to replace the header/footer content
        # FMI: ZS-4715
        next unless $subtemplate->modification_count;

        $paragraph->set_text($subtemplate->string);
    }
}

=head2 tag_selections

Returns a list of L<Zaaksysteem::ZTT::Selection> objects that reference
tags defined in the template.

    for my $selection ($openoffice_template->tag_selections) {
        # Process selection
    }

=cut

sub tag_selections {
    my $self = shift;

    my @retval;

    my $regexp = $self->tag_regexp;

    for my $element ($self->document->selectElementsByContent("$regexp")) {
        my $element_text = $element->text;
        while ($element_text =~ /$regexp/g) {
            my ($selection, $directive_text) = ($1, $2);
            my $directive = $self->directive_parser->parse($directive_text);

            # Just plain tags pls
            next unless defined $directive;
            next if exists $directive->{ iterate_context };

            push @retval, Zaaksysteem::ZTT::Selection::OpenOffice->new(
                element => $element,
                selection => $selection,

                tag => Zaaksysteem::ZTT::Tag->new(
                    name => $directive->{ attribute },
                    formatter => $directive->{ filter }
                )
            );
        }
    }

    return @retval;
}

=head2 inline_iterators

This method returns a list of L<selections|Zaaksysteem::ZTT::Selection> found
in the template where subcontext content is to be injected in-line.

=cut

sub inline_iterators {
    my $self = shift;

    my @retval;

    my $regexp = $self->tag_regexp;

    for my $element ($self->document->selectElementsByContent("$regexp")) {
        my ($selection, $directive_text) = $element->text =~ $regexp;

        my $directive = $self->directive_parser->parse($directive_text);

        # Just plain tags pls
        next unless defined $directive;
        next unless exists $directive->{ iterate_context };

        push @retval, Zaaksysteem::ZTT::Selection::OpenOffice->new(
            element => $element,
            selection => $selection,
            iterate => $directive->{ iterate_context },

            tag => Zaaksysteem::ZTT::Tag->new(
                name => $directive->{ attribute },
                formatter => $directive->{ filter }
            )
        );
    }

    return @retval;
}

=head2 sections

This method returns the combined output of L</plain_sections> and
L</table_sections>.

=cut

sub sections {
    my $self = shift;

    return ($self->plain_sections, $self->table_sections);
}

=head2 table_sections

This method returns a list of L<selections|Zaaksysteem::ZTT::Selection> found
in the template where subcontexts are to be injected in a pre-defined table of
the template.

=cut

sub table_sections {
    return map { Zaaksysteem::ZTT::Selection::OpenOffice->new(
        element => $_,
        decode_section_name($_->getAttribute('table:name'))
    ) } shift->subtemplate_table_sections;
}

=head2 subtemplate_table_sections

This method returns a list of all OpenOffice table elements where the name of
the element begins with C<itereer:>, implying that the table should be used for
subcontext injection.

=cut

sub subtemplate_table_sections {
    my $self = shift;

    return grep
        { $_->getAttribute('table:name') =~ m[^itereer:\w+] }
        $self->document->getTableList;
}

=head2 plain_sections

Returns a list of L<Zaaksysteem::ZTT::Selection> objects that reference each
section defined in the template. Sections are iterable sub-templates that get
processed in a different magic-string context than the case itself. They can be
used to inject subtemplates for each case relation, for example.

=cut

sub plain_sections {
    # Create a new Selection object for each subtemplate section
    # Add decoded attribute name for each constructor
    # Excuse the obtuse syntax formatting
    return map { Zaaksysteem::ZTT::Selection::OpenOffice->new(
        element => $_,
        decode_section_name($_->getAttribute('text:name'))
    ) } shift->subtemplate_sections;
}

=head2 decode_section_name

This method attempts to decode a C<itereer:> name string found in sections and
table elements in templates as a hash specifying the subcontext name to be
iterated, and the specific name of the iteration.

    my %info = $openoffice_template->decode_section_name('itereer:zaak_relaties # loop1')

The C<%info> hash will have the following content:

    {
        iterate => 'zaak_relaties',
        name => 'loop1'
    }

If no name is specified, the name key will have an undefined value.

=cut

sub decode_section_name {
    shift =~ m[^itereer:(?<iterate>[\w\.]+)(?:\s*\#\s*(?<name>[\w\.]+))?];

    return (
        iterate => $+{ iterate },
        name => $+{ name }
    );
}

=head2 subtemplate_sections

This method returns a list of OpenOffice section elements that have a proper
name, starting with the prefix C<itereer:>.

=cut

sub subtemplate_sections {
    my $self = shift;

    return grep
        { $_->getAttribute('text:name') =~ m[^itereer:\w+] }
        $self->document->getSections;
}

=head2 replace

Use a L<Zaaksysteem::ZTT::Selection> to replace a tag in the template with the
actual value. Second argument is expected to be a L<Zaaksysteem::ZTT::Element>,
or at least something that implements the sanitize method and has a getter at
for the value of the replacement.

=cut

sub replace {
    my $self = shift;

    my $selection = shift;
    my $replacement = shift;
    my $contexts = shift;

    return unless $selection->element->in($self->document->{ body });
    return unless $replacement;

    $replacement->sanitize;

    my $value = $replacement->value // '';

    if($replacement->type eq 'richtext') {
        my $p_to_replace = up($selection->element, sub { shift->isParagraph });

        for my $html (@{ $value }) {
            # If we get an unordered list, we know how to make those actual
            # lists in the ODT result. Otherwise, flatten to text and append
            # as a plain paragraph
            if($html->tag eq 'ul') {
                my $list = $self->document->insertItemList($p_to_replace,
                    position => 'before'
                );

                for my $item ($html->find('li')) {
                    $self->document->appendListItem($list,
                        text => $self->document->outputTextConversion(
                            $item->as_trimmed_text
                        )
                    );
                }
            } else {
                my $paragraph = $self->document->insertParagraph($p_to_replace,
                    text => $self->document->outputTextConversion(element2paragraph($html)),
                    position => 'before'
                );

                for my $a ($html->find('a')) {
                    my $ztt = Zaaksysteem::ZTT->new;
                    my $uri = URI->new($a->attr('href'));

                    $ztt->string_fetchers($contexts);

                    unless ($uri->scheme) {
                        $uri->scheme('http');
                    }

                    $self->document->setHyperlink(
                        $paragraph,
                        $a->as_trimmed_text,
                        $ztt->process_template($uri->canonical->as_string)->string
                    );
                }

                # Set the textstyle to the original style of the selected paragraph.
                $self->document->textStyle($paragraph, $self->document->textStyle($p_to_replace));
            }

            # Hackaround for inserted paragraphs, they don't seem to have a 'paragraph' newline after them,
            # so we insert an empty line and fix it visually that way.
            $self->document->insertParagraph($p_to_replace,
                text => $self->document->outputTextConversion(""),
                position => 'before'
            );
        }

        $p_to_replace->delete;

        return;
    } elsif ($replacement->type eq 'image') {
        $self->substitute_image($selection->element, $value);
        $value = '';
    } elsif ($replacement->type eq 'hyperlink') {
        my $text = $selection->selection;

        # OpenOffice::OODoc->replaceText uses regexes to replace text content
        # and we need to escape all regex-y chars in the text to be replaced
        $text =~ s/([\[\]\(\)\|])/\\$1/g;

        $self->document->replaceText(
            $selection->element,
            $text,
            $self->document->outputTextConversion($replacement->title)
        );

        $self->document->setHyperlink(
            up($selection->element, sub { shift->isParagraph }),
            $replacement->title,
            $replacement->value
        );

        return;
    } elsif($replacement->type eq 'list') {
        my $p_to_replace = up($selection->element, sub { shift->isParagraph });

        my $list = $self->document->insertItemList($p_to_replace,
            position => 'before'
        );

        my $value = $replacement->value;
        my @list_items = ref $value eq 'ARRAY' ? @{ $value } : $value;

        for my $list_item (@list_items) {
            $self->document->appendListItem($list,
                text => $self->document->outputTextConversion("$list_item")
            );
        }

        $p_to_replace->delete;

        return;
    }

    # Convert newlines to something we can capture after replacing the text.
    $value =~ s/(, )?\n/[[BR]]/g;

    my $replace_selection = $selection->selection;
    $replace_selection =~ s/([\[\]\(\)\|])/\\$1/g;

    $self->document->replaceText(
        $selection->element,
        $replace_selection,
        $self->document->outputTextConversion($value)
    );

    # Reify newlines by injecting line-breaks
    $self->document->setChildElements($selection->element, 'text:line-break',
        replace => '\[\[BR\]\]'
    );

    if ($replacement->strip_when_empty) {
        my $paragraph = up($selection->element, sub { shift->isParagraph });

        unless ($self->document->getFlatText($paragraph) =~ m[\w]u) {
            $paragraph->delete;
        }
    }
}

=head2 iterate

=cut

sub iterate {
    my $self = shift;

    my $contexts = shift;
    my $selection = shift;

    if ($selection->element->isTable) {
        return $self->iterate_table($contexts, $selection);
    }

    if ($selection->element->isSection) {
        return $self->iterate_section($contexts, $selection);
    }

    throw('ztt/iterator/unknown_type', 'Selection based on an element type I can\'t handle (yet..)');
}

=head2 iterate_table

=cut

sub iterate_table {
    my $self = shift;

    my $contexts = shift;
    my $selection = shift;

    # Assume the first row in the table is our template.
    my $tpl = $selection->element->child(0, 'table:table-row');

    for my $context (@{ $contexts }) {
        my $ztt = Zaaksysteem::ZTT->new;
        $ztt->add_context($context);

        # Copy the template row
        my $row = $self->document->insertTableRow($tpl);

        # Evaluate each cell as flattened (plaintext) template
        for my $cell ($self->document->getRowCells($row)) {
            my $value = $ztt->process_template(flatten_section($cell), 1)->string;

            # Remove all old content from the cell. ->updateCell() only
            # replaces the first <text:p> that it finds.
            my $cell = $self->document->getTableCell($cell);
            my ($first_child) = $cell->cut_children();

            # Remember the style, so alignment, etc. stay the same
            my $old_style = $first_child->getAttribute('text:style-name');

            # Hard-set the value of the cell with the processed template
            $self->document->updateCell($cell, $self->document->outputTextConversion($value));

            $cell->first_child->setAttribute('text:style-name', $old_style)
                if ($old_style);
        }
    }

    # Delete the template row
    $tpl->delete;

    # Rename the table so the next loop doesn't re-eval this one.
    $selection->element->setAttribute('table:name', 'iterdone');
}

=head2 iterate_section

=cut

sub iterate_section {
    my $self = shift;

    my $contexts = shift;
    my $selection = shift;

    my $element_to_replace = up($selection->element, sub { shift->isParagraph });

    for my $context (@{ $contexts }) {
        my $ztt = Zaaksysteem::ZTT->new;
        $ztt->add_context($context);

        # Process a COPY of the found element, processing is a non-idempotent operation, don't want
        # this loop to break for all but the first context.
        my $value = $ztt->process_template(flatten_section($selection->element), 1)->string;

        for my $ptext (split m[\n\s*\n], $value) {
            my $paragraph = $self->document->insertParagraph($selection->element,
                text => $self->document->outputTextConversion($ptext),
                position => 'before'
            );

            $self->document->textStyle($paragraph, $self->document->textStyle($element_to_replace));
        }
    }

    $selection->element->delete;
}

=head2 iterate_inline

=cut

sub iterate_inline {
    my $self = shift;

    my $contexts = shift;
    my $selection = shift;

    my @values;

    for my $context (@{ $contexts }) {
        my $ztt = Zaaksysteem::ZTT->new;

        $ztt->add_context($context);

        my $value = $ztt->process_template(sprintf('[[%s]]', $selection->tag->name))->string;

        if(length $value) {
            push @values, $value;
        }
    }

    my $replacement = Zaaksysteem::ZTT::Element->new(value => join(', ', @values));

    $self->replace($selection, $replacement);
}

=head1 FUNCTIONS

These functions are not class methods, they are just generic helpers
for easy traversal of the oodoc or for converting L<Zaaksysteem::ZTT::Element>s

=head2 up

This function returns the first element to return true for the test condition
supplied as the second argument by walking up the document tree. If no such
node can be found returns undef.

=cut

sub up {
    my $element = shift;
    my $code = shift;

    while($element && !$code->($element)) {
        $element = $element->getParentNode;
    }

    return $element;
}

=head2 element2paragraph

This function returns a textual representation of the HTML::Element supplied
in the context of oodocs.

=cut

sub element2paragraph {
    my $element = shift;

    my %dispatch = (
        p => sub { split(m[\n], shift->as_trimmed_text) },

        ol => sub {
            my $element = shift;
            my $iter = 1;

            return join("\n", map
                { sprintf("%d. %s", $iter++, $_->as_trimmed_text) }
                $element->find('li')
            );
        }
    );

    return unless exists $dispatch{ $element->tag };

    return $dispatch{ $element->tag }->($element);
}

=head2 substitute_image

This function will create a new OpenOffice image element and attach it to
the first paragraph in the node lineage of the provided element.

It will add the image to the document's manifest.

Currently only works for C<image/jpg> files.

=cut

sub substitute_image {
    my ($self, $element, $value) = @_;

    my ($image_path, $size);
    if (ref($value) eq 'HASH') {
        $image_path = $value->{image_path};
        $size = sprintf(
            "%.3f,%.3f",
            $value->{width},
            $value->{height}
        );
    }
    else {
        $image_path = $value;

        # Default size value, for backwards compatibility with older
        # "signatures" templates.
        #
        # Based on OpenOffice import behaviour. Imported a sample image into
        # OpenOffice, and recorded it's measurements in inches, converted these
        # to metric (cm).
        # 350px x 120px (3.65" x 1.25") equals in cm:
        $size = "9.271,3.175";
    }

    my $paragraph = up($element, sub { shift->isParagraph });

    # All images and styles must have unique names, numbers are acceptable
    my $style_name = sprintf("PhotoStyle%d", $self->modification_count);
    my $style = $self->document->createImageStyle($style_name);

    my $image = $self->document->createImageElement(
        sprintf('zsimage_%d', $self->modification_count),
        style      => $style_name,
        attachment => $paragraph,
        import     => $image_path,
        size       => $size,
    );

    $self->document->setAttributes($image, 'text:anchor-type' => 'paragraph');

    my $link = $self->document->imageLink($image);

    # TODO - detect actually mimetype
    $self->manifest->setEntry( $link, 'image/jpeg');

    return;
}

=head2 flatten_section

This function will attempt to create a string representation of any OpenOffice
document element provided.

If the provided element is parent to other elements, it will be flattened
recursively (watch out for circular referenced nodes!).

Currently supported elements:

=over 4

=item text:tab (tabs)

Replaced with a \t character.

=item text:linebreak (newlines)

Replaced with a \n character.

=item text:s (spaces)

Replaced with ' ' character(s).

=item text:p (paragraphs)

Replaced with the paragraph text content, followed by a \n.

=item text:span (spans)

Replaced with the span text content.

=back

=cut

sub flatten_section {
    my $element = shift;

    my $name = $element->getName;

    return "\t" if $name =~ m[^text:tab(|-stop)$];
    return "\n" if $name eq 'text-linebreak';
    return ' ' x ($element->att('text:c') || 1) if $name eq 'text:s';
    return sprintf("%s\n", $element->text) if $name eq 'text:p';
    return $element->text if $name eq 'text:span';
    return join '', map
        { $_->isElementNode ? flatten_section($_) : $_->text }
        $element->getChildNodes;
}

__PACKAGE__->meta->make_immutable;
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 substitute_image

TODO: Fix the POD

=cut

