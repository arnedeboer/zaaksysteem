package Zaaksysteem::ZTT::Element;

use Moose;
use Data::Dumper;

use Zaaksysteem::Profile;
use Zaaksysteem::Exception;

use HTML::TreeBuilder;

=head1 NAME

Zaaksysteem::ZTT::Element

=head1 ATTRIBUTES

=head2 attribute

=cut

has attribute => (
    is => 'ro',
    isa => 'Zaaksysteem::Model::DB::ZaaktypeKenmerken',
);

=head2 cleaner_skip

A hash reference, used by some L<cleaner> subs to check whether they should run
or not.

If a value is set (and true), the relevant cleaner will be skipped.

This has two handles:

=over

=item * cleaner_skip_set

Used to set a value in the skip list.

=item * cleaner_skip_get

Used to get a value from the skip list.

=back

=cut

has cleaner_skip => (
    is       => 'rw',
    isa      => 'HashRef',
    required => 0,
    default  => sub { {} },
    traits   => ['Hash'],
    handles  => {
        cleaner_skip_set => 'set',
        cleaner_skip_get => 'get',
    },

);

=head2 value

=cut

has value => (
    is => 'rw'
);

=head2 type

=cut

has type => (
    is => 'rw',
    isa => 'Str',
    default => 'plaintext'
);

=head2 title

=cut

has title => (
    is => 'rw',
    isa => 'Str'
);

=head2 strip_when_empty

This attribute indicates that the wrapping paragraph in which the magic string
is injected should be removed entirely when no printable characters where
rendered.

=cut

has strip_when_empty => (
    is => 'rw',
    isa => 'Bool',
    default => 0,
    required => 1
);

=head2 cleaners

=cut

has cleaners => (
    is => 'ro',
    default => sub { return [

        # This sanitization check goes on top, if the attribute
        # is a file, we don't want to do any processing at all
        sub {
            my $self = shift;

            return 1 if shift eq 'file';
        },

        # If the returned value is an array, we flatten it beforehand
        sub {
            my $self = shift;

            return if $self->cleaner_skip_get('array');

            if (ref($self->value) eq 'ARRAY' && !blessed($self->value->[0])) {
                $self->value(join(", \n", @{ $self->value }));
            }

            return;
        },

        # In case of a richtext attribute, we return the HTML::Elements
        # we actually wanna look at, and let the template class figure it out
        sub {
            my $self = shift;
            return unless shift eq 'richtext';

            # Force string context
            my $html = '' . $self->value;
            $html =~ s[<br \/>][\n]g;

            my $builder = HTML::TreeBuilder->new;
            $builder->implicit_body_p_tag(1);
            $builder->p_strict(1);
            $builder->no_space_compacting(1);

            my $tree = $builder->parse($html);
            $builder->eof;

            $self->value([ $tree->find(qw[p ul ol]) ]);
            $self->type('richtext');

            return;
        },

        sub {
            my $self = shift;

            return unless shift =~ m[valuta];
            return if $self->cleaner_skip_get('valuta');

            my $value = $self->value || '';

            return unless length $value;

            $value =~ s[,][.]g;
            $value = sprintf('%01.2f', $value);
            $value =~ s[\.][,]g;

            $self->value($value);

            return;
        },

        sub {
            my $self = shift;

            return unless shift =~ m[^bag];

            my $value = $self->value // '';

            return unless $value =~ m[\w+\-\d+];

            my $rs = $self->attribute->result_source->schema->resultset('BagNummeraanduiding');

            my @bags = map { $rs->get_record_by_source_identifier($_) } split m[,\s*], $value;

            $self->value(join('; ', map { $_->to_string } @bags)) if scalar @bags;

            return;
        }
    ]; }
);

=head1 METHODS

=head2 sanitize

This method takes the value in L</value> and sanitizes it if on of the defined
L</cleaners> knows how to handle it. The C<value> attribute will be updated,
and C<$self> will be returned.

    my $element = $element->sanitize;

=cut

sub sanitize {
    my $self = shift;

    # run all the cleaners. if one of these returns truthy, don't run the rest
    for my $code (@{ $self->cleaners }) {
        last if $self->$code(
            $self->attribute ? $self->attribute->bibliotheek_kenmerken_id->value_type : ''
        );
    }

    return $self;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
