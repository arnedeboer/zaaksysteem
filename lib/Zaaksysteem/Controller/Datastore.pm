package Zaaksysteem::Controller::Datastore;

use Moose;
use namespace::autoclean;

use Encode qw(encode);
use Zaaksysteem::Tools;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head1 NAME

Zaaksysteem::Controller::Datastore - ZAPI Controller

=head1 SYNOPSIS

See L<Zaaksysteem::Controller::Sysin>

=head1 DESCRIPTION

Zaaksysteem API Controller for Datastore (Gegevensmagazijn).

=head1 INSTRUCTIONS

=head2 FORM LAYOUT

You can use the C<zapi_form=1> query parameter to retrieve more information
from a function regarding the needed form parameters.

=head1 METHODS

=head2 /datastore [GET READ]

Returns a resultset of loaded interfaces

=cut

use constant DATASTORE_CLASSES => [
    qw/
        NatuurlijkPersoon
        Bedrijf
        BagLigplaats
        BagNummeraanduiding
        BagOpenbareruimte
        BagPand
        BagStandplaats
        BagVerblijfsobject
        BagWoonplaats
        /
];

use constant NATUURLIJK_PERSOON_DATE_FIELDS => qw/
    geboortedatum
    import_datum
    deleted_on
    datum_overlijden
    datum_huwelijk
    datum_huwelijk_ontbinding
    /;

use constant BEDRIJF_DATE_FIELDS => qw/
    import_datum
    deleted_on
    /;

use constant BAG_DATE_FIELDS => qw/
    begindatum
    einddatum
    documentdatum
    /;


# prep here so with large exports we save a few cpu cycles
has DATE_FIELDS_CONFIG => (
    is      => 'ro',
    lazy    => 1,
    default => sub {

        # expects a Perl datetime object or falsy
        my $date_formatter = sub {
            my $value = shift;
            return $value && $value->strftime('%d-%m-%Y %H:%M:%S');
        };

        # expects something like 20101012000000, 2009-07-01. 20070203 or falsy
        my $bag_date_formatter = sub {
            my $value = shift;

            # if the value is truthy and matches one of these three formats,
            # return a DateTime object
            if (
                $value
                && (   $value =~ m|^(\d{4})(\d{2})(\d{2})\d{6}$|
                    || $value =~ m|^(\d{4})-(\d{2})-(\d{2})$|
                    || $value =~ m|^(\d{4})(\d{2})(\d{2})$|)
                )
            {
                return DateTime->new(
                    year  => $1,
                    month => $2,
                    day   => $3
                )->strftime('%d-%m-%Y');
            }

            # fall through - if somehow no match, just return input
            return $value;
        };

        return {
            NatuurlijkPersoon => {
                map { $_ => $date_formatter } (NATUURLIJK_PERSOON_DATE_FIELDS)
            },
            Bedrijf => { map { $_ => $date_formatter } (BEDRIJF_DATE_FIELDS) },
            Bag => {

                # disabled Bag date formatting. Above code is functional, so for the sake
                # of future gains, and because this setup has been tested already
                # I'm leaving it intact.
                #map { $_ => $bag_date_formatter } (BAG_DATE_FIELDS)
            },
        };
    }
);


sub index
    : Chained('/')
    : PathPart('datastore')
    : Args(0)
    : ZAPI {
    my ($self, $c) = @_;

    $c->assert_any_user_permission('admin');

    $c->stash->{zapi} = [];
}

sub base
    : Chained('/')
    : PathPart('datastore/search')
    : Args(1) {
    my ($self, $c, $class) = @_;

    my $classes = DATASTORE_CLASSES;

    # Module/auth allowed check
    $c->assert_any_user_permission('admin');
    if (!grep { $_ eq $class } @{$classes}) {
        throw 'datastore/class_not_allowed',
            "Class $class is not allowed for datastore viewing/exporting";
    }

    if (exists $c->req->params->{zapi_crud}) {
        my @columns = _get_columns($c, $class);
        $c->stash->{zapi} = [
            Zaaksysteem::ZAPI::CRUD::Interface->new(
                options => { select => 'all', },
                actions => [],
                columns => \@columns,
                url     => '<[cl]>'
            )
        ];
        $c->detach();
    }

    $c->stash->{zapi} = _get_data_for_class($c, $class);
}

sub classes
    : Chained('/')
    : PathPart('datastore/classes') {
    my ($self, $c) = @_;
    my $classes = DATASTORE_CLASSES;
    $c->stash->{json} = \@$classes;
    $c->forward('Zaaksysteem::View::JSON');
}

sub csv : Chained('/') : PathPart('datastore/csv') : Args(1) {
    my ($self, $c, $class) = @_;

    my %active_object_subscription_ids = map {
        $_ => 1
    } $c->model("DB::ObjectSubscription")->search({
        local_table  => $class,
        date_deleted => undef
    })->get_column('local_id')->all;

    my @raw_columns = _get_columns($c, $class);
    my @columns     = map({ $_->resolve } grep({ $_->resolve } @raw_columns));

    my @csv;
    push(@csv, ['afnemerindicatie', @columns]);

    my $rs = _get_data_for_class($c, $class);
    my $count = 0;
    while (my $r = $rs->next) {
        $count++;
        my @line;
        if ($active_object_subscription_ids{ $r->id } && $r->can('subscription_id')) {
            push (@line, $r->subscription_id->external_id);
        } else {
            push (@line, '');
        }
        push(@line, $self->format_fields($class, $r, \@columns));
        push @csv, \@line;
    }

    $c->stash->{csv} = { data => \@csv };

    $c->res->headers->header('Content-Type' => 'application/x-download');
    $c->res->headers->header('Content-Disposition' => "attachment;filename=$class.csv");

    my $csv = $c->view('CSV')->render($c, $c->stash);
    $c->res->body($csv);
}

sub _get_data_for_class {
    my ($c, $class) = @_;
    my $search_opts = { prefetch => [], order_by => { -desc => 'me.id' } };

    my $search = { };


    if ($class eq 'NatuurlijkPersoon') {
        ### Join address
        push(@{ $search_opts->{prefetch} }, 'adres_id');
        $search->{'me.deleted_on'} = undef;
    }
    elsif ($class eq 'Bedrijf') {
        $search->{'me.deleted_on'} = undef;
    }
    elsif ($class =~ /BagLigplaats|BagStandplaats/) {
        push(@{ $search_opts->{prefetch} }, [qw/hoofdadres/]);
    }
    elsif ($class eq 'BagNummeraanduiding') {
        push(@{ $search_opts->{prefetch} }, { openbareruimte => 'woonplaats' });
    }
    elsif ($class eq 'BagOpenbareruimte') {
        push(@{ $search_opts->{prefetch} }, 'woonplaats');
    }

    if ($c->model("DB::$class")
        ->result_source->has_relationship('subscription_id'))
    {
        push(@{ $search_opts->{prefetch} }, 'subscription_id');
    }

    return $c->model("DB::$class")->search_rs(
        {
            %{$search},
            'me.id' => {

                # Make sure that any objects that are deleted in the database are
                # excluded from the resultset. This NEEDS to be done beforehand, all
                # alternatives would mean breaking the resultset later on. (And ruining
                # paging, easy exporting.)
                'not in ' => $c->model("DB::ObjectSubscription")->search(
                    {
                        local_table  => $class,
                        date_deleted => { '!=', undef }
                    }
                )->get_column('local_id::integer')->as_query
            }
        },
        $search_opts
    );
}

sub _get_columns {
    my ($c, $class) = @_;

    my @columns = grep {
               $_ ne 'search_index'
            && $_ ne 'search_term'
            && $_ ne 'searchable_id'
            && $_ ne 'object_type'
            && $_ ne 'adres_id'        ### Special natuurlijkpersoon case
    } $c->model("DB::$class")->result_source->columns;

    ### Skip some internal columns

    # Object subscription isn't an actual column
    my @generic_columns;
    push @generic_columns,
        Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
        id       => 'object_subscription',
        label    => 'afnemerindicatie',
        template => '<[item.object_subscription.external_id]>',
        when     => 'item.object_subscription'
        );

    # Process columns
    for my $col (@columns) {
        push @generic_columns,
            Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
            id      => 'me.' . $col,
            label   => $col,
            resolve => $col,
            );
    }

    ### Special NatuurlijkPersoon case
    if ($class eq 'NatuurlijkPersoon') {
        my @adr_columns = grep {
                   $_ ne 'search_index'
                && $_ ne 'search_term'
                && $_ ne 'searchable_id'
                && $_ ne 'object_type'
        } $c->model("DB::Adres")->result_source->columns;
for my $col (@adr_columns) {
            push @generic_columns,
                Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
                id      => 'adres_id.' . $col,
                label   => $col,
                resolve => 'adres_id.' . $col,
                );
        }
    }
    return @generic_columns;
}


=head2 format_fields

If a field is a designated date field, format as date. Otherwise return string
value.

=cut

sub format_fields {
    my ($self, $class, $row, $columns) = @_;

    # Bag* all share same config
    my $config_class = $class =~ m|^Bag| ? 'Bag' : $class;
    my $formatters = $self->DATE_FIELDS_CONFIG->{$config_class};

    my @fields;
    for my $colname (@{$columns}) {
        next unless $colname;
        my $value = $formatters->{$colname};
        if ($colname =~ /\./) {
            my ($table, $colname) = $colname =~ /^(.+)\.(.+)$/;

            $value = $formatters->{$colname};
            $value = $value ? $value->($row->$table->$colname) : $row->$table->get_column($colname);
        }
        else {
            $value = $value ? $value->($row->$colname) : $row->get_column($colname);
        }
        $value = encode('UTF-8', $value, 1) if defined $value;
        push(@fields, $value);
    }

    return @fields;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 DATASTORE_CLASSES

TODO: Fix the POD

=cut

=head2 base

TODO: Fix the POD

=cut

=head2 classes

TODO: Fix the POD

=cut

=head2 csv

TODO: Fix the POD

=cut

=head2 index

TODO: Fix the POD

=cut

