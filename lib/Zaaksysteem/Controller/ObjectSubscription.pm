package Zaaksysteem::Controller::ObjectSubscription;

use Moose;
use Moose::Util::TypeConstraints;
use namespace::autoclean;

use Zaaksysteem::Tools;

use Zaaksysteem::ZAPI::CRUD::Interface;
use Zaaksysteem::ZAPI::CRUD::Interface::Column;
use Zaaksysteem::ZAPI::CRUD::Interface::Filter;

use Zaaksysteem::Constants qw/STUF_SUBSCRIPTION_VIEWS/;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

use constant ZAPI_CRUD => {
    'read' => Zaaksysteem::ZAPI::CRUD::Interface->new(
        options     => {
            select      => 'all'
        },
        actions => [
            Zaaksysteem::ZAPI::CRUD::Interface::Action->new(
                id      => "object-subscription-delete",
                type    => "delete",
                label   => "Verwijderen",
                data    => {
                    url => '/object_subscription/delete'
                }
            ),
            Zaaksysteem::ZAPI::CRUD::Interface::Action->new(
                id      => "object-subscription-create",
                type    => "update",
                label   => "Aanmaken",
                data    => {
                    url => '/object_subscription/create'
                }
            ),
            Zaaksysteem::ZAPI::CRUD::Interface::Action->new(
                id      => 'object-subscription-download',
                type    => 'download',
                label   => 'Exporteer als CSV',
                data    => {
                    url => '/object_subscription/?zapi_format=csv&zapi_no_pager=1'
                }
            )
        ],
        filters => [
            Zaaksysteem::ZAPI::CRUD::Interface::Filter->new(
                name    => 'interface_id',
                type    => 'select',
                data    => {
                    'options' => '<[getInterfaceOptions()]>'
                },
                value   => '',
                label   => ''
            ),
            Zaaksysteem::ZAPI::CRUD::Interface::Filter->new(
                name    => 'view_id',
                type    => 'select',
                data    => {
                    'options' => [
                        map {
                            {
                                value    => $_,
                                label    => STUF_SUBSCRIPTION_VIEWS()->{ $_ }
                            }
                        } keys %{ STUF_SUBSCRIPTION_VIEWS() }
                    ],
                },
                value   => '',
                label   => ''
            ),
            Zaaksysteem::ZAPI::CRUD::Interface::Filter->new(
                name    => 'freeform_filter',
                type    => 'text',
                # data    => {
                #     'options' => '<[getInterfaceOptions()]>'
                # },
                value   => '',
                label   => 'Zoeken'
            ),
        ],
        columns         => [
            Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
                id          => 'local_id',
                label       => 'Tabel ID',
                template    => '<a href="/beheer/object/search/<[item.local_table]>/<[item.local_id]>"><[item.local_id]></a>'
            ),
            Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
                id          => 'local_table',
                label       => 'Tabel',
                resolve     => 'local_table',
            ),
            Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
                id          => 'object_preview',
                label       => 'Voorbeeld',
                resolve     => 'object_preview',
            ),
            Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
                id          => 'interface_id',
                label       => 'Interface',
                resolve     => 'interface_id.name',
            ),
            Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
                id          => 'external_id',
                label       => 'Extern ID',
                resolve     => 'external_id',
            ),
        ],
    ),
};

=head1 NAME

Zaaksysteem::Controller::ObjectSubscription - ZAPI Controller

=head1 SYNOPSIS

See L<Zaaksysteem::Controller::ObjectSubscription>

=head1 DESCRIPTION

Zaaksysteem API Controller for ObjectSubscription.

=head1 METHODS

=head2 /object_subscription [GET READ]

 /object_subscription?zapi_crud=1

Returns a list of object_subscriptions.

B<Special Query Params>

=over 4

=item C<zapi_crud=1>

Use the special query parameter C<zapi_crud=1> for getting a technical CRUD
description.

=back

=cut

sub index
    : Chained('/')
    : PathPart('object_subscription')
    : Args(0)
    : ZAPI
{
    my ($self, $c) = @_;

    $c->assert_any_user_permission('admin');

    if (exists $c->req->params->{zapi_crud}) {
        $c->stash->{zapi} = [ ZAPI_CRUD->{read}->from_catalyst($c) ];
        $c->detach;
    }

    my $selection_ids;
    $selection_ids = $c->req->params->{'selection_id[]'} if $c->req->params->{'selection_id[]'};

    if (exists $c->req->params->{is_error}) {
        $c->req->params
    }

    $c->stash->{zapi}   = $c->model('DB::ObjectSubscription')->search_filtered(
        { 
            %{ $c->req->params },
            selection_id    => $selection_ids,
        }
    );
}


sub base
    : Chained('/')
    : PathPart('object_subscription')
    : CaptureArgs(1)
{
    my ($self, $c, $id) = @_;

    $c->assert_any_user_permission('admin');

    $c->stash->{entry}  = $c->model('DB::ObjectSubscription')->find($id);
}

=head2 /object_subscription/ID [GET READ]

Reads information from ObjectSubscription by ID

B<Options>: none

=cut

sub read
    : Chained('base')
    : PathPart('')
    : Args(0)
    : ZAPI
{
    my ($self, $c) = @_;

    $c->stash->{zapi}   = $c->stash->{entry} || [];
}


=head2 /object_subscription/delete [POST DELETE]

Deletes object subscriptions from our database. This will mark these entries as deleted.

=cut

sub delete
    : Chained('/')
    : PathPart('object_subscription/delete')
    : Args(0)
    : ZAPI
{
    my ($self, $c) = @_;

    $self->_handle_object_subscription($c, { action => 'disable', label => 'Verwijder' });

}

sub create
    : Chained('/')
    : PathPart('object_subscription/create')
    : Args(0)
    : ZAPI
{
    my ($self, $c) = @_;

    $self->_handle_object_subscription($c, { action => 'enable', label => 'Aanmaken' });
}

define_profile _handle_object_subscription => (
    required    => {
        action  => subtype('Str' => where { $_ =~ /^(?:disable|enable)$/}),
        label   => 'Str',
    }
);

sub _handle_object_subscription {
    my ($self, $c)  = @_;
    my $options     = assert_profile($_[2] || {})->valid;

    $c->assert_any_user_permission('admin');

    throw('httpmethod/post', 'Invalid HTTP Method, no POST', []) unless
        lc($c->req->method) eq 'post';

    my $params = $c->req->params;

    my $object_subscriptions = $c->model('DB::ObjectSubscription')
        ->search_filtered($params);

    my @rows;
    while (my $os = $object_subscriptions->next) {
        push(@rows,
            {
                label => $options->{label} . ' afnemerindicatie voor extern ID ' . $os->external_id,
                data  => {
                    subscription_id => $os->id,
                    interface_id    => $os->get_column('interface_id'),
                    config_interface_id => $os->get_column('config_interface_id'),
                }
            }
        );
    }

    my $q = $c->model("DB::Queue");
    $q->create_items($options->{action} . '_object_subscription', @rows);

    $c->stash->{zapi} = [];
}

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 ZAPI_CRUD

TODO: Fix the POD

=cut

=head2 base

TODO: Fix the POD

=cut

=head2 index

TODO: Fix the POD

=cut

=head2 read

TODO: Fix the POD

=cut

