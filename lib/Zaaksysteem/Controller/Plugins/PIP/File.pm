package Zaaksysteem::Controller::Plugins::PIP::File;

use Moose;

use File::stat;

use Zaaksysteem::Constants;
use Zaaksysteem::Tools;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head2 file_search

Searches for zero or more files matching the given parameters. By default only
the last version of a file is returned.

=head3 Arguments

=over

=item case_id [required]

=back

=head3 Returns

A list containing one or more JSON structures containing the file
properties.

=cut

sub file_search : Chained('/plugins/pip/zaak_base') : PathPart('file/search') : ZAPI {
    my ($self, $c) = @_;

    my @files = $c->stash->{zaak}->files->search(
        {
            'me.publish_pip'  => 1,
            'me.date_deleted' => undef,
        }
    )->active_files;

    $c->stash->{zapi_no_pager} = 1;
    $c->stash->{zapi} = [map { $_->get_pip_data } @files];
}

=head2 file_create

Create a new file.

=head3 Arguments

=over

=item case_id [required]

Assign this file to a case. When this parameter is ommitted, the file
will show up in the global file queue for later processing.

=item publish_website [optional]

Boolean, decides whether this file may be exported outside of Zaaksysteem.

=item return_content_type

Option to override the content-type this call returns. Mostly for IE-compatibility.

=back

=head3 Location

POST: /pip/file/create

=head3 Returns

A JSON structure containing file properties.

=cut

sub file_create : Chained('/plugins/pip/base') : PathPart('file/create') : ZAPI {
    my ($self, $c) = @_;

    $c->{stash}->{json_content_type} = $c->req->params->{return_content_type};

    my %optional;
    if ($c->req->params->{case_id}) {
        $optional{case_id} = $c->req->params->{case_id};
    }
    my $upload  = $c->req->upload('file');
    my $subject = $c->session->{pip}->{ztc_aanvrager};

    # Create the DB-entry
    my $result = $c->model('DB::File')->file_create({
        db_params => {
            accepted     => 0,
            created_by   => $subject,
            %optional,
            publish_pip  => 1,
        },
        name              => $upload->filename,
        file_path         => $upload->tempname,
    });

    # Needs standardizing. Possibly in the JSON viewer?
    no strict 'refs';
    *DateTime::TO_JSON = sub {shift->iso8601};
    use strict;

    $c->stash->{zapi} = [$result->get_pip_data];
}

=head2 get_thumbnail

Returns the thumbnail.

=head3 Arguments

=over

=item file_id [required]

=back

=head3 Location

/pip/file/thumbnail/file_id/1234

=head3 Returns

Returns the thumbnail data.

=cut

sub get_thumbnail : Chained('/plugins/pip/base') : PathPart('file/thumbnail/file_id') : Args() {
    my ($self, $c, $file_id) = @_;

    my ($file) = $c->model('DB::File')->search({id => $file_id});

    if (!$file) {
        throw('/file/get_thumbnail/file_not_found', "File with ID $file_id not found");
    }

    # Generate thumbnail if it doesn't have one yet.
    if (!$file->filestore->thumbnail_uuid) {
        $file->filestore->generate_thumbnail;
    }

    my $thumbnail_path = $file->filestore->get_thumbnail_path;

    $c->serve_static_file($thumbnail_path);
    $c->res->headers->content_length(stat($thumbnail_path)->size);
    $c->res->headers->content_type('image/jpeg');
}

=head2 download

Offers a file up as a download regardless of mimetype/extension.

=head3 Arguments

=over

=item file_id [required]

=back

=head3 Location

/pip/file/download/file_id/1234

=head3 Returns

Returns the file as a download.

=cut

sub document_base : Chained('/plugins/pip/zaak_base') : PathPart('document') : CaptureArgs(1) {
    my ($self, $c, $file_id) = @_;

    unless($file_id =~ m|^\d+$|) {
        throw('request/invalid_parameter', 'file_id parameter must be numeric');
    }

    my $file = $c->stash->{ zaak }->files->find($file_id);

    unless($file) {
        throw('request/invalid_parameter', 'file_id parameter did not resolve to a file in the database');
    }

    if($file->deleted_by) {
        throw('file/not_available', 'file_id parameter resolved to a file that was deleted');
    }

    unless($file->publish_pip) {
        throw('file/not_published_on_pip', 'file not published on pip');
    }

    unless($file->filestore->filestat) {
        throw('file/not_available', 'file_id parameter resolved to a file that does not physically exist on this server');
    }

    $c->stash->{ file } = $file;
}

sub download : Chained('document_base') : PathPart('download') {
    my ($self, $c, $format) = @_;

    my $file = $c->stash->{ file };

    my ($path, $mime, $size, $name) = $file->get_download_info($format);

    $c->serve_static_file($path);
    $c->res->headers->content_length($size);
    $c->res->headers->content_type('application/octet-stream');
    $c->res->header('Cache-Control', 'must-revalidate');
    $c->res->header('Pragma', 'private');
    $c->res->header('Content-Disposition', sprintf('attachment; filename="%s"', $name));
}




=head2 upload

make case uploading functionality available from this context
we need the local protection here

=cut

sub upload : Chained('/plugins/pip/zaak_base') : PathPart('upload') {
    my ($self, $c) = @_;

    $c->forward('/zaak/upload/upload');

    # ZS-2194 - Controller derives from ZAPIController, this conflicts
    # with pages calling calling this action.
    $c->detach($c->view('TT'));
}

=head2 remove_upload

make case uploading functionality available from this context
we need the local protection here

=cut

sub remove_upload : Chained('/plugins/pip/zaak_base') : PathPart('upload/remove_upload') {
    my ($self, $c) = @_;

    $c->forward('/zaak/upload/remove_upload');

    # ZS-2194 - Controller derives from ZAPIController, this conflicts
    # with pages calling calling this action.
    $c->detach($c->view('TT'));
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 document_base

TODO: Fix the POD

=cut

