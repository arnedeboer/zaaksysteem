package Zaaksysteem::Controller::Plugins::Digid;

use Moose;

use Moose::Util qw/apply_all_roles does_role/;
use JSON;
use MIME::Base64;

BEGIN { extends 'Zaaksysteem::Controller' }

use Zaaksysteem::Constants qw/
    ZAAKSYSTEEM_GM_AUTHENTICATEDBY_DIGID
    ZAAKSYSTEEM_GM_AUTHENTICATEDBY_BEDRIJFID

    VALIDATION_CONTACT_DATA
    VALIDATION_EXTERNAL_CONTACT_DATA
/;

sub login : Chained('/') : PathPart('auth/digid'): Args() {
    my ($self, $c, $do_auth) = @_;

    if ($c->user_exists) {
        $c->delete_session;
    }

    ### In case of an XHR, and we get here...define we are not logged in
    if ($c->req->is_xhr) {
        $c->res->status('401');
    }

    $c->stash->{ success_endpoint } = $c->req->param('success_endpoint');

    $c->stash->{ idps } = [ $c->model('DB::Interface')->search_module('samlidp', '$.login_type_citizen') ];

    if($c->session->{ _saml_error }) {
        my %dispatch = (
            'urn:oasis:names:tc:SAML:2.0:status:AuthnFailed' => 'cancelled',
            'urn:oasis:names:tc:SAML:2.0:status:NoAuthnContext' => 'context_insufficient',
            'urn:oasis:names:tc:SAML:2.0:status:PartialLogout' => 'partial_logout',
            'urn:oasis:names:tc:SAML:2.0:status:RequestDenied' => 'denied'
        );

        $c->stash->{ saml_error } = $dispatch{ $c->session->{ _saml_error } } || 'unknown';

        $c->log->debug(sprintf('SAML Protocol Exchange came back with error: ', $c->session->{ _saml_error }));
    }

    $c->stash->{template} = 'plugins/digid/login.tt';
    $c->stash->{ page_title } = 'Inloggen met DigiD';
}

sub logout : Chained('/') : PathPart('auth/digid/logout'): Args() {
    my ($self, $c) = @_;

    delete $c->session->{ _saml };
    $c->stash->{template}   = 'plugins/digid/login.tt';
    $c->stash->{logged_out} = 1;
    $c->detach;
}


sub _zaak_create_secure_digid : Private {
    my ($self, $c) = @_;

    my $saml_state = $c->session->{ _saml } || {};

    if (
        $c->req->params->{authenticatie_methode} eq 'digid' ||
        $c->session->{_zaak_create}->{extern}->{verified} eq 'digid'
    ) {
        if($saml_state->{ success }) {
            $c->session->{_zaak_create}->{extern} = {};

            ### Check if we are allowed to crate this zaaktype
            $c->session->{_zaak_create}->{extern}->{aanvrager_type}
                = 'natuurlijk_persoon';
            $c->session->{_zaak_create}->{extern}->{verified}
                = 'digid';
            $c->session->{_zaak_create}->{extern}->{id}
                = $saml_state->{ uid };

            $c->stash->{aanvrager_type} = 'natuurlijk_persoon'
        } else {
            my $arguments = {};
            $arguments->{'authenticatie_methode'} = 'digid' if ($c->req->params->{authenticatie_methode});
            $arguments->{'ztc_aanvrager_type'} = 'natuurlijk_persoon' if ($c->req->params->{ztc_aanvrager_type});
            $arguments->{'sessreset'} = 1 if ($c->req->params->{sessreset});
            $arguments->{'zaaktype_id'} = $c->req->params->{zaaktype_id} if (
                $c->req->params->{zaaktype_id} &&
                $c->req->params->{zaaktype_id} =~ /^\d+$/
            );

            $c->res->redirect(
                $c->uri_for(
                    '/auth/digid',
                    {
                        success_endpoint => $c->uri_for(
                            '/zaak/create/webformulier/',
                            $arguments,
                        )
                    }
                )
            );

            ### Wipe out externe authenticatie
            if (
                $c->session->{_zaak_create}->{extern} &&
                $c->session->{_zaak_create}->{verified} eq 'digid'
            ) {
                delete($c->session->{_zaak_create}->{extern});
            }

            $c->detach;
        }
    } else {

        ### Geen digiid, stop here
        return;
    }

    ### Save aanvrager data
    $c->forward('_zaak_create_aanvrager');
}

sub _zaak_create_aanvrager : Private {
    my ($self, $c) = @_;

    return unless (
        $c->req->params->{aanvrager_update}
    );

    $c->log->debug('_zaak_create_aanvrager: Aanvrager update');

    my $callerclass     = 'Zaaksysteem::Betrokkene::Object::NatuurlijkPersoon';

    my $zaaktype = $c->model('DB::Zaaktype')->find($c->session->{_zaak_create}{zaaktype_id});
    my $zaaktype_node = $zaaktype->zaaktype_node_id;

    my $external_validation_profile = $c->user_exists
            ? VALIDATION_CONTACT_DATA
            : VALIDATION_EXTERNAL_CONTACT_DATA->($zaaktype_node);

    ### Only validate contact, which are all optional
    my $profile;
    if ($c->req->params->{contact_edit}) {
        $profile = $external_validation_profile;
    } else {
        ### Get profile from Model
        $profile         = $c->get_profile(
            'method'    => 'create',
            'caller'    => 'Zaaksysteem::Controller::Betrokkene'
        ) or die('Terrible die here');

        ### MERGE
        my $contact_profile = $external_validation_profile;
        while (my ($key, $data) = each %{ $contact_profile }) {
            unless ($profile->{$key}) {
                $profile->{$key} = $data;
                next;
            }

            if (UNIVERSAL::isa($data, 'ARRAY')) {
                push(@{ $profile->{$key} }, @{ $data });
                next;
            }

            if (UNIVERSAL::isa($data, 'HASH')) {
                while (my ($datakey, $dataval) = each %{ $data }) {
                    $profile->{$key}->{$datakey} = $dataval;
                }
                next;
            }
        }
    }

    Zaaksysteem->register_profile(
        method => '_zaak_create_aanvrager',
        profile => $profile,
    );

    if ($c->req->is_xhr) {
        $c->zvalidate;
        $c->detach;
    }

    my $dv      = $c->zvalidate;
    return unless ref($dv);

    return unless $dv->success;

    ### Post
    $c->log->debug('_zaak_create_aanvrager: Updated aanvrager');
    if ($c->req->params->{aanvrager_edit}) {
        $c->session->{_zaak_create}->{aanvrager_update} = $dv->valid;
    } elsif ($c->req->params->{contact_edit}) {
        for (qw/npc-email npc-telefoonnummer npc-mobiel/) {
            if (defined($c->req->params->{ $_ })) {
                $c->session->{_zaak_create}->{ $_ } =
                    $c->req->params->{ $_ };
            }
        }

    }
}

sub _zaak_create_load_externe_data : Private {
    my ($self, $c) = @_;

    return unless $c->session->{_zaak_create}->{extern}->{verified} eq 'digid' &&
        $c->session->{_zaak_create}->{aanvrager_update};

    if($c->req->params->{aanvrager_update}) {

        my $id = $c->model('Betrokkene')->create(
            'natuurlijk_persoon',
            {
                %{ $c->session->{_zaak_create}->{aanvrager_update} },
                'np-authenticated'   => 0,
                'np-authenticatedby' => ZAAKSYSTEEM_GM_AUTHENTICATEDBY_DIGID,
            }
        );

        $c->session->{_zaak_create}->{ztc_aanvrager_id}
            = 'betrokkene-natuurlijk_persoon-' .  $id;
    }
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 VALIDATION_CONTACT_DATA

TODO: Fix the POD

=cut

=head2 VALIDATION_EXTERNAL_CONTACT_DATA

TODO: Fix the POD

=cut

=head2 ZAAKSYSTEEM_GM_AUTHENTICATEDBY_DIGID

TODO: Fix the POD

=cut

=head2 login

TODO: Fix the POD

=cut

=head2 logout

TODO: Fix the POD

=cut

