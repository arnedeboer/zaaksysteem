package Zaaksysteem::Controller::Plugins::Maps;

use Moose;

use Geo::Coder::Google;

BEGIN { extends 'Zaaksysteem::Controller' }

use constant PROXY_MAP  => {
    geodata => 'https://geodata.nationaalgeoregister.nl',
};

=head2 proxy(type)

Poor mans proxy, use the following url to use it with nationaalregister.nl
geodata.

  http://catalyst.url/plugins/maps/proxy/geodata/tms/1.0.0/brtachtergrondkaart/3/3/3.png8

B<options>

=over 4

=item type

Which site type to use, see constant PROXY_MAP

=back

=cut

sub proxy : Local {
    my ($self, $c, $type)   = @_;

    $c->detach('/forbidden') unless PROXY_MAP->{$type};

    my $currentaction       = $c->uri_for($c->action) . '/' . $type;

    my $path                = $c->req->uri;
    $path                   =~ s/^$currentaction//g;

    $c->res->redirect(
        PROXY_MAP->{$type} . $path
    );
}

sub read : Chained('/'): PathPart('plugins/maps'): Args(0) {
    my ($self, $c, $term) = @_;
    my $local;

    if ($term) {
        $local++;
    }

    $term               ||= $c->req->params->{term};

    $c->detach('View::JSONlegacy') unless ($term);

    my $json_data       = { success => 0, addresses => [] };

    my $geocoder        = $c->model('Geo');

    eval {
        $geocoder->query($term);
        $geocoder->geocode;

        if ($geocoder->success) {
            $json_data->{addresses} = $geocoder->TO_JSON;
        }
    };

    if (scalar(@{ $json_data->{addresses} })) {
        $json_data->{success} = 1;
    }

    unless ($local) {
        $c->stash->{json}   = $json_data;
        $c->detach('View::JSONlegacy');
    }

    return $json_data;
}

sub _get_locations_from_case_query : Private {
    my ($self, $c, $search_query) = @_;

    my $results = $search_query->results(
        {
            c           => $c,
            get_total   => 'GET_TOTAL'
        }
    );

    $results    = $results->search(
        {
            'locatie_zaak'                          => { '!='   => undef },
            '-or'                                   => [
                'locatie_zaak.bag_nummeraanduiding_id'  => { '!='   => undef },
                'locatie_zaak.bag_openbareruimte_id'    => { '!='   => undef },
            ],
        },
        {
            join    => 'locatie_zaak'
        }
    );

    my @entries;
    while (my $case = $results->next) {
        my $bag = $case->locatie_zaak;

        unless ($bag->bag_coordinates_wsg) {
            $c->forward('_set_coordinates_on_bag', [$bag]);
        }

        next unless $bag->bag_coordinates_wsg;

        my $case_info       = {
            'id'            => $case->id,
            'description'   => $case->onderwerp,
            'requestor'     => (
                $case->aanvrager_object
                    ? $case->aanvrager_object->naam
                    : ''
            ),
            'manager'       => (
                $case->behandelaar_object
                    ? $case->behandelaar_object->naam
                    : ''
            ),
            'zaaktype'       => (
                $case->zaaktype_node_id->titel
            ),
            address         => $bag->maps_adres,
            url             => $c->uri_for('/zaak/' . $case->id)->as_string
        };

        $case_info->{id}    = $case->id;

        my $coords          = $bag->bag_coordinates_wsg;
        $coords             =~ s/[\(\)]//g;
        my @raw_coords      = split(',', $coords);
        $case_info->{coordinates}   = {
            lat => $raw_coords[0],
            lng => $raw_coords[1]
        };

        push(@entries, to_json($case_info));
    }

    return \@entries;
}

sub _set_coordinates_on_bag : Private {
    my ($self, $c, $bag) = @_;

    my $address = $bag->maps_adres;

    return unless $address;

    my $json_addresses = $c->forward('read', [ $address ]);

    return unless ($json_addresses && $json_addresses->{success});

    my $json_address = shift(@{ $json_addresses->{addresses} });

    my $coords       = $json_address->{coordinates};

    $bag->bag_coordinates_wsg($coords->{lat} . ',' . $coords->{lng});
    $bag->update;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 PROXY_MAP

TODO: Fix the POD

=cut

=head2 read

TODO: Fix the POD

=cut

