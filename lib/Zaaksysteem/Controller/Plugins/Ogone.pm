package Zaaksysteem::Controller::Plugins::Ogone;

use Moose;

use Zaaksysteem::Tools;
use Zaaksysteem::Constants;

BEGIN { extends 'Zaaksysteem::Controller' }

sub base : Chained('/') : PathPart('plugins/ogone/api'): CaptureArgs(0) {
    my ($self, $c) = @_;

    $c->stash->{nowrapper} = 1;

    $c->stash->{template}       = 'plugins/ogone/mislukt.tt';
    $c->stash->{ogone_error}    = 'Uw betaling is mislukt.';

    ### Loop over given parameters
    $c->stash->{ogone}      = $c->model('Plugins::Ogone');

    $c->stash->{ogone}->verify_payment(
        %{ $c->req->query_params },
    );

    $c->detach unless $c->stash->{ogone}->verified;

    ### Extra check. Make sure the amount is correct
    my $orderid     = $c->stash->{ogone}->orderid;

    my ($zaaknr)    = $orderid =~ /z(\d+)/;

    $c->stash->{zaak} = $c->model('DB::Zaak')->find($zaaknr);

    unless ($c->stash->{zaak}) {
        $c->log->error(
            'Z::C::P::Ogone->base: zaak not found'
        );

        $c->detach;
    }
}

sub betaling : Private {
    my ($self, $c) = @_;

    die "payment routine invoked without a case present" unless $c->stash->{zaak};

    my $zaak = $c->stash->{zaak}->discard_changes;
    my $human_amount = $zaak->payment_amount
        // $zaak->zaaktype_node_id->zaaktype_definitie_id->pdc_tarief;

    if (ref $human_amount eq 'CODE') {
        $human_amount = $human_amount->($c->stash->{ zaak });
        $human_amount =~ s|,|.|gis;
    }

    my $amount_in_cents = $self->amount_in_cents($human_amount);

    $c->stash->{ogone} = $c->model('Plugins::Ogone');
    $c->stash->{ogone_amount} = $human_amount;

    $c->stash->{ogone}->start_payment(
        amount => $amount_in_cents,
        zaak   => $c->stash->{ zaak },
    );

    $c->stash->{zaak}->payment_amount($human_amount); # not so human after all
    $c->stash->{zaak}->set_payment_status(CASE_PAYMENT_STATUS_PENDING, $human_amount);
    $c->stash->{zaak}->update();

    $c->stash->{nowrapper} = undef;
    $c->stash->{template}  = 'plugins/ogone/betaling.tt';
}


sig amount_in_cents => 'Num';

sub amount_in_cents {
    my ($self, $amount) = @_;

    # numbers + optionally a dot and 2 numbers
    # 123232 => OK
    # 12323. => wrong
    # 223223.1 => OK
    # 3231.34 => OK
    # .232332 => wrong
    # 0.22332 => OK

    $self->log->debug(sprintf("Original amount: %s", $amount));

    if ($amount && $amount =~ m|^\d+(\.\d+)?$|) {
        my $cents = $amount * 100;
        $self->log->debug("Ogone amount $amount in cents: $cents");
        return $cents;
    }
    else {
        throw("ogone/amount/invalid", "Invalid amount given : $amount");
    }
}


sub accept : Chained('base') : PathPart('accept'): Args(0) {
    my ($self, $c) = @_;

    unless ($c->stash->{ogone}->succes) {
        throw(
            'plugin/ogone/unsuccesful',
            'Ogone transaction indicates unsuccesful transaction. Bailing out.'
        );
    }

    # Some clarification is in order here, why bail out at all?
    # Payment transations are started once, and can only be finished once. So
    # checking the that the current status for this case is 'pending' is a
    # safe-guard against clickspam and other state-corrupting flows to this sub.
    if ($c->stash->{ zaak }->payment_status eq CASE_PAYMENT_STATUS_PENDING) {
        $c->forward('assert_amount');

        my $human_amount = sprintf("%.2f", ($c->stash->{ogone}->amount / 100));

        $c->stash->{zaak}->set_payment_status(CASE_PAYMENT_STATUS_SUCCESS, $human_amount);

        # Skipped notifications, now is time to send
        $c->forward('/zaak/_create_zaak_fire_phase_actions', [ 'email' ]);
    } else {
        $c->log->error(sprintf(
            'Skipping acceptance of payment for case %d which is not in-transation (pending confirmation).',
            $c->stash->{ zaak }->id
        ));
    }

    if ($c->session->{ _zaak_create }) {
        my $verified = $c->session->{ _zaak_create }{ extern }{ verified };

        $c->stash->{ is_preset_client } = $verified eq 'preset_client';
    }

    $c->stash->{zaaktype}   = $c->stash->{zaak}->zaaktype_node_id;
    $c->stash->{nowrapper}  = 1;
    $c->stash->{betaling}   = 1;

    $c->forward('/zaak/finish');
}


=head2 assert_amount

Make sure the amount ogone returns matches up with whatever is stored in the zaak.
During case creation it is possible to modify the amount using rules and
the contactchannel. The final amount isstored in the case.

=cut

sub assert_amount : Private {
    my ($self, $c) = @_;

    # check the amount that ogone return to the stored amount
    # in the zaak.payment_amount field
    my $ogone_amount  = $c->stash->{ogone}->amount;

    my $stored_amount
        = $self->amount_in_cents($c->stash->{zaak}->payment_amount);

    throw('plugin/ogone/corrupt_state',
        'Ogone payment amount and locally stored amount do not match, aborting. ' .
        "Ogone: $ogone_amount, stored: $stored_amount",
    ) unless $ogone_amount eq $stored_amount;

    $c->log->debug(sprintf(
        'Ogone transaction validated and accepted, payment received: %d¢',
        $stored_amount
    ));
}


sub decline : Chained('base') : PathPart('decline'): Args(0) {
    my ($self, $c) = @_;

    unless($c->stash->{zaak}->payment_status eq CASE_PAYMENT_STATUS_PENDING) {
        $c->log->warn('Received payment-declined for case %d, but transation status was not pending, giving up.');
        $c->detach;
    }

    $c->stash->{ogone_decline} = 1;
    $c->stash->{zaak}->set_payment_status(CASE_PAYMENT_STATUS_FAILED);
    $c->stash->{zaak}->set_deleted(process => 'Ogone', no_checks => 1);

    $c->delete_session('User declined ogone transaction');
}


# TODO: Evaluate if we need to keep the two actions below here... wut?
sub exception : Chained('base') : PathPart('exception'): Args(0) {
    my ($self, $c) = @_;

    #$c->stash->{zaak}->status('deleted');
}

=head2 cancel

The Ogone payment has been cancelled. The associated case will be deleted.

=cut

sub cancel : Chained('base') : PathPart('cancel'): Args(0) {
    my ($self, $c) = @_;

    $c->stash->{ogone_error}  = 'Uw betaling is geannuleerd.';
    $c->stash->{ogone_cancel} = 1;
    $c->stash->{zaak}->set_deleted(process => 'Ogone', no_checks => 1);
    $c->delete_session('User canceled ogone transaction');
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 CASE_PAYMENT_STATUS_FAILED

TODO: Fix the POD

=cut

=head2 CASE_PAYMENT_STATUS_PENDING

TODO: Fix the POD

=cut

=head2 CASE_PAYMENT_STATUS_SUCCESS

TODO: Fix the POD

=cut

=head2 accept

TODO: Fix the POD

=cut

=head2 amount_in_cents

TODO: Fix the POD

=cut

=head2 betaling

TODO: Fix the POD

=cut

=head2 cancel

TODO: Fix the POD

=cut

=head2 decline

TODO: Fix the POD

=cut

=head2 exception

TODO: Fix the POD

=head2 base

TODO: Fix the POD

=cut
