package Zaaksysteem::Controller::Plugins::PIP;

use Moose;

use File::stat;

use Zaaksysteem::Constants;
use Zaaksysteem::Tools;

BEGIN { extends 'Zaaksysteem::Controller' }

sub base : Chained('/') : PathPart('pip'): CaptureArgs(0) {
    my ($self, $c) = @_;

    ### ANNOUNCE PIP
    $c->stash->{pip} = 1;

    my $saml_state = $c->session->{ _saml } || {};

    ### Make sure we are logged in, or clean everything up, preventing other
    ### problems
    unless (
        $c->session->{pip} &&
        (
            (
                $c->session->{pip}->{ztc_aanvrager} =~
                    /^betrokkene-natuurlijk_persoon/ &&
                $saml_state->{ success }
            ) ||
            (
                $c->session->{pip}->{ztc_aanvrager} =~
                    /^betrokkene-bedrijf/ &&
                (
                    $c->model('Plugins::Bedrijfid')->succes ||
                    (
                        $saml_state->{ success } &&
                        $saml_state->{used_profile} =~ m[eherkenning|spoof]
                    )
                )
            )
        )
    ) {
        $c->log->debug(
            'Lost digid session, deleting pip: ' .
            ($c->session->{pip}->{ztc_aanvrager}||'-') . ':' .
            $saml_state->{ success } . ':' .
            $c->model('Plugins::Bedrijfid')->succes
        );

        delete($c->session->{pip});
    }

    if (
        !$c->session->{pip} &&
        $c->req->action !~ /pip\/login/
    ) {
        $c->response->redirect($c->uri_for('/pip/login'));
        $c->detach;
    }

    # Check if the interface exists, show the tab
    foreach (qw(controlpanel woz)) {
        $c->stash->{"show_$_"} = $c->model('DB::Interface')->search_active({ module => $_ })->first ? 1 : 0;
    }

    ($c->stash->{gws4all_interface}) = $c->model('DB::Interface')->search_module('gws4all');

    $c->stash->{betrokkene} = $c->model('Betrokkene')->get(
        {},
        $c->session->{pip}->{ztc_aanvrager}
    ) if $c->session->{pip};

    # only do this when actually logged in and when we want to show woz objects
    if($c->stash->{betrokkene} && $c->stash->{show_woz}) {
        $c->stash->{woz_objects} = $c->model('DB::WozObjects')->search_betrokkene_objects({
            betrokkene => $c->stash->{betrokkene}
        });
    }

    ### Make sure user is logged in with DigID
    $c->stash->{pip_session} = 1 if $c->session->{pip};
    $c->stash->{template_layout} = 'plugins/pip/layouts/pip.tt';
}


=head2 zaak_base

Abstract.

Load zaak from database. if fishy, redirect to main page.
Then load phase, execute rules.

=cut

sub zaak_base : Chained('base') : PathPart('zaak'): CaptureArgs(1) {
    my ($self, $c, $id) = @_;

    my $zaak = $c->stash->{zaak} = $c->model('DB::Zaak')->find($id);

    my $access_check = sub {
        return unless defined $zaak;
        return if $zaak->is_deleted;
        return if $zaak->zaaktype_node_id->prevent_pip;

        my (undef, $type, $id) = split m[\-], $c->session->{ pip }{ ztc_aanvrager };

        return unless defined $zaak->aanvrager && $zaak->aanvrager_object;
        return unless $zaak->aanvrager_object->betrokkene_identifier eq $c->session->{ pip }{ ztc_aanvrager } ||
                      $zaak->zaak_betrokkenen->search({
                          pip_authorized => 1,
                          betrokkene_type => $type,
                          gegevens_magazijn_id => $id
                      })->count;

        return 1;
    };

    unless($access_check->()) {
        $c->log->warn(sprintf(
            'Possible out-of-bounds access violation, user %s attempted to access case %d',
            $c->session->{ pip }{ ztc_aanvrager },
            $id
        ));

        $c->res->redirect($c->uri_for('/pip'));
        $c->detach;
    }

    ### Find fase
    my $fase = $c->req->params->{fase};
    if ($fase && $fase =~ /^\d+$/) {
        $c->stash->{requested_fase} = $zaak->zaaktype_node_id->zaaktype_statussen->search({ status  => $fase})->first;
    } else {
        $c->stash->{requested_fase} = $zaak->volgende_fase || $zaak->huidige_fase;
    }

    $c->stash->{is_array} = sub { my $argument = shift; return ref $argument && ref $argument eq 'ARRAY' };

    $c->forward('/zaak/_execute_regels');
}


sub zaak : Chained('zaak_base') : PathPart(''): Args() {
    my ($self, $c) = @_;

    $c->stash->{template_available} = $c->model('DB::Config')->get('feedback_email_template_id') ? 1 : 0;
    $c->stash->{aanvrager_email}    = $c->stash->{zaak}->aanvrager_object->email;

    $c->stash->{rule_engine}        = sub {
        my $opts    = shift || {};
        my $case    = shift;

        my $rv      = Zaaksysteem::Backend::Rules->generate_object_params(
            {
                'case' => $case,
                %{ $opts }
            }
        );

        if ($rv->{rules}) {
            ### Loop over statussen

            my @statussen = $c->stash->{zaak}->zaaktype_node_id->zaaktype_statussen->search(
                {}, {order_by => 'id'},
            );

            if (@statussen) {
                $rv->{active_attributes_per_status} = {};
            }

            for my $status (@statussen) {
                my $validation = $rv->{rules}->validate_from_case($case, { 'case.number_status' => $status->status });

                $rv->{active_attributes_per_status}->{$status->status} = $validation->active_attributes;
            }

            # $rv->{active_attributes} = $validation->active_attributes;
        }

        return $rv;
    };

    $c->stash->{template} = 'plugins/pip/case/view.tt';
    $c->stash->{ page_title } = $c->stash->{ zaak }->zaaktype_node_id->titel
}



sub view_element : Chained('zaak_base'): PathPart('view_element'): Args(1) {
    my ($self, $c, $element) = @_;

    $c->forward('/zaak/view_element', [ $element ]);
}

sub overview : Chained('zaak') : PathPart(''): Args() {
    my ($self, $c) = @_;



    $c->stash->{template} = 'plugins/pip/overview.tt';
}


sub index : Chained('base') : PathPart(''): Args(0) {
    my ($self, $c) = @_;

    $c->stash->{current_page} = 'zaken';
    $c->stash->{template}     = 'plugins/pip/index.tt';

    $c->stash->{onafgeronde_zaken} = $c->model('DB::ZaakOnafgerond')->search({
        betrokkene => $c->session->{pip}->{ztc_aanvrager},
    },
    {
        page                    => 1,
        rows                    => 10000, # template needs a pager :)
        order_by => { -desc => 'create_unixtime' }
    });

    $c->stash->{display_fields_onafgerond} = [qw/titel/];

    my %args = (
        page => 1,
        rows => 10000,    # template needs a pager :)
        betrokkene_type      => $c->stash->{betrokkene}->btype,
        gegevens_magazijn_id => $c->stash->{betrokkene}->ex_id,
        type_zaken           => 'open',
    );

    $c->stash->{zaken} = $c->model('Zaken')->zaken_pip({
        %args,
        as_aanvrager => 1
    });

    $c->stash->{afgehandelde_zaken} = $c->model('Zaken')->zaken_pip({
        %args,
        type_zaken   => [qw[resolved overdragen]],
        as_aanvrager => 1,
    });

    $c->stash->{gemachtigde_zaken} = $c->model('Zaken')->zaken_pip({
        %args,
        as_betrokkene => 1
    });

    $c->stash->{gemachtigde_afgehandelde_zaken} = $c->model('zaken')->zaken_pip({
        %args,
        type_zaken    => [qw[resolved overdragen]],
        as_betrokkene => 1,
    });

    $c->stash->{display_fields} = $c->model('SearchQuery')->get_display_fields({pip => 1});
}


sub contact : Chained('base') : PathPart('contact'): Args(0) {
    my ($self, $c) = @_;

    $c->stash->{current_page} = 'contact';

    my $res = $c->model('Betrokkene')->get(
        {},
        $c->session->{pip}->{ztc_aanvrager}
    );

    if (exists($c->req->params->{update})) {
        $res->mobiel($c->req->params->{'npc-mobiel'});
        $res->email($c->req->params->{'npc-email'});
        $res->telefoonnummer($c->req->params->{'npc-telefoonnummer'});
    }

    $c->stash->{'betrokkene'} = $res;

    $c->stash->{template} = 'plugins/pip/contact.tt';
    $c->stash->{ page_title } = 'Mijn gegevens';
}

sub instances : Chained('base') : PathPart('instances'): Args(0) {
    my ($self, $c) = @_;

    $c->stash->{current_page} = 'instances';

    $c->stash->{template} = 'plugins/pip/instances.tt';
}

=head2 uitkeringen

Show PIP uitkeringen

=cut

sub uitkeringen : Chained('base') : PathPart('uitkeringen'): Args(0) {
    my ($self, $c) = @_;

    $c->stash->{current_page} = 'uitkeringen';

    $c->stash->{template} = 'plugins/pip/uitkeringen.tt';
}

sub login : Chained('base') : PathPart('login'): Args() {
    my ($self, $c, $type) = @_;
    my ($bsn, $kvknummer, $samlid);

    my $saml_state = $c->session->{ _saml } || {};

    ### Type natuurlijk_persoon or bedrijf
    if (!$type) {
        # $c->stash->{template} = 'plugins/pip/login_type.tt';
        $c->stash->{template} = 'plugins/pip/layouts/pip.tt';
        $c->stash->{ page_title } = 'Inloggen';
        $c->stash->{login_type_page} = 1;

        # Retrieve possible saml error before logging out.
        if($c->session->{ _saml_error }) {
            my %dispatch = (
                'urn:oasis:names:tc:SAML:2.0:status:AuthnFailed' => 'cancelled',
                'urn:oasis:names:tc:SAML:2.0:status:NoAuthnContext' => 'context_insufficient',
                'urn:oasis:names:tc:SAML:2.0:status:PartialLogout' => 'partial_logout',
                'urn:oasis:names:tc:SAML:2.0:status:RequestDenied' => 'denied'
            );

            $c->stash->{ saml_error } = $dispatch{ $c->session->{ _saml_error } } || 'unknown';
        }

        ### Zorg voor een schone start
        $c->logout;
        $c->delete_session;

        # Setup IDPs
        $c->stash->{ citizen_idps } = [ $c->model('DB::Interface')->search_module('samlidp', '$.login_type_citizen') ];
        $c->stash->{ company_idps } = [ $c->model('DB::Interface')->search_module('samlidp', '$.login_type_company') ];

        $c->detach;
    }

    ### Just check if user is logged in via digid
    if ($type eq 'natuurlijk_persoon') {
        if (!$saml_state->{ success }) {
            $c->res->redirect($c->uri_for('/auth/saml/' . $c->req->param('idp_id'), {
                success_endpoint => $c->uri_for('/pip/login/natuurlijk_persoon')
            }));

            $c->detach;
        } else {
            $bsn = $saml_state->{ uid };
        }
    } elsif ($type eq 'bedrijf') {
        if (!$c->model('Plugins::Bedrijfid')->succes && !($saml_state->{ success } && $saml_state->{used_profile} =~ m[eherkenning|spoof])) {
            $c->res->redirect($c->uri_for('/auth/bedrijfid', {
                success_endpoint => $c->uri_for('/pip/login/bedrijf')
            }));

            $c->detach;
        } else {
            if ($c->model('Plugins::Bedrijfid')->succes) {
                $kvknummer = $c->model('Plugins::Bedrijfid')->login;
            } elsif ($saml_state->{ success } && $saml_state->{used_profile} =~ m[eherkenning|spoof]) {
                $samlid = $saml_state->{ uid }
            }
        }
    } else {
        $c->res->redirect($c->uri_for('/pip'));
        $c->detach;
    }

    if ($bsn) {
        my $res = $c->model('Betrokkene')->search(
            {
                type => 'natuurlijk_persoon',
                intern => 0,
            },
            {
                burgerservicenummer => $bsn
            },
        );

        if ($res->count) {
            my $bo = $res->next;

            if ($bo->gmid) {
                $c->session->{pip}->{ztc_aanvrager} = 'betrokkene-natuurlijk_persoon-'
                    . $bo->gmid;

                $c->push_flash_message('U bent succesvol aangemeld via Digid');
                $c->response->redirect($c->uri_for('/pip'));
                $c->detach;
            }
        }

        $c->log->debug("No subject found with bsn $bsn, logging out. Hint: create a subject with this bsn to log in using this digid account");

        ### Hmm, BSN not found, logout with message
        $c->push_flash_message(
            'U bent succesvol aangemeld, maar helaas kunnen'
            . ' wij geen zaken vinden in ons systeem. Om veiligheidsredenen'
            . ' bent u uitgelogd.'
        );

        $c->res->redirect($c->uri_for('/auth/digid/logout'));
        $c->detach;
    } elsif ($kvknummer || $samlid) {
        my $res;
        if ($kvknummer) {
            ### Search kvknumber
            my $bedrijf_auth = $c->model('DB::BedrijfAuthenticatie')->search(
                { 'login'     => $kvknummer }
            );

            if ($bedrijf_auth->count) {
                $res = $c->model('Betrokkene')->get(
                    {
                        type    => 'bedrijf',
                        intern  => 0,
                    },
                    $bedrijf_auth->first->gegevens_magazijn_id
                );

                $c->log->debug('Searching for identifier: ' . 'betrokkene-bedrijf-' . $bedrijf_auth->first->gegevens_magazijn_id);
            }
        } elsif ($samlid) {
            $kvknummer           = int(substr($samlid, 0, 8));
            my $vestigingsnummer = int(substr($samlid, 8, 12));
            $c->log->debug("Got SAML ID '$samlid', parsed into KvK nummer '$kvknummer' and vestigingsnummer '$vestigingsnummer'");

            $res = $c->model('Betrokkene')->search(
                {
                    type    => 'bedrijf',
                    intern  => 0,
                },
                {
                    dossiernummer       => $kvknummer,
                    $vestigingsnummer ? (vestigingsnummer    => $vestigingsnummer) : (),
                }
            );

            if ($res) {
                if ($res->count > 1) {
                    $c->log->error(
                        'PIP: Somehow we found more than one company with this kvknummer and/or vestigignsnummer'
                    );

                    $res = undef;
                } else {
                    $res = $res->next;
                }
            }
        }

        if ($res) {
            my $bo = $res;

            if ($bo->gmid) {
                $c->session->{ pip }{ ztc_aanvrager } = sprintf('betrokkene-bedrijf-%s', $bo->gmid);

                $c->push_flash_message('U bent succesvol aangemeld via Bedrijfid');
                $c->response->redirect($c->uri_for('/pip'));
                $c->detach;
            }
        }

        $c->log->debug("No subject found with kvk $kvknummer, logging out. Hint: create a subject with kvk bsn to log in using this eherkenning account");

        ### Hmm, BSN not found, logout with message
        $c->push_flash_message('U bent succesvol aangemeld, maar helaas kunnen'
            . ' wij geen zaken vinden in ons systeem. Om veiligheidsredenen'
            . ' bent u uitgelogd.'
        );

        $c->log->debug('Geen zaken voor Bedrijfid kvknummer: ' . $kvknummer);

        $c->res->redirect($c->uri_for('/auth/bedrijfid/logout'));
        $c->detach;
    }
}

sub logout : Chained('base') : PathPart('logout'): Args(0) {
    my ($self, $c) = @_;

    $c->delete_session;

    $c->response->redirect($c->uri_for('/pip'));
}

sub zaaktypeinfo : Chained('zaak_base'): PathPart('zaaktypeinfo'): Args(0) {
    my ($self, $c) = @_;

    $c->stash->{ nowrapper } = 1;
    $c->stash->{ template } = 'zaak/zaaktypeinfo.tt'
}

sub update_feedback : Chained('zaak_base'): PathPart('update/feedback') : Arg(0) {
    my ($self, $c) = @_;

    $c->res->redirect($c->req->referer);
    $c->detach unless $c->req->params->{note};

    $c->model('DB::Contactmoment')->contactmoment_create({
        type         => 'note',
        subject_id   => $c->stash->{ betrokkene }->betrokkene_identifier,
        created_by   => $c->stash->{ betrokkene }->betrokkene_identifier,
        case_id      => $c->stash->{ zaak }->id,
        medium       => 'webformulier',
        message      => $c->req->params->{ note },
    });

    my $event = $c->model('DB::Logging')->trigger('subject/contactmoment/create', {
        component => 'zaak',
        zaak_id => $c->stash->{ zaak }->id,
        created_by => $c->stash->{ betrokkene }->betrokkene_identifier,
        created_for => $c->stash->{ betrokkene }->betrokkene_identifier,
        data => {
            case_id => $c->stash->{ zaak }->id,
            content => $c->req->params->{ note },
            subject_id => $c->stash->{ betrokkene }->betrokkene_identifier,
            contact_channel => 'webformulier'
        }
    });
}


sub update_calendar_field : Chained('zaak_base') : PathPart('update_calendar_field') : Args() {
    my ($self, $c) = @_;

    my $params = $c->req->params;

    $c->stash->{zaak}->zaak_kenmerken->update_field({
        bibliotheek_kenmerken_id    => $params->{bibliotheek_kenmerken_id},
        new_values                  => [$params->{value}],
        zaak_id                     => $c->stash->{zaak}->id,
    });

    $c->forward('update_field_message', ['Kalender afspraak gewijzigd']);

    $c->response->body('OK');
    $c->detach();
}


sub update_field_message : Private {
    my ($self, $c, $description) = @_;

    my $case = $c->stash->{zaak} or die "need case";

    if ($case->behandelaar) {
        my $betrokkene_identifier = $case->behandelaar->betrokkene_identifier;

        $c->model('DB::Message')->message_create({
            message     => 'Kenmerk aangepast vanuit PIP: ' . $description,
            case_id     => $case->id,
            event_type  => 'case/pip/updatefield',
            subject_id  => $betrokkene_identifier,
        });
    }

}


sub request_attribute_update : Chained('zaak_base') : PathPart('request_attribute_update') {
    my ($self, $c) = @_;

    $c->stash->{ subject_identifier } = $c->session->{ pip }{ ztc_aanvrager };

    $c->forward('/api/case/request_attribute_update');
}

sub woz : Chained('base') : PathPart('woz'): Args() {
    my ($self, $c, $subpage, $id) = @_;

    $c->stash->{current_page} = 'woz';

    my $res = $c->model('Betrokkene')->get(
        {},
        $c->session->{pip}->{ztc_aanvrager}
    );

    if (exists($c->req->params->{update})) {
        $res->mobiel($c->req->params->{'npc-mobiel'});
        $res->email($c->req->params->{'npc-email'});
        $res->telefoonnummer($c->req->params->{'npc-telefoonnummer'});
    }

    $c->stash->{betrokkene} = $res;

    # load woz specific settings
    my $settings = $c->model('DB::Settings')->filter({
        filter => 'woz_'
    });

    foreach my $setting (@$settings) {
        $c->stash->{
            $setting->key
        } = $setting->value;
    }

    if($subpage && $id) {

        my $params = $c->req->params;

        $c->stash->{woz_objects} = $c->stash->{woz_objects}->search({
            owner => $params->{owner},
            object_id => $params->{object_id},
            id => $id
        });

        if($subpage eq 'report') {
            $c->stash->{extra_body_class} = ' woz-body';
            $c->stash->{layout_type} = 'simple';
            $c->stash->{woz_report} = 1;
            $c->stash->{template} = "plugins/woz/report.tt";
        }
        elsif ($subpage eq 'report_pdf') {
            $self->woz_report_pdf($c);
        }
        else {
            $c->stash->{template} = 'plugins/pip/woz.tt';
        }
    } else {
        $c->stash->{template} = 'plugins/pip/woz.tt';
    }
}

=head2 woz_report_pdf

Internal function, used to create the PDF version of the WOZ taxation report.

=cut

sub woz_report_pdf {
    my $self = shift;
    my ($c) = @_;

    my $settings = $c->model('DB::Settings')->filter({ filter => 'woz_' });
    my %settings;
    for my $setting (@$settings) {
        $settings{ $setting->key } = $setting->value;
    }

    my $bibliotheek_sjabloon = $c->model('DB::BibliotheekSjablonen')->find(
        $settings{woz_sjabloon}
    );

    my $context = Zaaksysteem::ZTT::Context::WOZ->new(
        woz_object => $c->stash->{woz_objects}->first,
        settings   => \%settings,
        betrokkene => $c->stash->{betrokkene},
    );

    my $pdf_data = $c->model('PDFGenerator')->generate_pdf(
        template => $bibliotheek_sjabloon,
        context  => $context
    );

    $c->res->content_type('application/pdf');
    $c->res->body($pdf_data);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 base

TODO: Fix the POD

=cut

=head2 contact

TODO: Fix the POD

=cut

=head2 index

TODO: Fix the POD

=cut

=head2 login

TODO: Fix the POD

=cut

=head2 logout

TODO: Fix the POD

=cut

=head2 overview

TODO: Fix the POD

=cut

=head2 request_attribute_update

TODO: Fix the POD

=cut

=head2 update_calendar_field

TODO: Fix the POD

=cut

=head2 update_feedback

TODO: Fix the POD

=cut

=head2 update_field_message

TODO: Fix the POD

=cut

=head2 view_element

TODO: Fix the POD

=cut

=head2 woz

TODO: Fix the POD

=cut

=head2 zaak

TODO: Fix the POD

=cut

=head2 zaaktypeinfo

TODO: Fix the POD

=cut

=head2 instances

TODO: Fix the POD

=cut
