package Zaaksysteem::Controller::UserSettings::FavoriteCasetypes;

use Moose;
use namespace::autoclean;

use List::Util qw(max);
use List::MoreUtils qw(insert_after);

use Zaaksysteem::Tools;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head1 NAME

Zaaksysteem::Controller::UserSettings::FavoriteCasetypes - ZAPI Controller

=head1 SYNOPSIS

=head1 DESCRIPTION

Zaaksysteem API Controller to manage a user's favorite case types.

=head1 METHODS

=head2 add

Add a new favorite case type.

=cut

sub base : Chained('/api/base') : PathPart('favorite/casetype') : CaptureArgs(0) {}

define_profile add => (
    required => ['object_id'],
    typed => {
        'object_id' => 'Int',
    },
);

sub add : Chained('base') : PathPart('add') : Args(0) : POST : ZAPI {
    my ($self, $c) = @_;
    my $opts = assert_profile($c->req->params)->valid;

    my $zaaktype = $c->model('DB::Zaaktype')->find($opts->{object_id});
    if (!$zaaktype) {
        throw(
            'request/invalid_parameter',
            "Zaaktype $opts->{object_id} was not found.",
        );
    }

    my $favorites = _get_favorites($c);
    my $id_max = max(map {$_->{id}} @$favorites) || 0;

    push @$favorites, {
        id          => $id_max + 1,
        casetype_id => $opts->{object_id},
    };

    _set_favorites($c, $favorites);
    $c->stash->{zapi} = [
        $favorites->[-1]
    ];
}

=head2 remove

Remove the specified case type from the user's favorite case type list.

=cut

define_profile remove => (
    required => ['favorite_id'],
    typed => {
        'favorite_id' => 'Int',
    },
);

sub remove : Chained('base') : PathPart('remove') : Args(0) : POST : ZAPI {
    my ($self, $c) = @_;
    my $opts = assert_profile($c->req->params)->valid;

    my $favorites = _get_favorites($c);

    @$favorites = grep { $_->{id} != $opts->{favorite_id} } @$favorites;

    _set_favorites($c, $favorites);
    $c->stash->{zapi} = [];
}

=head2 move

Move a favorite case type to a new location in the list.

=cut

define_profile move => (
    required => ['favorite_id'],
    optional => ['after'],
    typed => {
        'favorite_id' => 'Int',
        'after' => 'Int',
    },
);

sub move : Chained('base') : PathPart('move') : Args(0) : POST : ZAPI {
    my ($self, $c) = @_;
    my $opts = assert_profile($c->req->params)->valid;

    my $favorites = _get_favorites($c);

    my ($to_move) = grep { $_->{id} == $opts->{favorite_id} } @$favorites;
    @$favorites = grep { $_->{id} != $opts->{favorite_id} } @$favorites;

    if ($opts->{after}) {
        insert_after { $opts->{after} == $_->{id} } $to_move => @$favorites;
    }
    else {
        $favorites = [
            $to_move,
            @$favorites
        ];
    }

    _set_favorites($c, $favorites);

    $c->stash->{zapi} = [];
}

=head2 list

Return an (ordered) list of favorite casetype ids.

=cut

sub list : Chained('base') : PathPart('list') : Args(0) : GET : ZAPI {
    my ($self, $c) = @_;

    my $favorites = _get_favorites($c);

    $c->stash->{zapi} = [
        grep {
            defined($_->{object})
        }
        map {
            {
                type   => 'casetype',
                id     => $_->{id},
                object => $c->model('DB::Zaaktype')->find($_->{casetype_id}),
            };
        } @$favorites
    ];
}

=head2 _get_favorites

Helper function to retrieve case type favorites from userdata.

=cut

sub _get_favorites {
    my $c = shift;
    my $settings = $c->user->settings;

    return $settings->{favorite_case_types};
}

=head2 _set_favorites

Helper function to store case type favorites in userdata.

=cut

sub _set_favorites {
    my ($c, $favorites) = @_;

    my $settings = $c->user->settings;
    $settings->{favorite_case_types} = $favorites;

    $c->user->settings($settings);
    $c->user->update();

    return;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 base

TODO: Fix the POD

=cut

