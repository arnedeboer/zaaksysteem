package Zaaksysteem::Controller::UserSettings::MintlabLabs;

use Moose;
use namespace::autoclean;

use Zaaksysteem::Tools;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head1 NAME

Zaaksysteem::Controller::UserSettings::MintlabLabs - Expose boolean flag on users indicating

=head1 DESCRIPTION

=head1 CONSTANTS

=head2 LAB_FEATURE

This constant is a hash-reference with all enableable "lab" features. Values
are the defaults should a user with no labs settings use on of the actions.

=cut

use constant LAB_FEATURES => {
    object_search => \0
};

=head1 ACTIONS

=head2 base

The base (non-callable) action for this controller verifies the user settings's
state, creating a default item if no state yet exists.

=cut

sub base : Chained('/api/base') : PathPart('user/labs') : CaptureArgs(0) {
    my ($self, $c) = @_;

    my $settings = $c->user->settings;

    unless (exists $settings->{ labs }) {
        $settings->{ labs } = LAB_FEATURES();

        $c->user->settings($settings);
        $c->user->update;
    }

    my $id = $c->req->param('id');

    if($id) {
        unless (exists LAB_FEATURES()->{ $id }) {
            # Again, throw() won't die... http://i.imgur.com/4t7YnVN.jpg
            die("user/settings/labs: Unable to find feature \"$id\"");

            throw('user/settings/labs', sprintf(
                'Unable to find feature "%s"',
                $id
            ));
        }
    }
}

=head2 state

This action returns the current user settings state as a ZAPI response
containing one object.

=head3 URL

C</api/user/labs/state>

=head3 Example output

    {
        "next" : null,
        "status_code" : "200",
        "prev" : null,
        "num_rows" : 1,
        "rows" : 1,
        "comment" : null,
        "at" : null,
        "result" : [
            {
                "object_search" : false,
                .
                .
                .
            }
        ]
    }

=cut

sub state : Chained('base') : PathPart('state') : Args(0) {
    my ($self, $c) = @_;

    $c->stash->{ zapi } = [
        $c->user->settings->{ labs } // LAB_FEATURES()
    ];
}

=head2 toggle

Given an identifier for the setting, this action 'flips the switch' for it. So
true becomes false, false becomes true, and non-existant becomes true.

The returned response is what the L</state> controller would return after the
modification has been made.

=head3 URL

C</api/user/labs/toggle>

=head3 Parameters

=over 4

=item id

This parameter is expected to be a string representing the identifier for the
feature being toggled.

=back

=cut

define_profile toggle => (
    required => {
        id => 'Str'
    }
);

sub toggle : Chained('base') : PathPart('toggle') : Args(0) {
    my ($self, $c) = @_;

    my $opts = assert_profile($c->req->params)->valid;

    my $settings = $c->user->settings;

    $settings->{ labs }{ $opts->{ id } } = $settings->{ labs }{ $opts->{ id } } ? \0 : \1;

    $c->user->settings($settings);
    $c->user->update;

    $c->detach('state');
}

=head2 enable

Given an identifier for the feature, this action enables it for the current
user.

The returned response is what the L</state> controller would return after the
modification has been made.

=head3 URL

C</api/user/labs/enable>

=head3 Parameter

=over 4

=item id

This parameter is expected to be a string representing the identifier for the
feature being enabled.

=back

=cut

define_profile enable => (
    required => {
        id => 'Str'
    }
);

sub enable : Chained('base') : PathPart('enable') : Args(0) {
    my ($self, $c) = @_;

    my $opts = assert_profile($c->req->params)->valid;

    my $settings = $c->user->settings;

    $settings->{ labs }{ $opts->{ id } } = \1;

    $c->user->settings($settings);
    $c->user->update;

    $c->detach('state');
}

=head2 disable

Given an identifier for the feature, this action disables it for the current
user.

The returned response is what the L</state> controller would return after the
modification has been made.

=head3 URL

C</api/user/labs/disable>

=head3 Parameter

=over 4

=item id

This parameter is expected to be a string representing the identifier for the
feature being disabled.

=back

=cut

define_profile disable => (
    required => {
        id => 'Str'
    }
);

sub disable : Chained('base') : PathPart('disable') {
    my ($self, $c) = @_;

    my $opts = assert_profile($c->req->params)->valid;

    my $settings = $c->user->settings;

    $settings->{ labs }{ $opts->{ id } } = \0;

    $c->user->settings($settings);
    $c->user->update;

    $c->detach('state');
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 LAB_FEATURES

TODO: Fix the POD

=cut

