package Zaaksysteem::Controller::UserSettings::FavoriteSavedSearches;

use Moose;
use namespace::autoclean;

use List::Util qw(max);
use List::MoreUtils qw(insert_after);

use Zaaksysteem::Tools;

use Zaaksysteem::Types qw[UUID];

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head1 NAME

Zaaksysteem::Controller::UserSettings::FavoriteSavedSearches - ZAPI Controller

=head1 SYNOPSIS

=head1 DESCRIPTION

Zaaksysteem API Controller to manage a user's favorite saved searches

=head1 METHODS

=cut

sub base : Chained('/api/base') : PathPart('favorite/saved_search') : CaptureArgs(0) {
    my ($self, $c) = @_;

    my @favorites = _get_favorites($c);

    my @searches = $c->model('Object')->search('saved_search');

    $c->stash->{all_saved_searches} = \@searches;

    # First we prune saved_searches that may exist in the favorite settings,
    # but are nog longer visible to the user (perms or object->deleted)
    @favorites = grep {
        my $id = $_->{ saved_search_id };

        (scalar grep { $_->id eq $id } @searches) ? 1 : 0;
    } @favorites;

    my @new_shared_searches = grep {
        my $search = $_;

        # Not new if search already exists in favorites
        my $exists = scalar grep { $_->{ saved_search_id } eq $search->id } @favorites;

        $exists > 0 ? 0 : 1;
    } @searches;

    my $max = max(map { $_->{ id } } @favorites) || 0;

    push @favorites, map { {
        saved_search_id => $_->id,
        id => ++$max
    } } @new_shared_searches;

    _set_favorites($c, \@favorites);
}

=head2 add

Add a new favorite saved search.

=head3 URL

C</api/favorite/saved_search/add>

=head3 Parameters

=over 4

=item object_id

This parameter is expected to be a valid UUID that references a saved search
the user can access (he/she has read permissions for the object).

=back

=cut

define_profile add => (
    required => ['object_id'],
    typed => {
        'object_id' => UUID,
    },
);

sub add : Chained('base') : PathPart('add') : Args(0) : POST : ZAPI {
    my ($self, $c) = @_;
    my $opts = assert_profile($c->req->params)->valid;

    my $saved_search = $c->model('Object')->retrieve(uuid => $opts->{ object_id });
    if (!$saved_search) {
        throw(
            'request/invalid_parameter',
            "SavedSearch $opts->{ object_id } was not found.",
        );
    }

    my @favorites = _get_favorites($c);
    my $id_max = max(map { $_->{ id } } @favorites) || 0;

    push @favorites, {
        id              => $id_max + 1,
        saved_search_id => $opts->{ object_id },
    };

    _set_favorites($c, \@favorites);

    $c->stash->{ zapi } = [
        $favorites[-1]
    ];
}

=head2 remove

Remove the specified saved search from the user's favorite saved search list.

=head3 URL

C</api/favorite/saved_search/remove>

=head3 Parameters

=over 4

=item favorite_id

This parameter is expected to be a valid integer that references the favorited
item to be removed.

=back

=cut

define_profile remove => (
    required => {
        favorite_id => 'Int',
    },
);

sub remove : Chained('base') : PathPart('remove') : Args(0) : POST : ZAPI {
    my ($self, $c) = @_;
    my $opts = assert_profile($c->req->params)->valid;

    my @favorites = _get_favorites($c);

    @favorites = grep { $_->{ id } != $opts->{ favorite_id } } @favorites;

    _set_favorites($c, \@favorites);
    $c->stash->{ zapi } = [ @favorites ];
}

=head2 move

Move a favorite saved search to a new location in the list.

=head3 URL

<C/api/favorite/saved_search/move>

=head3 Parameters

=over 4

=item favorite_id

This parameter is expected to be a valid integer that references the favorited
item to be moved.

=item after

This optional parameter is expected to be a valid integer that references an
existing favorited item which should precede the C<favorite_id> provided.

Example (idiomatic):

    favorites = 1, 2, 3, 4, 5

    move(5, 1);

    favorites = 1, 5, 2, 3, 4

    move(4);

    favorites = 4, 1, 5, 2, 3

=back

=cut

define_profile move => (
    required => ['favorite_id'],
    optional => ['after'],
    typed => {
        'favorite_id' => 'Int',
        'after' => 'Int',
    },
);

sub move : Chained('base') : PathPart('move') : Args(0) : POST : ZAPI {
    my ($self, $c) = @_;
    my $opts = assert_profile($c->req->params)->valid;

    my @favorites = _get_favorites($c);

    my ($to_move) = grep { $_->{ id } == $opts->{ favorite_id } } @favorites;

    @favorites = grep { $_->{ id } != $opts->{ favorite_id } } @favorites;

    if ($opts->{ after }) {
        insert_after { $opts->{ after } == $_->{ id } } $to_move => @favorites;
    } else {
        unshift @favorites, $to_move;
    }

    _set_favorites($c, \@favorites);

    $c->stash->{ zapi } = [ @favorites ];
}

=head2 list

Return an (ordered) list of favorite saved search ids.

=head3 Example output

    {
        "next" : null,
        "status_code" : "200",
        "prev" : null,
        "num_rows" : 4,
        "rows" : 4,
        "comment" : null,
        "at" : null,
        "result" : [
            {
                "object" : { ... },
                "id" : 1,
                "type" : "saved_search"
            },
            {
                "object" : { ... },
                "id" : 4,
                "type" : "saved_search"
            },
            .
            .
            .
        ]
    }

=cut

sub list : Chained('base') : PathPart('list') : Args(0) : GET : ZAPI {
    my ($self, $c) = @_;

    my @favourites  = _get_favorites($c);

    my %objects     = map {
        $_->id => $_
    } @{ $c->stash->{all_saved_searches} };

    $c->stash->{ zapi } = [
        grep { defined $_->{ object } }
        map { {
            type   => 'saved_search',
            id     => $_->{ id },
            object => $objects{ $_->{ saved_search_id } }
        } } @favourites
    ];
}

=head1 INTERNAL METHODS

=head2 _get_favorites

Helper function to retrieve saved search favorites from userdata. Returns the
favorited items as a list.

=cut

sub _get_favorites {
    my $c = shift;
    my $settings = $c->user->settings;

    return unless exists $settings->{ favorite_saved_searches };

    return @{ $settings->{ favorite_saved_searches } };
}

=head2 _set_favorites

Helper function to store saved search favorites in userdata.

=cut

sub _set_favorites {
    my ($c, $favorites) = @_;

    my $settings = $c->user->settings;
    $settings->{ favorite_saved_searches } = $favorites;

    $c->user->settings($settings);
    $c->user->update();

    return;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 base

TODO: Fix the POD

=cut

