package Zaaksysteem::Controller::Page;

use Moose;

use File::stat;
use Zaaksysteem::Tools;

BEGIN { extends 'Zaaksysteem::Controller' }

=head2 index

Fallback for "/page" controller.

=cut

sub index :Path :Args(0) {
    my ( $self, $c ) = @_;

    $c->response->body('Matched Zaaksysteem::Controller::Page in Page.');
}

sub _preflight_request : Private {
    my ( $self, $c) = @_;

    ### No origin? Definitly not a preflight request
    return unless ($c->req->header('origin'));

    ### If referer is different from calling host, this could be a preflight
    ### request

    my $referer_uri = URI->new($c->req->referer);
    return unless (
        $referer_uri &&
        $c->req->base->host_port ne $referer_uri->host_port
    );

    ### Probably preflight, are we allowed?
    unless (
        $c->config->{preflight_iplist}
    ) {
        return 1;
    }

    my @allowed_ips;
    if (UNIVERSAL::isa($c->config->{preflight_iplist}, 'ARRAY')) {
        push(@allowed_ips, @{ $c->config->{preflight_iplist} });
    } else {
        push(@allowed_ips, $c->config->{preflight_iplist});
    }

    unless (
        grep { $_ && $c->req->address eq $_ } @allowed_ips
    ) {
        $c->log->debug(
            'PREFLIGHT: Unknown IP, if its valid,'
            .' add preflight_iplist = "' .
            $c->req->address . '" to zaaksysteem.conf'
        );
        return 1;
    }

    $c->log->debug('Preflight Request: IP [' . $c->req->address . '] Found, Allowed');

    $c->response->headers->header('Access-Control-Allow-Origin' => $c->req->header('origin'));
    $c->response->headers->header('Access-Control-Max-Age' => '1728000');
    $c->response->headers->header('Access-Control-Allow-Headers' => 'Content-Type,Accept,X-Requested-With');
    $c->response->headers->header('Access-Control-Allow-Credentials' => 'true');
    $c->response->headers->header('Access-Control-Allow-Methods' => '*');

    if (
        uc($c->req->method) eq 'OPTIONS' &&
        $c->req->header('Access-Control-Request-Headers') &&
        $c->req->header('Access-Control-Request-Method')
    ) {
        $c->res->body('OK');
        $c->detach;
    }
}

sub begin : Private {
    my ($self, $c) = @_;

    # No caching doen omdat IE de boel vernacheld
    my $useragent = $c->request->header('user-agent') || '';

    if($useragent =~ m|MSIE|) {
        $c->response->headers->last_modified(time);
        $c->response->headers->expires(time + ($self->{cache_time}||0));
        $c->response->headers->header(cache_control => "public, max-age=" . ($self->{cache_time}||0));
    }

    $c->forward('_preflight_request');

    ### Auth action:
    my $authaction = $c->req->action;
    $authaction =~ s|^/|| unless $authaction eq '/';
    $authaction = lc($authaction);

    $c->log->debug("-----------------------------------------------------")
        unless $ENV{CATALYST_DEBUG};

    $c->log->debug(
        "Request URI: ${$c->req->uri}; " .
        "Auth: requested action: $authaction"
    );

    ### PreAUTH: Speedbump for some special actions, javascript/css bundle
    if (
        $authaction eq 'page/minified' ||
        $authaction eq 'page/css_minified' ||
        $authaction eq '^html/(.*?)$' ||
        $authaction =~ /^tpl\/zaak_v1\/nl_nl\/css/
    ) {
        return 1;
    }

    if (
        $c->session->{zaaksysteem} &&
        $c->session->{zaaksysteem}->{mode} && $c->session->{zaaksysteem}->{mode} eq 'simple'
    ) {
        $c->stash->{layout_type} = 'simple'
    }

    $c->languages(['nl']) if $c->can('languages');

    # Hacking and slashing, merry on my way
    # Since access is fundamentally decided here, hook into the access attribute set on controllers
    my $access_cleared = 0;

    if(exists $c->action->attributes->{ Access }) {
        my ($tokens) = @{ $c->action->attributes->{ Access } };

        $access_cleared = grep { $_ eq '*' } split m[,\s*], $tokens;
    }

    if ($authaction eq 'man' && $c->model('DB::Config')->get('public_manpage')) {
        $access_cleared = 1;
    }

    if (
        $c->user_exists && $c->user->is_external_api &&
        $authaction !~ m[^api/(v1|externkoppelprofiel)/.*]
    ) {
        $c->log->error('API user tried accessing outside of namespace');

        $c->logout;
        $c->delete_session;
        $c->res->redirect('/auth/login');
        $c->detach;
    }

    # DigiD/eHerkenning sessions
    if ($authaction !~ m#api/users/(?:session|extend)#) {
        $c->check_saml_session ? $c->extend_saml_session_timeout() : $c->extend_session_timeout();
    }

    if (
        $c->user_exists &&
        (
            !$c->user->group_ids ||
            !scalar(@{ $c->user->group_ids })
        ) &&
        $authaction ne 'firstlogin/first_login' &&
        $authaction !~ /^auth/
    ) {
        $c->res->redirect($c->uri_for('/first_login'));
        $c->detach;
    }

    ### Make sure everyone is logged in
    if (
        !$c->user_exists &&
        $authaction !~ /^auth/ &&
        $authaction !~ /^form/ &&
        $authaction !~ m|^sysin/interface/trigger| &&
        $authaction !~ /^plugins\/pip/ &&
        $authaction !~ /^test.*/ &&
        $authaction !~ /^plugins\/digid.*/ &&
        $authaction !~ /^plugins\/maps.*/ &&
        $authaction !~ /^plugins\/woz\/.*/ &&
        $authaction !~ /^plugins\/bedrijfid.*/ &&
        $authaction !~ /^plugins\/ogone.*/ &&
        $authaction !~ /^api\/qmatic.*/ && # necessary for pip
        $authaction !~ /^api\/stuf\/bg0204.*/ &&
        $authaction !~ /^api\/stuf\/stuf0204.*/ &&
        $authaction !~ /^api\/stuf\/stuf0301.*/ &&
        $authaction !~ /^api\/rules\/base$/ &&
        $authaction !~ /^api\/mail.*/ &&
        $authaction !~ /^api\/users.*/ &&
        $authaction !~ /^api\/message.*/ &&
        $authaction !~ /^api\/v1\/.*/ &&
        $authaction !~ /^api\/scanstraat\/upload_document/ &&
        $authaction !~ m|^api/externkoppelprofiel/| &&
        $authaction !~ /^public\/search_case.*/ &&
        $authaction !~ /^kcc.*/ &&
        $authaction !~ /^logout.*/ &&
        $authaction !~ /^gegevens\/bag\/search.*/ &&
        $authaction !~ /^objectsearch\/bag.*/ &&
        $authaction ne 'api/kcc/register_call' &&
        $authaction ne 'schedule/run' &&
        $authaction ne 'zaak/create' &&
        $authaction ne 'zaak/start_nieuwe_zaak' &&
        $authaction ne 'page/retrieve_component' &&
        $authaction ne 'plugins/woz/woz_object_picture' &&
        $authaction ne 'plugins/woz/woz_object_picture_popup' &&
        $authaction ne 'search/public_map_by_id' &&
        $authaction ne 'sysin/interface/soap/enter' &&
        !$access_cleared &&
        ! (
            $authaction =~ /^beheer\/import\/.*\/run/ &&
            $c->config->{saas_range} &&
            $c->req->address =~ $c->config->{saas_range}
        ) &&
        ! (
            $authaction =~ /^gegevens\/bag\/import/ &&
            $c->config->{saas_range} &&
            $c->req->address =~ $c->config->{saas_range}
        )
    ) {
        $c->flash->{referer} = $c->req->uri;
        $c->response->redirect($c->uri_for('/auth/login'));
        $c->detach;
    }

    ### Give every controller a chance to load page specific data
    foreach my $controller ($c->controllers) {

        if ($c->controller($controller)->can('prepare_page')) {
            $c->controller($controller)->prepare_page($c);
        }
    }

    # Add these for all other dynamic requests, ZS-564 (Pentest issues ZS-557)
    $c->res->header('Cache-Control', 'no-cache, no-store, must-revalidate, private');
    $c->res->header('Pragma', 'no-cache');
    $c->res->header('X-Frame-Options', 'SAMEORIGIN');
    $c->res->header('X-Content-Type-Options', 'nosniff');

    return 1;
}

sub commit_message : Chained('/') : PathPart('page/commit_message') {
    my ($self, $c) = @_;

    $c->stash->{confirmation}->{type}       = 'yesno';

    $c->stash->{confirmation}->{uri}     = $c->req->uri;
    $c->stash->{confirmation}->{commit_message} = 1;
    $c->forward('/page/confirmation');
    $c->detach;
}


sub alert : Chained('/') : PathPart('page/alert') {
    my ($self, $c) = @_;

    $c->stash->{template} = 'alert.tt';
    $c->stash->{nowrapper} = 1;
    $c->detach;
}


sub confirmation : Chained('/') : PathPart('page/confirmation') {
    my ($self, $c) = @_;

    $c->stash->{template} = 'confirmation.tt';

    my $params = $c->req->params();

    if($params->{commit_message}) {
        $c->stash->{confirmation}->{commit_message} = 1;
    }
    $c->stash->{confirmation}->{message} ||= $params->{message};

    if ($c->req->is_xhr) {
        $c->stash->{nowrapper}  = 1;
        $c->stash->{xmlrequest} = 1;
    }
}


sub dialog : Private {
    my ($self, $c, $opt) = @_;

    Params::Profile->register_profile(
        'method'    => [caller(0)]->[3],
        'profile'   => $opt->{validatie}
    );

    ### Auth
    if ($opt->{permissions}) {
        $c->assert_any_zaak_permission(@{ $opt->{permissions} });
    } elsif ($opt->{user_permissions}) {
        $c->assert_any_user_permission(@{ $opt->{user_permissions} });
    } else {
        die(
            'Dialog handling: need at least permissions or '
           .' user_permissions option'
       );
    }

    $c->log->debug('Running validation with no json response');

    my $dv = $c->zvalidate(
        undef, {
            bypass_json => 1
        }
    );

    if ($c->req->is_xhr &&
        (
            $c->req->params->{do_validation} ||
            (!$dv || !$dv->success)
        )
    ) {
        $c->log->debug('Ajax request and validation requested');

        if ($c->req->params->{do_validation}) {
            $c->zvalidate;
            $c->detach;
        }

        $c->stash->{nowrapper} = 1;
        $c->stash->{template} = $opt->{template};
        $c->detach;
    }

    if (
        $c->req->params->{confirmed} &&
        (
            my $dv = $c->zvalidate(
                undef,
                {
                    bypass_json => 1,
                }
            )
        )
    ) {
        $c->log->debug('Confirmed call and validated');
        $c->res->redirect($opt->{complete_url})
            if (defined($opt->{complete_url}));

        return $dv;
    }

    return;
}


my $MINIFIED_MAPPING = {
    'css'   => {
        'common'    => 'common_header_includes_css.tt',
        'private'   => 'private_header_includes_css.tt',
    },
    'js'    => {
        'common'    => 'common_header_includes_js.tt',
        'private'   => 'private_header_includes_js.tt',
    },
};

sub css_minified : Path('/tpl/zaak_v1/nl_NL/css') {
    my ($self, $c, $template) = @_;

    $template =~ s/zsv_.*?-(\w+)\.css$/$1/;

    $c->forward('minified', [ $template, 'css' ]);
}

sub frontend_i18n_fallback : Regex('^html/(.*?)$') {
    my ($self, $c) = @_;

    $c->serve_static_file(
        $c->config->{root} . '/html/nl/' . $c->req->captures->[0]
    );
}

sub minified : Local {
    my ($self, $c, $template, $cat) = @_;

    $c->stash->{nowrapper}                      = 1;
    $c->stash->{invoke_assets_minified_request} = 1;

    ### Use zaaksysteem.js as modification time for last-modified header
    my $filename;
    if ($cat eq 'js') {
        $c->response->content_type("text/javascript");
        $filename    = $c->path_to(
            '/root/tpl/zaak_v1/nl_NL/js/zaaksysteem.js'
        );
    } else {
        $c->response->content_type('text/css');
        $filename    = $c->path_to(
            '/root/css/base.css'
        );
    }

    my $mtime       = $self->is_asset_modified($c, $filename);
    if ($MINIFIED_MAPPING->{$cat}->{$template}) {
        $c->stash->{template} = 'layouts/' .
            $MINIFIED_MAPPING->{$cat}->{$template};

        $c->response->headers->last_modified($mtime);
        $c->detach;
    };

    $c->res->body('Forbidden');
    $c->res->status(403);
}

sub is_asset_modified : Local {
    my ($self, $c, $filename) = @_;

    my $fileinfo = stat($filename);

    if (!$fileinfo) {
        $c->res->body('Forbidden');
        $c->res->status(403);
        $c->detach;
    }

    if (
        $c->req->headers->if_modified_since &&
        $c->req->headers->if_modified_since < time() &&
        $c->req->headers->if_modified_since >= $fileinfo->mtime
    ) {
        $c->res->status(304);
        $c->detach;
    }

    return $fileinfo->mtime;
}


sub javascript_ezra : Path('/tpl/zaak_v1/nl_NL/js/javascript_ezra.js') {
    my ($self, $c) = @_;

    $c->stash->{template} = 'layouts/javascript_ezra.tt';
    $c->stash->{nowrapper} = 1;
    $c->res->content_type('text/javascript');
}


sub javascript_libraries : Path('/tpl/zaak_v1/nl_NL/js/javascript_libraries.js') {
    my ($self, $c) = @_;


    $c->stash->{template} = 'layouts/javascript_libraries.tt';
    $c->stash->{nowrapper} = 1;
    $c->res->content_type('text/javascript');
}

sub about : Local {
    my ($self, $c)  = @_;

    $c->stash->{nowrapper} = 1;
    $c->stash->{template} = 'widgets/about.tt';

}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 about

TODO: Fix the POD

=cut

=head2 add_menu_item

TODO: Fix the POD

=cut

=head2 alert

TODO: Fix the POD

=cut

=head2 begin

TODO: Fix the POD

=cut

=head2 commit_message

TODO: Fix the POD

=cut

=head2 confirmation

TODO: Fix the POD

=cut

=head2 css_minified

TODO: Fix the POD

=cut

=head2 dialog

TODO: Fix the POD

=cut

=head2 frontend_i18n_fallback

TODO: Fix the POD

=cut

=head2 is_asset_modified

TODO: Fix the POD

=cut

=head2 javascript_ezra

TODO: Fix the POD

=cut

=head2 javascript_libraries

TODO: Fix the POD

=cut

=head2 minified

TODO: Fix the POD

=cut

=head2 retrieve_component

TODO: Fix the POD

=cut

=head2 under_construction

TODO: Fix the POD

=cut

