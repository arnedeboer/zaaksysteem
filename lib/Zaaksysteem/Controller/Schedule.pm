package Zaaksysteem::Controller::Schedule;

use Moose;

use NetAddr::IP;

use Zaaksysteem::Tools;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head1 NAME

Zaaksysteem::Controller::Schedule

=head1 SYNOPSIS

=head1 DESCRIPTION

Zaaksysteem controller for handling scheduled processes.

=head1 INSTRUCTIONS

=head1 METHODS

=head2 /schedule/run

Runs all jobs that are available on this domain/instance.

=cut

sub run
    : Chained('/')
    : PathPart('schedule/run')
    : ZAPI
    : DisableACL
{
    my ($self, $c) = @_;

    # Fallback: localhost can call the scheduler if it's not configured.
    $c->assert_allowed_ip($c->config->{scheduler_allowed_from} || '127.0.0.1'); 

    $c->model('DB::Transaction')->process_pending();

    $c->model('Zaken')->resume_temporarily_stalled_cases;
    $c->forward('check_buitenbeter_meldingen');

    $c->model('Scheduler')->run_pending();

    $c->stash->{zapi} = [{
        result => 'success',
    }];

}

sub check_buitenbeter_meldingen : Private {
    my ($self, $c) = @_;

    my $interface = $c->model('DB::Interface')->search_active({
        module => 'buitenbeter'
    })->first or throw("api/buitenbeter", "Buitenbeter koppeling is niet beschikbaar");

    my $cases = $interface->process_trigger('GetNewMeldingen');
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 check_buitenbeter_meldingen

TODO: Fix the POD

=cut

