package Zaaksysteem::Controller::Auth::SAML;

use Moose;

use Zaaksysteem::Tools;

use Zaaksysteem::SAML2;
use Zaaksysteem::SAML2::SP;

BEGIN { extends 'Zaaksysteem::Controller' }

=head1 NAME

Zaaksysteem::Controller::Auth::SAML;

=head1 METHODS

=head2 base

Base controller for SAML interactions. Validates the SAML Session State and
builds a L<Zaaksysteem::SAML2> object, which is available for chained requests
in the stash under the C<saml> key.

Also makes the SAML Session State available in the stash under the
C<saml_state> key, for easy access and all that.

=cut

define_profile base => (
    required => [qw[idp_id success]],
    optional => [qw[failure]],
);

sub base : Chained('/') : PathPart('auth/saml') : CaptureArgs(0) {
    my ($self, $c) = @_;

    if (ref($c->session->{_saml_state}) ne 'HASH') {
        $c->stash->{error_message} = sprintf(
            "Door de Identity Provider ge\N{U+00EF}nitieerde logins zijn niet ondersteund. (%s)",
            $c->stash->{request_id}
        );
        $c->stash->{template} = 'error.tt';
        $c->detach();
    }

    $c->stash->{ saml_state } = assert_profile($c->session->{ _saml_state }, error_wrapper => sub {
        return sprintf('SAML Session State could not be loaded: %s', shift);
    })->valid;

    $c->stash->{ saml } = Zaaksysteem::SAML2->new_from_interfaces(
        idp => $c->model('DB::Interface')->find($c->session->{ _saml_state }{ idp_id })
    );
}

=head2 initiate_exchange

This controller initiates a SAML2 Protocol Exchange. It also sets up a SAML
Session State which holds the required information to run the protocol
exchange.

=cut

define_profile initiate_exchange => (
    optional => [qw[failure_endpoint success_endpoint]],
    defaults => {
        success_endpoint => '/auth/login_saml'
    },
);

sub initiate_exchange : Chained('/') : PathPart('auth/saml') : Args(1) {
    my ($self, $c, $id) = @_;

    my $opts = assert_profile($c->req->params)->valid;

    if(exists $c->session->{ _saml_state }) {
        $c->log->debug(
            'Existing SAML Protocol Exchange state found in session, dumping info and clearing it.',
            sprintf("\tInterface: %s", $c->model('DB::Interface')->find($c->query_session('$._saml_state.idp_id'))->name),
            sprintf("\tSuccess endpoint: %s", $c->query_session('$._saml_state.success')),
            sprintf("\tFailure endpoint: %s", $c->query_session('$._saml_state.failure'))
        );
    }

    delete $c->session->{ _saml_state };

    my $saml = Zaaksysteem::SAML2->new_from_interfaces(
        idp => $c->model('DB::Interface')->find($id)
    );

    if($saml->idp->interface->jpath('$.saml_type') eq 'spoof') {
        unless($c->config->{ dev }) {
            throw('auth/saml/spoof', 'Refusing to spoof SAML Protocol Exchange on instances not configured with dev = truthy');
        }
    }

    $c->session->{ _saml_state } = {
        success => $opts->{ success_endpoint },
        failure =>    $opts->{ failure_endpoint }
                   || $c->req->referer
                   || $c->config->{gemeente}->{gemeente_portal},
        idp_id  => $id
    };

    $c->log->debug(
        'New SAML Protocol Exchange state created:',
        sprintf("\tInterface: %s", $saml->idp->interface->name),
        sprintf("\tSuccess endpoint: %s", $c->query_session('$._saml_state.success')),
        sprintf("\tFailure endpoint: %s", $c->query_session('$._saml_state.failure'))
    );

    $c->res->redirect($saml->authentication_redirect);
    $c->detach;
}

=head2 metadata

This controller action returns an XML document representing the configuration
of this Zaaksysteem instance as SP in the SAML authentication process.

=cut

sub metadata : Chained('/') : PathPart('auth/saml/metadata') : Args(0) {
    my ($self, $c)                  = @_;

    # Here starts a kludge to make metadata work without an IdP definition.
    my ($spi) = $c->model('DB::Interface')->search_module('samlsp');

    unless($spi) {
        throw(
            'saml2/metadata/no_sp_defined',
            'Unable to find a SP definition, cannot build SP metadata'
        );
    }

    my $sp = Zaaksysteem::SAML2::SP->new_from_interface(interface => $spi);

    $c->response->content_type('application/xml');
    $c->response->body($c->req->params->{signed} ? $sp->signed_metadata($c->req->params) : $sp->metadata($c->req->params));
}

=head2 single_logout (base)

This action is here merely as a placeholder for a generic 'single logout'
action. The actual implementation depends on the selected L<Net::SAML2::Binding>

=cut

sub single_logout : Chained('base') : PathPart('') : CaptureArgs(0) {
    my ($self, $c) = @_;

}

sub single_logout_soap : Chained('single_logout') : PathPart('slo-soap') : Args(0) {
    my ($self, $c) = @_;

}

sub single_logout_redirect : Chained('single_logout') : PathPart('sls-redirect-response') : Args(0) {
    my ($self, $c) = @_;

}

=head2 consumer (base)

This action is here merely as a placeholder for a generic 'consumer' action. The
actualy implementation depends on the selected L<Net::SAML2::Binding>.

=cut

sub consumer : Chained('base') : PathPart('') : CaptureArgs(0) {
    my ($self, $c) = @_;

}

=head2 consumer (post)

This controller action catches the UA coming back from the IdP with a
SAMLResponse encoded as HTTP Post data. We start the process of validating
the response, and if it does, the user is essentially authenticated.

=cut

define_profile consumer_post => (
    required        => [], # Dude... rancid
    require_some    => {
        saml_reply      => [qw[SAMLResponse SAMLart]]
    }
);

sub consumer_post : Chained('consumer') : PathPart('consumer-post') : Args(0) {
    my ($self, $c) = @_;

    my $params = assert_profile($c->req->params)->valid;

    my $saml_response = $params->{ SAMLResponse };

    if ($params->{ SAMLart }) {
        $saml_response = $c->stash->{ saml }->resolve_artifact($params->{ SAMLart });
    }

    my $assertion;

    try {
        $assertion = $c->stash->{ saml }->handle_response($saml_response);
    } catch {
        $c->log->error(sprintf('Error while handling SAMLResponse: %s', $_));

        if(eval { $_->can('object') } && $_->object) {
            if ($_->object->{status}) {
                $c->session->{ _saml_error } = $_->object->{ status };
            } else {
                $c->session->{ _saml_error } = 'Er is een onherstelbare fout opgetreden.';
            }

            if ($_->object->{response}) {
                $c->log->error('SAML Response xml: ' . $_->object->{response});
            }
        }

        $c->res->redirect($c->stash->{ saml_state }{ failure } || $c->uri_for('/'));
        $c->detach;
    };

    $c->session->{ _saml } = $c->stash->{ saml }->authenticated_identifier;
    $c->session_expire_key('_saml' => 900);

    if (
        $c->stash->{ saml }->authenticated_identifier &&
        $c->stash->{ saml }->authenticated_identifier->{success}
    ) {
        $c->statsd->increment('saml.login.ok', 1);
    }

    $c->res->redirect($c->stash->{ saml_state }{ success });
    $c->detach;
}

sub _is_eherkenning {
    my $self            = shift;
    my ($saml)          = @_;

    my $idp_entity_id = $saml->idp->interface->jpath('$.idp_entity_id');
    if (defined($idp_entity_id) && $idp_entity_id =~ /eherkenning/) {
        return 1;
    }

    return;
}

sub consumer_spoof : Chained('consumer') : PathPart('consumer-spoof') : Args(0) {
    my ($self, $c) = @_;

    unless($c->stash->{ saml }->idp->interface->jpath('$.saml_type') eq 'spoof') {
        throw('auth/saml2/spoof', 'Unable to spoof authentication request, IdP not configured as such.');
    }

    $c->session->{ _saml } = {
        used_profile => 'spoof',
        uid => $c->req->param('uid') || '',
        nameid => 'ehnope',
        success => 1
    };

    if ($self->_is_eherkenning($c->stash->{ saml })) {
        $c->session->{ _saml }->{uid} = sprintf("%08d", substr($c->req->param('kvk_dossiernummer'), 0, 8));
        $c->session->{ _saml }->{uid} .= sprintf("%012d", substr($c->req->param('kvk_vestigingsnummer'), 0, 12));
    }

    $c->log->debug('Spoofing UID: ' . $c->session->{ _saml }->{uid});

    $c->session_expire_key('_saml' => 900);

    $c->res->redirect($c->stash->{ saml_state }{ success });
    $c->detach;
}

sub prepare_spoof : Chained('base') : PathPart('prepare-spoof') : Args(0) {
    my ($self, $c) = @_;

    unless($c->stash->{ saml }->idp->interface->jpath('$.saml_type') eq 'spoof') {
        throw('auth/saml2/spoof', 'Unable to spoof authentication request, IdP not configured as such.');
    }

    $c->stash->{ is_eherkenning } = 1 if $self->_is_eherkenning($c->stash->{ saml });
    $c->stash->{ post_url } = $c->uri_for('/auth/saml/consumer-spoof');
    $c->stash->{ template } = 'auth/spoof_saml.tt';
}

=head2 consumer (artifact)

This controller action catches the UA coming back from the IdP with a reference
to a SAML artifact. This basically means we need to poke the configured IdP
with an L<Net::SAML2::Protocol::ArtifactResolve> request to the the data we
actually want (the SAMLResponse).

=cut

define_profile consumer_artifact => (
    required => [qw[SAMLart RelayState]]
);

sub consumer_artifact : Chained('consumer') : PathPart('consumer-artifact') : Args(0) {
    my ($self, $c) = @_;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 consumer_artifact

TODO: Fix the POD

=cut

=head2 consumer_post

TODO: Fix the POD

=cut

=head2 consumer_spoof

TODO: Fix the POD

=cut

=head2 prepare_spoof

TODO: Fix the POD

=cut

=head2 single_logout_redirect

TODO: Fix the POD

=cut

=head2 single_logout_soap

TODO: Fix the POD

=cut

