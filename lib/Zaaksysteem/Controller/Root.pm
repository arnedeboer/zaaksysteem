package Zaaksysteem::Controller::Root;

use Moose;

use Zaaksysteem::Tools;
use Zaaksysteem::Constants qw/
    ZAAKTYPE_KENMERKEN_ZTC_DEFINITIE
    ZAAKTYPE_KENMERKEN_DYN_DEFINITIE
/;

BEGIN { extends 'Zaaksysteem::Controller' }

#
# Sets the actions in this controller to be registered with no prefix
# so they function identically to actions created in MyApp.pm
#
__PACKAGE__->config->{namespace} = '';

=head2 begin

Always forwards to /page/begin, and uses its return value to determine whether
the request should be stopped or not.

=cut

sub begin : Private {
    my ($self, $c) = @_;

    ### C::P
    if (!$c->forward('/page/begin')) { return; }
}


sub index :Path :Args(0) {
    my ( $self, $c ) = @_;

    $c->forward('/zaak/list');
}


sub default :Path {
    my ( $self, $c ) = @_;
    $c->response->body( 'Page not found' );
    $c->response->status(404);
}

sub forbidden : Private {
    my ($self, $c) = @_;

    if ($c->res->status !~ /^4\d{2}$/) {
        $c->res->status(403);
    }
    $c->stash->{template} = 'forbidden.tt';
    $c->detach($c->view('TT'));
}

sub end : ActionClass('RenderView') {
    my ($self, $c) = @_;

    my @errors = @{ $c->error };

    $c->forward('touch_delayed_cases');

    if(scalar(@errors)) {
        my $err = shift @errors;

        $c->clear_errors;

        if(UNIVERSAL::isa($err, 'Zaaksysteem::Exception::Base')) {
            my $error = sprintf('Caught %s', $err->TO_STRING);
            my $sep = '=' x length $error;

            $c->error($error, $sep, join("\n", $err->trace_frames));

            if(($c->stash->{ current_view } // '') eq 'ZAPI') {
                $c->stash->{ zapi } = $err->get_ZAPI_error;
                $c->detach($c->view('ZAPI'));
            }
        } else {
            my $error = sprintf('Caught exception: %s', (blessed $err && $err->can('as_string')) ? $err->as_string : $err);

            $c->log->error($error);

            if(($c->stash->{ current_view } // '') eq 'ZAPI') {
                $c->stash->{ zapi } = Zaaksysteem::ZAPI::Error->new(
                    type     => 'unknown',
                    messages => $err,
                );

                $c->detach($c->view('ZAPI'));
            }
        }
    }
}

sub touch_delayed_cases : Private {
    my ($self, $c) = @_;

    my $schema = $c->model('DB')->schema;

    my $delayed_touch = $schema->default_resultset_attributes->{ delayed_touch };

    return unless scalar @{ $delayed_touch->cases };

    $c->log->debug(sprintf('Touching cases: %s', join(
        ', ',
        $delayed_touch->map_cases(sub { $_->id })
    )));

    $delayed_touch->execute($schema);

    return;
}

sub monitor : Global {
    my ($self, $c)  = @_;

    if ($c->req->params->{false}) {
        $c->res->body('CHECKFALSE (forcefalse: geforceerd afgebroken)');
        $c->detach;
    }

    my $logging = $c->model('DB::Logging')->search_events('monitor')->search({
        created => { '>', DateTime->now(time_zone => 'local')->subtract(minutes => 2) }
    });

    for my $event ($logging->all) {
        next unless $event->data->{ ip_address } eq $c->req->address;

        $c->res->body('CHECKOK');
        $c->detach;
    }

    my %errs;

    for my $check (qw/database/) {
        my $routine = '_monitor_' . $check;

        unless ((my $msg = $c->forward($routine)) eq 1) {
            $c->log->debug(sprintf(
                "Something's up, an error occurred while executing %s checks: %s",
                $check,
                $msg
            ));

            $errs{ $check } = $msg;
        }
    }

    unless(scalar(keys %errs)) {
        $c->res->body('CHECKOK');
        $c->detach;
    }

    $c->res->body(sprintf(
        'CHECKFALSE (%s)',
        join(',', map { sprintf('%s: %s', $_, $errs{ $_ }) } keys %errs)
    ));

    $c->detach;
}

sub _monitor_database : Private {
    my ($self, $c) = @_;

    my $event = $c->model('DB::Logging')->trigger('monitor/check', { component => 'monitor', data => {
        ip_address => $c->req->address
    }});

    unless($event) {
        return 'Could not write to database';
    }

    unless($c->model('DB::Logging')->search({}, { rows => 1 })->count) {
        return 'Could not read from database (or no logging found)';
    }

    return 1;
}

sub http_error : Private {
    my ( $self, $c, %opt ) = @_;
    my @valid_types = qw/404 403 500/;

    ### Defaults to 404 handling
    $opt{type} = 404 unless $opt{type};

    ### Some security awareness in place
    $opt{type} = 500 unless grep({$opt{type} eq $_} @valid_types);

    ### Set response status
    $c->res->status($opt{type});

    ### Error handling, send template error information and set view
    $c->stash->{error} = \%opt;
    $c->stash->{template} = 'error/' . $opt{type} . '.tt';

    return $opt{type};
}

sub logout : Local {
    my ($self, $c) = @_;

    $c->logout();
    $c->delete_session();

    my $redirect = $c->uri_for('/pip');
    if ($c->req->params->{redirect}) {
        $c->log->debug('REDIRECTING: ' . $c->req->params->{redirect});
        if ($c->req->params->{redirect} eq 'gemeente_portal') {
            $redirect = $c->config->{gemeente}->{gemeente_portal};
        }
    }

    $c->response->redirect($redirect);
    $c->detach;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 default

TODO: Fix the POD

=cut

=head2 end

TODO: Fix the POD

=cut

=head2 forbidden

TODO: Fix the POD

=cut

=head2 http_error

TODO: Fix the POD

=cut

=head2 index

TODO: Fix the POD

=cut

=head2 logout

TODO: Fix the POD

=cut

=head2 monitor

TODO: Fix the POD

=cut

=head2 touch_delayed_cases

TODO: Fix the POD

=cut

