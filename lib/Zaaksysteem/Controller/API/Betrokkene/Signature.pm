package Zaaksysteem::Controller::API::Betrokkene::Signature;

use Moose;
use Data::Dumper;
use File::stat;
use Image::Magick;

use Zaaksysteem::Tools;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

sub current : Local {
    my ($self, $c) = @_;

    my $subject = $self->get_subject($c);
    my $current_id = $subject->settings->{signature_filestore_id};

    die "need current id" unless $current_id;

    my $file = $c->model('DB::Filestore')->find($current_id);
    my $path = $file->get_path;

    $c->serve_static_file($path);

    $c->res->headers->content_length(stat($path)->size);
    $c->res->headers->content_type($file->mimetype);
}


sub upload : Local : ZAPI {
    my ($self, $c) = @_;

    # IE8 wants the specific content-type. ZAPI::JSON will use this variable
    $c->{stash}->{json_content_type} = $c->req->params->{return_content_type};

    my $signature = $c->request->uploads->{file};

    if ($self->validate_signature_file_format($c, $signature)) {
        my $file = $c->model('DB::Filestore')->filestore_create({
            original_name    => $signature->filename,
            file_path        => $signature->tempname,
            ignore_extension => 1,
        });

        my $subject = $self->get_subject($c);

        $self->store_signature($c, $file->id);

        $c->stash->{zapi} = [{
            filename => $file->original_name,
            uuid => $subject->uuid
        }];
    } else {
        throw('api/betrokkene/signature/upload/invalid_format', 'incorrect file format');
    }
}


sub delete : Local : ZAPI {
    my ($self, $c) = @_;

    my $subject = $self->get_subject($c);
    $subject->modify_setting('signature_filestore_id' => undef);

    $c->stash->{zapi} = [];
}


sub store_signature {
    my ($self, $c, $filestore_id) = @_;

    my $subject = $self->get_subject($c);
    $subject->modify_setting('signature_filestore_id' => $filestore_id);
}


sub get_subject {
    my ($self, $c) = @_;

    my $role = $c->model('DB::Config')->get('signature_upload_role') || '';

    if ($role eq 'behandelaar') {
        return $c->user;

    } elsif ($role eq 'zaaksysteembeheerder') {
        my $uuid = $c->req->params->{uuid} or die "need uuid";

        return $c->model('DB::Subject')->search({
            uuid => $uuid
        })->first or die "need subject";

    }
}

sub validate_signature_file_format {
    my ($self, $c, $signature) = @_;

    my $filename = $signature->tempname;
    unless ($filename =~ m/\.(jpg|gif|png)$/) {
        warn "signature file is not jpg/gif/png ($filename)";
        return;
    }

    my $magick = Image::Magick->new;
    my $error = $magick->Read($filename);
    return if $error;

    my $width  = $magick->Get('width');
    my $height = $magick->Get('height');

    # when changing this, also go into ZTT::OpenOffice and recalculate there.
    unless ($width eq '350' && $height eq '120') {
        warn "uploading signature image doesn't have correct dimensions ($width, $height)";
        return;
    }

    return 1; # valid
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 current

TODO: Fix the POD

=cut

=head2 delete

TODO: Fix the POD

=cut

=head2 get_subject

TODO: Fix the POD

=cut

=head2 store_signature

TODO: Fix the POD

=cut

=head2 upload

TODO: Fix the POD

=cut

=head2 validate_signature_file_format

TODO: Fix the POD

=cut

