package Zaaksysteem::Controller::API::Rules;

use Moose;
use Zaaksysteem::Tools;
use Zaaksysteem::Backend::Rules;

BEGIN {extends 'Zaaksysteem::General::ZAPIController'}

sub base : Chained('/') : PathPart('api/rules') : ZAPI {
    my ($self, $c)      = @_;

    my $rule_engine;
    if ($c->req->params->{case_id}) {
        $rule_engine     = Zaaksysteem::Backend::Rules->new_from_case(
            $c->model('DB::Zaak')->find($c->req->params->{case_id}),
        );
    } else {
        $rule_engine    = Zaaksysteem::Backend::Rules->new_from_casetype(
            $c->model('DB::ZaaktypeNode')->find($c->req->params->{'case.casetype.node.id'}),
            {
                %{ $c->req->params }
            }
        );
    }


    $c->stash->{zapi}   = $rule_engine->rules;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 base

TODO: Fix the POD

=cut

