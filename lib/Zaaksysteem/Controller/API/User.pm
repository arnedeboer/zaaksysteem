package Zaaksysteem::Controller::API::User;

use Moose;
use namespace::autoclean;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head1 NAME

Zaaksysteem::Controller::API::User

=head1 DESCRIPTION

=head1 ACTIONS

=head2 base

This action reserves the C</api/user> path namespace and checks that when
accessed, a user session exists.

If accessed while no user session exists, an exception will be thrown with the
type C<api/user/unauthorized>.

=cut

sub base : Chained('/api/base') : PathPart('user') : CaptureArgs(0) {
    my ($self, $c) = @_;

    unless ($c->user_exists) {
        throw('api/user/unauthorized', 'Log in first.');
    }
}

=head2 settings

This action allows the caller to perform CRUD operations on the the user's
settings.

A HTTP B<GET> request will simply respond with the settings object.
A HTTP B<POST> request expects a JSON request body with the new settings
object, and responds with the new settings.

To access only specific subkeys of the user settings, append the subkeys
to this action's pathparts.

=head3 URL

C</api/user/settings>, C</api/user/settings/labs>,
C</api/users/settings/favorite_saved_searches>, etc

=head3 Response

The response will be ZAPI-serialized, and contain one 'row', containing an
object with the user's settings.

For HTTP B<GET> C</api/user/settings>, it might look like this:

    {
       "at" : null,
       "comment" : null,
       "next" : null,
       "num_rows" : 1,
       "prev" : null,
       "result" : [
          {
             "favorite_saved_searches" : [
                {
                   "id" : 1,
                   "saved_search_id" : "797e57d9-b362-434e-a2e3-24888bb24cdb"
                }
             ],
             "kcc" : {
                "user_status" : false
             },
             "labs" : {
                "object_search" : true
             }
          }
       ],
       "rows" : 1,
       "status_code" : "200"
    }

For HTTP B<GET> C</api/user/settings/favorite_saved_searches> for the same
user, it will looke something like this:

    {
       "at" : null,
       "comment" : null,
       "next" : null,
       "num_rows" : 1,
       "prev" : null,
       "result" : [
          [
             {
                "id" : 1,
                "saved_search_id" : "797e57d9-b362-434e-a2e3-24888bb24cdb"
             }
          ]
       ],
       "rows" : 1,
       "status_code" : "200"
    }

=cut

sub settings : Chained('base') : PathPart('settings') : Args() {
    my ($self, $c, @path) = @_;

    my $settings = $c->user->settings;

    for (@path) {
        unless (exists $settings->{ $_ }) {
            $settings->{ $_ } = {};
        }

        $settings = $settings->{ $_ };
    }

    if ($c->req->method eq 'POST') {
        my %body = %{ $c->req->json_body };

        for (keys %body) {
            $settings->{ $_ } = $body{ $_ };
        }
    }

    $c->user->update({ settings => $settings });

    $c->stash->{ zapi } = [ $settings ];
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
