package Zaaksysteem::Controller::API::Authorization;

use Moose;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head2 org_unit

List available org_units, including the top-level
organization element - which typically is not shown, but
it's included for more flexibility and so that the depth
numbering may make sense.

=cut

sub org_unit : Local : ZAPI {
    my ($self, $c) = @_;

    my $tree = $c->model('DB::Groups')->get_as_tree($c->stash);

    $c->stash->{zapi} = [ $self->list_org_units($c, $tree) ];
}

sub list_org_units {
    my ($self, $c, $entries, $depth) = @_;

    $depth ||= 0;

    my $all_groups          = $c->model('DB::Groups')->get_all_cached($c->stash);
    my ($primary_group)     = grep { scalar @{ $_->path } == 1 } @$all_groups;

    my $all_roles           = $c->model('DB::Roles')->get_all_cached($c->stash);

    ### Primary group first, which is the company itself
    my @org_units;
    for my $group ($primary_group, grep({ scalar @{ $_->path } !=  1 } @{ $all_groups })) {
        my $result = {
            org_unit_id => "" . $group->id,  ### Make sure we send it as string and not integer, bw compatibility
            name        => $group->name,
            depth       => (scalar @{ $group->path } - 1),
            roles       => [],
        };

        for my $role (@{ $all_roles }) {
            next unless ($primary_group->id == $role->parent_group_id->id || $role->parent_group_id->id == $group->id);
            push(
                @{ $result->{roles} },
                {
                    system  => ($role->system_role ? 1 : 0),
                    name    => $role->name,
                    type    => 'entry',
                    role_id => "" . $role->id,
                }
            );
        }

        push(@org_units, $result);
    }

    return @org_units;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 format_role

TODO: Fix the POD

=cut

=head2 list_org_units

TODO: Fix the POD

=cut

