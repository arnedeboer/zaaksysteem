package Zaaksysteem::Controller::API::Message;

use Moose;
use namespace::autoclean;

use Zaaksysteem::Tools;

BEGIN { extends 'Zaaksysteem::Controller' }

=head1 NAME

Zaaksysteem::Controller::Message - Frontend library for Zaaksysteem messages.

=cut

=head2 create

Create a new file.

=head3 Arguments

=over

=item case_id [required]

=item message [required]

=back

=head3 Location

POST: /message/create

=head3 Returns

A JSON hash of the created object.

=cut

define_profile create => (
    required => [qw[message case_id]],
    optional => [qw[feedback_email]]
);

sub create
    : Chained('/')
    : PathPart('api/message/create')
{

    my ($self, $c) = @_;

    my $params = assert_profile($c->req->params)->valid;
    my ($case) = $c->model('DB::Zaak')->find($params->{case_id});

    my $betrokkene_identifier;
    if ($case->behandelaar) {
        $betrokkene_identifier = $case->behandelaar->betrokkene_identifier;
    }

    my $message = $c->model('DB::Message')->message_create({
        message     => $params->{message},
        case_id     => $params->{case_id},
        event_type  => 'case/pip/feedback',
        subject_id  => $betrokkene_identifier,
    });

    # Send a copy to the aanvrager if requested.
    if ($params->{feedback_email}) {
        my ($template) = $c->model('DB::BibliotheekNotificaties')->search({
            id => $c->model('DB::Config')->get('feedback_email_template_id'),
        });

        # Make sure magic string conversion knows which case is involved.
        $c->stash->{zaak} = $case;
        my $magic_strings = {
            pip_feedback_mail => $params->{message},
        };

        my $recipient = $case->notification_recipient({recipient_type => 'aanvrager'});

        if ($template && $recipient) {

            $case->mailer->send_from_case({
                recipient      => $recipient,
                subject        => $template->subject,
                body           => $template->message,
                additional_ztt_context => $magic_strings,
            });
        }
    };

    $c->stash->{json} = $message;
    $c->forward('Zaaksysteem::View::JSON');
}

=head2 get_for_user

Gets all messages for this user

=head3 Arguments

=over

=item page [optional]

=item rows [optional]

=back

=head3 Location

POST: /message/get_for_user?page=1&rows=5

=head3 Returns

JSON list of message object hashes.

=cut

sub get_for_user
    : Chained('/')
    : PathPart('api/message/get_for_user')
        : JSON
{
    my ($self, $c) = @_;

    my $rs = $c->model('DB::Message')->search(
        {
            subject_id => _get_subject($c)->betrokkene_identifier,
        },
        {
            page => $c->req->param('page') || 1,
            rows => $c->req->param('rows') || 10,
            join       => 'logging',
            order_by   => { -desc => 'logging.created' },
        },
    );

    my $pager = $rs->pager;
    if($pager->next_page) {
        $c->stash->{next_url} = $c->req->uri_with({
            before_id => undef,
            since_id => undef,
            page => $pager->next_page
        });
    }

    if($pager->previous_page) {
        $c->stash->{prev_url} = $c->req->uri_with({
            before_id => undef,
            since_id => undef,
            page => $pager->previous_page
        });
    }

    $c->stash->{json} = $rs;
    $c->forward('Zaaksysteem::View::JSON');
}


=head2 _get_subject

Helper for finding the Betrokkene object.

=cut

sub _get_subject {
    my ($c) = @_;
    return $c->model('Betrokkene')->get(
        {
            intern  => 0,
            type    => 'medewerker',
        },
        $c->user->uidnumber,
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
