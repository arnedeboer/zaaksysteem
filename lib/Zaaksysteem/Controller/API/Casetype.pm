package Zaaksysteem::Controller::API::Casetype;

use Moose;

use JSON qw[decode_json];

use Zaaksysteem::Tools;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

sub index : Chained('/') : PathPart('api/casetype') : CaptureArgs(1) {
    my ($self, $c, $casetype_id) = @_;

    $c->stash->{casetype} = $self->session_casetype($c, $casetype_id);
}


sub save_child_casetypes: Chained('index') : PathPart('save_child_casetypes') {
    my ($self, $c) = @_;

    my $child_casetypes = JSON::decode_json($c->req->params->{child_casetypes});

    # this effectively updates the casetype in the session
    $c->stash->{casetype}->{node}->{properties}->{child_casetypes} = $child_casetypes;
    $c->stash->{zapi} = [ $child_casetypes ];
}


sub is_casetype_mother : Chained('index') : PathPart('is_casetype_mother') {
    my ($self, $c) = @_;

    # this effectively updates the casetype in the session
    $c->stash->{casetype}->{node}->{properties}->{is_casetype_mother} =
        $c->req->params->{is_casetype_mother};

    $c->stash->{zapi} = [];
}


sub publish : Chained('index') : PathPart('publish') : ZAPI {
    my ($self, $c) = @_;

    try {
        $c->model('DB')->schema->txn_do(sub {
            my @children = $c->model('Zaaktypen')->list_child_casetypes({
                mother => $c->stash->{casetype}
            });

            my %queue = map {$_ => 0} (
                $c->stash->{casetype}->{zaaktype}->{id},
                map { $_->{casetype}{id} } @children
            );

            $self->initialize_queue($c, \%queue);

            $c->forward('publish_mother');

            # there will only be children if this has been configured
            foreach my $child_settings (@children) {
                $c->forward('publish_child', [$child_settings]);
            }

            $c->stash->{zapi} = [];

        });
    } catch {
        $c->stash->{zapi} = [];
    };
}


sub publish_mother : Private {
    my ($self, $c) = @_;

    my $commit_message = $c->req->params->{commit_message} or die "need commit_message";

    my $zaaktype_node = $c->model('Zaaktypen')->commit_session(
        session => $c->stash->{casetype},
        commit_message => $commit_message,
    );
    my $zaaktype_id = $zaaktype_node->get_column('zaaktype_id');

    $self->update_queue($c, $zaaktype_id);
    $self->flush($c, $zaaktype_id);
}


sub publish_child : Private {
    my ($self, $c, $child_settings) = @_;

    $c->model('Zaaktypen')->update_child_casetype({
        child_settings    => $child_settings,
        commit_message    => $c->req->params->{commit_message},
        mother            => $c->stash->{casetype}
    });

    $self->update_queue($c, $child_settings->{casetype}{id});

    # child casetypes may be open. when refreshing, the user
    # wants to see it updated with the applied changes
    $self->flush($c, $child_settings->{casetype}{id});
}


sub initialize_queue {
    my ($self, $c, $queue) = @_;

    $c->session->{casetype_mother} = $queue;
    $c->finalize_session; # save session so the publish_status request will see this
}


sub update_queue {
    my ($self, $c, $casetype_id) = @_;

    $c->session->{casetype_mother}->{$casetype_id} = 1;
    $c->finalize_session; # save session so the publish_status request will see this
}

sub publish_status : Chained('index') : PathPart('publish_status') : ZAPI {
    my ($self, $c) = @_;

    $c->stash->{zapi} = [$c->session->{casetype_mother}];
}

=head2 flush

casetypes are edited in the session - the pages manipulated the
casetype info in the session. after publishing the casetype is flushed
from the session.

=cut

sub flush {
    my ($self, $c, $casetype_id) = @_;

    die "need casetype_id" unless $casetype_id;

    delete $c->session->{zaaktypen}->{$casetype_id};
}


sub session_casetype {
    my ($self, $c, $casetype_id) = @_;

    return $c->session->{zaaktypen}->{$casetype_id} ||=
        $c->model('Zaaktypen')->retrieve(
            id         => $casetype_id,
            as_session => 1,
        ) or die "unable to load case type into session";
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 index

TODO: Fix the POD

=cut

=head2 initialize_queue

TODO: Fix the POD

=cut

=head2 is_casetype_mother

TODO: Fix the POD

=cut

=head2 publish

TODO: Fix the POD

=cut

=head2 publish_child

TODO: Fix the POD

=cut

=head2 publish_mother

TODO: Fix the POD

=cut

=head2 publish_status

TODO: Fix the POD

=cut

=head2 save_child_casetypes

TODO: Fix the POD

=cut

=head2 session_casetype

TODO: Fix the POD

=cut

=head2 update_queue

TODO: Fix the POD

=cut

