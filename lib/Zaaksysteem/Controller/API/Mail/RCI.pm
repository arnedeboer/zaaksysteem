package Zaaksysteem::Controller::API::Mail::RCI;

use Moose;

use MIME::Parser;
use MIME::Head;
use File::Copy;

use File::Temp qw/ :seekable/;

use Encode qw/from_to/;

use Data::Dumper;
use Zaaksysteem::Constants;

BEGIN { extends 'Zaaksysteem::Controller' }

### TODO
### - Prevent creation of duplicate betrokkenen
###

use constant RCI_XML_ABSTRACT  => [
    {
        property    => 'requestor',
        xpath       => '/data_ds/systeemkenmerken_aanvrager/*',
        filter      => sub {
            my $value   = shift;
            $value      =~ s/^\s*(.*?)\s*$/$1/;
            return $value;
        },
        mapping     => {
            'achternaam'        => {
                property    => 'geslachtsnaam',
            },
            'geboortedatum'     => {
                property    => 'geboortedatum',
                filter      => sub {
                    my $value   = shift;

                    my ($year, $month, $day) =
                        $value =~ /^(\d{4})(\d{2})(\d{2})$/;

                    return DateTime->new(
                        year    => $year,
                        day     => $day,
                        month   => $month,
                    );


                },
            },
            'geslacht'        => {
                property    => 'geslachtsaanduiding',
            },
            'postcode'        => {
                property    => 'postcode',
            },
#            'telefoonnummer'    => {
#                property    => 'telefoonnummer',
#            },
#            'email'             => {
#                property    => 'email',
#            },
            'voornamen'    => {
                property    => 'voornamen',
            },
        }
    },
    {
        property    => 'properties',
        xpath       => '/data_ds/systeemkenmerken_aanvrager/*',
        filter      => sub {
            my $value   = shift;
            $value      =~ s/^\s*(.*?)\s*$/$1/;
            return $value;
        },
        mapping     => {
            'rekeningnummer'        => {
                property    => 'rekeningnummer',
                filter      => sub {
                    my $value   = shift;

                    $value      = sprintf("%d", $value);

                    return undef unless $value;
                    return $value;
                }
            },
            'offertenummer'        => {
                property    => 'offertenummer',
                filter      => sub {
                    my $value   = shift;

                    $value      = sprintf("%d", $value);

                    return undef unless $value;
                    return $value;
                }
            },
            'contractnummer'        => {
                property    => 'contractnummer',
                filter      => sub {
                    my $value   = shift;

                    $value      = sprintf("%d", $value);

                    return undef unless $value;
                    return $value;
                }
            },
            'dealer'                => {
                property    => 'Dealernummer',
                filter      => sub {
                    my $value   = shift;

                    return sprintf("%d", $value);
                }
            },
        }
    },
    {
        property    => 'properties',
        xpath       => '/data_ds/fase_kenmerken/aanvullendebewijsstukken/*',
        filter      => sub {
            my $value   = shift;
            $value      =~ s/^\s*(.*?)\s*$/$1/;
            return $value;
        },
        mapping     => {
            'aantal_aanvullendebewijsstukken'   => {
                property    => 'Aanvullende documenten count',
                filter      => sub {
                    my $value   = shift;

                    $value      = sprintf("%d", $value);
                    return 'Geen' unless $value;
                    return $value;
                }
            },
            'aanvullend_document'   => {
                property    => 'Aanvullende documenten',
            }
        }
    },
];

sub handle : Private {
    my ($self, $c) = @_;

    eval {
        my $case_data   = $c->forward('_retrieve_case_data') or return;

        $c->log->debug(Dumper($case_data));

        my $requestor       = $c->forward(
            '_create_subject',
            [
                $case_data
            ]
        );

        unless ($requestor) {
            $c->res->body('ok');
            $c->detach;
        }

        my $case            = $c->forward(
            '_create_case',
            [
                $case_data,
                $requestor
            ]
        );

        if ($case) {
            $c->res->body('CASE: ' . $case->id);
            return;
        }
    };

    if ($@) {
        $c->log->error('Failed handling RCI mail: ' . $@);
    }

    $c->res->body('ok');
}

sub _retrieve_case_data : Private {
    my ($self, $c)  = @_;

    my $raw_case_data   = {
        requestor   => {},
        properties  => {},
    };

    ### Detect files
    {
        my ($parser, @parts) = $c
                                ->controller('API::Mail')
                                ->_retrieve_files( $c->req->params->{message} );

        my $found_xml;
        for my $part (@parts) {
            next unless $part->bodyhandle;

            my ($filename, $fh, $ext) = $c
                                ->controller('API::Mail')
                                ->_handle_part($part);


            if ($filename =~ /VERP/) {
                $c->forward('_extract_xml', [$raw_case_data, $fh->as_string])
                    or next;

                $c->log->debug('FOUND RCI XML');

                $found_xml++;
#            } else {
#                push(
#                    @{ $raw_case_data->{attachments} },
#                    {
#                        filename => lc($filename),
#                        fh       => $fh,
#                    }
#                );
            }
        }

        ### No XML, No valid RCI message, return
        $parser->filer->purge;

        unless ($found_xml) {
            return;
        }
    }

    $c->forward('_find_properties_by_name', [ $raw_case_data ]);

    my $case_data   = {
        requestor   => {
            type            => 'natuurlijk_persoon',
            properties      => $raw_case_data->{requestor}
        },
        properties          => $raw_case_data->{properties},
        human_properties    => $raw_case_data->{raw_properties},
        unique_id           => $raw_case_data->{unique_id}
    };


    ### Check for existing case
    return $case_data;
}

sub _find_properties_by_name : Private {
    my ($self, $c, $case_data) = @_;

    $case_data->{unique_id} = {};

    my $case_props = [];
    for my $key (%{ $case_data->{properties} }) {
        my $bib_kenmerk    = $c->model('DB::BibliotheekKenmerken')->search(
            {
                'naam'  => $key
            }
        )->first or next;

        if (
            $key eq 'Dealernummer' ||
            $key eq 'offertenummer' ||
            $key eq 'Aanvullende documenten' ||
            $key eq 'Aanvullende documenten count'
        ) {
            $case_data->{unique_id}->{ $key } = {
                bib_id  => $bib_kenmerk->id,
                value   => $case_data->{properties}->{$key}
            };
        }

        $c->log->debug(
            'Found case property: ' . $key . ': ' .
            $bib_kenmerk->id
        );

        push(@{ $case_props }, {
            $bib_kenmerk->id    => $case_data->{properties}->{$key}
        });
    }

    $case_data->{raw_properties}    = $case_data->{properties};
    $case_data->{properties}        = $case_props;
}

sub _extract_xml : Private {
    my ($self, $c, $case_data, $rawxml)  = @_;

    $c->forward('_parse_xml', [ $rawxml, $case_data ]) or return;
    $c->forward('_parse_xml_indications', [ $rawxml, $case_data ]);

    return 1;
}

sub _parse_xml_indications : Private {
    my ($self, $c, $xml, $case_data) = @_;

    $xml                =~ s/^\s.*?<\?xml/<?xml/gs;

    my $xp              = XML::XPath->new(xml => $xml);

    my $xpath_nodes = $xp->find('/data_ds/fase_kenmerken/bewijsstukken/*');
    my @documents;

    $case_data->{properties}->{'vereisten documenten'} = [];
    for my $node ($xpath_nodes->get_nodelist) {
        next unless $node->getName eq 'document_naam';

        my $value   = $node->string_value;
        $value      =~ s/^\s*(.*?)\s*$/$1/;

        $value      = $value;

        push(
            @{ $case_data->{properties}->{'vereisten documenten'} },
            $value
        );
    }
}

sub _parse_xml : Private {
    my ($self, $c, $xml, $case_data) = @_;

    $xml                =~ s/^\s.*?<\?xml/<?xml/gs;

    ### No valid RCI xml
    if ($xml !~ /data_ds/i) { return; }

    my $xp              = XML::XPath->new(xml => $xml);

    my $XML_ABSTRACT    = RCI_XML_ABSTRACT;

    for my $instruction (@{ $XML_ABSTRACT }) {
        my $key         = $instruction->{property};

        $case_data->{$key} = {} unless defined($case_data->{ $key });

        my $xpath_nodes = $xp->find($instruction->{xpath});
        for my $node ($xpath_nodes->get_nodelist) {
            my $value       = $instruction->{filter}->($node->string_value);
            my $property    = $node->getName;

            if (defined($instruction->{mapping}->{ $property })) {
                my $mapping = $instruction->{mapping}->{ $property };
                $property   = $mapping->{property};

                if (defined($mapping->{filter})) {
                    $value      = $mapping->{filter}->($value);
                }
            } else {
                unless (
                    defined($instruction->{keep_unknown}) &&
                    $instruction->{keep_unknown}
                ) {
                    next;
                }
            }

            if (
                defined($case_data->{ $key }->{ $property }) &&
                $case_data->{ $key }->{ $property }
            ) {
                if (UNIVERSAL::isa($case_data->{ $key }->{ $property }, 'ARRAY')) {
                    push(
                        @{ $case_data->{ $key }->{ $property } },
                        $value
                    );
                } else {
                    $case_data->{ $key }->{ $property } = [
                        $case_data->{ $key }->{ $property },
                        $value
                    ];
                }
            } else {
                $case_data->{ $key }->{ $property } = $value;
            }
        }
    }

    return 1;
}

sub _add_document : Private {
    my ($self, $c, $zaak, $fh, $filename) = @_;

    my $rci_kenmerken = $c
            ->customer_instance
            ->{start_config}
            ->{'Z::Plugins::RCI'}
            ->{kenmerken};

    unless ($rci_kenmerken->{ $filename }) {
        $c->log->debug('Did not find config for filename: ' . $filename);
        return;
    }

    my $kenmerk = $c->stash->{zaak}
        ->zaaktype_node_id
        ->zaaktype_kenmerken
        ->search(
            {
                'bibliotheek_kenmerken_id.value_type'   => 'file',
                'bibliotheek_kenmerken_id.naam'         =>
                    $rci_kenmerken->{ $filename }
            },
            {
                join    => 'bibliotheek_kenmerken_id'
            }
        );

    my %doc_args = (
        verplicht       => $kenmerk->value_mandatory,
        category        => $kenmerk->bibliotheek_kenmerken_id
                            ->document_categorie,
        pip             => $kenmerk->pip,
        catalogus       => $kenmerk->id,
        filename        => $filename,
        documenttype    => 'file',
    );

    return \%doc_args;
}


sub _DROPREPLACE_create_subject {
    my ($self, $c, $type, $properties) = @_;
    my $requestor;
    $properties  = { %{ $properties } };

    $c->log->debug('Called with: ' . Dumper(
        [$type, $properties]
    ));

    if ($type eq 'natuurlijk_persoon') {
        my $contact_map = {
            email           => '',
            telefoonnummer  => '',
            mobiel          => '',
        };

        for my $key (keys %{ $contact_map }) {
            next unless (
                defined($properties->{$key}) &&
                $properties->{$key}
            );

            $contact_map->{$key} = $properties->{$key};
            delete $properties->{$key};
        }

        my %oldproperties = map {
            'np-' . $_ => $properties->{ $_ }
        } keys %{ $properties};

        my $id = $c->model('Betrokkene')->create(
            'natuurlijk_persoon',
            {
                %oldproperties,
                authenticatedby =>
                    ZAAKSYSTEEM_GM_AUTHENTICATEDBY_BEHANDELAAR,
            }
        );

        $requestor = $c->model('Betrokkene')->get(
            {
                intern  => 0,
                type    => 'natuurlijk_persoon',
            },
            $id
        );

        $requestor->$_( $contact_map->{ $_ }) for keys %{ $contact_map };

        $c->log->debug('id: ' . $id);
    }

    return $requestor;
}

sub _create_subject : Private {
    my ($self, $c, $case_data) = @_;

    $c->log->debug('Called with: ' . Dumper(
        [$case_data]
    ));

    my $requestor_type  = $case_data->{requestor}->{type};
    my $requestor_prop  = $case_data->{requestor}->{properties};

    return $self->_DROPREPLACE_create_subject(
        $c,
        $requestor_type,
        $requestor_prop
    );
}

sub _create_case : Private {
    my ($self, $c, $case_data, $aanvrager) = @_;

    ### Check for existing case
    my $case;
    if ($case_data->{unique_id}) {
        $c->log->debug(Dumper($case_data->{unique_id}));

        my $offertenr = $c->model('DB::ZaakKenmerk')->search(
            {
                bibliotheek_kenmerken_id    => $case_data
                                                ->{unique_id}
                                                ->{'offertenummer'}
                                                ->{'bib_id'},
                value                       => $case_data
                                                ->{unique_id}
                                                ->{'offertenummer'}
                                                ->{value}
            }
        );

        $c->log->debug('Offertnr: ' . $offertenr);

        if ($offertenr->count) {
            $c->log->debug('Try find dealer!!!:' .
            $case_data
                                                        ->{unique_id}
                                                        ->{'Dealernummer'}
                                                        ->{'bib_id'}
            );
            if (
                my $dealer = $c->model('DB::ZaakKenmerk')->search(
                    {
                        bibliotheek_kenmerken_id    => $case_data
                                                        ->{unique_id}
                                                        ->{'Dealernummer'}
                                                        ->{'bib_id'},
                        zaak_id                     => {
                            '-in' => $offertenr->get_column('zaak_id')->as_query
                        },
                        value   => $case_data
                                                        ->{unique_id}
                                                        ->{'Dealernummer'}
                                                        ->{'value'},
                    }
                )->first
            ) {
                $case = $dealer->zaak_id;
            }
        }
    }

    if ($case) {
        $c->log->debug('Case found: ' . $case->id);

        my $extra_kenmerken = $case_data->{unique_id}->{
            'Aanvullende documenten'
        };
        $c->log->debug('Updating field: ' .
            Dumper($extra_kenmerken->{value})
        );

        $case->zaak_kenmerken->create_kenmerk(
            {
                bibliotheek_kenmerken_id    => $extra_kenmerken->{bib_id},
                values                      => $extra_kenmerken->{value},
                zaak_id                     => $case->id
            }
        );

        ### Count aanvullende documenten
        my $aanvullende_documenten_count = $case->zaak_kenmerken->search(
            {
                bibliotheek_kenmerken_id    => $extra_kenmerken->{bib_id},
                zaak_id                     => $case->id
            }
        )->count;

        $c->log->debug('Updating doc count to: ' .
            $aanvullende_documenten_count
        );

        $c->log->debug('update field options: ' . Dumper(
            {
                bibliotheek_kenmerken_id    => $case_data->{unique_id}->{
                                                'Aanvullende documenten count'
                                            }->{bib_id},
                zaak_id                     => $case->id,
                new_values                  => $aanvullende_documenten_count,
            }
        ));
        $case->zaak_kenmerken->update_field(
            {
                bibliotheek_kenmerken_id    => $case_data->{unique_id}->{
                                                'Aanvullende documenten count'
                                            }->{bib_id},
                zaak_id                     => $case->id,
                new_values                  => $aanvullende_documenten_count,
            }
        );

        $case->touch();

    } else {
        ### Search for existing case
        my $zaak_opts = {
            aanvraag_trigger    => 'extern',
            contactkanaal       => 'email',
            onderwerp           => 'RCI financiering',
            zaaktype_id         => $c->customer_instance
                ->{start_config}->{'Z::Plugins::RCI'}
                ->{zaaktype_id},
            aanvragers          => [
                {
                    betrokkene  => $aanvrager->betrokkene_identifier,
                    verificatie => 'medewerker',
                },
            ],
            kenmerken           => $case_data->{properties},
            registratiedatum    => DateTime->now(),
        };

        $case = $c->model('Zaken')->create($zaak_opts) or return;
    }

    ### Create txt document containing the case_data
    $c->forward('_add_xml_to_case', [ $case, $case_data ]);




    return $case;
}

sub _add_xml_to_case : Private {
    my ($self, $c, $case, $case_data)   = @_;

    my $tmph                        = File::Temp->new(UNLINK=>0, SUFFIX => 'txt');
    my $filename                    = scalar($tmph);

    print $tmph Dumper($case_data);
    $tmph->seek(0, IO::File::SEEK_SET);

    my $document    = $c->model('DB::File')->file_create({
        db_params => {
            created_by              => $case->aanvrager_object->betrokkene_identifier,
            case_id                 => $case->id,
            accepted                => 1,
        },
        file_path               => $filename,
        name                    => 'system_xml.txt',
    });

    unless(
        $document
    ) {
        $c->log->debug('ERROR!');
    }
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 RCI_XML_ABSTRACT

TODO: Fix the POD

=cut

=head2 ZAAKSYSTEEM_GM_AUTHENTICATEDBY_BEHANDELAAR

TODO: Fix the POD

=cut

=head2 handle

TODO: Fix the POD

=cut
