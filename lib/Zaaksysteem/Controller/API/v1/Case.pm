package Zaaksysteem::Controller::API::v1::Case;

use Moose;

use Zaaksysteem::Types qw[UUID];
use Zaaksysteem::Tools;
use Zaaksysteem::Object::Iterator;
use Zaaksysteem::Object::Types::ScheduledJob;
use Zaaksysteem::API::v1::PreparedFileBag;
use Zaaksysteem::API::v1::Set;
use Zaaksysteem::Constants qw[ZAAKSYSTEEM_CONSTANTS];

use Moose::Util::TypeConstraints qw[enum union];

use JSON qw[decode_json];
use DateTime;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

=head1 NAME

Zaaksysteem::Controller::API::v1::Case - APIv1 controller for case objects

=head1 DESCRIPTION

This is the controller API class for C<api/v1/case>. Extensive documentation about this
API can be found in:

L<Zaaksysteem::Manual::API::V1::Case>

Extensive tests about the usage via the JSON API can be found in:

L<TestFor::Catalyst::Controller::API::V1::Case>

=head1 ACTIONS

=head2 base

=cut

sub base : Chained('/api/v1/base') : PathPart('case') : CaptureArgs(0) : Scope('case') {
    my ($self, $c) = @_;

    my $zql = 'SELECT {} FROM case';
    my $query = $c->stash->{ interface }->jpath('$.query_constraint');

    # If set, resolve ZQL from saved_search object.
    if ($query) {
        $zql = try {
            # Ignore permissions on saved_search object. Enforcing it here
            # would only lead to confusion (you'd have to share the search
            # object with the API's selected user first).
            my $obj = $c->model('Object')->new_resultset->find($query->{ id });

            my $data = decode_json(
                $obj->get_object_attribute('query')->value
            );

            return $data->{ zql };
        } catch {
            $c->log->warn($_);

            throw(
                'api/v1/case',
                'API configuration error, unable to continue.'
            );
        };
    }

    $c->stash->{zql} = Zaaksysteem::Search::ZQL->new($zql);

    my $set = try {
        return Zaaksysteem::API::v1::Set->new(
            iterator => $c->model('Object')->zql_search($zql)
        )->init_paging($c->request);
    } catch {
        $c->log->warn($_);

        throw(
            'api/v1/case',
            'API configuration error, unable to continue.'
        );
    };

    $c->stash->{ set } = $set;
    $c->stash->{ cases } = $set->build_iterator->rs;
}

=head2 instance_base

=cut

sub instance_base : Chained('base') : PathPart('') : CaptureArgs(1) {
    my ($self, $c, $uuid) = @_;

    # Retrieve case via Object model so we can benefit from builtin ACL stuff
    $c->stash->{ case } = try {
        return $c->stash->{ cases }->find($uuid);
    } catch {
        $c->log->warn($_);

        throw('api/v1/case/retrieval_fault', sprintf(
            'Case retrieval failed, unable to continue.'
        ));
    };

    unless (defined $c->stash->{ case }) {
        throw('api/v1/case/not_found', sprintf(
            "The case object with UUID '%s' could not be found.",
            $uuid
        ), { http_code => 404 });
    }

    unless ($c->stash->{ case }->object_class eq 'case') {
        throw('api/v1/case/not_found', sprintf(
            "The case object with UUID '%s' could not be found.",
            $uuid
        ), { http_code => 404 });
    }
}

=head2 list

=head3 URL Path

C</api/v1/case>

=cut

sub list : Chained('base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    $c->stash->{ result } = $c->stash->{ set };
}

=head2 get

=head3 URL Path

C</api/v1/case/[UUID]>

=cut

sub get : Chained('instance_base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    my $query = $c->stash->{ interface }->jpath('$.query_constraint');
    my $zql;

    # Apply constraint query, if available.
    if ($query) {
        $zql = try {
            # Ignore permissions on saved_search object. Enforcing it here
            # would only lead to confusion (you'd have to share the search
            # object with the API's selected user first).
            my $obj = $c->model('Object')->new_resultset->find($query->{ id });

            my $data = decode_json(
                $obj->get_object_attribute('query')->value
            );

            return Zaaksysteem::Search::ZQL->new($data->{ zql });
        } catch {
            $c->log->warn($_);

            throw(
                'api/v1/case',
                'API configuration error, unable to continue.'
            );
        };
    }

    if ($zql) {
        $c->stash->{ serializer_opts } = {
            fields => [ $zql->cmd->probe_fieldnames ]
        };
    }

    $c->stash->{ result } = $c->stash->{ case };
}

=head2 create

=head3 URL Path

C</api/v1/case/create>

=cut

define_profile create => (
    required => {
        casetype_id => UUID,
        source => enum(ZAAKSYSTEEM_CONSTANTS->{ contactkanalen }),
        values => 'HashRef',
    },
    optional => {
        requestor => 'HashRef'
    }
);

sub create : Chained('base') : PathPart('create') : Args(0) : RW {
    my ($self, $c) = @_;

    my $params = assert_profile($c->req->params)->valid;
    my %values = %{ $params->{ values } };

    my $casetype = try {
        $c->model('Object')->retrieve(uuid => $params->{ casetype_id })
    } catch {
        $c->log->debug($_);

        throw(
            'api/v1/case/retrieval_fault',
            'Fault during casetype retrieval, unable to continue'
        );
    };

    unless (defined $casetype) {
        throw(
            'api/v1/case/casetype_not_found',
            sprintf(
                'Casetype "%s" could not be found, unable to continue',
                $params->{ casetype_id }
            ),
            { http_code => 400 } # sic, 400 indicates client fault
        );
    }

    my $node = try {
        $c->model('DB::Zaaktype')->find($casetype->casetype_id)->zaaktype_node_id;
    } catch {
        $c->log->debug($_);

        throw('api/v1/case/retrieval_fault', sprintf(
            'The casetype was found, but could not be retrieved from backend, unable to continue.'
        ));
    };

    # Extra validation for wrapped attribute values POST'ed to us.
    for my $key (keys %values) {
        unless (ref $values{ $key } eq 'ARRAY') {
            throw('api/v1/case/create', sprintf(
                'Every attribute value must be wrapped in an array. Validation for attribute "%s" failed.',
                $key
            ), { http_code => 400 });
        }
    }

    my $subject;

    if ($params->{ requestor }) {
        my $requestor = assert_profile($params->{ requestor }, profile => {
            required => {
                type => enum([qw[person company]]),
                id => union([qw[Str HashRef]])
            }
        })->valid;

        my %searchmap = (
            person => sub {
                return (
                    { type => 'natuurlijk_persoon', intern => 0 },
                    { burgerservicenummer => int($requestor->{ id }) }
                );
            },
            company => sub {
                return (
                    { type => 'bedrijf', intern => 0 },
                    {
                        dossiernummer => sprintf('%08d', $requestor->{ id }{ kvk_number }),
                        vestigingsnummer => $requestor->{ id }{ branch_number }
                    }
                );
            }
        );

        my $subjects = $c->model('Betrokkene')->search(
            $searchmap{ $requestor->{ type } }->()
        );

        # subjects->first is fundamentally broken
        # but I don't dare fix it, who knows what relies on that behavior
        $subject = $subjects->next;
    }

    unless (defined $subject) {
        my $definition = $node->zaaktype_definitie_id;

        unless ($definition->preset_client) {
            throw('api/v1/case/requestor_unresolvable', sprintf(
                'No (valid) requestor was provided, and no preset requestor available'
            ), { http_code => 400 })
        }

        $subject = $c->model('Betrokkene')->get({}, $definition->preset_client);
    }

    unless (defined $subject) {
        throw(
            'api/v1/case/create',
            'Unable to resolve requestor, unable to continue'
        );
    }

    my $kenmerken = $node->zaaktype_kenmerken->search_by_magic_strings(keys %values)->search(
        {
            required_permissions => [ undef, '{}' ], # 'specifieke behandelrechten' always off-limits
            'zaak_status_id.status' => 1
        },
        { prefetch => 'zaak_status_id' }
    );

    # File-based attributes and normal attributes need to be handled differently.
    my $normal_kenmerken = $kenmerken->search({
        'bibliotheek_kenmerken_id.value_type' => { '!=' => 'file' }
    });

    my $document_kenmerken = $kenmerken->search({
        'bibliotheek_kenmerken_id.value_type' => 'file'
    });

    # Unroll the intersection of supplied key-value-pairs and kenmerken in the
    # registration phase, and create an array of hashrefs of id => value pairs
    # Also, we don't yet support 'opplusbare velden' attributes, so deref the
    # first POST'ed value explicitly.
    my @normal_values = map { { $_->id => $values{ $_->magic_string }[0] } }
                        map { $_->bibliotheek_kenmerken_id }
                            $normal_kenmerken->all;

    my %document_values = map {
        $_->id => $values{ $_->bibliotheek_kenmerken_id->magic_string }
    } $document_kenmerken->all;

    try {
        my $zaak = $c->model('DB::Zaak')->create_zaak({
            zaaktype_id => $casetype->casetype_id,
            aanvraag_trigger => 'extern',
            contactkanaal => $params->{ source },
            delayed_touch => 0, # doesn't seem to do anything, _touch() below
            registratiedatum => DateTime->now,
            kenmerken => \@normal_values,
            aanvragers => [{
                betrokkene => $subject->betrokkene_identifier,
                verificatie => 'n/a',
            }]
        });

        if ($node->properties->{ preset_owner_identifier }) {
            $zaak->set_behandelaar($node->properties->{ preset_owner_identifier });
        }

        for my $zaaktype_kenmerk_id (keys %document_values) {
            my $refs = $document_values{ $zaaktype_kenmerk_id };

            for my $ref (ref $refs eq 'ARRAY' ? @$refs : $refs) {
                my $filestore_obj = $c->model('DB::Filestore')->find({
                    uuid => $ref,
                });

                next unless defined $filestore_obj;

                $c->model('DB::File')->file_create({
                    case_document_ids => [ $zaaktype_kenmerk_id ],
                    name => $filestore_obj->original_name,
                    db_params => {
                        accepted => 1,
                        filestore_id => $filestore_obj->id,
                        created_by => $c->user->betrokkene_identifier,
                        case_id => $zaak->id
                    }
                });
            }
        }

        $zaak->_touch;

        $c->stash->{ case } = $zaak->object_data;
    } catch {
        $c->log->debug($_);

        throw(
            'api/v1/case/create',
            'Case creation failed, unable to continue.'
        );
    };

    $c->forward('get');
}

=head2 prepare_file

=head3 URL

C</api/v1/case/prepare_file>

=cut

sub prepare_file : Chained('base') : PathPart('prepare_file') : Args(0) : RW {
    my ($self, $c) = @_;

    my @uploads = map { ref $_ eq 'ARRAY' ? @$_ : $_ } values %{ $c->req->uploads };

    unless (scalar @uploads) {
        throw('api/v1/case/upload', sprintf(
            'Upload(s) missing.'
        ), { http_code => 400 });
    }

    my $filestore = $c->model('DB::Filestore');

    $c->stash->{ result } = Zaaksysteem::API::v1::PreparedFileBag->new;

    for my $upload (@uploads) {
        my $file = try {
            return $filestore->filestore_create({
                original_name => $upload->filename,
                file_path     => $upload->tempname,
            });
        } catch {
            $c->log->warn($_);

            throw('api/v1/case/upload_validation', sprintf(
                'File creation failed, unable to continue.'
            ));
        };

        $c->stash->{ result }->add($file);

        my $clean_job = Zaaksysteem::Object::Types::ScheduledJob->new(
            job => 'CleanTmp',
            interval_period => 'once',
            next_run => DateTime->now->add(minutes => 15),
            data => $file->uuid
        );

        try { $c->model('Object')->save_object(object => $clean_job) } catch {
            $c->log->warn("Non-fatal; failed to schedule temporary file cleaner job. Original error follows:", $_);
        };
    }
}

=head2 transition

=head3 URL

C</api/v1/case/[UUID]/transition>

=cut

define_profile transition => (
    optional => {
        result_id => 'Num'
    }
);

sub transition : Chained('instance_base') : PathPart('transition') : Args(0) : RW {
    my ($self, $c) = @_;

    my $opts = assert_profile($c->req->params)->valid;

    my $zaak = try { $c->stash->{ case }->get_source_object } catch {
        $c->log->warn($_);

        throw('api/v1/case/retrieval_fault', sprintf(
            'Case retrieval failed, unable to continue.'
        ));
    };

    unless ($zaak->zaaktype_node_id->properties->{ api_can_transition }) {
        throw('api/v1/case/transition', sprintf(
            'Case transitioning is not enabled for this casetype.'
        ));
    }

    try {
        if(exists $opts->{ result_id }) {
            if($zaak->afhandel_fase->status eq ($zaak->milestone + 1)) {
                $zaak->set_result_by_id($opts->{ result_id });
            }
        }

        $zaak->advance({ context => $c });
    } catch {
        ### I was hoping to use ZS::Exception::grab() for this one, but it does not allow
        ### chaining yet.
        
        my $errormsg = 'unknown error';
        if (blessed($_) && $_->isa('Zaaksysteem::Exception::Base')) {
            ### TODO: We really need information about the "why", owner not complete? Missing result?
            my $object  = $_->object;

            $errormsg   = 'No transition possible for this case, missing ' .
                join(
                    ', ',
                    map(
                        { $_ =~ s/_complete//; $_; }
                        grep (
                            { ! $object->{transition_states}->{$_} }
                            keys %{ $object->{transition_states} }
                        )
                    ));

            $c->log->error('Cannot transition case: ' . $zaak->id . ', because: ' . $errormsg);

        } else {
            $c->log->error('Cannot transition case: ' . $zaak->id . ', because: ' . $_);
        }

        throw('api/v1/case/transition', sprintf(
            $errormsg
        ))
    };

    $zaak->_touch;

    $c->stash->{ case } = $zaak->object_data;

    $c->detach('get');
}

=head2 update

=head3 URL Path

C</api/v1/case/[UUID]/update>

=cut

define_profile update => (
    required => {
        values => 'HashRef'
    }
);

sub update : Chained('instance_base') : PathPart('update') : Args(0) : RW {
    my ($self, $c) = @_;

    my $opts = assert_profile($c->req->params)->valid;

    my $zaak = try { $c->stash->{ case }->get_source_object } catch {
        $c->log->warn($_);

        throw('api/v1/case/retrieval_fault', sprintf(
            'Case retrieval failed, unable to continue.'
        ));
    };

    # Shouldn't happen, internal fault.
    unless (defined $zaak) {
        $c->log->warn($_);

        throw('api/v1/case/retrieval_fault', sprintf(
            'Case retrieval failed, unable to continue.'
        ));
    }

    my @fields = keys %{ $opts->{ values } };

    my $kenmerken = $zaak->zaaktype_node_id->zaaktype_kenmerken->search(
        {
            # Only get those kenmerken we need.
            'bibliotheek_kenmerken_id.magic_string' => { -in => \@fields },

            # And only those in the current phase.
            'zaak_status_id.status' => $zaak->milestone + 1,

            # 'specifieke behandelrechten' always off-limits for API users
            required_permissions => [ undef, '{}' ]
        },
        {
            prefetch => [qw[bibliotheek_kenmerken_id zaak_status_id]]
        }
    );

    unless ($kenmerken->count) {
        # Note that this error may occur when a valid attribute name was
        # provided, but it isn't bound to the current phase.
        throw('api/v1/case/nop', sprintf(
            'Refusing to update because no supplied value resolved to an (authorized) attribute'
        ), { http_code => 400 });
    }

    my $normal_kenmerken = $kenmerken->search({
        'bibliotheek_kenmerken_id.value_type' => { '!=' => 'file' }
    });

    my $document_kenmerken = $kenmerken->search({
        'bibliotheek_kenmerken_id.value_type' => 'file'
    });

    my %new_values;
    my %document_values;

    # Iterate over all found attributes, ignoring the ones we could not
    # resolve, and build the %new_values var.
    while (my $type_kenmerk = $normal_kenmerken->next) {
        my $kenmerk = $type_kenmerk->bibliotheek_kenmerken_id;

        my $values = $opts->{ values }{ $kenmerk->magic_string };

        unless (ref $values eq 'ARRAY') {
            throw('api/v1/case/update', sprintf(
                'Every attribute value must be wrapped in an array. Validation for attribute "%s" failed.',
                $kenmerk->magic_string
            ), { http_code => 400 });
        }

        # We do not support 'opplusbare velden' yet, deref first item.
        $new_values{ $kenmerk->id } = $values->[0];
    }

    while (my $type_kenmerk = $document_kenmerken->next) {
        my $kenmerk = $type_kenmerk->bibliotheek_kenmerken_id;

        $document_values{ $type_kenmerk->id } = $opts->{ values }{ $kenmerk->magic_string };
    }

    try {
        $zaak->zaak_kenmerken->update_fields({
            new_values => \%new_values,
            zaak_id => $zaak->id
        });
    } catch {
        $c->log->warn($_);

        throw('api/v1/case/update_fault', sprintf(
            'Case update failed, unable to continue'
        ));
    };

    try {
        for my $zaaktype_kenmerk_id (keys %document_values) {
            my $refs = $document_values{ $zaaktype_kenmerk_id };

            for my $ref (ref $refs eq 'ARRAY' ? @$refs : $refs) {
                my $filestore_obj = $c->model('DB::Filestore')->find({
                    uuid => $document_values{ $zaaktype_kenmerk_id }
                });

                next unless defined $filestore_obj;

                $c->model('DB::File')->file_create({
                    case_document_ids => [ $zaaktype_kenmerk_id ],
                    name => $filestore_obj->original_name,
                    db_params => {
                        accepted => 1,
                        filestore_id => $filestore_obj->id,
                        created_by => $c->user->betrokkene_identifier,
                        case_id => $zaak->id
                    }
                });
            }
        }
    } catch {
        $c->log->warn($_);

        throw('api/v1/case/update_document_fault', sprintf(
            'Case document update failed, unable to continue.'
        ));
    };

    $zaak->_touch;

    $c->stash->{ case } = $zaak->object_data;

    $c->detach('get');
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
