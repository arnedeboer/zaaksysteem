package Zaaksysteem::Controller::API::v1::Sysin::Interface;

use Moose;
use Moose::Util::TypeConstraints qw[enum union];

use DateTime;
use JSON qw[decode_json];

use List::MoreUtils qw(none);

use Zaaksysteem::Tools;
use Zaaksysteem::Types qw(SubjectType);
use Zaaksysteem::Constants::Interfaces qw(ALLOWED_INTERFACES);

use Zaaksysteem::API::v1::ArraySet;

use Zaaksysteem::BR::Subject;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

### WARNING: Every action in this controller is ACCESS: Public, no logged in user required. Handle (custom)
### authentication in subroutines.

has 'api_capabilities' => (
    is          => 'ro',
    default     => sub { return [qw/extern public_access/] }
);

my @ALLOWED_INTERFACE_TYPES = (ALLOWED_INTERFACES);

=head1 NAME

Zaaksysteem::Controller::API::V1::Sysin::Interface - API calls for system integration module: Interface

=head1 DESCRIPTION

This is the controller API class for C<api/v1/sysin/interface>. Extensive documentation about this
API can be found in:

L<Zaaksysteem::Manual::API::V1::Sysin::Interface>

Extensive tests about the usage via the JSON API can be found in:

L<TestFor::Catalyst::Controller::API::V1::Sysin::Interface>

=head1 ACTIONS

=head2 base

Reserves the C</api/v1/sysin/interface> URI namespace.

=cut

sub base : Chained('/api/v1/base') : PathPart('sysin/interface') : CaptureArgs(0) : Scope('sysin') {}

=head2 security_base

Chains from the L</base> action and asserts several
authentication/authorization predicates.

=cut

sub security_base : Chained('base') : PathPart('') : CaptureArgs(0) {
    my ($self, $c)  = @_;

    $c->assert_user;

    $c->stash->{v1_serializer_options}{is_admin} = $c->check_any_user_permission('admin');
    $c->stash->{v1_serializer_options}{user}     = $c->user;
}

=head2 get_all

Retrieve interface configuration for all supported (whitelisted) interfaces.

=head3 URL Path

C</api/v1/sysin/interface/get_all>

=cut

sub get_all : Chained('security_base') : PathPart('get_all') : RO {
    my ($self, $c) = @_;

    my @interfaces = $c->model('DB::Interface')->search_active({
        module => \@ALLOWED_INTERFACE_TYPES
    })->all;

    $c->stash->{set} = Zaaksysteem::API::v1::ArraySet->new(
        content => \@interfaces
    );
    $self->list_set($c);
}

=head2 instance_base

=cut

sub instance_base : Chained('base') : PathPart('') : CaptureArgs(1) {
    my ($self, $c, $interface_id) = @_;

    ### Check for valid interface_id and existing of interface
    unless ($c->stash->{interface} = $c->model('DB::Interface')->search({ uuid => $interface_id })->first) {
        throw('api/v1/sysin/interface/unknown_id', 'Unknown interface id');
    }
}

=head2 trigger

Triggers an interface process

=head3 URL Path

C</api/v1/sysin/interface/[UUID]/trigger/[name]>

=cut

sub trigger : Chained('instance_base') : PathPart('trigger') : Args(1) : RW {
    my ($self, $c, $trigger)  = @_;
    my $params      = {};

    ### Get necessary params:
    $params->{request_params}   = $c->req->params;

    ### Hash with uploads as key value pair, e.g. { form_upload => bless({ ... }, 'Catalyst::Request::Upload') }
    $params->{uploads}          = $c->req->uploads;

    ### HTTP::Headers
    $params->{headers}          = $c->req->headers;

    ### Body
    $params->{body}             = $c->req->body;

    $c->log->debug('api/v1/interface/trigger - content-type: ' . $c->req->content_type);

    ### Method
    $params->{method}           = lc($c->req->method);

    ### API trigger
    my $result                  = $c->stash->{interface}->process_api_trigger($trigger, $params);

    my $serializer = Zaaksysteem::API::v1::Serializer->new;

    if (blessed($result)) {

        if ($result->isa('DBIx::Class::ResultSet')) {
            $c->stash->{ result } = {
                type => 'set',
                instance => {
                    rows => [ map { $serializer->read($_) } $result->all ]
                }
            }
        } elsif ($result->isa('DBIx::Class::Row')) {
            ### A downlaod?
            if (
                $c->req->params->{result_as} eq 'download' &&
                $result->isa('Zaaksysteem::Model::DB::File')
            ) {
                $c->stash->{document} = $result;
                $c->detach('/api/v1/case/document/download');
            } else {
                $c->stash->{ result } = $result;
            }
        }
    }
}

=head1 METHODS

=head2 get_by_module_name

Retrieve a specific interface configuration by interface name.

=cut

sub get_by_module_name : Chained('security_base') : PathPart('get_by_module_name') : Args(1) : RO {
    my ($self, $c, $module_name) = @_;

    my $name = lc($module_name);

    if (none {$_ eq $name} @ALLOWED_INTERFACE_TYPES) {
        throw("sysin/interface/get_module", "Unable to retrieve module information");
    }

    my @interfaces = $c->model('DB::Interface')->search_active({
        module => $module_name
    })->all;

    $c->stash->{set} = Zaaksysteem::API::v1::ArraySet->new(
        content => \@interfaces
    );
    $self->list_set($c);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
