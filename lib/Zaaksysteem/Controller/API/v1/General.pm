package Zaaksysteem::Controller::API::v1::General;
use Moose;

use Zaaksysteem::Tools;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

sub base : Chained('/api/v1/base') : PathPart('general') : CaptureArgs(0) : Scope('general') {}

__PACKAGE__->meta->make_immutable;

__END__

=head1 NAME

Zaaksysteem::Controller::API::v1::General - API v1 controller for general Zaaksysteem information and calls

=head1 DESCRIPTION

This is the controller API class for C<api/v1/general>.

=head1 ACTIONS

=head2 base

=head1 TESTS

L<TestFor::Catalyst::Controller::API::V1::Dashboard>

=head1 SEE ALSO

L<Zaaksysteem::Manual::API::V1::Dashboard>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
