package Zaaksysteem::Controller::API::v1::ScheduledJob;
use Moose;

use Zaaksysteem::Object::Types::ScheduledJob;
use Zaaksysteem::Object::Relation;
use Zaaksysteem::Tools;
use Zaaksysteem::Types qw(
    PackageElementStr
    Timestamp
    IntervalStr
    UUID
);

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

has api_capabilities => (
    is          => 'ro',
    default     => sub { return [qw/intern/] }
);

has '+namespace' => ( default => 'scheduled_jobs' );

=head1 NAME

Zaaksysteem::Controller::API::v1::ScheduledJob - API v1 controller for Scheduled Jobs

=head1 DESCRIPTION

This controller returns scheduled jobs

=head1 ACTIONS

=head2 base

=cut

sub base : Chained('/api/v1/base') : PathPart('scheduled_job') : CaptureArgs(0) : Scope('scheduled_job') {
    my ($self, $c) = @_;
    $self->get_set_from_zql($c, 'SELECT {} from scheduled_job');
}

=head2 list

=head3 URL Path

C</api/v1/scheduled_jobs>

=cut

sub list : Chained('base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;
    $self->list_set($c);
}

=head2 object_base

=cut

sub object_base : Chained('base') : PathPart('') : CaptureArgs(1) {
    my ($self, $c, $uuid) = @_;

    my $o = $c->stash->{$self->namespace}->find($uuid);

    if ($o) {
        $c->stash->{$self->namespace} = $o;
    }
    else {
        throw("scheduled_job/not_found", "Could not find scheduled job with UUID '$uuid'");
    }
}

=head2 get

=head3 URL Path

C</api/v1/scheduled_job/[UUID]>

=cut

sub get : Chained('object_base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;
    $self->get_object($c);
}

=head2 create

=head3 URL Path

C</api/v1/scheduled_job/create>

=cut

define_profile create => (
    required => {
        job      => PackageElementStr,
        next_run => 'Str',
    },
    optional => {
        interval_value  => 'Int',
        interval_period => IntervalStr,
        runs_left       => 'Int',
        interface       => UUID,
        data            => 'Any',
        # Type checking is done by other validators: pass through
        case            => 'Any',
        case_uuid       => UUID,
        casetype        => 'Any',
        casetype_uuid   => UUID,
        copy_relations  => 'Bool',
    },
    defaults => {
        interval_period => 'once',
        runs_left       => 0,
        copy_relations  => 0,
    },
);

sub create : Chained('base') : PathPart('create') : Args(0) : RW {
    my ($self, $c) = @_;

    $self->assert_post($c);

    my $params = assert_profile($c->req->params)->valid;
    my $o = Zaaksysteem::Object::Types::ScheduledJob->new(%{$params});
    if ($params->{job} eq 'CreateCase') {
        $self->_create_scheduled_case($c, { %$params, job => $o});
    }
    else {
        $self->save_object($c, $o);
    }
    $c->forward('get');
}

=head2 update

=head3 URL Path

C</api/v1/scheduled_job/[UUID]/update>

=cut

define_profile update => (
    required => {
        job       => PackageElementStr,
    },
    optional => {
        interval_value  => 'Int',
        interval_period => IntervalStr,
        next_run        => 'Str',
        runs_left       => 'Int',
    },
    missing_optional_valid => 1,
);

sub update : Chained('object_base') : PathPart('update') : Args(0) : RW {
    my ($self, $c) = @_;

    $self->assert_post($c);

    my $sj = $self->get_object($c);
    $sj = $c->model('Object')->inflate_from_row($sj);

    my $params = assert_profile($c->req->params)->valid;

    foreach (keys %$params) {
        my $clearer = "clear_$_";
        if (defined $params->{$_}) {
            $sj->$_($params->{$_});
        }
        elsif ($sj->can($clearer)) {
            $sj->$clearer;
        }
    }

    $self->save_object($c, $sj);
    $c->forward('get');
}

=head2 delete

=head3 URL Path

C</api/v1/scheduled_job/[UUID]/delete>

=cut

sub delete : Chained('object_base') : PathPart('delete') : Args(0) : RW {
    my ($self, $c) = @_;

    $self->assert_post($c);
    my $o = $self->get_object($c);

    my $sj = $c->model('Object')->inflate_from_row($o);

    if ($sj->does('Zaaksysteem::Object::Roles::Relation')) {
        if (!$sj->is_deleteable) {
            throw('api/v1/widgets/delete', "Unable to delete scheduled job");
        }
        $o->delete;
    }
    $c->forward('list');
}


define_profile _create_scheduled_job_relationships => (
    required => {
        job      => 'Zaaksysteem::Object::Types::ScheduledJob',
        case     => 'Zaaksysteem::Object::Types::Case',
        casetype => 'Zaaksysteem::Object::Types::Casetype',
    },
    optional => {
        copy_relations => 'Bool',
    },
    defaults => {
        copy_relations => 0,
    }
);

sub _create_scheduled_job_relationships {
    my $self = shift;
    my $options = assert_profile({@_})->valid;

    my $case_relation = Zaaksysteem::Object::Relation->new(
        locks_deletion      => 1,
        related_object_id   => $options->{case}->id,
        related_object_type => 'case',
        relationship_name_a => 'job_dependency',
        relationship_name_b => 'scheduled_by',
    );

    my $casetype_relation = Zaaksysteem::Object::Relation->new(
        locks_deletion      => 1,
        related_object_id   => $options->{casetype}->id,
        related_object_type => 'casetype',
        relationship_name_a => 'job_dependency',
        relationship_name_b => 'scheduled_by',
    );

    $options->{job}->add_relation(
        $case_relation
    );
    $options->{job}->add_relation(
        $casetype_relation
    );
    if ($options->{copy_relations}) {
        my @r = $options->{case}->all_relations;
        foreach (@r) {
            $options->{job}->add_relation($_);
        }
    }
    return $options->{job};
}

define_profile _create_scheduled_case => (
    required => {
        job => 'Any'
    },
    optional => {
        case            => 'HashRef',
        casetype        => 'HashRef',
        case_uuid       => UUID,
        casetype_uuid   => UUID,
        copy_relations  => 'Bool',
    },
    require_some => {
        case_or_uuid => [1, qw(case case_uuid)],
        casetype_or_uuid => [1 , qw(casetype casetype_uuid)],
    },
);

sub _create_scheduled_case {
    my ($self, $c, $options) = @_;
    $options = assert_profile($options)->valid;

    my $m = $c->model('Object');

    my $uuid = $options->{case_uuid} // $options->{case}{reference};
    my ($case, $casetype);

    if ($uuid) {
        $case = $m->retrieve(uuid => $uuid);
    }

    if (!$case) {
        throw('scheduled_jobs/relationship/unknown',
            "Unable to find case with uuid $uuid");
    }

    $uuid = $options->{casetype_uuid} // $options->{casetype}{reference};
    if ($uuid) {
        $casetype = $m->retrieve(uuid =>$uuid);
    }

    if (!$casetype) {
        throw('scheduled_jobs/relationship/unknown',
            "Unable to find casetype with uuid $uuid");
    }

    return $self->save_object(
        $c,
        $self->_create_scheduled_job_relationships(
            job            => $self->save_object($c, $options->{job}),
            case           => $case,
            casetype       => $casetype,
            copy_relations => $options->{copy_relations},
        )
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
