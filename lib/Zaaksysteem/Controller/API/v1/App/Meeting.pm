package Zaaksysteem::Controller::API::v1::App::Meeting;

use Moose;

use Zaaksysteem::Tools;
use Zaaksysteem::Types qw(UUID);

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

use constant MODULE_NAME => 'app_meeting';

=head1 NAME

Zaaksysteem::Controller::API::V1::App::Meeting - Meeting App specific api calls

=head1 DESCRIPTION

This is the controller API class for C<api/v1/app/meeting>. Extensive documentation about this
API can be found in:

L<Zaaksysteem::Manual::API::V1::App::Meeting>

Extensive tests about the usage via the JSON API can be found in:

L<TestFor::Catalyst::Controller::API::V1::App::Meeting>

=head1 ATTRIBUTES

=head2 api_capabilities

Capabilities of this API. For more information, see L<Zaaksysteem::Controller::API::v1>

=cut

has 'api_capabilities' => (
    is          => 'ro',
    default     => sub { return [qw/public_access/] }
);

=head2 api_control_module_types

Which modules control the access of this controller. For more information, see L<Zaaksysteem::Controller::API::v1>

=cut

has 'api_control_module_types' => (
    is          => 'rw',
    default     => sub { ['app', 'meeting'] },
);

=head1 ACTIONS

=head2 base

api/v1/app/app_meeting

=cut

sub base : Chained('/api/v1/base') : PathPart('app/app_meeting') : CaptureArgs(0) : Scope('sysin') {}

=head2 case_base

api/v1/app/app_meeting/case/UUID

=cut

sub case_base : Chained('base') : PathPart('case') : CaptureArgs(1) {
    my ($self, $c, $case_uuid) = @_;

    throw('api/v1/app/invalide_appname', 'Invalid uuid given') unless UUID->check($case_uuid);

    ### Check for valid interface_id and existing of interface
    my $app                 = $c->model('DB::Interface')->search_active({ module => MODULE_NAME })->first;

    if (!$app) {
        throw(
            'api/v1/app/not_found',
            'No app found by name: meeting'
        );
    }

    if ($app->get_interface_config->{access} ne 'rw') {
        throw(
            'api/v1/app/meeting/read_only',
            MODULE_NAME . ' is read-only'
        );   
    }

    my $object_data = $c->model('DB::ObjectData')->search({
        object_class    => 'case',
        uuid            => $case_uuid,
    })->first;
    my $case        = $c->model('DB::Zaak')->find($object_data->object_id);

    throw('api/v1/app/meeting/case/not_found', "Case not found by uuid: $case_uuid") unless ($object_data && $case);

    $c->stash->{zaak}            = $case;
    $c->stash->{case}            = $object_data;   
}

=head2 update_attributes

api/v1/app/app_meeting/case/UUID/update_attributes

=cut

define_profile update_attributes => (
    required    => {
        values  => 'HashRef',
    }
);

sub update_attributes : Chained('case_base') : PathPart('update_attributes') : Args(0) : RW {
    my ($self, $c)  = @_;
    my $params      = assert_profile($c->req->params)->valid;

    throw('api/v1/meeting/no_values_given', 'JSON parameter "values" not given') unless ref $params->{values} eq 'HASH';

    unless ($c->can_change()) {
        throw('api/v1/meeting/no_permissions', 'You have no permission to edit this case');
    }

    $c->stash->{zaak}->zaak_kenmerken->update_fields_authorized({
        new_values  => $params->{values},
        zaak        => $c->stash->{zaak},
    });

    $c->stash->{zaak}->_touch();

    ### Reretrieve object
    $c->stash->{case}   = $c->model('DB::ObjectData')->find($c->stash->{case}->uuid);

    $c->detach('/api/v1/case/get');
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
