package Zaaksysteem::Controller::API::v1::Controlpanel::Host;

use Moose;

use Zaaksysteem::Types qw[UUID FQDN ArrayRefIPs CustomerType Betrokkene Otap IPv4];
use Zaaksysteem::Tools;

use Zaaksysteem::Object::Types::Host;

use Moose::Util::TypeConstraints qw[enum union];

use JSON qw[decode_json];
use DateTime;

use Zaaksysteem::BR::Controlpanel;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

has 'api_capabilities' => (
    is          => 'ro',
    default     => sub { return [qw/extern allow_pip/] }
);

has 'api_control_module_types' => (
    is          => 'rw',
    default     => sub { ['controlpanel'] },
);


=head1 NAME

Zaaksysteem::Controller::API::v1::Controlpanel::Host - APIv1 controller for Controlpanel objects

=head1 DESCRIPTION

This is the controller API class for C<api/v1/controlpanel/ID/host>. Extensive documentation about this
API can be found in:

L<Zaaksysteem::Manual::API::V1::Controlpanel::Host>

Extensive tests about the usage via the JSON API can be found in:

L<TestFor::Catalyst::Controller::API::V1::Controlpanel::Host>

=head1 ACTIONS

=head2 base

=cut

sub base : Chained('/api/v1/controlpanel/instance_base') : PathPart('host') : CaptureArgs(0) : Scope('controlpanel') {
    my ($self, $c)      = @_;

    my $zql             = 'SELECT {} FROM host where owner="' . $c->stash->{controlpanel}->get_object_attribute('owner')->value . '"';

    $c->stash->{zql}    = Zaaksysteem::Search::ZQL->new($zql);

    my $set = try {
        return Zaaksysteem::API::v1::Set->new(
            iterator => $c->model('Object')->zql_search($zql)
        )->init_paging($c->request);
    } catch {
        $c->log->warn($_);

        throw(
            'api/v1/controlpanel/host',
            'API configuration error, unable to continue.'
        );
    };

    $c->stash->{ host_set }     = $set;
    $c->stash->{ hosts }        = $set->build_iterator->rs;
}

=head2 list

=head3 URL Path

C</api/v1/controlpanel/ID/host>

=cut

sub list : Chained('base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    $c->stash->{ result } = $c->stash->{ host_set };
}

=head2 instance_base

=cut

sub instance_base : Chained('base') : PathPart('') : CaptureArgs(1) {
    my ($self, $c, $uuid) = @_;

    # Retrieve case via Object model so we can benefit from builtin ACL stuff
    $c->stash->{ host } = try {
        return $c->stash->{ hosts }->find($uuid);
    } catch {
        $c->log->warn($_);

        throw('api/v1/controlpanel/host/not_found', sprintf(
            "The controlpanel host object with UUID '%s' could not be found.",
            $uuid
        ), { http_code => 404 });
    };

    unless (defined $c->stash->{ host } && $c->stash->{ host }->object_class eq 'host') {
        throw('api/v1/controlpanel/host/not_found', sprintf(
            "The controlpanel host object with UUID '%s' could not be found.",
            $uuid
        ), { http_code => 404 });
    }
}

=head2 get

=head3 URL Path

C</api/v1/controlpanel/[cpuuid]/host/[UUID]>

=cut

sub get : Chained('instance_base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    $c->stash->{ result } = $c->stash->{ host };
}


=head2 create

=head3 URL Path

C</api/v1/controlpanel/[cpuuid]/host/create>

=cut

define_profile create => (
    required => {
        label      => 'Str',
        ip         => IPv4,
    },
    optional => { ssl_key => 'Str', ssl_cert => 'Str', instance   => UUID, },
    require_some => {
        fqdn => [1, qw/fqdn/],
    },
    constraint_methods => {
        fqdn    => sub {
            my ($dfv, $val)     = @_;

            if ($val =~ /zaaksysteem\.[nlcorgmet]{2,3}$/) {
                $dfv->{_custom_messages}->{fqdn} = 'Hostname given for [_1] is reserved';
            } elsif (FQDN->check($val)) {
                return 1;
            }

            return;
        },
    },
    field_filters => {
        fqdn    => ['lc']
    },
    msgs    => sub {
        my $dfv     = shift;
        my $rv      = {};

        return $rv unless $dfv->{_custom_messages};

        for my $key (keys %{ $dfv->{_custom_messages} }) {
            $rv->{$key} = $dfv->{_custom_messages}->{$key};
        }

        return $rv;
    }
);

sub create : Chained('base') : PathPart('create') : Args(0) : RW {
    my ($self, $c) = @_;

    my $params  = assert_profile($c->req->params)->valid;

    ### Validate if owner already has an object
    if ($c->model('Object')->count('Host', { fqdn => $params->{fqdn} })) {
        throw(
            'api/v1/controlpanel/host/fqdn_exists',
            'controlpanel host already exists with given name'
        );
    }

    my $host;
    eval {
        $c->model('DB')->txn_do(
            sub {
                $host = $self->_create_host_object(
                    $c->model('Object'),
                    {
                        controlpanel        => $c->stash->{controlpanel},
                        %$params
                    },
                    $c
                );
            }
        );
    };

    if ($@) {
        throw(
            'api/v1/controlpanel/host/fault',
            'There was a problem creating this controlpanel host object: ' . $@
        );
    }

    $c->stash->{result} = $c->model('DB::ObjectData')->find({ uuid => $host->id });
}

define_profile _create_host_object => (
    required    => {
        controlpanel        => 'Zaaksysteem::Backend::Object::Data::Component',
        fqdn       => FQDN,
        label      => 'Str',
        ip         => IPv4,
    },
    optional => { ssl_key => 'Str', ssl_cert => 'Str', instance   => UUID, },
);

sub _create_host_object {
    my $self                    = shift;
    my $objectmodel             = shift;

    my $params                  = assert_profile(shift || {})->valid;
    my $c                       = shift;

    my $host                    = Zaaksysteem::Object::Types::Host->new(
        owner    => $params->{controlpanel}->get_object_attribute('owner')->value,
        %$params
    );

    ### Every controlpanel gets a default ACL for behandelaar
    my ($behandelaar) = grep ({ $_->system_role && $_->name eq 'Behandelaar' } @{ $c->model('DB::Roles')->get_all_cached($c->stash) });
    $host->permit($behandelaar, qw/read write/);

    if (exists $params->{instance}) {
        $host->set_instance($objectmodel, $params->{instance});
    }

    $objectmodel->save(
        object => $host
    );
}

=head2 update

=head3 URL Path

C</api/v1/controlpanel/[cpuuid]/host/[hostuuid]/update>

=cut

define_profile update => (
    missing_optional_valid => 1,
    optional => {
        label      => 'Str',
        ip         => IPv4,
        ssl_cert   => 'Str',
        ssl_key    => 'Str',
        instance   => UUID,
    },
    require_some => {
        fqdn => [1, qw/fqdn/],
    },
    constraint_methods => {
        fqdn    => sub {
            my ($dfv, $val)     = @_;

            my $fqdn            = $val;
            if ($val =~ /zaaksysteem\.[nlcorgmet]{2,3}$/) {
                $fqdn            = Zaaksysteem::BR::Controlpanel::_verify_host_or_fqdn($dfv, $dfv->{__INPUT_DATA}->{_controlpanel}, $dfv->{__INPUT_DATA}->{_objectmodel}, $val) or return;
            }

            $dfv->get_filtered_data->{fqdn}         = $fqdn;
            return 1;
        },
    },
    msgs    => sub {
        my $dfv     = shift;
        my $rv      = {};

        return $rv unless $dfv->{_custom_messages};

        for my $key (keys %{ $dfv->{_custom_messages} }) {
            $rv->{$key} = $dfv->{_custom_messages}->{$key};
        }

        return $rv;
    }
);

sub update : Chained('instance_base') : PathPart('update') : Args(0) : RW {
    my ($self, $c) = @_;

    my $params  = assert_profile($c->req->params)->valid;

    if (keys %$params) {
        eval {
            $c->model('DB')->txn_do(
                sub {
                    $self->_update_host_object($c->model('Object'), $c->stash->{host}, $params);
                }
            );
        };

        if ($@) {
            throw(
                'api/v1/controlpanel/host/fault',
                'There was a problem updating this controlpanel host object: ' . $@
            );
        }
    }

    $c->stash->{result} = $c->model('DB::ObjectData')->find({ uuid => $c->stash->{host}->uuid });
}

define_profile _update_host_object => (
    missing_optional_valid => 1,
    optional    => {
        fqdn       => FQDN,
        label      => 'Str',
        ip         => IPv4,
        ssl_cert   => 'Str',
        ssl_key    => 'Str',
        instance   => UUID,
    },
);

sub _update_host_object {
    my $self                    = shift;
    my $objectmodel             = shift;
    my $host                    = shift;
    my $params                  = assert_profile(shift || {})->valid;

    my $object                  = $objectmodel->retrieve(uuid => $host->id);

    ###
    ### Manage related instances
    ###
    if (exists $params->{instance}) {
        $object->set_instance($objectmodel, $params->{instance});
        delete($params->{instance});
    }

    $object->$_($params->{$_}) for grep { exists($params->{ $_ }) } keys %$params;

    $objectmodel->save(object => $object);

}


__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
