package Zaaksysteem::Controller::API::v1::Casetype;

use Moose;

use Zaaksysteem::Tools;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

=head1 NAME

Zaaksysteem::Controller::API::v1::Casetype - APIv1 controller for casetype objects

=head1 DESCRIPTION

=head1 ACTIONS

=head2 base

=cut

sub base : Chained('/api/v1/base') : PathPart('casetype') : CaptureArgs(0) : Scope('case') {
    my ($self, $c) = @_;

    my $zql = 'SELECT {} FROM casetype';

    my $set = try {
        return Zaaksysteem::API::v1::Set->new(
            iterator => $c->model('Object')->zql_search($zql)
        )->init_paging($c->request);
    } catch {
        $c->log->warn($_);

        throw(
            'api/v1/casetype',
            'API configuration error, unable to continue'
        );
    };

    $c->stash->{ set } = $set;
    $c->stash->{ casetypes } = $set->build_iterator->rs;
}

=head2 instance_base

=cut

sub instance_base : Chained('base') : PathPart('') : CaptureArgs(1) {
    my ($self, $c, $uuid) = @_;

    $c->stash->{ casetype } = try {
        return $c->stash->{ casetypes }->find($uuid);
    } catch {
        $c->log->debug($_);

        throw('api/v1/casetype/retrieval_fault', sprintf(
            'Casetype retrieval failed, unable to continue.'
        ));
    };

    unless (defined $c->stash->{ casetype }) {
        throw('api/v1/casetype/not_found', sprintf(
            "The casetype object with UUID '%s' could not be found.",
            $uuid
        ), { http_code => 404 });
    }

    unless ($c->stash->{ casetype }->object_class eq 'casetype') {
        throw('api/v1/casetype/not_found', sprintf(
            "The casetype object with UUID '%s' could not be found.",
            $uuid
        ), { http_code => 404 });
    }
}

=head2 list

=head3 URL Path

C</api/v1/casetype>

=cut

sub list : Chained('base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    $c->stash->{ result } = $c->stash->{ set };
}

=head2 get

=head3 URL Path

C</api/v1/casetype/[UUID]>

=cut

sub get : Chained('instance_base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    $c->stash->{ result } = $c->stash->{ casetype };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
