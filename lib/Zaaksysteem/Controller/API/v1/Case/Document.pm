package Zaaksysteem::Controller::API::v1::Case::Document;

use Moose;

use Zaaksysteem::Types qw[UUID];
use Zaaksysteem::Tools;
use Zaaksysteem::Object::Iterator;
use Zaaksysteem::Object::Types::ScheduledJob;
use Zaaksysteem::API::v1::PreparedFileBag;
use Zaaksysteem::API::v1::Set;
use Zaaksysteem::Constants qw[ZAAKSYSTEEM_CONSTANTS];

use Moose::Util::TypeConstraints qw[enum union];

use JSON qw[decode_json];
use DateTime;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

=head1 NAME

Zaaksysteem::Controller::API::v1::Case - APIv1 controller for case objects

=head1 DESCRIPTION

This is the controller API class for C<api/v1/case>. Extensive documentation about this
API can be found in:

L<Zaaksysteem::Manual::API::V1::Case>

Extensive tests about the usage via the JSON API can be found in:

L<TestFor::Catalyst::Controller::API::V1::Case>

=head1 ACTIONS

=head2 document_base

=cut

sub document_base : Chained('/api/v1/case/instance_base') : PathPart('document') : CaptureArgs(1) {
    my ($self, $c, $uuid) = @_;

    unless ($c->stash->{zql} && grep { 'case.documents' eq $_ } $c->stash->{zql}->cmd->probe_fieldnames) {
        throw(
            'api/v1/case/document',
            'Configured ZQL query does not allow you to view metadata of documents'
        );
    }

    my $base_rs = $c->model('DB::File')->search(
        {
            case_id => $c->stash->{ case }->object_id,
            date_deleted => undef,
            destroyed => 0,
            accepted => 1,
            'filestore_id.uuid' => $uuid
        },
        {
            prefetch => 'filestore_id'
        }
    );

    $c->stash->{documents} = $base_rs;

    try {
        $c->stash->{ document } = $base_rs->first;
    } catch {
        $c->log->warn($_);

        throw('api/v1/case/document/retrieval_fault', sprintf(
            'Document retrieval failed, unable to continue.'
        ));
    };

    unless (defined $c->stash->{ document }) {
        throw('api/v1/case/document/not_found', sprintf(
            "The document object with UUID '%s' could not be found.",
            $uuid
        ), { http_code => 404 });
    }
}

=head2 list

=head3 URL

C</api/v1/case/[UUID]/document>

=cut

sub list : Chained('/api/v1/case/instance_base') : PathPart('document') : Args(0) : RO {
    my ($self, $c) = @_;

    unless ($c->stash->{zql} && grep { 'case.documents' eq $_ } $c->stash->{zql}->cmd->probe_fieldnames) {
        throw(
            'api/v1/case/document',
            'Configured ZQL query does not allow you to view metadata of documents'
        );
    }

    my $serializer = Zaaksysteem::API::v1::Serializer->new;

    $c->stash->{ result } = {
        type => 'set',
        instance => {
            rows => [ map { $serializer->read($_) } $c->stash->{case}->documents->all ]
        }
    }
}

=head2 get

=head3 URL

C</api/v1/case/[UUID]/document/[UUID]>

=cut

sub get : Chained('document_base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    $c->stash->{ result } = $c->stash->{ document };
}


=head2 download

=head3 URL

C</api/v1/case/[UUID]/document/[UUID]/download>

=cut

sub download : Chained('document_base') : PathPart('download') : Args(0) : RO {
    my ($self, $c) = @_;

    my $filestore = $c->stash->{ document }->filestore_id;

    # Prevent View::API::v1 from mangling the response.
    $c->stash->{ digest_response } = 1;

    $c->serve_static_file($filestore->get_path);

    $c->res->headers->content_length($filestore->size);
    $c->res->headers->content_type($filestore->mimetype);

    $c->res->header('Cache-Control', 'must-revalidate');
    $c->res->header('Pragma', 'private');

    $c->res->header('Content-Disposition', sprintf(
        'attachment; filename="%s"',
        $c->stash->{ document }->filename)
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
