package Zaaksysteem::Controller::API::Subject;

use Moose;

use Zaaksysteem::Tools;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head1 NAME

Zaaksysteem::Controller::API::Subject - ZAPI Subject controller

=head1 DESCRIPTION

=head1 ACTIONS

=head2 base

This I<base> action provides a common API for all actions contained in this
controller.

It will attempt to capture a single argument, used to retrieve a subject by ID
from the old L<Zaaksysteem::Betrokkene> model.

=cut

sub base : Chained('/api/base') : PathPart('subject') : CaptureArgs(1) {
    my ($self, $c, $subject_id) = @_;

    my $subject = $c->model('Betrokkene')->get({ }, $subject_id);

    unless (defined $subject) {
        die('api/subject/find_subject');

        throw('api/subject/find_subject', sprintf(
            'Unable to find subject with ID "%s"',
            $subject
        ));
    }

    $c->stash->{ subject } = $subject;
}

=head2 get

=head3 URL

C</api/subject/[SUBJECT_ID]>

=cut

sub get : Chained('base') : PathPart('') : Args(0) : ZAPI {
    my ($self, $c) = @_;

    $c->stash->{ zapi } = [ $c->stash->{ subject } ];
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
