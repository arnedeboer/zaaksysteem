package Zaaksysteem::Controller::API::Mail;

use Moose;
use namespace::autoclean;

use File::Basename;
use File::Temp;
use Mail::Track;

BEGIN { extends 'Zaaksysteem::Controller' }

=head2 intake

E-mail intake, accepts the MIME message as plain text and processes it depending on the interfaces which are configured.

=cut

sub intake : Chained('/api/base') : PathPart('mail') {
    my ($self, $c) = @_;

    my $params = $c->req->params;
    die('No message found') unless $params->{message};
    my $mailfile = _save_message_as_file($c->model('DB::File'), $params->{message});

    my $outgoing = $c->model('DB::Interface')->search_active({module => 'emailconfiguration'})->first;

    if ($outgoing) {
        $c->stash->{interface} = $outgoing;
        my $config = $outgoing->get_interface_config;
        my $regexp = qr/(\d+-[a-z0-9]{6})/;

        my $mt = Mail::Track->new(
            subject_prefix_name  => $config->{subject},
            identifier_regex     => qr/$regexp/,
        );

        my $msg = $mt->parse($params->{message});

        if ($msg->identifier) {
            my $ok = $c->stash->{interface}->process_trigger('process_mail', { message => $mailfile });
            if ($ok) {
                $c->res->body('ok');
            }
            else {
                $c->res->body('Failure reading your mail');
            }
            return;
        }
    }

    my $interfaces = $c->model('DB::Interface')->search_active({module => 'emailintake'});
    if ($interfaces->count == 0) {
        # Be backward compatible, future e-mail intakes will have specific
        # interfaces based on the username so we can get rid of the
        # eval { rci/yucay } construction below
        $mailfile->delete();
        $mailfile->filestore->delete();
        $c->detach('intake_process');
    }

    my $mt = Mail::Track->new();
    my $msg = $mt->parse($params->{message});
    my ($to) = Email::Address->parse($msg->to);
    $to = lc($to->address);

    my $interface;
    while ($interface = $interfaces->next) {
        my $config = $interface->get_interface_config;
        if (lc($config->{api_user}) eq $to) {
            $c->stash->{interface} = $interface;
            my $ok = $c->stash->{interface}->process_trigger('process_mail', { message => $mailfile });
            if ($ok) {
                $c->res->body('ok');
            }
            else {
                $c->res->body('Failure reading your mail');
            }
            return;
        }
    }

    die "Unable to determine which e-mail interface you want to use, please contact support and/or create a new e-mail intake interface";

}

=head2 intake_process

Old skool e-mail intake process. Not based on any interface. Just grabs the attachments and adds them to the document queue.

=cut

sub intake_process : Local {
    my ($self, $c) = @_;

    die('No message found') unless $c->req->params->{message};
    eval {
        $c->forward('/api/mail/intake/handle');
    };

    if ($@) {
        $c->log->error('Mail error: ' . $@);
        $c->res->body($@);
    }
    else {
        $c->res->body('ok');
    }
}

sub input : Local {
    my ($self, $c) = @_;

    die('No message found') unless $c->req->params->{message};

    $c->log->debug(
        'API::Mail->input: MAIL handler'
    );

    eval {
        $c->forward('/api/mail/rci/handle');
        $c->forward('/api/mail/yucat/handle');
    };

    if ($@) {
        $c->log->error('Mail error: ' . $@);
    }

    $c->res->body('ok');
}

# This should not be in the controller.
sub _retrieve_files {
    my ($self, $message) = @_;

    my $parser = new MIME::Parser;

    $parser->extract_uuencode(1);

    my $entity = $parser->parse_data($message);

    return ($parser, $entity->parts_DFS);
}

sub _handle_part {
    my ($self, $part) = @_;

    my $raw_filename = $part->head->recommended_filename;
    if (!$raw_filename) {
        $raw_filename = $part->bodyhandle->path;
    }

    my ($filename, $dir, $ext) = fileparse($raw_filename, '\.[^.]*');

    return ($filename, $part->bodyhandle, $ext, $dir);
}

sub _save_message_as_file {
    my $rs  = shift;
    my $msg = shift;
    my $fh = File::Temp->new(SUFFIX => '.mime');
    my $path = $fh->filename;
    print $fh $msg;
    close($fh);
    my ($filename, undef, $ext) = fileparse($path, '\.[^.]*');

    my $mailfile = $rs->file_create({
        name       => $filename . $ext,
        file_path  => $path,
        ignore_extension => 1,
        db_params  => {
            created_by => "Zaaksysteem",
            queue      => 0,
        },
    });
    unlink($path);
    return $mailfile->id;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 input

TODO: Fix the POD

=cut

