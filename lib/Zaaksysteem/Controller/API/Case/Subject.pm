package Zaaksysteem::Controller::API::Case::Subject;

use Moose;

use Zaaksysteem::Tools;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head1 NAME

=head1 SYNOPSIS

=head1 ACTIONS

=head2 base

Base action for subject actions in this controller. It sets up a C<subjects>
key in the L<stash|Catalyst/c-stash>, which is a
L<Zaaksysteem::Schema::ZaakBetrokkenen> resultset, joined on the case, and
sorted by the L<name|Zaaksysteem::Schema::ZaakBetrokkenen/naam> field,
excluding deleted subjects.

=cut

sub base : Chained('/api/case/base') : PathPart('subjects') : CaptureArgs(0) {
    my ($self, $c) = @_;

    $c->stash->{ subjects } = $c->stash->{ zaak }->zaak_betrokkenen->search({
        deleted => undef
    });
}

=head2 base_rw

TODO: Fix me

=cut

sub base_rw : Chained('base') : PathPart('') : CaptureArgs(0) {
    my ($self, $c) = @_;

    unless($c->check_any_zaak_permission(qw[zaak_edit zaak_beheer])) {
        throw('api/case/subject/access_violation', sprintf(
            'Unable to chain into non-idempotent action, user has insufficient permissions.'
        ));
    }
}

=head2 instance_base

Base action for singleton subject actions in this controller. It sets up a
C<subject> key in de L<stash|Catalyst/c-stash>, which is a
L<Zaaksysteem::Schema::ZaakBetrokkenen> row.

=cut

sub instance_base : Chained('base') : PathPart('') : CaptureArgs(1) {
    my ($self, $c, $id) = @_;

    my $subject = $c->stash->{ subjects }->find($id);

    unless ($subject) {
        throw('api/case/subject/not_found', sprintf(
            'Provided id (%s) did not resolve to a subject',
            $id
        ));
    }

    $c->stash->{ subject } = $subject;
}

=head2 instance_base_rw

Base action that chains L</instance_base>, adding a authorization assertion so
chained actions don't need to check themselves. The security checks are based
on the old L<Zaaksysteem::General::Authentication/check_any_zaak_permission>
infrastructure.

=cut

sub instance_base_rw : Chained('instance_base') : PathPart('') : CaptureArgs(0) {
    my ($self, $c) = @_;

    unless($c->check_any_zaak_permission(qw[zaak_edit zaak_beheer])) {
        throw('api/case/subject/access_violation', sprintf(
            'Unable to chain into non-idempotent action, user has insufficient permissions.'
        ));
    }
}

=head2 list

This action hydrates a complete listing of all subjects related to the case.

=head3 URL

C</api/case/[CASE_ID]/subjects>

=head3 Response

Response will be a default ZAPI wrapper containing 0 or more subjectrelations.

For an example of a single subjectrelation hydration see L</get>.

=cut

sub list : Chained('base') : PathPart('') : Args(0) : ZAPI {
    my ($self, $c) = @_;

    $c->stash->{ zapi } = [
        $c->stash->{ subjects }->all
    ];
}

=head2 get

This action hydrates a single relation between the case and a subject.

=head3 URL

C</api/case/[CASE_ID]/subjects/[SUBJECT_ID]>

=head3 Response

The generated response depends on
L<Zaaksysteem::DB::Component::ZaakBetrokkenen/TO_JSON>, but at the time of
writing looks like the following example.

    {
        "next" : null,
        "status_code" : "200",
        "prev" : null,
        "num_rows" : 1,
        "rows" : 1,
        "comment" : null,
        "at" : null,
        "result" : [
            {
                "pip_authorized" : false,
                "name" : "admin",
                "id" : 161,
                "betrokkene_identifier" : "betrokkene-medewerker-4",
                "magic_string_prefix" : "derping",
                "role" : "derping",
                "betrokkene_id" : 4,
                "betrokkene_type" : "medewerker"
            }
        ]
    }

=cut

sub get : Chained('instance_base') : PathPart('') : Args(0) : ZAPI {
    my ($self, $c) = @_;

    $c->stash->{ zapi } = $c->stash->{ subject };
}

=head2 update

This action updates existing relations between the case and a subject.

=head3 URL

C</api/case/[CASE_ID]/subjects/[SUBJECT_ID]/update>

=head3 Response

B<Redirect> (302) to L</get>.

=head3 Parameters

=over 4

=item role

This I<string> field is a textual descriptive caption of the relation. It is
inert, and only used for the user accessing subject relationships'
convenience.

=item betrokkene_identifier

This I<string> field must contain a valid 'betrokkene identifier' (e.g.
C<betrokkene-natuurlijk_persoon-4>).

=item magic_string_prefix

This I<string> field defines the magic string prefix for the relation.
Templates that are processed in context of a case can use this prefix in
combination with the usual magic strings that resolve subject personalia to
render information about the subject.

=item pip_authorized

This I<boolean> flag indicates whether the subject is authorized to update
the associated case object. This field is only relevant for subjects who are
not also the current requestor of the case.

=back

=cut

define_profile update => (
    optional => {
        role => 'Str',
        betrokkene_identifier => 'Str',
        magic_string_prefix => 'Str',
        pip_authorized => 'Bool'
    }
);

sub update : Chained('instance_base_rw') : PathPart('update') : Args(0) : ZAPI {
    my ($self, $c) = @_;

    my $subject = $c->stash->{ subject };
    my $params = assert_profile($c->req->params)->valid;

    my %updaters = (
        role => sub {
            $subject->rol(shift)
        },

        magic_string_prefix => sub {
            $subject->magic_string_prefix(lc(shift))
        },

        pip_authorized => sub {
            $subject->pip_authorized($_[0] ? 1 : 0)
        },

        betrokkene_identifier => sub {
            $subject->betrokkene_identifier(shift);
        }
    );

    for my $key (keys %{ $params }) {
        $updaters{ $key }->($params->{ $key });
    }

    $subject->update;
    $c->stash->{zaak}->update;

    $c->res->redirect($c->uri_for($self->action_for('get'), [
        $c->stash->{ zaak }->id,
        $subject->id
    ]));

    $c->detach;
}

=head2 delete

This call permanently deletes a subject<->case relation. If will fail with a
C<db> exception response if a 'core' relation removal is attempted (current
case owner, initiator, etc)

=head3 URL

C</api/case/[CASE_ID]/subjects/[SUBJECT_ID]/delete>

=head3 Response

B<Redirect> (302) to L</list>.

=cut

sub delete : Chained('instance_base_rw') : PathPart('delete') : Args(0) : ZAPI {
    my ($self, $c) = @_;

    $c->stash->{ subject }->delete;
    $c->stash->{zaak}->update;

    $c->res->redirect($c->uri_for($self->action_for('list'), [
        $c->stash->{ zaak }->id
    ]));

    $c->detach;
}

=head2 create

This actions allows for the creation of subject-case relations via a simple
(JSON) POST. This operation may fail without clear warnings if the API
consumer attempts to create a relation with a C<role> or C<magic_string_prefix>
that already exists in the database for this case.

An additional gotcha is that because deletions of existing relations are
soft-delete only, a deleted relation with those fieldvalues may exist, and be
the reason for a failing creation POST.

=head3 URL

C</api/case/[CASE_ID]/subjects/create>

=head3 Response

B<Redirect> (302) to L</get>.

=head3 Parameters

See L</update>.

=cut

sub create : Chained('base_rw') : PathPart('create') : Args(0) : ZAPI {
    my ($self, $c) = @_;

    my $params = $c->req->params;

    # Keep our external API sane, but the internal API still expects localized
    # keys...
    $params->{ rol } = delete $params->{ role };

    my $event = $c->stash->{ zaak }->betrokkene_relateren($params);

    unless (defined $event) {
        throw('api/case/subject/create', sprintf(
            'Could not create subject-case relation. No idea why.'
        ));
    }

    if ($params->{ notify_subject } && $params->{ pip_authorized }) {
        my $template_id = $c->model('DB::Config')->get('subject_pip_authorization_confirmation_template_id');

        unless($template_id) {
            $c->push_flash_message({ type => 'error', message => sprintf(
                'Kon geen e-mail notificatie template vinden, geen notificatie verstuurd'
            )});

            return;
        }

        my $template = $c->model('DB::BibliotheekNotificaties')->find($template_id);

        unless($template) {
            $c->push_flash_message({ type => 'error', message => sprintf(
                'Notificatie template kon niet gevonden worden, geen e-mail notificatie verstuurd'
            )});

            return;
        }

        my $subject = $c->model('Betrokkene')->get_by_string($params->{ betrokkene_identifier });

        unless(defined $subject) {
            throw('case/add_subject', sprintf(
                'Subject with identifier "%s" could not be found, this can\'t be right.',
                $params->{ betrokkene_identifier }
            ));
        }

        unless($subject->email) {
            $c->push_flash_message({ type => 'error', message => sprintf(
                'Geen e-mail adres gevonden voor "%s", geen notificatie verstuurd',
                $params->{ betrokkene_identifier }
            )});

            return;
        }

        $c->stash->{ zaak }->mailer->send_case_notification({
            notification => $template,
            recipient => $subject->email
        });
    }

    $c->stash->{zaak}->update;

    $c->res->redirect($c->uri_for($self->action_for('get'), [
        $c->stash->{ zaak }->id,
        $event->data->{ case_subject_id }
    ]));

    $c->detach;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
