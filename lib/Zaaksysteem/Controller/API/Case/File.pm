package Zaaksysteem::Controller::API::Case::File;

use Moose;

use Archive::Tar::Stream;
use Archive::Zip;
use DateTime;

use Zaaksysteem::Tools;
use Zaaksysteem::Tie::CallingHandle;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

sub index : Chained('/api/case/base') : PathPart('file') : CaptureArgs(1) {
    my ($self, $c, $file_id) = @_;

    $c->stash->{file} = $c->stash->{zaak}->files->search({
        id => $file_id
    })->active->first or throw('api/case/file', 'file $file_id not available');
}


=head2 search

Display active files for a case. Used to supply the document tab.

=cut

sub search : Chained('/api/case/base') : PathPart('file/search') : ZAPI {
    my ($self, $c) = @_;

    $c->stash->{zapi_no_pager} = 1;
    $c->stash->{zapi} = $c->stash->{zaak}->active_files;
}

=head2 download

Download files related to the case via zip or tar archive. If no files are mentioned all files for the case are downloaded. If no format is given, zip is assumed.

=head3 SYNOPSIS

    GET /api/case/4/file/download/?file=1&file=2&format=tar HTTP/1.1

=cut

sub download : Chained('/api/case/base') : PathPart('file/download') : GET {
    my ($self, $c) = @_;

    my $format = $c->req->param('format') || 'zip';
    my @files  = $c->req->param('file');
    my $rs = $c->stash->{zaak}->search_active_files(\@files);
    if ($rs->count == 0) {
        $c->res->body('No files found!');
        return;
    }

    my $filename = sprintf("zs-%s-%s.%s",
        $c->stash->{zaak}->id,
        DateTime->now->iso8601,
        $format
    );

    $c->res->header('Content-Disposition',
        qq[attachment; filename="$filename"]);

    my $handle = Zaaksysteem::Tie::CallingHandle->create(
        write_cb => sub { $c->res->write($_[0]); },);

    if ($format eq 'tar') {
        $c->res->content_type('application/x-tar');
        my $tar = Archive::Tar::Stream->new(outfh => $handle);

        while (my $file = $rs->next) {
            my $fh = $file->filestore->read_handle;
            my $rc = $tar->AddFile($file->filepath, -s $fh, $fh, mode => oct('0644'));
        }
        $tar->FinishTar();
    }
    else {
        $c->res->content_type('application/zip');

        my $zip = Archive::Zip->new();
        while (my $file = $rs->next) {
            $zip->addFile($file->filestore->get_path, $file->filepath);
        }
        $zip->writeToFileHandle($handle, 0);
    }

    # Streaming downloads don't mix well with views.
    $c->res->body('');
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 index

TODO: Fix the POD

=cut

