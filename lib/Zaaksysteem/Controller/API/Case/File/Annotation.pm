package Zaaksysteem::Controller::API::Case::File::Annotation;

use Moose;

use Zaaksysteem::Tools;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

sub index : Chained('/api/case/file/index') : PathPart('annotation') : CaptureArgs(0) {} # glue


sub save : Chained('index') : PathPart('save') : ZAPI {
    my ($self, $c) = @_;

    $c->forward('/api/file/annotation/do_save');
}


sub delete : Chained('index') : PathPart('delete') : ZAPI {
    my ($self, $c) = @_;

    $c->forward('/api/file/annotation/do_delete');
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 delete

TODO: Fix the POD

=cut

=head2 index

TODO: Fix the POD

=cut

=head2 save

TODO: Fix the POD

=cut

