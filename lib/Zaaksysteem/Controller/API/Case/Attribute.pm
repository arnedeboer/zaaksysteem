package Zaaksysteem::Controller::API::Case::Attribute;

use Moose;

use Zaaksysteem::Tools;
use Zaaksysteem::Constants qw/ZAAK_CONFIDENTIALITY/;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head1 NAME

Zaaksysteem::Controller::API::Case::Attribute - Manipulation of case attributes (custom fields)

=head1 CONSTANTS

Defines a dispatch table of attribute name <-> action subs. If an attribute
keyed in this constant hashref is changed, the attached subref is executed.

=head2 SYSTEM_ATTRIBUTE_ACTION_MAP

=over 4

=item case.result

Alter resultaat of this case

=item case.confidentiality

Set the confidentiality of this case

=back

=cut

use constant SYSTEM_ATTRIBUTE_ACTION_MAP => {
    'case.result'   => sub {
        my ($c, $case, $value) = @_;

        throw (
            'api/case/attribute/save/case.result/access_denied',
            'Cannot save confidentiality, insufficient permissions'
        ) unless ($c->check_any_zaak_permission('zaak_beheer') || $c->can_change);
    },
    'case.confidentiality'    => sub {
        my ($c, $case, $value) = @_;

        throw (
            'api/case/attribute/save/case.confidentiality/access_denied',
            'Cannot save confidentiality, insufficient permissions'
        ) unless $c->check_any_zaak_permission(qw[zaak_beheer zaak_edit]);

        throw (
            'api/case/attribute/save/case.confidentiality',
            'Cannot save confidentiality, unknown value given'
        ) unless ZAAK_CONFIDENTIALITY->($value);
    }
};

=head1 ACTIONS

Contains the API calls for manipulating case attributes.

=head2 base

This base action only reserves the base path for this controller, which is
C</api/case/[ZAAK_NR]/attribute/>.

=cut

sub base : Chained('/api/case/base') : PathPart('attribute') : CaptureArgs(0) { }

=head2 save

Will update a user attribute, calls the rule engine to fix everything, and returns
the output of api/case/ID

=head3 URL

C</api/case/[ZAAK_NR]/attribute/save>

=head3 Parameters

A JSON request body is expected, with the following keys

    {
        fields => {
            'attribute.kenteken' => '44-AB-44',
            'case.result'        => 'Bewaren'
        },

        phases => [ "50" ]
    }

=over 4

=item fields

C<fields> is expected to be a map of attribute names to values which will be set.

=item phases

C<phases> is expected to be an array of phases... that do what exactly?

=back

=head3 Response

See L<Zaaksysteem::Controller::API::Case/get>.

=cut

sub save : Chained('base') : PathPart('save') : Args(0) : ZAPI {
    my ($self, $c)              = @_;

    my $fields                  = $c->req->params->{fields};
    my $phases                  = $c->req->params->{phases};

    throw(
        'api/case/attribute/save/no_post',
        'Method can only be called via a post call'
    ) unless lc($c->req->method) eq 'post';

    throw(
        'api/case/attribute/save/no_permission',
        'Need edit or beheer permissions to edit this case'
    ) unless $c->check_any_zaak_permission(qw[zaak_beheer zaak_edit]);

    throw(
        'api/case/attribute/save/missing_param_fields',
        'Missing parameter: fields'
    ) unless $fields;

    throw(
        'api/case/attribute/save/invalid_param_fields',
        'Invalid parameter: fields, which needs to be a HASH'
    ) unless ref $fields eq 'HASH';

    my $result = $fields->{ 'case.result' };

    # Only do can_change if there are attributes to update (almost always)
    if (scalar keys %{ $fields }) {
        # True-ish value indicates can_change not to set a flash message in
        # case of failure. Yeah this is a bad design, but I don't want to break
        # the existing callers.
        unless ($c->can_change(1)) {
            throw('api/case/attribute/save/authorization_failure', sprintf(
                'Update attempt blocked, case might be closed, user may have insufficient authorizations'
            ))
        }

        $self->_update_attributes($c, $fields, $phases);
    }

    if ($result) {
        SYSTEM_ATTRIBUTE_ACTION_MAP->{ 'case.result' }->($c, $c->stash->{ zaak }, $result);
    }

    $self->_execute_rules($c, $phases);
    # Cleanup for the re-dispatch to case/get.
    $c->forward('/touch_delayed_cases');
    $c->go('/api/case/get', [ $c->stash->{ zaak }->id ], []);
}

sub _update_attributes {
    my ($self, $c, $fields, $phases)      = @_;

    my @magic_strings = map { m[^attribute\.(.*)$] } keys %{ $fields };

    my @zaaktype_kenmerken = $c->stash->{zaak}->zaaktype_node_id->zaaktype_kenmerken->search(
        {
            'bibliotheek_kenmerken_id.magic_string' => { -in => [ @magic_strings ] }
        },
        {
            prefetch => 'bibliotheek_kenmerken_id'
        }
    )->all;

    my %attributes;
    my %magic_string_map;
    my %zaaktype_kenmerk_map;

    for my $ztk (@zaaktype_kenmerken) {
        my $bibliotheek_kenmerk = $ztk->bibliotheek_kenmerken_id;

        $magic_string_map{ $bibliotheek_kenmerk->magic_string } = $bibliotheek_kenmerk->id;
        $zaaktype_kenmerk_map{ $bibliotheek_kenmerk->id } = $ztk;
    }

    foreach my $kenmerk_name (keys %{ $fields }) {
        my $value = $fields->{ $kenmerk_name };

        # If we have a specific handler for the requested update, and it's
        # *not* case.result, execute said handler.
        if (SYSTEM_ATTRIBUTE_ACTION_MAP->{$kenmerk_name}) {
            SYSTEM_ATTRIBUTE_ACTION_MAP->{$kenmerk_name}->($c, $c->stash->{zaak}, $value);
        }

        unless (ref $value eq 'ARRAY') {
            $value = [ $value ];
        }

        my $bib_id;

        if($kenmerk_name =~ m|^kenmerk_id_\d+$|) {
            ($bib_id) = $kenmerk_name =~ m|^kenmerk_id_(\d+)|;
        } elsif (my ($magic_string) = $kenmerk_name =~ /^attribute\.(.*)$/) {
            $bib_id = $magic_string_map{ $magic_string };
        } elsif ($kenmerk_name =~ /^case\./) {
            $bib_id = $kenmerk_name;
        }

        next unless defined $bib_id;

        $attributes{ $bib_id } = $value;

        my $ztk = $zaaktype_kenmerk_map{ $bib_id };

        next unless defined $ztk;

        my $bibliotheek_kenmerk = $ztk->bibliotheek_kenmerken_id;

        next unless defined $bibliotheek_kenmerk;

        # In case of geolatlon (Locatie met kaart), we want to check if the
        # ZTB config instructs us to use new values as a case location. If so,
        # push a new task to the back
        if ($bibliotheek_kenmerk->value_type eq 'geolatlon') {
            if ($ztk->properties->{ map_case_location }) {
                my ($lat, $lon) = split m[,], $value->[0];

                my $queue_items = $c->model('DB')->schema->default_resultset_attributes->{ queue_items };

                push @{ $queue_items }, $c->stash->{ case }->queues->create_item(
                    'update_case_location',
                    {
                        object_id => $c->stash->{ case }->id,
                        label => 'Zaaklocatie instellen',
                        data => {
                            latitude => $lat,
                            longitude => $lon
                        }
                    }
                );
            }
        }
    }

    if (keys %attributes) {
        $c->stash->{zaak}->zaak_kenmerken->update_fields(
            {
                new_values => \%attributes,
                zaak       => $c->stash->{zaak},
            }
        );
    }
}

sub _execute_rules {
    my ($self, $c, $phases) = @_;

}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
