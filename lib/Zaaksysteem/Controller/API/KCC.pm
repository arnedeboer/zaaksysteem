package Zaaksysteem::Controller::API::KCC;

use Moose;
use namespace::autoclean;

use Zaaksysteem::Tools;

use Date::Parse qw(str2time);

use JSON;
use XML::XPath;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head1 NAME

Zaaksysteem::Controller::KCC::Call - KlantContactCenter API

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=head2 status

Check whether the KCC interface exists and is enabled.

=head3 URL

B</api/kcc/status>

=head3 Parameters

None.

=head3 Returns

A ZAPI data structure, with one entry with one key:

=over

=item * kcc

True if the KCC interface exists and is enabled, false if it doesn't exist or
isn't enabled.

=back

=cut

sub status : Chained('/') : PathPart('api/kcc/service/status') : GET : ZAPI {
    my ($self, $c) = @_;

    my $kcc = $self->_get_kcc_interface($c);
    $self->stash->{zapi} = [ kcc => $kcc ? 1 : 0 ];

    return;
}

=head2 base

Do some setup for the JSON API functions.

=cut

sub base : Chained('/') : PathPart('api/kcc') : CaptureArgs(0) {
    my ($self, $c) = @_;

    my $kcc = $self->_get_kcc_interface($c);
    if(!$kcc) {
        throw(
            'zaaksysteem/configuration',
            'No PBX settings found. Check your Zaaksysteem configuration.'
        );
    }

    $c->stash->{interface} = $kcc;
}

=head2 register_call

This easiest way of registering an incoming phone call.

Hit this URL with a POST request and a new KCC call transaction will be inserted.

=head3 URL

B</api/kcc/call/register>

=head3 Parameters

The parameters can be supplied as POST parameters.

=over

=item phonenumber

This parameter must contain a 10-digit telephonenumber (eg. 0201234567)
without any breaks or punctuation.

=item extension

This parameter identifies the device which receives the call, there are no hard
restrictions on the content of the field, but it preferably uniquely identifies
the device in a way that is linkable to a KCC employee.

=back

=head3 Example response body

The response to a succesful call registration is a JSON data structure
containing the id the call was registered under in Zaaksysteem.

This information can safely be discarded.

=head3 Required configuration

The PBX that will be executing the POST to this controller must be exempted
from the application's firewall. This can be done in the 'Koppelingen Configuratie'
interface in the Zaaksysteem interface.

=head4 Setting the exemption

=over

=item 1. Login on Zaaksysteem as an administrator

=item 2. Navigate to 'Koppelingen' in the 'Beheer' menu

=item 3. Navigate to the 'Configuratiescherm', found under the 'Gear' icon

=item 4. Set the C<pbx_ip> entry to the IPv4 address of the PBX

=back

=cut

define_profile register_call => (
    required => [qw[phonenumber extension]],
    constraint_methods => {
        phonenumber => qr[^\+?\d{10,}$]
    }
);

sub register_call : Chained('base') : PathPart('call/register') : POST : Args(0) : ZAPI {
    my ($self, $c) = @_;
    my $opts = assert_profile($c->req->params)->valid;

    # Morph "IPv4-mapped IPv6 address" into just the IPv4 address
    my ($client_ip) = $c->req->address =~ qr[((\d{1,3}\.?){4,})];

    my $transaction = $c->stash->{interface}->process_trigger(
        'incoming_call',
        {
            client_ip   => $client_ip,
            phonenumber => $opts->{'phonenumber'},
            extension   => $opts->{'extension'},
        },
    );

    # Return something useful.
    $c->stash->{zapi} = [ $transaction ];
    return;
}

=head2 register_call_xml

XML-based way for a PBX to register a call in Zaaksysteem using a POST
("VoiceWorks-style").

Since VoiceWorks sends us a non-standard HTTP POST, we do some manual fixing of
the incoming POST body.

=cut

sub register_call_xml : Chained('base') : PathPart('call/register_xml') : Args(0) {
    my ($self, $c) = @_;

    my $body = "";

    # Fix call made by VoiceWorks: it sets an incorrect Content-Type, which
    # causes problems when parsing the request.
    for my $param (keys %{$c->req->params}) {
        $body .= sprintf("%s=%s", $param, $c->req->param($param));
    }

    $c->res->content_type('application/json');
    $c->res->body(to_json({ success => 1 }));

    my $xpath = XML::XPath->new(xml => $body);

    my $request_time = DateTime->now(time_zone => 'Europe/Amsterdam');
    my $pbx_time = DateTime->from_epoch(
        epoch => str2time($xpath->find('/message/messageheader/msgdatetime')->string_value)
    );

    $c->log->debug(sprintf(
        'Difference between PBX and request time: %d seconds',
        $pbx_time->subtract_datetime_absolute($request_time)->in_units('seconds')
    ));

    # TODO figure out timezone difference
    # and detach if difference between requestime and pbx time is too great

    unless($xpath->find('/message/messagebody/clip')->string_value =~ /\d{10}/) {
        $c->detach;

        return;
    }

    # Morph "IPv4-mapped IPv6 address" into just the IPv4 address
    my ($client_ip) = $c->req->address =~ qr[((\d{1,3}\.?){4,})];

    $c->stash->{interface}->process_trigger(
        'incoming_call',
        {
            client_ip   => $client_ip,
            phonenumber => $xpath->find('/message/messagebody/clip')->string_value,
            extension   => $xpath->find('/message/messagebody/extension')->string_value,
        },
    );
}

=head2 accept

This method allows a client to accept an incoming call.

It should (and can) only be called once. Since this call modifies the state of
Zaaksysteem, it can only be done with the POST request method.

=head3 URL

B</api/kcc/call/accept>

=head3 Parameters

=over

=item call_id

The ID of the transaction that represents the call to be accepted/rejected.

=back

=head3 Response

If the request validates, the response will be an HTTP 200, the body will be a
ZAPI response, containing.

=cut

define_profile accept => (
    required => [qw[call_id]],
    constraint_methods => {
        call_id => qr[^\d+$]
    }
);

sub accept : Chained('base') : PathPart('call/accept') : Args(0) : POST : ZAPI {
    my ($self, $c) = @_;
    my $opts = assert_profile($c->req->params)->valid;

    my $rv = $c->stash->{interface}->process_trigger(
        'mark_call',
        {
            transaction_id => $opts->{call_id},
            mark           => 'accepted',
        },
    );

    if (my ($betrokkene) = @{ $rv->betrokkene() }) {
        $c->betrokkene_session_enable($betrokkene);
        $c->stash->{zapi} = [$betrokkene->as_hashref];
    }
    else {
        $c->stash->{zapi} = [];
    }

    return;
}

=head2 reject

This API method allows a client to reject an incoming call.

It should (and can) only be called once. Since this call modifies the state of
Zaaksysteem, it can only be done with the POST request method.

=head3 URL

B</api/kcc/call/reject>

=head3 Parameters

=over

=item call_id

The ID of the event that represents the call to be rejected/rejected.

=back

=head3 Response

If the request validates, the response will be an HTTP 200, the body will be a
ZAPI response, containing.

=cut

define_profile reject => (
    required => [qw[call_id]],
    constraint_methods => {
        call_id => qr[^\d+$]
    }
);

sub reject : Chained('base') : PathPart('call/reject') : Args(0) : POST : ZAPI {
    my ($self, $c) = @_;
    my $opts = assert_profile($c->req->params)->valid;

    $c->stash->{zapi} = [
        $c->stash->{interface}->process_trigger(
            'mark_call',
            {
                transaction_id => $opts->{call_id},
                mark           => 'rejected',
            },
        ),
    ];

    return;
}

=head2 list

Returns a list of active calls for the current user.

=head3 URL

B</api/kcc/call/list>

=head3 Parameters

None

=head3 Returns

A ZAPI data structure, containing the result of the L<list_active_calls>
trigger in the KCC interface.

=cut

sub list : Chained('base') : PathPart('call/list') : GET : ZAPI {
    my ($self, $c) = @_;

    my $extension = $c->user->settings->{kcc}{extension};

    $c->stash->{zapi} = $c->stash->{interface}->process_trigger(
        'list_active_calls',
        { extension => $extension },
    );
    return;
}

=head2 extension

Set the extension of the current user to the value specified in the "extension"
parameter.

=head3 URL

B</api/kcc/user/extension>

=head3 Parameters

=over

=item * extension

The (new) extension of this user.

=back

=cut

define_profile extension => (
    required => [qw[extension]],
);

sub extension : Chained('base') : PathPart('user/extension') : POST : ZAPI {
    my ($self, $c) = @_;
    my $opts = assert_profile($c->req->params)->valid;

    my $usersettings = $c->user->settings;
    $usersettings->{kcc}{extension} = $opts->{extension};

    $c->user->settings($usersettings);
    $c->user->update();

    $c->stash->{zapi} = [ $usersettings->{kcc} ];
    return;
}

=head2 user_status

Return whether the user is available for calls.

=head3 URL

B</api/kcc/user/status>

=head3 Parameters

None

=head3 Returns

A ZAPI data structure, containing one row, with one key:

=over

=item * user_status

True if the user is available for calls, false otherwise.

=back

=cut

sub user_status : Chained('base') : PathPart('user/status') : GET : ZAPI {
    my ($self, $c) = @_;
    my $usersettings = $c->user->settings;

    $c->stash->{zapi} = [
        { user_status => $usersettings->{kcc}{user_status} ? 1 : 0 }
    ];
    return;
}

=head2 enable

Make the current user "available for calls".

=head3 URL

B</api/kcc/user/enable>

=head3 Parameters

None

=head3 Returns

The same as C</api/kcc/user/status>

=cut

sub enable : Chained('base') : PathPart('user/enable') : POST : ZAPI {
    my ($self, $c) = @_;

    my $usersettings = $c->user->settings;
    $usersettings->{kcc}{user_status} = 1;

    $c->user->settings($usersettings);
    $c->user->update();

    $self->user_status($c);
    return;
}

=head2 disable

Make the current user "not available for calls".

=head3 URL

B</api/kcc/user/disable>

=head3 Parameters

None.

=head3 Returns

The same as C</api/kcc/user_status>

=cut

sub disable : Chained('base') : PathPart('user/disable') : POST : ZAPI {
    my ($self, $c) = @_;

    my $usersettings = $c->user->settings;
    $usersettings->{kcc}{user_status} = 0;

    $c->user->settings($usersettings);
    $c->user->update();

    $self->user_status($c);
    return;
}

=head2 prepare_page

Special "hook" method called by L<Zaaksysteem::Controller::Page::begin> to
initialize page specific data.

=cut

sub prepare_page {
    my ($self, $c) = @_;

    if (!$c->user_exists) {
        # Don't break the page if nobody is logged in.
        return;
    }

    my $kcc = $self->_get_kcc_interface($c);
    my $rv = {
        kcc_service_available => JSON::false,
    };

    if ($kcc) {
        my $usersettings = $c->user->settings;
        $rv = {
            kcc_service_available => JSON::true,
            kcc_user_available =>
                $usersettings->{kcc}{user_status}
                    ? JSON::true
                    : JSON::false,
        };
        $c->stash->{kcc_extension} = encode_json(
            { value => $usersettings->{kcc}{extension} }
        );
    }

    $c->stash->{kcc_metadata} = encode_json($rv);

    return;
}

sub _get_kcc_interface {
    my ($self, $c) = @_;

    my $kcc = $c->model('DB::Interface')->search_active({module => 'kcc'})->first;
    unless ($kcc) {
        return;
    }
    return $kcc;
}

__PACKAGE__->meta->make_immutable;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

