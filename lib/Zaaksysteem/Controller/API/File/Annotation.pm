package Zaaksysteem::Controller::API::File::Annotation;

use Moose;

use Zaaksysteem::Tools;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

sub save : Chained('/api/file/base') : PathPart('annotation/save') : ZAPI {
    my ($self, $c) = @_;

    $c->forward('do_save');
}


sub delete : Chained('/api/file/base') : PathPart('annotation/delete') : ZAPI {
    my ($self, $c) = @_;

    $c->forward('do_delete');
}


sub do_save : Private {
    my ($self, $c) = @_;

    my $params = $c->req->params;

    my $annotation = $c->model('DB::FileAnnotation')->update_or_create({
        id => $params->{id},
        file_id => $c->stash->{file}->id,
        subject => $self->subject($c),
        properties => $params
    });

    $c->stash->{zapi} = [$annotation->properties];
}


sub do_delete : Private {
    my ($self, $c) = @_;

    my $params = $c->req->params;

    my $annotation = $c->model('DB::FileAnnotation')->search({
        id => $params->{id},
        file_id => $c->stash->{file}->id,
        subject => $self->subject($c)
    })->delete;

    $c->stash->{zapi} = [{
        message => 'annotation deleted'
    }];
}

sub subject {
    my ($self, $c) = @_;

    throw('case/file/annotation/no_user', 'user is not logged') unless $c->user_exists;

    return 'betrokkene-medewerker-' . $c->user->uidnumber;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 delete

TODO: Fix the POD

=cut

=head2 do_delete

TODO: Fix the POD

=cut

=head2 do_save

TODO: Fix the POD

=cut

=head2 save

TODO: Fix the POD

=cut

=head2 subject

TODO: Fix the POD

=cut

