package Zaaksysteem::Controller::API::ControlPanel;

use Moose;
use namespace::autoclean;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

# /api/controlpanel/$object
sub base : Chained('/'): PathPart('api/controlpanel'): CaptureArgs(1) {
    my ($self, $c, $object) = @_;


    $self->_get_interface($c);

    if (!$object) {
        throw('api/cp/missing_parameters', "Missing paramaters");
    }

    if ($object !~ m/^customer_(?:d|config)$/) {
        throw("/api/controlpanel/no_such_object/$object",
            "Not a valid object to query: '$object'"
        );
    }

    $c->stash->{object} = $object;
}

sub _get_interface {
    my ($self, $c) = @_;

    my $interface_rs = $c->model('DB::Interface')->search_active({ module => 'controlpanel' });
    if (!$interface_rs->count) {
        throw("/api/public/interface/inactive_or_missing",
            'Extern koppelprofiel koppeling is niet beschikbaar of inactief'
        );
    }

    my $interface =  $interface_rs->first;
    if (!$c->user_exists) {
        my $user = $c->authenticate({ interface => $interface }, 'api');
        if (!$user) {
            throw("/api/cp/authentication_failed",
                "Digest authentication failed, check your credentials!");
        }
    }
    $c->stash->{interface} = $interface;
}

sub list : Chained('/') : PathPart('api/controlpanel/list') : Args(1) {
    my ($self, $c, $type) = @_;
    $self->_get_interface($c);

    if (!grep { $_ eq $type } qw(releases templates clouds loadbalancers)) {
        throw('/api/cp/list/invalid', 'Invalid list option');
    }

    $c->stash->{zapi} = [ $c->stash->{interface}->process_trigger($type) ];
}

# /api/controlpanel/$object
sub get : Chained('base') : PathPart('') : Args(0) {
    my ($self, $c) = @_;

    $c->stash->{zapi} = $c->stash->{interface}->process_trigger(
        'search_object',
        {
            object_class => $c->stash->{object},
            model        => $c->model('Object'),
            %{$c->req->params || {}}
        }
    );
}

# /api/controlpanel/$object/$uuid
sub get_by_uuid : Chained('base') : PathPart('') : Args(1) {
    my ($self, $c, $uuid) = @_;

    $c->stash->{zapi} = $c->stash->{interface}->process_trigger(
        'search_object',
        {
            object_class => $c->stash->{object},
            model        => $c->model('Object'),
            uuid         => $uuid
        },
    );
}

sub throw {
    die(sprintf"%s: %s\n", @_);
}

__PACKAGE__->meta->make_immutable();

__END__

=head1 NAME

Zaaksysteem::Controller::API::ControlPanel - Controller voor control panel objecten

=head2 DESCRIPTION

Basicly the same as /api/object, but implemented with HTTP Digest, perhaps we should move some of the logic to that.

=head1 METHODS

=head2 base

The default, this is where we check our credentials

=head2 get

Get all objects

=head2 get_by_uuid

Get the objects based on uuid

=head2 list

List the available templates, releases, clouds or loadbalancers

=head2 throw

Because catalyst doesn't rethrow an exception in a chain, it just continues. Die helps.

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
