package Zaaksysteem::Controller::API::Case;

use Moose;

use Zaaksysteem::Tools;
use Zaaksysteem::ZTT;

use JSON;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head1 NAME

Zaaksysteem::Controller::API::Case - ZAPI Case controller

=head1 DESCRIPTION

Case API. Based on "old" case backend.

=head1 ACTIONS

=head2 base

=cut

sub base : Chained('/api/base') : PathPart('case') : CaptureArgs(1) {
    my ($self, $c, $case_id) = @_;

    $c->stash->{ zaak } = try {
        return $c->model('DB::Zaak')->find($case_id)
    } catch {
        throw('api/case/retrieval_fault', sprintf(
            'Internal error while retrieving case by id "%s"',
            $case_id
        ));
    };

    if (!defined $c->stash->{zaak} || $c->stash->{zaak}->is_deleted) {
        throw('api/case/find_case', sprintf(
            'Unable to find case by id "%s"',
            $case_id
        ));
    }

    unless($c->check_any_zaak_permission(qw[zaak_read zaak_beheer zaak_edit])) {
        throw('api/case/authorization', 'Access denied');
    }

    $c->stash->{ zapi } = [];
    $c->stash->{ case } = $c->stash->{ zaak }->object_data;

    # We're special, this controller has a seperate implementation, we don't
    # want the Object model to get in our way.
    $c->model('Object')->prefetch_relations(0);
}

=head2 caseless_base

This base action exists such that APIs that are considered part of the Case
domain can chain off the API like L<Zaaksysteem::Controller::API::Case::File>
does, but without a case number.

=cut

sub caseless_base : Chained('/api/base') : PathPart('case') : CaptureArgs(0) { }

=head2 get

=head3 URL

C</api/case/[CASE_ID]>

=cut

sub get: Chained('base') : PathPart('') : Args(0): ZAPI {
    my ($self, $c) = @_;

    my $output = $c->stash->{ case }->TO_JSON;

    $output->{ case } = {
        checklist       => $self->_retrieve_checklist($c),
        case_actions    => $self->_retrieve_actions($c),
        relations       => $self->_retrieve_relations($c),
        case_documents  => $self->_retrieve_documents($c),
    };

    $output->{related_objects} = $self->_retrieve_related_objects($c, $c->stash->{case});
    $output->{type} = 'case';

    $output->{ values } = {
        %{ $output->{ values } },
        $self->_retrieve_mutations($c)
    };

    $c->stash->{zapi} = [ $output ];
}

=head2 confidentiality

Set a new confidentiality category for the referenced case.

=head3 URL

C</api/case/[CASE_ID]/confidentiality>

=head3 Parameters

=over 4

=item confidentiality

=back

=head3 Response

Return value: ZAPI on success

    {
        results: {
            message: 'Confidentiality updated',
            value: $params->{confidentiality}
        }
    }

=cut

### TODO: Parameter checking
sub confidentiality: Chained('base') : PathPart('confidentiality') : ZAPI {
    my ($self, $c) = @_;

    throw ('api/case/confidentiality', 'access violation')
        unless $c->check_any_zaak_permission('zaak_read','zaak_beheer','zaak_edit');

    throw ('api/case/confidentiality/method_not_post', 'only POST requests allowed')
        unless $c->req->method eq 'POST';

    my $new_value = $c->req->params->{confidentiality};

    $c->stash->{zaak}->set_confidentiality($new_value);
    $c->stash->{zaak}->update;

    $c->stash->{zapi} = [{
        message => 'Confidentiality updated',
        value   => $c->stash->{zaak}->confidentiality
    }];
}

=head2 request_attribute_update

request update to a field. the citizens are not allowed to change fields
without an official reviewing first, this sends a request to the official.

=head3 URL

C</api/case/[CASE_ID]/request_attribute_update>

=cut

sub request_attribute_update : Chained('base') : PathPart('request_attribute_update') : ZAPI {
    my ($self, $c, $bibliotheek_kenmerken_id) = @_;

    my $params = $c->req->params;

    throw('api/case/request_attribute_update', "need toelichting")
        unless defined $params->{toelichting}; # optional, can't be undef

    throw('api/case/request_attribute_update', "need bibliotheek_kenmerken_id")
        unless $bibliotheek_kenmerken_id;

    my $kenmerk = $c->stash->{ zaak }->zaaktype_node_id->zaaktype_kenmerken->find({
        bibliotheek_kenmerken_id => $bibliotheek_kenmerken_id,
    });

    unless($kenmerk) {
        throw('pip/update_field', "Couldn't find requested attribute.");
    }

    # get it, put on the stash where we need it
    $c->forward('stash_submitted_field_value', [ $kenmerk, $params ]);

    my $subject_identifier = $c->stash->{ subject_identifier } // ($c->user_exists ? $c->user->betrokkene_identifier : undef);

    my $scheduled_job = $c->model('DB')->resultset('ScheduledJobs')->create_task({
        task                        => 'case/update_kenmerk',
        bibliotheek_kenmerken_id    => $bibliotheek_kenmerken_id,
        value                       => $c->stash->{ veldoptie_value },

        # as soon as the subjects table /user management system
        # include the citizens we could add them here
        created_by                  => $subject_identifier,
        reason                      => $params->{ toelichting },
        case                        => $c->stash->{ zaak },
    });

    $self->_log_update_field($c, {
        toelichting => $params->{toelichting},
        new_values  => $c->stash->{veldoptie_value},
        value_type  => $kenmerk->bibliotheek_kenmerken_id->value_type,
        kenmerk     => $kenmerk->label || $kenmerk->bibliotheek_kenmerken_id->naam
    });

    # this functionality will quickly become viable, so let's keep it in commented form
    #$c->forward('update_field_message', [$scheduled_job->apply_roles->description]);
    $c->stash->{ template } = 'widgets/general/veldoptie_view.tt';
    $c->stash->{ nowrapper } = 1;

    $c->stash->{ veldoptie_multiple } = $kenmerk->bibliotheek_kenmerken_id->type_multiple;
    $c->stash->{ veldoptie_type } = $kenmerk->type;
    # veldoptie_value is put on the stash by sub-action

    my $html = $c->view('TT')->render($c, 'widgets/general/veldoptie_view.tt');
    $c->stash->{zapi} = [{ attribute_value_as_html => $html }];
}

=head2 stash_submitted_field_value

B<PRIVATE>

=cut

sub stash_submitted_field_value : Private {
    my ($self, $c, $kenmerk, $params) = @_;

    my $key = 'kenmerk_id_' . $kenmerk->get_column('bibliotheek_kenmerken_id');
    my $value = $params->{ $key };

    # if no checkboxes are checked, we will receive nothing, which would be OK
    if ($params->{ $key . '_checkbox' }) {
        $value //= [];
    }

    unless (defined $value) {
        throw('pip/field_value', 'Unable to get value for attribute "%s"', $kenmerk->bibliotheek_kenmerken_id->naam);
    }

    $c->stash->{ veldoptie_value } = $value;
}

=head1 HELPER METHODS

=head2 _retrieve_documents

=cut

sub _retrieve_documents {
    my ($self, $c)  = @_;

    my $case        = $c->stash->{ zaak };

    my $case_type_properties = $case->zaaktype_node_id->zaaktype_kenmerken;

    my @case_documents  = $case_type_properties->search(
        {
            'bibliotheek_kenmerken_id.value_type' => 'file',
        },
        {
            join => [
                'bibliotheek_kenmerken_id',
            ],
            prefetch => [
                'zaaktype_node_id',
                'bibliotheek_kenmerken_id'
            ]
        }
    )->all;

    return \@case_documents;
}

=head2 _retrieve_related_objects

=cut

sub _retrieve_related_objects {
    my ($self, $c, $row) = @_;

    # This spot right here is out of reach of the automated deep_relations
    # behaviour found in ZS::Object::Model, so it's explicit use is warrented
    # here.
    my $deep = $c->req->param('deep_relations') ? 1 : 0;

    my @relations;

    for my $rel ($row->object_relationships_object1_uuids) {
        my $relation = Zaaksysteem::Object::Relation->new(
            related_object_id   => $rel->get_column('object2_uuid'),
            related_object_type => $rel->object2_type,
            relationship_name_a => $rel->type1,
            relationship_name_b => $rel->type2,
            blocks_deletion     => $rel->blocks_deletion,
        );

        if($deep) {
            $relation->related_object(
                $c->model('Object')->inflate_from_row($rel->object2_uuid)
            );
        }

        push @relations, $relation;
    }

    for my $rel ($row->object_relationships_object2_uuids) {
        my $relation = Zaaksysteem::Object::Relation->new(
            related_object_id   => $rel->get_column('object1_uuid'),
            related_object_type => $rel->object1_type,
            relationship_name_a => $rel->type2,
            relationship_name_b => $rel->type1,
            blocks_deletion     => $rel->blocks_deletion,
        );

        if($deep) {
            $relation->related_object(
                $c->model('Object')->inflate_from_row($rel->object1_uuid)
            );
        }

        push @relations, $relation;
    }

    return \@relations;
}

=head2 _retrieve_relations

=cut

sub _retrieve_relations {
    my ($self, $c) = @_;

    my @relations;

    my $rels1 = $c->stash->{ case }->object_relationships_object1_uuids->search({ type1 => 'related' });
    my $rels2 = $c->stash->{ case }->object_relationships_object2_uuids->search({ type2 => 'related' });
    my $model = $c->model('Object');

    for my $rel1 ($rels1->all) {
        my $object_data = $rel1->object2_uuid;

        my $label = try {
            return $c->model('Object')->inflate_from_row($object_data)->TO_STRING;
        } catch {
            return 'INVALIDE';
        };

        push @relations, {
            object_type => $object_data->object_class,
            object_label => $label,
            object_uuid => $object_data->uuid,
        };
    }

    for my $rel2 ($rels2->all) {
        my $object_data = $rel2->object1_uuid;

        my $label = try {
            return $c->model('Object')->inflate_from_row($object_data)->TO_STRING;
        } catch {
            return 'INVALDE';
        };

        push @relations, {
            object_type => $object_data->object_class,
            object_label => $label,
            object_uuid => $object_data->uuid,
        };
    }

    return \@relations;
};

=head2 _retrieve_mutations

=cut

sub _retrieve_mutations {
    my ($self, $c) = @_;

    my %retval;

    for my $attr ($c->stash->{ zaak }->zaaktype_node_id->zaaktype_kenmerken->all) {
        next unless $attr->get_column('object_id');

        my $type = $c->model('Object')->inflate_from_row($attr->object_id);

        unless($type->type eq 'type') {
            throw('api/case/get/object_type', sprintf(
                'Expected a Type object to be linked via ZaaktypeKenmerken, found a %s',
                $type->type
            ));
        }

        my $key = sprintf('object.%s', $type->prefix);

        my $mutations_rs = $c->stash->{ case }->object_mutation_lock_object_uuids->search(
            { object_type => $type->prefix },
            { order_by => 'date_created' }
        );

        my @mutations;

        for my $mutation ($mutations_rs->all) {
            push @mutations, $mutation;

            my $object;

            # Build an in-memory object from the mutation values we receive, so we can TO_STRING
            if ($mutation->type eq 'create') {
                my %kvp;

                for my $attr (keys %{ $mutation->values }) {
                    my ($namespace, $attr_name) = split m[\.], $attr;

                    next unless $namespace eq 'attribute';

                    $kvp{ $attr_name } = $mutation->values->{ $attr };
                }

                $object = eval { $c->model('Object')->type_model->new_object($mutation->object_type, %kvp) };
            } else {
                $object = eval { $c->model('Object')->inflate_from_row($mutation->object_uuid) };
            }

            if (defined $object) {
                $mutation->label($object->TO_STRING);
            } else {
                my $type = $c->model('Object')->search('type', { prefix => $mutation->object_type })->next;
                my $name;

                if ($type->has_title_template) {
                    my $ztt = Zaaksysteem::ZTT->new->add_context($mutation->values);

                    $name = $ztt->process_template($type->title_template)->string;
                } else {
                    $name = $type->name;
                }

                $mutation->label(sprintf('INCOMPLEET: %s', $name));
            }
        }

        my $events = $c->stash->{ zaak }->loggings->search(
            { event_type => { 'ILIKE' => 'case/object/%' } },
            { order_by => { 'desc' => 'created_on' } }
        );

        for my $event ($c->stash->{ zaak }->loggings->search({ event_type => { 'ILIKE' => 'case/object/%' }})) {
            next unless $event->event_data;

            my $event_data = JSON->new->decode($event->event_data);

            next unless $event_data->{ object_type } eq $type->prefix;

            my ($mutation_type) = reverse split m[/], $event->event_type;
            my ($subject_id) = $event->created_by =~ m[betrokkene\-\w+\-(\d+)];

            my $mutation = $mutations_rs->new_result({
                object_uuid => $event->get_column('object_uuid'),
                lock_object_uuid => undef,
                object_type => $type->prefix,
                type => $mutation_type,
                executed => 1,
                values => {
                    map { $_->{ field } => $_->{ new_value } } @{ $event_data->{ changes } }
                }
            });

            # Set volatile fields
            $mutation->read_only(1);
            $mutation->label($event_data->{ object_label });

            push @mutations, $mutation;
        }

        $retval{ $key } = \@mutations;
    }

    return %retval;
}

=head2 _retrieve_checklists

=cut

sub _retrieve_checklist {
    my ($self, $c) = @_;

    my $checklists = $c->stash->{ zaak }->checklists->search(undef, {
        order_by => 'case_milestone'
    });

    # Index all lists by milestone
    my %checklists = map {
        $_->case_milestone => [
            $_->checklist_items(undef, { order_by => ['sequence', 'id'] })->all
        ]
    } $checklists->all;

    return {
        by_milestone => \%checklists
    };
}

=head2 _retrieve_actions

=cut

sub _retrieve_actions {
    my ($self, $c) = @_;

    my $milestones = $c->stash->{ zaak }->zaaktype_node_id->zaaktype_statussen->search(undef, {
        order_by => 'status'
    });

    # Collect all case actions, create ad-hoc if missing.
    my $action_rs = $c->stash->{ zaak }->case_actions_cine;

    # Index all actions by milestone
    my %actions = map {
        $_->status => [ $action_rs->milestone($_->status)->sorted ]
    } $milestones->all;

    return {
        by_milestone => \%actions
    };
}

=head2 _log_update_field

=cut

define_profile _log_update_field => (
    required => [qw[kenmerk value_type]],
    optional => [qw[new_values toelichting]],
);

sub _log_update_field {
    my ($self, $c, $opts) = @_;

    my $valid_opts = assert_profile($opts)->valid;

    my $new_values = $valid_opts->{value_type} =~ m|^bag| ?
        $c->model('Gegevens::Bag')->humanize($valid_opts->{new_values}) :
        $valid_opts->{new_values};

    return $c->model('DB::Logging')->trigger(
        'case/pip/updatefield', {
            component => 'zaak',
            zaak_id   => $c->stash->{zaak}->id,
            data => {
                kenmerk     => $valid_opts->{kenmerk},
                new_values  => $new_values,
                toelichting => $valid_opts->{toelichting}
            }
        }
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
