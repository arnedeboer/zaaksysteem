package Zaaksysteem::Controller::Auth;

use Moose;

use Zaaksysteem::Access;
use Zaaksysteem::Exception;
use Zaaksysteem::Tools;

use Crypt::OpenSSL::Random qw(random_bytes);
use Digest::SHA qw(sha256_base64);
use DateTime;
use NetAddr::IP;

BEGIN { extends 'Zaaksysteem::Controller' }

=head1 NAME

Zaaksysteem::Controller::Auth - Login/Logoff and authentication actions

=head1 DESCRIPTION

=head1 ACTIONS

=head2 login

This action provides the flow to login a user.

=head3 URL

C</auth/login>

=cut

sub login : Local : Access(*) {
    my ($self, $c)      = @_;


    ### In case of an XHR, and we get here...define we are not logged in
    if ($c->req->is_xhr) {
        $c->res->status('401');
    }

    my $interface = $c->model('DB::Interface')->find_by_module_name('authrestriction');

    if ($interface) {
        my $nets = $interface->jpath('$.ip_restrictions]');

        $c->log->info(
            'Found active AuthRestriction interface',
            'Testing nets: ' . $nets
        );

        $c->assert_allowed_ip($nets);
    }

    {
        # SAML
        $c->stash->{ idps } = [
            $c->model('DB::Interface')->search_module('samlidp', '$.login_type_user')
        ];

        $c->stash->{ success_endpoint } = $c->uri_for('/auth/login_saml');
    }

    if ($c->flash->{referer}) {
        $c->session->{'referer'} = $c->flash->{referer};
    } elsif ($c->req->params->{referer}) {
        $c->session->{'referer'} = $c->req->params->{referer};
    }

    if ($c->user_exists && !$c->user->is_external_api) {
        $c->res->redirect(
            ($c->session->{'referer'} ||
            $c->uri_for( '/')),
        );
        $c->detach;
    } else {
        $c->stash->{referer} = $c->session->{referer};
        $c->stash->{template} = 'auth/login.tt';
    }

    if (exists($c->req->params->{ username })) {
        my $username = lc($c->req->params->{ username });

        $c->detach('login_failure') unless length $username;

        my $auth_ok = try {
            return $c->authenticate({
                username => $username,
                password => $c->req->params->{ password },
            });
        } catch {
            $c->log->error('Something went wrong when logging in: ' . $_);
        };

        $c->detach('login_failure') unless $auth_ok;
        $c->detach('login_success');
    }

    ### Not logged in, return to auth/login OR directly to SAML provider when directlogin is set and login_screen = 0
    if ($c->stash->{ idps } && @{ $c->stash->{ idps } } == 1 && !$c->stash->{ prevent_saml }) {
        my ($saml_interface) = @{ $c->stash->{ idps } };

        if ($saml_interface->jpath('$.login_type_user_direct')) {
            $c->res->redirect($c->uri_for(
                sprintf('/auth/saml/%d', $saml_interface->id),
                { success_endpoint => $c->stash->{success_endpoint} }
            ));
        }
    }

    $c->detach;
}

=head2 page

    URL: /auth/page

The login page itself

=cut

sub page : Local : Access(*) {
    my ($self, $c)      = @_;

    $c->stash->{prevent_saml} = 1;
    $c->forward('login');
}


=head2 logout

This action will logout and wipe the session of the currently logged in
user.

=head3 URL

C</auth/logout>

=cut

sub logout : Local : Access(*) {
    my ($self, $c) = @_;

    $c->logout();
    $c->delete_session();

    $c->stash->{ message } = 'You have been logged out';

    $c->res->redirect($c->uri_for('/auth/page'));

    # Make sure this action cannot be part of a forward/go chain.
    $c->detach;
}

=head2 login_saml

Perform an employee login using the SAML IdP configured for those logins.

=cut

sub login_saml : Local : Access(*) {
    my ($self, $c) = @_;

    my $saml_state = $c->session->{ _saml } || {};

    if (!$saml_state->{ success }) {
        $c->res->redirect($c->uri_for("/auth/login"));
        $c->detach;
    }

    # Load correct IdP interface into authentication module
    my $interface = $c->model('DB::Interface')->find($c->query_session('$._saml_state.idp_id'));

    throw(
        'auth/saml/interface_config',
        "Configuration does not allow employee logins using this IdP"
    ) unless $interface->jpath('$.login_type_user');

    $c->get_auth_realm('saml')->credential->interface($interface);

    if ($c->authenticate($saml_state, 'saml')) {
        $c->forward("login_success");
    }
    else {
        $c->forward("login_failure");
    }
}

=head2 login_success

Private action. Used to set some session data after login, most notably the
XSRF token.

=cut

sub login_success : Private {
    my ($self, $c) = @_;
    $c->statsd->increment('employee.login.ok', 1);
    delete $c->session->{login_failed};

    my $random = random_bytes(8);

    $c->session->{ _xsrf_token } = sha256_base64(
        $random,
        $c->user->username,
        $c->config->{gemeente_id},
        DateTime->now->datetime,
    );
    $c->res->cookies->{ 'XSRF-TOKEN' } = {
        value => $c->session->{ _xsrf_token },
        secure => ($c->req->uri->scheme eq 'https' ? 1 : 0) # HAAAAACK
    };

    if (!keys %{ $c->get_user_permissions }) {
        # User has no rights (yet)
        $c->response->redirect($c->uri_for('/first_login'));
        $c->detach;
    }

    if ($c->session->{referer}) {
        $c->response->redirect($c->session->{referer});
    } else {
        $c->response->redirect($c->uri_for('/'));
    }
}

=head2 login_failure

Private action, called when an employee login fails.

Sends a message to statsd and ups a counter in the session so a nice error
message can be shown.

=cut

sub login_failure : Private {
    my ($self, $c) = @_;

    $c->statsd->increment('employee.login.failed', 1);
    $c->session->{login_failed}++;
    $c->response->redirect($c->uri_for('/auth/page'));
}

=head2 retrieve_roles

This action hydrates a set of roles under a specific organisational unit.

This action is only referenced in C<zaaksysteem.js> code, and should be
regarded as I<deprecated>.

=head3 URL

C</auth/retrieve_roles/[OU_ID]>

=head3 Parameters

Additionally to the required C<OU> identifier, this action checks for the
C<select_complete_afdeling> GET parameter. If set to a true-ish value, the
'null' role will be added to the response. This role implies a wildcard for
any role within the OU.

=head3 Example response

    {
       "json" : {
          "roles" : [
             {
                "has_children" : null,
                "entry" : {
                   "gidNumber" : "30032",
                   "cn" : "Burgemeester",
                   "memberUid" : [
                      "cn=cvanderknaap,ou=College,o=dev,dc=zaaksysteem,dc=nl",
                      "cn=evanmilligen,ou=College,o=sprint,dc=zaaksysteem,dc=nl",
                      "cn=rslotjes,ou=Beheer,o=sprint,dc=zaaksysteem,dc=nl"
                   ],
                   "description" : "Burgemeester"
                },
                "system" : 0,
                "name" : "Burgemeester",
                "children" : [],
                "internal_id" : "30032",
                "members" : [
                   "cn=cvanderknaap,ou=College,o=dev,dc=zaaksysteem,dc=nl",
                   "cn=evanmilligen,ou=College,o=sprint,dc=zaaksysteem,dc=nl",
                   "cn=rslotjes,ou=Beheer,o=sprint,dc=zaaksysteem,dc=nl"
                ],
                "counter" : 7,
                "type" : "posixGroup",
                "dn" : "cn=Burgemeester,ou=College,o=sprint,dc=zaaksysteem,dc=nl"
             },
             "split",
             .
             .
             .
          ]
       }
    }

The C<roles> key is the main output of this action. Take not of the C<split>
string included in the example. This is not meant to be a selectable role, but
is meant to inform the frontend select widget to insert a non-selectable split
visually.

=cut

sub retrieve_roles : Local {
    my ($self, $c, $ouid)   = @_;

    return unless $c->req->is_xhr;

    my $all_roles           = $c->model('DB::Roles')->get_all_cached($c->stash);

    my $roles               = [ grep { $_->system_role || $_->get_column('parent_group_id') == $ouid } @$all_roles ];

    # $c->log->debug('Roles: ' . Data::Dumper::Dumper([$roles, $all_roles]));

    my @rv                  = map {
        {
            children    => [],
            name        => ($_->system_role ? '' : '* ') . $_->name,
            internal_id => $_->id
        }
    } @$roles;

    @rv                     = sort { $a->{name} cmp $b->{name} } @rv;

    if($c->req->params->{select_complete_afdeling}) {
        unshift @rv, 'split';
        unshift @rv, {
            children        => [],
            name            => 'Gehele afdeling',
            internal_id     => ''
        };
    }

    my $json = {
        'roles' => \@rv
    };

    $c->stash->{json} = $json;
    $c->forward('Zaaksysteem::View::JSONlegacy');
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 login

TODO: Fix the POD

=cut

=head2 logout

TODO: Fix the POD

=cut

=head2 retrieve_roles

TODO: Fix the POD

=cut

