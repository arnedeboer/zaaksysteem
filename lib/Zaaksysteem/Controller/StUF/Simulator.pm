package Zaaksysteem::Controller::StUF::Simulator;
use Moose;

BEGIN { extends 'Zaaksysteem::Controller'; }

=head1 NAME

Zaaksysteem::Controller::StUF::Simulator - Simple StUF-ZKN client for demonstration purposes

=cut

use Encode qw(encode_utf8);
use HTTP::Request;
use HTTP::Headers;
use LWP::UserAgent;
use Zaaksysteem::Tools;

=head1 METHODS

=head2 assert_active

Asserts that the StUF-ZKN isn't disabled by the configuration (config flag
"enable_stufzkn_simulator" must be enabled).

If the simulator is not enabled, an error is shown.

=cut

sub assert_active : Private {
    my ($self, $c) = @_;

    if (! $c->model('DB::Config')->get_value('enable_stufzkn_simulator')) {
        $c->error('StUF-ZKN simulator is disabled');
        $c->detach();
    }

    return;
}

=head2 show_simulator

Show the simulator page (only if the simulator is enabled).

=cut

sub show_simulator : Chained('/') : PathPart('stuf/simulator') : Args(0) : Method('GET') {
    my ($self, $c) = @_;

    $c->forward('assert_active');

    $c->stash->{template} = 'stuf/simulator.tt';
}

=head2 process

Process the StUF-ZKN form submission and send the StUF-ZKN request.

This will send a request of the type specified in the C<soap_call> parameter to
the SOAP endpoint specified in C<endpoint>, with a StUF-ZKN message generated
using the rest of the parameters.

=cut

sub process : Chained('/') : PathPart('stuf/simulator/process') : Args(0) : Method('POST') {
    my ($self, $c) = @_;

    $c->forward('assert_active');
    $c->assert_any_user_permission('admin');

    my $endpoint = $c->req->params->{endpoint};
    my $method = $c->req->params->{soap_call};
    my $key = $c->req->upload('client_key');
    my $cert = $c->req->upload('client_cert');
    my $operation;

    my %args;
    $args{stuurgegevens}{zender}{applicatie} = $c->req->params->{zender_applicatie};
    $args{stuurgegevens}{zender}{administratie} = $c->req->params->{zender_administratie};
    $args{stuurgegevens}{zender}{organisatie} = $c->req->params->{zender_organisatie};
    $args{stuurgegevens}{zender}{gebruiker} = $c->req->params->{zender_gebruiker};
    $args{stuurgegevens}{ontvanger}{applicatie} = $c->req->params->{ontvanger_applicatie};
    $args{stuurgegevens}{ontvanger}{administratie} = $c->req->params->{ontvanger_administratie};
    $args{stuurgegevens}{ontvanger}{organisatie} = $c->req->params->{ontvanger_organisatie};
    $args{stuurgegevens}{ontvanger}{gebruiker} = $c->req->params->{ontvanger_gebruiker};

    if ($method eq 'genereerZaakIdentificatie_Di02') {
        $operation = 'generate_case_id';
        $args{stuurgegevens}{berichtcode} = 'Di02';
        $args{stuurgegevens}{functie} = 'genereerZaakidentificatie';
        $args{stuurgegevens}{referentienummer} = $c->req->params->{referentienummer};
    }
    elsif ($method eq 'genereerDocumentIdentificatie_Di02') {
        $operation = 'generate_document_id';
        $args{stuurgegevens}{berichtcode} = 'Di02';
        $args{stuurgegevens}{functie} = 'genereerDocumentidentificatie';
        $args{stuurgegevens}{referentienummer} = $c->req->params->{referentienummer};
    }
    elsif ($method eq 'zakLv01') {
        $operation = 'get_case_details';
        $args{stuurgegevens}{berichtcode} = 'Lv01';
        $args{stuurgegevens}{entiteittype} = 'ZAK';
        $args{stuurgegevens}{referentienummer} = $c->req->params->{referentienummer};
        $args{parameters}{sortering} = 1;
        $args{gelijk}{identificatie} = $c->req->params->{case_id};
    }
    elsif ($method eq 'edcLv01') {
        $operation = 'get_case_document';
        $args{stuurgegevens}{berichtcode} = 'Lv01';
        $args{stuurgegevens}{entiteittype} = 'EDC';
        $args{stuurgegevens}{referentienummer} = $c->req->params->{referentienummer};
        $args{parameters}{sortering} = 1;
        $args{gelijk}{identificatie} = $c->req->params->{document_id};
        $args{scope}{object}{isRelevantVoor}{gerelateerde}{identificatie} = $c->req->params->{case_id};
    }
    elsif ($method eq 'edcLk01') {
        my $upload = $c->req->upload('document');

        $operation = 'write_case_document';
        $args{stuurgegevens}{berichtcode} = 'Lk01';
        $args{stuurgegevens}{entiteittype} = 'EDC';
        $args{stuurgegevens}{referentienummer} = $c->req->params->{referentienummer};
        $args{stuurgegevens}{tijdstipBericht} = DateTime->now()->strftime('%Y%m%d%H%M%S%03N');
        $args{parameters}{mutatiesoort} = 'T';

        $args{object} = [
            {
                identificatie => { _ => $c->req->params->{document_id} },
                isRelevantVoor => {
                    gerelateerde => {
                        identificatie => $c->req->params->{case_id}
                    }
                },
                inhoud => {
                    contentType => $upload->type,
                    bestandsnaam => $c->req->params->{document_filename},
                    _ => $upload->slurp,
                },
                titel => $c->req->params->{description},
                vertrouwelijkAanduiding => $c->req->params->{vertrouwelijkheid},
                auteur => 'Naam',
                taal => 'NL',
            }
        ];

        $args{object}[0]{verzenddatum} = { _ => $c->req->params->{verzenddatum} }
            if $c->req->params->{verzenddatum};
        $args{object}[0]{ontvangstdatum} = { _ => $c->req->params->{ontvangstdatum} }
            if $c->req->params->{ontvangstdatum};
    }
    elsif ($method eq 'zakLk01a') {
        $operation = 'write_case';
        $args{stuurgegevens}{berichtcode} = 'Lk01';
        $args{stuurgegevens}{entiteittype} = 'ZAK';
        $args{stuurgegevens}{referentienummer} = $c->req->params->{referentienummer};
        $args{stuurgegevens}{tijdstipBericht} = DateTime->now()->strftime('%Y%m%d%H%M%S%03N');
        $args{parameters}{mutatiesoort} = 'T';
        $args{parameters}{indicatorOvername} = 'V';

        $args{object} = [
            {
                identificatie => { _ => $c->req->params->{case_id} },
                omschrijving => $c->req->params->{omschrijving},
                startdatum => { _ => $c->req->params->{start_date} },
                registratiedatum => { _ => $c->req->params->{registration_date} },
                zaakniveau => $c->req->params->{level} || 1,
                deelzakenIndicatie => { _ =>  $c->req->params->{deelzaken_indicator} }, # J/N
                isVan => {
                    entiteittype => 'ZAKZKT',
                    gerelateerde => {
                        entiteittype => 'ZKT',
                        omschrijving => $c->req->params->{zkt_omschrijving} // 'Onbekend',
                        code => $c->req->params->{code},
                        ingangsdatum => { _ => '20160101' },
                    },
                },
            },
        ];

        if ($c->req->params->{kenmerk} && $c->req->params->{kenmerk_bron}) {
            push @{ $args{object}[0]{kenmerk} }, {
                kenmerk => { _ => $c->req->params->{kenmerk} },
                bron    => { _ => $c->req->params->{kenmerk_bron} },
            };
        }

        my $initiator_type = $c->req->params->{initiator_type};
        if ($initiator_type eq 'medewerker') {
            $args{object}[0]{heeftAlsInitiator}{gerelateerde}{medewerker}{identificatie} = $c->req->params->{initiator_identification};
        }
        elsif ($initiator_type eq 'natuurlijk_persoon') {
            $args{object}[0]{heeftAlsInitiator}{gerelateerde}{natuurlijkPersoon}{'inp.bsn'} = $c->req->params->{initiator_identification};
        }
        elsif ($initiator_type eq 'vestiging') {
            $args{object}[0]{heeftAlsInitiator}{gerelateerde}{vestiging}{vestigingsNummer} = $c->req->params->{initiator_identification};
        }

        if ($c->req->params->{hasUitvoerende}) {
            $args{object}[0]{heeftAlsUitvoerende}{gerelateerde}{medewerker}{identificatie} = $c->req->params->{uitvoerende_identification};
        }
        if ($c->req->params->{hasVerantwoordelijke}) {
            # Yes, no "medewerker" sub-tree.
            $args{object}[0]{heeftAlsVerantwoordelijke}{gerelateerde}{identificatie} = $c->req->params->{verantwoordelijke_identification};
        }
        if ($c->req->params->{hasGemachtigde}) {
            my $type = $c->req->params->{gemachtigde_type};
            my $ident = $c->req->params->{gemachtigde_identification};
            if ($type eq 'medewerker') {
                $args{object}[0]{heeftAlsGemachtigde}[0]{gerelateerde}{medewerker}{identificatie} = $ident;
            }
            elsif ($type eq 'natuurlijk_persoon') {
                $args{object}[0]{heeftAlsGemachtigde}[0]{gerelateerde}{natuurlijkPersoon}{'inp.bsn'} = $ident;
            }
            elsif ($type eq 'vestiging') {
                $args{object}[0]{heeftAlsGemachtigde}[0]{gerelateerde}{vestiging}{vestigingsNummer} = $ident;
            }
        }
        if ($c->req->params->{hasBelanghebbende}) {
            my $type = $c->req->params->{belanghebbende_type};
            my $ident = $c->req->params->{belanghebbende_identification};
            if ($type eq 'medewerker') {
                $args{object}[0]{heeftAlsBelanghebbende}[0]{gerelateerde}{medewerker}{identificatie} = $ident;
            }
            elsif ($type eq 'natuurlijk_persoon') {
                $args{object}[0]{heeftAlsBelanghebbende}[0]{gerelateerde}{natuurlijkPersoon}{'inp.bsn'} = $ident;
            }
            elsif ($type eq 'vestiging') {
                $args{object}[0]{heeftAlsBelanghebbende}[0]{gerelateerde}{vestiging}{vestigingsNummer} = $ident;
            }
        }
        if ($c->req->params->{hasOverigBetrokkene}) {
            my $type = $c->req->params->{overigbetrokkene_type};
            my $ident = $c->req->params->{overigbetrokkene_identification};

            if ($type eq 'medewerker') {
                $args{object}[0]{heeftAlsOverigBetrokkene}[0]{gerelateerde}{medewerker}{identificatie} = $ident;
            }
            elsif ($type eq 'natuurlijk_persoon') {
                $args{object}[0]{heeftAlsOverigBetrokkene}[0]{gerelateerde}{natuurlijkPersoon}{'inp.bsn'} = $ident;
            }
            elsif ($type eq 'vestiging') {
                $args{object}[0]{heeftAlsOverigBetrokkene}[0]{gerelateerde}{vestiging}{vestigingsNummer} = $ident;
            }
        }
    }
    elsif ($method eq 'zakLk01b') {
        $operation = 'write_case';
        $args{stuurgegevens}{berichtcode} = 'Lk01';
        $args{stuurgegevens}{entiteittype} = 'ZAK';
        $args{stuurgegevens}{referentienummer} = $c->req->params->{referentienummer};
        $args{stuurgegevens}{tijdstipBericht} = DateTime->now()->strftime('%Y%m%d%H%M%S%03N');
        $args{parameters}{mutatiesoort} = 'W';
        $args{parameters}{indicatorOvername} = 'V';

        $args{object} = [
            # First object is ignored anyway.
            { identificatie => { _ => $c->req->params->{case_id} } },
            {
                identificatie => { _ => $c->req->params->{case_id} },
                heeft => [
                    {
                        gerelateerde => {
                            omschrijving => $c->req->params->{status_description},
                            volgnummer   => $c->req->params->{status_number},
                        },
                        datumStatusGezet => { _ => DateTime->now->strftime('%Y%m%d') },
                        isGezetDoor => {
                            gerelateerde => {
                                # Broken - something in XML::Compile breaks this.
                                medewerker => {
                                    identificatie => { _ => $c->req->params->{status_username} },
                                },
                            },
                        },
                    },
                ]
            },
        ];
    }
    elsif ($method eq 'zakLk01c') {
        $operation = 'write_case';
        $args{stuurgegevens}{berichtcode} = 'Lk01';
        $args{stuurgegevens}{entiteittype} = 'ZAK';
        $args{stuurgegevens}{referentienummer} = $c->req->params->{referentienummer};
        $args{stuurgegevens}{tijdstipBericht} = DateTime->now()->strftime('%Y%m%d%H%M%S%03N');
        $args{parameters}{mutatiesoort} = 'W';
        $args{parameters}{indicatorOvername} = 'V';

        $args{object} = [
            {
                # First object is ignored anyway
                identificatie => { _ => $c->req->params->{case_id} },
            },
            {
                identificatie => { _ => $c->req->params->{case_id} },
                startdatum => { _ => $c->req->params->{start_date} },
                registratiedatum => { _ => $c->req->params->{registration_date} },
                zaakniveau => $c->req->params->{level} || 1,
                deelzakenIndicatie => { _ =>  $c->req->params->{deelzaken_indicator} }, # J/N
            },
        ];

        if (my $einddatum = $c->req->params->{end_date}) {
            $args{object}[1]{einddatum} = { _ => $einddatum },
        }

        if (my $streefafhandeldatum = $c->req->params->{end_date_planned}) {
            $args{object}[1]{einddatumGepland} = { _ => $streefafhandeldatum },
        }

        if (my $result = $c->req->params->{result}) {
            $args{object}[1]{resultaat}{omschrijving} = $result;
            $args{object}[1]{resultaat}{toelichting} = "Toelichting: $result";
        }

        my $initiator_type = $c->req->params->{initiator_type};
        if ($initiator_type eq 'medewerker') {
            $args{object}[1]{heeftAlsInitiator}{gerelateerde}{medewerker}{identificatie} = $c->req->params->{initiator_identification};
        }
        elsif ($initiator_type eq 'natuurlijk_persoon') {
            $args{object}[1]{heeftAlsInitiator}{gerelateerde}{natuurlijkPersoon}{'inp.bsn'} = $c->req->params->{initiator_identification};
        }
        elsif ($initiator_type eq 'vestiging') {
            $args{object}[1]{heeftAlsInitiator}{gerelateerde}{vestiging}{vestigingsNummer} = $c->req->params->{initiator_identification};
        }

        if ($c->req->params->{hasUitvoerende}) {
            $args{object}[1]{heeftAlsUitvoerende}{gerelateerde}{medewerker}{identificatie} = $c->req->params->{uitvoerende_identification};
        }
        if ($c->req->params->{hasVerantwoordelijke}) {
            # Yes, no "medewerker" sub-tree.
            $args{object}[1]{heeftAlsVerantwoordelijke}{gerelateerde}{identificatie} = $c->req->params->{verantwoordelijke_identification};
        }
        if ($c->req->params->{hasGemachtigde}) {
            my $type = $c->req->params->{gemachtigde_type};
            my $ident = $c->req->params->{gemachtigde_identification};
            if ($type eq 'medewerker') {
                $args{object}[1]{heeftAlsGemachtigde}[0]{gerelateerde}{medewerker}{identificatie} = $ident;
            }
            elsif ($type eq 'natuurlijk_persoon') {
                $args{object}[1]{heeftAlsGemachtigde}[0]{gerelateerde}{natuurlijkPersoon}{'inp.bsn'} = $ident;
            }
            elsif ($type eq 'vestiging') {
                $args{object}[1]{heeftAlsGemachtigde}[0]{gerelateerde}{vestiging}{vestigingsNummer} = $ident;
            }
        }
        if ($c->req->params->{hasBelanghebbende}) {
            my $type = $c->req->params->{belanghebbende_type};
            my $ident = $c->req->params->{belanghebbende_identification};
            if ($type eq 'medewerker') {
                $args{object}[1]{heeftAlsBelanghebbende}[0]{gerelateerde}{medewerker}{identificatie} = $ident;
            }
            elsif ($type eq 'natuurlijk_persoon') {
                $args{object}[1]{heeftAlsBelanghebbende}[0]{gerelateerde}{natuurlijkPersoon}{'inp.bsn'} = $ident;
            }
            elsif ($type eq 'vestiging') {
                $args{object}[1]{heeftAlsBelanghebbende}[0]{gerelateerde}{vestiging}{vestigingsNummer} = $ident;
            }
        }
        if ($c->req->params->{hasOverigBetrokkene}) {
            my $type = $c->req->params->{overigbetrokkene_type};
            my $ident = $c->req->params->{overigbetrokkene_identification};

            if ($type eq 'medewerker') {
                $args{object}[1]{heeftAlsOverigBetrokkene}[0]{gerelateerde}{medewerker}{identificatie} = $ident;
            }
            elsif ($type eq 'natuurlijk_persoon') {
                $args{object}[1]{heeftAlsOverigBetrokkene}[0]{gerelateerde}{natuurlijkPersoon}{'inp.bsn'} = $ident;
            }
            elsif ($type eq 'vestiging') {
                $args{object}[1]{heeftAlsOverigBetrokkene}[0]{gerelateerde}{vestiging}{vestigingsNummer} = $ident;
            }
        }
    }
    else {
        throw(
            "soap/unsupported",
            "Unsupported call: $method"
        );
    }

    $args{stuurgegevens}{tijdstipBericht} = DateTime->now()->strftime('%Y%m%d%H%M%S%3N');

    my $ua = LWP::UserAgent->new(
        timeout => 10,
        ssl_opts => {
            verify_hostname => 0,
            keep_alive => 1,
            SSL_cert_file => $cert->tempname,
            SSL_key_file  => $key->tempname,
        }
    );

    $self->_call_soap($c, $ua, $endpoint, $operation, %args);

    $c->stash->{template} = 'stuf/simulator_result.tt';
}

sub _call_soap {
    my $self = shift;
    my $c = shift;
    my ($ua, $endpoint, $operation, %args) = @_;

    $c->xml_compile->add_class('Zaaksysteem::XML::Generator::StUF0310');
    my $stuf0310 = $c->xml_compile->stuf0310;

    my $xml = $stuf0310->$operation('reader', \%args);

    my $soap_client = Zaaksysteem::SOAP::Client->new(
        ua       => $ua,
        endpoint => $endpoint,
    );
    my $soap_data = $soap_client->call('', $xml);

    $c->stash->{soap_request}  = $soap_data->{request};
    $c->stash->{soap_response} = $soap_data->{response};
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
