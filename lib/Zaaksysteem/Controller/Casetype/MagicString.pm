package Zaaksysteem::Controller::Casetype::MagicString;

use Moose;
use namespace::autoclean;

use Zaaksysteem::Constants;
use Zaaksysteem::Attributes;

use Zaaksysteem::Tools;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head1 NAME

Zaaksysteem::Controller::Casetype::MagicString - ZAPI Controller

=head1 SYNOPSIS

 # /casetype/magicstring

=head1 DESCRIPTION

Zaaksysteem API Controller for magicstrings related to Casetypes.

=head1 METHODS

=head2 /casetype/CASETYPE_ID/magicstring [GET READ]

Returns a resultset of all possible magicstrings related to a casetype,
which also includes system strings like 'aanvrager_naam' when C<include_system>
is given.

B<Query Parameters>

=over 4

=item query

Type: STRING

 # /casetype/1/magicstring?query=test

Filters the results with the given query string

=item include_system

 # /casetype/1/magicstring?query=test&include_system=1

=back

=cut

sub index
    : Chained('/casetype/instance_base')
    : PathPart('magicstring')
    : Args(0)
    : ZAPI
{
    my ($self, $c)      = @_;

    throw('httpmethod/post', 'Invalid HTTP Method, no GET', []) unless
        lc($c->req->method) eq 'get';


    my $query           = lc($c->req->params->{query});

    ### TODO MOVE THIS TO MODEL!! :(
    my $rs_kenmerken    = $c->model('DB::BibliotheekKenmerken')
                        ->search_freeform(
                            $query,
                        );

    if ($c->stash->{zaaktype}) {
        $rs_kenmerken = $self->scope_to_zaaktype_id($c, $rs_kenmerken);
    }

    my @rv;
    while (my $kenmerk = $rs_kenmerken->next) {
        push(
            @rv,
            {
                'searchable_object_id'              => $kenmerk->magic_string,
                'searchable_object_label'           => $kenmerk->naam,
                'searchable_object_label_public'    => ($kenmerk->naam_public || $kenmerk->naam),
                'searchable_object_type'            => 'magicstring',
            },
        );
    }

    if ($c->req->params->{include_system}) {
        for my $magic_string (ZAAKSYSTEEM_MAGIC_STRINGS) {
            next unless !$query || $magic_string =~ /$query/;

            push(
                @rv,
                {
                    'searchable_object_id'          => $magic_string,
                    'searchable_object_label'       => $magic_string,
                    'searchable_object_type'        => 'magicstring',
                },
            );

        }
    }

    $c->stash->{zapi} = \@rv;
}


sub scope_to_zaaktype_id {
    my ($self, $c, $rs_kenmerken) = @_;

    $rs_kenmerken       = $rs_kenmerken->search(
        {
            'zaaktype_kenmerkens.zaaktype_node_id' => $c->stash
                                                        ->{zaaktype}
                                                        ->zaaktype_node_id
                                                        ->id,
        },
        {
            'join'  => 'zaaktype_kenmerkens',
        }
    );
    return $rs_kenmerken;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::API>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 index

TODO: Fix the POD

=cut

=head2 scope_to_zaaktype_id

TODO: Fix the POD

=cut

