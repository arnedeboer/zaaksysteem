package Zaaksysteem::Controller::Casetype::Attribute;

use Moose;
use namespace::autoclean;

use Zaaksysteem::Tools;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head1 NAME

Zaaksysteem::Controller::Casetype::Attribute - ZAPI Controller

=head1 SYNOPSIS

 # /casetype/attribute

=head1 DESCRIPTION

Zaaksysteem API Controller for attributes related to Casetypes.

=head1 METHODS

=head2 /casetype/ID/attribute [GET READ]

Returns a resultset of attributes related to a casetype

B<Query Parameters>

=over 4

=item query

Type: STRING

 # /casetype/1/attribute?query=test

Filters the results with the given query string

=back

=cut

sub index
    : Chained('/casetype/base')
    : PathPart('attribute')
    : Args(0)
    : ZAPI
{
    my ($self, $c) = @_;

    throw('httpmethod/post', 'Invalid HTTP Method, no GET', []) unless
        lc($c->req->method) eq 'get';

    my $rs              = $c->model('DB::BibliotheekKenmerken')
                        ->search_freeform(
                            $c->req->params->{query},
                        );

    ### Scope to zaaktype_id
    $rs                 = $rs->search(
        {
            'zaaktype_kenmerkens.zaaktype_node_id' => $c->stash
                                                        ->{zaaktype}
                                                        ->zaaktype_node_id
                                                        ->id,
        },
        {
            'join'  => 'zaaktype_kenmerkens',
        }
    );

    $c->stash->{zapi} = $rs;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::API>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 index

TODO: Fix the POD

=cut

