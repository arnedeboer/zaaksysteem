package Zaaksysteem::Controller::Beheer::Bibliotheek::Notificaties;

use Moose;

use File::stat;
use Zaaksysteem::Constants;

use Zaaksysteem::Tools;

BEGIN { extends 'Zaaksysteem::Controller' }

use constant NOTIFICATIES           => 'notificaties';
use constant NOTIFICATIES_MODEL     => 'Bibliotheek::Notificaties';
use constant NOTIFICATIES_DB        => 'DB::BibliotheekNotificaties';
use constant CATEGORIES_DB          => 'DB::BibliotheekCategorie';
use constant DATABASE_FIELDS        => qw/label message subject bibliotheek_categorie_id sender sender_address/;

sub base : Chained('/') : PathPart('beheer/bibliotheek/notificaties'): CaptureArgs(1) {
    my ( $self, $c, $id ) = @_;

    $c->assert_any_user_permission(qw[beheer beheer_zaaktype_admin]);

    $c->stash->{bib_type}   = NOTIFICATIES;

    if ($id) {
        my $entry = $c->model(NOTIFICATIES_DB)->find($id);

        unless($entry) {
            $c->res->redirect($c->uri_for('/beheer/bibliotheek'));
            $c->detach;
        }

        $c->stash->{ bib_entry } = $entry;

    } else {
        $c->stash->{bib_new}    = 1;
    }
}

sub preview : Chained('base') : PathPart('preview') {
    my ($self, $c) = @_;

    $c->log->debug("notificaties preview");

    $c->stash->{template} = 'beheer/bibliotheek/notificaties/view.tt';
    $c->stash->{nowrapper} = 1;
}

sub get : JSON : Chained('/') : PathPart('beheer/bibliotheek/notificaties/get') : Args() {
    my ($self, $c, $bib_notificatie_id) = @_;

    $c->stash->{ json } = $c->model(NOTIFICATIES_DB)->search({
        id => $bib_notificatie_id
    });

    $c->forward('Zaaksysteem::View::JSON');
}


=head2 bewerken

create or update entries.

=cut

{
    define_profile bewerken => (
        required => [qw/label message subject bibliotheek_categorie_id commit_message/],
        optional => [qw/sender sender_address/]
    );
    sub bewerken : Chained('base'): PathPart('bewerken'): Args() {
        my ( $self, $c, $bibliotheek_categorie_id ) = @_;
        my ($dv);

        if ($c->stash->{bib_new}) {
            $c->stash->{bib_id} = 0;
        } else {
            $c->stash->{bib_entry} = $c->model('DB::BibliotheekNotificaties')
                ->search({id => $c->req->params->{bibliotheek_notificatie_id}})->single;
            $c->stash->{bib_id} = $c->stash->{bib_entry}->id;
        }

        if ($c->stash->{categorie}) {
            $c->stash->{categorie_id} =
                $c->stash->{categorie}->id;
        }

        $c->stash->{bib_cat}        = $c->model(CATEGORIES_DB)->search(
            {
                'system'    => undef,
            },
            {
                order_by    => ['pid','naam']
            }
        );

        # if ($c->req->params->{update}) {
            $c->stash->{categorie_id} =
                $c->req->params->{bibliotheek_categorie_id};
            my $validated = 0;


            ### Default validation
            if ($dv = $c->zvalidate) {
                # check if a similar item exists already
                my $count = $c->model(NOTIFICATIES_DB)->search({
                    label => $c->req->params->{label},
                })->count;

                if ($c->stash->{bib_new} && $count) {

                    my @valid = grep { $_ ne 'label' } $dv->valid;
                    my $json = {
                        success     => $dv->success,
                        missing     => [ $dv->missing ],
                        invalid     => [ $dv->invalid ],
                        unknown     => [ $dv->unknown ],
                        valid       => [ @valid ],
                        msgs        => $dv->msgs,
                    };


                    push @{$json->{invalid}}, 'label';
                    $json->{success} = 0;
                    $json->{msgs}->{label} = 'Label bestaat al';
                    $c->zcvalidate($json);

                    $c->detach();
                } else {
                    $validated = 1;
                }
            }

            if (
                !$validated || $c->req->is_xhr &&
                exists($c->req->params->{do_validation})
            ) {
                $c->detach;
            }

            ### Let's work our magic on the bibliotheek
            my $options = $dv->valid;

            # select fields that need to go into the database
            my $fields = { map { $_ => $options->{$_} } DATABASE_FIELDS };

            my $current;
            if($c->stash->{bib_new}) {
                $current = $c->model(NOTIFICATIES_DB)->create($fields);
            } else {
                $current = $c->stash->{bib_entry};
                if($current) {
                    $current->update($fields);
                }
            }

            if ($c->req->params->{attachments}) {
                my $schema = $current->result_source->schema;
                my $bks = $schema->resultset('BibliotheekKenmerken')
                    ->search({id => $c->req->params->{attachments}});

                # Wipe all existing ones (history not required) and simply write what was given
                my $existing_documents = $current->bibliotheek_notificatie_kenmerks;
                if ($existing_documents->count) {
                    $existing_documents->delete;
                }

                while (my $bk = $bks->next) {
                    $schema->resultset('BibliotheekNotificatieKenmerk')->create({
                        bibliotheek_kenmerken_id   => $bk->id,
                        bibliotheek_notificatie_id => $current->id,
                    });
                }
            }


            $c->model('DB::Logging')->trigger($c->stash->{ bib_new } ? 'template/email/create' : 'template/email/update', {
                component => LOGGING_COMPONENT_NOTIFICATIE,
                component_id => $current->id,
                data => {
                    template_id => $current->id,
                    reason => $options->{ commit_message }
                }
            });

            if ($c->req->params->{json_response}) {
                $c->stash->{json} = {
                    'id'    => $current->id
                };
                $c->forward('Zaaksysteem::View::JSONlegacy');
                $c->detach;
            }

            $c->push_flash_message('E-mailsjabloon succesvol opgeslagen');
        # }



        if ($c->req->is_xhr) {
            $c->stash->{bibliotheek_categorie_id} ||= $bibliotheek_categorie_id;
            $c->stash->{template} = 'beheer/bibliotheek/notificaties/edit.tt';
            $c->detach;
        }

        $c->res->redirect(
            $c->uri_for(
                '/beheer/bibliotheek/'
                . $c->stash->{categorie_id}
            )
        );
        $c->detach;
    }
}

sub verwijderen : Chained('base'): PathPart('verwijderen'): Args() {
    my ( $self, $c )    = @_;
    my $entry           = $c->stash->{bib_entry};

    return unless $entry;

    ### Confirmed
    my $flag_only = 0;
    if (
        $entry->zaaktype_notificaties->count
    ) {
        ### in depth search
        my $used_in_zaaktype_notificaties = $entry->zaaktype_notificaties->search;
        my $notused = 1;
        while (
            $notused &&
            (my $zt_notificatie = $used_in_zaaktype_notificaties->next)
        ) {
            $c->log->debug("my zt_notificatie" . $zt_notificatie->id);
            if (
                $zt_notificatie->zaaktype_node_id->id eq
                $zt_notificatie->zaaktype_node_id->zaaktype_id->zaaktype_node_id->id &&
                !$zt_notificatie->zaaktype_node_id->zaaktype_id->deleted
            ) {
                $c->stash->{confirmation}->{message} =
                    'Helaas, dit e-mailsjabloon is in gebruik door een of meerdere actieve zaaktypen.';
                $notused=0;
                next;
            }

            ### Ok: Er zijn alleen nog verwijderde zaaktypen, is er een zaak
            ### ooit aan gekoppeld?
            if ($zt_notificatie->zaaktype_node_id->zaaks->count) {
                ### En is minstens 1 zaak _niet_ vernietigd
                if ($zt_notificatie->zaaktype_node_id->zaaks->search(
                        { status => { '!=' => 'deleted' }}
                    )->count
                ) {
                    $c->log->debug('Vond een actieve zaak met deze notificatie');
                    $c->stash->{confirmation}->{message} =
                        'Helaas, dit e-mailsjabloon is in gebruik door een of meerdere zaken.';
                    $notused = 0;
                    next;
                } else {
                    $flag_only = 1;
                }
            }

            ### Ok, looks like it is not used, er is geen actieve zaaktype
            ### en de inactieve zaaktypen hebben allemaal geen zaken gekoppeld
            ### gehad... Free to wipe, notused=1
        }
        if (!$notused) {
            $c->stash->{confirmation}->{msgonly}    = '1';

            ### Msg
            $c->detach('/page/confirmation');
        }
    }

    ### Post
    if ( $c->req->params->{confirmed}) {
        $c->model('DB::Logging')->trigger('template/email/remove', {
            component => LOGGING_COMPONENT_NOTIFICATIE,
            component_id => $entry->id,
            data => {
                template_id => $entry->id,
                reason => $c->req->param('commit_message')
            }
        });

        if ($flag_only) {
            $entry->deleted(DateTime->now());
            $entry->update;

            $c->log->debug(
                'E-mailsjabloon ' . $entry->id . ' verwijderd dmv flag'
            );
        } else {
            ### Do not forget to delete magic strings
            $entry->delete;
            $c->log->debug(
                'E-mailsjabloon ' . $entry->id . ' verwijderd'
            );
        }

        ### Msg
        $c->push_flash_message('E-mailsjabloon succesvol verwijderd.');
        $c->res->redirect(
            $c->uri_for(
                '/beheer/bibliotheek/'
                . $entry->bibliotheek_categorie_id->id
            )
        );

        $c->detach;
        return;
    }


    $c->stash->{confirmation}->{message}    =
        'Weet u zeker dat u dit e-mailsjabloon wilt verwijderen?'
        . ' Deze actie kan niet ongedaan gemaakt worden. Maar geen zorgen, dit
        e-mailsjabloon is niet in gebruik door een zaaktype';


    $c->stash->{confirmation}->{commit_message} = 1;
    $c->stash->{confirmation}->{type}       = 'yesno';

    $c->stash->{confirmation}->{uri}     = $c->uri_for(
                            '/beheer/bibliotheek/' . $c->stash->{bib_type} . '/'
#                            . $entry->bibliotheek_categorie_id->id . '/'
                            . $entry->id . '/verwijderen'
        );
    $c->forward('/page/confirmation');
    $c->detach;
}



sub search
    : Chained('/beheer/bibliotheek/base')
    : PathPart('notificaties/search')
    : Args()
{
    my ( $self, $c )        = @_;

    die "xml_http_only" unless ($c->req->is_xhr);

    my $params = $c->req->params();

    $c->stash->{nowrapper} = 1;
    $c->stash->{template} = 'beheer/bibliotheek/notificaties/search.tt';

    if ($params->{search}) {

        my $options = { deleted => undef };

        if($params->{term}) {
            $options->{search_term} = {
                'ilike' =>  '%'. $params->{term} .'%',
            };
        }
        $c->stash->{results} = $c->model(NOTIFICATIES_DB)->search($options,{
            order_by    => 'label',
            rows        => 20,
        });

        $c->stash->{template} = 'beheer/bibliotheek/notificaties/result.tt';
    }
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 CATEGORIES_DB

TODO: Fix the POD

=cut

=head2 LOGGING_COMPONENT_NOTIFICATIE

TODO: Fix the POD

=cut

=head2 NOTIFICATIES

TODO: Fix the POD

=cut

=head2 NOTIFICATIES_DB

TODO: Fix the POD

=cut

=head2 NOTIFICATIES_MODEL

TODO: Fix the POD

=cut

=head2 PARAMS_PROFILE_DEFAULT_MSGS

TODO: Fix the POD

=cut

=head2 base

TODO: Fix the POD

=cut

=head2 bewerken

TODO: Fix the POD

=cut

=head2 get

TODO: Fix the POD

=cut

=head2 preview

TODO: Fix the POD

=cut

=head2 search

TODO: Fix the POD

=cut

=head2 verwijderen

TODO: Fix the POD

=cut

