package Zaaksysteem::Controller::Beheer::Configuration;

use Moose;

BEGIN { extends 'Zaaksysteem::Controller' }

sub configuration : Path {
    my ($self, $c) = @_;

    $c->assert_any_user_permission('admin');

    my $advanced = $c->req->param('advanced') ? 1 : 0;

    $c->stash->{notifications} = [$c->model('DB::BibliotheekNotificaties')->search({}, {
        order_by => 'label'
    })->all];

    $c->stash->{config} = $c->model('DB::Config')->get_all($advanced);
    _delete_locations($c->stash->{config});
    $c->stash->{ template } = 'beheer/configuration.tt';
}

sub _delete_locations {
    my $params = shift;
    delete $params->{filestore_location};
    delete $params->{tmp_location};
}

sub save : Local {
    my ($self, $c) = @_;

    $c->assert_any_user_permission('admin');

    my $advanced = $c->req->param('advanced') ? 1 : 0;

    my $params = $c->req->params;
    _delete_locations($params);

    $c->model('DB::Config')->save($params, $advanced);

    $c->push_flash_message('Instellingen opgeslagen');
    $c->forward('configuration');
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 configuration

TODO: Fix the POD

=cut

=head2 save

TODO: Fix the POD

=cut

