package Zaaksysteem::Controller::Beheer::Zaaktypen;

use Moose;
use namespace::autoclean;

use Hash::Merge::Simple qw( clone_merge );
use Params::Profile;
use Zaaksysteem::Tools;

use Zaaksysteem::Constants;

BEGIN { extends 'Zaaksysteem::Controller' }

use constant ZAAKTYPEN              => 'zaaktypen';
use constant ZAAKTYPEN_MODEL        => 'DB::Zaaktype';
use constant CATEGORIES_DB          => 'DB::BibliotheekCategorie';

sub base : Chained('/') : PathPart('beheer/zaaktypen'): CaptureArgs(1) {
    my ( $self, $c, $zaaktype_id ) = @_;

    $c->assert_any_user_permission('beheer');

    $c->response->headers->header('Cache-Control', 'No-store');

    if($zaaktype_id && $zaaktype_id ne 'create') {
        my $zaaktype = $c->model('DB::Zaaktype')->find($zaaktype_id);

        $c->stash->{zaaktype_id}            = $zaaktype_id;
        $c->stash->{zaaktype_node_id}       = $zaaktype->zaaktype_node_id->id;
        $c->stash->{zaaktype_node_title}    = $zaaktype->zaaktype_node_id->titel;

    } else {
        $c->stash->{zaaktype_id}        = 'create';
        $c->stash->{zaaktype_node_id}   = 0;
    }

    $c->stash->{custom_objects_enabled} = $c->model('DB::Config')->get_value('custom_objects_enabled');

    if($zaaktype_id) {
        # Aanvraag voor het bewerken van een zaaktype opslaan
        my $gegevens_magazijn_id = $c->user->uidnumber;
        my $session_id           = $c->get_session_id();
        my $current_unixtime     = time();
        my $usage_seconds        = 600; # 10 minuten!
        my $update_or_create     = 1;

        my $zaaktype_lock = $c->model('DB::UserAppLock')->search({type => 'zaaktypen', type_id => $zaaktype_id});
        $zaaktype_lock    = $zaaktype_lock->first;


        if (defined $zaaktype_lock) {
            my $bid     = $zaaktype_lock->get_column('uidnumber');
            my $bo      = $c->model('Betrokkene')->get(
                {
                    extern  => 1,
                    type    => 'medewerker',
                },
                $bid
            );

            my $lastlog_unixtime                           = $zaaktype_lock->create_unixtime;
            my $seconds_between_inactivity                 = $current_unixtime - $lastlog_unixtime;
            my ($days, $hours, $minutes, $seconds)         = (gmtime $seconds_between_inactivity)[7,2,1,0];
            my ($days_t, $hours_t, $minutes_t, $seconds_t) = (gmtime $usage_seconds)[7,2,1,0];

            if ($zaaktype_lock->uidnumber ne $gegevens_magazijn_id) {
                # Na $usage_seconds seconden mag degene verder anders moet ie wachten
                if ($seconds_between_inactivity <= $usage_seconds) {

                    my $melding = 'Onder andere gebruiker \''.$bo->naam.'\' (vanaf ip-adres: "'.$c->engine->env->{REMOTE_ADDR}.'") ziet momenteel het zaaktype '.$c->stash->{zaaktype_node_title}.' in. <br/>'.
                                  "De laatste activiteit was $minutes minuten en $seconds seconden geleden.<br/>".
                                  "HOU ER DUS REKENING MEE DAT IEMAND ANDERS TEGELIJKERTIJD HET ZAAKTYPE KAN PUBLICEREN!<br/>".
                                  'Deze melding verdwijnt weer nadat de andere gebruiker het zaaktype heeft gepubliceerd of wanneer er binnen '.$minutes_t.' minuten geen activiteit heeft plaatsgevonden.';

                    $c->push_flash_message($melding);
                } else {
                    # Het kan zijn dat er andere gebruikers bezig waren met dit zaaktype
                    # Deze gebruikers waren echter meer dan x seconden inactief dus worden verwijdert
                    $c->model('DB::UserAppLock')->search({type => 'zaaktypen', type_id => $zaaktype_id})->delete;
                }
            } elsif ($zaaktype_lock->uidnumber eq $gegevens_magazijn_id) {
                if ($seconds_between_inactivity <= $usage_seconds) {
                    # In geval het dezelfde gebruiker is maar !een andere sessie!
                    if ($zaaktype_lock->session_id ne $session_id) {
                    my $melding = 'Onder andere gebruiker \''.$bo->naam.'\' (vanaf ip-adres: "'.$c->engine->env->{REMOTE_ADDR}.'") ziet momenteel het zaaktype '.$c->stash->{zaaktype_node_title}.' in. <br/>'.
                                  "De laatste activiteit was $minutes minuten en $seconds seconden geleden.<br/>".
                                  "HOU ER DUS REKENING MEE DAT IEMAND ANDERS TEGELIJKERTIJD HET ZAAKTYPE KAN PUBLICEREN!<br/>".
                                  'Deze melding verdwijnt weer nadat de andere gebruiker het zaaktype heeft gepubliceerd of wanneer er binnen '.$minutes_t.' minuten geen activiteit heeft plaatsgevonden.';

                        # Omdat de "PK" uid, type en type_id is en we dezelfde gebruiker hebben die wil bewerken
                        # moet niet het session_id worden ge-update (Het is namelijk een (ander) persoon onder dezelfde login!)
                        $update_or_create = 0;

                        $c->push_flash_message($melding);
                    }
                }
            }
        }

        if (0 && $update_or_create) {
            my $update = $c->model('DB::UserAppLock')->update_or_create({
                                        'uidnumber'       => $gegevens_magazijn_id,
                                        'type'            => 'zaaktypen',
                                        'type_id'         => $zaaktype_id,
                                        'create_unixtime' => $current_unixtime,
                                        'session_id'      => $session_id,
                                        });
        }
    }
}


sub zaaktypen_flush : Chained('/'): PathPart('beheer/zaaktypen/flush'): Args(1) {
    my ($self, $c, $zaaktype_id, $category_id) = @_;

    my @uri_args;

    if (defined $category_id) {
        push @uri_args, $category_id;
    }

    $c->assert_any_user_permission('beheer');

    die 'need zaaktype_id' unless($zaaktype_id);

    delete($c->session->{zaaktypen}->{$zaaktype_id});

    $c->res->redirect($c->uri_for('/beheer/bibliotheek', @uri_args));
    $c->detach;
}


sub flush : Chained('base'): PathPart('flush') {
    my ($self, $c) = @_;

    $c->assert_any_user_permission('beheer');

    my $zaaktype_id = $c->stash->{zaaktype_id};
    die 'need zaaktype_id' unless($zaaktype_id);

    delete($c->session->{zaaktypen}->{$zaaktype_id});

    $c->res->body("flushed zaaktype from your session: " . $zaaktype_id);
    $c->detach;
}


sub zaaktypen_clone : Chained('base'): PathPart('clone'): Args(0) {
    my ($self, $c)   = @_;

    my $zaaktype_id = $c->stash->{zaaktype_id} or die 'need zaaktype_id';

    # the cloned version will, once being re-commited, yield a copy
    $c->session->{zaaktypen}->{$zaaktype_id} = $c->model('Zaaktypen')->retrieve(
        id              => $zaaktype_id,
        as_session      => 1,
        as_clone        => 1,
    );

    $c->res->redirect(
        $c->uri_for('/beheer/zaaktypen/'. $zaaktype_id . '/bewerken')
    );
}

=head2 _zaaktype_niet_gevonden

helper function to bounce users into an error if the case type cannot be found

=cut

sub _zaaktype_niet_gevonden : Private {
    my ($self, $c) = @_;
    $c->push_flash_message('Zaaktype kon niet worden gevonden');
    $c->res->redirect($c->uri_for('/beheer/bibliotheek'));
    $c->detach;

}

sub zaaktypen_verwijder : Chained('base'): PathPart('verwijder'): Args(0) {
    my ($self, $c) = @_;

    my $zt_node = $c->model('Zaaktypen')->retrieve(nid => $c->stash->{zaaktype_node_id},);
    $c->forward('_zaaktype_niet_gevonden') if !$zt_node;

    if (!$c->req->params->{confirmed}) {
        $c->stash->{confirmation}->{message} =
            'Weet u zeker dat u zaaktype "'
            . $zt_node->titel . '"  wilt verwijderen?'
            . ' Deze actie kan niet ongedaan gemaakt worden';

        $c->stash->{confirmation}->{type}           = 'yesno';
        $c->stash->{confirmation}->{uri}            = $c->req->uri;
        $c->stash->{confirmation}->{commit_message} = 1;

        $c->forward('/page/confirmation');
        $c->detach;
    }

    my $zaaktype = $c->model('DB::Zaaktype')->search(
        zaaktype_node_id => $zt_node->id,
    )->first;

    $c->forward('_zaaktype_niet_gevonden') if !$zaaktype;

    my $categorie_id = $zaaktype->bibliotheek_categorie_id ? $zaaktype->bibliotheek_categorie_id->id : '';

    my $event = $c->model('DB::Logging')->trigger(
        'casetype/remove',
        {
            component    => LOGGING_COMPONENT_ZAAKTYPE,
            component_id => $zaaktype->id,
            data         => {
                casetype_id => $zaaktype->id,
                reason      => $c->req->param('commit_message')
            }
        }
    );

    if ($c->model('Zaaktypen')->verwijder(nid => $zt_node->id)) {
        $c->push_flash_message($event->onderwerp);
    }

    $zaaktype->update();

    $c->res->redirect(
        $c->uri_for(
            '/beheer/bibliotheek'
                . ($categorie_id ? '/' . $categorie_id : '')
        )
    );
    $c->detach;
}




sub zaaktypen_bewerken : Chained('base'): PathPart('bewerken'): CaptureArgs(0) {
    my ($self, $c)   = @_;

    my $params = $c->req->params();

    my $zaaktype_id = $c->stash->{zaaktype_id};
    # existing zaaktype
    if($zaaktype_id && $zaaktype_id ne 'create') {

        # load the zaaktype into the session, unless it's already there
        unless($c->session->{zaaktypen}->{$zaaktype_id}) {

            $c->session->{zaaktypen}->{$zaaktype_id} = $c->model('Zaaktypen')->retrieve(
                    id              => $zaaktype_id,
                    as_session      => 1,
                );
        }

    } else {
        # no zaaktype_id - we're creating one
        unless($c->session->{zaaktypen}->{create}) {

            $c->session->{zaaktypen}->{create} = {
                'create'    => 1,
                'node'      => {
                    'id'        => 0,
                    'version'   => 1,
                },
                active      => 0,
            };
        }
        $c->stash->{categorie_id} = $params->{bibliotheek_categorie_id};
    }

    $c->stash->{baseaction} = $c->stash->{formaction}   = $c->uri_for(
        '/beheer/zaaktypen/' .
        ( $c->stash->{zaaktype_id} || 0)
        . '/bewerken'
    );

    $c->stash->{params} = $c->stash->{zaaktype} = $c->session->{zaaktypen}->{$zaaktype_id};

    $c->stash->{categorie_id} ||= $c->stash->{zaaktype}->{zaaktype}->{bibliotheek_categorie_id};

    $c->stash->{categorie_id} ||= $c->session->{categorie_id};

    if ($c->stash->{params}->{definitie}->{oud_zaaktype}) {
        $c->stash->{flash} = 'LET OP: Dit is een oud zaaktype uit versie 1.1.'
            . ' Bij publicatie worden alle oude zaaktypen bijgewerkt met de'
            . ' nieuwe fasenamen';
    }

    $c->forward('_load_session_and_params');

    if($c->req->is_xhr && $c->req->params->{autosave}) {
        $c->stash->{json} = {success => 1};
        $c->forward('Zaaksysteem::View::JSONlegacy');
        $c->detach;
    }
}




sub zaaktypen_start : Chained('zaaktypen_bewerken'): PathPart(''): Args(0) {
    my ($self, $c) = @_;

    $c->forward('algemeen');
}




sub zaaktypen_view : Chained('base'): PathPart('view'): Args(0) {
    my ($self, $c) = @_;

    $c->stash->{catalogus} = $c->model('Zaaktype')->retrieve(
        nid => $c->stash->{zaaktype_node_id}
    );

    $c->stash->{template} = 'beheer/zaaktypen/view.tt';
}


sub get_steps {
    my ($self, $c) = @_;

    my @stappen_plan = qw/
        algemeen
        relaties
        acties
        milestone_definitie
        milestones
        auth
    /;

    if ($c->stash->{zaaktype}->{node}->{properties}->{is_casetype_mother}) {
        push @stappen_plan, 'children';
    }

    push @stappen_plan, 'finish';

    return @stappen_plan;
}

sub generate_steps : Private {
    my ($self, $c)   = @_;
    my $huidige_stap;

    my $stapnaam     = $c->stash->{huidige_stapnaam};
    my @stappen_plan = $self->get_steps($c);

    my $zaaktype_id = $c->stash->{zaaktype_id} or die 'need zaaktype_id';
    my $params = $c->req->params();

    my $vorige_stap;
    while (@stappen_plan && !$huidige_stap) {
        my $stap_data   = shift @stappen_plan;

        if ($stapnaam eq $stap_data) {
            if ($vorige_stap) {
                $c->stash->{vorige_stap}    = $vorige_stap;
            } elsif ($params->{goback}) {
                $c->forward('zaaktypen_flush', [$zaaktype_id]);
            }

            $huidige_stap = $stap_data;

            $c->stash->{volgende_stap}  = shift @stappen_plan;
        }

        $vorige_stap = $stap_data;
    }

    if ($params->{goback}) {
        if ($c->stash->{vorige_stapurl}) {
            $c->res->redirect(
                $c->stash->{vorige_stapurl}
            );
        } else {
            $c->res->redirect(
                $c->stash->{baseaction} . '/' .
                     $c->stash->{vorige_stap}
            );
        }
        $c->detach;
    }
}

sub _validate_part : Private {
    my ($self, $c)   = @_;

    $c->forward('generate_steps');

    # Submit
    if (
        $c->req->params && $c->req->params->{zaaktype_update} &&
        $c->stash->{validation}->{validation_profile}->{success} &&
        !($c->req->is_xhr && $c->req->params->{do_validation})
    ) {
        $self->merge_stash_into_session($c);

        my $destination = $c->req->param('destination') || '';
        if($destination eq 'finish') {
            $destination = '/beheer/zaaktypen/'.$c->stash->{zaaktype_id}.'/bewerken/finish';
        }
        if($destination) {
            $c->res->redirect(
                $c->uri_for($destination)
            );
        } else {
            $c->res->redirect(
                (
                    $c->stash->{volgende_stapurl} ||
                    $c->uri_for(
                        '/beheer/zaaktypen/'
                        . $c->stash->{zaaktype_id} . '/bewerken/'
                        . $c->stash->{volgende_stap}
                    )
                )
            );
        }
        $c->detach;
    }
}


sub merge_stash_into_session {
    my ($self, $c) = @_;

    delete($c->session->{params});

    my $tomerge = {};
    if ($c->stash->{params}->{params}) {
        $tomerge = $c->stash->{params}->{params};
    } else {
        $tomerge = $c->stash->{params};
    }

    # clean up temporary
    # should they be here in the first place?
    my $milestone_number = $c->stash->{milestone_number};
    if($milestone_number) {
        if(exists $tomerge->{statussen}->{ $c->stash->{milestone_number} }->{elementen}->{regels}) {
            my $regels = $tomerge->{statussen}->{ $c->stash->{milestone_number} }->{elementen}->{regels};
            foreach my $regel (values %$regels) {
                foreach my $key (keys %$regel) {
                    delete $regel->{$key} if($key =~ m|_previous|);
                }
            }
        }
    }

    my $zaaktype_id = $c->stash->{zaaktype_id} or die "need zaaktype_id";

    my $sessionmerged = clone_merge(
        $c->session->{zaaktypen}->{$zaaktype_id},
        $tomerge
    );


    ### Delete tmp session data
    $c->session->{zaaktypen}->{$zaaktype_id} = $sessionmerged;
}


sub _load_params_status_update : Private {
    my ($self, $c)  = @_;

    ### We need to reshake the params a bit, just to make it easy for
    ### javascript to work with the dynamic tables

    my $mnumber             = $c->req->params->{milestone_number};
    my $zaaktype_id = $c->stash->{zaaktype_id} or die 'need zaaktype_id';

    ### Load all data according to template
    #my $zaaktype_template   = $c->model('Zaaktypen')->session_template;

    ### Session parameters
    $c->stash->{milestone_number}   = $mnumber;


    #### Default parameters
    my %found_elementdata   = ();
    my $newreqparams        = {};

    ### Let's make sure we got all the checkboxes
    my @given_checkboxes = ();
    if ($c->req->params->{ezra_checkboxes}) {
        if (UNIVERSAL::isa($c->req->params->{ezra_checkboxes}, 'ARRAY')) {
            @given_checkboxes = @{ $c->req->params->{ezra_checkboxes} };
        } else {
            push(@given_checkboxes,
                $c->req->params->{ezra_checkboxes}
            );
        }
    }

    for my $paramkey (keys %{ $c->stash->{req_params} }) {
        my $paramvalue          = $c->stash->{req_params}->{$paramkey};

        next unless $paramkey   =~ /^params\.status\./;

        my $paramkeyoriginal = $paramkey;

        ### Are we able to work with this param?
        if (
            $paramkey !~ /^params\.status\.[\w\d_]+\.[\w\d_]+\.\d+$/
        ) {
            ### Remove bogus keys
            next;
        }

        ### Make param zaaksysteem readable
        {
            for my $given_checkbox (@given_checkboxes) {
                if (
                    !$c->stash->{req_params}->{$given_checkbox} &&
                    $given_checkbox eq $paramkey
                ) {
                    $paramvalue = undef;
                }
            }

            ### CHANGE: params.status.notificatie.label.1
            ### INTO: params.statussen.notificatie.1.label
            $paramkey   =~
            s/^params\.status(\.[\w\d_]+)(\.[\w\d_]+)(\.\d+)$/params\.statussen\.$mnumber\.elementen$1$3$2/g;

            ### Delete deprecated entry and set new one
            $newreqparams->{$paramkey} = (
                $paramvalue ? $paramvalue : undef
            );
        }

        ### Load kenmerk dialog data into params
        {
            my ($element, $elementnumber)   = $paramkey =~
                /^params\.statussen\.$mnumber\.elementen\.([\w\d_]+)\.(\d+)/;

            if (! defined($found_elementdata{$element . $elementnumber}) ) {
                my $elementdata = $c->session->{zaaktypen}
                    ->{$zaaktype_id}
                    ->{statussen}
                    ->{ $mnumber }
                    ->{ 'elementen' }
                    ->{ $element }
                    ->{ $elementnumber };

                if ($elementdata) {
                    for my $key (keys %{ $elementdata }) {
                        next if exists $newreqparams->{
                            'params.statussen.' . $mnumber
                            . '.elementen.' . $element
                            . '.' . $elementnumber . '.' . $key
                        };

                        $newreqparams->{
                            'params.statussen.' . $mnumber
                            . '.elementen.' . $element
                            . '.' . $elementnumber . '.' . $key
                        } = $elementdata->{$key};
                    }
                }

                $found_elementdata{$element . $elementnumber} = 1;
            }
        }
    }

    $c->stash->{req_params} = $newreqparams;

}

sub _handle_deleted_entries {
    my ($self, $c) = @_;

    ### Only for milestone handling
    return unless $c->stash->{milestone_number};

    my $zaaktype_id = $c->stash->{zaaktype_id} or die "need zaaktype_id!";
    my $milestone_number = $c->stash->{milestone_number} or die "need milestone number";


    my $old_params = $self->_get_session_zaaktype_status($c, {
        zaaktype_id => $zaaktype_id,
        milestone   => $milestone_number,
    })->{elementen};


    my $new_params  = $c->stash
        ->{new_params}
        ->{params}
        ->{statussen}
        ->{ $milestone_number }
        ->{ elementen };

    for my $element ( keys %{ $old_params }) {
        unless ($new_params->{ $element }) {

            delete $old_params->{$element};

            next;
        }
        for my $i (keys %{ $old_params->{ $element } }) {
            unless ($new_params->{ $element }->{ $i }) {

                delete $old_params->{$element}->{$i};
            }
        }
    }
}



# TODO every sub should use this or similar to retrieve zaaktype data from session.
# session police.
Params::Profile->register_profile(
    method  => '_get_session_zaaktype_status',
    profile => {
        required => [qw/zaaktype_id milestone/],
    }
);
sub _get_session_zaaktype_status {
    my ($self, $c, $params) = @_;

    my $dv = Params::Profile->check(params  => $params);
    die "invalid options for _get_session_zaaktype_status" unless $dv->success;

    my $zaaktype_id = $params->{zaaktype_id};
    my $milestone   = $params->{milestone};

    return $c->session->{zaaktypen}->{$zaaktype_id}->{params}->{statussen}->{$milestone} ||= {};
}



{
    sub _load_session_and_params : Private {
        my ($self, $c)   = @_;

        return unless $c->req->params && $c->req->params->{zaaktype_update};



        ### Clone parameters
        $c->stash->{req_params} = { %{ $c->req->params } };

        my $params = $c->req->params();


        if(exists $params->{'zaaktype_betrokkenen.betrokkene_type'}) {

            my $betrokkene_checkboxes = $params->{'zaaktype_betrokkenen.betrokkene_type'};

            $betrokkene_checkboxes = [$betrokkene_checkboxes]
                unless(
                    ref $betrokkene_checkboxes &&
                    ref $betrokkene_checkboxes eq 'ARRAY'
                );
            my $checkbox_map = {map {$_, 1} @$betrokkene_checkboxes};

            if(exists $checkbox_map->{preset_client}) {
                my $preset_client = $c->req->params->{preset_client};
                $c->stash->{req_params}->{'definitie.preset_client'} = $preset_client;
            } else {
                $c->stash->{req_params}->{'definitie.preset_client'} = undef;
            }
        }

        ### Let's make sure we got all the checkboxes
        if ($c->stash->{req_params}->{ezra_checkboxes}) {
            my @given_checkboxes = ();
            if (UNIVERSAL::isa($c->stash->{req_params}->{ezra_checkboxes}, 'ARRAY')) {
                @given_checkboxes = @{ $c->stash->{req_params}->{ezra_checkboxes} };
            } else {
                push(@given_checkboxes,
                    $c->stash->{req_params}->{ezra_checkboxes}
                );
            }

            for my $given_checkbox (@given_checkboxes) {
                if (!$c->stash->{req_params}->{$given_checkbox}) {
                    $c->stash->{req_params}->{$given_checkbox} = undef;

                    $c->log->debug('FOUND GIVEN CHECKBOX: ' .  $given_checkbox);
                }
            }

            delete($c->stash->{req_params}->{ezra_checkboxes});
        }

        ### Reorden 'special' params status update
        if ($c->req->params->{status_update}) {
            $c->forward('_load_params_status_update');
        }

        ### Update params
        $c->stash->{new_params} = {};
        for my $param (keys %{ $c->stash->{req_params} }) {
            # Security, only test.bla[.bla.bla]
            next unless $param  =~ /^[\w\d\_]+\.[\w\d\_\.]+$/;

            my $eval = '$c->stash->{new_params}->{';
            $eval   .= join('}->{', split(/\./, $param)) . '}';

            $eval   .= ' = $c->stash->{params}->{';
            $eval   .= join('}->{', split(/\./, $param)) . '}';

            $eval   .= ' = $c->stash->{req_params}->{\'' . $param . '\'}';
            eval($eval);
        }


        $self->clean_empty_rows($c->stash->{new_params});

        $self->_handle_deleted_entries($c);

        my $zaaktype_id = $c->stash->{zaaktype_id} or die 'need zaaktype_id';
        ### Resort params
        if ($c->req->params->{status_update}) {

            foreach my $elementnaam (
                keys %{ $c->stash->{params}->{params}->{statussen}->{
                        $c->stash->{milestone_number}
                    }->{elementen}
                }
            ) {
                my $sorted_params = {
                    map {
                        $_->{mijlsort} => $_
                    } values %{
                        $c->stash->{params}->{params}->{statussen}->{
                            $c->stash->{milestone_number}
                        }->{elementen}->{$elementnaam}
                    }
                };
                $c->stash->{params}->{params}->{statussen}->{
                    $c->stash->{milestone_number}
                }->{elementen}->{$elementnaam} = $sorted_params;
            }

            ### OK...finalize
            $c->session->{zaaktypen}->{$zaaktype_id}->{statussen}->{
                $c->stash->{milestone_number}
            }->{elementen} = $c->stash->{params}->{params}->{statussen}->{
                $c->stash->{milestone_number}
            }->{elementen};
        }


        if (exists($c->stash->{req_params}->{'zaaktype_betrokkenen.betrokkene_type'})) {
            my @betrokkenen = ();
            if (
                UNIVERSAL::isa(
                    $c->req->params->{'zaaktype_betrokkenen.betrokkene_type'},
                    'ARRAY'
                )
            ) {
                push(@betrokkenen,
                    @{ $c->req->params->{'zaaktype_betrokkenen.betrokkene_type'} }
                );
            } elsif ($c->req->params->{'zaaktype_betrokkenen.betrokkene_type'}) {
                push(@betrokkenen,
                    $c->req->params->{'zaaktype_betrokkenen.betrokkene_type'}
                );
            }

            my $counter = 0;
            $c->stash->{params}->{betrokkenen} = {};
            for my $betrokkene (@betrokkenen) {
                $c->stash->{params}->{betrokkenen}->{
                    ++$counter
                }   = {
                    betrokkene_type => $betrokkene,
                }
            }
        }
        ### Validate params
        $c->stash->{validation} = $c->model('Zaaktypen')->validate_session(
            'session'       => $c->stash->{params},
            'zs_fields'     => [ keys %{ $c->req->params } ],
        );

        ### Give JSON some feedback
        if ($c->req->is_xhr && $c->req->params->{do_validation}) {
                $c->zcvalidate($c->stash->{validation}->{validation_profile});
                $c->detach;
        }
    }
}
=head2 clean_empty_rows

The javascript frontend is programmed in such a way that when you cancel
the component creation dialog you are left with an empty row. This
can be easily removed but also easily forgotten. This routine does cleanup
getting rid of a useless rows that would otherwise cause a die.

=cut

sub clean_empty_rows {
    my ($self, $params) = @_;

    # anti auto-vivification
    return unless my $statussen = $params->{params}->{statussen};

    ### Filters
    my $filterkeys = {
        notificaties    => ['bibliotheek_notificaties_id'],
        regels          => ['naam'],
        kenmerken       => [qw(naam label)],
        checklists      => ['label'],
        relaties        => ['relatie_zaaktype_id'],
        resultaten      => ['resultaat'],
    };

    my $filter = sub {
        my $elementen   = shift;
        my $element     = shift;
        my $checkkey    = shift;

        # once again - programmed this way to stop auto-vivification
        my $tocheck = $elementen->{$element} or return;

        foreach my $k (keys %$tocheck) {
            my $v = $tocheck->{$k};
            if (!grep {length($v->{$_})} @$checkkey) {
                delete $tocheck->{$k};
            }
        }
    };

    for my $filterkey (keys %$filterkeys) {
        $filter->($_->{elementen}, $filterkey, $filterkeys->{$filterkey}) for values %$statussen;
    }
}

sub algemeen : Chained('zaaktypen_bewerken'): PathPart('algemeen') {
    my ($self, $c) = @_;

    $c->stash->{bib_cat}        = $c->model(CATEGORIES_DB)->search(
        {
            'system'    => { 'is' => undef },
            'pid'       => undef,
        },
        {
            order_by    => ['pid','naam']
        }
    );

    $c->stash->{huidige_stapnaam}   = 'algemeen';

    $c->stash->{formaction}         .= '/algemeen';

    $c->forward('_validate_part');

    ### Speciale webformulieren
    {
        $c->stash->{speciale_webformulieren} = [];
        if (-d $c->config->{root} . '/tpl/zaak_v1/nl_NL/form/custom') {
            opendir(my $DIR, $c->config->{root} . '/tpl/zaak_v1/nl_NL/form/custom');
            while (my $file = readdir($DIR)) {
                next unless $file =~ /\.tt$/;

                $file =~ s/\.tt$//;

                push(@{ $c->stash->{speciale_webformulieren} },
                    $file
                );
            }
        }
    }

    $c->stash->{template}   = 'beheer/zaaktypen/algemeen/edit.tt';
}

sub relaties : Chained('zaaktypen_bewerken'): PathPart('relaties') {
    my ($self, $c) = @_;

    $c->stash->{huidige_stapnaam}   = 'relaties';

    $c->stash->{formaction}         .= '/relaties';

    ## Custom
    {
        $c->stash->{zaaktype_betrokkenen} = {};

        while (
            my ($bid, $betrokkene) = each %{
                $c->stash->{zaaktype}->{betrokkenen}
            }
        ) {
            $c->stash->{zaaktype_betrokkenen}->{
                $betrokkene->{betrokkene_type}
            } = 1;
        }
    }

    $c->forward('_validate_part');

    $c->stash->{template}   = 'beheer/zaaktypen/relaties/edit.tt';
}

sub acties : Chained('zaaktypen_bewerken'): PathPart('acties') {
    my ($self, $c) = @_;

    $c->stash->{huidige_stapnaam}   = 'acties';

    $c->stash->{formaction}         .= '/acties';

    $c->forward('_validate_part');

    $c->forward('case_action_checkboxes');
    $c->stash->{template}   = 'beheer/zaaktypen/acties/edit.tt';
}

sub children : Chained('zaaktypen_bewerken'): PathPart('children') {
    my ($self, $c) = @_;

    $c->stash->{huidige_stapnaam}   = 'children';
    $c->stash->{formaction}         .= '/children';

    $c->forward('_validate_part');
    $c->forward('children_titles');
    $c->stash->{template}   = 'beheer/zaaktypen/children.tt';
}


sub children_titles : Private {
    my ($self, $c) = @_;

    my $children = $c->stash->{zaaktype}->{node}->{properties}->{child_casetypes};

    # consolidate database query
    my @children_ids = map { $_->{casetype}{id} } @$children;

    my %titles = map { $_->id, $_->zaaktype_node_id->titel }
        $c->model('DB::Zaaktype')->search({ id => \@children_ids})->all;

    # supply frontend with handy structure
    foreach my $casetype (@$children) {
        my $id = $casetype->{casetype}{id};
        $casetype->{casetype} = { id => $id, title => $titles{$id} };
    }

    $c->stash->{child_casetypes} = $children;
}


sub auth : Chained('zaaktypen_bewerken'): PathPart('auth') {
    my ($self, $c) = @_;

    $c->stash->{huidige_stapnaam}   = 'auth';
    $c->stash->{formaction}         .= '/auth';

    $c->forward('_validate_part');
    $c->stash->{template}   = 'beheer/zaaktypen/auth/edit.tt';
}


sub finish : Chained('zaaktypen_bewerken'): PathPart('finish') {
    my ($self, $c) = @_;

    $c->stash->{huidige_stapnaam}   = 'finish';
    $c->stash->{formaction}         .= '/finish';

    $c->forward('_validate_part');

    $c->detach('publish') if $c->req->params->{commit};

    $c->stash->{template}   = 'beheer/zaaktypen/finish/view.tt';
}


=head2 get_commit_message

at least one checked checkbox is required, commit message
is optional.

=cut

sub get_commit_message {
    my ($self, $c) = @_;

    my @components = $c->req->param('components');
    die "commit message obligatory" unless @components;

    my $message = '. Componenten gewijzigd: ' . join ", ", @components;
    my $commit_message = $c->req->params->{commit_message};

    return $commit_message ? $message . '. Opmerkingen: ' . $commit_message : $message;
}


sub publish : Private {
    my ($self, $c) = @_;

    $c->stash->{commit_message} = $self->get_commit_message($c);

    my $casetype = $c->stash->{zaaktype};

    $c->detach('publish_single') unless $casetype->{node}->{properties}->{is_casetype_mother};

    my $casetype_display = {
        casetype => {
            id    => $casetype->{zaaktype}->{id},
            title => $casetype->{node}->{titel}
        }
    };

    my @children = $c->model('Zaaktypen')->list_child_casetypes({ mother => $casetype });

    $c->stash->{publish_queue} = [$casetype_display, @children];
    $c->stash->{template} = 'beheer/zaaktypen/finish/publish.tt';
}


sub publish_single : Private {
    my ($self, $c) = @_;

    # undef category is fine for zaaktypen_flush
    my $category_id = eval {
        $c->stash->{ zaaktype }{ zaaktype }{ bibliotheek_categorie_id }
    };

    $c->model('Zaaktypen')->commit_session(
        session     => $c->stash->{zaaktype},
        commit_message => $c->stash->{commit_message},
    );

    my $zaaktype_id = $c->stash->{zaaktype_id} or die 'need zaaktype_id';

    $c->push_flash_message('Zaaktype succesvol bijgewerkt');

    # zaaktypen_flush detaches
    $c->forward('zaaktypen_flush', [ $zaaktype_id, $category_id ]);
}


sub confirm_finish : Chained('') : PathPart('beheer/zaaktypen/finish_confirm') {
    my ($self, $c) = @_;

    $c->stash->{nowrapper} = 1;
    $c->stash->{template} = 'widgets/confirm.tt';
}


sub _add_milestone : Private {
    my ($self, $c) = @_;

    my @statusnums  = sort { $a <=> $b } keys %{ $c->stash->{zaaktype}->{statussen} };
    my $laststatus  = pop @statusnums;

    ### Reroute
    $c->stash->{zaaktype}->{statussen}->{ $laststatus }->{definitie}->{status} =
        ($laststatus + 1);

    $c->stash->{zaaktype}->{statussen}->{ ($laststatus + 1) } =
        $c->stash->{zaaktype}->{statussen}->{ $laststatus };

    $c->stash->{zaaktype}->{statussen}->{ $laststatus } = {
        'elementen' => {},
        'definitie' => {
            status  => $laststatus,
            create  => 1,
            ou_id   => $c->stash->{zaaktype}->{statussen}->{ $laststatus }
                ->{definitie}->{ou_id},
            role_id => $c->stash->{zaaktype}->{statussen}->{ $laststatus }
                ->{definitie}->{role_id}
        }
    };
}

sub _verify_milestone_definitie : Private {
    my ($self, $c) = @_;

    if ($c->req->params->{definitie_update}) {
        {
            my %availablestatussen = ();
            my $statussen;
            for my $key ( grep { /statussen\./ } keys %{ $c->req->params } ) {
                my ($statusid) = $key =~ /statussen\.(\d+)\./;

                if (! $availablestatussen{$statusid} ) {
                    if (
                        $c->req->params->{
                            'statussen.' . $statusid . '.definitie.naam'
                        }
                    ) {
                        $statussen->{$statusid} = $c->stash->{zaaktype}
                            ->{statussen}->{ $statusid };
                    }
                }

                $availablestatussen{$statusid} = 1;
            }

            $c->stash->{zaaktype}->{statussen} = $statussen;
        }

        ### Update data
        {
            my @statusnums     = sort { $a <=> $b } keys %{ $c->stash->{zaaktype}->{statussen} };
            my ($role_id,$ou_id);
            for my $statusnr (@statusnums) {
                # Naam
                $c->stash->{zaaktype}->{statussen}->{ $statusnr }
                    ->{definitie}->{naam}   = $c->req->params->{
                        'statussen.' . $statusnr . '.definitie.naam'
                    };


                # Fase
                $c->stash->{zaaktype}->{statussen}->{ $statusnr }
                    ->{definitie}->{fase}   = $c->req->params->{
                        'statussen.' . $statusnr . '.definitie.fase'
                    };

                my $role_set = $c->req->params->{
                    'statussen.' . $statusnr .  '.definitie.role_set'
                };

                # ou_id
                if (
                    $role_set || $statusnr == 1
                ) {
                    $ou_id      = $c->req->params->{
                        'statussen.' . $statusnr . '.definitie.ou_id'
                    };

                    $role_id    = $c->req->params->{
                        'statussen.' . $statusnr . '.definitie.role_id'
                    };
                }


                $c->stash->{zaaktype}->{statussen}->{ $statusnr }
                    ->{definitie}->{ou_id}   = $ou_id;

                $c->stash->{zaaktype}->{statussen}->{ $statusnr }
                    ->{definitie}->{role_set}   = $role_set;

                # scope_id
                $c->stash->{zaaktype}->{statussen}->{ $statusnr }
                    ->{definitie}->{role_id}   = $role_id;

            }
        }
    }

    ### Resort data
    {
        my @statusnums     = sort { $a <=> $b } keys %{ $c->stash->{zaaktype}->{statussen} };
        my ($statussen, $counter) = ({}, 0);
        my ($role_id, $ou_id) = @_;
        for my $statusnr (@statusnums) {
            ++$counter;
            my $statusdata  = $c->stash->{zaaktype}->{statussen}->{$statusnr};

            ### Make DAMN sure counter is the same as status
            $statusdata->{definitie}->{status} = $counter;

            $statussen->{$counter} = $statusdata;

            ### XXX Dit is pre-2.1 code, wat alleen gebruikt wordt bij
            ### bestaande zaaktypen van voor 2.1 die bewerkt worden. Het vinkje:
            ### toewijziging activeren staat standaard uit, en moet even aangezet
            ### worden voor afwijkende roles/ou's
            ### {
            if (
                !$c->req->params->{definitie_update} &&
                (
                    $statusdata->{definitie}->{ou_id} ne $ou_id ||
                    $statusdata->{definitie}->{role_id} ne $role_id
                )
            ) {
                $ou_id      = $statusdata->{definitie}->{ou_id};
                $role_id    = $statusdata->{definitie}->{role_id};
                $statusdata->{definitie}->{role_set} = 1;
            }
            ### }
        }

        $c->stash->{zaaktype}->{statussen} = $statussen;
    }
}

sub milestone_definitie : Chained('zaaktypen_bewerken'): PathPart('milestone_definitie') {
    my ($self, $c) = @_;

    $c->stash->{huidige_stapnaam}   = 'milestone_definitie';

    $c->stash->{formaction}         .= '/milestone_definitie';

    $c->forward('_verify_milestone_definitie');
    ### Ajax action
    if ($c->req->is_xhr && $c->req->params->{action}) {
        $c->forward('_add_milestone');

        $c->stash->{nowrapper} = 1;
        $c->stash->{template} =
            'beheer/zaaktypen/milestone_definitie/ajax_table.tt';
        $c->detach;
    }

    $c->forward('_validate_part');

    if (
        !$c->stash->{zaaktype}->{statussen} ||
        scalar(keys %{ $c->stash->{zaaktype}->{statussen} }) < 2
    ) {
        $c->stash->{zaaktype}->{statussen} = {
            1   => {
                'definitie' => {
                    'naam'      => 'Geregistreerd',
                    'fase'      => 'Registreren',
                    'status'    => 1,
                },
            },
            2   => {
                'definitie' => {
                    'naam'      => 'Afgehandeld',
                    'fase'      => 'Afhandelen',
                    'status'    => 2,
                }
            },
        };
    }

    if ($c->stash->{zaaktype}->{definitie}->{oud_zaaktype}) {
        my $laatste_status = scalar(
            keys(%{ $c->stash->{zaaktype}->{statussen} })
        );

        $c->stash->{zaaktype}->{statussen}->{1}
            ->{definitie}->{fase} = 'Registreren';
        $c->stash->{zaaktype}->{statussen}->{$laatste_status}
            ->{definitie}->{fase} = 'Afhandelen';
    }

    $c->stash->{template}   = 'beheer/zaaktypen/milestone_definitie/edit.tt';
}


sub milestones_base : Chained('zaaktypen_bewerken'): PathPart('milestones'): CaptureArgs(1) {
    my ($self, $c, $milestone_number) = @_;

    $c->stash->{milestone_number}   = $milestone_number;
    $c->stash->{page_title} = "Fase $milestone_number";

    if ($c->stash->{milestone_number} < 2) {
        $c->stash->{milestone_first} = 1;
    }

    if (
        $c->stash->{zaaktype}->{statussen} &&
        scalar(keys %{
            $c->stash->{zaaktype}->{statussen}
            }
        ) == $c->stash->{milestone_number}
    ) {
        $c->stash->{milestone_last} = 1;
    }

    $c->stash->{milestone} = $c->stash->{zaaktype}->{statussen}->{
        $c->stash->{milestone_number}
    };

    $c->stash->{milestoneurl}       .= $c->stash->{formaction} . '/milestones';
    $c->stash->{formaction}         .= '/milestones/'
        . $c->stash->{milestone_number};

    ### Define volgende stap bypass:
    if ($c->stash->{milestone_number} > 1) {
        $c->stash->{vorige_stapurl} =
            $c->stash->{milestoneurl} . '/' .
            ($c->stash->{milestone_number} - 1)
    }


    if ($c->req->params->{status_update}) {
        if (
            $c->stash->{zaaktype}->{statussen}->{
                ($c->stash->{milestone_number} + 1)
            }
        ) {
            $c->stash->{volgende_stapurl} =
                $c->stash->{milestoneurl} . '/' .
                ($c->stash->{milestone_number} + 1)
        }
    }

    $c->stash->{huidige_stapnaam}   = 'milestones';

    $c->forward('_validate_part');
}


sub milestones_start : Chained('zaaktypen_bewerken'): PathPart('milestones'): Args(0) {
    my ($self, $c) = @_;

    ### Forward to milestone number 1
    $c->forward('milestones_base', [1]);
    $c->forward('milestones');
}


sub milestones : Chained('milestones_base'): PathPart(''): Args(0) {
    my ($self, $c) = @_;

    my $zaaktype_id = $c->stash->{zaaktype_id};

    $c->stash->{integrity}->{rules} = Zaaksysteem::Backend::Rules->integrity(
        {
            from_session    => $c->session->{zaaktypen}->{$zaaktype_id}
        },
        $c->model('DB')
    )->for_status_number($c->stash->{milestone_number});

    $c->stash->{template} = 'beheer/zaaktypen/milestones/edit.tt';
}


sub _dialog_process_post : Private {
    my ($self, $c) = @_;

    return unless $c->req->params->{update};

    my $zaaktype_id = $c->stash->{zaaktype_id} or die 'need zaaktype_id';

    my $dialog_data = $c->session->{zaaktypen}
                    ->{$zaaktype_id}
                    ->{statussen}
                    ->{ $c->stash->{milestone_number} }
                    ->{ 'elementen' }
                    ->{ $c->stash->{zaaktypen_tmp_store_element} }
                    ->{ $c->stash->{rownumber} };

    my $element     = $c->stash->{zaaktypen_tmp_store_element};

    my $elemdata    = {
        map {
            my $key = $_;
            $key    =~ s/^${element}_//;
            $key    => $c->req->params->{ $_ }
        } grep {
            $_ =~ /^${element}_/
        } %{ $c->req->params } };


    if ($c->req->params->{update_regel_editor}) {
        # Not interested in the past. Deletion of a condition or action should not keep
        # old data. So just get rid of old stuff.
        $dialog_data = $elemdata;
    }

    ### NEXT, drop empty checkboxes
    {
        my @dialog_checkboxes;
        if (
            $c->req->params->{dialog_checkboxes} &&
            UNIVERSAL::isa(
                $c->req->params->{dialog_checkboxes},
                'ARRAY'
            )
        ) {
            @dialog_checkboxes  = @{ $c->req->params->{dialog_checkboxes} };
        } elsif ($c->req->params->{dialog_checkboxes}) {
            @dialog_checkboxes  = ($c->req->params->{dialog_checkboxes});
        }

        for (@dialog_checkboxes) {
            my $key = $_;
            $key    =~ s/^${element}_//;
            if (!$elemdata->{ $key }) {
                $elemdata->{ $key } = undef;
            }
        }
    }

    if ($c->stash->{objecttype}) {
        for my $key (keys %{ $elemdata }) {
            my $val = ($key =~ m[_label$] ? $elemdata->{ $key } : int($elemdata->{ $key }));

            $dialog_data->{ object_metadata }{ $key } = $val;
        }
    } else {
        $dialog_data->{ $_ } = $elemdata->{ $_ } for keys %{ $elemdata };
    }

    # database chokes on an empty string value, but it likes undefs
    $dialog_data->{bibliotheek_kenmerken_id} ||= undef;

    $c->session->{zaaktypen}
        ->{$zaaktype_id}
        ->{statussen}
        ->{ $c->stash->{milestone_number} }
        ->{ 'elementen' }
        ->{ $c->stash->{zaaktypen_tmp_store_element} }
        ->{ $c->stash->{rownumber} } = $dialog_data;

    return 1;
}

sub _dialog_load_kenmerken : Private {
    my ($self, $c) = @_;

    my $params = $c->req->params();

    if (
        $c->stash->{zaaktypen_tmp_store_element} eq 'kenmerken' &&
        !exists($c->stash->{params}->{id}) &&
        !exists($c->stash->{params}->{label}) &&
        $params->{edit_id} &&
        $params->{kenmerk_type} eq 'kenmerk'
    ) {
        my $edit_id = $params->{edit_id};

        my $bibliotheek_kenmerk =
            $c->model('DB::BibliotheekKenmerken')->find($edit_id);

        if ($bibliotheek_kenmerk) {
            my @columns = $bibliotheek_kenmerk->result_source->columns;

            my %data;
            for my $column (@columns) {
                my $columnname      = $column;

                $columnname         = 'type'
                    if ($column eq 'value_type');

                $data{$columnname}  = $bibliotheek_kenmerk->$column;
            }

            $c->stash->{params} = \%data;
        }
    }

}

sub dialog_bewerken_base : Private {
    my ($self, $c) = @_;

    my $zaaktype_id             = $c->stash->{zaaktype_id} or die 'need zaaktype_id';

    $c->stash->{rownumber}      = $c->req->params->{rownumber};

    $c->forward('_dialog_process_post');

    $c->stash->{params}         = $c->session->{zaaktypen}
                                ->{$zaaktype_id}
                                ->{statussen}
                                ->{ $c->stash->{milestone_number} }
                                ->{ 'elementen' }
                                ->{ $c->stash->{zaaktypen_tmp_store_element} }
                                ->{ $c->stash->{rownumber} };

    $c->forward('_dialog_load_kenmerken');
    $c->stash->{nowrapper}  = 1;
}



=head2 kenmerk_bewerken


=cut

sub kenmerk_bewerken : Chained('milestones_base'): PathPart('kenmerk'): Args() {
    my ($self, $c) = @_;

    my $params = $c->req->params;

    my $id = $c->req->params->{edit_id};
    throw( "ztb/kenmerk/404", "Unable to find kenmerk with id $id") unless $id;

    $c->stash->{zaaktypen_tmp_store}         = 'milestones';
    $c->stash->{zaaktypen_tmp_store_element} = 'kenmerken';
    $c->stash->{template} = 'beheer/zaaktypen/milestones/edit_kenmerk.tt';

    my $kenmerk = $c->model('DB::BibliotheekKenmerken')->search_rs({ id => $id})->first;
    throw("ztb/kenmerk/404", "Unable to find kenmerk with id $id") unless $kenmerk;

    $c->stash->{bibliotheek_kenmerk} = $kenmerk;

    $c->forward('dialog_bewerken_base');
}

sub objecttype_bewerken : Chained('milestones_base') : PathPart('objecttype') : Args() {
    my ($self, $c) = @_;
    my $params = $c->req->params;

    $c->stash->{zaaktypen_tmp_store_element}    = 'kenmerken';
    $c->stash->{objecttype} = $c->model('Object')->retrieve(uuid => $c->req->params->{edit_id});

    $c->stash->{template}   = 'beheer/zaaktypen/milestones/edit_objecttype.tt';
    $c->forward('dialog_bewerken_base');
}

sub kenmerkgroup_bewerken : Chained('milestones_base'): PathPart('kenmerkgroup'): Args() {
    my ($self, $c) = @_;

    $c->stash->{zaaktypen_tmp_store}            = 'milestones';
    $c->stash->{zaaktypen_tmp_store_element}    = 'kenmerken';

    $c->stash->{template}   = 'beheer/zaaktypen/milestones/edit_kenmerk_group.tt';

    $c->forward('dialog_bewerken_base');
}

sub regelgroup_bewerken : Chained('milestones_base'): PathPart('regelgroup'): Args() {
    my ($self, $c) = @_;

    $c->stash->{zaaktypen_tmp_store}            = 'milestones';
    $c->stash->{zaaktypen_tmp_store_element}    = 'regels';

    $c->stash->{template}   = 'beheer/zaaktypen/milestones/edit_regel_group.tt';

    $c->forward('dialog_bewerken_base');
}


sub sjabloon_bewerken : Chained('milestones_base'): PathPart('sjabloon'): Args() {
    my ($self, $c) = @_;

    $c->stash->{zaaktypen_tmp_store}            = 'milestones';
    $c->stash->{zaaktypen_tmp_store_element}    = 'sjablonen';

    $c->stash->{template}   = 'beheer/zaaktypen/milestones/edit_sjabloon.tt';

    $c->forward('dialog_bewerken_base');
}


sub checklist_bewerken : Chained('milestones_base'): PathPart('checklist'): Args() {
    my ($self, $c) = @_;

    $c->stash->{zaaktypen_tmp_store}            = 'milestones';
    $c->stash->{zaaktypen_tmp_store_element}    = 'checklists';

    $c->stash->{template}   = 'beheer/zaaktypen/milestones/edit_checklist.tt';

    $c->forward('dialog_bewerken_base');
}


sub notificatie_bewerken : Chained('milestones_base'): PathPart('notificatie'): Args() {
    my ($self, $c) = @_;

    my $params = $c->req->params();

    $c->stash->{zaaktypen_tmp_store}            = 'milestones';
    $c->stash->{zaaktypen_tmp_store_element}    = 'notificaties';

    $c->stash->{template}   = 'beheer/zaaktypen/milestones/edit_notificatie.tt';

    $c->forward('dialog_bewerken_base');
}


sub resultaat_bewerken : Chained('milestones_base'): PathPart('resultaat'): Args() {
    my ($self, $c) = @_;

    my $params = $c->req->params();

    $c->stash->{zaaktypen_tmp_store}            = 'milestones';
    $c->stash->{zaaktypen_tmp_store_element}    = 'resultaten';

    $c->stash->{template}   = 'beheer/zaaktypen/milestones/edit_resultaat.tt';

    $c->forward('dialog_bewerken_base');
}


sub regel_bewerken : Chained('milestones_base'): PathPart('regel'): Args() {
    my ($self, $c) = @_;

    $c->stash->{zaaktypen_tmp_store}            = 'milestones';
    $c->stash->{zaaktypen_tmp_store_element}    = 'regels';


    my $params = $c->req->params();

    if($params->{'add_voorwaarde'}) {
        my $voorwaarden = $params->{'regels_voorwaarden'};
        $voorwaarden ||= '1';
        unless(ref $voorwaarden && ref $voorwaarden eq 'ARRAY') {
            $voorwaarden = [$voorwaarden];
        }
        # find a free slot
        my $lookup = {map {$_ => 1} @$voorwaarden};
        my $i = 0;
        while($lookup->{++$i}) {}
        $c->stash->{new_voorwaarde} = "$i";
    } elsif($params->{'add_actie'}) {
        my $acties = $params->{'regels_acties'};
        $acties ||= '1';
        unless(ref $acties && ref $acties eq 'ARRAY') {
            $acties = [$acties];
        }
        # find a free slot
        my $lookup = {map {$_ => 1} @$acties};
        my $i = 0;
        while($lookup->{++$i}) {}
        $c->stash->{new_actie} = "$i";
    } elsif($params->{'add_anders'}) {
        my $anders = $params->{'regels_anders'};
        $anders ||= '1';
        unless(ref $anders && ref $anders eq 'ARRAY') {
            $anders = [$anders];
        }
        # find a free slot
        my $lookup = {map {$_ => 1} @$anders};
        my $i = 0;
        while($lookup->{++$i}) {}
        $c->stash->{new_anders} = "$i";
    }

    # provide last_milestone
    my $zaaktype_id = $c->stash->{zaaktype_id};
    my $statussen = $c->session->{zaaktypen}->{$zaaktype_id}->{statussen};
    my ($last_milestone_id) = sort { $b <=> $a } keys %$statussen;
    $c->stash->{zaaktype_resultaten} = $statussen->{$last_milestone_id}->{elementen}->{resultaten};


    $c->stash->{params} = $c->req->params();

    # the checkbox filters need the structure with active fields now
    $c->stash->{add_active_structure} = sub {
        my $values = shift;
        return [] unless $values && @$values;

        # only add the active structure if it isn't there already
        return [map { ref $_ ? $_ : {value => $_, active => 1} } @$values];
    };

    for my $interface_name (qw(buitenbeter mijnoverheid)) {
        $c->stash->{interface_available}{$interface_name} = $c->model('DB::Interface')->find_by_module_name($interface_name) ? 1 :0;
    }

    $c->stash->{template}   = 'beheer/zaaktypen/milestones/edit_regel.tt';

    $c->forward('dialog_bewerken_base');
}


sub relatie_bewerken : Chained('milestones_base'): PathPart('relatie'): Args() {
    my ($self, $c) = @_;

    $c->stash->{zaaktypen_tmp_store}            = 'milestones';
    $c->stash->{zaaktypen_tmp_store_element}    = 'relaties';

    $c->stash->{template}   = 'beheer/zaaktypen/milestones/edit_relatie.tt';

    $c->forward('dialog_bewerken_base');

#    if(my $parent_advance_results = $c->stash->{params}->{parent_advance_results}) {
#        my $json = new Zaaksysteem::JSON;
#        $c->stash->{params}->{parent_advance_results} =
#            $json->_decode_from_json($parent_advance_results);
#    }
}



sub overzicht_milestones : Chained('milestones_base'): PathPart('overzicht') : Args() {
    my ($self, $c) = @_;

    $c->stash->{template} =
        'beheer/zaaktypen/milestones/overzicht_milestones.tt';
}



Params::Profile->register_profile(
    method  => '_session_zaaktype',
    profile => {
        required => [ qw/zaaktype_id/ ],
    }
);

sub _session_zaaktype {
    my ($self, $c, $params) = @_;

    my $dv = Params::Profile->check(params => $params);
    die "invalid options for _session_zaaktype" unless $dv->success;

    my $zaaktype_id = $params->{zaaktype_id};
    return $c->session->{zaaktypen}->{$zaaktype_id} ||= {};
}


sub GET : JSON : Chained('base'): PathPart('GET') {
    my ($self, $c) = @_;

    my $zaaktype_id = $c->stash->{zaaktype_id};

    $c->stash->{json} = $c->session->{zaaktypen}->{$zaaktype_id} || {};

    $c->forward('Zaaksysteem::View::JSON');
}

sub POST : JSON : Chained('base'): PathPart('POST') {
    my ($self, $c) = @_;

    my $params = $c->req->params;

    my $zaaktype_id = $c->stash->{zaaktype_id};

    my $zaaktype = $c->session->{zaaktypen}->{$zaaktype_id} = {};
    $zaaktype->{definitie}->{pdc_tarief} = $params->{pdc_tarief};

    $c->forward('Zaaksysteem::View::JSON');
}

sub case_action_checkboxes : Private {
    my ($self, $c) = @_;

    my $checkboxes = $c->model('Zaaktypen')->casetype_actions;

    # this works around some TT/Zaaksysteem legacy, could be avoided altogether by just
    # using a different interface to get info to the browser.
    # specifically talking to the params hash that is used to populate the html
    foreach my $checkbox (@$checkboxes) {
        my @path = split /\./, $checkbox->{field};
        my $field = pop @path;

        my $pointer = $c->stash->{params};
        map { $pointer = $pointer->{$_} } @path;
        $checkbox->{value} = $pointer->{$field};
    }
    $c->stash->{case_action_checkboxes} = $checkboxes;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 CATEGORIES_DB

TODO: Fix the POD

=cut

=head2 GET

TODO: Fix the POD

=cut

=head2 LOGGING_COMPONENT_ZAAKTYPE

TODO: Fix the POD

=cut

=head2 POST

TODO: Fix the POD

=cut

=head2 ZAAKTYPEN

TODO: Fix the POD

=cut

=head2 ZAAKTYPEN_MODEL

TODO: Fix the POD

=cut

=head2 acties

TODO: Fix the POD

=cut

=head2 algemeen

TODO: Fix the POD

=cut

=head2 auth

TODO: Fix the POD

=cut

=head2 base

TODO: Fix the POD

=cut

=head2 case_action_checkboxes

TODO: Fix the POD

=cut

=head2 checklist_bewerken

TODO: Fix the POD

=cut

=head2 children

TODO: Fix the POD

=cut

=head2 children_titles

TODO: Fix the POD

=cut

=head2 confirm_finish

TODO: Fix the POD

=cut

=head2 dialog_bewerken_base

TODO: Fix the POD

=cut

=head2 finish

TODO: Fix the POD

=cut

=head2 flush

TODO: Fix the POD

=cut

=head2 generate_steps

TODO: Fix the POD

=cut

=head2 get_steps

TODO: Fix the POD

=cut

=head2 kenmerk_bewerken

TODO: Fix the POD

=cut

=head2 kenmerkgroup_bewerken

TODO: Fix the POD

=cut

=head2 merge_stash_into_session

TODO: Fix the POD

=cut

=head2 milestone_definitie

TODO: Fix the POD

=cut

=head2 milestones

TODO: Fix the POD

=cut

=head2 milestones_base

TODO: Fix the POD

=cut

=head2 milestones_start

TODO: Fix the POD

=cut

=head2 notificatie_bewerken

TODO: Fix the POD

=cut

=head2 objecttype_bewerken

TODO: Fix the POD

=cut

=head2 overzicht_milestones

TODO: Fix the POD

=cut

=head2 publish

TODO: Fix the POD

=cut

=head2 publish_single

TODO: Fix the POD

=cut

=head2 regel_bewerken

TODO: Fix the POD

=cut

=head2 regelgroup_bewerken

TODO: Fix the POD

=cut

=head2 relatie_bewerken

TODO: Fix the POD

=cut

=head2 relaties

TODO: Fix the POD

=cut

=head2 resultaat_bewerken

TODO: Fix the POD

=cut

=head2 sjabloon_bewerken

TODO: Fix the POD

=cut

=head2 zaaktypen_bewerken

TODO: Fix the POD

=cut

=head2 zaaktypen_clone

TODO: Fix the POD

=cut

=head2 zaaktypen_flush

TODO: Fix the POD

=cut

=head2 zaaktypen_start

TODO: Fix the POD

=cut

=head2 zaaktypen_verwijder

TODO: Fix the POD

=cut

=head2 zaaktypen_view

TODO: Fix the POD

=head2 clean_empty_rows

TODO: Fix the POD

=cut

