package Zaaksysteem::Controller::Beheer::Plugins;

use Moose;
use namespace::autoclean;

BEGIN { extends 'Zaaksysteem::Controller' }

sub base : Chained('/') : PathPart('beheer/plugins'): CaptureArgs(1) {
    my ( $self, $c, $import_id ) = @_;

    $c->stash->{entry} = $c->model('DB::BeheerPlugins')->find($import_id);

    if (!$c->stash->{entry}) {
        $c->res->redirect($c->uri_for('/beheer/plugins'));
        $c->detach;
    }

}

sub index : Chained('/') : PathPart('beheer/plugins'): Args(0) {
    my ( $self, $c ) = @_;

    $c->stash->{plugin_list} = $c->model('DB::BeheerPlugins')->search(
        {
        },
        {
            order_by    => 'label'
        }
    );

    $c->stash->{template} = 'beheer/plugins/list.tt';
}

sub view : Chained('base') : PathPart(''): Args() {
    my ( $self, $c ) = @_;

    $c->stash->{template} = 'beheer/plugins/view.tt'
}

sub inschakelen : Chained('base') : PathPart('inschakelen'): Args() {
    my ( $self, $c ) = @_;

    if ($c->req->params->{confirmed}) {
        $c->log->debug(
            'Schakel plugin ' . $c->stash->{entry}->label . ' uit'
        );

        $c->stash->{entry}->actief(1);

        if ($c->stash->{entry}->update) {
            $c->push_flash_message(
                'Plugin "' . $c->stash->{entry}->label
                . '" succesvol ingeschakeld'
            );
        } else {
            $c->push_flash_message(
                'ERROR:  Plugin "' . $c->stash->{entry}->label
                . '"  kon niet worden ingeschakeld'
            );
        }

        $c->res->redirect($c->uri_for(
            '/beheer/plugins'
        ));
        $c->detach;
    }

    $c->stash->{confirmation}->{message}    =
        'Weet u zeker dat u de plugin "' . $c->stash->{entry}->label . '"'
        . ' wilt inschakelen?';

    $c->stash->{confirmation}->{type}       = 'yesno';
    $c->stash->{confirmation}->{uri}        = $c->uri_for(
        '/beheer/plugins/' . $c->stash->{entry}->id
        . '/inschakelen'
    );

    $c->forward('/page/confirmation');
    $c->detach;
}

sub uitschakelen : Chained('base') : PathPart('uitschakelen'): Args() {
    my ( $self, $c ) = @_;

    if ($c->req->params->{confirmed}) {
        $c->log->debug(
            'Schakel plugin ' . $c->stash->{entry}->label . ' uit'
        );

        $c->stash->{entry}->actief(0);

        if ($c->stash->{entry}->update) {
            $c->push_flash_message(
                'Plugin "' . $c->stash->{entry}->label
                . '" succesvol uitgeschakeld'
            );
        } else {
            $c->push_flash_message(
                'ERROR:  Plugin "' . $c->stash->{entry}->label
                . '"  kon niet worden uitgeschakeld'
            );
        }

        $c->res->redirect($c->uri_for(
            '/beheer/plugins'
        ));
        $c->detach;
    }

    $c->stash->{confirmation}->{message}    =
        'Weet u zeker dat u de plugin "' . $c->stash->{entry}->label . '"'
        . ' wilt uitschakelen?';

    $c->stash->{confirmation}->{type}       = 'yesno';
    $c->stash->{confirmation}->{uri}        = $c->uri_for(
        '/beheer/plugins/' . $c->stash->{entry}->id
        . '/uitschakelen'
    );

    $c->forward('/page/confirmation');
    $c->detach;
}

sub disabled : Private {
    my ($self, $c) = @_;

    $c->stash->{template} = 'beheer/plugins/disabled.tt';
}

sub prepare_page : Private {
    my ($self, $c) = @_;

    my $plugins = $c->model('DB::BeheerPlugins')->search(
        {
            'actief'    => 1
        },
    );

    $c->stash->{_plugins} = {};
    while (my $plugin = $plugins->next) {
        $c->stash->{_plugins}->{
            $plugin->naam
        } = 1;
    }
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 base

TODO: Fix the POD

=cut

=head2 disabled

TODO: Fix the POD

=cut

=head2 index

TODO: Fix the POD

=cut

=head2 inschakelen

TODO: Fix the POD

=cut

=head2 prepare_page

TODO: Fix the POD

=cut

=head2 uitschakelen

TODO: Fix the POD

=cut

=head2 view

TODO: Fix the POD

=cut

