package Zaaksysteem::Controller::Beheer::Objecttypen;

use Moose;
use namespace::autoclean;

use JSON;

BEGIN { extends 'Zaaksysteem::Controller' }

=head1 NAME

Zaaksysteem::Controller::Beheer::Objecttypen

=head1 DESCRIPTION

=head1 ACTIONS

=head2 base

=cut

sub base : Chained('/') : PathPart('beheer/objecttypen'): CaptureArgs(1) {
    my ( $self, $c, $objecttype_id ) = @_;

    $c->assert_any_user_permission('beheer');

    $c->stash->{ objecttype_id } = $objecttype_id;
    $c->stash->{ category_id   } = $c->req->param('bibliotheek_categorie_id');
}

=head2 edit

=head3 URL

C</beheer/objecttypen/[UUID]/bewerken>

=cut

sub edit : Chained('base') : PathPart('bewerken') : Args(0) {
    my ($self, $c) = @_;

    $c->stash->{ category_options } = JSON->new->encode([
        map { { label => $_->{ name }, value => $_->{ id } } }
            $c->model('DB::BibliotheekCategorie')->tree
    ]);

    $c->stash->{ template } = 'beheer/objecttypen/edit.tt';
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
