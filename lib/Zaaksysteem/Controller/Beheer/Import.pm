package Zaaksysteem::Controller::Beheer::Import;

use Moose;
use namespace::autoclean;

BEGIN { extends 'Zaaksysteem::Controller' }

sub index :Path :Args(0) {
    my ( $self, $c ) = @_;

    $c->stash->{template} = 'beheer/import.tt';
}

sub manual : Local {
    my ($self, $c)                  = @_;

    $c->assert_any_user_permission('admin');

    $c->stash->{template}           = 'beheer/import/manual.tt';

    my $kvk_importer                = $c->model('Beheer::Import::KVK');

    $c->stash->{import_definitions} = $kvk_importer->get_import_config;

    if (
        lc($c->req->method) eq 'post' &&
        $c->req->params->{filestore_uuid} &&
        $c->req->params->{import_definition}
    ) {
        my $filestore = $c->model('DB::Filestore')->find(
            {
                uuid    => $c->req->params->{filestore_uuid}
            }
        );

        $c->log->info('Parsing uploaded file: ' . $filestore->get_path);

        $kvk_importer->import_kvk(
            'import_definition' => $c->req->params->{import_definition},
            'options'   => {
                'filename'  => $filestore->get_path
            }
        );

        if ($kvk_importer->error) {
            $c->stash->{result} = $kvk_importer->error_msg;
            $c->stash->{error}  = $kvk_importer->error_msg;
        } else {
            $c->stash->{succes}     = $kvk_importer->import_rv->succesvol;
            $c->stash->{entries}    = $kvk_importer->import_rv->entries;
        }

    }
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 index

TODO: Fix the POD

=cut

=head2 manual

TODO: Fix the POD

=cut

