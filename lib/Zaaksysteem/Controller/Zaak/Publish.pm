package Zaaksysteem::Controller::Zaak::Publish;

use Moose;
use File::Spec::Functions;

use Zaaksysteem::Constants;
use Zaaksysteem::Tools;

BEGIN { extends 'Zaaksysteem::Controller' }

Zaaksysteem->register_profile(
    method  => 'publish',
    profile => {
        required => [ qw/selection profile/],
        optional => [ qw/zql selected_case_ids/],
    },
);

sub bulk_publish : Chained('/') : PathPart('bulk/publish') {
    my ($self, $c) = @_;

    my $params = $c->req->params();

    my $interfaces = $c->model('DB::Interface')->search_active({module => 'legacy_publicaties'});

    my $available_profiles = {};
    while (my $i = $interfaces->next) {
        my $config = $i->get_interface_config();
        $available_profiles->{$i->name} = $config;
    }
    $c->stash->{profiles} = $available_profiles;

    my $selection = $params->{selection};

# TODO: When checkboxes are used to make a selection, the display fields
# of the search query are not used. This can be fixed by changing the javascript
# to look for an active search query, passing on the parameter and getting the
# fields from that SearchQuery object.

    # default display fields
    my $search_query = $c->model('SearchQuery');
    my $display_fields = $search_query->get_display_fields();

    if($selection eq 'selected_cases') {
        my @case_ids = split /,/, $params->{selected_case_ids};

        $c->stash->{cases} = $c->model('DB::Zaak')->search_extended({
            'me.id' => {
                '-in' => \@case_ids
            }
        });
    } elsif($selection eq 'zql') {
        my $results = $c->model('Object')->zql_search($params->{zql});
        my $object_rs = $results->rs->search(undef, {columns => 'me.object_id'});

        $c->stash->{cases} = $c->model('DB::Zaak')->search_extended(
            { 'me.id' => { '-in' => $object_rs->as_query } }
        );
    } else {
        die "unknown selection: $selection";
    }

    for (keys %$params) {
        $c->stash->{$_} = $params->{$_};
    }

    my %publish_related_ids;

    foreach my $related_case_id ($c->req->param('related_case_id')) {
        $publish_related_ids{ $related_case_id } = 1;
    }

    my $published_file_ids = {};
    foreach my $file_id_combi ($c->req->param('file_id')) {
        # namespacing ot allow the same files be used differently in different cases
        my ($case_id, $file_id) = split /-/, $file_id_combi;

        next unless $c->model('DB::File')->search({
            id => $file_id,
            case_id => { -in => [keys %publish_related_ids] }
        })->count;

       $published_file_ids->{ $file_id_combi } = 1;
    }


    if($params->{commit}) {
        my $profile = $params->{profile};

        eval {
            my $result = $c->stash->{cases}->publish({
                root_dir        => $c->config->{root},
                files_dir       => $c->model('DB')->schema->tmp_path,
                hostname        => $c->req->uri->host,
                publish_script  => catfile($c->config->{home}, qw(bin legacy_publish.pl)),
                profile         => $profile,
                config          => $c->config->{publish}->{$profile},
                display_fields  => $display_fields,
                published_file_ids => $published_file_ids,
                published_related_ids => \%publish_related_ids,
                dry_run         => $params->{dry_run}
            });

            $c->stash->{publish_result} = $result->{result};
            $c->stash->{publish_logging} = $result->{logging};
        };

        if($@) {
            $c->stash->{publish_result} = 1;
            $c->stash->{publish_logging} .= "Server fout: " . $@;
        }

    }
    $c->stash->{cases} = $c->stash->{cases}->search({}, {rows=>10});
    $c->stash->{template} = 'zaak/publish.tt';
    $c->stash->{nowrapper} = 1;
}


sub testpublish : Chained('/') : PathPart('testpublish') : Args() {
    my ($self, $c, $id, $publish) = @_;

    my $zaak = $c->model('DB::Zaak')->find($id);

    $c->res->content_type('text/xml');
    $c->res->body(
        $zaak->ede_vergadering_document({
            cases => {},
            files => {},
            publish => $publish
        })->toStringHTML()
    );
    $c->detach;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 testpublish

TODO: Fix the POD

=head2 bulk_publish

TODO: Fix the POD

=cut

