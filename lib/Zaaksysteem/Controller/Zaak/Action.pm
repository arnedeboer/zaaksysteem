package Zaaksysteem::Controller::Zaak::Action;

use Moose;

use Zaaksysteem::Tools;

BEGIN { extends 'Zaaksysteem::Controller' }

sub base : JSON : Chained('/zaak/base') : PathPart('') : CaptureArgs(0) {
    my ($self, $c) = @_;

    $c->stash->{ actions } = $c->stash->{ zaak }->case_actions_cine;
}

define_profile phased_actions => (
    optional => [ qw[milestone] ],
    constraint_methods => {
        milestone => qr/^\d+$/
    }
);

sub phased_actions : Chained('base') : PathPart('actions') : CaptureArgs(0) {
    my ($self, $c) = @_;

    assert_profile($c->req->params);

    if($c->req->param('milestone')) {
        $c->stash->{ actions } = $c->stash->{ actions }->milestone($c->req->param('milestone'));
    } else {
        $c->stash->{ actions } = $c->stash->{ actions }->current;
    }
}

define_profile single_action => (
    required => [ qw[id] ],
    constraint_methods => {
        id => qr/^\d+$/
    }
);

sub single_action : Chained('base') : PathPart('action') : CaptureArgs(0) {
    my ($self, $c) = @_;

    assert_profile($c->req->params);

    my $action = $c->stash->{ actions }->find($c->req->param('id'));

    unless($action) {
        throw('action/not_found', "Invalid parameter `id` supplied, action could not be found in DB.");
    }

    $c->stash->{ action } = $c->stash->{ actions }->find($c->req->param('id'));
}

=head2 list

This action hydrates the specified actions in a JSON view.

=head3 URL

B</zaak/I<[case_id]>/actions>

=head3 Parameters

=over 4

=item milestone

I<Optional>.

Sequence number for the phase in which the checklist exists.

=back

=head3 Response

A simple non-paginated JSON body with an array of action objects, sorted by the order of execution.

    [
        {
            id: 123,
            type: 'template|case|email|allocation',
            label: 'This is an action',
            url: '/zaak/[case_id]/action?id=[action_id]'
            automatic: true|false
        },
        .
        .
        .
    ]

=cut

sub list : Chained('phased_actions') : PathPart('') : Args(0) {
    my ($self, $c) = @_;

    my $actions = $c->stash->{ actions }->sorted;

    $c->stash->{ json } = $actions;

    ### TODO SPEED When verified , below can be removed. apply rules just got called by "phased_actions"
    # $c->stash->{ json } = $c->stash->{ actions }->sorted->apply_rules({
    #     case => $c->stash->{ zaak }
    # });

    $c->detach('Zaaksysteem::View::JSON');
}

=head2 view

This action is used for retrieving an HTML form that is setup to modify
settings for the action.

=head3 URL

B</zaak/I<[case_id]>/action>

=head3 Parameters

=over 4

=item id

The id of the action to be retrieved.

=back

=head3 Response

HTML to be injected in a modal dialog.

=cut

sub view : Chained('single_action') : PathPart('') : Args(0) {
    my ($self, $c) = @_;

    # Unroll the data attributes of the action into the stash for easy
    # access in the template
    for my $key (keys %{ $c->stash->{ action }->data }) {

        if ($key eq 'case_document_attachments') {
            $c->stash->{case_document_attachments} = $c->stash->{ action }->non_empty_attachments;
        }
        else {
            $c->stash->{ $key } = $c->stash->{ action }->data->{ $key };
        }
    }

    $c->stash->{ milestone_last } = $c->stash->{ action }->casetype_status_id->is_last;
    $c->stash->{ template } = 'zaak/action.tt';
    $c->stash->{ nowrapper } = 1;
}

=head2 update

This action allows the frontend to set or unset the checkbox flag on an action.

=head3 URL

B</zaak/I<[case_id]>/action/update>

=head3 Parameters

=over 4

=item id

The id of the action to be retrieved

=item automatic

A boolean field setting or unsetting the automatic field on the action object

=back

=head3 Response

A single JSON object representing the updated action, formatted the same as for
the L</list> controller.

=cut

define_profile update => (
    required => [ qw[automatic] ]
);

sub update : Chained('single_action') : PathPart('update') : Args(0) {
    my ($self, $c) = @_;

    assert_profile($c->req->params);

    $c->stash->{ action }->automatic($c->req->param('automatic') ? 1 : 0);
    $c->stash->{ action }->state_tainted(1);
    $c->stash->{ action }->update;

    $c->stash->{ json } = $c->stash->{ action };
    $c->detach('Zaaksysteem::View::JSON');
}

=head2 data

This action allows the frontend to save the changes to the data of an action

=head3 URL

B</zaak/I<[case_id]>/action/data>

=head3 Parameters

=over 4

=item id

The id of the action to be modified

=item *

Any fields that where found in the data key of the JSON hydration of the
object to be saved are expected to be present.

=back

=head3 Response

A single JSON object representing the updated action, formatted the same as for
the L</list> controller.

=cut

sub data : Chained('single_action') : PathPart('data') : Args(0) {
    my ($self, $c) = @_;

    my $params = $c->req->params;
    my $action = $c->stash->{action};
    my $type   = $action->type;
    my $data   = $action->data;

    if ($type eq 'case') {
        delete $data->{$_} for qw/automatisch_behandelen kopieren_kenmerken status/;
        foreach my $param (keys %{ $params }) {
            if($param =~ m|^relaties_(.*)$|) {
                $data->{$1} = $params->{$param};
            }
        }
    }
    elsif ($type eq 'email') {
        my $cds = delete $params->{notificaties_case_document_attachments} || [];
        my %case_doc_ids = map { $_ => 1 } @$cds;

        my @notifications = grep { m|^notificaties_.+$| } keys %$params;
        foreach (@notifications) {
            $data->{$1} = $params->{$_} if $_ =~ m|^notificaties_(.+)$|
        }

        if ($params->{notificaties_rcpt} // '' eq 'medewerker_uuid') {
            $data->{behandelaar} = 'betrokkene-medewerker-' . $params->{notificaties_email};
        }

        if (exists $params->{betrokkene_naam}) {
            $data->{betrokkene_naam} = $params->{betrokkene_naam};
        }

        for my $ctd (@{$data->{case_document_attachments}}) {
            $ctd->{selected} = exists $case_doc_ids{$ctd->{case_document_ids}} ? 1 : 0;
        }
    }
    elsif ($type eq 'allocation') {
        $data->{$_} = $params->{$_} for qw[ou_id role_id];
    }
    elsif ($type eq 'template') {
        $data->{$_} = $params->{$_} for qw[filename bibliotheek_kenmerken_id target_format];
    }

    $action->data($data);
    $action->data_tainted(1);
    $action->update;

    if($params->{trigger} && ($params->{trigger} eq 'execute')) {
        $c->detach('execute');
    }

    $c->stash->{json} = $action;
    $c->detach('Zaaksysteem::View::JSON');
}

=head2 execute

This action executes a single action.

=head3 URL

B</zaak/I<[case_id]>/action/execute>

=head3 Parameters

=over 4

=item id

The id of the action to be executed.

=back

=head3 Response

A simple JSON message indicating success of the operation.

    { "success": true }

=cut

sub execute : Chained('single_action') : PathPart('execute') : Args(0) {
    my ($self, $c) = @_;

    my $action = $c->stash->{ action };

    my %action_args = (
        action => $action
    );

    if ($c->user_exists) {
        $action_args{ current_user } = $c->user;
    }

    my $item = $c->model('Queue')->run(
        $c->stash->{ zaak }->fire_action(%action_args)
    );

    my $redirect;

    # Allocations are executed directly
    if ($item->type eq 'allocate_case') {
        $redirect = $c->uri_for('/')->as_string;
    }

    my $response = {
        success => \1,

        # Fields required by old case view
        flash_message => sprintf('Actie "%s" uitgevoerd', $item->label),
        redirect => $redirect,
        #

        action_is_delayed => ($item->status eq 'pending' ? \1 : \0),
        queue_item => $item
    };

    $c->stash->{ json } = $response;
    $c->detach('Zaaksysteem::View::JSON');
}

=head2 untaint

This action completely untaints a single action.

=head3 URL

B</zaak/I<[case_id]>/action/untaint>

=head3 Parameters

=over 4

=item id

The id of the action to be untainted.

=back

=head3 Response

The same content as L</view>

=cut

sub untaint : Chained('single_action') : PathPart('untaint') : Args(0) {
    my ($self, $c) = @_;

    $c->stash->{ action }->state_tainted(0);
    $c->stash->{ action }->data_tainted(0);
    $c->stash->{ action }->update;

    $c->stash->{ json } = $c->stash->{ action };
    $c->detach('Zaaksysteem::View::JSON');
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 base

TODO: Fix the POD

=cut

=head2 phased_actions

TODO: Fix the POD

=cut

=head2 single_action

TODO: Fix the POD

=cut

