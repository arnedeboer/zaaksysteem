package Zaaksysteem::Controller::Zaak::Status;

use Moose;

use Zaaksysteem::Tools;

BEGIN { extends 'Zaaksysteem::Controller' }

=head2 index

Show the basic "status" template.

=cut

sub index :Chained('/zaak/base') : PathPart('status'): Args(0) {
    my ( $self, $c ) = @_;

    $c->stash->{nowrapper} = 1;
    $c->stash->{template} = 'zaak/elements/status.tt';
}


sub status_base : Chained('/zaak/base') : PathPart('status'): CaptureArgs(0) {
    my ($self, $c) = @_;

    $c->assert_any_zaak_permission('zaak_beheer','zaak_edit');

    if (!$c->can_change) {
        $c->res->redirect(
            $c->uri_for(
                '/zaak/' . $c->stash->{zaak}->id
            )
        );
        $c->detach;
    }

    if ($c->stash->{zaak}->is_afgehandeld) {
        my $errmsg = 'Deze zaak is afgehandeld, '
            .'extra wijzigingen zijn niet meer mogelijk';

        $c->log->warn($errmsg);
        $c->push_flash_message($errmsg);

        $c->response->redirect(
            $c->uri_for('/zaak/' . $c->stash->{zaak}->nr)
        );

        $c->detach;
    }
}


sub can_advance : Chained('status_base') : PathPart('can_advance') {
    my ($self, $c) = @_;

    my $case = $c->stash->{zaak};

    my $can_advance = $case->can_volgende_fase($c->model('Object'))->can_advance();
    $c->stash->{json} = $can_advance;
    $c->detach('Zaaksysteem::View::JSONlegacy');
}


=head2 advance


=cut

sub advance :Chained('status_base') : PathPart('advance'): Args(0) {
    my ($self, $c) = @_;

    my $case = $c->stash->{zaak};

    unless(
        ($case->behandelaar && $case->behandelaar->gegevens_magazijn_id eq $c->user->uidnumber) ||
        ($case->coordinator && $case->coordinator->gegevens_magazijn_id eq $c->user->uidnumber) ||
        $c->check_any_zaak_permission('zaak_beheer', 'zaak_edit')
    ) {
        $c->push_flash_message('U heeft geen rechten om de fase van de zaak aan te passen.');

        $c->stash->{ zapi } = [{
            success  => 0,
            redirect => $c->uri_for('/zaak/' . $c->stash->{ zaak }->id)->as_string,
            advance_result => {
                type => 'case/advance/not_allowed',
                message => 'U heeft geen rechten om de fase van de zaak aan te passen.',
                not_allowed => 1
            }
        }];

        $c->detach($c->view('ZAPI'));
    }

    try {
        # stuff goes awry, we wanna have a plan B
        $c->model('DB')->txn_do(sub {
            my $result = $case->advance({ context => $c });

            map { $c->push_flash_message($_) } @{ $result->{ flash_messages } };

            my $redirect;
            if($result->{ redirect_to_dashboard }) {
                $redirect = $c->uri_for('/');
            } else {
                $redirect = $c->uri_for('/zaak/' . $c->stash->{zaak}->nr);
            }

            $c->stash->{zapi} = [
                {
                    'success'           => 1,
                    'redirect'          => $redirect->as_string,
                    'advance_result'    => ''
                }
            ];
        });
    } catch {
        if (blessed($_) && $_->can('type')) {
            $c->stash->{zapi} = [
                {
                    'success'           => 0,
                    'redirect'          => '',
                    'advance_result'    => {
                        type    => $_->type,
                        message => $_->message,
                        object  => $_->object
                    }
                }
            ];
        } else {
            $c->stash->{zapi} = [
                {
                    'success'           => 0,
                    'redirect'          => '',
                    'advance_result'    => {
                        type    => 'unknown_error',
                        message => 'Een onbekend probleem heeft plaatsgevonden, actie kan niet worden uitgevoerd',
                    }
                }
            ];

            $c->log->error('Catched error: ' . $_);
        }
    };

    $c->detach($c->view('ZAPI'));
}


sub advance_waiting :Chained('/') : PathPart('zaak/status/advance_waiting'): Args(0) {
    my ($self, $c) = @_;

    $c->stash->{template} = 'zaak/status/advancewaiting.tt';
    $c->stash->{nowrapper} = 1;
}


sub advance_result_error :Chained('status_base') : PathPart('advance_error'): Args(0) {
    my ($self, $c) = @_;

    $c->stash->{advance_result}     = $c->req->params;
    $c->stash->{template} = 'zaak/status/advanceresult.tt';
    $c->stash->{nowrapper} = 1;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 advance_result_error

TODO: Fix the POD

=cut

=head2 advance_waiting

TODO: Fix the POD

=cut

=head2 can_advance

TODO: Fix the POD

=cut

=head2 status_base

TODO: Fix the POD

=cut

