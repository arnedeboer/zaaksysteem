package Zaaksysteem::Controller::Zaak::Relation;

use Moose;

use Zaaksysteem::Tools;

BEGIN { extends 'Zaaksysteem::Controller' }

=head2 relations

List all known plain relations of a case

=head3 URL

C</zaak/B<[case_id]>/relations>

=head3 Response

A simple non-paginated JSON body with an array of related cases, ordered according to the saved sequence values.

    [
        {
            "relation_id" => 123,
            "order" => 1,
            "type" => "continuation",
            "case" => {
                # Case JSON serialization
            }
        },
        .
        .
        .
    ]

=cut

sub relations : JSON : Chained('/zaak/base') : PathPart('relations') : Args(0) {
    my ($self, $c) = @_;

    my $case_id = $c->stash->{ zaak }->id;

    $c->stash->{ json } = [ $c->model('DB::CaseRelation')->get_sorted($case_id) ];

    $c->detach('Zaaksysteem::View::JSON');
}

=head2 remove

Remove a specific relationship between cases

=head3 URL

C</zaak/B<[case_id]>/relations/remove>

=head3 Parameters

=over 4

=item relation_id

I<relation_id> of the relation to remove.

=back

=head3 Response

The updated listing of the L</relations> call.

=cut

sub remove : Chained('/zaak/base') : PathPart('relations/remove') : Args(0) {
    my ($self, $c) = @_;

    my $relation = $c->model('DB::CaseRelation')->find($c->req->param('relation_id'));

    unless($relation) {
        die('No such relation found, id: ' . $c->req->param('relation_id'));
    }

    $relation->delete;

    $c->model('DB::CaseRelation')->recalculate_order($c->stash->{ zaak }->id);

    $c->detach('relations');
}

=head2 add

Add a relation between two cases

=head3 URL

C</zaak/B<[case_id]>/relations/add>

=head3 Parameters

=over 4

=item case_id

The I<case_id> of the case being related

=back

=head3 Response

Same as the L</relations> call, updated with the new order.

=cut

sub add : Chained('/zaak/base') : PathPart('relations/add') : Args(0) {
    my ($self, $c) = @_;

    my $case_id_a = $c->req->param('case_id');
    my $case_id_b = $c->stash->{ zaak }->id;

    for my $id ($case_id_a, $case_id_b) {
        unless($c->model('DB::Zaak')->find($case_id_a)) {
            die('No such case could be found, id: '. $case_id_a);
        }
    }

    my $relation = $c->model('DB::CaseRelation')->add_relation($case_id_a, $case_id_b);

    $c->stash->{ json } = [ $relation->view_in_context($case_id_b) ];

    $c->detach('Zaaksysteem::View::JSON');
}

=head2 hierarchy

List the tree hierarchy of this case, fully hydrated parent/child objects.

=head3 URL

C</zaak/B<[case_id]>/hierarchy>

=head3 Response

A simple non-paginated JSON body with an object that may contain 0 or more children, as well as a case serialization.

    {
        "case": { # Case JSON serialization },
        "children": [
            {
                "case": { # Child case JSON serialiation },
                "children": [ ... ]
            },
            .
            .
            .
        ]
    }

=cut

sub hierarchy : Chained('/zaak/base') : PathPart('hierarchy') : Args(0) {
    my ($self, $c) = @_;

    my $case_id = $c->stash->{ zaak }->id;

    $c->stash->{ json } = $c->stash->{ zaak }->hierarchy;

    $c->detach('Zaaksysteem::View::JSON');
}

=head2 move

Move a relation after an arbitrairy relation in the ordered relations structure

=head3 URL

C</zaak/B<[case_id]>/relations/move>

=head3 Parameters

=over 4

=item relation_id

This parameter must contain a I<relation_id> of the relation being moved.

=item after

This parameter must contain a I<relation_id> or null, defining the position of the related I<relation_id> in the sequence.

=back

=head3 Response

Same as the L</relations> call, updated with the new order.

=cut

sub move : Chained('/zaak/base') : PathPart('relations/move') : Args(0) {
    my ($self, $c) = @_;

    my $case_id = $c->stash->{ zaak }->id;

    my $relation = $c->model('DB::CaseRelation')->find($c->req->param('relation_id'));

    unless($relation) {
        die('Relation to move could not be found');
    }

    # Just make sure the after exists if provided
    if($c->req->param('after')) {
        my $after_relation = $c->model('DB::CaseRelation')->find(
            $c->req->param('after')
        );

        unless($after_relation) {
            die('Relation to move after could not be found');
        }
    }

    my @related_cases = $c->model('DB::CaseRelation')->get_sorted($case_id);

    my ($hijacked) = grep { $_->id eq $relation->id } @related_cases;
    @related_cases = grep { $_->id ne $relation->id } @related_cases;

    my @sorted_cases;

    $c->log->debug(sprintf(
        'Moving relation %s after %s',
        $hijacked->to_string,
        $c->req->param('after') || 'null'
    ));

    $c->log->debug(sprintf(
        'Order before move: %s',
        join(', ', map { $_->to_string } @related_cases)
    ));

    if($c->req->param('after')) {
        @sorted_cases = map
            { $_->id eq $c->req->param('after') ? ($_, $hijacked) : ($_) }
            @related_cases;

    } else {
        @sorted_cases = ($hijacked, @related_cases);
    }

    $c->log->debug(sprintf(
        'New order after move: %s',
        join(', ', map { $_->to_string } @sorted_cases)
    ));

    my $iter = 1;

    for my $rel (@sorted_cases) {
        $rel->order_seq($iter);
        $rel->update;

        $iter++;
    }

    $c->detach('relations');
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

