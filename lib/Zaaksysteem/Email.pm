package Zaaksysteem::Email;
use Moose;

use Zaaksysteem::Tools;

=head1 NAME

Zaaksysteem::Email - An Zaaksysteem e-mail processor

=head1 SYNOPIS

=cut

use Email::Address;
use Encode qw(encode_utf8);
use File::Basename;
use File::Slurp qw(read_file);
use File::Temp;

=head1 ATTRIBUTES

=head2 message

A L<Mail::Track::Message> object. Required.

=cut

has message => (
    is       => 'rw',
    isa      => 'Mail::Track::Message',
    required => 1,
);

=head2 schema

A L<Zaaksysteem::Schema> object. Required.

=cut

has schema => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Schema',
    required => 1,
);

=head1 METHODS

=head2 add_to_case

Add the E-mail message to a case.

=head3 ARGUMENTS

=over

=item * case

A Zaaksysteem case

=back

=cut

sub add_to_case {
    my ($self, $case, %args) = @_;

    my $message = $self->message;
    my @attachments = @{ $message->attachments };

    my $subject = defined $args{subject}
        ? "$args{subject} - " . $message->subject
        : $message->subject;

    my %options = (
        intake_owner => $self->get_intake_user || '',
        from         => $message->from         || '',
        subject      => $subject               || '',
    );

    # Save the body as files
    $self->_add_body_as_files(
        $case,
        subject => $options{subject},
        accept  => $args{accept}
    );

    my @failed_attachments;
    foreach (@attachments) {
        eval {
            $self->add_file(
                $_,
                case_id   => $case->id,
                directory => $options{subject},
                accept    => $args{accept},
            );
        };
        if ($@) {
            if ($_->can('filename')) {
                push(@failed_attachments, $_->filename);
            }
            else {
                use Data::Dumper;
                die Dumper { object => $_, error => $@ };
            }
        }
    }

    if (@failed_attachments) {
        my $fh = File::Temp->new(SUFFIX => '.txt');
        my $path = $fh->filename;

        print $fh "Geweigerde e-mailbestanden.\n";
        print $fh "$_\n" foreach @failed_attachments;

        close($fh);
        my $filename = "Geweigerde bestanden " . $message->subject_raw . ".txt";

        $self->schema->resultset('File')->file_create(
            {
                db_params => {
                    intake_owner => 'Zaaksysteem mailer',
                    created_by   => 'Zaaksysteem mailer',
                    directory_id =>
                        $self->create_dir_for_mail($case->id, $options{subject},
                        )->id,
                    case_id => $case->id,
                },
                name      => $filename,
                file_path => $path,
            }
        );
    }

    my $nested = $message->nested_messages;
    foreach (@{$nested}) {
        my $ze = Zaaksysteem::Email->new(
            message => $_,
            schema  => $self->schema
        );
        $ze->add_to_case($case, %args, subject => $subject);
    }
    return 1;
}

=head2 add_attachments

Adds attachments as individual files to the document intake.

=cut

sub add_attachments {
    my $self        = shift;
    my $intake_user = $self->get_intake_user();
    my @attachments = @{ $self->message->attachments };
    foreach (@attachments) {
        eval { $self->add_file($_, created_by => $intake_user,); };
    }
}

=head2 add_to_omgevingsloket

Creates a 'Omgevingsloket' case if the correct interface exists. It saves the XML as an attachments to the case.

=cut

sub add_to_omgevingsloket {
    my $self = shift;
    my $olo  = $self->schema->resultset('Interface')
        ->search_active({ module => 'omgevingsloket' });
    my $colo = $olo->count;

    throw("Email/OLO/not_active_or_existing",
        "Omgevingsloketinterface is niet bestaand of actief")
        if !$colo;
    throw("Email/OLO/multiple",
        "Multiple Omgevingsloketten actief, onmogelijk!")
        if $colo > 1;

    $olo = $olo->first;

    my $subject = $self->message->subject;

    my $lvlnummer = 0;
    # Four digits are the bare minimum for an LVL number
    if ($subject =~ /\s+(\d{4,})/) {
        $lvlnummer = $1;
    }

    my $xml = $self->_get_xml_from_email;
    if ($xml) {
        my $intake_user = $self->get_intake_user();

        my $pt = $olo->process({ input_data => scalar read_file($xml->path) });
        if (!$pt->success_count) {
            # If you see this failing in your vagrant, set ZS_DISABLE_STUF_PRELOAD=0
            throw("Email/OLO/olo/no_case",
                "Omgevingsloket kon geen zaak aanmaken of updaten");
        }

        # The last transaction should be ours
        my $rs = $pt->records({}, { order => '-desc' })
            ->first->transaction_record_to_objects({}, { order => '-desc' })
            ->first;

        # Very defensive programming
        throw("Email/OLO/wrong_transaction", "This isn't a case")
            if $rs->local_table ne 'Zaak';

        my $zaak = $self->schema->resultset('Zaak')->find($rs->local_id);
        $zaak->_touch;

        $self->add_file(
            $xml,
            case_id    => $zaak->id,
            directory  => $self->message->subject,
            accept     => 1,
            created_by => $intake_user,
        );
        return 1;
    }

    my ($mapping) = grep { $_->{external_name} eq 'aanvraagnummer' }  @{ $olo->get_interface_config->{attribute_mapping} };
    my $model = Zaaksysteem::Object::Model->new(schema => $self->schema);
    my $rs = $model->search_rs('case', { 'attribute.' . $mapping->{internal_name}{searchable_object_id} => $lvlnummer });

    if ($rs->count == 1) {
        my $case = $self->schema->resultset('Zaak')->find($rs->first->object_id);
        $self->add_to_case($case);
        return 1;
    }

    throw("Email/OLO/olo/no_case/$lvlnummer/". $rs->count, "Omgevingsloket kon geen zaak aanmaken met LVO id $lvlnummer");
}

sub _get_xml_from_email {
    my $self = shift;
    my @attachments = @{ $self->message->attachments };

    foreach my $part (@attachments) {
        my (undef, undef, $ext) = fileparse($part->path, '\.[^.]*');
        return $part if $ext eq '.xml';
    }
    return undef;
}

=head2 get_message_body

Get the message body, if the body is empty it returns the string C<Zaaksysteem heeft een lege e-mail gedetecteerd>.

=cut

sub get_message_body {
    my $self = shift;

    my $b = $self->message->body->as_text;
    $b =~ tr/\n//s;
    $b =~ s/\s*//g;
    if (!length($b)) {
        return "Zaaksysteem heeft een lege e-mail gedetecteerd";
    }
    return $self->message->body->as_text;
}

=head2 add_file

Adds the e-mail part as a file to Zaaksysteem.

=cut

sub add_file {
    my ($self, $part, %options) = @_;

    my $path = $part->path;
    my ($filename, $dir, $ext) = fileparse($path, '\.[^.]*');

    if ($filename =~ /^msg/) {
        my $is_pdf = 1;
        if ($ext =~ m/^\.html?/i) {
            $path = $self->html_body_to_pdf();
        }
        elsif ($ext =~ /^\.rtf/i) {
            # Outlook messages contain rtf files, we don't want them
            $is_pdf = 0;
        }
        else {
            $path = $self->text_body_to_pdf();
        }
        $filename = $options{directory} if $options{directory};
        $filename .= $is_pdf ? "$ext.pdf" : $ext;
    }
    else {
        $filename = $part->filename;
    }

    return $self->_add_file($path, $filename, %options);
}

=head2 get_intake_user

Returns the intake user for the specific mail.

=cut

sub get_intake_user {
    my $self = shift;

    my ($user) = Email::Address->parse($self->message->to);
    return $user ? $user->user : '';
}


=head2 create_dir_for_mail

Creates a directory for the mail.

=cut

sub create_dir_for_mail {
    my ($self, $case_id, $path) = @_;

    $path ||= $self->message->subject;

    my $dirs = $self->schema->resultset('Directory')->search_rs(
        {
            case_id => $case_id,
            name    => encode_utf8("Email $path"),
        }
    );
    if ($dirs->count) {
        return $dirs->first;
    }
    else {
        return $self->schema->resultset('Directory')->create(
            {
                case_id       => $case_id,
                name          => encode_utf8("Email $path"),
                original_name => encode_utf8("Email $path"),
            }
        );
    }
}

=head2 html_body_to_pdf

Save the HTML body to PDF. Beware, this will not save the headers of the e-mail just the body.

=head3 RETURNS

The path to the PDF file

=cut

sub html_body_to_pdf {
    my $self = shift;
    my $msg  = $self->message;

    return $self->_convert_to_pdf(
        suffix  => '.html',
        content => $msg->body->as_html,
        name    => $msg->subject_raw
    );
}

=head2 text_body_to_pdf

Save the plain text body to PDF. This includes some of the mail headers.

=head3 RETURNS

The path to the PDF file

=cut

sub text_body_to_pdf {
    my $self = shift;
    my $msg  = $self->message;

    my $header = sprintf("To: %s\nFrom: %s\nOnderwerp: %s\nDatum: %s\n",
        $msg->to, $msg->from, $msg->subject_raw, $msg->date);

    return $self->_convert_to_pdf(
        suffix  => '.txt',
        content => encode_utf8($header) . "\n" . $msg->body->as_text,
        name    => $msg->subject_raw
    );

}

=head1 PRIVATE METHODS

=head2 _convert_to_pdf

Convert a file to PDF for mail.

=head3 ARGUMENTS

=over

=item content

The file contents

=item suffix

The source format (.html, .htm, .txt).

=item name

The name of the file

=back

=head3 RETURNS

The path to the PDF file

=cut

sub _convert_to_pdf {
    my ($self, %options) = @_;

    my $fh = File::Temp->new(SUFFIX => $options{suffix}, UNLINK => 1);
    my $path = $fh->filename;
    print $fh ($options{content});
    close($fh);

    my $filestore = $self->schema->resultset('Filestore')->filestore_create(
        {
            file_path     => $path,
            original_name => $options{name} . $options{suffix},
        }
    );

    return $filestore->convert_to_pdf($options{suffix}, $path . ".pdf", 0);

}

=head2 _add_body_as_files

Add the message body as files to a case.

=cut

sub _add_body_as_files {
    my ($self, $case, %options) = @_;

    if (!defined $options{subject}) {
        $options{subject} = $self->message->subject;
    }

    my $filename = $options{subject} .  ".pdf";

    if ($self->message->body->as_html) {
        my $path = $self->html_body_to_pdf();
        $self->_add_file(
            $path,
            "HTML: $filename",
            case_id   => $case->id,
            directory => $options{subject},
            accept    => $options{accept},
        );
    }
    my $path = $self->text_body_to_pdf();
    $self->_add_file(
        $path,
        "Text: $filename",
        case_id   => $case->id,
        directory => $options{subject},
        accept    => $options{accept},
    );
}

=head2 _add_files

Be able to add files via similar ways

=cut

sub _add_file {
    my ($self, $path, $filename, %options) = @_;

    my $args = {
        db_params => {
            $options{intake_owner}
            ? (intake_owner => $options{intake_owner})
            : (),
            created_by => $options{created_by} || 'Onbekend',
            accepted   => $options{accept}     || 0,
        },
        name      => $filename,
        file_path => $path,
    };
    if ($options{case_id}) {
        $args->{db_params}{case_id} = $options{case_id};
        if ($options{directory}) {
            $args->{db_params}{directory_id}
                = $self->create_dir_for_mail($options{case_id},
                $options{directory})->id;
        }
    }
    return $self->schema->resultset('File')->file_create($args);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
