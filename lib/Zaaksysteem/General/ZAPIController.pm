package Zaaksysteem::General::ZAPIController;

use Moose;
use namespace::autoclean;
use Zaaksysteem::Exception;

BEGIN {
    extends 'Catalyst::Controller::ActionRole';
    with 'MooseX::Log::Log4perl';
}

__PACKAGE__->config(
    action_roles    => ['ZAPI', 'Profile'],
    dv_profiles     => {},
    default_view    => 'ZAPI',
);

# sub vprofile {
#     my ($self, $c)          = @_;

#     my $caller  = $self->path_prefix . '.' . [ caller(1) ]->[3];

#     return unless (
#         defined( $c->stash->{_zapi} ) &&
#         defined( $c->stash->{_zapi}->{dv_result} ) &&
#         defined( $c->stash->{_zapi}->{dv_result}->{$caller})
#     );

#     return $c->stash->{_zapi}->{dv_result}->{$caller};
# }

sub begin : Private {
    my ($self, $c) = @_;

    $c->forward('/page/begin');

    $c->stash->{current_view} = 'ZAPI';
}

sub help : Local('help') : ZAPIMan { }

sub crud : Local('crud') {
    my ($self, $c) = @_;

    throw(
        'zapi/crud_not_set',
        'Failed loading CRUD data, make sure this API support CRUD by setting '
        . 'zapi_crud config variable'
    ) unless (exists $self->config->{zapi_crud} && $self->config->{zapi_crud});

    $c->stash->{zapi}               = [$self->config->{zapi_crud}];
    $c->stash->{zapi}->[0]->{url}   = $c->uri_for()->as_string
}


1;




__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 begin

TODO: Fix the POD

=cut

=head2 crud

TODO: Fix the POD

=cut

=head2 help

TODO: Fix the POD

=cut

