package Zaaksysteem::General::Actions;

use File::Spec::Functions qw(catfile);
use Zaaksysteem::Tools;
use Zaaksysteem::Config;
use DateTime;

=head1 NAME

Zaaksysteem::General::Actions - Different actions to call on $c

=head1 SYNOPSIS

  $can_upload = $c->can_upload;

=head1 DESCRIPTION

When you would like to create a method in the L<Zaaksysteem> namespace, please DO NOT place
them directly in L<Zaaksysteem>. It prevents you from testing the method entirely. Instead, place
it in one of the classes subclassed in L<Zaaksysteem::General>, or, much easyer: in this class.

This way they will be all found together.

=head1 METHODS

=head2 load_customer_d_configs

Load the customer configurations of zaaksysteem

=head3 ARGUMENTS

None

=head3 RETURNS

Returns true on success, undef in case of errors.

=cut

sub load_customer_d_configs {
    my $c = shift;

    my $digest = $c->config->{digest_customerd};
    my $digest_cache = $c->cache->get('digest_customerd') || "No digest in cache";

    if ($digest && $digest eq $digest_cache ) {
        return 1;
    }

    $digest = $digest_cache;

    $c->log->debug("Reloading customer.d configurations");

    my $ZS = Zaaksysteem::Config->new(
        zs_customer_d => catfile($c->config->{config_directory}, 'customer.d'),
    );

    my $domains = $ZS->customers;
    $c->customer({});
    for my $host (keys %{$domains}) {
        $c->customer->{$host} = {
            'dbh'          => undef,
            'dbgh'         => undef,
            'start_config' => $domains->{$host},
            'run_config'   => undef,
        };
    }

    $c->config->{customers} = $domains;
    $c->config->{digest_customerd} = $digest;

    return 1;
}

=head2 show_woz

=cut

sub show_woz {
    my $self = shift;

    return $self->model('DB::Interface')->search_active({module => 'woz'})->first
        && $self->check_any_user_permission('woz_objects') ? 1 : 0;
}

=head2 set_zs_version

=cut

sub set_zs_version {
    my ($c, $version) = @_;
    $version =~ s/^v//;
    $version =~ s/^(\d+\.\d+\.\d+)\.(\d+)/$1rc$2/;
    $version =~ s|_|.|;
    $c->config->{ZS_VERSION} = $version;
}


### Basic zaak authorisation
sub _can_change_messages {
    my ($c) = @_;

    return unless $c->user_exists;

    my $case = $c->stash->{ zaak };
    my $case_open_url = $c->uri_for(sprintf('/zaak/%d/open', $case->id));

    if ($case->is_afgehandeld) {
        $c->push_flash_message(
            'U kunt geen wijzigingen aanbrengen aan deze zaak. Deze zaak is afgehandeld'
        );

        return 1;
    }

    if($case->status eq 'new') {
        unless($case->behandelaar) {
            $c->push_flash_message(sprintf(
                'U bent niet de behandelaar van deze zaak. Klik <a href="%s">hier</a> om in behandeling te nemen.',
                $case_open_url->as_string
            ));
        } elsif ($case->behandelaar->gegevens_magazijn_id eq $c->user->uidnumber) {
            $c->push_flash_message(sprintf(
                'U heeft deze zaak nog niet in behandeling genomen. Klik <a href="%s">hier</a> om in behandeling te nemen.',
                $case_open_url->as_string
            ));
        } else {
            $c->push_flash_message('U bent niet de behandelaar van deze zaak.');
        }
    }

    if($case->status eq 'open' || $case->status eq 'stalled') {
        unless($case->behandelaar) {
            $c->push_flash_message(sprintf(
                'U bent niet de behandelaar van deze zaak. Klik <a href="%s">hier</a> om in behandeling te nemen.',
                $case_open_url->as_string
            ));
        } elsif ($c->user_exists && $case->behandelaar->gegevens_magazijn_id ne $c->user->uidnumber) {
            $c->push_flash_message('U bent niet de behandelaar van deze zaak.');
        }
    }

    if ($case->status eq 'stalled') {
        $c->push_flash_message(sprintf(
            '<span class="flash-urgent">Zaak opgeschort: %s</span>',
            $case->reden_opschorten || ''
        ));
    }
}



=head2 $c->can_change

Arguments: none

Return value: true, when a person is allowed to change zaak

Determines wether the current user is allowed to make changes
on the current case.

=cut

sub can_change {
    my $c = shift;

    # This boolflag is basically a bad idea, but I don't want to break
    # the existing callers of this method. Either this thing should assert
    # a consistant state and throw some meaningful exception on failure, *or*
    # it should silently do it's job, without side effects like setting a
    # flash message. TODO
    my $silent = shift;

    my $zaak = $c->stash->{zaak};
    return unless $zaak;

    # only once per request
    unless($c->req->param('_can_change_messages')) {
        $c->req->param('_can_change_messages', 1);
        $c->_can_change_messages;
    }

    return if $zaak->is_afgehandeld;

    ### Zaak beheerders mogen wijzigingen aanbrengen ondanks dat ze geen
    ### behandelaar.
    return 1 if $c->check_any_zaak_permission('zaak_beheer');

    ### Override when we have the correct permissions, and we have a
    ### coordinator and behandelaar
    return 1 if (
        $zaak->behandelaar &&
        $zaak->coordinator &&
        $c->check_any_zaak_permission('zaak_beheer','zaak_edit')
    );

    if (
        !$zaak->behandelaar ||
        !$c->user_exists ||
        !$c->user->uidnumber ||
        (
            $zaak->behandelaar &&
            $zaak->behandelaar->gegevens_magazijn_id ne $c->user->uidnumber &&
            (
                !$zaak->coordinator ||
                $zaak->coordinator->gegevens_magazijn_id ne $c->user->uidnumber
            )
        )
    ){
        $c->push_flash_message(
            'U kunt geen wijzigingen aanbrengen aan deze zaak, u bent geen behandelaar / coordinator.'
        ) unless $silent;

        return;
    }

    return 1;
}

=head2 $c->check_queue_coworker_changes

Arguments: TODO

Return value: TODO

Implements case action 'queue coworker changes'. This makes exclusive editing
rights possible for case behandelaars, all other users will be forced to submit
to their approval. (except admins)

Returns a true value when the case is using that action, and the current user
is not the behandelaar (owner).

=cut

sub check_queue_coworker_changes {
    my $c    = shift;
    my $zaak = $c->stash->{zaak};

    # admins always get what they want, this doesn't need to be queued
    return if $c->check_any_user_permission('admin');

    return unless $c->user;

    return $zaak->check_queue_coworker_changes($c->user->uidnumber);
}



# See:
# http://search.cpan.org/~bobtfish/Catalyst-Plugin-Session-0.35/lib/Catalyst/Plugin/Session.pm
#

=head2 $c->push_flash_message

Arguments: \%MSG | @MSGS

Return value: $flash_message

TODO: needs documenting

Pushes a flash message on the stack and returns the given message.

=cut

sub push_flash_message {
    my $c       = shift;
    my $message;

    if (UNIVERSAL::isa($_[0], 'HASH')) {
        $message    = shift;
    } else {
        $message    = sprintf shift, @_;
    }

    my $result;

    $result = $c->flash->{result} || [];

    # As long as flash->{result} is used without this sub, we need to accomodate
    unless(ref $result && ref $result eq 'ARRAY') {
        $result = [$result];
    } elsif (!ref($result)) {
        $result = {
            message         => $result,
            type            => 'info'
        };
    }

    if (!UNIVERSAL::isa($message, 'HASH')) {
        $message    = {
            message     => $message,
            type        => 'info',
        };
    }

    # Since same code runs multiple times sometimes, check for unicity
    my $lookup = { map { $_->{message} =>  1} grep { UNIVERSAL::isa($_, 'HASH') } @$result };
    unless($lookup->{$message->{message}}) {
        push @$result, $message;
    }

    $c->flash->{result} = $result;

    return $result;
}

=head2 customer_instance

Get the customer instance

=cut

sub customer_instance {
    my $c = shift;

    $c->load_customer_d_configs;

    my $hostname;

    if (!ref($c) && $ENV{ZAAKSYSTEEM_CURRENT_CUSTOMER}) {
        $hostname = $ENV{ZAAKSYSTEEM_CURRENT_CUSTOMER}
    } else {
        $hostname = $c->req->uri->host;
    }

    my $customer = $c->customer->{$hostname};

    if (!$customer && exists $c->config->{default_customer}) {
        $customer = $c->customer->{$c->config->{default_customer}};
    }

    if (!$customer) {
        die("Could not find configuration for hostname '$hostname'");
    }

    if ($customer->{start_config}->{customer_id}) {
        $c->config->{gemeente_id} = $customer->{start_config}->{customer_id}
    }

    $c->config->{logging_id} =
           $customer->{start_config}{logging_id}
        // $customer->{start_config}{customer_id}
        // 'unknown';

    if ($customer->{start_config}->{dropdir}) {
        $c->config->{dropdir} = $customer->{start_config}->{dropdir};
    }

    my $tt_template = $customer->{start_config}->{template} ? $customer->{start_config}->{template} : 'zaak_v1';

    $c->_additional_static([
        $c->config->{root},
        $c->config->{root} . '/tpl/'
            . $tt_template
            . '/' . $c->config->{'View::TT'}->{locale}
    ]);

    $c->config->{static}->{include_path} = $c->_additional_static;

    # Should move to a config table
    if ($customer->{start_config}->{customer_info}) {
        $c->config->{gemeente} = $customer->{start_config}->{customer_info};
    }

    if ($customer->{start_config}->{publish}) {
        $c->config->{publish} = $customer->{start_config}->{publish};
    }

    return $customer;
}

=head2 $c->can_upload

Arguments: none

Return value: true, when a person is allowed to upload in zaak

Will return C<true> when a user is able to upload in the current dossier. It differs from
C<can_change> in the way that people are able to add documents to the document queue when the
dossier is not yet resolved. Even when they are not allowed to alter the dossier.

In short: it will return C<true> when the case is open/stalled or new.

=cut

sub can_upload {
    my ($c) = @_;

    return unless $c->stash->{zaak};

    return 1 if $c->stash->{zaak}->status =~ /^open|stalled|new$/;

    return;
}

=head2 assert_allowed_ip

Assert that the source IP is allowed, by checking it against the provided
network list.  Aborts the request (and redirects to the configured "portal"
page) if it's not allowed.

=cut

sub assert_allowed_ip {
    my ($self, $allowed_ip_list) = @_;

    my $allowed_nets = $allowed_ip_list;

    my $real_ip = $self->req->header('X-Real-IP') || $self->req->address;
    my $client_ip = NetAddr::IP->new($real_ip);

    my $ok;
    for (split m[\s*,\s*], $allowed_ip_list) {
        my $net = NetAddr::IP->new($_);

        unless($net) {
            $self->log->info("Unable to parse range '$_'");
            next;
        }

        $ok = $net->contains($client_ip);
        last if $ok;
    }

    if (!$ok) {
        $self->log->info(sprintf(
            'Client %s not whitelisted, redirecting.',
            $self->req->address
        ));

        $self->detach('/forbidden');
    }

    return;
}

=head2 get_saml_session_timeout

Get the ammount of milliseconds left before the end of the SAML session.

=head3 RETURNS

Any number of milliseconds left before the session is expired.
0 if there is no SAML session or if it is expired.

=cut

sub get_saml_session_timeout {
    my $self = shift;

    return 0 unless $self->check_saml_session;
    return $self->_get_session_diff($self->session->{__expire_keys}{_saml});
}

=head2 extend_saml_session_timeout

Extend the SAML session with 15 minutes.

=head3 RETURNS

1 if the session is extended, 0 if not.

=cut

sub extend_saml_session_timeout {
    my $self = shift;

    if ($self->get_saml_session_timeout) {
        $self->session_expire_key('_saml' => '900');
        return 1;
    }
    return 0;

}

sub _get_session_diff {
    my ($self, $expires) = @_;

    my $diff = DateTime->from_epoch(epoch => $expires) - DateTime->now();

    return 0 if !$diff->is_positive;
    return ( ( ( ( $diff->hours * 60 ) + $diff->minutes ) * 60 ) + $diff->seconds ) * 1000;

}

=head2 get_session_timeout

Get the ammount of milliseconds left before the end of the session.

=head3 RETURNS

Any number of milliseconds left before the session is expired.
0 if there is no session or if it is expired.

=cut

sub get_session_timeout {
    my $self = shift;
    return $self->_get_session_diff($self->session_expires);
}

=head2 extend_session_timeout

Extend the session with two hours

=head3 RETURNS

1 if the session is extended, 0 if not.

=cut

sub extend_session_timeout {
    my $self = shift;

    if ($self->get_session_timeout) {
        $self->extend_session_expires('7200');
        return 1;
    }
    return 0;

}

=head2 check_saml_session

Soft fail for saml sessions. If we don't have a expire key for saml

=cut

sub check_saml_session {
    my $self = shift;

    if ($self->user_exists || !exists $self->session->{__expire_keys}{_saml}) {
        return 0;
    }
    return 1;
}

=head2 return_error

Set error status 500 and return the specified error as formatted by format_error().

=cut

sub return_error {
    my $c = shift;
    $c->res->status(500);
    return $c->format_error(@_)
}

=head2 format_error

Takes an exception object and formats it for a JSON response.

=cut

sub format_error {
    my $c = shift;
    my $error = @_;
    my $error_ref   = ref $error;

    # Pretty Exception::Class errors
    if (eval { $error->isa('Exception::Class::Base') }) {
        my @trace_args;

        # Stracktraces can be -huge-, only add them in developer mode.
        if ($c->debug) {
            @trace_args = map {
                {
                    package    => $_->package,
                    line       => $_->line,
                    filename   => $_->filename,
                    subroutine => $_->subroutine,
                    args_passed_to_sub => [$_->args],
                }
            } $error->trace->frames;
        }

        $c->log->error('Global exception caught', $error->as_string);

        return {
            stacktrace      => \@trace_args,
            debug           => $c->debug,
            body_parameters => $c->req->body_parameters,
            error_code      => $error->code || 'unhandled',
            category        => $error_ref,
            messages        => [$error->as_string],
        };
    } elsif (eval { $error->isa('Zaaksysteem::Exception::Base') }) {
        my $formatter = sub {
            my $frame = shift;

            return {
                package => $frame->package,
                filename => $frame->filename,
                line => $frame->line,
                subroutine => $frame->subroutine,
                args_passwd_to_sub => []
            };
        };

        $c->log->error('Global exception caught', $error->TO_STRING);

        my @trace_args;

        if($c->debug) {
            @trace_args = map { $formatter->($_) } $error->stack_trace->frames;
            $c->log->debug('Stacktrace dump', $error->stack_trace->as_string);
        }

        return {
            stacktrace      => \@trace_args,
            debug           => $c->debug,
            body_parameters => $c->req->body_parameters,
            error_code      => 500,
            category        => ref $error,
            messages        => [ $error->message ]
        };
    }
    # Non-Exception::Class errors - i.e. regular die's
    else {
        $c->log->error('Global exception caught', burp($error));

        return {
            debug           => $c->debug,
            body_parameters => [$c->req->body_parameters],
            error_code      => 'unhandled',
            category        => sprintf("%s", $c->action),
            messages        => burp($error),
            stacktrace      => 'Not (yet) available for non-exception errors',
        };
    }
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Install>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
