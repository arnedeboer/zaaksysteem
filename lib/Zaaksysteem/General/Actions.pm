package Zaaksysteem::General::Actions;
use strict;
use warnings;

use File::Spec::Functions qw(catfile);
use Zaaksysteem::Tools;
use Zaaksysteem::Config;
use DateTime;
use URI;

=head1 NAME

Zaaksysteem::General::Actions - Different actions to call on $c

=head1 SYNOPSIS

  $can_upload = $c->can_upload;

=head1 DESCRIPTION

When you would like to create a method in the L<Zaaksysteem> namespace, please DO NOT place
them directly in L<Zaaksysteem>. It prevents you from testing the method entirely. Instead, place
it in one of the classes subclassed in L<Zaaksysteem::General>, or, much easyer: in this class.

This way they will be all found together.

=head1 METHODS

=head2 load_customer_d_configs

Load the customer configurations of zaaksysteem

=head3 ARGUMENTS

None

=head3 RETURNS

Returns true on success, undef in case of errors.

=cut

sub _check_digest {
    my $c = shift;

    my $digest = $c->config->{digest_customerd} // '';
    my $digest_cache = $c->cache->get('digest_customerd') // 'No digest in cache';

    if ($digest eq $digest_cache ) {
        return 0;
    }
    return $digest_cache;
}

sub load_customer_d_configs {
    my $c = shift;

    my $digest = $c->_check_digest();
    return if !$digest;

    $c->log->debug("Reloading customer.d configurations");

    my $ZS = Zaaksysteem::Config->new(
        zs_customer_d => catfile($c->config->{config_directory}, 'customer.d'),
    );

    try {
        my $domains = $ZS->customers;
        $c->customer({});
        for my $host (keys %{$domains}) {
            $c->customer->{$host} = {
                'dbh'          => undef,
                'dbgh'         => undef,
                'start_config' => $domains->{$host},
                'run_config'   => undef,
            };
        }
        $c->config->{customers} = $domains;
        $c->config->{digest_customerd} = $digest;
    }
    catch {
        $c->log->error("Unable to load customer.d configurations: $_");
        $c->config->{customers} //= {};
    };

    return 1;
}

=head2 show_woz

Show WOZ boolean

=cut

sub show_woz {
    my $self = shift;

    return $self->model('DB::Interface')->search_active({module => 'woz'})->first
        && $self->check_any_user_permission('woz_objects') ? 1 : 0;
}

=head2 show_show_alternative_authentication

Show Alternative authentication boolean for the contactdossier

=cut


sub show_alternative_authentication {
    my $self = shift;
    return $self->model('DB::Interface')->search_active({module => 'auth_twofactor'})->first;
}

=head2 set_zs_version

=cut

sub set_zs_version {
    my ($c, $version) = @_;
    $version =~ s/^v//;
    $version =~ s/^(\d+\.\d+\.\d+)\.(\d+)/$1rc$2/;
    $version =~ s|_|.|;
    $c->config->{ZS_VERSION} = $version;
}


### Basic zaak authorisation
sub _can_change_messages {
    my ($c) = @_;

    return unless $c->user_exists;

    my $case = $c->stash->{ zaak };
    my $case_open_url = $c->uri_for(sprintf('/zaak/%d/open', $case->id));

    if ($case->is_afgehandeld) {
        $c->push_flash_message(
            'U kunt geen wijzigingen aanbrengen aan deze zaak. Deze zaak is afgehandeld'
        );

        return 1;
    }

    if ($case->status eq 'new') {
        unless($case->behandelaar) {
            $c->push_flash_message(sprintf(
                'U bent niet de behandelaar van deze zaak. Klik <a href="%s">hier</a> om in behandeling te nemen.',
                $case_open_url->as_string
            ));
        } elsif ($case->behandelaar->gegevens_magazijn_id eq $c->user->uidnumber) {
            $c->push_flash_message(sprintf(
                'U heeft deze zaak nog niet in behandeling genomen. Klik <a href="%s">hier</a> om in behandeling te nemen.',
                $case_open_url->as_string
            ));
        } else {
            $c->push_flash_message('U bent niet de behandelaar van deze zaak.');
        }
        return 1;
    }

    if ($case->status eq 'stalled') {
        my $event = $case->logging->search_rs({event_type => "case/suspend"}, { order_by => { -desc => 'id' }, rows => 1})->first;
        $c->push_flash_message($event->onderwerp);
    }

    # I don't want the case to be reopened without hitting "hervatten" in the
    # GUI. Because "hervatten" has some logic which open does not have.
    # Only open cases can directly assign a case to self.
    if ($case->status eq 'open' && !$case->behandelaar) {
        $c->push_flash_message(sprintf(
            'U bent niet de behandelaar van deze zaak. Klik <a href="%s">hier</a> om in behandeling te nemen.',
            $case_open_url->as_string
        ));
    } elsif (!$case->behandelaar || $c->user_exists && $case->behandelaar->gegevens_magazijn_id ne $c->user->uidnumber) {
        $c->push_flash_message('U bent niet de behandelaar van deze zaak.');
    }

}



=head2 $c->can_change

Arguments: none

Return value: true, when a person is allowed to change zaak

Determines wether the current user is allowed to make changes
on the current case.

=cut

sub can_change {
    my $c = shift;

    # This boolflag is basically a bad idea, but I don't want to break
    # the existing callers of this method. Either this thing should assert
    # a consistant state and throw some meaningful exception on failure, *or*
    # it should silently do it's job, without side effects like setting a
    # flash message. TODO
    my $silent = shift;

    my $zaak = $c->stash->{zaak};
    return unless $zaak;

    # only once per request
    unless($c->req->param('_can_change_messages')) {
        $c->req->param('_can_change_messages', 1);
        $c->_can_change_messages;
    }

    return if $zaak->is_afgehandeld;

    ### Zaak beheerders mogen wijzigingen aanbrengen ondanks dat ze geen
    ### behandelaar.
    return 1 if $c->check_any_zaak_permission('zaak_beheer');

    ### Override when we have the correct permissions, and we have a
    ### coordinator and behandelaar
    return 1 if (
        $zaak->behandelaar &&
        $zaak->coordinator &&
        $c->check_any_zaak_permission('zaak_beheer','zaak_edit')
    );

    if (
        !$zaak->behandelaar ||
        !$c->user_exists ||
        !$c->user->uidnumber ||
        (
            $zaak->behandelaar &&
            $zaak->behandelaar->gegevens_magazijn_id ne $c->user->uidnumber &&
            (
                !$zaak->coordinator ||
                $zaak->coordinator->gegevens_magazijn_id ne $c->user->uidnumber
            )
        )
    ){
        $c->push_flash_message(
            'U kunt geen wijzigingen aanbrengen aan deze zaak, u bent geen behandelaar / coordinator.'
        ) unless $silent;

        return;
    }

    return 1;
}

=head2 $c->check_queue_coworker_changes

Arguments: TODO

Return value: TODO

Implements case action 'queue coworker changes'. This makes exclusive editing
rights possible for case behandelaars, all other users will be forced to submit
to their approval. (except admins)

Returns a true value when the case is using that action, and the current user
is not the behandelaar (owner).

=cut

sub check_queue_coworker_changes {
    my $c    = shift;
    my $zaak = $c->stash->{zaak};

    # admins always get what they want, this doesn't need to be queued
    return if $c->check_any_user_permission('admin');

    return unless $c->user;

    return $zaak->check_queue_coworker_changes($c->user->uidnumber);
}

=head2 has_client_type

Returns true if the current request has the C<X-Client-Type> header.

=cut

sub has_client_type {
    my $c = shift;
    if(my $type = $c->req->header('X-Client-Type')) {
        $c->log->debug("Client reported X-Client-Type of '$type'");
        return 1;
    }
    return 0;

}

# See:
# http://search.cpan.org/~bobtfish/Catalyst-Plugin-Session-0.35/lib/Catalyst/Plugin/Session.pm
#

=head2 push_flash_message

Arguments: \%MSG | @MSGS

Return value: $flash_message

TODO: needs documenting

Pushes a flash message on the stack and returns the given message.

=cut

sub push_flash_message {
    my $c       = shift;
    my $message;

    if ($c->has_client_type) {
        return;
    }

    if (UNIVERSAL::isa($_[0], 'HASH')) {
        $message    = shift;
    } else {
        $message    = sprintf shift, @_;
    }

    my $result = $c->flash->{result} || [];

    # As long as flash->{result} is used without this sub, we need to accomodate
    unless(ref $result && ref $result eq 'ARRAY') {
        $result = [$result];
    } elsif (!ref($result)) {
        $result = {
            message         => $result,
            type            => 'info'
        };
    }

    if (!UNIVERSAL::isa($message, 'HASH')) {
        $message    = {
            message     => $message,
            type        => 'info',
            ## Debugging information
            date        => time(),
        };
    }

    # Since same code runs multiple times sometimes, check for unicity
    my $lookup = { map { $_->{message} =>  1} grep { UNIVERSAL::isa($_, 'HASH') } @$result };
    unless($lookup->{$message->{message}}) {
        push @$result, $message;
    }

    $c->flash->{result} = $result;
    return $result;
}

=head2 customer_instance

Get the customer instance

=cut

sub customer_instance {
    my $c = shift;

    $c->load_customer_d_configs;

    my $hostname;

    if (!ref($c) && $ENV{ZAAKSYSTEEM_CURRENT_CUSTOMER}) {
        $hostname = $ENV{ZAAKSYSTEEM_CURRENT_CUSTOMER}
    } else {
        $hostname = $c->req->uri->host;
    }

    my $customer = $c->customer->{$hostname};

    if (!$customer && exists $c->config->{default_customer}) {
        $customer = $c->customer->{$c->config->{default_customer}};
    }

    if (!$customer) {
        my $msg = "Could not find configuration for hostname '$hostname'";
        $c->log->fatal($msg);
        die($msg);
    }

    if ($customer->{start_config}{customer_id}) {
        $c->config->{gemeente_id} = $customer->{start_config}{customer_id}
    }

    $c->config->{services_base} = $customer->{start_config}{services_base}
        // 'https://unconfigured.services.zaaksysteem.nl';

    $c->config->{instance_uuid} = $customer->{start_config}{instance_uuid};
    $c->config->{logging_id} =
           $customer->{start_config}{logging_id}
        // $customer->{start_config}{customer_id}
        // 'unknown';

    if ($customer->{start_config}{dropdir}) {
        $c->config->{dropdir} = $customer->{start_config}{dropdir};
    }

    my $tt_template = $customer->{start_config}->{template} ? $customer->{start_config}->{template} : 'zaak_v1';

    $c->_additional_static([
        $c->config->{root},
        $c->config->{root} . '/tpl/'
            . $tt_template
            . '/' . $c->config->{'View::TT'}{locale}
    ]);

    $c->config->{static}{include_path} = $c->_additional_static;

    if ($customer->{start_config}->{customer_info}) {
        $c->config->{gemeente} = $customer->{start_config}{customer_info};
    }

    # ZS-11433: Override dev flag on customer instance
    if (defined $customer->{start_config}{dev}) {
        $c->config->{dev} = $customer->{start_config}{dev};
    }

    return $customer;
}

=head2 get_customer_info

Get the customer information from the database, or get it from the customerd file.
If the information is not present in the database we insert it into the database.
Customers can later change all the relevant information.
If nothing is found, insert some defaults so you have a working solution.

=cut

sub get_customer_info {
    my $self = shift;

    my $config = $self->model('DB')->schema->resultset('Config')->get_customer_config;
    if ($config && %$config) {
        $self->config->{gemeente} = $config;
        return $config;
    }
    else {
        my $config = $self->config->{gemeente} // {
            naam             => "Zaaksysteem by Mintlab",
            naam_lang        => "Mintlab B.V.",
            naam_kort        => "Mintlab",
            woonplaats       => "Amsterdam",
            adres            => "Donker Curtiusstraat 7 - 521",
            straatnaam       => "Donker Curtiusstraat",
            huisnummer       => "7-521",
            postcode         => "1051 JL",
            postbus          => "",
            postbus_postcode => "",
            website          => "http://www.mintlab.nl/",
            email            => "servicedesk\@mintlab.nl",
            telefoonnummer   => "020 - 737 000 5",
            faxnummer        => "",
            zaak_email       => "no-reply\@zaaksysteem.nl",
            latitude         => "52.378979",
            longitude        => "4.871620",
            gemeente_id_url  => "http://www.mintlab.nl/",
            gemeente_portal  => "http://www.mintlab.nl/",
        };

        foreach (keys %{$config}) {
            $self->model('DB::Config')->create(
                {
                    parameter => "customer_info_$_",
                    value     => $config->{$_},
                    advanced  => 0,
                }
            );
        }
        return $config;
    }
}

=head2 $c->can_upload

Arguments: none

Return value: true, when a person is allowed to upload in zaak

Will return C<true> when a user is able to upload in the current dossier. It differs from
C<can_change> in the way that people are able to add documents to the document queue when the
dossier is not yet resolved. Even when they are not allowed to alter the dossier.

In short: it will return C<true> when the case is open/stalled or new.

=cut

sub can_upload {
    my ($c) = @_;

    return unless $c->stash->{zaak};

    return 1 if $c->stash->{zaak}->status =~ /^open|stalled|new$/;

    return;
}

=head2 get_client_ip

Assert that the source IP is allowed, by checking it against the provided
network list.  Aborts the request (and redirects to the configured "portal"
page) if it's not allowed.

=cut

sub get_client_ip {
    my $self = shift;

    my $real_ip;
    if ($real_ip = $self->req->header('X-Real-IP')) {
        $self->log->debug("Found X-Real-IP");
    }
    elsif ($real_ip = $self->req->header('X-Forwarded-For')) {
        $self->log->debug("Found X-Forwarded-For");
        # Strip everything after the first comma.
        $real_ip =~ s/,.*//;
    }
    else {
        $self->log->debug("Original request address");
        $real_ip = $self->req->address;
    }
    $self->log->debug("Got IP: $real_ip");

    return $real_ip;

}


=head2 assert_allowed_ip

Assert that the source IP is allowed, by checking it against the provided
network list.  Aborts the request (and redirects to the configured "portal"
page) if it's not allowed.

=cut

sub assert_allowed_ip {
    my ($self, $allowed_ip_list) = @_;

    my $allowed_nets = $allowed_ip_list;

    my $real_ip   = $self->get_client_ip;
    my $client_ip = NetAddr::IP->new($real_ip);

    for (split m[\s*,\s*], $allowed_ip_list) {
        my $net = NetAddr::IP->new($_);

        unless($net) {
            $self->log->warn("Unable to parse range '$_'");
            next;
        }

        return 1 if $net->contains($client_ip);
    }

    $self->log->warn(sprintf(
        'Client %s not whitelisted, redirecting.',
        $real_ip
    ));

    $self->detach('/forbidden');
}

=head2 get_saml_session_timeout

Get the ammount of milliseconds left before the end of the SAML session.

=head3 RETURNS

Any number of milliseconds left before the session is expired.
0 if there is no SAML session or if it is expired.

=cut

sub get_saml_session_timeout {
    my $self = shift;

    return 0 unless $self->check_saml_session;
    return $self->_get_session_diff($self->session->{__expire_keys}{_saml});
}

=head2 extend_saml_session_timeout

Extend the SAML session with 15 minutes.

=head3 RETURNS

1 if the session is extended, 0 if not.

=cut

sub extend_saml_session_timeout {
    my $self = shift;

    if ($self->get_saml_session_timeout) {
        $self->session_expire_key('_saml' => '900');
        return 1;
    }
    return 0;

}

sub _get_session_diff {
    my ($self, $expires) = @_;

    my $diff = DateTime->from_epoch(epoch => $expires) - DateTime->now();

    return 0 if !$diff->is_positive;
    return ( ( ( ( $diff->hours * 60 ) + $diff->minutes ) * 60 ) + $diff->seconds ) * 1000;

}

=head2 get_session_timeout

Get the ammount of milliseconds left before the end of the session.

=head3 RETURNS

Any number of milliseconds left before the session is expired.
0 if there is no session or if it is expired.

=cut

sub get_session_timeout {
    my $self = shift;
    return $self->_get_session_diff($self->session_expires);
}

=head2 extend_session_timeout

Extend the session with two hours

=head3 RETURNS

1 if the session is extended, 0 if not.

=cut

sub extend_session_timeout {
    my $self = shift;

    if ($self->get_session_timeout) {
        $self->extend_session_expires('7200');
        return 1;
    }
    return 0;

}

=head2 check_saml_session

Soft fail for saml sessions. If we don't have a expire key for saml

=cut

sub check_saml_session {
    my $self = shift;

    if ($self->user_exists || !exists $self->session->{__expire_keys}{_saml}) {
        return 0;
    }
    return 1;
}

=head2 return_error

Set error status 500 and return the specified error as formatted by format_error().

=cut

sub return_error {
    my $c = shift;
    $c->res->status(500);
    return $c->format_error(@_)
}

=head2 process_error

Takes an exception and returns a formatted variant, while logging the message
and setting the status of the response to C<500>.

=cut

sub process_error {
    my $c = shift;
    my $error = shift;

    $c->log->warn($error);
    $c->res->code(500);

    return $c->format_error($error);
}

=head2 format_error

Takes an exception object and formats it for a JSON response.

=cut

sub format_error {
    my $c = shift;
    my $error = shift;
    my $error_ref = ref $error;

    # Pretty Exception::Class errors
    if (eval { $error->isa('Exception::Class::Base') }) {
        my @trace_args;

        # Stracktraces can be -huge-, only add them in developer mode.
        if ($c->debug) {
            @trace_args = map {
                {
                    package    => $_->package,
                    line       => $_->line,
                    filename   => $_->filename,
                    subroutine => $_->subroutine,
                    args_passed_to_sub => [$_->args],
                }
            } $error->trace->frames;
        }

        $c->log->error(sprintf('Global exception caught: %s', $error->as_string));

        return {
            stacktrace      => \@trace_args,
            debug           => $c->debug,
            body_parameters => $c->req->body_parameters,
            error_code      => $error->code || 'unhandled',
            category        => $error_ref,
            messages        => [$error->as_string],
        };
    } elsif (eval { $error->isa('Zaaksysteem::Exception::Base') }) {
        my $formatter = sub {
            my $frame = shift;

            return {
                package => $frame->package,
                filename => $frame->filename,
                line => $frame->line,
                subroutine => $frame->subroutine,
                args_passwd_to_sub => []
            };
        };

        $c->log->error(sprintf('Global exception caught: %s', $error->TO_STRING));

        my @trace_args;

        if($c->debug) {
            @trace_args = map { $formatter->($_) } $error->stack_trace->frames;
            $c->log->debug($error->stack_trace->as_string);
        }

        return {
            stacktrace      => \@trace_args,
            debug           => $c->debug,
            body_parameters => $c->req->body_parameters,
            error_code      => 500,
            category        => ref $error,
            messages        => [ $error->message ]
        };
    }
    # Non-Exception::Class errors - i.e. regular die's
    else {
        $c->log->error(sprintf('Global exception caught: %s', burp($error)));

        return {
            debug           => $c->debug,
            body_parameters => [$c->req->body_parameters],
            error_code      => 'unhandled',
            category        => sprintf("%s", $c->action),
            messages        => burp($error),
            stacktrace      => 'Not (yet) available for non-exception errors',
        };
    }
}

=head2 init_log4perl

Initializes the L<Log::Log4perl::Catalyst> instance for the current request.

Will attempt to load C</etc/zaaksysteem/log4perl.conf>,
C</vagrant/etc/log4perl.custom.conf>, C</vagrant/etc/log4perl.conf>.

This behavior can be overridden via the C<Log4perl> config item in the main
configuration file.

=cut

sub init_log4perl {
    my $self = shift;

    if (!$self->config->{Log4perl}) {
        foreach (qw(/etc/zaaksysteem/log4perl.conf /vagrant/etc/log4perl.custom.conf /vagrant/etc/log4perl.conf)) {
            next if (!-e $_);
            $self->log(Log::Log4perl::Catalyst->new($_));
            $self->log->warn("Loaded $_. Please set Log4perl in your configuration file");
            return;
        }
        return;
    }

    $self->log(
        Log::Log4perl::Catalyst->new(
            $self->config->{Log4perl}{file},
            %{$self->config->{Log4perl}{options}}
        )
    );
    $self->log->info(sprintf("Loaded %s from configuration file", $self->config->{Log4perl}{file}));
}

=head2 load_xml_compile_classes

Initializes L</xml_compile> with known classes, unless the instance is
configured to forgo early initialization via the
C<disable_xml_compile_preload> configuration key, or the
C<ZS_DISABLE_STUF_PRELOAD> environment variable.

=cut

sub load_xml_compile_classes {
    my $self = shift;

    if ($self->config->{disable_xml_compile_preload} || $ENV{ZS_DISABLE_STUF_PRELOAD}) {
        $self->log->info("Skipping preloading of XML compile classes");
        return 0;
    }

    $self->xml_compile->add_class([qw/
        Zaaksysteem::StUF::0204::Instance
        Zaaksysteem::StUF::0301::Instance
        Zaaksysteem::StUF::0312::Instance
        Zaaksysteem::XML::MijnOverheid::Instance
        Zaaksysteem::XML::Xential::Instance
        Zaaksysteem::XML::Zaaksysteem::Instance

        Zaaksysteem::XML::Generator::StUF0310
    /]);

    return 1;
}

=head2 set_referer

Set the referer in a safe way so we don't go outside our own domain.
If the referer starts with a C</> we treat this as within out own domain.
All the query parameters are used to redirect if the hostname equals to our own host, otherwise you will be redirected to C</>.

=cut

sub set_referer {
    my ($self, $uri) = @_;

    return unless $uri;

    my $req_host = lc($self->req->uri->host);

    if ($uri =~ /^\//) {
        $uri = URI->new_abs($uri, "https://$req_host");
    }
    else {
        $uri = URI->new($uri);
    }

    if (($uri->scheme //'') ne 'https') {
        $self->log->warn("URI scheme is not 'https'");
        return $self->uri_for('/');
    }

    my $host = lc($uri->host);
    if ($req_host eq $host) {
        return $self->uri_for($uri->path_query);
    }
    else {
        $self->log->warn("Referer host '$host' does not match '$req_host', redirecting to /");
        return $self->uri_for('/');
    }
}

=head2 get_zs_session_id

Returns the session id

=cut

sub get_zs_session_id {
    my $self = shift;
    return $self->stash->{request_id};
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Install>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
