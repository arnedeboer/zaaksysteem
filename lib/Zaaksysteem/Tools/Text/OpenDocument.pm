package Zaaksysteem::Tools::Text::OpenDocument;

use Moose;

use Zaaksysteem::Tools;

use OpenOffice::OODoc;

=head1 NAME

Zaaksysteem::Tools::Text::OpenDocument - Provide a plaintext interface to
OpenDocument files

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head2 file

This attribute should hold the path to an OpenDocument file to be converted.

=cut

has file => (
    is => 'rw',
    isa => 'Str',
    required => 1
);

=head2 plaintext

=head3 Interface

=over 4

=item has_plaintext

Value predicate for this attribute.

=item build_plaintext

Our implementation of this attribute's value.

=back

=cut

has plaintext => (
    is => 'rw',
    isa => 'Str',
    predicate => 'has_plaintext',
    builder => 'build_plaintext'
);

=head1 BUILDERS

=head2 build_plaintext

Default value builder implementation for the L</plaintext> attribute.

=cut

sub build_plaintext {
    my $self = shift;

    my $doc = odfText(file => $self->file);

    return $doc->getTextContent;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
