package Zaaksysteem::EZSearch;

use Class::MOP;
use Moose;
use Moose::Util::TypeConstraints;

use Zaaksysteem::Tools;
use Zaaksysteem::EZSearch::Result::Row;

use constant SEARCH_TYPES => {
    casetype    => 'Zaaksysteem::EZSearch::Casetype',
};

with ('MooseX::Log::Log4perl', values %{ SEARCH_TYPES() });

=head1 NAME

Zaaksysteem::EZSearch - Elastic Zaaksysteem search, which is a first step towards external searching in ElasticSearch

=head1 TESTS

Tests can be found in L<TestFor::General::EZSearch>, proven by

    ./zs_prove -v t/lib/TestFor/General/EZSearch.pm

=head1 DESCRIPTION

Module which handles simple searching, and is a replacement for objectsearch. In the future, we will move to an
external solution for our search requests. In the meantime, we create an API, to prevent force us to work
in one simple way.

=head1 ATTRIBUTES

=head2 schema

Type: L<Zaaksysteem::Schema>

This module needs to be able to query the database

=cut

has 'schema'    => (
    is          => 'ro',
    isa         => 'Zaaksysteem::Schema',
    required    => 1,
);

=head2 query

Type: String

The query to search for

=cut

has 'query'     => (
    is          => 'rw',
    isa         => 'Str',
);

=head2 search_type

Type: Str

The main type to search within

=cut

has 'search_type' => (
    is          => 'rw',
    isa         => subtype('Str' => where { SEARCH_TYPES->{$_} }),
    required    => 1,
);

=head2 filters

Type: HashRef

Filter the search results

=cut

has 'filters'   => (
    is          => 'rw',
    isa         => 'HashRef',
    default     => sub { {}; },
    lazy        => 1,
);

=head2 max_results_per_type

Type: Int
Default: 10

The maximum results per type to request

=cut

has 'max_results_per_type'     => (
    is          => 'rw',
    isa         => 'Int',
    default     => 10,
);

=head1 METHODS

=head2 search

Returns search results when a query is given. The query must be at least 3 characters.

=cut

sub search {
    my $self        = shift;
    my $query       = shift;

    $self->query($query) if $query;

    throw('ZS/EZSearch/no_query_given', 'Cannot initiate a search without query set') unless $self->query;

    ### Ram protection, do not show any results when amount of characters is below 3.
    if ($self->query && length $self->query >= 3) {
        return [ $self->_search() ];
    } else {
        $self->log->info('Length of query is lower than 3, returning 0 results');
    }

    return [];
}

=head1 PRIVATE METHODS

=head2 _search

Handles the actual searching, after parameters and input are checked. Around modifiers on the different roles
can hook into this function to add their results.

=cut

sub _search { return (); }

=head2 _add_result

Adds a result to the resultset

=cut

sub _add_result {
    my $self        = shift;
    my %opts        = @_;

    Zaaksysteem::EZSearch::Result::Row->new(%opts);
}

__PACKAGE__->meta->make_immutable;


__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
