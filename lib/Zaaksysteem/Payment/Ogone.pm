package Zaaksysteem::Payment::Ogone;
use Moose;
use Moose::Util::TypeConstraints;

use Digest::SHA qw/sha1_hex sha512_hex/;
use LWP::UserAgent;
use Text::Unidecode qw[unidecode];
use URI::QueryParam;
use URI;
use Zaaksysteem::Tools;
use Zaaksysteem::Types qw(PositiveNumber);
use Zaaksysteem::ZTT;

my %PUBLIC_ATTR = (
    ### Variable
    orderid     => 'orderID',
    amount      => 'amount',
    pspid       => 'PSPID',
    shasign     => 'SHASign',
    com         => 'COM',

    ### Static
    'language' => 'language',
    'currency' => 'currency',
    'accepturl' => 'accepturl',
    'declineurl' => 'declineurl',
    'exceptionurl' => 'exceptionurl',
    'cancelurl' => 'cancelurl',
    'backurl' => 'backurl',
    'homeurl' => 'homeurl',
);

my @SHA1_ATTR   = (
    'amount',
    'currency',
    'orderid',
);

use constant OGONE_SERVER_DATA => {
    TEST => {
        'posturl'   => 'https://secure.ogone.com/ncol/test/orderstandard.asp',
        #'posturl'   => 'http://dev.zaaksysteem.nl:3000/plugins/ogone/test',
        'accepturl' =>
            'plugins/ogone/api/accept',
        'declineurl' =>
            'plugins/ogone/api/decline',
        'exceptionurl' =>
            'plugins/ogone/api/exception',
        'cancelurl' =>
            'plugins/ogone/api/cancel',
        'layout'    => {},
        'language'  => 'nl_NL',
        'currency'  => 'EUR',
    },
    PROD => {
        'posturl'   => 'https://secure.ogone.com/ncol/prod/orderstandard.asp',
        #'posturl'   => 'http://dev.zaaksysteem.nl:3000/plugins/ogone/test',
        'accepturl' =>
            'plugins/ogone/api/accept',
        'declineurl' =>
            'plugins/ogone/api/decline',
        'exceptionurl' =>
            'plugins/ogone/api/exception',
        'cancelurl' =>
            'plugins/ogone/api/cancel',
        'layout'    => {},
        'language'  => 'nl_NL',
        'currency'  => 'EUR',
    },
};

### OGONE PUBLIC VARS
has [ keys %PUBLIC_ATTR ] => (
    is      => 'rw',
);

### OGONE STATIC VARS
has [qw/variables zaak layout posturl shasign succes status verified baseurl/] => (
    is      => 'rw'
);

### Defaults for model
has [qw/log session params/] => (
    is      => 'rw',
);

has 'prod'  => (
    'is'        => 'rw',
);

has 'dummy' => (
    'is'        => 'rw',
);

has [qw/shapass shapassout hash_algorithm order_description_template/] => (
    'is' => 'ro',
);

has hash_algorithm => (
    is      => 'rw',
    isa     => enum([qw(sha1 sha512)]),
    default => 'sha1',
);

define_profile start_payment => (
    required => {
        amount => PositiveNumber,
        zaak   => 'Zaaksysteem::Schema::Zaak',
    }
);

sub start_payment {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    $self->clear_payment;

    $self->amount($opts->{amount});
    $self->zaak($opts->{zaak});

    my $ztt = Zaaksysteem::ZTT->new->add_context($self->zaak);

    $self->com(substr(
        unidecode($ztt->process_template($self->order_description_template)->string),
        0,
        100
    ));

    ### URLS
    {
        my $prod_test_data  = OGONE_SERVER_DATA;

        my $prodortest  = 'TEST';
        $prodortest     = 'PROD' if $self->prod;

        while (my ($target, $data) = each %{ $prod_test_data->{$prodortest} }) {
            if ($target =~ /url/ && $data !~ /http/) {
                $self->$target($self->baseurl . $data);
                next;
            }
            $self->$target($data);
        }
    }

    $self->orderid(time() . 'z' . $self->zaak->id);

    my $shapass = $self->{shapass};

    my ($field, $logfield) = ('','');
    for my $key (sort keys %PUBLIC_ATTR) {
        next if ($self->$key // '') eq '';
        $logfield .= uc($key) . '=' . $self->$key;
        $field .= uc($key) . '=' . $self->$key . $shapass;
    }

    my ($digest, $digestlog) = $self->_hash_digest_and_log(
        shatype   => $self->hash_algorithm,
        digest    => $field,
        digestlog => $logfield,
    );

    $self->shasign($digest);
    $self->variables(\%PUBLIC_ATTR);
}

sub _hash_digest_and_log {
    my ($self, %options) = @_;

    no strict 'refs';
    my $shafunc = $self->hash_algorithm . '_hex';

    my $digest    = uc($shafunc->($options{digest}));
    my $digestlog = uc($shafunc->($options{digestlog}));

    $self->log->debug("Hashed digest: '$digest'");
    $self->log->debug("Hashed digest log: '$digestlog'");

    return ($digest, $digestlog);
}

sub verify_payment {
    my ($self, %opt) = @_;

    $self->clear_payment;

    my $shapassout = $self->shapassout;
    my $shagiven   = delete $opt{SHASIGN};

    my %fields;
    $fields{uc($_)} = $opt{$_} for keys %opt;

    my ($field, $logfield) = ('','');
    for my $key (sort keys %fields) {
        my $value   = $fields{$key};
        next if ($value // '') eq '';

        $field      .= uc($key) . '=' . $value . $shapassout;
        $logfield   .= uc($key) . '=' . $value;
    }

    my ($digest, $digestlog) = $self->_hash_digest_and_log(
        digest    => $field,
        digestlog => $logfield,
    );

    ### Fill information
    for my $key (keys %fields) {
        my $lckey   = lc($key);
        next unless $self->can($lckey);

        $self->$lckey($fields{$key});
    }
    $self->log->debug( "Ogone hash: '$shagiven'");

    if (uc($digest) eq uc($shagiven)) {
        $self->verified(1);

        $self->log->debug('Ogone hash verified');

        if ($fields{STATUS} eq '9') {
            $self->succes(1);
            $self->log->info(
                sprintf("Succesful transaction for order %s", $self->orderid)
            );

        } else {
            $self->log->error(
                sprintf(
                    "Unsuccesful transaction for order %s: Statuscode: %s",
                    $self->orderid, $fields{STATUS}
                )
            );
        }
    } else {
        $self->log->error(
            sprintf(
                "Ogone hash (%s) does not match our digest hash (%s) for order %s",
                $shagiven, $digest, $self->orderid
            )
        );
        $self->verified(undef);
        $self->succes(undef);
    }

    # Correct amount, irritating ogone:
    my $cents = $self->amount * 100;
    $self->log->info(
        sprintf(
            "Ogone order %s for %s (%d in cents)",
            $self->orderid, $self->amount, $cents
        )
    );
    $self->amount($cents);

    return $self->verified;
}

sub clear_payment {
    my ($self) = @_;

    $self->$_(undef) for qw/amount zaak orderid shasign succes/;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head2 clear_payment

TODO: Fix the POD

=cut

=head2 start_payment

TODO: Fix the POD

=cut

=head2 verify_payment

TODO: Fix the POD
