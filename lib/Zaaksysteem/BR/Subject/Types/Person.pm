package Zaaksysteem::BR::Subject::Types::Person;
use Moose::Role;
use Zaaksysteem::Tools;

use Zaaksysteem::Object::Types::ExternalSubscription;

with 'MooseX::Log::Log4perl';

use Zaaksysteem::BR::Subject::Constants qw/SUBJECT_CONFIGURATION SUBJECT_MAPPING/;

use constant SUBJECT_TYPE => 'person';
use constant SCHEMA_TABLE => 'NatuurlijkPersoon';

with 'Zaaksysteem::BR::Subject::Queue::Person';


=head1 NAME

Zaaksysteem::BR::Subject::Types::Person - Bridge specific role for this type

=head1 DESCRIPTION

Applies logic for the L<Zaaksysteem::BR::Subject> bridge for inflating various types.

=head1 ATTRIBUTES

=head2 _table_id

Private method: contains the reference to the original table ('natuurlijk_persoon.id')

=cut

has '_table_id' => (
    is      => 'rw',
    isa     => 'Int',
);

=head1 METHODS

=head2 new_from_row

=cut

sub new_from_row {
    my ($class, $row) = @_;

    my $table_name  = $row->result_source->name;
    my $subjecttype = SUBJECT_MAPPING->{ $table_name };
    my $mapping     = SUBJECT_CONFIGURATION->{ $subjecttype }->{mapping};


    my %values;
    foreach my $col (keys %$mapping) {
        $values{$mapping->{$col}} = $row->$col if defined $row->get_column($col);
    }

    # Initials are not always in the database, this need to be created
    if (!$values{initials}) {
        $values{intials} = $row->_build_voorletters;
    }

    my %contact     = $class->_load_contact_data_from_row($row);
    my %addresses   = $class->_load_addresses_from_row($row);
    my %partner     = $class->_load_partner_from_row($row);

    $values{is_secret} = $class->_is_secret($values{is_secret});

    ### Empty bsn is allowed...unfortunatly
    delete($values{$_}) for grep({ !length($values{$_}) } qw/personal_number personal_number_a/);

    my $person = $class->new(%values, %contact, %addresses, %partner);

    $person->id($row->uuid);
    $person->_table_id($row->id);

    return $person;
}

=head2 new_from_params

=cut

sub new_from_params {
    my $class           = shift;
    my $rawparams       = { %{ (shift || {}) } };
    my %params          = map({ $_ => $rawparams->{ $_ } } grep( { defined($rawparams->{ $_ }) } keys %$rawparams));

    ### Only load adresses when either street or foreign_address_line1 is filled. Could be an empty HASHREF
    if (
        $params{address_correspondence} &&
        ($params{address_correspondence}->{street} || $params{address_correspondence}->{foreign_address_line1})
    ) {
        $params{address_correspondence}   = $class->_get_address_object_from_params($params{address_correspondence});
    } else {
        delete $params{address_correspondence};
    }

    ### Only load adresses when either street or foreign_address_line1 is filled. Could be an empty HASHREF
    if (
        $params{address_residence} &&
        ($params{address_residence}->{street} || $params{address_residence}->{foreign_address_line1})
    ) {
        $params{address_residence}        = $class->_get_address_object_from_params($params{address_residence});
    } else {
        delete $params{address_residence};
    }

    ### Only load partner when family name is filled, could be an empty hashref
    if ($params{partner} && $params{partner}->{family_name}) {
        $params{partner}                  = $class->_load_partner_from_params($params{partner});
    } else {
        delete $params{partner};
    }

    $params{is_secret} = $class->_is_secret($params{is_secret});

    delete($params{$_}) for grep({ !defined($params{$_}) } keys %params);

    return $class->new(%params);
}

sub _is_secret {
    my ($self, $secrecy) = @_;
    return 0 if !$secrecy;
    $secrecy = uc($secrecy);
    if ($secrecy eq 'N') {
        return 0;
    }
    return 1;
}


=head2 save_to_tables

Please do not use this function directly, instead, use the "bridge": L<Zaaksysteem::Bridge::Subject>

=cut

define_profile 'save_to_tables' => (
    required    => {
        schema  => 'Zaaksysteem::Schema'
    }
);

sub save_to_tables {
    my $self        = shift;
    my $options     = assert_profile({ @_ })->valid;
    my $schema      = $options->{schema};

    $self->check_object(schema => $schema);

    my $mapping = SUBJECT_CONFIGURATION->{ SUBJECT_TYPE() }->{mapping};

    ### Collect values
    my %values      = map(
        { my $key = $mapping->{ $_ }; $_ => $self->$key }
        keys %$mapping
    );

    ### Correct address and legal type
    my %addresses = (
        correspondence  => $self->_get_address_values(type => 'address_correspondence'),
        residence       => $self->_get_address_values(type => 'address_residence'),
    );

    #### Argh, these damn hacks: we need to turn "is_secret" into an integer.
    $values{indicatie_geheim} = ($values{indicatie_geheim} ? 1 : 0);

    #### Add partner to values
    my $partnermapping  = SUBJECT_CONFIGURATION->{ SUBJECT_TYPE() }->{partner_mapping};
    $partnermapping = { map { $partnermapping->{$_} => $_ } keys %$partnermapping }; # Reverse
    if (my $partner = $self->partner) {
        $values{ $partnermapping->{$_} } = $partner->$_ for keys %$partnermapping;
    }
    else {
        $values{ $partnermapping->{$_} } = undef for keys %$partnermapping;
    }

    try {
        $schema->txn_do(
            sub {
                my $row = $self->id
                    ? $self->_update_entry($schema, \%values)
                    : $self->_create_entry($schema, \%values);

                $self->update_addresses($row, \%addresses);
                $self->run_post_triggers($row, \%values);
                $row->discard_changes;
                $self->id($row->uuid);
                $self->_table_id($row->id);
                if (!$self->initials) {
                    $self->initials($row->_build_voorletters);
                }
            }
        );
    }
    catch {
        $self->log->error($_);
        if (blessed($_) && $_->can('throw')) {
            $_->throw();
        }
        die $_;
    };
    return $self;
}

sub _update_entry {
    my ($self, $schema, $values) = @_;
    my $row = $schema->resultset(SCHEMA_TABLE)->search({ uuid => $self->id })
        ->first;

    try {
        $row->update($values);
    }
    catch {
        $self->log->error("Error updating natuurlijk persoon with values [%s]: %s", dump_terse($values), $_);
        throw(
            'object/types/person/update',
            sprintf(
                'Failed updating person by uuid %s [%d]',
                $row->uuid, $row->id
            )
        );
    };
    return $row;
}

sub _create_entry {
    my ($self, $schema, $values) = @_;
    return try {
        return $schema->resultset('NatuurlijkPersoon')->create($values);
    }
    catch {
        $self->log->error("Error inserting natuurlijk persoon with values [%s]: %s", dump_terse($values), $_);
        throw(
            'object/types/person/create',
            sprintf(
                'Failed saving new natuurlijk persoon'
            )
        );
    };
}

=head2 check_object

    $entity->check_object(schema => $schema);

Will return the result of a C<assert_profile> when the object is missing some params. The entity
objects are rather free form. Mostly because of our legacy, but sometimes also when we just want
to see part of a subject.

This function makes sure we create or save objects into our database in a complete form, to prevent
further "legacy"

=cut

define_profile check_object => (
    %{ SUBJECT_CONFIGURATION->{ SUBJECT_TYPE() }->{profiles}->{save} }
);

sub check_object {
    my $self        = shift;
    my (%opts)      = @_;

    my %values      = map(
        { my $key = $_->name; $key => $self->$key }
        grep({ $_->does('Zaaksysteem::Metarole::ObjectAttribute') || $_->does('Zaaksysteem::Metarole::ObjectRelation') } $self->meta->get_all_attributes)
    );

    assert_profile({ %values, _schema => $opts{schema} })->valid;

    ### Extra, check addresses
    $self->$_->check_object for grep({ $self->$_ } qw/address_correspondence address_residence/);

    return 1;
}

=head2 update_addresses

Arguments: \%PARAMS

Called from Bridge object

=cut

define_profile update_addresses => (
    required => {},
    optional => {
        correspondence  => 'HashRef',
        residence       => 'HashRef',
    }
);

sub update_addresses {
    my $self        = shift;
    my $row         = shift;
    my $params      = assert_profile(shift || {})->valid;

    if ($params->{correspondence} && keys %{ $params->{correspondence} }) {
        $self->_update_or_create_address($row, $params->{correspondence}, 'B');
    }

    if ($params->{residence} && keys %{ $params->{residence} }) {
        $self->_update_or_create_address($row, $params->{residence}, 'W');
    }
}

=head1 PRIVATE METHODS

=head2 _update_or_create_address

=cut

sub _update_or_create_address {
    my $self        = shift;
    my $row         = shift;
    my $params      = shift;
    my $address_flag = shift;

    my $current_address;
    if ($address_flag eq 'B') {
        $current_address = $row->correspondentieadres;
    } else {
        $current_address = $row->verblijfsadres;
    }

    if ($current_address) {
        $current_address->update({ %$params, functie_adres => $address_flag });
        $row->update_address_links;
    } else {
        $row->add_address({ %$params, functie_adres => $address_flag });
    }
}


=head2 _get_address_values

=cut

define_profile _get_address_values => (
    required    => {
        type        => 'Str',
    }
);

sub _get_address_values {
    my $self    = shift;
    my $options = assert_profile({ @_ })->valid;
    my $attrkey = $options->{type};

    my $address = $self->$attrkey;
    return {} unless $address;     # No address set

    my $mapping = SUBJECT_CONFIGURATION->{SUBJECT_TYPE()}->{address_mapping};

    my %values;
    for my $key (keys %$mapping) {
        my $attr  = $mapping->{$key};
        my $value = $address->$attr;

        if ($attr =~ /^(?:country|municipality)$/ && $value) {
            $value = $value->dutch_code;
        }

        $values{$key} = $value;
    }

    return \%values;
}

=head2 _build_display_name

Returns "initials surname"

=cut

sub _build_display_name {
    my $self            = shift;

    my @i;
    foreach (qw(initials surname)) {
        push(@i, $self->$_) if $self->$_;
    }
    return join(" ", @i);
}

=head2 _load_contact_data_from_row

Loads the contact data into the subject (phone numbers, email address)

=cut

sub _load_contact_data_from_row {
    my ($class, $row) = @_;
    
    my %rv;

    my $contactdata = $row->result_source->schema->resultset('ContactData')->search({
        gegevens_magazijn_id    => $row->id,
        betrokkene_type         => 1, # "Person"
    })->first;

    return unless $contactdata;

    $rv{mobile_phone_number} = $contactdata->mobiel         if $contactdata->mobiel;
    $rv{phone_number}        = $contactdata->telefoonnummer if $contactdata->telefoonnummer;
    $rv{email_address}       = $contactdata->email          if $contactdata->email;

    return %rv;
}

=head2 _load_addresses_from_row

Loads the addresses into this subject

=cut

sub _load_addresses_from_row {
    my ($class, $row) = @_;
    my %rv;

    if ($row->correspondentieadres) {
        $rv{address_correspondence}   = $class->_get_address_object_from_row($row->correspondentieadres);
    }

    if ($row->verblijfsadres) {
        $rv{address_residence}        = $class->_get_address_object_from_row($row->verblijfsadres);
    }

    return %rv;
}

=head2 _load_partner_from_row

Loads the partner into this subject

=cut

sub _load_partner_from_row {
    my ($class, $row)   = @_;
    my $mapping         = SUBJECT_CONFIGURATION->{'person' }->{partner_mapping};

    my %rv              = map { $mapping->{$_} => $row->get_column($_) } grep ({ length $row->get_column($_) } keys %$mapping);

    return (partner => Zaaksysteem::Object::Types::Person->new(%rv));
}

=head2 _load_partner_from_params

Loads the partner into this subject

=cut

sub _load_partner_from_params {
    my ($class, $params)   = @_;

    delete($params->{$_}) for grep({ !defined($params->{$_}) } keys %$params);

    return Zaaksysteem::Object::Types::Person->new(%$params);
}

=head2 _get_address_object_from_row

Returns a filled object of type L<Zaaksysteem::Object::Types::Address>

=cut

sub _get_address_object_from_row {
    my ($class, $row) = @_;

    unless ($row) {
        throw('object/types/person/no_row', 'No row given, code error');
    }

    my $mapping     = SUBJECT_CONFIGURATION->{'person' }->{address_mapping};

    my %values      = map(
        { $mapping->{$_} => $row->$_ }
        grep({ length $row->get_column($_) } keys %$mapping)
    );

    # TODO Possibly saner to move this to e.g. new_from_row in Types/Address
    $values{country}        = Zaaksysteem::Object::Types::CountryCode->new_from_code($values{country}) if $values{country};
    $values{municipality}   = Zaaksysteem::Object::Types::MunicipalityCode->new_from_code($values{municipality}) if $values{municipality};

    ### :( make sure zipcode is uppercase
    $values{zipcode} = uc($values{zipcode}) if $values{zipcode};

    return Zaaksysteem::Object::Types::Address->new(%values);
}

=head2 _get_address_object_from_row

Returns a filled object of type L<Zaaksysteem::Object::Types::Address>

=cut

sub _get_address_object_from_params {
    my ($class, $params)    = @_;
    my %values              = map({ $_ => $params->{ $_ } } grep( { defined($params->{ $_ }) } keys %$params));

    unless ($params) {
        throw('object/types/person/no_row', 'No row given, code error');
    }

    # TODO Possibly saner to move this to e.g. new_from_row in Types/Address
    $values{country}        = Zaaksysteem::Object::Types::CountryCode->new_from_code($values{country}->{dutch_code}) if $values{country};
    $values{municipality}   = Zaaksysteem::Object::Types::MunicipalityCode->new_from_code($values{municipality}->{dutch_code}) if $values{municipality};

    return Zaaksysteem::Object::Types::Address->new(%values);
}

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
