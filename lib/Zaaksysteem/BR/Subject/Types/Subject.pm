package Zaaksysteem::BR::Subject::Types::Subject;

use Moose::Role;
use Zaaksysteem::Tools;

with 'MooseX::Log::Log4perl';

use Zaaksysteem::BR::Subject::Constants qw/SUBJECT_MAPPING TYPE_TO_SCHEMA/;

use Zaaksysteem::Types qw(ObjectSubjectType JSONBoolean Timestamp);


=head1 NAME

Zaaksysteem::BR::Subject::Types::Subject - Bridge specific role for this type

=head1 DESCRIPTION

Applies logic for the L<Zaaksysteem::BR::Subject> bridge for inflating various types.

=cut

=head2 new_from_row

    my $object = Zaaksysteem::Object::Types::Subject->new_from_row($schema->resultset('NatuurlijkPersoon')->first);

=cut

sub new_from_row {
    my $class       = shift;
    my $row         = shift;

    throw(
        'object/types/subject/new_from_row/invalid_row',
        'Row must inherit from Zaaksysteem::BR::Subject::Component'
    ) unless blessed($row) && $row->can('as_object');

    my $table_name  = $row->result_source->name;
    my $subjecttype = SUBJECT_MAPPING->{ $table_name };

    my $sclass      = 'Zaaksysteem::Object::Types::' . ucfirst($subjecttype);

    my $subject     = $sclass->new_from_row($row);

    my $authentication  = Zaaksysteem::Object::Types::Authentication->new_from_related_row($row);
    my $os = Zaaksysteem::Object::Types::ExternalSubscription->new;
    $os = $os->new_from_related_row($row);

    return $class->new(
        subject_type    => $subjecttype,
        subject         => $subject,
        id              => $subject->id,
        ($authentication ? (authentication  => $authentication) : ()),
        ($os ? (external_subscription  => $os) : ()),
    );
}

=head2 new_from_params

=cut

sub new_from_params {
    my $class       = shift;
    my $params      = { %{ (shift || {}) } };

    throw('object/types/subject/invalid_subject_type', 'Invalid subject_type given')
        unless ObjectSubjectType->check($params->{subject_type});

    my $sclass      = 'Zaaksysteem::Object::Types::' . ucfirst($params->{subject_type});

    my %args = (
        subject_type => $params->{subject_type},
        subject      => $sclass->new_from_params($params->{subject}),
    );

    if ($params->{authentication} && $params->{subject_type} eq 'company') {
        $args{authentication} = Zaaksysteem::Object::Types::Authentication->new(
            entity_type => $params->{subject_type},
            %{ $params->{authentication} }
        );
    }

    if ($params->{external_subscription}) {
        $args{external_subscription} =
            Zaaksysteem::Object::Types::ExternalSubscription->new_from_params(
                %{ $params->{external_subscription} }
            )
        ;
    }

    return $class->new(%args);
}

=head2 save_to_tables

Please do not use this function directly, instead, use the "bridge": L<Zaaksysteem::Bridge::Subject>

=cut

define_profile 'save_to_tables' => (
    required    => {
        schema  => 'Zaaksysteem::Schema'
    }
);

sub save_to_tables {
    my $self   = shift;
    my $params = assert_profile({@_})->valid;

    $self->subject->save_to_tables(@_);

    $self->authentication(
        $self->authentication->save_to_tables(@_, gegevens_magazijn_id => $self->subject->_table_id)
    ) if ($self->authentication && $self->subject->_table_id);

    Zaaksysteem::Object::Types::ExternalSubscription->save_to_tables(
        schema       => $params->{schema},
        subject      => $self,
        subscription => $self->external_subscription,
    );

    ### Set uuid to uuid of subject
    if ($self->subject->id) {
        $self->id($self->subject->id);
    }

    return $self;
}

=head2 discard_changes

Discards changes and returns this object

=cut

define_profile 'discard_changes' => (
    required    => {
        schema  => 'Zaaksysteem::Schema'
    }
);

sub discard_changes {
    my $self        = shift;
    my $opts        = assert_profile({ @_ })->valid;

    my $source      = TYPE_TO_SCHEMA->{ $self->subject_type };
    my $row         = $opts->{schema}->resultset($source)->search({ uuid => $self->id })->first;

    throw(
        'object/types/subject/discard_changes/row_not_found',
        'Could not discard changes: cannot find source'
    ) unless $row;

    return $self->new_from_row($row);
}


=head2 set_authenticated

    $self->set_authenticated($schema, $interface, 'MK22332SUBSCRIPTION_IDENTIFIER234234');

    # Sets the authenticated to true, and authenticatedby to the name of the interface

Will set the authenticated flag on this object to true and makes sure the authenticatedby is set
to the given parameters

=cut

sub set_authenticated {
    my ($self, $schema, $interface, $subscription_id) = @_;

    throw(
        'object/types/company/set_authenticated/no_id',
        'Failed updating company: No id found.'
    ) unless $self->id;

    my $company = $schema->resultset('Bedrijf')->search({ uuid => $self->id})->first;

    throw(
        'object/types/company/set_authenticated/no_id',
        'Failed updating company: ' . $self->id . '. Not found in database.'
    ) unless $company;

    $schema->resultset('ObjectSubscription')->create(
        {
            interface_id    => $interface->get_column('id'),
            external_id     => ($subscription_id || 'UNKNOWN'),
            local_table     => 'Bedrijf',
            local_id        => $company->get_column('id'),
            object_preview  => $self->display_name,
        }
    );

    $company->authenticated(1);
    $company->authenticatedby($interface->get_column('name'));
    $company->update;

    return $self;
}

1;


__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
