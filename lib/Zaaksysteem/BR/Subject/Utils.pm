package Zaaksysteem::BR::Subject::Utils;

use Moose::Role;

use Zaaksysteem::Tools;
use Zaaksysteem::Types qw(UUID);

use Zaaksysteem::BR::Subject::Constants qw/SUBJECT_CONFIGURATION SUBJECT_MAPPING/;

with 'MooseX::Log::Log4perl';

=head1 NAME

aaksysteem::BR::Subject::Utils - Utility role for bridge objects.

=head1 DESCRIPTION

This class defines some utility functions. Think about methods for mapping attributes etc.

=head1 SYNOPIS

=head1 METHODS

=head2 map_search_params

    $bridge->map_search_params(
        {
            personal_number => '123456789',
            address_residence.street => 'Donker Curtiusstraat'
        },
        'person'
    );

    # Returns:
    {
        'burgerservicenummer' => '123456789',
        'address_residence'   => {
            straatnaam => 'Donker Curtiusstraat'
        }
    }

B<Return value>: HashRef

Maps the given "english" search params to the dutch postgresql table columns

=cut

sub map_search_params {
    my $self        = shift;
    my $params      = shift;
    my $stype       = shift;

    my %search_params;

    ### Revert mapping
    my $config          = SUBJECT_CONFIGURATION->{$stype};

    my $hash            = $self->_inflate_key_value_to_hash($params);
    my $mapped_hash     = $self->map_hash_values($config, $hash);

    return $mapped_hash;
}

=head2 _inflate_key_value_to_hash

    my $hash = $self->_inflate_key_value_to_hash(
        {
            'subject.company' => 'Testcompany',
            'subject.address_residence.street' => 'Donker'
        }
    );

    # Outputs
    $VAR1 = {
        subject     => {
            company => 'Testcompany',
            address_residence => {
                street  => 'Donker'
            }
        }
    }

Will transform a key => pair with dotted seperated keys into a multi-dimensional
hash

=cut

sub _inflate_key_value_to_hash {
    my $self        = shift;
    my $params      = shift;

    ### Get hash
    my $hashed      = {};

    for my $k (keys %$params) {
        my @parts = split /\./, $k;

        my $d = \$hashed;
        for my $part (@parts) {
            $d = \${$d}->{$part};
        }
        ${$d} = $params->{$k};
    }

    return $hashed;
}

=head2 map_hash_values

    my $mapped = $self->map_hash_values(
        $config, 
        {
            subject     => {
                company => 'Testcompany',
                address_residence => {
                    street  => 'Donker'
                }
            }
        }
    );

    # Outputs
    $VAR1 = {
        subject     => {
            handelsnaam => 'Testcompany',
            address_residence => {
                straatnaam  => 'Donker'
            }
        }
    }

Will map keys according to the settings in L<Zaaksysteem::BR::Subject::Constants>

=cut

sub map_hash_values {
    my ($self, $config, $tparams, $prefix) = @_;
    my $map;
    if ($prefix && $config->{mapper}->{$prefix} && $config->{ $config->{mapper}->{$prefix} }) {
        $map    = $config->{ $config->{mapper}->{$prefix} };
        $prefix .= '.';
    } else {
        $map    = $config->{mapping};
        $prefix = '';
    }

    my %mapping = map { $map->{ $_ } => $_ } keys %{ $map };

    my @delete_keys;
    for my $key (%$tparams) {
        if (ref $tparams->{$key} eq 'HASH') {
            my $newkey = $key;

            $tparams->{$key} = $self->map_hash_values($config, $tparams->{$key}, $prefix . $key);
        } else {
            if ($mapping{$key}) {
                $tparams->{ $mapping{$key} } = $tparams->{$key};
                push(@delete_keys, $key);
            }
        }
    }

    delete($tparams->{$_}) for @delete_keys;

    return $tparams;
}

=head2 _search_via_preferred

Makes sure we search via the preferred way, either search_via_bridge or a standard search
call with params converted

=cut

sub _search_via_preferred {
    my $self        = shift;
    my $resultset   = shift;
    my $params      = shift;

    my ($query, $caller);
    if ($caller = $resultset->can('search_from_bridge')) {
        $query       = $params;
    } else {
        $query       = $self->map_search_params($params->{subject}, $params->{subject_type});
        $caller      = $resultset->can('search');
    }

    ### Caller is a reference to either: search or search_from_bridge
    return $caller->($resultset, $query, @_);
}

=head2 __bridge_format_for_dbix

Formats a search value before going to the database.

=cut

sub _bridge_format_for_dbix {
    my $self        = shift;
    my $stype       = shift;
    my $key         = shift;
    my $value       = shift;

    my $search_filters = SUBJECT_CONFIGURATION->{$stype}->{search_filter};

    if (my $ref = $search_filters->{$key}) {
        if ($ref eq 'like' || $ref eq 'ilike') {
            return { $ref => "%$value%" };
        } elsif (ref $ref eq 'CODE') {
            return $ref->($value);
        }
    }

    return $value;
}

=head2 object_as_params
    
    $self->object_as_params($subject);

    # Returns
    {
        personal_number => 24324234,
        address_residence => {
            street  => 'Donker',
            [...]
        }
        [...]
    }

=cut

sub object_as_params {
    my $self        = shift;
    my $object      = shift || $self;

    my @attrs = grep { $_->does('Zaaksysteem::Metarole::ObjectAttribute') && $_->name !~ /^(?:date_modified|date_created)$/ }
                     $object->meta->get_all_attributes;

    my @rels = grep { $_->does('Zaaksysteem::Metarole::ObjectRelation') }
                    $object->meta->get_all_attributes;

    my %instance_attributes = map { my $value = $_->get_value($object); ($_->isa('Zaaksysteem::Object') ? ($_->name => $self->object_as_params($value)) : ($_->name => $value)) } @attrs;
    my %instance_relations  = map { $_->name => ($_->get_value($object) ? $self->object_as_params($_->get_value($object)) : undef) } @rels;

    return { %instance_attributes, %instance_relations };
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
