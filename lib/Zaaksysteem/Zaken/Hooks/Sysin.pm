package Zaaksysteem::Zaken::Hooks::Sysin;

use Moose::Role;

=head2 hook_case_register

Checks every casetype interface for a hook

=cut

after 'hook_case_register' => sub {
    my $self                        = shift;

    my $interfaces                  = $self->zaaktype_id->interfaces->search_active;

    return unless $interfaces->count;

    while (my $interface = $interfaces->next) {
        my $hooks   = $interface->module_object->case_hooks;

        for my $hook (grep { $_->{when} eq 'case_register' } @$hooks) {
            eval {
                $interface->process_trigger(
                    $hook->{trigger},
                    {
                        case => $self
                    }
                );
            };

            if ($@) {
                warn('Hook: ' . $hook->{trigger} . ' went wrong: ' . $@);
            }
        }
    }
};

1;


__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

