package Zaaksysteem::Zaken::DestroyReport;

use Data::Dumper;
use Moose;
use Zaaksysteem::Profile;
use Zaaksysteem::Exception;

=head2 cases

holds a structure like:
    my $cases = {
        64 => {
            errors => ['Is niet vernietigbaar'],
            warnings => ['Heeft deelzaken die niet vernietigd zijn', 'nog wat']
        },
        65 => {},
        66 => {
            errors => ['Heeft niet status vernietiging']
        }
    };

=cut

has cases => ( is => 'rw', default => sub { {} } );


define_profile add_case => (
    required => [qw/case_id/],
    optional => [qw/errors warnings/]
);
sub add_case {
    my $self = shift;
    my $arguments = assert_profile(shift)->valid;

    my $cases = $self->cases;
    my $case_id = $arguments->{case_id};

    $cases->{$case_id} = {
        errors   => $arguments->{errors},
        warnings => $arguments->{warnings}
    };
    $self->cases($cases);
}


sub has_warnings {
    my ($self) = @_;

    my $cases = $self->cases;
    grep { $_->{warnings} && @{ $_->{warnings} } } values %$cases;
}


sub has_errors {
    my ($self) = @_;

    my $cases = $self->cases;
    grep { $_->{errors} && @{ $_->{errors} } } values %$cases;
}


sub report {
    my $self = shift;

    my $cases = $self->cases;

    # filter only cases with actual warnings or errors
    my @case_ids = grep {
        $cases->{$_}->{warnings} || $cases->{$_}->{errors}
    } keys %$cases;

    return {map { $_ => $cases->{$_} } @case_ids};
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 add_case

TODO: Fix the POD

=cut

=head2 has_errors

TODO: Fix the POD

=cut

=head2 has_warnings

TODO: Fix the POD

=cut

=head2 report

TODO: Fix the POD

=cut

