package Zaaksysteem::Zaken::ComponentZaakBetrokkenen;

use Moose;
use Data::UUID;

extends 'DBIx::Class';

=head1 NAME

Zaaksysteem::Zaken::ComponentZaakBetrokkenen

=head1 DESCRIPTION

This package defines additional data and behavior for the
L<ZaakBetrokkenen|Zaaksysteem::Schema::ZaakBetrokkenen> table.

=head1 ATTRIBUTES

=head2 betrokkene_identifier

This attribute holds a stringified 'betrokkene identifier', composed from
our internal data.

B<Warning>: This attribute may be out of date after updates to the
L<betrokkene_type|Zaaksysteem::Schema::ZaakBetrokkenen/betrokkene_type> or
L<gegevens_magazijn_id|Zaaksysteem::Schema::ZaakBetrokkenen/gegevens_magazijn_id>
fields.

=cut

has betrokkene_identifier => (
    is          => 'rw',
    lazy        => 1,
    default     => sub {
        my $self = shift;

        return sprintf(
            'betrokkene-%s-%s',
            $self->betrokkene_type,
            $self->gegevens_magazijn_id
        );
    }
);

=head1 METHODS

=head2 TO_JSON

This method returns a hashref which can be used by L<JSON/encode_json> to
serialize instances of this class.

=cut

sub TO_JSON {
    my $self = shift;

    return {
        id => $self->id,
        name => $self->naam,
        role => $self->rol,
        pip_authorized => $self->pip_authorized ? \1 : \0,
        betrokkene_id => $self->betrokkene_id,
        betrokkene_type => $self->betrokkene_type,
        betrokkene_identifier => $self->betrokkene_identifier,
        magic_string_prefix => $self->magic_string_prefix,
    };
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
