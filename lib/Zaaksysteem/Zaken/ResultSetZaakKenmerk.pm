package Zaaksysteem::Zaken::ResultSetZaakKenmerk;

use Moose;
use Data::Dumper;
use Params::Profile;

use Zaaksysteem::Constants;
use Zaaksysteem::Exception;
use Zaaksysteem::Profile;

use Clone qw/clone/;
use List::Util;
use Time::HiRes qw/tv_interval gettimeofday/;

extends 'DBIx::Class::ResultSet';


{
    define_profile create_kenmerken => (
        required => [ qw/zaak_id kenmerken/],
    );

    sub create_kenmerken {
        my ($self, $params) = @_;
        $params = assert_profile($params)->valid();

        my $zaak_id   = $params->{zaak_id};
        my $kenmerken = $params->{kenmerken};

        if (ref $kenmerken ne 'ARRAY') {
            throw(
                'create_kenmerken: input $kenmerken not an array'
            )
        }

        for my $kenmerk (@$kenmerken) {
            if (ref $kenmerk ne 'HASH') {
                throw('create_kenmerken: $kenmerk not a HASHREF: ' . Dumper($kenmerk));
            }

            throw("create kenmerken incorrect format " . Dumper ($kenmerk))
                unless (scalar keys %$kenmerk == 1);

            my ($bibliotheek_kenmerken_id, $values) = each %$kenmerk;

            $self->create_kenmerk({
                zaak_id                     => $zaak_id,
                bibliotheek_kenmerken_id    => $bibliotheek_kenmerken_id,
                values                      => $values
            });

            $self->log_field_create({
                zaak_id                     => $zaak_id,
                bibliotheek_kenmerken_id    => $bibliotheek_kenmerken_id,
                values                      => $values
            });
        }
    }
}


sub log_field_create {
    my ($self, $params) = @_;

    my $logging = $self->result_source->schema->resultset('Logging');


    # For some exotic reason sometimes $params->{values} is an array and
    # sometimes it is not.
    my $values = $params->{values} || [];
    if (!UNIVERSAL::isa($values, 'ARRAY') && $values) {
       $values = [$values];
    }

    my $value_string = join(", ", grep { defined($_) } @$values);

    return unless $value_string;

    # Convert nummeraanduiding to human readable.
    if ($value_string =~ /^(nummeraanduiding|openbareruimte|woonplaats)-\d+.*$/) {
        $value_string = $self->get_bag_hr(@$values);
    }

    my $data = {
        case_id         => $params->{zaak_id},
        attribute_id    => $params->{bibliotheek_kenmerken_id},
        attribute_value => $value_string
    };

    $logging->trigger('case/attribute/create', {
        component   => 'kenmerk',
        zaak_id     => $params->{ zaak_id },
        data        => $data,
    });
}


{
    Params::Profile->register_profile(
        method  => 'create_kenmerk',
        profile => {
            required => [ qw/zaak_id bibliotheek_kenmerken_id values/],
        }
    );

    my @numeric_types = qw(numeric valuta valutain valutaex valutain6 valutaex6 valutain21 valutaex21);

    sub create_kenmerk {
        my ($self, $params) = @_;

        my $schema = $self->result_source->schema;
        my $bibliotheek_kenmerk = $schema->resultset('BibliotheekKenmerken')->find($params->{bibliotheek_kenmerken_id});
        my $value_type = $bibliotheek_kenmerk->value_type;

        my $values = $params->{values};

        # Pre-process the numeric types to ensure correct formatting of decimal
        # separator and thousands separator.
        if ( List::Util::first { $_ eq $value_type } @numeric_types ) {
            $values = $bibliotheek_kenmerk->filter( $params->{values} );
        }

        $values = UNIVERSAL::isa($values, 'ARRAY') ? $values : [$values];

        foreach my $value (@{ $values }) {
            next unless length($value);

            my $row = $self->create({
                zaak_id                     => $params->{zaak_id},
                bibliotheek_kenmerken_id    => $params->{bibliotheek_kenmerken_id},
                value                       => $value
            });
            $row->set_value($value);
        }
    }
}

sub log_field_update {
    my ($self, $params) = @_;

    my $logging = $self->result_source->schema->resultset('Logging');

    my $values = $params->{values} || [];
    my $value_string = join(", ", grep { defined($_) } @$values);

    return unless defined($value_string);

    # Convert nummeraanduiding to human readable.
    if ($value_string =~ /^(nummeraanduiding|openbareruimte|woonplaats)-\d+.*$/) {
        $value_string = $self->get_bag_hr(@$values);
    }


    my $data = {
        case_id         => $params->{zaak_id},
        attribute_id    => $params->{bibliotheek_kenmerken_id},
        attribute_value => $value_string
    };


    my $event = $logging->find_recent({
        data => $data,
        event_type => 'case/attribute/update'
    });

    if($event) {
        $event->data($data);
        $event->update;
    } else {
        $logging->trigger('case/attribute/update', {
            component   => 'kenmerk',
            zaak_id     => $params->{ zaak_id },
            data        => $data,
        });
    }
}




{
    Params::Profile->register_profile(
        method  => 'replace_kenmerk',
        profile => 'Zaaksysteem::Zaken::ResultSetZaakKenmerk::create_kenmerk',
    );

    sub replace_kenmerk {
        my ($self, $params) = @_;

        eval {
            $self->result_source->schema->txn_do(sub {

                # Remove existing values for this bibliotheek_kenmerken_id
                $self->result_source->resultset->search({
                    zaak_id                     => $params->{zaak_id},
                    bibliotheek_kenmerken_id    => $params->{bibliotheek_kenmerken_id},
                })->delete;

                # Re-create this kenmerk with new values
                $self->create_kenmerk({
                    zaak_id                     => $params->{zaak_id},
                    bibliotheek_kenmerken_id    => $params->{bibliotheek_kenmerken_id},
                    values                      => $params->{values}
                });
            });
        };

        if ($@) {
            warn('Arguments: ' . Dumper($params));
            die('Replace kenmerk failed: ' . $@)
        }

        return 1;
    }
}




{
    Params::Profile->register_profile(
        method  => 'get',
        profile => {
            required => [ qw/bibliotheek_kenmerken_id/ ]
        }
    );


    sub get {
        my ($self, $params) = @_;

        my $dv = Params::Profile->check(params  => $params);
        die "invalid options for get" unless $dv->success;

        my $bibliotheek_kenmerken_id = $params->{bibliotheek_kenmerken_id};

        my $kenmerken   = $self->search(
            {
                bibliotheek_kenmerken_id => $bibliotheek_kenmerken_id
            },
            {
                prefetch        => [
                    'bibliotheek_kenmerken_id'
                ],
            }
        );

        return $kenmerken->first();
    }
}



Params::Profile->register_profile(
    method  => 'update_field',
    profile => {
        required => [ qw/bibliotheek_kenmerken_id zaak_id/ ],
        optional => [ qw/new_values/ ]
    }
);

sub update_field {
    my ($self, $opts) = @_;

    my $dv = Params::Profile->check(params  => $opts);
    die "invalid options for update_field" unless $dv->success;

    my $bibliotheek_kenmerken_id    = $dv->valid('bibliotheek_kenmerken_id');
    my $new_values                  = $dv->valid('new_values');
    my $zaak_id                     = $dv->valid('zaak_id');

    my ($removed,$race, $raceprotector) = (0,1,0);
    my ($errormsg);

    ### Race condition security, prevent multiple updates;
    while ($race && $raceprotector < 5) {
        $raceprotector++;

        my $params = {
            bibliotheek_kenmerken_id    => $bibliotheek_kenmerken_id,
            zaak_id                     => $zaak_id,
        };

        eval {
            $self->result_source->schema->txn_do(sub {
                my $attributes = $self->result_source->resultset->search($params);
                $removed = $attributes->count;

                if($removed) {
                    $attributes->delete;
                };

                $params->{values} = $new_values;

                if(defined $new_values)  {
                    # Re-create this kenmerk with new values
                    $self->create_kenmerk($params);
                }

                $self->log_field_update($params);
            });
        };

        ### RACE PROTECTION CODE
        ### The code below checks the amount of kenmerken which would be added
        ### as a result of the above code. Another request could in theory and,
        ### unfortunatly, already call another update_field which results
        ### in duplicated entries.
        ###
        ### See: https://github.com/mintlab/Zaaksysteem/issues/1833
        ### Only testable on more or less slow connections, like test-e
        ###
        ### Improvement hint: do not use count from $params->{values}, but from
        ### a counter of the real rows above.
        ###
        ### - michiel
        if ($@) {
            $errormsg = $@;
            if ($errormsg =~ /ERROR:  deadlock detected/) {
                ### Try again
                warn('Deadlock, try again');
                next;
            }
        } else {
            $errormsg = undef;
        }

        {
            my %check_params = %{ $params };
            delete($check_params{values});

            $params->{values} = [ $params->{values} ]
                unless UNIVERSAL::isa($params->{values}, 'ARRAY');

            my $kenmerk_count = $self->result_source->resultset->search(\%check_params);

            if ($kenmerk_count <= scalar @{ $params->{values}}) {
                $race=0;
            } else {
                warn('RACE CONDITION, TRY AGAIN');
            }
        }
    }

    if ($errormsg) {
        die("Could not update kenmerk: " . $errormsg);
    }


    return $removed;
}

define_profile update_fields => (
    required => {
        new_values => 'HashRef'
    },
    optional => {
        zaak_id => 'Int',
        zaak => 'Zaaksysteem::Zaken::ComponentZaak',
        ignore_log_update => 'Bool'
    },
    require_some => {
        case => [ 1, qw[zaak_id zaak] ]
    }
);

sub update_fields {
    my $self = shift;

    my $params = assert_profile(shift)->valid;

    my $new_values = $params->{ new_values };
    my $zaak = $params->{ zaak };
    my $zaak_id = $zaak ? $zaak->id : $params->{ zaak_id };

    my $error = 0;
    my $something_changed = 0;

    $self->result_source->schema->txn_do(sub {
        eval {
            my $params = {
                bibliotheek_kenmerken_id    => { in => [ keys %$new_values ]},
                zaak_id                     => $zaak_id,
            };

            my $kenmerken = $self->search($params,
                {
                    prefetch => 'bibliotheek_kenmerken_id'
                }
            );

            ### Only create kenmerk when value is not equal 'new value'
            $self->strip_identical_fields($new_values,$kenmerken);

            ### Delete old params
            $self->search(
                {
                    bibliotheek_kenmerken_id    => { in => [ keys %$new_values ]},
                    zaak_id                     => $zaak_id,
                },
            )->delete;

            for my $bibid (keys %{ $new_values }) {
                $something_changed = 1;
                my $create_kenmerk_params = {
                    bibliotheek_kenmerken_id    => $bibid,
                    zaak_id                     => $zaak_id,
                    values                      => $new_values->{$bibid}
                };

                $self->replace_kenmerk($create_kenmerk_params);

                unless ($params->{ ignore_log_update }) {
                    my $values = $create_kenmerk_params->{values};

                    $create_kenmerk_params->{values} =
                        UNIVERSAL::isa($values, 'ARRAY') ? $values : [$values];

                    $self->log_field_update($create_kenmerk_params);
                }
            }
        };

        if ($@) {
            die("Could not update kenmerk: " . $@);
            $error++;
        }

        if ($something_changed) {
            unless ($zaak) {
                $zaak = $self->result_source->schema->resultset('Zaak')->find($zaak_id);
            }

            $zaak->touch;
        }
    });

    return not $error;
}

sub strip_identical_fields {
    my ($self, $new_values, $kenmerken) = @_;

    my $db_values;
    while (my $kenmerk = $kenmerken->next) {
        my $bibid = $kenmerk->bibliotheek_kenmerken_id->id;

        if (
            $db_values->{$bibid}
        ) {
            unless (UNIVERSAL::isa($db_values, 'ARRAY')) {
                $db_values->{$bibid} = [
                    $db_values->{$bibid}
                ];
            }

            push(
                @{ $db_values->{$bibid} }, $kenmerk->value
            );
        }

        $db_values->{$bibid} = $kenmerk->value;
    }

    for my $bibid (keys %{ $new_values }) {
        if (
            !ref($db_values->{$bibid}) &&
            !ref($new_values->{$bibid}) &&
            !length($db_values->{$bibid}) && !length($new_values->{$bibid})
        ) {
            delete($new_values->{$bibid})
        }


        next unless $db_values->{$bibid};

        if (
            !ref($db_values->{$bibid}) &&
            !ref($new_values->{$bibid}) &&
            (
                defined $db_values->{$bibid} ? $db_values->{$bibid} : ''
            ) eq (
                defined $new_values->{$bibid} ? $new_values->{$bibid} : ''
            )
        ) {
            delete($new_values->{$bibid});
        }

        if (
            UNIVERSAL::isa($db_values->{$bibid}, 'ARRAY') &&
            UNIVERSAL::isa($new_values->{$bibid}, 'ARRAY') &&
            scalar(@{ $db_values->{$bibid} }) == scalar(@{ $new_values->{$bibid} })
        ) {
            my $match = 1;
            for my $value (@{ $new_values->{$bibid} }) {
                $match = 0 unless (grep { $value eq $_ } @{ $db_values->{$bibid} });
            }

            if ($match) {
                delete($new_values->{$bibid});
            }
        }
    }
}


=head2 get_bag_hr

Convert bag strings to something human readable.

=cut

sub get_bag_hr {
    my ($self) = shift;
    my @values = @_;

    my @hr;
    my $value_string;
    for my $bag_id (@values) {
        push @hr, $self->result_source->schema->resultset('BagNummeraanduiding')
            ->get_record_by_source_identifier($bag_id)->to_string;
    }
    if (@hr > 1) {
        $value_string = join(", ", grep { defined($_) } @hr);
    }
    else {
        $value_string = $hr[0];
    }

    return $value_string;
}


=head2 delete_fields

The use case for this method is fields that are being hidden by rules.
We receive a list of field_ids (bibliotheek_kenmerken) and we look
if any values are stored. If so, these are deleted, and the fields that
are actually affected are gathered and put in a log.

=cut

sub delete_fields {
    my ($self, $arguments) = @_;

    my $bibliotheek_kenmerken_ids = $arguments->{bibliotheek_kenmerken_ids} or die "need bibliotheek_kenmerken_ids";
    my $zaak_id = $arguments->{zaak_id} or die "need zaak_id";

    warn "Hiding fields in case $zaak_id " . join ", ", @$bibliotheek_kenmerken_ids;

    my $current = $self->search({
        zaak_id => $zaak_id, # ensure only this case is affected
        bibliotheek_kenmerken_id => {
            '-in' => $bibliotheek_kenmerken_ids
        }
    });

    # if there are no values stored that need deletion, we're done. nothing to do here
    my @current = $current->all
        or return;

    # a list with removed attributes. because checkboxes are stored as separate values
    # abuse a hash to ensure uniqueness.
    # using get_column to avoid subqueries - performance tweak
    my $deleted = { map { $_->get_column('bibliotheek_kenmerken_id') => 1 } @current };

    $current->delete;

    my $schema = $self->result_source->schema;

    my $logging = $schema->resultset('Logging');
    my $bibliotheek_kenmerken = $schema->resultset('BibliotheekKenmerken');

    # get the names of the deceased attributes
    my $attributes = join ", ", map { $_->naam } $bibliotheek_kenmerken->search({
        id => {
            '-in' => [keys $deleted]
        }
    })->all;

    # if you delete a bunch of fields, the logging line get too long
    # we need to solve this in the logging module, danger, danger
    $attributes = substr($attributes, 0, 100) . '...' if length $attributes > 100;

    $logging->trigger('case/attribute/removebulk', {
        component => 'kenmerk',
        zaak_id => $zaak_id,
        data => {
            attributes => $attributes,
            reason => 'verborgen door regels'
        }
    });
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 create_kenmerk

TODO: Fix the POD

=cut

=head2 create_kenmerken

TODO: Fix the POD

=cut

=head2 get

TODO: Fix the POD

=cut

=head2 log_field_create

TODO: Fix the POD

=cut

=head2 log_field_update

TODO: Fix the POD

=cut

=head2 replace_kenmerk

TODO: Fix the POD

=cut

=head2 strip_identical_fields

TODO: Fix the POD

=cut

=head2 update_field

TODO: Fix the POD

=cut

=head2 update_fields

TODO: Fix the POD

=cut

