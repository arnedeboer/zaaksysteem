package Zaaksysteem::Zaken::ResultSetBag;
use Moose;
extends 'DBIx::Class::ResultSet';

=head2 create_bag

Create a BAG entry

=cut

sub create_bag {
    my  $self   = shift;

    my $row     = $self->create(@_);

    $row->update_bag;

    return $row;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
