package Zaaksysteem::Zaken::Roles::RouteObjecten;
use Moose::Role;

=head1 NAME

Zaaksysteem::Zaken::Roles::RouteObjecten - A routing role

=cut

=head2 _bootstrap_route

Bootstraps the route of the case to whatever the case needs.

=cut

sub _bootstrap_route {
    my ($self, $opts) = @_;

    if (defined $opts->{role_id} && defined $opts->{ou_id}) {
        $self->wijzig_route({
            route_ou   => $opts->{ou_id},
            route_role => $opts->{role_id},
        });
    }
    elsif (defined $opts->{route_ou} && defined $opts->{route_role}) {
        $self->wijzig_route({
            route_ou   => $opts->{route_ou},
            route_role => $opts->{route_role},
        });
    }
    elsif (!$self->route_ou && !$self->route_role) {
        $self->wijzig_route({
            route_ou   => $self->huidige_fase->ou_id,
            route_role => $self->huidige_fase->role_id
        });
    }

    if (defined $opts->{assignee_id}) {
        $self->wijzig_behandelaar({'betrokkene_identifier' => $opts->{assignee_id}});
    }

    return 1;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
