package Zaaksysteem::Zaken::Roles::Acties;

use Moose::Role;
use Data::Dumper;
use DateTime::Format::Strptime;

use Zaaksysteem::Constants;
use Zaaksysteem::Tools;
use Zaaksysteem::Profiles;
use Zaaksysteem::Backend::Tools::Term qw/calculate_term/;

=head1 METHODS

=head2 wijzig_vernietigingsdatum(\%options)

Return value: $true_or_false

 $zaak->wijzig_vernietigingsdatum(
    {
        vernietigingsdatum  => '24-10-2012',
        reden               => VERNIETIGINGS_REDENEN,
    }
 );

B<Options>

=over 4

=item vernietigingsdatum

required

Status naam, e.g. deleted, open, new etc

=back

B<Rule method>: can_[function name]

=cut

{
    Params::Profile->register_profile(
        'method'    => 'wijzig_vernietigingsdatum',
        'profile'   => ZAAK_WIJZIG_VERNIETIGINGSDATUM_PROFILE,
    );

    sub can_wijzig_vernietigingsdatum {
        my $self    = shift;

        return 1;
    }

    sub wijzig_vernietigingsdatum {
        my $self        = shift;
        my $opts        = shift;

        my $dv          = Params::Profile->check(params => $opts);

        die('Parameters incorrect:' . Dumper($opts)) unless $dv->success;

        my $vernietigingsdatum_type = $dv->valid('vernietigingsdatum_type') || '';

        my $purge_date_type;

        if ($vernietigingsdatum_type eq 'bewaren') {
            my $bewaardagen = $dv->valid('vernietigingsdatum_recalculate');

            my $vernietigingsdatum  = DateTime->now()->add(days => $bewaardagen);
            $self->vernietigingsdatum($vernietigingsdatum);

            if (ZAAKSYSTEEM_OPTIONS->{BEWAARTERMIJN}{$bewaardagen} eq 'Bewaren') {
                $self->archival_state('overdragen');
                $purge_date_type = 'bewaren';
            } else {
                $self->archival_state('vernietigen');
                $purge_date_type = 'termijn';
            }
        } else {
            # Setting a specific date-of-destruction implies the archival state
            # should be set to "vernietigen".
            $self->vernietigingsdatum($dv->valid('vernietigingsdatum'));
            $self->archival_state('vernietigen');
        }

        if ($self->update) {
            $self->trigger_logging('case/update/purge_date', {
                component => LOGGING_COMPONENT_ZAAK,
                data => {
                    case_id => $self->id,
                    purge_date => $self->vernietigingsdatum->dmy,
                    purge_date_type => $purge_date_type,
                    reason => $dv->valid('reden')
                }
            });
        }
    }
}





=head2 wijzig_streefafhandeldatum(\%options)

Return value: $true_or_false

 $zaak->wijzig_streefafhandeldatum(
    {
        streefafhandeldatum => '24-10-2012',
    }
 );

B<Options>

=over 4

=item streefafhandeldatum

required

=cut

{
    Params::Profile->register_profile(
        'method'    => 'wijzig_streefafhandeldatum',
        'profile'   => ZAAK_WIJZIG_STREEFAFHANDELDATUM_PROFILE,
    );

    sub can_wijzig_streefafhandeldatum {
        my $self    = shift;

        return 1;
    }

    sub wijzig_streefafhandeldatum {
        my $self        = shift;
        my $opts        = shift;

        my $dv          = Params::Profile->check(params => $opts);

        die('Parameters incorrect:' . Dumper($opts)) unless $dv->success;

        $self->streefafhandeldatum($dv->valid('streefafhandeldatum'));

        if ($self->update) {
            $self->trigger_logging('case/update/target_date', {
                component => LOGGING_COMPONENT_ZAAK,
                data => {
                    case_id => $self->id,
                    target_date => $self->streefafhandeldatum->dmy
                }
            });
        }
    }
}

=back

=head2 wijzig_registratiedatum(\%options)

Return value: $true_or_false

 $zaak->wijzig_registratiedatum(
    {
        registratiedatum => '24-10-2012',
    }
 );

B<Options>

=over 4

=item registratiedatum

required

=cut

{
    Params::Profile->register_profile(
        'method'    => 'wijzig_registratiedatum',
        'profile'   => ZAAK_WIJZIG_REGISTRATIEDATUM_PROFILE,
    );

    sub can_wijzig_registratiedatum {
        my $self    = shift;

        return 1;
    }

    sub wijzig_registratiedatum {
        my $self        = shift;
        my $opts        = shift;

        my $dv          = Params::Profile->check(params => $opts);

        die('Parameters incorrect:' . Dumper($opts)) unless $dv->success;

        $self->registratiedatum($dv->valid('registratiedatum'));

        if ($self->update) {
            $self->trigger_logging('case/update/registration_date', {
                component => LOGGING_COMPONENT_ZAAK,
                data => {
                    case_id => $self->id,
                    registration_date => $self->registratiedatum->dmy
                }
            });
        }
    }
}

=back

=head2 wijzig_status(\%options)

Return value: $true_or_false

 $zaak->wijzig_status(
    {
        status      => 'deleted'
    }
 );

B<Options>

=over 4

=item status

required

Status naam, e.g. deleted, overdragen, open, new etc

=back

B<Rule method>: can_[function name]

=cut

define_profile wijzig_status => (
    required            => [qw/status/],
    optional            => [qw/
        reason
        suspension_term_type
        suspension_term_amount
        stalled_since
        stalled_until
    /],
    constraint_methods  => {
        status              => sub {
            my ($dfv, $value) = @_;

            my $statussen = ZAKEN_STATUSSEN;

            return grep { $_ eq $value } @{ $statussen };
        },
    },
    msgs        => PARAMS_PROFILE_DEFAULT_MSGS,
);

sub wijzig_status {
    my $self       = shift;
    my $arguments  = assert_profile(shift)->valid;
    my $new_status = $arguments->{status};

    throw('case/actions/set_status/invalid_status',
        'Deleting is not permitted through this method')
        if $new_status eq 'deleted';

    my $old_status = $self->status;
    return 1 if ($old_status eq $new_status);

    # Force status 'new' when the case isn't assigned to anyone specific, to
    # make the case to appear in the relevant intake.
    if ($old_status eq 'stalled' && !$self->behandelaar) {
        $new_status = 'new';
    }

    if ($old_status eq 'resolved') {
        $self->set_heropen;
        if ($self->afhandel_fase->status eq $self->milestone) {
            $self->milestone($self->milestone - 1);
        }
    }
    $self->set_gesloten if $new_status eq 'resolved';

    $self->status($new_status);

    # ### Make sure we have a coordinator
    if ($self->status eq 'open' && !$self->coordinator && $self->behandelaar) {
        $self->log->info('Status set to open and no coordinator set. Setting coordinator to: ' . $self->behandelaar_object->naam);
        $self->set_coordinator($self->behandelaar_object->betrokkene_identifier);
    }

    my $event = $self->trigger_logging('case/update/status', {
        component => LOGGING_COMPONENT_ZAAK,
        data => {
            case_id => $self->id,
            status  => $new_status,
        }
    });
    if ($old_status eq 'stalled') {
        $event = $self->resume($arguments)  if $old_status eq 'stalled';
    }
    elsif ($new_status eq 'stalled') {
        $event = $self->suspend($arguments) if $new_status eq 'stalled';
    }

    $self->update;

    return $event;
}

=head2 suspend

when stalling a case, notate the time so when it is resumed,
the new streefafhandeldatum can be calculated. when a case is
stalled, the clock stops.

also, if the case is scheduled for resume, notate the scheduled
resumption date.

=cut

define_profile suspend => (
    optional => [qw/
        reason
        suspension_term_type
        suspension_term_amount
    /],
);

sub suspend {
    my $self      = shift;
    my $arguments = assert_profile(shift)->valid;

    $self->stalled_since(DateTime->now);
    $self->reden_opschorten($arguments->{reason});

    my $logging_data = {
        case_id         => $self->id,
        reason          => ($arguments->{reason} || 'No reason given'),
        stalled_since   => $self->stalled_since->ymd
    };

    if ($arguments->{suspension_term_amount}) {
        my $stalled_until = calculate_term({
            start  => $self->stalled_since,
            amount => $arguments->{suspension_term_amount},
            type   => $arguments->{suspension_term_type}
        });

        $self->stalled_until($stalled_until);
        $logging_data->{stalled_until} = $stalled_until->ymd;
    }

    return $self->trigger_logging('case/suspend', { component => 'zaak', data => $logging_data});
}


=head2 _parse_date

Given a string, check for YYYY-MM-DD format, return DateTime object or throw.

=cut

sub _parse_date {
    my ($self, $date_string) = @_;

    return unless $date_string;

    my $datetime = assert_date($date_string);

    throw('case/suspend/date_in_future', 'Datum mag niet in toekomst liggen')
        unless $datetime->epoch <= DateTime->now->epoch;

    return $datetime;
}

=head2 resume

when the status is changed back from stalled - and it's not deleted -
see if the streefafhandeldatum can be adjusted.

also clear the scheduled resumption date, this will not have effect and
be noise.

=cut

define_profile resume => (
    required => [qw/status/],
    optional => [qw/reason stalled_since stalled_until/],
);

sub resume {
    my $self      = shift;
    my $arguments = assert_profile(shift)->valid;

    my $stalled_since = $self->_parse_date($arguments->{stalled_since})
        || $self->stalled_since;

    my $stalled_until = $self->_parse_date($arguments->{stalled_until})
        || $self->stalled_until || DateTime->now;

    # when resuming, the stalled period has to be added at the end of the legal term
    # for the case.
    if ($stalled_since) {
        my $stalled_period = $stalled_until - $stalled_since;

        $self->wijzig_streefafhandeldatum({
            streefafhandeldatum => $self->streefafhandeldatum + $stalled_period
        });
    }

    $self->stalled_until(undef);
    $self->reden_opschorten(undef);

    return $self->trigger_logging('case/resume', { component => 'zaak', data => {
        case_id => $self->id,
        reason => $arguments->{reason} || 'No reason given',
        stalled_since => ($stalled_since && $stalled_since->dmy) || '',
        stalled_until => $stalled_until->dmy
    }});
}


=head2 wijzig_fase(\%options)

Return value: $true_or_false

 $zaak->wijzig_fase(
    {
        milestone   => 1,
    }
 );

Om een of twee fasen terug te gaan. Kan niet gebruikt worden om naar een fase
in de toekomst te gaan. Dit kan enkel via het volgende_fase systeem.

B<Options>

=over 4

=item milestone

required

Milestone nummer to go to

=back

B<Rule method>: can_[function name]

=cut

{
    Params::Profile->register_profile(
        'method'    => 'wijzig_fase',
        'profile'   => PROFILE_WIJZIG_FASE
    );

    sub can_wijzig_fase {
        my $self    = shift;

        return 1;
    }


    sub wijzig_fase {
        my $self        = shift;
        my $opts        = shift;

        my $dv          = Params::Profile->check(params => $opts);

        die('Parameters incorrect:' . Dumper($opts)) unless $dv->success;

        my $milestone       = $dv->valid('milestone');

        ### milestone is larger than huidige fase
        if ($milestone < 1 || $milestone > $self->huidige_fase->status) {
            return;
        }

        ### Milestone is same as current fase, no change
        if ($milestone == $self->huidige_fase->status) {
            return 1;
        }

        if ( $self->is_afgehandeld) {
            $self->set_heropen;
        }

        $self->flush_cache;
        $self->_set_fase($milestone);

        my $new_status = $self->zaaktype_node_id->zaaktype_statuses->search({
            status => $milestone + 1
        })->first;

        $self->trigger_logging('case/update/milestone', {
            component => 'zaak',
            data => {
                case_id => $self->id,
                admin => 1,
                phase_id => $new_status->id
            }
        });

        return 1;
    }
}

=head2 wijzig_route(\%options)

Return value: $true_or_false

 $zaak->wijzig_route(
    {
        route_ou    => '20009',
        route_role  => '23001'
    }
 );

B<Options>

=over 4

=item route_ou

required

Organisational OU for zaak

=item route_role

required

Role of zaak

=item change_only_route_fields

optional

Will only change the route fields, and leave the behandelaar, status etc
alone.

=item force

Force a change of route: normally it wont change if a case is closed or
something.

=back

B<Rule method>: can_[function name]

=cut

{
    sub can_wijzig_route {
        my $self    = shift;

        return if $self->is_afgehandeld;

        return 1;
    }

    define_profile wijzig_route => %{PROFILE_WIJZIG_ROUTE()};
    sub wijzig_route {
        my ($self, $opts) = @_;
        $opts = assert_profile($opts)->valid;

        ### Niet mogelijk bij afgehandelde zaken
        unless (exists($opts->{force}) && $opts->{force}) {
            return if $self->is_afgehandeld;
        }

        my $route_ou    = $opts->{route_ou};
        my $route_role  = $opts->{route_role};

        return unless $route_ou && $route_role;

        $self->route_ou($route_ou);
        $self->route_role($route_role);

        my $group = $self->result_source->schema->resultset('Groups')->find($route_ou);
        my $role = $self->result_source->schema->resultset('Roles')->find($route_role);

        $self->trigger_logging('case/update/allocation', { component => 'zaak', data => {
            ou_name => $group ? $group->name : sprintf('afdeling-onbekend(%d)', $route_ou),
            role_name => $role ? $role->name : sprintf('rol-onbekend(%d)', $route_role),
            ou_id => $route_ou,
            role_id => $route_role
        }});

        unless ($opts->{change_only_route_fields}) {
            $self->wijzig_status({ status => 'new' });
            $self->behandelaar          (undef);
            $self->behandelaar_gm_id    (undef);
        }

        $self->update;
    }
}

=head2 wijzig_behandelaar(\%options)

Return value: $true_or_false

 $zaak->wijzig_behandelaar(
    {
        betrokkene_identifier => 'betrokkene-medewerker-1'
    }
 );

B<Options>

=over 4

=item betrokkene_identifier

required

identifier of betrokkene

=back

B<Rule method>: can_[function name]

=cut

{
    Params::Profile->register_profile(
        'method'    => 'wijzig_behandelaar',
        'profile'   => PROFILE_WIJZIG_BETROKKENE
    );

    sub can_wijzig_behandelaar {
        my $self    = shift;

        return if $self->is_afgehandeld;

        return 1;
    }

    sub wijzig_behandelaar {
        my $self        = shift;
        my $opts        = shift;

        my $dv          = Params::Profile->check(params => $opts);

        die('Parameters incorrect:' . Dumper($opts)) unless $dv->success;

        my $ok = try {
            return $self->_wijzig_betrokkene(
                'behandelaar',
                $dv->valid('betrokkene_identifier')
            );
        }
        catch {
            $self->log->warn(
                sprintf(
                    "Kon de behandelaar niet wijzigen voor zaak %d: %s",
                    $self->id, $_
                )
            );
            return 0;
        };
        return $ok;

    }

}

=head2 wijzig_aanvrager(\%options)

Return value: $true_or_false

 $zaak->wijzig_aanvrager(
    {
        betrokkene_identifier => 'betrokkene-natuurlijk_persoon-1'
    }
 );

B<Options>

=over 4

=item betrokkene_identifier

required

identifier of betrokkene

=back

B<Rule method>: can_[function name]

=cut

{
    Params::Profile->register_profile(
        'method'    => 'wijzig_aanvrager',
        'profile'   => PROFILE_WIJZIG_BETROKKENE
    );

    sub can_wijzig_aanvrager {
        my $self    = shift;

        return 1;
    }

    sub wijzig_aanvrager {
        my $self        = shift;
        my $opts        = shift;

        my $dv          = Params::Profile->check(params => $opts);

        die('Parameters incorrect:' . Dumper($opts)) unless $dv->success;

        return $self->_wijzig_betrokkene(
            'aanvrager',
            $dv->valid('betrokkene_identifier')
        );
    }

}

=head2 wijzig_coordinator(\%options)

Return value: $true_or_false

 $zaak->wijzig_coordinator(
    {
        betrokkene_identifier => 'betrokkene-medewerker-1'
    }
 );

B<Options>

=over 4

=item betrokkene_identifier

required

identifier of betrokkene

=back

B<Rule method>: can_[function name]

=cut

{
    Params::Profile->register_profile(
        'method'    => 'wijzig_coordinator',
        'profile'   => PROFILE_WIJZIG_BETROKKENE
    );

    sub can_wijzig_coordinator {
        my $self    = shift;

        return if $self->is_afgehandeld;

        return 1;
    }

    sub wijzig_coordinator {
        my $self        = shift;
        my $opts        = shift;

        my $dv          = Params::Profile->check(params => $opts);

        die('Parameters incorrect:' . Dumper($opts)) unless $dv->success;

        return $self->_wijzig_betrokkene(
            'coordinator',
            $dv->valid('betrokkene_identifier')
        );
    }

}

=head1 PRIVATE METHODS

=head2 _wijzig_betrokkene

Private helper method for wijzig_betrokkene

=cut

sub _wijzig_betrokkene {
    my ($self, $betrokkenetarget, $identifier) = @_;

    my $setmethod = sprintf('set_%s', $betrokkenetarget);

    return $self->$setmethod($identifier) && $self->_load_betrokkene_object($self->$betrokkenetarget);
}


=head2 undo(\%options)


 $zaak->undo_event({ event_id => $event_id});

B<Options>

=over 4

=item event_id

required

reference to logging table.

=back

=cut

sub can_undo {
    my ($self, $options) = @_;

    my $logging_id = $options->{logging_id} or die "need logging_id";

    my $logging_rs = $self->result_source->schema->resultset('Logging');

    # See if this is the latest event of this type on this case.
    my $logging_row = $logging_rs->find($logging_id) or return;

    # only case/accept is supported right now
    return unless $logging_row->event_type eq 'case/accept';

    my $case_id = $logging_row->data->{case_id};

    my $latest_logging_id = $logging_rs->search({
        zaak_id     => $case_id,
        event_type  => $logging_row->event_type,
    }, {
        order_by    => {'-desc' => 'id'}
    })->first->id;

    return unless $logging_id == $latest_logging_id;

    my $new_state = $logging_row->data->{new_state};

    my $current_state = {
        status      => $self->status,
        behandelaar => $self->behandelaar ? $self->behandelaar->id : undef,
        coordinator => $self->coordinator ? $self->coordinator->id : undef,
    };

    # Data::Dumper should be set to output keys in sorted order for this to work.
    return unless Dumper $current_state eq Dumper $new_state;

    return 1;
}


sub undo {
    my ($self, $options) = @_;

    my $logging_id = $options->{logging_id} or die "need logging_id";

    return unless $self->can_undo($options);

    my $logging_rs = $self->result_source->schema->resultset('Logging');
    my $logging_row = $logging_rs->find($logging_id)
        or return;

    my $event_type = $logging_row->event_type;

    if($event_type eq 'case/accept') {
        return $self->undo_accept({logging_row => $logging_row});
    } else {
        die "incorrect event_type";
    }
}


sub undo_accept {
    my ($self, $options) = @_;

    my $logging_row = $options->{logging_row} or die "need logging_row";

    my $former_state = $logging_row->data->{former_state};

    $self->status($former_state->{status});

    my $zaak_betrokkenen_rs = $self->result_source->schema->resultset('ZaakBetrokkenen');

    $self->behandelaar($former_state->{behandelaar});

    if($former_state->{behandelaar}) {
        # undelete the corresponding row
        my $row = $zaak_betrokkenen_rs->find($former_state->{behandelaar});
        if($row) {
            $row->deleted(undef);
            $row->update;
        }
    }

    $self->coordinator($former_state->{coordinator});

    if($former_state->{coordinator}) {
        # undelete the corresponding row
        my $row = $zaak_betrokkenen_rs->find($former_state->{coordinator});
        $row->deleted(undef);
        $row->update;
    }

    $self->update;
}


1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 LOGGING_COMPONENT_ZAAK

TODO: Fix the POD

=cut

=head2 PARAMS_PROFILE_DEFAULT_MSGS

TODO: Fix the POD

=cut

=head2 PROFILE_WIJZIG_BETROKKENE

TODO: Fix the POD

=cut

=head2 PROFILE_WIJZIG_FASE

TODO: Fix the POD

=cut

=head2 PROFILE_WIJZIG_ROUTE

TODO: Fix the POD

=cut

=head2 ZAAKSYSTEEM_OPTIONS

TODO: Fix the POD

=cut

=head2 ZAAK_WIJZIG_REGISTRATIEDATUM_PROFILE

TODO: Fix the POD

=cut

=head2 ZAAK_WIJZIG_STREEFAFHANDELDATUM_PROFILE

TODO: Fix the POD

=cut

=head2 ZAAK_WIJZIG_VERNIETIGINGSDATUM_PROFILE

TODO: Fix the POD

=cut

=head2 ZAKEN_STATUSSEN

TODO: Fix the POD

=cut

=head2 can_undo

TODO: Fix the POD

=cut

=head2 can_wijzig_aanvrager

TODO: Fix the POD

=cut

=head2 can_wijzig_behandelaar

TODO: Fix the POD

=cut

=head2 can_wijzig_coordinator

TODO: Fix the POD

=cut

=head2 can_wijzig_fase

TODO: Fix the POD

=cut

=head2 can_wijzig_registratiedatum

TODO: Fix the POD

=cut

=head2 can_wijzig_route

TODO: Fix the POD

=cut

=head2 can_wijzig_streefafhandeldatum

TODO: Fix the POD

=cut

=head2 can_wijzig_vernietigingsdatum

TODO: Fix the POD

=cut

=head2 undo_accept

TODO: Fix the POD

=cut

