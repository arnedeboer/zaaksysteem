package Zaaksysteem::Zaken::Roles::ZTT;

use Moose::Role;
use Zaaksysteem::Constants;

use Zaaksysteem::Object::Attribute;
use Zaaksysteem::Attributes;

use Zaaksysteem::Types::Filestore;

=head1 NAME

Zaaksysteem::Zaken::Roles::ZTT - Magic String handling and Zaaksysteem Template Toolkit

=head1 SYNOPSIS

    my @magic_strings = @{ $case->list_magic_strings };

    ### Returns
    # zaak_nummer, zaaktype_titel, [...]

    my $object_attributes = $case->object_attributes

    ### Returns an arrayref with L<Zaaksysteem::Object::Attribute> objects


=head1 DESCRIPTION

Component Magic Strings within zaaksysteem, technical documentation

=head1 ATTRIBUTES

=head2 object_attributes

Return value: ArrayRef[Zaaksysteem::Object::Attribute]

     my $object_attributes = $case->object_attributes

Returns a list of L<Zaaksysteem::Object::Attribute>, together with their values

=cut

has 'object_attributes'    => (
    'is'            => 'ro',
    'lazy'          => 1,
    'isa'           => 'ArrayRef[Zaaksysteem::Object::Attribute]',
    'builder'       => '_build_attribute_list',
    'clearer'       => 'clear_object_attributes'
);

=head2 casetype_attributes

This attribute builds a list of L<attributes|Zaaksysteem::Object::Attribute>
by processing L<casetype|Zaaksysteem::DB::Component::ZaaktypeNode>'s list of
associated L<kenmerken|Zaaksysteem::DB::Component::ZaaktypeKenmerken>.

=cut

has casetype_attributes => (
    is => 'ro',
    isa => 'ArrayRef[Zaaksysteem::Object::Attribute]',
    lazy => 1,
    default => sub {
        my $self = shift;
        my @magic_strings;

        my $kenmerken = $self->zaaktype_node_id->zaaktype_kenmerken->search(
            { 'me.is_group' => undef },
            { prefetch => 'bibliotheek_kenmerken_id' }
        );

        my %seen;

        while (my $kenmerk = $kenmerken->next) {
            my $bibliotheek_kenmerk = $kenmerk->bibliotheek_kenmerken_id;

            if ($kenmerk->object_id) {
                next;

                push @magic_strings, Zaaksysteem::Object::Attribute->new(
                    name => sprintf('object.%s', $kenmerk->object_id->get_object_attribute('prefix')->value),
                    attribute_type => 'object',
                    object_row => $kenmerk->object_id
                );

                next;
            }

            # kenmerken can occur multiple times in a casetype.
            next if !$bibliotheek_kenmerk->magic_string || $seen{$bibliotheek_kenmerk->magic_string};
            $seen{$bibliotheek_kenmerk->magic_string} = 1;

            my $type = $bibliotheek_kenmerk->value_type;
            my $type_definition = ZAAKSYSTEEM_CONSTANTS->{veld_opties}->{$type};

            my $magic_string = Zaaksysteem::Object::Attribute->new(
                bwcompat_name  => $bibliotheek_kenmerk->magic_string,
                label          => $kenmerk->label || $bibliotheek_kenmerk->label || $bibliotheek_kenmerk->naam,
                name           => "attribute." . $bibliotheek_kenmerk->magic_string,
                attribute_type => $type_definition->{ object_search_type } // 'text',
                object_table   => 'zaaktype_kenmerken',
                object_id      => $kenmerk->id,
                object_row     => $kenmerk,
            );

            push @magic_strings, $magic_string;
        }

        return \@magic_strings;
    }
);

=head2 magic_strings

This attribute builds a map of magic strings to their respective values. The
keys for this map are based off the
L<Zaaksysteem::Object::Attribute/get_bwcompat_name> method and the attribute
name, so expect the same value to pop up multiple times when iterating over
the map carelessly.

=cut

has magic_strings => (
    is            => 'ro',
    lazy          => 1,
    default       => sub {
        my $self = shift;

        my $attributes = $self->object_attributes;

        my $magic_strings = {};

        for my $attribute (@$attributes) {

            my @aliases = ($attribute->name, $attribute->get_bwcompat_name);
            my $human_value = $attribute->human_value;

            $magic_strings->{$_} = $human_value for @aliases;
        }

        return $magic_strings;
    }
);

=head1 METHODS

=head2 systeemkenmerk

Convenience method that dereferences L</magic_strings> based on the provided
key.

    my $value = $case->systeemkenmerk('case.number');

=cut

sub systeemkenmerk {
    my ($self, $label) = @_;

    return $self->magic_strings->{$label};
}

=head2 get_string_fetchers

This method implements a behavior required for the case object's usage as
a source of data for L<Zaaksysteem::ZTT>. It builds a list of coderefs that
take a 'magic string' and return a value if the fetcher is familiar with the
key.

    my @fetchers = $case->get_string_fetchers;

    for my $f (@fetchers) {
        my $v = $f->($magic_string);

        return $v if defined $v;
    }

B<Note>: this method is public to a case object, but should really only be
used by L<ZTT|Zaaksysteem::ZTT>.

=cut

sub get_string_fetchers {
    my $self = shift;

    my @fetchers;

    # Involved subjects
    push @fetchers, sub {
        my $tag = shift;

        my $betrokkenen = $self->zaak_betrokkenen->search_gerelateerd;

        # Usual magic strings here look like 'aanvrager_naam'
        my ($prefix, $tagname) = split m[_], $tag->name;

        # TODO remove this iterator, search the db directly for related subjects
        # that match the prefix
        for my $betrokkene ($betrokkenen->all) {
            next unless $prefix eq $betrokkene->magic_string_prefix;

            return Zaaksysteem::ZTT::Element->new(
                value => (ZAAKSYSTEEM_BETROKKENE_SUB->(
                    $self->betrokkene_object({ magic_string_prefix => $prefix }),
                    $tagname
                ) || '')
            );
        }
    };

    # Case attributes
    push @fetchers, sub {
        my $tag = shift;

        my $attribute = $self->zaaktype_node_id->zaaktype_kenmerken->search(
            { 'library_attribute.magic_string' => $tag->name },
            { join => 'library_attribute' }
        )->first;

        return unless $attribute;

        my $library_id = $attribute->get_column('bibliotheek_kenmerken_id');
        my $source = ($attribute->referential && $self->pid) ? $self->pid : $self;

        my $field_values = $source->field_values({
            bibliotheek_kenmerken_id => $library_id
        });

        return Zaaksysteem::ZTT::Element->new(
            attribute => $attribute,
            value => $field_values->{ $library_id }
        );
    };

    # Simple case, translate systemattributes
    push @fetchers, sub {
        my $tagname = shift->name;

        my $value = $self->systeemkenmerk($tagname);

        return unless $value || $tagname eq 'null';

        my %params = (value => $value);
        $params{type} = 'image' if $tagname eq 'behandelaar_handtekening';

        return Zaaksysteem::ZTT::Element->new(%params);
    };

    return @fetchers;
}

=head2 get_context_iterators

This method builds a map of 'subcontexts' for use by L<Zaaksysteem::ZTT>.
Basically, it tells the template processor the case is dereferenceable, in
the context of a L<ZTT|Zaaksysteem::ZTT> template, allowing the engine to
'unnest' any 1:N relations the case may have, and use them as data sources
as well.

B<Note>: this method is public to a case object, but should really only be
used by L<ZTT|Zaaksysteem::ZTT>.

=cut

sub get_context_iterators {
    my $self = shift;

    return {
        zaak_relaties => sub {
            return [ map { $_->case } $self->zaak_relaties ];
        },

        'case.related_cases' => sub {
            return [ map { $_->case } $self->zaak_relaties ];
        }
    };
}

=head2 _build_attribute_list

See L</object_attributes>.

=cut

sub _build_attribute_list {
    my $self   = shift;
    my $object = $self->object_data;

    # It is deleted, so no use in doing all the other bits
    return [] if $self->is_deleted;
    throw("ZTT/object_data/missing", "Object data missing") if (!$object);

    my $kenmerken = $self->field_values;

    my @attributes = (
        @{ $self->casetype_attributes },
        Zaaksysteem::Attributes::predefined_case_attributes
    );

    for my $magic_string (@attributes) {
        $magic_string->parent_object($object);

        if ($magic_string->is_systeemkenmerk) {
            my $value = $magic_string->systeemkenmerk_reference->($self);

            $magic_string->value($value);
        } elsif ($magic_string->attribute_type eq 'object') {
            my $rels1 = $self->object_data->object_relationships_object1_uuids->search({
                object2_type => $magic_string->object_row->get_object_attribute('prefix')->value
            });

            my $rels2 = $self->object_data->object_relationships_object2_uuids->search({
                object1_type => $magic_string->object_row->get_object_attribute('prefix')->value
            });

            for my $rel1 ($rels1->all) {
                warn Data::Dumper::Dumper($rel1->object2_uuid->TO_JSON);
            }

            for my $rel2 ($rels2->all) {
                warn Data::Dumper::Dumper($rel2->object1_uuid->TO_JSON);
            }

            $magic_string->value([]);
        } elsif ($magic_string->object_table eq 'zaak_betrokkenen') {
            ### XXX TODO, FIX THIS

        } elsif ($magic_string->object_table eq 'zaaktype_kenmerken') {
            my $bibliotheek_kenmerk = $magic_string->object_row->bibliotheek_kenmerken_id;

            my $value;

            if($bibliotheek_kenmerk->value_type eq 'file') {
                my $case_files = $self->file_field_documents($bibliotheek_kenmerk->id);

                my @files;
                for my $case_file (@$case_files) {
                    push(
                        @files,
                        Zaaksysteem::Types::Filestore->new(
                            map ({ $_ => ($case_file->{ $_ } || undef) } @{ $Zaaksysteem::Types::Filestore::ATTRIBUTES })
                        )
                    );
                }

                $value = \@files;
            } else {
                $value = $kenmerken->{ $bibliotheek_kenmerk->id };
            }

            my $filtered = $bibliotheek_kenmerk->filter($value);

            $magic_string->value($filtered);
        }
    }

    $self->object_data->replace_object_attributes(@attributes);

    return \@attributes;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 ZAAKSYSTEEM_BETROKKENE_SUB

TODO: Fix the POD

=cut

=head2 ZAAKSYSTEEM_CONSTANTS

TODO: Fix the POD

=cut

