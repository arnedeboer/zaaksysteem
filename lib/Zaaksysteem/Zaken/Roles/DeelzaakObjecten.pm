package Zaaksysteem::Zaken::Roles::DeelzaakObjecten;

use Moose::Role;
use Data::Dumper;


sub set_relatie {
    my $self    = shift;
    my $opts    = shift;


    die('Missing arguments') unless (
        $opts->{relatie} &&
        $opts->{relatie_zaak}
    );

    my  $relatie_zaak   = $opts->{relatie_zaak};

    if (!ref($opts->{relatie_zaak})) {
        die('Geen zaak_id of geen nummer') unless (
            $opts->{relatie_zaak} &&
            $opts->{relatie_zaak} =~ /^\d+$/
        );

        $relatie_zaak   = $self->result_source
            ->schema
            ->resultset('Zaak')
            ->find($opts->{relatie_zaak})
            or die('Zaak kan niet gevonden worden');
    }

    if ($opts->{relatie} && $opts->{relatie} eq 'gerelateerd') {
        $self->result_source->schema->resultset('CaseRelation')->add_relation(
            $self->id, $relatie_zaak->id
        );
    }

    if (
        $opts->{relatie} &&
        (
            $opts->{relatie} eq 'vervolgzaak' ||
            $opts->{relatie} eq 'vervolgzaak_datum'
        )
    ) {
        my $relation = $self->result_source->schema->resultset('CaseRelation')->add_relation(
            $self->id, $relatie_zaak->id
        );

        $relation->view_in_context($self->id)->type('initiator');
        $relation->view_in_context($relatie_zaak->id)->type('continuation');

        $relation->update;
    }

    if ($opts->{relatie} && $opts->{relatie} eq 'deelzaak') {
        $self->pid($relatie_zaak->id);
    }


    if ($opts->{'actie_kopieren_kenmerken'}) {
        my $kenmerken       = $relatie_zaak->field_values();

        my $deelzaak_kenmerken = $self->field_values();

#        warn "Deelzaak kenmerken: " . Dumper $deelzaak_kenmerken;
        # Self = the newly created zaak. Kenmerken from the relatie_zaak need to be copied

        if (scalar(keys %{ $kenmerken })) {
            while (my ($kenmerk_id, $kenmerk_value) = each %{ $kenmerken }) {
                $self->zaak_kenmerken->create_kenmerk({
                    zaak_id                     => $self->id,
                    bibliotheek_kenmerken_id    => $kenmerk_id,
                    values                      => $kenmerk_value,
                });
            }

            $self->touch();
        }
    }

    $self->update;
}



sub register_required_subcase {
    my ($self, $args) = @_;

    die "need subcase_id" unless $args->{subcase_id};
    die "need required"   unless $args->{required}; # name should be required_fase

    $self->zaak_subcases->create({
        relation_zaak_id        => $args->{subcase_id},
        required                => $args->{required},
        parent_advance_results  => $args->{parent_advance_results},
    });
}

around can_volgende_fase => sub {
    my $orig    = shift;
    my $self    = shift;

    my $advance_result = $self->$orig(@_);

    my $unfinished = $self->required_subcases_unfinished();

    if (scalar keys %$unfinished) {
        $advance_result->fail('subcases_complete');
    } else {
        $advance_result->ok('subcases_complete');
    }

    return $advance_result;
};

=head2 required_subcases_unfinished

Find out wether this case has any subcases that still have to be finished.
This is only active if for the relationship the 'required' attribute
has been set.

=cut

sub required_subcases_unfinished {
    my $self = shift;

    # list with related subcases
    my $related_cases = {};

    ### Check if this is afhandelfase
    my $afhandelfase = 0;
    $afhandelfase++ if $self->is_volgende_afhandel_fase;

    # required casetypes
    my $required_casetypes = {};

    foreach my $relation (qw/zaak_children/) {
        my $rs = $self->$relation->search({
            'me.status' => { -not_in => ['resolved', 'overdragen', 'deleted'] },
        });

        while(my $row = $rs->next()) {
            my $zaaktype_id = $row->zaaktype_id->id;

            push @{
                $related_cases->{$zaaktype_id} ||= []
            }, $row->id;

            ### Afhandelfase, EVERY child is required
            if ($afhandelfase && $relation eq 'zaak_children') {
                $required_casetypes->{$zaaktype_id} = 1;
            }
        }
    }

    my $is_afhandel_fase = $self->is_afhandel_fase;

    # get child cases
    my $rs = $self->zaak_children->search({
        'me.status' => { -not_in => ['resolved','overdragen','deleted'] },
    });

    while(my $row = $rs->next()) {
        my $zaaktype_id = $row->zaaktype_id->id;

        push @{
            $related_cases->{$zaaktype_id} ||= []
        }, $row->id;

        ### Child case is ALWAYS required when in last phase
        $required_casetypes->{$zaaktype_id} = 1 if $is_afhandel_fase;
    }

    my $zaaktype_required_relaties = $self->zaaktype_id->zaaktype_node_id->zaaktype_relaties->search({
        required => $self->milestone + 1,
    });

    while(my $row = $zaaktype_required_relaties->next()) {
        my $zaaktype_id = $row->relatie_zaaktype_id->id;

        if(exists $related_cases->{$zaaktype_id}) {
            $required_casetypes->{$zaaktype_id} = 1;
        }
    }

    # required subcases
    my $required_zaak_subcases_rs = $self->zaak_subcases->search({
        required => $self->milestone + 1
    });

    while(my $row = $required_zaak_subcases_rs->next()) {
        my $zaaktype_id = $row->relation_zaak_id->zaaktype_node_id->zaaktype_id->id;

        unless(
            grep(
                { $row->relation_zaak_id->status eq $_ }
                qw/
                resolved
                deleted
                overdragen
                /
            )
        ) {
            $required_casetypes->{$zaaktype_id} = 1;
        }
    }

    return $required_casetypes;
}

sub hierarchy {
    my $self = shift;

    my $iter = $self;

    # After this loop, $iter will be the topmost parent,
    # from which we'll unroll the entire structure.
    while($iter->get_column('pid')) {
        $iter = $iter->pid;
    }

    return $self->unroll_hierarchy_level($iter);
}

sub unroll_hierarchy_level {
    my $self = shift;

    my $node = shift;

    return {
        case => $node,
        children => [
            map { $self->unroll_hierarchy_level($_) } $node->zaak_pids
        ]
    };
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 hierarchy

TODO: Fix the POD

=cut

=head2 register_required_subcase

TODO: Fix the POD

=cut

=head2 set_relatie

TODO: Fix the POD

=cut

=head2 unroll_hierarchy_level

TODO: Fix the POD

=cut

