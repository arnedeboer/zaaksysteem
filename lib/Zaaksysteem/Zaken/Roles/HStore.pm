package Zaaksysteem::Zaken::Roles::HStore;
use Moose::Role;

=head1 NAME

Zaaksysteem::Zaken::Roles::HStore - Role that adds hstore column updating ability.

=head1 METHODS

=head2 update_hstore

Update the "hstore_properties" column with the value of the
C<object_attributes> attribute.

=cut

sub update_hstore {
    my $self = shift;

    my $case_object = $self->object_data;

    return if not $case_object;

    $case_object->class_uuid($self->zaaktype_id->_object->uuid);

    $self->clear_object_attributes();
    $case_object->replace_object_attributes(@{ $self->object_attributes });

    $self->_update_object_relationships($case_object);
    $self->_update_object_acl($case_object);

    $case_object->update;
}

=head2 _update_object_acl

This method will update the L<Zaaksysteem::Schema::ObjectAclEntry>s associated
with the case instance. It does a destructive replace of all ACL rules with
freshly generated rules.

=cut

# XXX TODO
# That said, this thing can be performance buzzkill in the long run, we can
# update more efficiently by just saving the diff to the rules (hell, lets
# start by not updating if nothing's changed)

sub _update_object_acl {
    my $self = shift;
    my $case_object = shift;

    $self->result_source->schema->txn_do(sub {
        $case_object->object_acl_entries->delete_all;

        my @entries;

        # Iterator over aanvrager, coordinator and behandelaar objects
        # Skip unless defined, or skip when btype != medewerker
        # Store in hashmap indexed on betrokkene_identifier, so we don't
        # generate double entries
        my %subjects = map { $_->betrokkene_identifier => $_ } grep
            { defined && $_->btype eq 'medewerker' }
            (
                $self->aanvrager_object,
                $self->coordinator_object,
                $self->behandelaar_object
            );

        # Iterator over involved subjects, creating appropriate
        # ACL entries along the way.
        for my $security_id (keys %subjects) {
            push @entries, $case_object->grant($subjects{ $security_id }, capabilities => [qw[read write]]);
        }

        $case_object->acl_groupname($self->confidentiality eq 'confidential' ? 'confidential' : 'public');
    });
}

=head2 _update_object_relationships

This method will update all L<Zaaksysteem::Schema::ObjectRelationships>
associated with the case instance. It does a destructive replace of all
existing relations, creating a fresh set from the currently available case
state data.

=cut

# XXX TODO
# That said, this thing can be performance buzzkill in the long run, we can
# update more efficiently by just saving the diff to the rules (hell, lets
# start by not updating if nothing's changed)

sub _update_object_relationships {
    my $self = shift;
    my ($case_object) = @_;

    my $schema = $self->result_source->schema;

    # Delete all *Case* relationships to other cases (don't worry, we're going to re-create them).
    $case_object->object_relationships_object1_uuids->search({ object2_type => 'case' })->delete;
    $case_object->object_relationships_object2_uuids->search({ object1_type => 'case' })->delete;

    # Parent/children
    my $parent = $self->pid;
    if($parent && !$parent->is_deleted) {
        my $parent_object = $parent->object_data;
        $schema->resultset('ObjectRelationships')->create(
            {
                object1_uuid => $parent_object,
                object1_type => $parent_object->object_class,
                type1        => 'parent',

                owner_object_uuid => $case_object->uuid,

                object2_uuid => $case_object,
                object2_type => $case_object->object_class,
                type2        => 'child',
            },
        );
    }

    my $children = $self->zaak_children->search;
    while(my $child = $children->next) {
        next if $child->is_deleted;

        my $child_object = $child->object_data;
        $schema->resultset('ObjectRelationships')->create(
            {
                owner_object_uuid => $case_object->uuid,

                object1_uuid => $case_object,
                object1_type => $case_object->object_class,
                type1        => 'parent',

                object2_uuid => $child_object,
                object2_type => $child_object->object_class,
                type2        => 'child',
            },
        );
    }

    # Other case relationships
    my $case_relations = $schema->resultset('CaseRelation')->search({ case_id => $self->id });
    while (my $cr = $case_relations->next) {
        if ($cr->get_column('case_id_a') == $self->id) {
            # We're A
            my $b_object = $cr->case_id_b->object_data;
            next if not $b_object;

            $schema->resultset('ObjectRelationships')->create(
                {
                    object1_uuid => $case_object->id,
                    object1_type => $case_object->object_class,
                    type1        => $cr->type_a,

                    object2_uuid => $b_object,
                    object2_type => $b_object->object_class,
                    type2        => $cr->type_b,
                },
            );
        }
        else {
            # We're B
            my $a_object = $cr->case_id_a->object_data;
            next if not $a_object;

            $schema->resultset('ObjectRelationships')->create(
                {
                    object1_uuid => $a_object,
                    object1_type => $a_object->object_class,
                    type1        => $cr->type_a,

                    object2_uuid => $case_object->id,
                    object2_type => $case_object->object_class,
                    type2        => $cr->type_b
                },
            );
        }
    }

    return;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
