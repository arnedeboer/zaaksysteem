package Zaaksysteem::Zaken::Roles::FaseObjecten;
use Moose::Role;

use DateTime;
use DateTime::Format::ISO8601;

use Email::Valid;
use List::MoreUtils qw/all/;

use Zaaksysteem::Tools;
use Zaaksysteem::ZTT;

use Zaaksysteem::Zaken::AdvanceResult;
use Zaaksysteem::Constants qw/ZAAKSYSTEEM_OPTIONS/;

with 'Zaaksysteem::Zaken::Roles::ZaakSetup';

sub zaak_cache {
    my $self           = shift;
    my $caching_object = shift;

    my $calling_sub = [caller(1)]->[3];

    if ($caching_object) {
        $self->{_zaak_cache} = {}
          unless $self->{_zaak_cache};

        return ($self->{_zaak_cache}->{$calling_sub} = $caching_object);
    }

    if ($self->{_zaak_cache}) {
        return $self->{_zaak_cache}->{$calling_sub}
          if $self->{_zaak_cache}->{$calling_sub};
    }

    return;
}

sub flush_cache {
    my $self            = shift;

    return unless $self->{_zaak_cache};

    delete($self->{_zaak_cache});
}

sub _set_fase {
    my $self        = shift;
    my $milestone   = shift;

    return unless $milestone;

    $self->milestone($milestone);
    $self->flush_cache;
    $self->update;
}


sub set_volgende_fase {
    my $self            = shift;

    my $volgende_fase   = $self->volgende_fase;
    return unless $volgende_fase;

    my $milestone = $volgende_fase->status;

    $self->_set_fase($milestone) or return;

    if ($self->is_afhandel_fase) {
        $self->set_gesloten;
    } else {
        $self->logging->trigger('case/update/milestone', {
            component => 'zaak',
            data => {
                case_id => $self->id,
                phase_id => $self->huidige_fase->id
            }
        });
    }

    $self->flush_cache;
    return 1;
}


sub set_vorige_fase {
    my $self            = shift;

    my $vorige_fase     = $self->vorige_fase;

    return unless $vorige_fase;

    return unless $self->can_vorige_fase;

    if ( $self->is_afgehandeld) {
        $self->set_heropen;
    }

    $self->flush_cache;
    $self->_set_fase($vorige_fase->status);
}

sub set_heropen {
    my $self            = shift;

    my $status = $self->status;

    if ($status !~ /^(?:resolved|overdragen|stalled)$/) {
        return
    }

    if ($status =~ /^(?:resolved|overdragen)$/) {
        $self->afhandeldatum(undef);
        $self->vernietigingsdatum(undef);
    }

    $self->status('open');

    $self->logging->trigger('case/reopen', { component => 'zaak', data => {
        case_id => $self->id
    }});

    $self->flush_cache;
    $self->update;
}


sub set_gesloten {
    my $self = shift;
    my $time = shift;

    $time ||= DateTime->now;

    $self->afhandeldatum($time);
    $self->set_vernietigingsdatum;

    if ($self->status !~ /^(?:resolved|overdragen)$/) {
        $self->status('resolved');
    }

    $self->flush_cache;

    $self->logging->trigger(
        'case/close',
        {
            component => 'zaak',
            data      => {
                case_id     => $self->id,
                timestamp   => $time->datetime,
                case_result => $self->resultaat || '',
            }
        }
    );

    $self->update;
}

=head2 set_vernietigingsdatum

Apply a destruction date on a case.

If there is no result the destruction date is 1 year in the future.
If there is a result the vernietigingsdatum will be set according to the rules of the resulttype.

=head3 RETURNS

undef if there is no 'afhandeldatum'.
Returns a DateTime object if successful.
Dies in case of an error.

=cut

sub set_vernietigingsdatum {
    my $self = shift;
    my $afhandeldatum = $self->afhandeldatum;

    return undef if !$afhandeldatum;

    my $afhandel_clone = $afhandeldatum->clone();

    # Geen resultaat: 1 year default
    unless ($self->resultaat) {
        return $self->vernietigingsdatum($afhandel_clone->add(years => 1));
    }

    my $resultaat = $self->zaaktype_node_id->zaaktype_resultaten->search(
        {resultaat => $self->resultaat}
    )->first;

    if (!$resultaat) {
        throw(
            'case/set_vernietigingsdatum',
            sprintf("Unable to find resultaat '%s' while it is defined", $self->resultaat),
        );
    }

    my $dt = $afhandeldatum->clone();
    $dt->add(days => $resultaat->bewaartermijn);

    if (   ZAAKSYSTEEM_OPTIONS->{BEWAARTERMIJN}->{$resultaat->bewaartermijn} eq 'Bewaren'
        && $self->status ne 'overdragen'
    ) {
        $self->status('overdragen');
    }
    elsif (   ZAAKSYSTEEM_OPTIONS->{BEWAARTERMIJN}->{$resultaat->bewaartermijn} ne 'Bewaren'
           && $self->status eq 'overdragen'
    ) {
        $self->status('resolved');
    }

    my $vd = $self->vernietigingsdatum;
    if (!$vd || $dt ne $vd) {
        $self->vernietigingsdatum($dt);
        $self->logging->trigger('case/update/purge_date', {
                component => 'zaak',
                data      => {
                    purge_date => $dt->dmy,
                    case_id    => $self->id,
                },
            }
        );
    }
    return $self->vernietigingsdatum;
}


sub fasen {
    my $self    = shift;

    return $self->zaaktype_node_id->zaaktype_statussen(
        undef,
        {
            order_by    => { -asc   => 'status' }
        }
    );
}


sub huidige_fase {
    my $self    = shift;

    return $self->zaak_cache if $self->zaak_cache;

    return $self->zaak_cache($self->zaaktype_node_id->zaaktype_statussen->search({
        status  => $self->milestone,
    })->first);
}


sub volgende_fase {
    my $self    = shift;

    return $self->zaak_cache if $self->zaak_cache;

    return $self->zaak_cache($self->zaaktype_node_id->zaaktype_statussen->search({
        status  => ($self->milestone + 1)
    })->first);
}


sub vorige_fase {
    my $self    = shift;

    return $self->zaak_cache if $self->zaak_cache;

    return $self->zaak_cache($self->zaaktype_node_id->zaaktype_statussen->search({
        status  => ($self->milestone - 1)
    })->first);
}


sub registratie_fase {
    my $self    = shift;

    # TODO: no return $self->zaak_cache here?

    return $self->zaak_cache($self->zaaktype_node_id->zaaktype_statussen->search(
        undef,
        {
            order_by    => { -asc => 'status' },
            rows        => 1,
        }
    )->first)
}


sub afhandel_fase {
    my $self    = shift;

    return $self->zaak_cache if $self->zaak_cache;

    return $self->zaak_cache($self->zaaktype_node_id->zaaktype_statussen->search(
        undef,
        {
            order_by    => { -desc => 'status' },
            rows        => 1,
        }
    )->first);
}

=head2 is_in_phase($phase)

my $bool = $case->is_in_phase('registratie_fase');

Check if a zaak is in the specified fase (phase).

=head3 ARGUMENTS

=over

=item phase [REQUIRED]

=back

=head3 RETURNS

A boolean, 1 if true, 0 if false

=cut

sub is_in_phase {
    my ($self, $phase) = @_;

    if (!$self->can($phase)) {
        throw(
            'case/phase',
            "Unable to determine phase '$phase' for $self"
        );
    }

    return $self->$phase->status eq $self->huidige_fase->status;
}

sub is_afhandel_fase {
    my $self    = shift;
    return $self->is_in_phase('afhandel_fase');

    if ($self->afhandel_fase->status eq $self->huidige_fase->status) {
        return 1;
    }

    return;
}

sub is_afgehandeld {
    my $self    = shift;

    return 1 if ($self->status eq 'resolved');
    return 1 if ($self->status eq 'overdragen');

    if ($self->afhandel_fase->status eq $self->milestone) {
        return 1;
    }

    return;
}


sub is_open {
    my $self    = shift;

    return 1 if ($self->status =~ /new|open/);
    return;
}



sub is_volgende_afhandel_fase {
    my $self    = shift;

    return unless $self->volgende_fase;

    if ($self->afhandel_fase->status eq $self->volgende_fase->status) {
        return 1;
    }

    return;
}


=head2 can_volgende_fase

Determine wether the case is ready for the next move. This routine is expanded (using
around) throughout the other roles for the class. The results are bundled together in
an object, as to allow specific feedback on which items are not ready yet.

=cut

sub can_volgende_fase {
    my $self = shift;
    my $object_model = shift;

    my $advance_result = Zaaksysteem::Zaken::AdvanceResult->new;

    if ($self->is_volgende_afhandel_fase && not $self->resultaat) {
        $advance_result->fail('result_complete', 'No result set');
    } else {
        $advance_result->ok('result_complete');
    }

    # Key2Finance calls us, but has no way of accessing a model instance. A
    # reference to the model can't be hidden in the default_attributes of the
    # schema either , that'll cause circuclar references, and cleanup is a
    # mess.
    # XXX Find workaround
    unless (defined $object_model) {
        warn "can_volgende_fase called without ZS::Object::Model instance, not all checks have executed.";

        $advance_result->ok('object_mutations_complete');

        return $advance_result;
    }

    $advance_result->ok('object_mutations_complete');

    for my $mutation ($self->object_data->object_mutation_lock_object_uuids) {
        try {
            $object_model->validate_mutation($mutation);
        } catch {
            $advance_result->fail('object_mutations_complete');
        };
    }

    return $advance_result;
}

before can_volgende_fase => sub {
    my $self = shift;
    $self->log->debug("!! Start can_volgende_fase");
};

after can_volgende_fase => sub {
    my $self = shift;
    $self->log->debug("End can_volgende_fase");
};

sub can_vorige_fase {
    my $self    = shift;

    return 1;
}

=head2 advance

Move to the next phase, or already in last phase, close case.
Then perform phase transition actions.

This sub is written to accomodate for the following scenario:
- a subcase if finished
- it signals its parent that it's finished
- the parent case is advanced to the next phase
- all default phase actions, as configured in the 'zaaktype' are fired.

=cut

sub advance {
    my ($self, $opts) = @_;

    # evil; to be EXTERMINATED asap!
    my $c = $opts->{context} or die 'need context ($c) object';

    my $advance_result = $self->can_volgende_fase($c->model('Object'));

    unless ($advance_result->can_advance) {
        throw(
            'case/advance',
            'Unable to advance case due to can_volgende_fase checks',
            $advance_result
        );
    }

    $self->send_external_system_messages(
        base_url => $c->uri_for('/')->as_string
    );

    $self->set_volgende_fase;

    return $self->fire_phase_actions({ context => $c });
}


=head2 send_external_system_messages

Before advancing, see if the rule demand messages to be sent in this phase.

To test:
Create a casetype with a external message rule.
Create a case.
Create an interface.

$case->send_external_system_messages;

=cut

sub send_external_system_messages {
    my $self = shift;
    my %args = @_;

    my $rules_result = $self->execute_rules({status => $self->milestone + 1});

    if ($rules_result && $rules_result->{send_external_system_message}) {
        for my $message (@{ $rules_result->{send_external_system_message} }) {
            my $interface = $self->result_source->schema->resultset('Interface')->search_active({
                module => $message->{type}
            })->first or throw("api/interface_not_found", "Koppeling $message->{type} is niet beschikbaar");

            # Interfaces that support external system messages will need to have a
            # trigger that supports this API
            $interface->process_trigger('PostStatusUpdate', {
                case_id    => $self->id,
                kenmerken  => $self->field_values,
                message    => $message,
                base_url   => $args{base_url},
                statusText => $message->{message},
                statusCode => $message->{status},
            });
        }
    }
}

sub fire_phase_actions {
    my ($self, $options) = @_;

    my $c = $options->{context} or die "need context";

    my $actions_rs = $self->case_actions_cine->current->active->sorted;

    my @flash_messages;

    my $allocation_action = 0;

    while(my $action = $actions_rs->next()) {
        $allocation_action ||= $action->type eq 'allocation';

        push @flash_messages, $self->fire_action({
            context => $c,
            action  => $action,
        });;
    }

    my $redirect = $allocation_action > 0 || $self->is_afgehandeld;

    return {
        redirect_to_dashboard => $redirect,
        flash_messages => \@flash_messages
    };
}


=head2 fire_action

Return 1 when allocation action performed - redirect to dashboard is necessary

=cut

sub fire_action {
    my ($self, $options) = @_;

    my $action  = $options->{action} or die "need action";
    my $context = $options->{context} or die "need context";

    my $type    = $action->type or die "need action type";

    if($type eq 'email') {
        return $self->mail_action({
            case         => $action->case_id,
            action_data  => $action->data,
            email_sender => Zaaksysteem::Backend::Mailer->new
        });
    } elsif($type eq 'template') {
        $self->template_action({
            context         => $context,
            case_action     => $action,
        });

        return "Sjabloon aangemaakt";
    } elsif($type eq 'allocation') {
        $self->allocation_action({
            ou_id   => $action->data->{ou_id},
            role_id => $action->data->{role_id},
            change_only_route_fields => $options->{change_only_route_fields},
        });

        return "Zaak toegewezen";
    } elsif($type eq 'case') {
        my $subcase_event = $self->start_subcase({
            context => $context,
            case_action => $action,
        });

        return $subcase_event->onderwerp;
    } elsif($type eq 'object_mutation') {
        my $mutations = $self->object_data->object_mutation_lock_object_uuids->search({
            object_type => $action->data->{ object_type_prefix }
        });

        my @messages;

        for my $mutation ($mutations->all) {
            try {
                my $data = $context->model('Object')->mutate($mutation);

                $mutation->delete;

                $data->{ object_type_name } = $action->data->{ object_type_name };

                my $event = $self->logging->trigger(sprintf('case/object/%s', $mutation->type), {
                    object_uuid => delete $data->{ object_uuid },
                    data => $data
                });

                push @messages, $event->onderwerp;
            } catch {
                $mutation->update({ executed => 1 });

                push @messages, {
                    type => 'error',
                    message => sprintf(
                        'Het uitvoeren een mutatie tijdens faseovergang in zaak <a href="/zaak/%s">%s</a> is mislukt.',
                        $mutation->label,
                        $self->id,
                        $self->id
                    )
                };
            };
        }

        return @messages;
    }

    return { message => "ID-10T Error", type => 'error' };
}

sub start_subcase {
    my ($self, $arguments) = @_;

    my $case_action = $arguments->{case_action} or die "need case_action";
    my $c           = $arguments->{context}     or die "need context";


    # odd, but some of the fields differ in naming (e.g. automatisch_behandelen)
    my $settings    = $case_action->data;
    my $action_data = $case_action->data;

    # these are the exceptions and the checks
    $settings->{ou_id}                      = $action_data->{ou_id} or die "need ou_id";
    $settings->{role_id}                    = $action_data->{role_id} or die "need role_id";
    $settings->{type_zaak}                  = $action_data->{relatie_type} or die "need relatie_type";
    $settings->{aanvrager_type}             = $action_data->{eigenaar_type} or die "need eigenaar_type";
    $settings->{aanvrager_id}               = $action_data->{eigenaar_id} if $action_data->{ eigenaar_type } eq 'anders';
    $settings->{actie_kopieren_kenmerken}   = $action_data->{kopieren_kenmerken};
    $settings->{zaaktype_id}                = $action_data->{relatie_zaaktype_id} or die "need relatie_zaaktype_id";
    $settings->{actie_automatisch_behandelen} = $action_data->{automatisch_behandelen};

    # If you create an automatic subcase in the first phase (registration), and the case
    # is created without setting the behandelaar, there's a doom scenario. There's a few workarounds,
    # this one at least created the subcase. When the case gets a behandelaar, the behandelaar is responsible
    # for taking ownership of that case.
    # A better fix is to make it impossible to create this scenario, however that is a substantial change
    # in Zaaktypebeheer, and hardly feasible without the current workflow.
    if($settings->{aanvrager_type} eq 'behandelaar' && !$self->behandelaar) {
        $c->push_flash_message("Deelzaak kon niet worden aangemaakt met 'behandelaar = aanvrager' omdat behandelaar niet is ingesteld.");
        $settings->{aanvrager_type} = 'aanvrager';
    }

    $settings->{onderwerp} = $self->onderwerp;

    my $subcase = $c->model('DB::Zaak')->create_relatie(
        $self, # fishy ## noshit
        %$settings
    );

    if ($action_data->{ betrokkene_id }) {
        $subcase->betrokkene_relateren({
            betrokkene_identifier  => $action_data->{ betrokkene_id },
            magic_string_prefix    => $action_data->{ betrokkene_prefix },
            rol                    => $action_data->{ betrokkene_role },
            pip_authorized         => $action_data->{ betrokkene_authorized },
            send_auth_confirmation => $action_data->{ betrokkene_notify }
        });
    }

    $subcase->fire_phase_actions({ context => $c });

    if($settings->{required} && $settings->{relatie_type} eq 'deelzaak') {
        $self->register_required_subcase({
            subcase_id              => $subcase->id,
            required                => $settings->{required},
            parent_advance_results  => $settings->{parent_advance_results},
        });
    }

    $subcase->execute_rules({
        status => 2,    # Run rules for second status
        cache  => 0,
    });

    return $self->logging->trigger('case/subcase', { component => 'zaak', data => {
        subcase_id => $subcase->id,
        type => $action_data->{ relatie_type }
    }});
}



=head2 mail_action

Perform a case mail action, either schedule or send directly.

    $case->mail_action({
        send_date => '27-01-2014',
        bibliotheek_notificaties_id => 27
        ...,
        case => $subcase,
        email_sender => Zaaksysteem::Backend::Mailer->new
    });

=cut

define_profile mail_action => (
    required => {
        action_data => 'HashRef',
        case => 'Zaaksysteem::Schema::Zaak', # could be a different case, e.g. subcase
        email_sender => 'Object' # for testability we want to be able to substitute
    }
);

sub mail_action {
    my $self = shift;
    my $params = assert_profile(shift)->valid;

    my $action_data = $params->{action_data};

    if(my $send_date = $action_data->{send_date}) {
        my $send_date_dt = $action_data->{schedule_test} ?
            DateTime->from_epoch(epoch => $send_date) :
            DateTime::Format::ISO8601->parse_datetime($send_date);

        $self->result_source->schema->resultset('ScheduledJobs')->create_zaak_notificatie({
            bibliotheek_notificaties_id => $action_data->{bibliotheek_notificaties_id},
            zaaktype_notificatie_id => $action_data->{zaaktype_notificatie_id},
            scheduled_for   => $send_date_dt,
            recipient_type  => $action_data->{rcpt},
            behandelaar     => $action_data->{behandelaar},
            email           => $action_data->{email},
            zaak_id         => $self->id,
        });

        return "E-mail ingepland";
    }

    my $recipient = $self->notification_recipient({
        recipient_type  => $action_data->{rcpt},
        behandelaar     => $action_data->{behandelaar},
        email           => $action_data->{email},
    });

    return if !$recipient;

    my $mailer = $self->mailer($params->{email_sender});
    my $attachments = $action_data->{case_document_attachments};
    my @zaaktype_kenmerken_ids = map { $_->{case_document_ids} }
        grep { $_->{selected} } @$attachments;

    my $ok = $mailer->send_from_case({
        recipient       => $recipient,
        body            => $action_data->{body},
        subject         => $action_data->{subject},
        sender_address  => $action_data->{sender_address},
        sender          => $action_data->{sender},
        cc              => $action_data->{cc},
        bcc             => $action_data->{bcc},
        attachments     => \@zaaktype_kenmerken_ids
    });

    return $ok ? "E-mail verstuurd" : "E-mail is niet verstuurd";
}


sub template_action {
    my ($self, $options) = @_;

    my $c               = $options->{context}       or die "need context";
    my $case_action     = $options->{case_action}   or die "need case_action";

    my $action_data = $case_action->data;

    my $bibliotheek_sjablonen_id = $action_data->{bibliotheek_sjablonen_id}
        or die "need bibliotheek_sjablonen_id";

    my $sjabloon = $self->result_source->schema->resultset('BibliotheekSjablonen')->find($bibliotheek_sjablonen_id)
        or die "need sjabloon";

    my $case_sjabloon = $self->zaaktype_node_id->zaaktype_sjablonen->search({
        bibliotheek_kenmerken_id => $action_data->{bibliotheek_kenmerken_id}
    })->single;

    my %file_create_opts = (
        name          => $sjabloon->filestore_id->name_without_extension,
        case          => $self,
        subject       => $self->_get_subject($c),
        target_format => $action_data->{target_format} || $case_sjabloon->target_format,
    );

    if($action_data->{ bibliotheek_kenmerken_id }) {
        my $case_type_attribute = $self->result_source->schema->resultset('ZaaktypeKenmerken')->search(
            zaaktype_node_id => $self->get_column('zaaktype_node_id'),
            bibliotheek_kenmerken_id => $action_data->{ bibliotheek_kenmerken_id }
        )->first;

        if ($case_type_attribute) {
            $file_create_opts{ case_document_ids } = $case_type_attribute->id;
        }
    }

    $sjabloon->file_create(\%file_create_opts);
}


sub _get_subject {
    my ($self, $c) = @_;

    ### Only when user exists...when from the outside, this is possible a
    ### case create. And we use the aanvrager key.
    my $can = eval { $c->can('user_exists') };
    if ($can && $c->user_exists) {
        return $c->model('Betrokkene')->get(
            {
                intern  => 0,
                type    => 'medewerker',
            },
            $c->user->uidnumber,
        )->betrokkene_identifier;
    }
    return $self->aanvrager_object->betrokkene_identifier;
}



#
# For every phase a new allocation can be automatically set.
#
sub allocation_action {
    my ($self, $options) = @_;

    my $role_id = $options->{role_id}   or die "need role_id";
    my $ou_id   = $options->{ou_id}     or die "ou_id";

    # Next time? There won't be no next time
    my $volgende_fase = $self->volgende_fase;

    if (!$volgende_fase) {
        throw("ZS/Z/R/FO", "Aint no next phase, no point");
    }

    $self->wijzig_route({
        route_ou    => $ou_id,
        route_role  => $role_id,
        change_only_route_fields => $options->{ change_only_route_fields }
    });
}



=head2 notification_recipient

Determine email recipient based on case context

=cut

define_profile notification_recipient => (
    required => [qw/recipient_type/],
    optional => [qw/email behandelaar/],
    constraint_methods => {
        recipient_type => sub {
            my ($dfv, $value) = @_;
            return $value ~~ [qw/aanvrager behandelaar medewerker_uuid medewerker coordinator gemachtigde overig/];
        }
    }
);
sub notification_recipient {
    my $self = shift;
    my $params = assert_profile(shift)->valid;;

    my $recipient_type  = $params->{recipient_type};
    my $behandelaar     = $params->{behandelaar} || '';

    throw('case/notification_recipient/no_behandelaar_supplied', 'Behandelaar niet ingesteld')
        if $recipient_type eq 'behandelaar' && $behandelaar !~ m/\-\d+$/;

    my $email           = $params->{email};

    my $betrokkene_sub = sub {
        my ($betrokkene_id) = $behandelaar =~ m/\-(\d+)$/;

        my $betrokkene_object = $self->result_source->schema->betrokkene_model->get({
                extern  => 1,
                type => 'medewerker'
            },
            $betrokkene_id
        ) or throw('case/notification_recipient/subject_not_found', "Betrokkene $betrokkene_id niet gevonden");

        return $betrokkene_object->email;
    };

    my $dispatch_table = {
        aanvrager       => sub { $self->aanvrager_object->email },
        behandelaar     => $betrokkene_sub,
        medewerker_uuid => $betrokkene_sub,
        medewerker      => $betrokkene_sub,
        gemachtigde     => sub {
            my @authorized = $self->pip_authorized_betrokkenen;
            return '' if !@authorized;
            return join(";", map { $_->email } @authorized);
        },
        coordinator => sub {
            my $co = $self->coordinator_object;
            return '' if !defined $co;
            return $co->email;
        },
        overig => sub {
            my $ztt = Zaaksysteem::ZTT->new;
            $ztt->add_context($self);

            return $ztt->process_template($email)->string;
        },
    };

    my $recipient = $dispatch_table->{$recipient_type}->();

    if ($recipient) {
        my @emails = grep { length $_ } split /\s*[;,]\s*/, $recipient;

        throw ("backend/email/notification/invalid_email", "Ongeldig e-mail adres: " . $recipient)
            unless all { Email::Valid->address($_) } @emails;

        return join ';', @emails;
    }

    return;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 ZAAKSYSTEEM_OPTIONS

TODO: Fix the POD

=cut

=head2 afhandel_fase

TODO: Fix the POD

=cut

=head2 allocation_action

TODO: Fix the POD

=cut

=head2 can_vorige_fase

TODO: Fix the POD

=cut

=head2 fasen

TODO: Fix the POD

=cut

=head2 fire_phase_actions

TODO: Fix the POD

=cut

=head2 flush_cache

TODO: Fix the POD

=cut

=head2 huidige_fase

TODO: Fix the POD

=cut

=head2 is_afgehandeld

TODO: Fix the POD

=cut

=head2 is_afhandel_fase

TODO: Fix the POD

=cut

=head2 is_open

TODO: Fix the POD

=cut

=head2 is_volgende_afhandel_fase

TODO: Fix the POD

=cut

=head2 registratie_fase

TODO: Fix the POD

=cut

=head2 set_gesloten

TODO: Fix the POD

=cut

=head2 set_heropen

TODO: Fix the POD

=cut

=head2 set_volgende_fase

TODO: Fix the POD

=cut

=head2 set_vorige_fase

TODO: Fix the POD

=cut

=head2 start_subcase

TODO: Fix the POD

=cut

=head2 template_action

TODO: Fix the POD

=cut

=head2 volgende_fase

TODO: Fix the POD

=cut

=head2 vorige_fase

TODO: Fix the POD

=cut

=head2 zaak_cache

TODO: Fix the POD

=cut

