package Zaaksysteem::Zaken::Roles::BagSetup;
use Moose::Role;
use Zaaksysteem::Tools;

around '_create_zaak' => sub {
    my $orig            = shift;
    my $self            = shift;
    my ($opts)          = @_;

    if ($opts->{bag}) {
        my $bag_info = $self->_load_bag_items($opts->{bag});

        if ($bag_info) {
            $opts->{locatie_zaak} = $bag_info->{locatie_zaak}
                if ($bag_info->{locatie_zaak});

            $opts->{locatie_correspondentie}    =
                $bag_info->{locatie_correspondentie}
                    if $bag_info->{locatie_correspondentie};
        }
    }

    for my $update_field (qw/locatie_zaak locatie_correspondentie/) {
        my $value = delete $opts->{$update_field};
        next unless $value;

        $self->log->debug('Adding bag item: ' . $update_field);

        my $bag = $self->_load_bag_item($value);

        $self->log->debug('Changed option: ' . $update_field . 'to:' .
            $bag->id
        );
        $opts->{$update_field} = $bag->id;
    }

    my $zaak = $self->$orig(@_);

    ### After creation of zaak, check if aanvrager is also zaak_locatie
    if ( my $verblijfsobject = $zaak->aanvrager_object->verblijfsobject ) {
        my $verblijfsobject_id = (
            ref($verblijfsobject->identificatie)
                ? $verblijfsobject->identificatie->identificatie
                : $verblijfsobject->identificatie
            );

        my $bag_credentials = {
            bag_id                  => $verblijfsobject_id,
            bag_type                => 'verblijfsobject',
            bag_verblijfsobject_id  => $verblijfsobject_id,
        };

        if ($zaak->zaaktype_node_id->adres_andere_locatie) {
            my $zaak_bag = $zaak->zaak_bags->create_bag($bag_credentials);
            $zaak->update({ locatie_zaak => $zaak_bag->id });
        }
    }

    return $zaak;
};

# bagopts = {
#   bag_type  => 'nummeraanduiding',
#   bag_id    => '3232323292892034',
#   __OPTIONEEL__
#   bag_pand_id => '23423423423423',
#   bag_verblijfsobject_id => '23423423423423',
#   bag_nummeraanduiding_id => '23423423423423',
#   bag_openbareruimte_id => '23423423423423',
# }

sub _load_bag_item {
    my ($self, $bagobject) = @_;

    if (ref $bagobject ne 'HASH') {
        throw("/ZS/Z/R/BagSetup", "Bagobject is not a hash");
    }

    my @columns =
      $self->result_source->schema->resultset('ZaakBag')->result_source->columns;

    my $bagdata = {};
    for (@columns) {
        next unless $bagobject->{$_};
        $bagdata->{$_} = $bagobject->{$_};
    }

    return $self->result_source->schema->resultset('ZaakBag')->create_bag($bagdata);
}

sub _load_bag_items {
    my $self    = shift;
    my $bagopts = shift;
    my ($rv, @bag_objecten);

    if (UNIVERSAL::isa($bagopts, 'ARRAY')) {
        @bag_objecten    = @{ $bagopts };
    } else {
        push(@bag_objecten, $bagopts);
    }

    for my $bagobject (@bag_objecten) {
        my $rv  = $self->_load_bag_item($bagobject);

        if ($bagobject->{locatie_zaak}) {
            $rv->{locatie_zaak}                 = $rv->id;
        }
        if ($bagobject->{locatie_correspondentie}) {
            $rv->{locatie_correspondentie}      = $rv->id;
        }
    }

    return $rv;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
