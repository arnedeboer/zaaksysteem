package Zaaksysteem::SQL;
use Moose;

use autodie;
use namespace::autoclean;

use File::Temp qw/tempdir/;
use File::Spec::Functions qw(catfile);

=head1 NAME

Zaaksysteem::SQL - Generate SQL code from the current Zaaksysteem instance

=head1 ATTRIBUTES

=head2 schema

The Zaaksysteem schema object

=cut

has schema => (
    is       => 'ro',
    required => 1,
    isa      => 'Zaaksysteem::Schema',
);


=head2 version

The Zaaksysteem version

=cut

has version => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);


=head2 tmp_dir

The basedir (Str) of where the SQL file is dumped

=cut

has tmp_dir => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);


=head2 sql

The Zaaksysteem SQL code itself.

=cut

has sql => (
    is      => 'ro',
    isa     => 'HashRef',
    builder => '_generate_ddl',
    lazy    => 1,
);


=head2 databases

The supported database schema's. Defaults to PostgreSQL, possible values can be found at L<DBIx::Class::Storage::DBI/create_ddl_dir>

=cut

has databases => (
    is      => 'ro',
    isa     => 'ArrayRef',
    default => sub { return [qw(PostgreSQL)] },
);


=head1 METHODS

=head2 _generate_ddl

Generate the DDL

=head3 RETURNS

An HashRef containing the filename and the contents of the file.

    {
        Zaaksysteem-Schema-3.18.1-PostgreSQL.sql => '-- Created by SQL::Translator::Producer::PostgreSQL\n-- Created on Mon Jul 28 15:52:47 2014\n-- \n-- etcetera'
    }

=cut

sub _generate_ddl {
    my $self = shift;

    my $tmp_dir = tempdir(
        CLEANUP => 1,
        DIR     => $self->tmp_dir,
    );

    $self->schema->create_ddl_dir($self->databases, $self->version, $tmp_dir, undef, { add_drop_table => 0 });

    my $dh;
    opendir $dh, $tmp_dir;
    my @result = grep { $_ !~ /^\.+$/ } readdir($dh);
    closedir($dh);

    my %files;
    foreach my $f (@result) {
        my $file = catfile($tmp_dir, $f);
        open my $fh, '<', $file;
        {   # Some may want to use some kind of file slurper
            # but this does the trick in a couple of lines.
            local $/;
            $files{$f} = <$fh>;
            $files{$f} .= qq{
--
-- Defaults for using Zaaksysteem
--
INSERT INTO config VALUES    (1, 'jodconverter_url', 'http://localhost:8080/converter/service', true);
INSERT INTO config VALUES    (2, 'allocation_notification_template_id', '', false);
INSERT INTO config VALUES    (3, 'file_username_seperator', '-', true);
INSERT INTO config VALUES    (4, 'feedback_email_template_id', NULL, true);
INSERT INTO config VALUES    (5, 'document_intake_user', 'intake', true);
INSERT INTO config VALUES    (6, 'users_can_change_password', 'off', true);
INSERT INTO config VALUES    (7, 'filestore_location', '/path/to/storage', true);
INSERT INTO config VALUES    (8, 'tmp_location', '/path/to/temp/storage', true);
INSERT INTO interface VALUES (1, 'LDAP Authenticatie', true, NULL, 10, '{}', true, 'authldap', NULL);
};

            $files{$f} = qq{
--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: hstore; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS hstore WITH SCHEMA public;


--
-- Name: EXTENSION hstore; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION hstore IS 'data type for storing sets of (key, value) pairs';


--
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


SET search_path = public, pg_catalog;

--
-- Name: confidentiality; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE confidentiality AS ENUM (
    'public',
    'internal',
    'confidential'
);


--
-- Name: contactmoment_medium; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE contactmoment_medium AS ENUM (
    'behandelaar',
    'balie',
    'telefoon',
    'post',
    'email',
    'webformulier'
);


--
-- Name: contactmoment_type; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE contactmoment_type AS ENUM (
    'email',
    'note'
);


--
-- Name: zaaksysteem_bag_types; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE zaaksysteem_bag_types AS ENUM (
    'nummeraanduiding',
    'verblijfsobject',
    'pand',
    'openbareruimte'
);


--
-- Name: zaaksysteem_status; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE zaaksysteem_status AS ENUM (
    'new',
    'open',
    'resolved',
    'stalled',
    'deleted',
    'overdragen'
);


--
-- Name: zaaksysteem_trigger; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE zaaksysteem_trigger AS ENUM (
    'extern',
    'intern'
);


SET default_tablespace = '';

SET default_with_oids = false;

} .  $files{$f};


        }
        close $fh;
        unlink $file;
    }

    rmdir($tmp_dir);
    return \%files;
}


__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
