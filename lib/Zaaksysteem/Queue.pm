package Zaaksysteem::Queue;
use Moose;

use Zaaksysteem::Tools;

with 'MooseX::Log::Log4perl';

=head1 NAME

Zaaksysteem::Queue - A wrapper module around the queue item mechanism

=head1 SYNOPSIS

    use Zaaksysteem::Queue;
    my $queue = Zaaksysteem::Queue->new(
        schema => $schema;
    );

    $queue->create_item();

=head1 ATTRIBUTES

=head2 schema

An L<Zaaksysteem::Schema> object, required

=cut

has schema => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Schema',
    required => 1,
);

has lock_id => (
    is        => 'rw',
    isa       => 'Int',
    predicate => 'has_lock',
    clearer   => 'clear_lock',
);

sub lock {
    my $self = shift;
    if ($self->has_lock) {
        $self->log->trace(sprintf("Setting lock %d", $self->lock_id));
        $self->schema->lock($self->lock_id);
    }
    return 1;
}

sub unlock {
    my $self = shift;
    if ($self->has_lock) {
        $self->log->trace(sprintf("Unlocking lock %d", $self->lock_id));
        $self->schema->unlock($self->lock_id);
    }
    return 1;
}

=head1 METHODS

=head2 create_item

Creates a queue item for you and it checks if there are already similar items in the queue with the same label, data and type.

=cut

define_profile create_item => (
    required => {
        label => 'Str',
        type  => 'Str',
        data  => 'HashRef',
    },
);

sub create_item {
    my $self = shift;
#    my $options = assert_profile({@_})->valid;
    my $options = {@_};

    my $label   = $options->{label};
    my $type    = $options->{type};
    my $data    = $options->{data};

    $self->log->trace(
        sprintf(
            "Creating queue item with label: '%s', type: '%s' data: %s)",
            $label, $type, dump_terse($data)
        )
    );

    # This uses the same ordering of overriding hash keys as the create_item
    # call below ends up with.
    my $queue_data = JSON::XS->new->canonical(1)->utf8(1)->encode($data);

    $self->lock();

    my $existing = $self->schema->resultset('Queue')->search(
        {
            status => ['pending', 'waiting'],
            type   => $type,
            label  => $label,
            data   => $queue_data,
        }
    )->first;

    if ($existing) {
        $self->unlock();
        $self->log->trace("Found existing queue-item for this task.");
        return undef;
    }

    my $item = $self->schema->resultset('Queue')->create_item(
        $type,
        {
            label => $label,
            data  => $data,
        }
    );

    $self->unlock();
    return $item;
}

sub add_to_queue {
    my $self = shift;
    my $item = shift;
    push(@{$self->schema->default_resultset_attributes->{queue_items}}, $item);
}



__PACKAGE__->meta->make_immutable;

__END__


=head1 AUTHOR

Wesley Schwengle

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
