package Zaaksysteem::Types;

use Zaaksysteem::Constants qw(VALID_FQDN);

use MooseX::Types -declare => [qw(
    ArrayRefOfHashRefs
    Boolean
    IntervalStr
    JSONBoolean
    JSON_XS_Boolean
    NonEmptyStr
    PackageElementStr
    RelatedCaseType
    RelatedCaseTypes
    Timestamp
    UUID
    ZSNetworkACL
    ZSNetworkACLs
    ZSvhosts
    zs_ipv4
    zs_ipv6
    FQDN
)];

use MooseX::Types::Moose qw(Str Int Bool ArrayRef HashRef Item);

use DateTime::Format::ISO8601 qw[];

=head1 NAME

Zaaksysteem::Types - Custom types for Zaaksysteem

=head1 SYNOPSIS

    package MyClass;
    use Moose;
    use Zaaksysteem::Types qw(TYPE1 TYPE2);

    has attr => (
        isa => TYPE1,
        is => 'ro',
    );

=head1 AVAILABLE TYPES

=head2 ZSNetworkACL

A ZSNetworkACL type

=cut

subtype ZSNetworkACL, as Str,
    where {
        my $str = shift;
        # IPv4/IPv6 regexp are coming from https://github.com/waterkip/regexp-ip which is not yet on CPAN. I should probably do that one day.
        my $ipv4_re = '(?^:([0-9]|[1-9][0-9]|1[0-9][0-9]|2([0-4][0-9]|5[0-5]))\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2([0-4][0-9]|5[0-5]))\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2([0-4][0-9]|5[0-5]))\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2([0-4][0-9]|5[0-5])))';
        my $ipv6_re = '(?^:(?::(?::[0-9a-fA-F]{1,4}){0,5}(?:(?::[0-9a-fA-F]{1,4}){1,2}|:(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5])))|[0-9a-fA-F]{1,4}:(?:[0-9a-fA-F]{1,4}:(?:[0-9a-fA-F]{1,4}:(?:[0-9a-fA-F]{1,4}:(?:[0-9a-fA-F]{1,4}:(?:[0-9a-fA-F]{1,4}:(?:[0-9a-fA-F]{1,4}:(?:[0-9a-fA-F]{1,4}|:)|(?::(?:[0-9a-fA-F]{1,4})?|(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))))|:(?:(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))|[0-9a-fA-F]{1,4}(?::[0-9a-fA-F]{1,4})?|))|(?::(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))|:[0-9a-fA-F]{1,4}(?::(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))|(?::[0-9a-fA-F]{1,4}){0,2})|:))|(?:(?::[0-9a-fA-F]{1,4}){0,2}(?::(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))|(?::[0-9a-fA-F]{1,4}){1,2})|:))|(?:(?::[0-9a-fA-F]{1,4}){0,3}(?::(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))|(?::[0-9a-fA-F]{1,4}){1,2})|:))|(?:(?::[0-9a-fA-F]{1,4}){0,4}(?::(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))|(?::[0-9a-fA-F]{1,4}){1,2})|:))|::))';
        my $re = qr/^(?:$ipv4_re|$ipv6_re)(?:|\s+(?:.+))?$/;
        return $str =~ /$re/;
    };

subtype ZSNetworkACLs, as ArrayRef[ZSNetworkACL];

subtype zs_ipv4, as Str,
    where {
        my ($ip, $range) = split(/\//, shift);
        return 0 if !$ip;
        my $ipv4_re = '(?^:([0-9]|[1-9][0-9]|1[0-9][0-9]|2([0-4][0-9]|5[0-5]))\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2([0-4][0-9]|5[0-5]))\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2([0-4][0-9]|5[0-5]))\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2([0-4][0-9]|5[0-5])))';

        $ipv4_re = qr/^$ipv4_re$/;

        if ($ip =~ /$ipv4_re/ && ( !defined $range or $range =~ /^\d+/ && $range >= 1 && $range <= 32)) {
            return 1;
        }
        return 0;
    },
    message { "$_ is not a valid IPv4 CIDR address" };


subtype zs_ipv6, as Str,
    where {
        my ($ip, $range) = split(/\//, shift);
        return 0 if !$ip;
        my $ipv6_re = '(?^:(?::(?::[0-9a-fA-F]{1,4}){0,5}(?:(?::[0-9a-fA-F]{1,4}){1,2}|:(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5])))|[0-9a-fA-F]{1,4}:(?:[0-9a-fA-F]{1,4}:(?:[0-9a-fA-F]{1,4}:(?:[0-9a-fA-F]{1,4}:(?:[0-9a-fA-F]{1,4}:(?:[0-9a-fA-F]{1,4}:(?:[0-9a-fA-F]{1,4}:(?:[0-9a-fA-F]{1,4}|:)|(?::(?:[0-9a-fA-F]{1,4})?|(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))))|:(?:(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))|[0-9a-fA-F]{1,4}(?::[0-9a-fA-F]{1,4})?|))|(?::(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))|:[0-9a-fA-F]{1,4}(?::(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))|(?::[0-9a-fA-F]{1,4}){0,2})|:))|(?:(?::[0-9a-fA-F]{1,4}){0,2}(?::(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))|(?::[0-9a-fA-F]{1,4}){1,2})|:))|(?:(?::[0-9a-fA-F]{1,4}){0,3}(?::(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))|(?::[0-9a-fA-F]{1,4}){1,2})|:))|(?:(?::[0-9a-fA-F]{1,4}){0,4}(?::(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))|(?::[0-9a-fA-F]{1,4}){1,2})|:))|::))';

        $ipv6_re = qr/$ipv6_re/;
        if ($ip =~ /$ipv6_re/ && ( !defined $range or $range =~ /^\d+/ && $range >= 1 && $range <= 64)) {
            return 1;
        }
        return 0;
    },
    message { "$_ is not a valid IPv6 CIDR address" };

=head2 FQDN

A type (subtype of Str) that only allows syntactically valid fully qualified domain names.

=cut

subtype FQDN,
    as Str,
    where {
        return VALID_FQDN->($_);
    };

subtype ZSvhosts, as ArrayRef[FQDN];

subtype zs_ipv4, as Str,
    where {
        my ($ip, $range) = split(/\//, shift);
        return 0 if !$ip;
        my $ipv4_re = '(?^:([0-9]|[1-9][0-9]|1[0-9][0-9]|2([0-4][0-9]|5[0-5]))\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2([0-4][0-9]|5[0-5]))\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2([0-4][0-9]|5[0-5]))\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2([0-4][0-9]|5[0-5])))';

        $ipv4_re = qr/^$ipv4_re$/;

        if ($ip =~ /$ipv4_re/ && ( !defined $range or $range =~ /^\d+/ && $range >= 1 && $range <= 32)) {
            return 1;
        }
        return 0;
    },
    message { "$_ is not a valid IPv4 CIDR address" };


subtype zs_ipv6, as Str,
    where {
        my ($ip, $range) = split(/\//, shift);
        return 0 if !$ip;
        my $ipv6_re = '(?^:(?::(?::[0-9a-fA-F]{1,4}){0,5}(?:(?::[0-9a-fA-F]{1,4}){1,2}|:(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5])))|[0-9a-fA-F]{1,4}:(?:[0-9a-fA-F]{1,4}:(?:[0-9a-fA-F]{1,4}:(?:[0-9a-fA-F]{1,4}:(?:[0-9a-fA-F]{1,4}:(?:[0-9a-fA-F]{1,4}:(?:[0-9a-fA-F]{1,4}:(?:[0-9a-fA-F]{1,4}|:)|(?::(?:[0-9a-fA-F]{1,4})?|(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))))|:(?:(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))|[0-9a-fA-F]{1,4}(?::[0-9a-fA-F]{1,4})?|))|(?::(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))|:[0-9a-fA-F]{1,4}(?::(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))|(?::[0-9a-fA-F]{1,4}){0,2})|:))|(?:(?::[0-9a-fA-F]{1,4}){0,2}(?::(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))|(?::[0-9a-fA-F]{1,4}){1,2})|:))|(?:(?::[0-9a-fA-F]{1,4}){0,3}(?::(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))|(?::[0-9a-fA-F]{1,4}){1,2})|:))|(?:(?::[0-9a-fA-F]{1,4}){0,4}(?::(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))|(?::[0-9a-fA-F]{1,4}){1,2})|:))|::))';

        $ipv6_re = qr/$ipv6_re/;
        if ($ip =~ /$ipv6_re/ && ( !defined $range or $range =~ /^\d+/ && $range >= 1 && $range <= 64)) {
            return 1;
        }
        return 0;
    },
    message { "$_ is not a valid IPv6 CIDR address" };

=head2 UUID

A type (subtype of Str) that only allows syntactically valid UUIDs.

=cut

subtype UUID,
    as Str,
    where {
        my $str = shift;
        return $str =~ /^[0-9a-z]{8}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{12}$/i;
    };

=head2 NonEmptyStr

A type (subtype of Str) that defines a label

=cut

subtype NonEmptyStr,
    as Str,
    where {
        my $str = shift;
        return $str =~ /^\p{XPosixSpace}*(?:\p{XPosixAlnum}|\p{XPosixPunct})+/;
    };

=head2 Boolean

Unfortunatly, the C<Bool> type of Moose will stringify your input, and modules like JSON will see it as a string.

B<example>

    has disabled => (
        is     => 'rw',
        isa    => 'Bool',
        default => 1,
    );

    ### $object->disabled becomes "disabled: '1'"

Except, when using Boolean, it will work as you would expect. It has the EXACT same functionality
as Bool, but will also return the right value to JSON.

    has disabled => (
        is     => 'rw',
        isa    => 'Boolean',
        default => 1,
    );

    ### $object->disabled becomes "disabled: 1"

=cut

## !ref($_) added to correctly find out JSON::XS::Boolean, because it normally stringifies to 0 or 1. But
## when send to JSON, it will inflate to "true" or "false". This could be a good implementation, but we
## chose to send back 1 of 0.

subtype Boolean, as Item,
    where { !ref($_) && (!defined($_) || $_ eq '1' || $_ eq '0' || "$_" eq '1'|| "$_" eq '0' || $_ eq "") };

class_type JSON_XS_Boolean, { class => 'JSON::XS::Boolean' };
coerce Boolean, from JSON_XS_Boolean, via { if (JSON::XS::is_bool($_)) { return ($_ == 1 ? 1 : 0) } };

=head2 JSONBoolean

As a bonus, we extended the C<Boolean> function. This one will send back proper true and false values. It will
coerce everything into JSON::XS::true or JSON::XS::false.

B<example>

    has disabled => (
        is     => 'rw',
        isa    => 'JSONBoolean',
        default => 1,
        coerce => 1,
    );

    ### $object->disabled becomes "disabled: true"

    has disabled => (
        is     => 'rw',
        isa    => 'JSONBoolean',
        default => { JSON::XS::true },
        coerce => 1,
    );

    ### $object->disabled becomes "disabled: true"

    has disabled => (
        is     => 'rw',
        isa    => 'JSONBoolean',
        default => 0,
        coerce => 1,
    );

    ### $object->disabled becomes "disabled: false"

    has disabled => (
        is     => 'rw',
        isa    => 'JSONBoolean',
        default => { JSON::XS::false },
        coerce => 1,
    );

    ### $object->disabled becomes "disabled: false"


=cut

subtype JSONBoolean, as Item,
    where { JSON::XS::is_bool($_); };

coerce JSONBoolean, from Bool, via { ($_ ? JSON::XS::true : JSON::XS::false) };

=head2 Timestamp

The C<Timestamp> type wraps L<DateTime> objects, and adds C<Str> => C<DateTime>
coercion (via L<DateTime::Format::ISO8601/parse_datetime>.

=cut

class_type Timestamp, { class => 'DateTime' };
coerce Timestamp, from Str, via { DateTime::Format::ISO8601->parse_datetime($_) };

=head2 IntervalStr

A string representing an interval "type".

Allowed values are:

=over

=item * years

=item * months

=item * weeks

=item * days

=item * once

=back

=cut

subtype IntervalStr, as Str,
    where { $_ =~ /^(?:years|months|weeks|days|once)$/ };

=head2 PackageElementStr

A valid element of a Perl package name.

Currently, we only allow ASCII (even though Perl allows Unicode in its package
names).

=cut

subtype PackageElementStr, as Str,
    where { $_ =~ /^[a-zA-Z0-9_]+$/ };

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

