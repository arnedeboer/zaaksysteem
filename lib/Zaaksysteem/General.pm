package Zaaksysteem::General;
use strict;
use warnings;

use Zaaksysteem::Constants;
use Zaaksysteem::Tools;
use Scalar::Util qw/blessed/;
use Number::Format;
use Zaaksysteem::BrowserDetect;

use base qw/
    Zaaksysteem::General::Authentication
    Zaaksysteem::General::Upgrade
    Zaaksysteem::General::Actions
/;



sub add_trail {
    my ($c, $opts) = @_;

    if (!$c->stash->{trail}) {
        $c->stash->{trail} = [];
        $c->stash->{trail}->[0] = {
            'uri'   => $c->uri_for('/'),
            'label' => 'Dashboard',
        };
    }

    push(@{ $c->stash->{trail} }, $opts);
}


sub zvalidate {
    my ($c, $dv, $opts) = @_;

    unless (defined($dv)) {
        $dv = $c->check(params => $c->req->params, method => [caller(1)]->[3]);
    }

    die('Definition not found for: ' . [caller(1)]->[3]) unless ref($dv);

    $c->stash->{last_validation} = $dv;

    if ($c->req->is_xhr &&
        (!defined($opts->{bypass_json}) || !$opts->{bypass_json})
    ) {
        # Do some JSON things
        my $json = {
            success     => $dv->success,
            missing     => [ $dv->missing ],
            invalid     => [ $dv->invalid ],
            unknown     => [ $dv->unknown ],
            valid       => [ $dv->valid ],
            msgs        => $dv->msgs,
        };

        $c->zcvalidate($json);
    }

    if ($dv->success) { return $dv; }

    ### Go log something
    my $errmsg = "Problems validating profile:";
    $errmsg .= "\n        Missing params:\n        * " .
                join("\n        * ", $dv->missing)
                if $dv->has_missing;
    $errmsg .= "\n        Invalid params:\n        * " .
                join("\n        * ", $dv->invalid)
                if $dv->has_invalid;
    $errmsg .= "\n        Unknown params:\n        * " .
                join("\n        * ", $dv->unknown)
                if $dv->has_unknown;
    $c->log->debug($errmsg);

    return;
}


sub zcvalidate {
    my ($c, $opts) = @_;
    my ($json);

    return unless $c->req->is_xhr;

    unless (
        $opts->{success} ||
        ($opts->{invalid} && UNIVERSAL::isa($opts->{invalid}, 'ARRAY'))
    ) {
        return;
    }
    $json->{invalid}    = $opts->{invalid};

    $json->{success}    = $opts->{success} || undef;
    $json->{missing}    = $opts->{missing} || [];
    $json->{unknown}    = $opts->{invalid} || [];
    $json->{valid}      = $opts->{valid}   || [];
    $json->{msgs}       = $opts->{msgs}    || [];


    $c->stash->{json} = $json;
    $c->forward('Zaaksysteem::View::JSONlegacy');
}

sub is_externe_aanvraag {
    my $c   = shift;

    return if $c->user_exists;

    return 1;
}

sub about {
    my $c   = shift;

    return {
        applicatie      => ZAAKSYSTEEM_NAAM,
        omschrijving    => ZAAKSYSTEEM_OMSCHRIJVING,
        leverancier     => ZAAKSYSTEEM_LEVERANCIER,
        versie          => $c->config->{ZS_VERSION},
        startdatum      => ZAAKSYSTEEM_STARTDATUM,
        licentie        => ZAAKSYSTEEM_LICENSE
    };
}

sub betrokkene_session {
    my $c           = shift;

    return unless $c->session->{betrokkene_session};

    return $c->stash->{_betrokkene_session} if ($c->stash->{_betrokkene_session});

    return ($c->stash->{_betrokkene_session} = $c->model('Betrokkene')->get(
            {},
            $c->session->{betrokkene_session}->{betrokkene_identifier}
        )
    );
}

sub betrokkene_session_enable {
    my $c           = shift;
    my $betrokkene  = shift;

    return unless ref($c);

    $c->log->info(
        'Starting betrokkene session for: '
        . $betrokkene->naam
    );

    $c->session->{betrokkene_session} = {
        betrokkene_identifier   => $betrokkene->betrokkene_identifier,
        betrokkene_naam         => $betrokkene->naam,
    };

    # Remove the cached "betrokkene_session", and re-cache.
    delete $c->stash->{_betrokkene_session};
    $c->betrokkene_session();
}

sub betrokkene_session_disable {
    my $c           = shift;
    my $betrokkene  = shift;

    return unless ref($c);

    return unless $c->session->{ betrokkene_session };

    $c->log->info(
        'Ending betrokkene session for: '
        . $c->betrokkene_session->naam
    );

    delete($c->session->{betrokkene_session});

    return 1;
}

sub is_allowed_browser {
    my ($c) = @_;
    return Zaaksysteem::BrowserDetect->new(useragent => $c->req->user_agent)->check;
}

=head2 labs_enabled

Shortcut method to determine whether a specific "MintLab Labs" feature is enabled.

Returns a true value if the Labs feature is enabled for this user, false otherwise.

=cut

sub labs_enabled {
    my $c = shift;
    my ($lab_key) = @_;

    if (   $c->user_exists
        && exists $c->user->settings->{ labs }
        && $c->user->settings->{ labs }{ $lab_key }
    ) {
        return 1;
    }

    return 0;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 DENIED_BROWSERS

TODO: Fix the POD

=cut

=head2 ZAAKSYSTEEM_LEVERANCIER

TODO: Fix the POD

=cut

=head2 ZAAKSYSTEEM_LICENSE

TODO: Fix the POD

=cut

=head2 ZAAKSYSTEEM_NAAM

TODO: Fix the POD

=cut

=head2 ZAAKSYSTEEM_OMSCHRIJVING

TODO: Fix the POD

=cut

=head2 ZAAKSYSTEEM_STARTDATUM

TODO: Fix the POD

=cut

=head2 about

TODO: Fix the POD

=cut

=head2 add_trail

TODO: Fix the POD

=cut

=head2 betrokkene_session

TODO: Fix the POD

=cut

=head2 betrokkene_session_disable

TODO: Fix the POD

=cut

=head2 betrokkene_session_enable

TODO: Fix the POD

=cut

=head2 is_allowed_browser

TODO: Fix the POD

=cut

=head2 is_externe_aanvraag

TODO: Fix the POD

=cut

=head2 zcvalidate

TODO: Fix the POD

=cut

=head2 zvalidate

TODO: Fix the POD

=cut

