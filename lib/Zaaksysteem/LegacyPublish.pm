package Zaaksysteem::LegacyPublish;
use Moose;

=head1 NAME

Zaaksysteem::LegacyPublish - Publish cases to an external system

=head1 DESCRIPTION

Publish cases via an external system via scp and/or ftp

=head1 SYNOPSIS

    use Zaaksysteem::LegacyPublish;
    my $publisher = Zaaksysteem::LegacyPublish->new(
        hostname => $hostname,
        port     => 22,
        protocol => 'scp',
        username => 'foo',
        password => 'bar',
        src_dir  => '/path/to/files',

        # SCP required
        dst_dir  => '/path/on/dst',

        # FTP required
        csv_filename => 'foo.csv',

        notify_url      => 'https://localhost',
        notify_wait     => 2,
        notify_filename => 'foo.txt',
    );

    $publisher->run();

=cut

use File::Spec::Functions;
use HTTP::Request;
use LWP::UserAgent;
use Net::FTP;
use Net::SCP::Expect;
use POSIX qw(strftime);
use Zaaksysteem::Tools;

has hostname => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

has port => (
    is       => 'ro',
    isa      => 'Int',
    default  => 22,
);

has protocol => (
    is       => 'ro',
    isa      => 'Str',
    default  => 'scp',
);

has username => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

has password => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

has src_dir => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

has dst_dir => (
    is       => 'ro',
    isa      => 'Str',
);

has csv_filename => (
    is       => 'ro',
    isa      => 'Str',
);

has notify_url => (
    is       => 'ro',
    isa      => 'Str',
);

has notify_wait => (
    is       => 'ro',
    isa      => 'Int',
    default  => 2,
);

has notify_filename => (
    is       => 'ro',
    isa      => 'Str',
);

=head2 run

Run the publisher.

=cut

sub run {
    my $self = shift;

    my $method = '_' . $self->protocol;
    if ($self->can($method)) {
        return $self->$method();
    }
    throw("LegacyPublish/unsupported", "Unsupported protocol " . $self->protocol);
}

=head2 _ftp

Copy files around by virtue of insecure FTP

=cut

sub _ftp {
    my $self = shift;

    my $ftp = Net::FTP->new(
        Host  => $self->hostname,
        Port  => $self->port,
        Debug => 0
    ) or throw('ZS/LegacyPubish/ftp/connect', "Error setting up FTP connection: $@");

    $ftp->login(
        $self->username,
        $self->password
    ) or throw('ZS/LegacyPubish/ftp/login', "Error logging into FTP server: " . $ftp->message);

    my $filename = catfile($self->src_dir, $self->csv_filename);
    $self->_ftp_put_file($ftp, $filename);

    if($self->notify_filename) {
        my $filename = catfile($self->src_dir, $self->notify_filename);

        my $content = _get_header_file_content();

        {   use autodie;
            open my $fh, ">", $filename;
            print $fh $content;
            close $fh;
        }

        $self->_ftp_put_file($ftp, $filename);
        unlink($filename);
    }
    $ftp->quit;

    $self->_send_notify();
    return 1;
}

sub _ftp_put_file {
    my ($self, $ftp, $filename) = @_;
    $ftp->put($filename) or throw(
        "ZS/LegacyPubish/ftp/put",
        sprintf(
            "Unable to put file '%s' on the server: %s",
            $filename, $ftp->message
        )
    );
    return 1;
}

sub _get_header_file_content {

    # or for GMT formatted appropriately for your locale:
    my $time = strftime "%H:%M:%S", gmtime;
    my $date = strftime "%d-%m-%Y", gmtime;

    return qq{


----- Dit bestand is gegenereerd met de Zaaksysteem.nl module -----
Datum       : $date
Tijd        : $time
Gebruiker   : Mintlab
Module      : Gevonden voorwerpen
};
}


=head2 _scp

Copy files around by virtue of Secure File Copy

=cut

sub _scp {
    my $self = shift;

    # Make sure SCP globs all the files *in* the source directory.
    my $src_dir = $self->src_dir . '/.';

    # Prep for log4perl
    #    $log->debug(
    #        sprintf(
    #            "Starting SCP transfer to: %s:%d with username %s",
    #            $destination, $port, $username
    #        )
    #    );

    my $scpe = Net::SCP::Expect->new(
        auto_yes  => 1,
        recursive => 1,
        compress  => 1,
        port      => $self->port,
        host      => $self->hostname,
        user      => $self->username,
        password  => $self->password,
    );

    eval { $scpe->scp($src_dir, $self->dst_dir); };

    if ($@) {
        throw('ZS/LegacyPubish/scp', "Error during SCP transfer: $@");
    }

    $self->_send_notify();
    return 1;
}

=head2 _send_notify

Send a notification to a server

=cut

sub _send_notify {
    my $self = shift;
    if ($self->notify_url) {
        # sleep to give receiving system time to recover from previous onslaught.
        sleep($self->notify_wait);

        #$log->debug(
        #    sprintf(
        #        "Sending notification to notify url '%s'", $opts->{notify_url}
        #    )
        #);

        my $ua       = LWP::UserAgent->new;
        my $request  = HTTP::Request->new(GET => $self->notify_url);
        my $response = $ua->request($request);
        my $code     = $response->code;

        if ($code == 503) {
            throw('ZS/LegacyPubish/server/busy',
                "503: Server too busy. Try again in a few minutes.");
        }
        elsif ($code != 200) {
            throw('ZS/LegacyPubish/server/failure',
                "$code: Server failure, please contact your administrator");
        }
    }
    return 1;
}

__PACKAGE__->meta->make_immutable;

__END__


=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
