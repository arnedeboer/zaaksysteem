package Zaaksysteem::ZTT;

use Moose;

use DateTime::Format::Strptime qw(strptime);
use Locales;
use Number::Format;
use Zaaksysteem::Profile;
use Zaaksysteem::Exception;
use Zaaksysteem::Constants;

use Zaaksysteem::ZTT::Element;
use Zaaksysteem::ZTT::Template;

=head1 NAME

Zaaksysteem::ZTT - Zaaksysteem Template Toolkit

=head1 DESCRIPTION

This package abstracts the processing of OpenOffice or plaintext templates.

=head1 ATTRIBUTES

=head2 string_fetchers

This attribute contains all the stringfetchers the ZTT instance can use.

String fetchers are coderefs that reference functions that take as their first
argument a ZTT::Tag object, and return a ZTT::Element object containing the
value to be injected in the document.

=head3 Delegations

Via the L<Array|Moose::Meta::Attribute::Native::Trait::Array> trait.

=over 4

=item add_string_fetcher => push

=item all_string_fetchers => elements

=back

=cut

has string_fetchers => (
    is => 'rw',
    isa => 'ArrayRef[CodeRef]',
    traits => [qw[Array]],
    default => sub { [] },
    handles => {
        add_string_fetcher => 'push',
        all_string_fetchers => 'elements'
    }
);

=head2 iterators

This attribute may contain iterable contexts that the template may request.

Conceptually, objects associated with the main context for this template can
be fed to ZTT, and L</process_template> will use them when a iteration context
is declared in the template.

For example, in a plaintext template:

    Ohai [[ name ]],

    Your orders: [[ itereer:case_relations:order_title ]].

    Cya

When the above template is processed, ZTT expects there to be an iterator
context named C<case_relations>, which in turn has fetcher(s) for the
C<order_title> attribute.

=head3 Delegations

Via the L<Hash|Moose::Meta::Attribute::Native::Trait::Hash> trait.

=over 4

=item set_iterator => set

    $ztt->set_iterator(case_relations => sub { # return contexts arrayref });

=back

=cut

has iterators => (
    is => 'ro',
    isa => 'HashRef[CodeRef]',
    traits => [qw[Hash]],
    default => sub { {} },
    handles => {
        set_iterator => 'set'
    }
);

=head2 locale

Locale to use for number/date/etc. formatting.

=cut

has locale => (
    is      => 'ro',
    isa     => 'Str',
    default => 'nl',
);

=head1 METHODS

=head2 add_context

This method adds a template processing context to the ZTT instance. A context
can be one of three things, an object, an arrayref, or a hashref.

The object variant is most likely what you'd want to use when adding a new
processing context. It is expected to have a method C<get_string_fetchers>
which should return a list of string fetchers. These will be added to the
L</string_fetchers> attribute. The object may also define a
C<get_context_iterators> method, which is expected to return a hashref of
coderefs which when executed return arrays of contexts to be used in
subtemplates.

The idiomatic implementation of the object-variant can be found in
L<Zaaksysteem::Zaken::Roles::ZTT/get_string_fetchers>.

The second form, where the provided context is a hashref, is simpler. The
hashref will be used as a source of string fetcher data. If the template
engine encounters a tag, and hits this context, the tag name will be used as
a key for which the hashref may have a value. That value is then plainly
returned.

    $ztt->add_context({ name => 'Derpy', age => 15 });
    my $doc = $ztt->process_template('[[ age ]] year old [[ name ]] is a few fries short of a happy meal.');

    warn $doc;     # will print '15 year old Derpy is a few fries short of a happy meal.'

The last form, where the provided context is an arrayref, is somewhat
different. A string fetcher will be added to the ZTT instance that simply
shifts the first value off the arrayref every time it is hit.

    $ztt->add_context([qw[a b c d e f g]]);
    my $doc = $ztt->process_template('[[ name ]] [[ tag ]] [ derp ]]');

    warn $doc;      # will print 'a b c'

=cut

sub add_context {
    my $self = shift;
    my $context = shift;

    if (blessed $context) {
        unless($context->can('get_string_fetchers')) {
            throw('ztt/add_context', sprintf(
                "Provided a %s instance, but package does not provide string fetchers",
                blessed($context)
            ));
        }

        $self->add_string_fetcher($context->get_string_fetchers);

        if ($context->can('get_context_iterators')) {
            my $iterators = $context->get_context_iterators;
            if (keys %$iterators) {
                $self->set_iterator(%{ $context->get_context_iterators });
            }
        }

        return $self;
    }

    if (ref $context eq 'HASH') {
        $self->add_string_fetcher(sub {
            my $tag = shift;
            my $value = $context->{ $tag->name };

            return unless defined $value;
            return Zaaksysteem::ZTT::Element->new(value => $value);
        });

        return $self;
    }

    if (ref $context eq 'ARRAY') {
        $self->add_string_fetcher(sub {
            my $value = shift @{ $context };

            return unless $value;
            return Zaaksysteem::ZTT::Element->new(value => $value);
        });

        return $self;
    }

    throw('ztt/add_context', 'Unable to interpret context, not a blessed object, not a HASHREF or ARRAY, giving up man.');
}

=head2 apply_formatter

In templates you can use the construct [[registratiedatum | date]]
which should yield something like 'maandag 14 april 2013' instead
of 14-04-2013. When parsing the parsed tag gets its formatter property
set. This function scans for such a property and applies the filter.

If no formatter is set, this will leave the value of the element untouched.

If the value of the element can not be parsed properly, the original value
will be retained.

I would prefer to build this with the element outputting a formatted version
itself, however this would have high impact and would require a fair amount
of tests to be added.

=cut

define_profile apply_formatter => (
    required => [qw/element/],
    optional => [qw/formatter/],
    typed => {
        element => 'Zaaksysteem::ZTT::Element',
        formatter => 'Maybe[HashRef]'
    }
);

sub apply_formatter {
    my $self = shift;

    my $params = assert_profile(shift)->valid;

    my $element = $params->{element};
    my $formatter = $params->{formatter};

    my $value = $element->value;

    return unless $formatter;

    if ($formatter->{ name } eq 'date') {

        # strptime is sensitive and will bork out if given incorrect input
        eval {
            my $datetime = strptime('%d-%m-%Y', $value);
            my $formatted = $datetime->set_locale('nl')->strftime('%{day} %B %Y');
            $element->value($formatted);
        };

        # this is for the convenience of developers, there's currently no other way
        # to communicate the failure, and a caught exception with no output makes for
        # a lot of time wasted debugging.
        if ($@) {
            warn "Exception when ztt formatting: $@. " .
                "Could not strptime '$value', not applying formatting\n";
        }

        return;
    }

    if($formatter->{ name } eq 'link_to') {
        my ($link_target, $link_name) = @{ $formatter->{ args } };

        unless($link_name) {
            $link_name = $link_target;
        }

        my $value = $element->value;

        $value = shift @{ $value } if ref $value eq 'ARRAY';

        if ($value) {
            $element->type('hyperlink');
            $element->value(sprintf($link_target, $value));
            $element->title(sprintf($link_name, $value));
        }
    }

    if($formatter->{ name } eq 'hyperlink') {
        my ($uri, $title) = @{ $formatter->{ args } };

        $element->type('hyperlink');
        $element->value($uri);
        $element->title($title);
    }

    if($formatter->{ name } eq 'image_size') {
        my ($width, $height) = @{ $formatter->{ args } };

        $element->type('image');
        $element->value(
            {
                image_path => $value,
                width      => $width,
                height     => $height,
            }
        );
    }

    if($formatter->{ name } eq 'currency' && length($value)) {
        my ($format) = @{ $formatter->{ args } };

        my $decimals = 2;
        if ($format && $format eq 'whole_number') {
            $decimals = 0;
        }

        my $locale = Locales->new($self->locale);

        # TODO This doesn't cope with locales that don't have groups of 3
        # digits between their "thousands" separator.
        my $numf = $locale->numf(1);

        my $formatter = Number::Format->new(
            THOUSANDS_SEP  => ($numf == 1 ? ',' : '.'),
            DECIMAL_POINT  => ($numf == 1 ? '.' : ','),
            DECIMAL_DIGITS => $decimals,
            DECIMAL_FILL   => 1,
        );

        $element->cleaner_skip_set(valuta => 1);
        $element->value($formatter->format_number($value));
    }

    if ($formatter->{ name } eq 'list') {
        $element->type('list');
        $element->cleaner_skip_set(array => 1);
    }

    if ($formatter->{ name } eq 'break') {
        my $value = $element->value;

        # Check that the value has *some* printable chars.
        if ($value =~ m[\w]u) {
            $element->value($value . "\n");
        }
    }

    if ($formatter->{ name } eq 'strip_when_empty') {
        $element->strip_when_empty(1);
    }
}

=head2 fetch_string

Scan through the available string fetcher to see if any of these
are able to provide an interpolation for the given magic string.

The first one to answer with an interpolation gets the cake. This
means that when configuring the string fetchers the order matters,
if they share namespace.

=cut

sub fetch_string {
    my $self = shift;
    my $tag = shift;

    for my $fetcher ($self->all_string_fetchers) {
        my $interpolation = $fetcher->($tag);

        if ($interpolation) {
            $self->apply_formatter({
                element => $interpolation,
                formatter => $tag->formatter
            });

            return $interpolation;
        }
    }

    # Return an empty element by default, if it cannot be found by string fetchers
    # the tag should be removed from the document still
    return Zaaksysteem::ZTT::Element->new(value => '');
}

=head2 process_template

This method takes one parameter and attempts to process it as a template. If
the provided item is not an instance of L<Zaaksysteem::ZTT::Template>, that
package's L<new_from_thing|Zaaksysteem::ZTT::Template/new_from_thing>
constructor will be used to create a template instance.

Next, this method will collect all modifications that can be found in the
template body, and will attempt to apply them to the template. A singular run
of applying modifications may result in a finished document, or an
intermediate document that has new directives that need to be processed. In
this latter case up to 10 interative attempts are done to resolve all
directives.

The simplest form of template processing would be something like the
following:

    my $document = $ztt->process_template("[[ system.uname ]]");

=cut

sub process_template {
    my $self = shift;
    my $template = shift;
    my $recurse_limit = shift || 10;

    unless (eval { $template->isa('Zaaksysteem::ZTT::Template'); }) {
        $template = Zaaksysteem::ZTT::Template->new_from_thing($template);
    }

    while($recurse_limit--) {
        my @mods = $template->modifications;

        last unless scalar(@mods);

        for my $mod (@mods) {
            $self->apply($mod, $template);
        }
    }

    if($template->can('post_process')) {
        $template->post_process($self->all_string_fetchers);
    }

    return $template;
}

=head2 apply

This method takes a modification and a template, and attempts to apply said
modification to the template. Currently the following modification types are
generically supported:

=over 4

=item replace

Calls L<Zaaksysteem::ZTT::Template/replace> with the modification's text
selection and a fetched_string.

=item iterate

Calls L<Zaaksysteem::ZTT::Template/iterate> with a registered iteration
context and the modification's selection.

=item iterate_inline

Calls L<Zaaksysteem::ZTT::Template/iterate_inline> with a registered iteration
context and the modification's selection.

=back

This method may throw a C<ztt/template/apply> exception when a unknown
modification type is found. It also will not wrap the concrete application
call in a try/catch block, so lower-level exceptions may bubble up.

Also a C<ztt/template/iterator> or C<ztt/template/inline_iterator> exception
may be thrown when a iterator context that is not registered is requested by
a modification.

=cut

sub apply {
    my ($self, $mod, $template) = @_;

    $template->increment_modifications;

    my %dispatch = (
        replace => sub {
            $template->replace(
                $mod->selection,
                $self->fetch_string($mod->selection->tag),
                $self->string_fetchers
            );
        },

        iterate => sub {
            my $iterator_type = $mod->selection->iterate;

            unless (exists $self->iterators->{ $iterator_type }) {
                throw('ztt/template/iterator', 'Unable to find iterator for type ' . $iterator_type);
            }

            $template->iterate(
                $self->iterators->{ $iterator_type }->(),
                $mod->selection
            );
        },

        iterate_inline => sub {
            my $iterator_type = $mod->selection->iterate;

            unless (exists $self->iterators->{ $iterator_type }) {
                throw('ztt/template/inline_iterator', 'Unable to find iterator for type ' . $iterator_type);
            }

            $template->iterate_inline(
                $self->iterators->{ $iterator_type }->(),
                $mod->selection
            );
        },
    );

    unless (exists $dispatch{ $mod->type }) {
        throw('ztt/template/apply', "Don't know how to handle modifications of '$mod->type' type.");
    }

    $dispatch{ $mod->type }->($mod, $template);
}

__PACKAGE__->meta->make_immutable;
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
