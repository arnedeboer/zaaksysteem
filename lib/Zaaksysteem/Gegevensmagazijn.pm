package Zaaksysteem::Gegevensmagazijn;
use Moose;

=head1 NAME

Zaaksysteem::Gegevensmagazijn - Gegevensmagazijn logic for Natuurlijk Personen

=head2 SYNOPSIS

    use Zaaksysteem::Gegevensmagazijn;

    my $gm = Zaaksysteem::Gegevensmagazijn->new(
        schema => $schema,
    );

    my $natuurlijk_persoon_rs = $gm->search($search, $search_opts, %params);

=cut

use Zaaksysteem::Tools;
use DateTime;

with 'MooseX::Log::Log4perl';

=head1 ATTRIBUTES

=head2 schema

An L<Zaaksysteem::Schema> object

=cut

has schema => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Schema',
    required => 1,
);


=head2 filter_afnemerindicatie

Filter names for afnemerindicatie

=cut

has filter_afnemerindicatie => (
    is => 'ro',
    isa => 'ArrayRef',
    default => sub {
        return [
            '' => 'Met of zonder afnemersindicatie',
            1 => 'Met afnemerindicitatie',
            2 => 'Zonder afnemerindicitatie',
        ],
    },
);


=head2 filter_active

Filter names for active or inactive persons

=cut

has filter_active => (
    is => 'ro',
    isa => 'ArrayRef',
    default => sub {
        return [
            '' => 'Actieve of inactieve personen',
            1 => 'Actieve personen',
            2 => 'Inactieve personen',
        ],
    },
);


=head2 filter_cases

Filter names for involvement in cases

=cut

has filter_cases => (
    is => 'ro',
    isa => 'ArrayRef',
    default => sub {
        return [
            '' => 'Wel of niet betrokken bij zaken',
            1 => 'Betrokken bij zaken',
            2 => 'Niet betrokken bij zaken',
        ],
    },
);


=head2 filter_address

Filter names for addresses

=cut

has filter_address => (
    is => 'ro',
    isa => 'ArrayRef',
    default => sub {
        return [
            '' => 'Met of zonder adres',
            1 => 'Met adres',
            2 => 'Zonder adres',
        ],
    },
);

=head2 filter_deceased

Filter names for deceased or living persons

=cut

has filter_deceased => (
    is => 'ro',
    isa => 'ArrayRef',
    default => sub {
        return [
            '' => 'Overleden of levende personen',
            1 => 'Overleden personen',
            2 => 'Levende personen',
        ],
    },
);

=head2 filter_muncipality

Filter names for muncipality

=cut

has filter_muncipality => (
    is => 'ro',
    isa => 'ArrayRef',
    default => sub {
        return [
            '' => 'Binnen- of buitengemeentelijk',
            1 => 'Binnengemeentelijke personen',
            2 => 'Buitengemeentelijke personen',
        ],
    },
);

=head1 METHODS

=head2 search

Search in Zaaksysteem for with specific filters.
The search and search opts arguments are passed directly into a DBIx Resultset search
The params are boolean values for the defined filters (as seen in filter_xxxx attributes):

=over

=item object_subscription

True if object subscriptions must exists, false if they don't

=item cases

True if people should be associated with cases, false if not

=item address

True if people have an addres, false if they have empty addresses

=item deceased

True if people are deceased, false if they are alive

=item muncipality

True if people are living within the muncipality, false it they aren't.

=back

If the parameter isn't supplied the filter isn't used in the search.

The parameter C<freeform_filter> searches for persons in the search_term of natuurlijk persoon
or if the text is found in the object_subscription (if this filter is supplied).

    $gm->search(
        $search,
        $search_opts,
        %params,
    );

=cut

define_profile search => (
    optional => {
        object_subscription => 'Bool',
        cases               => 'Bool',
        address             => 'Bool',
        active              => 'Bool',
        deceased            => 'Bool',
        muncipality         => 'Bool',
        freeform_filter     => 'Str',
    },
    field_filters => {
        active              => \&_field_filter,
        address             => \&_field_filter,
        cases               => \&_field_filter,
        deceased            => \&_field_filter,
        muncipality         => \&_field_filter,
        object_subscription => \&_field_filter,
    },
);

sub _field_filter {
    my $val = shift;

    if (length($val) > 0) {
        return $val eq 1 ? 1 : 0;
    }

}

sub search {
    my $self        = shift;
    my $search      = shift;
    my $search_opts = shift;
    my $opts        = assert_profile({@_})->valid;

    my $schema = $self->schema;

    # Only look for active persons
    my $rs = $self->schema->resultset('NatuurlijkPersoon')->search_rs($search, $search_opts);

    if (exists $opts->{freeform_filter}) {
        $rs = $rs->search_rs({ 'me.search_term' => { ilike => "%" . $opts->{freeform_filter} . "%" } });
    }
    my $os;

    if (exists $opts->{object_subscription}) {
        $os = $self->_search_object_subscriptions_as_query(
            $opts->{object_subscription},
            $opts->{freeform_filter}
        );

        if (!$opts->{object_subscription}) {
            $rs = $rs->search_rs(
                {
                    -or => [
                        { 'me.authenticated' => [ 0, undef ] },
                        {
                            'me.id'            => { 'not in' => $os },
                            'me.authenticated' => 1
                        },
                    ]
                }
            );
        }
        else {
            $rs = $rs->search_rs({ 'me.id' => { 'in' => $os }, 'me.authenticated' => 1 });
        }
    }

    if (exists $opts->{active}) {
        $rs = $rs->search_rs({ 'me.active' => $opts->{active}});
    }

    my $cases;
    if (exists $opts->{cases}) {
        my $requestors = $self->_search_case_requestors_as_query();
        $rs = $rs->search_rs({ 'me.id' => { ($opts->{cases} ? 'in' : 'not in' ) => $requestors } });
    }

    if (exists $opts->{address}) {
        $rs = $rs->search_rs({ 'me.adres_id' => $opts->{address} ? { '!=' => undef } : undef });
    }

    if (exists $opts->{deceased}) {
        $rs = $rs->search_rs({ 'me.datum_overlijden' => $opts->{deceased} ? { '!=' => undef } : undef });
    }

    if (exists $opts->{muncipality}) {
        $rs = $rs->search_rs({ 'me.in_gemeente' => $opts->{muncipality} });
    }

    return $rs;
}

sub _search_object_subscriptions_as_query {
    my ($self, $active, $freeform) = @_;


    my $dt;
    if ($active) {
        my $dtf = $self->schema->storage->datetime_parser;
        $dt = $dtf->format_datetime(DateTime->now->subtract(days => 1));
    }

    return $self->schema->resultset('ObjectSubscription')->search_rs({
        local_table => 'NatuurlijkPersoon',
        $dt ? ( date_created => { '<' => $dt } ) : (),
        date_deleted => undef,
        $freeform ? ( object_preview => { ilike => "%$freeform%" }) : (),
    })->get_column('local_id::integer')->as_query;
}

sub _search_case_requestors_as_query {
    my ($self) = @_;

    return $self->schema->resultset('ZaakBetrokkenen')
        ->search_rs(
        {
            'me.betrokkene_type' => 'natuurlijk_persoon',
            'me.deleted'         => undef,
            'zaak_id.deleted'    => undef,
        },
        { join => 'zaak_id' },
    )->get_column('me.gegevens_magazijn_id')->as_query;

}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
