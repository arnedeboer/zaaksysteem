package Zaaksysteem::StUF;

use Moose;
use Moose::Util qw[apply_all_roles];

use Zaaksysteem::Exception;
use Zaaksysteem::Profile;
use Zaaksysteem::Constants qw(STUF_VERSIONS);

use Zaaksysteem::StUF::Parser::0204;
use Zaaksysteem::StUF::Parser::0301;
use Zaaksysteem::StUF::Stuurgegevens;

use Zaaksysteem::StUF::Body::PRS;           # 0204
use Zaaksysteem::StUF::Body::NPS;           # 0301

use Zaaksysteem::StUF::Body::NNP;

use Zaaksysteem::StUF::Body::ADR;           # 0204
use Zaaksysteem::StUF::Body::AOA;           # 0301

use Zaaksysteem::StUF::Body::TGO;           # 0301


use Zaaksysteem::StUF::Body::VBO;
use Zaaksysteem::StUF::Body::R02;
use Zaaksysteem::StUF::Body::R03;

use Zaaksysteem::StUF::Body::OPR;

use Zaaksysteem::StUF::SOAP;

use Data::Dumper;

has 'stuurgegevens'                         => (
    'is'        => 'rw',
);

has 'parser'                                => (
    'is'        => 'ro',
);

has 'parser_options'                        => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub { return {}; }
);

has 'entiteittype'                          => (
    'is'        => 'rw',
);

has 'soap_ssl_key' => (
    'is' => 'rw',
);

has 'soap_ssl_crt' => (
    'is' => 'rw',
);

has 'body'                                  => (
    'is'        => 'rw',
    'isa'       => 'Maybe[ArrayRef]',
    'default'   => sub { return []; },
);

has 'namespace'         => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my $self            = shift;
        return 'http://www.egem.nl/StUF/StUF0204' unless $self->parser;

        return $self->parser->namespace;
    }
);

has 'element'          => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my $self            = shift;
        return unless $self->parser;

        return $self->parser->element;
    }
);

has 'asynchroon'        => (
    'is'        => 'rw',
);

has 'async_url'         => (
    'is'        => 'rw',
    'isa'       => 'Maybe[Str]',
);

has 'sync_url'          => (
    'is'        => 'rw',
    'isa'       => 'Maybe[Str]',
);

has 'party_name'        => (
    'is'        => 'rw',
    'isa'       => 'Maybe[Str]',
);

has 'version'        => (
    'is'        => 'rw',
    'isa'       => 'Maybe[Str]',
);

has 'mutatiesoort'  => (
    'is'        => 'rw',
    'isa'       => 'Maybe[Str]',
);

use constant STUF_SOAP_CONFIG => {
    async       => {
        port            => 'StUFBGAsynchronePort',
        service         => 'StUFBGAsynchroon',
    },
    sync        => {
        port            => 'StUFBGSynchronePort',
        service         => 'StUFBGSynchroon',
    }
};

use constant BERICHTSOORT_TO_TYPE => {
    'lk01'      => 'async',
    'lv01'      => 'sync',
    'la01'      => 'sync',
    'lv02'      => 'async',
    'la02'      => 'async',
    'bv01'      => 'sync',
    'fo01'      => 'sync',
};

use constant BERICHTSOORT_TO_CALL => {
    'lk01'      => 'ontvangKennisgeving',
    'lv01'      => 'beantwoordSynchroneVraag',
    'la01'      => 'sync',
    'lv02'      => 'ontvangAsynchroneVraag',
    'la02'      => 'ontvangAsynchroonAntwoord',
    'bv01'      => 'sync',
    'fo01'      => 'ontvangFout',
};

=head1 NAME

Zaaksysteem::StUF - A base class for parsing StUF objects

=head1 SYNOPSIS

    my $stuf    = Zaaksysteem::StUF->from_file('filename-prs-add.xml');

    print $self->entiteittype;
    # print PRS


=head1 DESCRIPTION

Modules are interfaces with the outside world in catalyst. These interfaces can
be configured in our sysin/interface component.

=head1 ATTRIBUTES

=head2 entiteittype

isa: Str

=head2 berichtsoort

isa: Str

=head2 sectormodel

Type: Str

=head2 versieStUF

Type: Num

=head2 versieSectormodel

Type: Num

=head1 METHODS

=head2 from_file($FILENAME)

Return value: StUF object

=cut

sub from_file {
    my $class    = shift;
    my $filename = shift;
    my $version  = shift || '0204';
    my $xml;

    # Get XML
    open(my $fh, "<:encoding(UTF-8)", $filename) or
        throw(
            'stuf/from_file/filename_error',
            "Failed loading file '$filename': $!"
        );

    while (<$fh>) {
        $xml    .= $_;
    }

    close($fh);

    return $class->from_xml(
        $xml,
        {
            version => $version
        }
    );
}

sub from_xml {
    my $class = shift;
    my $xml = shift;
    my $parser_options = shift || {};

    throw(
        'stuf/from_parser_object/no_xml',
        'No XML supplied'
    ) unless $xml;

    my $parser;
    my $stufversion = $parser_options->{version};

    if ($stufversion && $stufversion eq '0310') {
        $parser =  Zaaksysteem::StUF::Parser::0301->new(
            %{$parser_options},
            xml => $xml,
        );
    } else {
        $parser =  Zaaksysteem::StUF::Parser::0204->new(
            %{$parser_options},
            xml => $xml,
        );
    }


    $parser->process;

    throw(
        'stuf/from_parser_object/no_data',
        'Parser does not contain data'
    ) unless $parser->data;

    my $self                = $class->new(
        parser          => $parser,
        parser_options  => $parser_options,
        version         => ($stufversion || '0204')
    );
}

sub BUILD {
    my $self                = shift;

    if ($self->version && $self->version eq '0310') {
        apply_all_roles(
            $self, __PACKAGE__ . '::' . $self->version
        );

    }

    if ($self->parser && $self->parser->data) {
        throw(
            'stuf/from_parser_object/invalid_stuf',
            'Cannot find stuurgegevens element in StUF xml'
        ) unless exists($self->parser->data->{stuurgegevens});

        $self->entiteittype(
            uc($self->parser->data->{stuurgegevens}->{entiteittype})
        );

        my $stuurgegevens   = Zaaksysteem::StUF::Stuurgegevens->new(
            %{ $self->parser->data->{stuurgegevens} }
        );

        $self->stuurgegevens(
            $self->_parse_stuurgegevens($stuurgegevens)
        );

        $self->_parse_body;
    }

    throw(
        'stuf/new/missing_entiteittype',
        'Entiteittype not given'
    ) unless $self->entiteittype;

    apply_all_roles(
        $self, __PACKAGE__ . '::' . uc($self->entiteittype)
    );
}

sub _parse_body {
    my $self            = shift;
    my $body            = $self->parser->data->{body};

    my $objectclass     = 'Zaaksysteem::StUF::Body::' . $self->entiteittype;

    for my $xml_object (@{ $body->{ $self->entiteittype } }) {
        my $object      = $objectclass->new();

        $object->_body_params($xml_object);
        $object->_load_body_params;

        push(
            @{ $self->body },
            $object
        );
    }
}

sub _parse_stuurgegevens {
    my $self            = shift;
    my ($stuurgegevens) = @_;

    if (
        $stuurgegevens->{kennisgeving} &&
        $stuurgegevens->kennisgeving->{mutatiesoort}
    ) {
        $self->mutatiesoort($stuurgegevens->{kennisgeving}->{mutatiesoort});
    }

    return @_;
}


=head2 to_xml

Generate XML from StUF object

=cut

sub to_xml {
    my $self        = shift;

    throw(
        'stuf/no_element_set',
        'No element set on object, don\'t know where to dispatch to'
    ) unless $self->element;

    throw(
        'stuf/no_namespace_set',
        'No namespace set on object, don\'t know where to dispatch to'
    ) unless $self->namespace;


    my $parser;
    if ($self->version && $self->version eq '0310') {
        $parser      = Zaaksysteem::StUF::Parser::0301->new(
            %{ $self->parser_options },
            %{ $self->TO_STUF },
            namespace       => $self->namespace,
            element         => $self->element,
        );
    } else {
        $parser      = Zaaksysteem::StUF::Parser::0204->new(
            %{ $self->parser_options },
            %{ $self->TO_STUF },
            namespace       => $self->namespace,
            element         => $self->element,
        );
    }

    return $parser->to_xml;
}

=head2 acknowledge(\%options)

Send out a bevestiging

B<Options>

=over 4

=item date

=item reference_id

=back

=cut

define_profile acknowledge => (
    required        => [qw/reference_id date/],
);

sub acknowledge {
    my $self                    = shift;
    my $options                 = assert_profile(shift || {})->valid;

    my $stufobject      = Zaaksysteem::StUF->new(
        entiteittype    => $self->entiteittype,
        stuurgegevens   => $self->stuurgegevens,
        namespace       => 'http://www.egem.nl/StUF/StUF0204',
        element         => 'bevestigingsBericht',
    );

    $stufobject->body(undef);

    $stufobject->stuurgegevens->berichtsoort('Bv01');
    $stufobject->stuurgegevens->tijdstipBericht(
        $options->{date}->strftime('%Y%m%d%H%M%S00')
    );

    $stufobject->stuurgegevens->referentienummer(
        $options->{reference_id}
    );

    return $stufobject;
}

define_profile fout => (
    required => [qw[reference_id date code omschrijving]],
    optional => [qw[plek]]
);

sub fout {
    my $self                    = shift;
    my $options                 = assert_profile(shift || {})->valid;

    my $parser      = Zaaksysteem::StUF::Parser::0204->new(
        %{ $self->parser_options },
        namespace       => 'http://www.egem.nl/StUF/StUF0204',
        element         => 'foutBericht',
        stuurgegevens   => $self->stuurgegevens,
        body            => {
            code            => $options->{code},
            plek            => $options->{plek} || 'server',
            omschrijving    => substr($options->{omschrijving}, 0, 200),
        }
    );

    return $parser->to_xml;
}


=head2 set_afnemerindicatie(\%options)

Return value: L<Zaaksysteem::StUF::PRS>

    my $stuf    = Zaaksysteem::StUF->new(
        entiteittype    => 'PRS',
        stuurgegevens   => Zaaksysteem::StUF::Stuurgegevens->new(
            zender              => {
                applicatie          => 'ZSNL',
            },
            ontvanger           => {
                applicatie          => 'CGM',
            },
        ),
    )->set_afnemerindicatie(
        {
            reference_id    => 29237824,
            date            => DateTime->now(),
            sleutelGegevensbeheer => 33,
        }
    );

Sets a afnemerindicatie on a given C<sleutelGegevensbeheer>

B<Options>

=over 4

=item date

=item reference_id

=item sleutelGegevensbeheer

=item afnemerindicatie [optional]

default: 1

=back

=cut

define_profile set_afnemerindicatie => (
    required        => [qw/sleutelGegevensbeheer date reference_id/],
    optional        => [qw/afnemerindicatie/],
    defaults        => {
        afnemerindicatie    => 1,
    }
);

sub set_afnemerindicatie {
    my $self                        = shift;
    my $options                     = assert_profile(shift || {})->valid;

    throw(
        'stuf/set_afnemerindicatie/invalid_stuurgegevens',
        'Cannot run this function without stuurgegevens set on object'
    ) unless $self->stuurgegevens;

    my $stufobject      = Zaaksysteem::StUF->new(
        entiteittype    => $self->entiteittype,
        stuurgegevens   => $self->stuurgegevens,
        namespace       => 'http://www.egem.nl/StUF/sector/bg/0204',
        element         => 'kennisgevingsBericht',
        asynchroon      => 1,
    );

    $stufobject->stuurgegevens->berichtsoort('Lk01');
    $stufobject->stuurgegevens->entiteittype($self->entiteittype);

    $stufobject->stuurgegevens->referentienummer(
        $options->{reference_id}
    );
    $stufobject->stuurgegevens->tijdstipBericht(
        $options->{date}->strftime('%Y%m%d%H%M%S00')
    );

    $stufobject->stuurgegevens->vraag({
        indicatorAfnemerIndicatie   => $options->{afnemerindicatie},
    });

    my $entiteit_class = 'Zaaksysteem::StUF::Body::' . $self->entiteittype;

    $stufobject->body(
        [
            $entiteit_class->new(
                sleutelGegevensbeheer   => $options->{sleutelGegevensbeheer},
                verwerkingssoort        => 'I',
            ),
        ]
    );

    return $stufobject;
}


=head2 as_params

Retrieve subject as key => value pairs.

=cut

sub as_params {
    my $self            = shift;

    if (
        $self->mutatiesoort && (
            $self->mutatiesoort eq 'W' ||
            $self->mutatiesoort eq 'C'
        )
    ) {
        return $self->body->[1]->as_params;
    }

    if ($self->stuurgegevens->antwoord) {
        return [ map { $_->as_params } @{ $self->body } ];
    }

    return $self->body->[0]->as_params;
}

=head2 TO_STUF

Returns this object as a hash for use in a StUF message

=cut

sub TO_STUF {
    my $self        = shift;
    my $rv          = {
        stuurgegevens   => $self->stuurgegevens->TO_STUF,
    };

    return $rv unless $self->body;

    my @body;
    for my $body (@{ $self->body }) {
        push(@body, $body->TO_STUF);
    }

    $rv->{body}     = {
        $self->entiteittype     => (
            scalar(@body) > 1
                ? \@body
                : $body[0]
        )
    };

    return $rv;
}

=head2 $stuf->load_from_interface($INTERFACE_OBJECT)

Loads necessary values from the interface config into this object

=cut

sub load_from_interface {
    my $self                    = shift;
    my $interface               = shift;

    my $config                  = $interface->get_interface_config;

    $self->async_url(
        $config->{mk_async_url}
    );

    $self->sync_url(
        $config->{mk_sync_url}
    );

    $self->party_name(
        $config->{mk_ontvanger}
    );
}

=head2 $stuf->dispatch

Dispatches this xml message to "the other side" through SOAP, and returns
a new SOAP object containing the response

=cut

sub dispatch {
    my $self                    = shift;

    ### Recognize berichtsoort, and find out correct port
    my $type                    = BERICHTSOORT_TO_TYPE->{
        lc($self->stuurgegevens->berichtsoort)
    };

    my $stufdata                = STUF_SOAP_CONFIG->{$type};

    my $endpoint                = $self->sync_url;
    $endpoint                   = $self->async_url if $type eq 'async';

    my $dispatcher              = Zaaksysteem::StUF::SOAP->new(
        soap_ssl_key        => $self->soap_ssl_key,
        soap_ssl_crt        => $self->soap_ssl_crt,
        soap_service        => $stufdata->{service},
        soap_port           => $stufdata->{port},
        soap_endpoint       => $endpoint,
        soap_action         => '{' . $self->namespace . '}' . $self->element,
    );

    my ($reply, $trace) = $dispatcher->dispatch(
        BERICHTSOORT_TO_CALL->{ lc($self->stuurgegevens->berichtsoort) },
        $self->TO_STUF
    );

    if ($reply) {
        return Zaaksysteem::StUF->from_xml(
            $trace->response->content
        );
    }
}

__PACKAGE__->meta->make_immutable();




__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 BERICHTSOORT_TO_CALL

TODO: Fix the POD

=cut

=head2 BERICHTSOORT_TO_TYPE

TODO: Fix the POD

=cut

=head2 BUILD

TODO: Fix the POD

=cut

=head2 STUF_SOAP_CONFIG

TODO: Fix the POD

=cut

=head2 fout

TODO: Fix the POD

=cut

=head2 from_xml

TODO: Fix the POD

=cut

