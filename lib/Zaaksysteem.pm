package Zaaksysteem;

=head1 NAME

Zaaksysteem - Case management system for improving business processes

=head1 SYNOPSIS

  # When cloned from source, you can start the local development server by running:

  bash$ ./script/zaaksysteem_server.pl

  ### Alternative, start fastcgi process with two instances

  bash# ./script/dev_fastcgi.pl

=head1 DESCRIPTION

Zaaksysteem is a case management solution for businesses. Althoug we primarily
focues on local government projects. The program is widely used in other SaaS
deployments

=head1 DOCUMENTATION

User documentation can be found on our wiki page:
L<http://wiki.zaaksysteem.nl/>

For development documentation, which gives insight in our API documentation. But also for
connecting Frontend code to our Backend, please start by reading our Manual:

L<Zaaksysteem::Manual>

This document gives insight in the API for Zaaksysteem

=head1 METHODS

B<Please, do NOT create any methods in this namespace, please use:>

L<Zaaksysteem::General::Actions>

You will be able to test them better, and documentation is in one place

=cut

use Moose;

use Digest::MD5 qw(md5_hex);
use File::Basename;
use IO::Socket;
use JSON::Path;
use Log::Log4perl::Catalyst;
use Log::Log4perl::MDC;
use Memory::Usage;
use Sys::Hostname;
use Time::HiRes qw/gettimeofday tv_interval/;
use Try::Tiny;

use Zaaksysteem::Request;
use Zaaksysteem::Cache;
use Zaaksysteem::Tools;

use Catalyst qw/
    ConfigLoader
    Static::Simple

    Authentication
    Authorization::Roles

    Params::Profile

    Unicode::Encoding
    I18N
    ClamAV

    +Zaaksysteem::StatsD
    +Zaaksysteem::XML::Compile
    CustomErrorMessage

    Cache

    Session
    Session::State::Cookie
    Session::Store::Memcached
/;

# Plugins which will be loaded based on env variables
my @additional_plugins = qw();

use CatalystX::RoleApplicator;

extends qw/Catalyst Zaaksysteem::General/;

# Changed the version to adhere Perl best practises, using rc notation could break stuff
# This means that for release "v1.23.4rc1" $VERSION should be "v1.23.4.1"
our $VERSION = 'v3.21.0';

#extends 'Catalyst';
#with 'CatalystX::LeakChecker';
#with 'CatalystX::LeakChecker';

__PACKAGE__->request_class('Zaaksysteem::Request');
__PACKAGE__->apply_request_class_roles(qw/
    Zaaksysteem::TraitFor::Request::Params
/);

# Configure the application.
#
# Note that settings in zaaksysteem.conf (or other external
# configuration file that you set up manually) take precedence
# over this when using ConfigLoader. Thus configuration
# details given here can function as a default configuration,
# with an external configuration file acting as an override for
# local deployment.

sub _assert_configfile {
    foreach ($ENV{ZAAKSYSTEEM_CONF}, '/etc/zaaksysteem/zaaksysteem.conf', 'etc/zaaksysteem.conf') {
        return $_ if (defined $_ && -f $_);
    }
    throw('Zaaksysteem', "No configfiles found, unable to start Zaaksysteem");
}

my $configfile = _assert_configfile;

__PACKAGE__->config(
    'Plugin::ConfigLoader' => {
        file => $configfile,
        driver => {
            # Make sure we can use arrays in our config as well
            'General' => { -ForceArray => 1 }
        },
    },
    'name'              => 'Zaaksysteem',
    'View::TT'          => {
        tpl       => 'zaak_v1',
        locale    => 'nl_NL',
    },
    'View::JSON' => {
        allow_callback  => 0,
        expose_stash    => 'json',
    },
    'View::JSONlegacy' => {
        allow_callback  => 0,    # defaults to 0
        expose_stash    => [ qw(json) ],
    },
    'View::Email'          => {
        stash_key => 'email',
        template_prefix => 'tpl/zaak_v1/nl_NL',
        default => {
            content_type => 'text/plain',
            charset => 'utf-8',
            from    => 'info@zaaksysteem.nl',
            view        => 'TT',
        },
        sender => {
            'mailer'    => 'Sendmail'
        }
    },
    'default_view'      => 'TT',
    'static'            => {
        ignore_extensions => [ qw/tmpl tt tt2/ ],
    },
    'Plugin::Authentication' => {
        default_realm => 'zaaksysteem',

        zaaksysteem => {
            interface => 'authldap',
            credential =>
                { class => "+Zaaksysteem::Auth::Credential::LDAP", },
            store => { class => "+Zaaksysteem::Auth::Store", }
        },
        api => {
            interface => 'authldap',
            credential =>
                { class => "+Zaaksysteem::Auth::Credential::Digest", },
            store => { class => "+Zaaksysteem::Auth::Store", }
        },
        saml => {
            interface => 'authldap',
            credential => { class => "+Zaaksysteem::Auth::Credential::SAML", },
            store => { class => "+Zaaksysteem::Auth::Store", }
        },
    },

    recaptcha => {
        pub_key => '6LfWuOkSAAAAAE9lve8PsEIBSf7FkBeTeE4Wa-i4',
        priv_key => '6LfWuOkSAAAAABYINXupw1FyybAADV7I4FIgp9TE',
        options => { theme => 'clean', lang => 'nl' }
    },

    # So this need not to be in the configuration file
    'Model::DB' => {
        connect_info => {
            dsn            => "dbi:Pg:dbname=nonexistent",
            pg_enable_utf8 => 1
        }
    },

    'custom-error-message' => {
        'error-template' => 'error.tt',
    },
);

__PACKAGE__->mk_classdata($_) for qw/
    _additional_static
    customer
/;

__PACKAGE__->set_zs_version($VERSION);
__PACKAGE__->config->{config_directory} = dirname($configfile);

{
    my @logfile =
           (-e '/etc/zaaksysteem/log4perl.conf'    && '/etc/zaaksysteem/log4perl.conf')
        || (-e '/vagrant/etc/log4perl.custom.conf' && '/vagrant/etc/log4perl.custom.conf')
        || (-e '/vagrant/etc/log4perl.conf'        && '/vagrant/etc/log4perl.conf')
        || ();

    __PACKAGE__->log(
        Log::Log4perl::Catalyst->new(@logfile)
    );
}

__PACKAGE__->setup(@additional_plugins);

# present the option of not preloading to save ~11 seconds startup time
# during development
unless ($ENV{ZS_DISABLE_STUF_PRELOAD}) {
    __PACKAGE__->xml_compile->add_class([qw/
        Zaaksysteem::StUF::0204::Instance
        Zaaksysteem::StUF::0301::Instance
        Zaaksysteem::StUF::0312::Instance
        Zaaksysteem::XML::MijnOverheid::Instance
    /]);
}

__PACKAGE__->config->{'Plugin::Captcha'} = {
    session_name => 'captcha_string',
    new          => {
        width     => 300,
        height    => 100,
        scramble  => 1,
        ptsize    => 34,
        frame     => 2,
        rndmax    => 5,
        thickness => 1,
        lines     => 25,
        color     => '#FFCC00',
    },
    create   => ['ttf', 'circle', '#3E8FA4', '#999',],
    particle => [1500],
    out => { force => 'jpeg' }
};

sub _setup_authentication_interface {
    my $c           = shift;

    my $realm     = $c->get_auth_realm('default');
    my $module    = $realm->{config}{interface};
    my $interface = $c->model('DB::Interface')->find_by_module_name($module);

    unless($interface) {
        throw('auth/interface', "Unable to locate interface with module '$module'");
    }

    $realm->credential->interface($interface);

    $c->get_auth_realm('api')->credential->interface($interface);
    $c->get_auth_realm('saml')->credential->interface($interface);

    $c->model('DB')->schema->default_resultset_attributes->{ current_user } = $c->model('DB')->schema->current_user(undef);
}

sub _finish_graphing {
    my $c           = shift;
    my $t0          = shift;

    my $result      = int(tv_interval ( $t0, [gettimeofday])*1000);

    my $status      = $c->response->status;
    if (scalar @{ $c->error }) {
        $status = 500;
    }
    #$c->log->debug('Request overall took: ' . $result . ' / status: ' . $c->response->status);
    $c->statsd->timing('request.time', $result);
    $c->statsd->increment('response.status.' . $status, 1);
}



my ($mu, $counter)  = (undef, 0);
if ($ENV{'ZS_MEMORY_USAGE'}) {
    $mu = Memory::Usage->new();
}

sub _dispatch_profile_memory {
    my $c       = shift;
    my $msg     = shift || 'before';
    my $dump    = shift;

    return unless $mu;

    my $authaction = $c->req->action;
    $authaction =~ s|^/|| unless $authaction eq '/';
    $authaction = lc($authaction);

    $mu->record($msg . ' request ' . ++$counter . ': ' . $authaction) if $mu;

    if ($dump) {
        $mu->dump if $mu;
    }
}

sub _dispatch_logger {
    my $c       = shift;

    Zaaksysteem::Tools::set_logger($c->log);

    # Generate a request ID
    $c->stash->{request_id} = sprintf(
        "%s-%s-%s",
        ($c->config->{logging_id} || ''),
        substr($c->get_session_id()|| '', -6),
        substr(md5_hex(($c->config->{gemeente_id} || ''), time, rand), -6),
    );
    Log::Log4perl::MDC->put('request_id', $c->stash->{request_id});
}

sub _dispatch_verify_security {
    my $c       = shift;

    return 1 unless $c->sessionid;

    if (!$c->session->{__session_host}) {
        $c->session->{__session_host} = lc($c->req->uri->host);
        return 1;
    }

    ### Security breach, session copied, ABORT.. *HONK* *HONK*...DEFCON 1
    if ($c->session->{__session_host} ne lc($c->req->uri->host)) {
        $c->log->error('Security warning: URI host doesn\'t match the original session origin host from IP, origination IP: ' . $c->req->address);
        $c->delete_session('Invalid session_host');
        $c->res->redirect($c->uri_for('/'));

        return;
    }

    return 1;
}

around 'dispatch' => sub {
    my $orig    = shift;
    my $c       = shift;

    ### Timer start
    my $t0      = [gettimeofday];
    my $schema  = $c->model('DB')->schema;

    ### Setup dispatching
    $c->_dispatch_logger();
    $c->_dispatch_profile_memory('before');
    $c->_dispatch_debug_queries($schema, 1);

    ### Security checks
    return unless $c->_dispatch_verify_security;

    ### Setup authentication
    $c->_setup_authentication_interface();

    if ($c->user_exists) {
        Log::Log4perl::MDC->put('username', $c->user->username);
    }
    else {
        Log::Log4perl::MDC->put('username', 'no user');
    }

    ### Dispatch!
    my $ret     = $c->$orig(@_);

    ### Post dispatch
    $c->_dispatch_debug_queries($schema);
    $c->_finish_graphing($t0);

    ### Cleanup
    $schema->default_resultset_attributes->{ current_user } = $schema->current_user(undef);

    unless($ENV{ZAAKSYSTEEM_SCHEMA}) {
        $schema->_clear_schema();
        $schema->storage->disconnect;
    }

    $c->_dispatch_profile_memory('after', 1);

    return $ret;
};

sub _dispatch_debug_queries {
    my $c           = shift;
    my $schema      = shift;
    my $reset       = shift;

    if ($reset) {
        $schema->storage->debugobj->reset_query_count;
        return;
    }

    my $queries     = $schema->storage->debugobj->queries;

    if (scalar(@$queries) > 20) {
        $c->log->error('WARNING: More than 20 queries executed: ' . scalar(@$queries));
    }

    if ($ENV{ZS_DBIC_TRACE}) {
        $c->log->debug("\nTotal DB queries executed: " . scalar(@$queries));
    }

}

### Basic zaak authorisation
sub _can_change_messages {
    my ($c) = @_;

    return unless $c->user_exists;

    my $case = $c->stash->{ zaak };
    my $case_open_url = $c->uri_for(sprintf('/zaak/%d/open', $case->id));

    if ($case->is_afgehandeld) {
        $c->push_flash_message(
            'U kunt geen wijzigingen aanbrengen aan deze zaak. Deze zaak is afgehandeld'
        );

        return 1;
    }

    if($case->status eq 'new') {
        unless($case->behandelaar) {
            $c->push_flash_message(sprintf(
                'U bent niet de behandelaar van deze zaak. Klik <a href="%s">hier</a> om in behandeling te nemen.',
                $case_open_url->as_string
            ));
        } elsif ($case->behandelaar->gegevens_magazijn_id eq $c->user->uidnumber) {
            $c->push_flash_message(sprintf(
                'U heeft deze zaak nog niet in behandeling genomen. Klik <a href="%s">hier</a> om in behandeling te nemen.',
                $case_open_url->as_string
            ));
        } else {
            $c->push_flash_message('U bent niet de behandelaar van deze zaak.');
        }
    }

    if($case->status eq 'open' || $case->status eq 'stalled') {
        unless($case->behandelaar) {
            $c->push_flash_message(sprintf(
                'U bent niet de behandelaar van deze zaak. Klik <a href="%s">hier</a> om in behandeling te nemen.',
                $case_open_url->as_string
            ));
        } elsif ($c->user_exists && $case->behandelaar->gegevens_magazijn_id ne $c->user->uidnumber) {
            $c->push_flash_message('U bent niet de behandelaar van deze zaak.');
        }
    }

    if ($case->status eq 'stalled') {
        $c->push_flash_message(sprintf(
            '<span class="flash-urgent">Zaak opgeschort: %s</span>',
            $case->reden_opschorten || ''
        ));
    }
}



=head2 can_change

Determines wether the current user is allowed to make changes
on the current case.

=cut

sub can_change {
    my ($c, $opt) = @_;

    my $zaak = $c->stash->{zaak};
    return unless $zaak;

    # only once per request
    unless($c->req->param('_can_change_messages')) {
        $c->req->param('_can_change_messages', 1);
        $c->_can_change_messages;
    }


    return if $zaak->is_afgehandeld && !$opt->{ignore_afgehandeld};

    return 1 if $c->check_any_zaak_permission('zaak_beheer','zaak_edit');

    if (
        !$zaak->behandelaar ||
        !$c->user_exists ||
        !$c->user->uidnumber ||
        (
            $zaak->behandelaar &&
            $zaak->behandelaar->gegevens_magazijn_id ne $c->user->uidnumber &&
            (
                !$zaak->coordinator ||
                $zaak->coordinator->gegevens_magazijn_id ne $c->user->uidnumber
            )
        )
    ){
        $c->push_flash_message(
            'U kunt geen wijzigingen aanbrengen aan deze zaak, u bent geen behandelaar / coordinator.'
        );

        return;
    }

    return 1;
}


=pod

Implements case action 'queue coworker changes'. This makes exclusive editing
rights possible for case behandelaars, all other users will be forced to submit
to their approval. (except admins)

Returns a true value when the case is using that action, and the current user
is not the behandelaar (owner).

=cut

sub check_queue_coworker_changes {
    my $c    = shift;
    my $zaak = $c->stash->{zaak};

    # admins always get what they want, this doesn't need to be queued
    return if $c->check_any_user_permission('admin');

    return unless $c->user;

    return $zaak->check_queue_coworker_changes($c->user->uidnumber);
}



# See:
# http://search.cpan.org/~bobtfish/Catalyst-Plugin-Session-0.35/lib/Catalyst/Plugin/Session.pm
#
sub push_flash_message {
    my $c       = shift;
    my $message;

    if (UNIVERSAL::isa($_[0], 'HASH')) {
        $message    = shift;
    } else {
        $message    = sprintf shift, @_;
    }

    my $result;

    $result = $c->flash->{result} || [];

    # As long as flash->{result} is used without this sub, we need to accomodate
    unless(ref $result && ref $result eq 'ARRAY') {
        $result = [$result];
    } elsif (!ref($result)) {
        $result = {
            message         => $result,
            type            => 'info'
        };
    }

    if (!UNIVERSAL::isa($message, 'HASH')) {
        $message    = {
            message     => $message,
            type        => 'info',
        };
    }

    # Since same code runs multiple times sometimes, check for unicity
    my $lookup = { map { $_->{message} =>  1} grep { UNIVERSAL::isa($_, 'HASH') } @$result };
    unless($lookup->{$message->{message}}) {
        push @$result, $message;
    }

    $c->flash->{result} = $result;

    return $result;
}

sub uri_format_ampersands {
    my ($c, $uri) = @_;

    $uri =~ s|&(?!amp;)|&amp;|gis;

    return $uri;
}

sub zs_cache {
    my $c   = shift;

    return $c->stash->{__zs_cache} if ref $c->stash->{__zs_cache} eq 'Zaaksysteem::Cache';

    $c->stash->{__zs_cache_store} = {};

    return Zaaksysteem::Cache->new(storage => $c->stash->{__zs_cache_store});
}

sub query_session {
    my $self = shift;

    return JSON::Path->new(shift)->value($self->session);
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Install>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 check_queue_coworker_changes

TODO: Fix the POD

=cut

=head2 format_error

TODO: Fix the POD

=cut

=head2 push_flash_message

TODO: Fix the POD

=cut

=head2 query_session

TODO: Fix the POD

=cut

=head2 uri_format_ampersands

TODO: Fix the POD

=cut

=head2 zs_cache

TODO: Fix the POD

=cut

