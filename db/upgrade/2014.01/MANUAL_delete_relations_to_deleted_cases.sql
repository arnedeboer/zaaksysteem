BEGIN;

WITH deleted_cases AS (
    SELECT id FROM zaak WHERE deleted IS NOT NULL
)
DELETE FROM case_relation
    WHERE case_id_a IN (SELECT id FROM deleted_cases)
       OR case_id_b IN (SELECT id FROM deleted_cases);

COMMIT;
