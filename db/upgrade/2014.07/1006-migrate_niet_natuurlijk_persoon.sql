BEGIN;

-- Create a row in zaaktypen_betrokkenen with value "niet_natuurlijk_persoon" for every
-- zaaktype_node which has a niet_natuurlijk_persoon_na entry.
--
-- We loose the differentiation between 'binnengemeentelijke' and 'buitengemeentelijke' companies

INSERT INTO
    zaaktype_betrokkenen (zaaktype_node_id, betrokkene_type, created, last_modified)
(
    SELECT
        zaaktype_node_id, 'niet_natuurlijk_persoon', created, last_modified
    FROM
        zaaktype_betrokkenen
    WHERE
        betrokkene_type = 'niet_natuurlijk_persoon_na' and
        zaaktype_node_id NOT IN (
            SELECT
                zaaktype_node_id
            FROM
                zaaktype_betrokkenen
            WHERE betrokkene_type = 'niet_natuurlijk_persoon'
        )
);

COMMIT;
