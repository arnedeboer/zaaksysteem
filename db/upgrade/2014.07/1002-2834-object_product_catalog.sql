BEGIN;

ALTER TABLE object_type_bibliotheek_entry ALTER object_type DROP DEFAULT;
ALTER TABLE object_type_bibliotheek_entry RENAME TO object_bibliotheek_entry;

COMMIT;
