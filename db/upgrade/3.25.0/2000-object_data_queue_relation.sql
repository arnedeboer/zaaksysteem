BEGIN;

    ALTER TABLE queue DROP CONSTRAINT IF EXISTS queue_object_id_fkey;
    ALTER TABLE queue ADD CONSTRAINT queue_object_id_fkey
        FOREIGN KEY (object_id) REFERENCES object_data(uuid)
        ON DELETE CASCADE DEFERRABLE;

COMMIT;
