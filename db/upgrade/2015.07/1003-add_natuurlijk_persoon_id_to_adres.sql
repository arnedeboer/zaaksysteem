BEGIN;

-- Add natuurlijk_persoon_id to address
ALTER TABLE adres ADD COLUMN natuurlijk_persoon_id INTEGER REFERENCES natuurlijk_persoon(id);
ALTER TABLE gm_adres ADD COLUMN natuurlijk_persoon_id INTEGER REFERENCES gm_natuurlijk_persoon(id);

-- Make sure every address in our db defaults to functie_adres = 'W';
UPDATE adres SET functie_adres='W' WHERE functie_adres is null;
UPDATE gm_adres SET functie_adres='W' WHERE functie_adres is null;

ALTER TABLE adres ALTER COLUMN functie_adres SET NOT NULL;
ALTER TABLE gm_adres ALTER COLUMN functie_adres SET NOT NULL;

ALTER TABLE gm_adres ADD COLUMN deleted_on timestamp(6) without time zone;

UPDATE adres SET natuurlijk_persoon_id=sub.id FROM (
    select adres_id,id from natuurlijk_persoon 
) as sub where adres.id=sub.adres_id;

COMMIT;
