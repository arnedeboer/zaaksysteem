BEGIN;

DROP TABLE IF EXISTS interface_local_to_remote;

CREATE TABLE transaction_record_to_object (
    id SERIAL PRIMARY KEY,
    transaction_record_id INTEGER REFERENCES transaction_record(id) NOT NULL,
    local_table VARCHAR(100) NOT NULL,
    local_id VARCHAR(255) NOT NULL,
    mutations TEXT
);

COMMIT;
