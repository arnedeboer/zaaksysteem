BEGIN;

ALTER TABLE transaction_record_to_object ADD COLUMN mutation_type VARCHAR(100);

UPDATE transaction_record_to_object SET mutation_type = 'update';

COMMIT;
