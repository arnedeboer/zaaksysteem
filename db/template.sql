--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: hstore; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS hstore WITH SCHEMA public;


--
-- Name: EXTENSION hstore; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION hstore IS 'data type for storing sets of (key, value) pairs';


--
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


SET search_path = public, pg_catalog;

--
-- Name: confidentiality; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE confidentiality AS ENUM (
    'public',
    'internal',
    'confidential'
);


--
-- Name: contactmoment_medium; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE contactmoment_medium AS ENUM (
    'behandelaar',
    'balie',
    'telefoon',
    'post',
    'email',
    'webformulier'
);


--
-- Name: contactmoment_type; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE contactmoment_type AS ENUM (
    'email',
    'note'
);


--
-- Name: documentstatus; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE documentstatus AS ENUM (
    'original',
    'copy',
    'replaced',
    'converted'
);


--
-- Name: zaaksysteem_bag_types; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE zaaksysteem_bag_types AS ENUM (
    'nummeraanduiding',
    'verblijfsobject',
    'pand',
    'openbareruimte'
);


--
-- Name: zaaksysteem_status; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE zaaksysteem_status AS ENUM (
    'new',
    'open',
    'resolved',
    'stalled',
    'deleted',
    'overdragen'
);


--
-- Name: zaaksysteem_trigger; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE zaaksysteem_trigger AS ENUM (
    'extern',
    'intern'
);


--
-- Name: hstore_to_timestamp(character varying); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION hstore_to_timestamp(date_field character varying) RETURNS timestamp without time zone
    LANGUAGE sql IMMUTABLE
    AS $_$
  SELECT to_timestamp($1, 'YYYY-MM-DD"T"HH24:MI:SS')::TIMESTAMP;
$_$;


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: adres; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE adres (
    id integer NOT NULL,
    straatnaam text,
    huisnummer smallint,
    huisletter character(1),
    huisnummertoevoeging character varying(4),
    nadere_aanduiding character varying(35),
    postcode character varying(6),
    woonplaats text,
    gemeentedeel text,
    functie_adres character(1) NOT NULL,
    datum_aanvang_bewoning date,
    woonplaats_id character varying(32),
    gemeente_code smallint,
    hash character varying(32),
    import_datum timestamp(6) without time zone,
    deleted_on timestamp(6) without time zone,
    adres_buitenland1 text,
    adres_buitenland2 text,
    adres_buitenland3 text,
    landcode integer DEFAULT 6030,
    natuurlijk_persoon_id integer
);


--
-- Name: adres_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE adres_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: adres_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE adres_id_seq OWNED BY adres.id;


--
-- Name: alternative_authentication_activation_link; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE alternative_authentication_activation_link (
    token text NOT NULL,
    subject_id uuid NOT NULL,
    expires timestamp without time zone NOT NULL
);


--
-- Name: bag_ligplaats; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE bag_ligplaats (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14) NOT NULL,
    einddatum character varying(14),
    officieel character varying(1),
    status character varying(80) NOT NULL,
    hoofdadres character varying(16) NOT NULL,
    inonderzoek character varying(1) NOT NULL,
    documentdatum character varying(14),
    documentnummer character varying(20),
    correctie character varying(1),
    id integer NOT NULL
);


--
-- Name: TABLE bag_ligplaats; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE bag_ligplaats IS '55 : een ligplaats is een formeel door de gemeenteraad als zodanig aangewezen plaats in het water, al dan niet aangevuld met een op de oever aanwezig terrein of een gedeelte daarvan, dat bestemd is voor het permanent afmeren van een voor woon-, bedrijfsmatige- of recreatieve doeleinden geschikt vaartuig.';


--
-- Name: COLUMN bag_ligplaats.identificatie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_ligplaats.identificatie IS '58.01 : de unieke aanduiding van een ligplaats.';


--
-- Name: COLUMN bag_ligplaats.begindatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_ligplaats.begindatum IS '58.91 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een ligplaats een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_ligplaats.einddatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_ligplaats.einddatum IS '58.92 : de einddatum van een periode waarin er geen wijzigingen hebben plaatsgevonden in de gegevens die worden bijgehouden over een ligplaats.';


--
-- Name: COLUMN bag_ligplaats.officieel; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_ligplaats.officieel IS '58.02 : een aanduiding waarmee kan worden aangegeven dat een object in de registratie is opgenomen als gevolg van een feitelijke constatering, zonder dat er op het moment van opname sprake is van een formele grondslag voor deze opname.';


--
-- Name: COLUMN bag_ligplaats.status; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_ligplaats.status IS '58.03 : de fase van de levenscyclus van een ligplaats, waarin de betreffende ligplaats zich bevindt.';


--
-- Name: COLUMN bag_ligplaats.hoofdadres; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_ligplaats.hoofdadres IS '58:10 : de identificatiecode nummeraanduiding waaronder het hoofdadres van een ligplaats, dat in het kader van de basis gebouwen registratie als zodanig is aangemerkt, is opgenomen in de basis registratie adressen.';


--
-- Name: COLUMN bag_ligplaats.inonderzoek; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_ligplaats.inonderzoek IS '58.93 : een aanduiding waarmee wordt aangegeven dat een onderzoek wordt uitgevoerd naar de juistheid van een of meerdere gegevens van het betreffende object.';


--
-- Name: COLUMN bag_ligplaats.documentdatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_ligplaats.documentdatum IS '58.97 : de datum waarop het brondocument is vastgesteld, op basis waarvan een opname, mutatie of een in de historie plaatsen van gegevens ten aanzien van een ligplaats heeft plaatsgevonden.';


--
-- Name: COLUMN bag_ligplaats.documentnummer; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_ligplaats.documentnummer IS '58.98 : de unieke aanduiding van het brondocument op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een ligplaats heeft plaatsgevonden, binnen een gemeente.';


--
-- Name: COLUMN bag_ligplaats.correctie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_ligplaats.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: bag_ligplaats_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE bag_ligplaats_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bag_ligplaats_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE bag_ligplaats_id_seq OWNED BY bag_ligplaats.id;


--
-- Name: bag_ligplaats_nevenadres; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE bag_ligplaats_nevenadres (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14) NOT NULL,
    nevenadres character varying(16) NOT NULL,
    correctie character varying(1) NOT NULL
);


--
-- Name: TABLE bag_ligplaats_nevenadres; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE bag_ligplaats_nevenadres IS 'koppeltabel voor nevenadressen bij ligplaats';


--
-- Name: COLUMN bag_ligplaats_nevenadres.identificatie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_ligplaats_nevenadres.identificatie IS '58.01 : de unieke aanduiding van een ligplaats.';


--
-- Name: COLUMN bag_ligplaats_nevenadres.begindatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_ligplaats_nevenadres.begindatum IS '58.91 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een ligplaats een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_ligplaats_nevenadres.nevenadres; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_ligplaats_nevenadres.nevenadres IS '58.11 : de identificatiecodes nummeraanduiding waaronder nevenadressen van een ligplaats, die in het kader van de basis gebouwen registratie als zodanig zijn aangemerkt, zijn opgenomen in de basis registratie adressen.';


--
-- Name: COLUMN bag_ligplaats_nevenadres.correctie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_ligplaats_nevenadres.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: bag_nummeraanduiding; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE bag_nummeraanduiding (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14) NOT NULL,
    einddatum character varying(14),
    huisnummer integer NOT NULL,
    officieel character varying(1),
    huisletter character varying(1),
    huisnummertoevoeging character varying(4),
    postcode character varying(6),
    woonplaats character varying(4),
    inonderzoek character varying(1) NOT NULL,
    openbareruimte character varying(16) NOT NULL,
    type character varying(20) NOT NULL,
    documentdatum character varying(14),
    documentnummer character varying(20),
    status character varying(80) NOT NULL,
    correctie character varying(1),
    id integer NOT NULL,
    gps_lat_lon point
);


--
-- Name: TABLE bag_nummeraanduiding; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE bag_nummeraanduiding IS '11.2 : een nummeraanduiding is een door de gemeenteraad als zodanig toegekende aanduiding van een adresseerbaar object.';


--
-- Name: COLUMN bag_nummeraanduiding.identificatie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_nummeraanduiding.identificatie IS '11.02 : de unieke aanduiding van een nummeraanduiding.';


--
-- Name: COLUMN bag_nummeraanduiding.begindatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_nummeraanduiding.begindatum IS '11.62 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een nummeraanduiding een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_nummeraanduiding.einddatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_nummeraanduiding.einddatum IS '11.63 : de einddatum van een periode waarin er geen wijzigingen hebben plaatsgevonden in de gegevens die worden bijgehouden over een nummeraanduiding.';


--
-- Name: COLUMN bag_nummeraanduiding.huisnummer; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_nummeraanduiding.huisnummer IS '11.20 : een door of namens het gemeentebestuur ten aanzien van een adresseerbaar object toegekende nummering.';


--
-- Name: COLUMN bag_nummeraanduiding.officieel; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_nummeraanduiding.officieel IS '11.21 : een aanduiding waarmee kan worden aangegeven dat een object in de registratie is opgenomen als gevolg van een feitelijke constatering, zonder dat er op het moment van opname sprake is van een formele grondslag voor deze opname.';


--
-- Name: COLUMN bag_nummeraanduiding.huisletter; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_nummeraanduiding.huisletter IS '11.30 : een door of namens het gemeentebestuur ten aanzien van  een adresseerbaar object toegekende toevoeging aan een huisnummer in de vorm van een alfanumeriek teken.';


--
-- Name: COLUMN bag_nummeraanduiding.huisnummertoevoeging; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_nummeraanduiding.huisnummertoevoeging IS '11.40 : een door of namens het gemeentebestuur ten aanzien van  een adresseerbaar object toegekende nadere toevoeging aan een huisnummer of een combinatie van huisnummer en huisletter.';


--
-- Name: COLUMN bag_nummeraanduiding.postcode; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_nummeraanduiding.postcode IS '11.60 : de door tnt post vastgestelde code behorende bij een bepaalde combinatie van een straatnaam en een huisnummer.';


--
-- Name: COLUMN bag_nummeraanduiding.woonplaats; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_nummeraanduiding.woonplaats IS '11.61 : unieke aanduiding van de woonplaats waarbinnen het object waaraan de nummeraanduiding is toegekend is gelegen.';


--
-- Name: COLUMN bag_nummeraanduiding.inonderzoek; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_nummeraanduiding.inonderzoek IS '11.64 : een aanduiding waarmee wordt aangegeven dat een onderzoek wordt uitgevoerd naar de juistheid van een of meerdere gegevens van het betreffende object.';


--
-- Name: COLUMN bag_nummeraanduiding.openbareruimte; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_nummeraanduiding.openbareruimte IS '11.65 : de unieke aanduiding van een openbare ruimte waaraan een adresseerbaar object is gelegen.';


--
-- Name: COLUMN bag_nummeraanduiding.type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_nummeraanduiding.type IS '11.66 : de aard van een als zodanig benoemde nummeraanduiding.';


--
-- Name: COLUMN bag_nummeraanduiding.documentdatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_nummeraanduiding.documentdatum IS '11.67 : de datum waarop het brondocument is vastgesteld, op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een nummeraanduiding heeft plaatsgevonden.';


--
-- Name: COLUMN bag_nummeraanduiding.documentnummer; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_nummeraanduiding.documentnummer IS '11.68 : de unieke aanduiding van het brondocument op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een nummeraanduiding heeft plaatsgevonden, binnen een gemeente.';


--
-- Name: COLUMN bag_nummeraanduiding.status; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_nummeraanduiding.status IS '11.69 : de fase van de levenscyclus van een nummeraanduiding, waarin de betreffende nummeraanduiding zich bevindt.';


--
-- Name: COLUMN bag_nummeraanduiding.correctie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_nummeraanduiding.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: bag_nummeraanduiding_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE bag_nummeraanduiding_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bag_nummeraanduiding_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE bag_nummeraanduiding_id_seq OWNED BY bag_nummeraanduiding.id;


--
-- Name: bag_openbareruimte; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE bag_openbareruimte (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14) NOT NULL,
    einddatum character varying(14),
    naam character varying(80) NOT NULL,
    officieel character varying(1),
    woonplaats character varying(4),
    type character varying(40) NOT NULL,
    inonderzoek character varying(1) NOT NULL,
    documentdatum character varying(14),
    documentnummer character varying(20),
    status character varying(80) NOT NULL,
    correctie character varying(1),
    id integer NOT NULL,
    gps_lat_lon point
);


--
-- Name: TABLE bag_openbareruimte; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE bag_openbareruimte IS '11.1 : een openbare ruimte is een door de gemeenteraad als zodanig aangewezen benaming van een binnen een woonplaats gelegen buitenruimte.';


--
-- Name: COLUMN bag_openbareruimte.identificatie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_openbareruimte.identificatie IS '11.01 : de unieke aanduiding van een openbare ruimte.';


--
-- Name: COLUMN bag_openbareruimte.begindatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_openbareruimte.begindatum IS '11.12 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een openbare ruimte een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_openbareruimte.einddatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_openbareruimte.einddatum IS '11.13 : de einddatum van een periode waarin er geen wijzigingen hebben plaatsgevonden in de gegevens die worden bijgehouden over een openbare ruimte.';


--
-- Name: COLUMN bag_openbareruimte.naam; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_openbareruimte.naam IS '11.10 : een naam die aan een openbare ruimte is toegekend in een daartoe strekkend formeel gemeentelijk besluit.';


--
-- Name: COLUMN bag_openbareruimte.officieel; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_openbareruimte.officieel IS '11.11 : een aanduiding waarmee kan worden aangegeven dat een object in de registratie is opgenomen als gevolg van een feitelijke constatering, zonder dat er op het moment van opname sprake is van een formele grondslag voor deze opname.';


--
-- Name: COLUMN bag_openbareruimte.woonplaats; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_openbareruimte.woonplaats IS '11.15 : unieke aanduiding van de woonplaats waarbinnen een openbare ruimte is gelegen.';


--
-- Name: COLUMN bag_openbareruimte.type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_openbareruimte.type IS '11.16 : de aard van de als zodanig benoemde openbare ruimte.';


--
-- Name: COLUMN bag_openbareruimte.inonderzoek; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_openbareruimte.inonderzoek IS '11.14 : een aanduiding waarmee wordt aangegeven dat een onderzoek wordt uitgevoerd naar de juistheid van een of meerdere gegevens van het betreffende object.';


--
-- Name: COLUMN bag_openbareruimte.documentdatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_openbareruimte.documentdatum IS '11.17 : de datum waarop het brondocument is vastgesteld, op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een openbare ruimte heeft plaatsgevonden.';


--
-- Name: COLUMN bag_openbareruimte.documentnummer; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_openbareruimte.documentnummer IS '11.18 : de unieke aanduiding van het brondocument op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een openbare ruimte heeft plaatsgevonden, binnen een gemeente.';


--
-- Name: COLUMN bag_openbareruimte.status; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_openbareruimte.status IS '11.19 : de fase van de levenscyclus van een openbare ruimte, waarin de betreffende openbare ruimte zich bevindt.';


--
-- Name: COLUMN bag_openbareruimte.correctie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_openbareruimte.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: bag_openbareruimte_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE bag_openbareruimte_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bag_openbareruimte_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE bag_openbareruimte_id_seq OWNED BY bag_openbareruimte.id;


--
-- Name: bag_pand; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE bag_pand (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14) NOT NULL,
    einddatum character varying(14),
    officieel character varying(1),
    bouwjaar integer,
    status character varying(80) NOT NULL,
    inonderzoek character varying(1) NOT NULL,
    documentdatum character varying(14),
    documentnummer character varying(20),
    correctie character varying(1),
    id integer NOT NULL
);


--
-- Name: TABLE bag_pand; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE bag_pand IS '55 : een pand is de kleinste, bij de totstandkoming functioneel en bouwkundig constructief zelfstandige eenheid, die direct en duurzaam met de aarde is verbonden.';


--
-- Name: COLUMN bag_pand.identificatie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_pand.identificatie IS '55.01 : de unieke aanduiding van een pand';


--
-- Name: COLUMN bag_pand.begindatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_pand.begindatum IS '55.91 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een pand een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_pand.einddatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_pand.einddatum IS '55.92 : de einddatum van een periode waarin er geen wijzigingen hebben plaatsgevonden in de gegevens die worden bijgehouden over een pand.';


--
-- Name: COLUMN bag_pand.officieel; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_pand.officieel IS '55.02 : een aanduiding waarmee kan worden aangegeven dat een object in de registratie is opgenomen als gevolg van een feitelijke constatering, zonder dat er op het moment van opname sprake is van een formele grondslag voor deze opname';


--
-- Name: COLUMN bag_pand.bouwjaar; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_pand.bouwjaar IS '55.30 : de aanduiding van het jaar waarin een pand oorspronkelijk als bouwkundig gereed is opgeleverd.';


--
-- Name: COLUMN bag_pand.status; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_pand.status IS '55.31 : de fase van de levenscyclus van een pand, waarin het betreffende pand zich bevindt.';


--
-- Name: COLUMN bag_pand.inonderzoek; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_pand.inonderzoek IS '55.93 : een aanduiding waarmee wordt aangegeven dat een onderzoek wordt uitgevoerd naar de juistheid van een of meerdere gegevens van het betreffende object.';


--
-- Name: COLUMN bag_pand.documentdatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_pand.documentdatum IS '55.97 : de datum waarop het brondocument is vastgesteld, op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een pand heeft plaatsgevonden.';


--
-- Name: COLUMN bag_pand.documentnummer; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_pand.documentnummer IS '55.98 : de unieke aanduiding van het brondocument op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een pand heeft plaatsgevonden, binnen een gemeente.';


--
-- Name: COLUMN bag_pand.correctie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_pand.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: bag_pand_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE bag_pand_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bag_pand_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE bag_pand_id_seq OWNED BY bag_pand.id;


--
-- Name: bag_standplaats; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE bag_standplaats (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14) NOT NULL,
    einddatum character varying(14),
    officieel character varying(1),
    status character varying(80) NOT NULL,
    hoofdadres character varying(16) NOT NULL,
    inonderzoek character varying(1) NOT NULL,
    documentdatum character varying(14),
    documentnummer character varying(20),
    correctie character varying(1),
    id integer NOT NULL
);


--
-- Name: TABLE bag_standplaats; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE bag_standplaats IS '57 : een standplaats is een formeel door de gemeenteraad als zodanig aangewezen terrein of een gedeelte daarvan, dat bestemd is voor het permanent plaatsen van een niet direct en duurzaam met de aarde verbonden en voor woon -, bedrijfsmatige - of recreatieve doeleinden geschikte ruimte.';


--
-- Name: COLUMN bag_standplaats.identificatie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_standplaats.identificatie IS '57.01 : de unieke aanduiding van een standplaats.';


--
-- Name: COLUMN bag_standplaats.begindatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_standplaats.begindatum IS '57.91 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een standplaats een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_standplaats.einddatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_standplaats.einddatum IS '57.92 : de einddatum van een periode waarin er geen wijzigingen hebben plaatsgevonden in de gegevens die worden bijgehouden over een standplaats.';


--
-- Name: COLUMN bag_standplaats.officieel; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_standplaats.officieel IS '57.02 : een aanduiding waarmee kan worden aangegeven dat een object in de registratie is opgenomen als gevolg van een feitelijke constatering, zonder dat er op het moment van opname sprake is van een formele grondslag voor deze opname.';


--
-- Name: COLUMN bag_standplaats.status; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_standplaats.status IS '57.03 : de fase van de levenscyclus van een standplaats, waarin de betreffende standplaats zich bevindt.';


--
-- Name: COLUMN bag_standplaats.hoofdadres; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_standplaats.hoofdadres IS '57:10 : de identificatiecode nummeraanduiding waaronder het hoofdadres van een standplaats, dat in het kader van de basis gebouwen registratie als zodanig is aangemerkt, is opgenomen in de basis registratie adressen.';


--
-- Name: COLUMN bag_standplaats.inonderzoek; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_standplaats.inonderzoek IS '57.93 : een aanduiding waarmee wordt aangegeven dat een onderzoek wordt uitgevoerd naar de juistheid van een of meerdere gegevens van het betreffende object.';


--
-- Name: COLUMN bag_standplaats.documentdatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_standplaats.documentdatum IS '57.97 : de datum waarop het brondocument is vastgesteld, op basis waarvan een opname, mutatie of een in de historie plaatsen van gegevens ten aanzien van een standplaats heeft plaatsgevonden.';


--
-- Name: COLUMN bag_standplaats.documentnummer; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_standplaats.documentnummer IS '57.98 : de unieke aanduiding van het brondocument op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een standplaats heeft plaatsgevonden, binnen een gemeente.';


--
-- Name: COLUMN bag_standplaats.correctie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_standplaats.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: bag_standplaats_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE bag_standplaats_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bag_standplaats_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE bag_standplaats_id_seq OWNED BY bag_standplaats.id;


--
-- Name: bag_verblijfsobject; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE bag_verblijfsobject (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14) NOT NULL,
    einddatum character varying(14),
    officieel character varying(1),
    hoofdadres character varying(16) NOT NULL,
    oppervlakte integer,
    status character varying(80) NOT NULL,
    inonderzoek character varying(1) NOT NULL,
    documentdatum character varying(14),
    documentnummer character varying(20),
    correctie character varying(1),
    id integer NOT NULL
);


--
-- Name: TABLE bag_verblijfsobject; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE bag_verblijfsobject IS '56 : een verblijfsobject is de kleinste binnen een of meerdere panden gelegen en voor woon -, bedrijfsmatige - of recreatieve doeleinden geschikte eenheid van gebruik, die ontsloten wordt via een eigen toegang vanaf de openbare weg, een erf of een gedeelde verkeersruimte en die onderwerp kan zijn van rechtshandelingen.';


--
-- Name: COLUMN bag_verblijfsobject.identificatie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_verblijfsobject.identificatie IS '56.01 : de unieke aanduiding van een verblijfsobject';


--
-- Name: COLUMN bag_verblijfsobject.begindatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_verblijfsobject.begindatum IS '56.91 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een verblijfsobject een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_verblijfsobject.einddatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_verblijfsobject.einddatum IS '56.92 : de einddatum van een periode waarin er geen wijzigingen hebben plaatsgevonden in de gegevens die worden bijgehouden over een verblijfsobject.';


--
-- Name: COLUMN bag_verblijfsobject.officieel; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_verblijfsobject.officieel IS '56.02 : een aanduiding waarmee kan worden aangegeven dat een object in de registratie is opgenomen als gevolg van een feitelijke constatering, zonder dat er op het moment van opname sprake is van een formele grondslag voor deze opname';


--
-- Name: COLUMN bag_verblijfsobject.hoofdadres; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_verblijfsobject.hoofdadres IS '56:10 : de identificatiecode nummeraanduiding waaronder het hoofdadres van een verblijfsobject, dat in het kader van de basis gebouwen registratie als zodanig is aangemerkt, is opgenomen in de basis registratie adressen.';


--
-- Name: COLUMN bag_verblijfsobject.oppervlakte; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_verblijfsobject.oppervlakte IS '56.31 : de gebruiksoppervlakte van een verblijfsobject in gehele vierkante meters.';


--
-- Name: COLUMN bag_verblijfsobject.status; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_verblijfsobject.status IS '56.32 : de fase van de levenscyclus van een verblijfsobject, waarin het betreffende verblijfsobject zich bevindt.';


--
-- Name: COLUMN bag_verblijfsobject.inonderzoek; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_verblijfsobject.inonderzoek IS '56.93 : een aanduiding waarmee wordt aangegeven dat een onderzoek wordt uitgevoerd naar de juistheid van een of meerdere gegevens van het betreffende object.';


--
-- Name: COLUMN bag_verblijfsobject.documentdatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_verblijfsobject.documentdatum IS '56.97 : de datum waarop het brondocument is vastgesteld, op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een verblijfsobject heeft plaatsgevonden.';


--
-- Name: COLUMN bag_verblijfsobject.documentnummer; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_verblijfsobject.documentnummer IS '56.98 : de unieke aanduiding van het brondocument op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een verblijfsobject heeft plaatsgevonden, binnen een gemeente.';


--
-- Name: COLUMN bag_verblijfsobject.correctie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_verblijfsobject.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: bag_verblijfsobject_gebruiksdoel; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE bag_verblijfsobject_gebruiksdoel (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14),
    gebruiksdoel character varying(80) NOT NULL,
    correctie character varying(1),
    id integer NOT NULL
);


--
-- Name: TABLE bag_verblijfsobject_gebruiksdoel; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE bag_verblijfsobject_gebruiksdoel IS 'koppeltabel voor gebruiksdoelen bij verblijfsobject';


--
-- Name: COLUMN bag_verblijfsobject_gebruiksdoel.identificatie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_verblijfsobject_gebruiksdoel.identificatie IS '56.01 : de unieke aanduiding van een verblijfsobject';


--
-- Name: COLUMN bag_verblijfsobject_gebruiksdoel.begindatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_verblijfsobject_gebruiksdoel.begindatum IS '56.91 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een verblijfsobject een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_verblijfsobject_gebruiksdoel.gebruiksdoel; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_verblijfsobject_gebruiksdoel.gebruiksdoel IS '56.30 : een categorisering van de gebruiksdoelen van het betreffende verblijfsobject, zoals dit  formeel door de overheid als zodanig is toegestaan.';


--
-- Name: COLUMN bag_verblijfsobject_gebruiksdoel.correctie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_verblijfsobject_gebruiksdoel.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: bag_verblijfsobject_gebruiksdoel_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE bag_verblijfsobject_gebruiksdoel_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bag_verblijfsobject_gebruiksdoel_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE bag_verblijfsobject_gebruiksdoel_id_seq OWNED BY bag_verblijfsobject_gebruiksdoel.id;


--
-- Name: bag_verblijfsobject_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE bag_verblijfsobject_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bag_verblijfsobject_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE bag_verblijfsobject_id_seq OWNED BY bag_verblijfsobject.id;


--
-- Name: bag_verblijfsobject_pand; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE bag_verblijfsobject_pand (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14),
    pand character varying(16) NOT NULL,
    correctie character varying(1),
    id integer NOT NULL
);


--
-- Name: TABLE bag_verblijfsobject_pand; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE bag_verblijfsobject_pand IS 'koppeltabel voor panden bij verblijfsobject';


--
-- Name: COLUMN bag_verblijfsobject_pand.identificatie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_verblijfsobject_pand.identificatie IS '56.01 : de unieke aanduiding van een verblijfsobject';


--
-- Name: COLUMN bag_verblijfsobject_pand.begindatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_verblijfsobject_pand.begindatum IS '56.91 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een verblijfsobject een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_verblijfsobject_pand.pand; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_verblijfsobject_pand.pand IS '56.90 : de unieke aanduidingen van de panden waarvan het verblijfsobject onderdeel uitmaakt.';


--
-- Name: COLUMN bag_verblijfsobject_pand.correctie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_verblijfsobject_pand.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: bag_verblijfsobject_pand_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE bag_verblijfsobject_pand_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bag_verblijfsobject_pand_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE bag_verblijfsobject_pand_id_seq OWNED BY bag_verblijfsobject_pand.id;


--
-- Name: bag_woonplaats; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE bag_woonplaats (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14) NOT NULL,
    einddatum character varying(14),
    officieel character varying(1),
    naam character varying(80) NOT NULL,
    status character varying(80) NOT NULL,
    inonderzoek character varying(1) NOT NULL,
    documentdatum character varying(14),
    documentnummer character varying(20),
    correctie character varying(1),
    id integer NOT NULL
);


--
-- Name: TABLE bag_woonplaats; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE bag_woonplaats IS '11.7 : een woonplaats is een door de gemeenteraad als zodanig aangewezen gedeelte van het gemeentelijk grondgebied.';


--
-- Name: COLUMN bag_woonplaats.identificatie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_woonplaats.identificatie IS '11.03 : de landelijk unieke aanduiding van een woonplaats, zoals vastgesteld door de beheerder van de landelijke tabel voor woonplaatsen.';


--
-- Name: COLUMN bag_woonplaats.begindatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_woonplaats.begindatum IS '11.73 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een woonplaats een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_woonplaats.einddatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_woonplaats.einddatum IS '11.74 : de einddatum van een periode waarin er geen wijzigingen hebben plaatsgevonden in de gegevens die worden bijgehouden over een woonplaats.';


--
-- Name: COLUMN bag_woonplaats.officieel; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_woonplaats.officieel IS '11.72 : een aanduiding waarmee kan worden aangegeven dat een object in de registratie is opgenomen als gevolg van een feitelijke constatering, zonder dat er op het moment van opname sprake is van een formele grondslag voor deze opname.';


--
-- Name: COLUMN bag_woonplaats.naam; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_woonplaats.naam IS '11.70 : de benaming van een door het gemeentebestuur aangewezen woonplaats.';


--
-- Name: COLUMN bag_woonplaats.status; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_woonplaats.status IS '11.79 : de fase van de levenscyclus van een woonplaats, waarin de betreffende woonplaats zich bevindt.';


--
-- Name: COLUMN bag_woonplaats.inonderzoek; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_woonplaats.inonderzoek IS '11.75 : een aanduiding waarmee wordt aangegeven dat een onderzoek wordt uitgevoerd naar de juistheid van een of meerdere gegevens van het betreffende object.';


--
-- Name: COLUMN bag_woonplaats.documentdatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_woonplaats.documentdatum IS '11.77 : de datum waarop het brondocument is vastgesteld, op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een woonplaats heeft plaatsgevonden.';


--
-- Name: COLUMN bag_woonplaats.documentnummer; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_woonplaats.documentnummer IS '11.78 : de unieke aanduiding van het brondocument op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een woonplaats heeft plaatsgevonden, binnen een gemeente.';


--
-- Name: COLUMN bag_woonplaats.correctie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN bag_woonplaats.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: bag_woonplaats_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE bag_woonplaats_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bag_woonplaats_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE bag_woonplaats_id_seq OWNED BY bag_woonplaats.id;


--
-- Name: searchable; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE searchable (
    search_index tsvector,
    search_term text,
    object_type text,
    searchable_id integer NOT NULL,
    search_order text
);


--
-- Name: bedrijf; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE bedrijf (
    search_index tsvector,
    search_term text,
    object_type text DEFAULT 'bedrijf'::character varying,
    id integer NOT NULL,
    dossiernummer character varying(8),
    subdossiernummer character varying(4),
    hoofdvestiging_dossiernummer character varying(8),
    hoofdvestiging_subdossiernummer character varying(4),
    vorig_dossiernummer character varying(8),
    vorig_subdossiernummer character varying(4),
    handelsnaam text,
    rechtsvorm smallint,
    kamernummer smallint,
    faillisement smallint,
    surseance smallint,
    telefoonnummer character varying(10),
    email character varying(128),
    vestiging_adres text,
    vestiging_straatnaam text,
    vestiging_huisnummer character varying(6),
    vestiging_huisnummertoevoeging character varying(12),
    vestiging_postcodewoonplaats text,
    vestiging_postcode character varying(6),
    vestiging_woonplaats text,
    correspondentie_adres text,
    correspondentie_straatnaam text,
    correspondentie_huisnummer character varying(6),
    correspondentie_huisnummertoevoeging character varying(12),
    correspondentie_postcodewoonplaats text,
    correspondentie_postcode character varying(6),
    correspondentie_woonplaats text,
    hoofdactiviteitencode integer,
    nevenactiviteitencode1 integer,
    nevenactiviteitencode2 integer,
    werkzamepersonen integer,
    contact_naam character varying(64),
    contact_aanspreektitel character varying(45),
    contact_voorletters character varying(19),
    contact_voorvoegsel character varying(8),
    contact_geslachtsnaam character varying(95),
    contact_geslachtsaanduiding character varying(1),
    authenticated smallint,
    authenticatedby text,
    fulldossiernummer text,
    import_datum timestamp(6) without time zone,
    deleted_on timestamp(6) without time zone,
    verblijfsobject_id character varying(16),
    system_of_record character varying(32),
    system_of_record_id bigint,
    vestigingsnummer bigint,
    vestiging_huisletter text,
    correspondentie_huisletter text,
    vestiging_adres_buitenland1 text,
    vestiging_adres_buitenland2 text,
    vestiging_adres_buitenland3 text,
    vestiging_landcode integer DEFAULT 6030,
    correspondentie_adres_buitenland1 text,
    correspondentie_adres_buitenland2 text,
    correspondentie_adres_buitenland3 text,
    correspondentie_landcode integer DEFAULT 6030,
    uuid uuid DEFAULT uuid_generate_v4()
)
INHERITS (searchable);


--
-- Name: bedrijf_authenticatie; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE bedrijf_authenticatie (
    id integer NOT NULL,
    gegevens_magazijn_id integer,
    login integer,
    password character varying(255),
    created timestamp without time zone,
    last_modified timestamp without time zone
);


--
-- Name: bedrijf_authenticatie_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE bedrijf_authenticatie_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bedrijf_authenticatie_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE bedrijf_authenticatie_id_seq OWNED BY bedrijf_authenticatie.id;


--
-- Name: bedrijf_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE bedrijf_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bedrijf_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE bedrijf_id_seq OWNED BY bedrijf.id;


--
-- Name: beheer_import; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE beheer_import (
    id integer NOT NULL,
    importtype character varying(256),
    succesvol integer,
    finished timestamp without time zone,
    import_create integer,
    import_update integer,
    error integer,
    error_message text,
    entries integer,
    created timestamp without time zone,
    last_modified timestamp without time zone
);


--
-- Name: beheer_import_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE beheer_import_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: beheer_import_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE beheer_import_id_seq OWNED BY beheer_import.id;


--
-- Name: beheer_import_log; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE beheer_import_log (
    id integer NOT NULL,
    import_id integer,
    old_data text,
    new_data text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    kolom text,
    identifier text,
    action character varying(255)
);


--
-- Name: beheer_import_log_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE beheer_import_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: beheer_import_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE beheer_import_log_id_seq OWNED BY beheer_import_log.id;


--
-- Name: betrokkene_notes; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE betrokkene_notes (
    id integer NOT NULL,
    betrokkene_exid integer,
    betrokkene_type text,
    betrokkene_from text,
    ntype text,
    subject text,
    message text,
    created timestamp without time zone,
    last_modified timestamp without time zone
);


--
-- Name: betrokkene_notes_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE betrokkene_notes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: betrokkene_notes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE betrokkene_notes_id_seq OWNED BY betrokkene_notes.id;


--
-- Name: betrokkenen; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE betrokkenen (
    id integer NOT NULL,
    btype integer,
    gm_natuurlijk_persoon_id integer,
    naam text
);


--
-- Name: betrokkenen_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE betrokkenen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: betrokkenen_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE betrokkenen_id_seq OWNED BY betrokkenen.id;


--
-- Name: bibliotheek_categorie; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE bibliotheek_categorie (
    search_index tsvector,
    search_term text,
    object_type text DEFAULT 'bibliotheek_categorie'::character varying,
    id integer NOT NULL,
    naam character varying(256),
    label text,
    description text,
    help text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    system integer,
    pid integer
)
INHERITS (searchable);


--
-- Name: bibliotheek_categorie_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE bibliotheek_categorie_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bibliotheek_categorie_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE bibliotheek_categorie_id_seq OWNED BY bibliotheek_categorie.id;


--
-- Name: bibliotheek_kenmerken; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE bibliotheek_kenmerken (
    search_index tsvector,
    search_term text,
    object_type text DEFAULT 'bibliotheek_kenmerken'::character varying,
    id integer NOT NULL,
    naam character varying(256),
    value_type text,
    value_default text,
    label text,
    description text,
    help text,
    magic_string text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    bibliotheek_categorie_id integer,
    document_categorie text,
    system integer,
    type_multiple integer,
    deleted timestamp without time zone,
    file_metadata_id integer,
    version integer DEFAULT 1 NOT NULL,
    properties text DEFAULT '{}'::text,
    naam_public text,
    CONSTRAINT bibliotheek_kenmerken_value_type_check CHECK ((value_type = ANY (ARRAY['text_uc'::text, 'checkbox'::text, 'richtext'::text, 'date'::text, 'file'::text, 'bag_straat_adres'::text, 'email'::text, 'valutaex'::text, 'bag_openbareruimte'::text, 'text'::text, 'bag_openbareruimtes'::text, 'url'::text, 'valuta'::text, 'option'::text, 'bag_adres'::text, 'select'::text, 'valutain6'::text, 'valutaex6'::text, 'valutaex21'::text, 'image_from_url'::text, 'bag_adressen'::text, 'valutain'::text, 'calendar'::text, 'calendar_supersaas'::text, 'bag_straat_adressen'::text, 'googlemaps'::text, 'numeric'::text, 'valutain21'::text, 'textarea'::text, 'bankaccount'::text, 'subject'::text, 'geolatlon'::text])))
)
INHERITS (searchable);


--
-- Name: bibliotheek_kenmerken_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE bibliotheek_kenmerken_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bibliotheek_kenmerken_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE bibliotheek_kenmerken_id_seq OWNED BY bibliotheek_kenmerken.id;


--
-- Name: bibliotheek_kenmerken_values; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE bibliotheek_kenmerken_values (
    id integer NOT NULL,
    bibliotheek_kenmerken_id integer,
    value text,
    active boolean DEFAULT true NOT NULL,
    sort_order integer NOT NULL
);


--
-- Name: bibliotheek_kenmerken_values_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE bibliotheek_kenmerken_values_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bibliotheek_kenmerken_values_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE bibliotheek_kenmerken_values_id_seq OWNED BY bibliotheek_kenmerken_values.id;


--
-- Name: bibliotheek_kenmerken_values_sort_order_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE bibliotheek_kenmerken_values_sort_order_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bibliotheek_kenmerken_values_sort_order_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE bibliotheek_kenmerken_values_sort_order_seq OWNED BY bibliotheek_kenmerken_values.sort_order;


--
-- Name: bibliotheek_notificatie_kenmerk; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE bibliotheek_notificatie_kenmerk (
    id integer NOT NULL,
    bibliotheek_notificatie_id integer NOT NULL,
    bibliotheek_kenmerken_id integer NOT NULL
);


--
-- Name: bibliotheek_notificatie_kenmerk_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE bibliotheek_notificatie_kenmerk_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bibliotheek_notificatie_kenmerk_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE bibliotheek_notificatie_kenmerk_id_seq OWNED BY bibliotheek_notificatie_kenmerk.id;


--
-- Name: bibliotheek_notificaties; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE bibliotheek_notificaties (
    search_index tsvector,
    search_term text,
    object_type text DEFAULT 'bibliotheek_notificaties'::character varying,
    id integer NOT NULL,
    bibliotheek_categorie_id integer,
    label text,
    subject text,
    message text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    deleted timestamp without time zone,
    sender text,
    sender_address text
)
INHERITS (searchable);


--
-- Name: bibliotheek_notificaties_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE bibliotheek_notificaties_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bibliotheek_notificaties_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE bibliotheek_notificaties_id_seq OWNED BY bibliotheek_notificaties.id;


--
-- Name: bibliotheek_sjablonen; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE bibliotheek_sjablonen (
    search_index tsvector,
    search_term text,
    object_type text DEFAULT 'bibliotheek_sjablonen'::character varying,
    id integer NOT NULL,
    bibliotheek_categorie_id integer,
    naam character varying(256),
    label text,
    description text,
    help text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    filestore_id integer,
    deleted timestamp without time zone,
    interface_id integer,
    template_external_name text
)
INHERITS (searchable);


--
-- Name: bibliotheek_sjablonen_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE bibliotheek_sjablonen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bibliotheek_sjablonen_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE bibliotheek_sjablonen_id_seq OWNED BY bibliotheek_sjablonen.id;


--
-- Name: bibliotheek_sjablonen_magic_string; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE bibliotheek_sjablonen_magic_string (
    id integer NOT NULL,
    bibliotheek_sjablonen_id integer,
    value text
);


--
-- Name: bibliotheek_sjablonen_magic_string_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE bibliotheek_sjablonen_magic_string_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bibliotheek_sjablonen_magic_string_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE bibliotheek_sjablonen_magic_string_id_seq OWNED BY bibliotheek_sjablonen_magic_string.id;


--
-- Name: case_action; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE case_action (
    id integer NOT NULL,
    case_id integer NOT NULL,
    casetype_status_id integer,
    type character varying(64),
    label character varying(255),
    automatic boolean,
    data text,
    state_tainted boolean DEFAULT false,
    data_tainted boolean DEFAULT false
);


--
-- Name: case_action_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE case_action_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: case_action_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE case_action_id_seq OWNED BY case_action.id;


--
-- Name: case_relation; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE case_relation (
    id integer NOT NULL,
    case_id_a integer,
    case_id_b integer,
    order_seq_a integer,
    order_seq_b integer,
    type_a character varying(64),
    type_b character varying(64),
    uuid uuid DEFAULT uuid_generate_v4()
);


--
-- Name: case_relation_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE case_relation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: case_relation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE case_relation_id_seq OWNED BY case_relation.id;


--
-- Name: checklist; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE checklist (
    id integer NOT NULL,
    case_id integer NOT NULL,
    case_milestone integer
);


--
-- Name: checklist_antwoord_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE checklist_antwoord_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: checklist_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE checklist_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: checklist_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE checklist_id_seq OWNED BY checklist.id;


--
-- Name: checklist_item; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE checklist_item (
    id integer NOT NULL,
    checklist_id integer NOT NULL,
    label text,
    state boolean DEFAULT false NOT NULL,
    sequence integer,
    user_defined boolean DEFAULT true NOT NULL,
    deprecated_answer character varying(8)
);


--
-- Name: checklist_item_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE checklist_item_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: checklist_item_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE checklist_item_id_seq OWNED BY checklist_item.id;


--
-- Name: config; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE config (
    id integer NOT NULL,
    parameter character varying(128),
    value text,
    advanced boolean DEFAULT true NOT NULL
);


--
-- Name: config_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE config_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: config_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE config_id_seq OWNED BY config.id;


--
-- Name: contact_data; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE contact_data (
    id integer NOT NULL,
    gegevens_magazijn_id integer,
    betrokkene_type integer,
    mobiel character varying(255),
    telefoonnummer character varying(255),
    email character varying(255),
    created timestamp without time zone,
    last_modified timestamp without time zone,
    note text
);


--
-- Name: contact_data_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE contact_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: contact_data_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE contact_data_id_seq OWNED BY contact_data.id;


--
-- Name: contactmoment; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE contactmoment (
    id integer NOT NULL,
    subject_id character varying(100),
    case_id integer,
    type contactmoment_type NOT NULL,
    medium contactmoment_medium NOT NULL,
    date_created timestamp without time zone DEFAULT now() NOT NULL,
    created_by text NOT NULL,
    uuid uuid DEFAULT uuid_generate_v4() NOT NULL
);


--
-- Name: contactmoment_email; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE contactmoment_email (
    id integer NOT NULL,
    filestore_id integer NOT NULL,
    contactmoment_id integer NOT NULL,
    body text NOT NULL,
    subject text NOT NULL,
    recipient text NOT NULL,
    cc text,
    bcc text
);


--
-- Name: contactmoment_email_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE contactmoment_email_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: contactmoment_email_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE contactmoment_email_id_seq OWNED BY contactmoment_email.id;


--
-- Name: contactmoment_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE contactmoment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: contactmoment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE contactmoment_id_seq OWNED BY contactmoment.id;


--
-- Name: contactmoment_note; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE contactmoment_note (
    id integer NOT NULL,
    message character varying,
    contactmoment_id integer NOT NULL
);


--
-- Name: contactmoment_note_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE contactmoment_note_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: contactmoment_note_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE contactmoment_note_id_seq OWNED BY contactmoment_note.id;


--
-- Name: directory; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE directory (
    id integer NOT NULL,
    name text NOT NULL,
    case_id integer NOT NULL,
    original_name text NOT NULL,
    path integer[] DEFAULT ARRAY[]::integer[] NOT NULL
);


--
-- Name: directory_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE directory_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: directory_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE directory_id_seq OWNED BY directory.id;


--
-- Name: searchable_searchable_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE searchable_searchable_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: searchable_searchable_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE searchable_searchable_id_seq OWNED BY searchable.searchable_id;


--
-- Name: file; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE file (
    search_index tsvector,
    search_term text,
    object_type text DEFAULT 'file'::character varying,
    searchable_id integer DEFAULT nextval('searchable_searchable_id_seq'::regclass),
    id integer NOT NULL,
    filestore_id integer NOT NULL,
    name character varying(250) NOT NULL,
    extension character varying(10) NOT NULL,
    root_file_id integer,
    version integer DEFAULT 1,
    case_id integer,
    metadata_id integer,
    subject_id character varying(100),
    directory_id integer,
    creation_reason text NOT NULL,
    accepted boolean DEFAULT false NOT NULL,
    rejection_reason text,
    reject_to_queue boolean DEFAULT false,
    is_duplicate_name boolean DEFAULT false NOT NULL,
    publish_pip boolean DEFAULT false NOT NULL,
    publish_website boolean DEFAULT false NOT NULL,
    date_created timestamp without time zone DEFAULT now() NOT NULL,
    created_by character varying(100) NOT NULL,
    date_modified timestamp without time zone,
    modified_by character varying(100),
    date_deleted timestamp without time zone,
    deleted_by character varying(100),
    destroyed boolean DEFAULT false,
    scheduled_jobs_id integer,
    intake_owner character varying,
    active_version boolean DEFAULT false NOT NULL,
    is_duplicate_of integer,
    queue boolean DEFAULT true NOT NULL,
    document_status documentstatus DEFAULT 'original'::documentstatus NOT NULL,
    generator text,
    lock_timestamp timestamp without time zone,
    lock_subject_id uuid,
    lock_subject_name text,
    CONSTRAINT lock_fields_check CHECK (((lock_timestamp IS NULL) OR ((lock_subject_id IS NOT NULL) AND (lock_subject_name IS NOT NULL))))
)
INHERITS (searchable);


--
-- Name: file_annotation; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE file_annotation (
    id uuid NOT NULL,
    file_id integer NOT NULL,
    subject character varying(255) NOT NULL,
    properties text DEFAULT '{}'::text NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone
);


--
-- Name: file_case_document; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE file_case_document (
    id integer NOT NULL,
    file_id integer NOT NULL,
    case_document_id integer NOT NULL
);


--
-- Name: file_case_document_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE file_case_document_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: file_case_document_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE file_case_document_id_seq OWNED BY file_case_document.id;


--
-- Name: file_derivative; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE file_derivative (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    file_id integer NOT NULL,
    filestore_id integer NOT NULL,
    max_width integer NOT NULL,
    max_height integer NOT NULL,
    date_generated timestamp without time zone DEFAULT now() NOT NULL,
    type text NOT NULL,
    CONSTRAINT file_thumbnail_type_check CHECK ((type = ANY (ARRAY['thumbnail'::text, 'pdf'::text, 'doc'::text])))
);


--
-- Name: file_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE file_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: file_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE file_id_seq OWNED BY file.id;


--
-- Name: file_metadata; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE file_metadata (
    id integer NOT NULL,
    description text,
    trust_level character varying(100) DEFAULT 'Zaakvertrouwelijk'::character varying NOT NULL,
    origin character varying(100),
    document_category character varying(150),
    origin_date date,
    pronom_format text,
    appearance text,
    structure text,
    CONSTRAINT file_metadata_origin_check CHECK (((origin)::text ~ '(Inkomend|Uitgaand|Intern)'::text)),
    CONSTRAINT file_metadata_trust_level_check CHECK (((trust_level)::text ~ '(Openbaar|Beperkt openbaar|Intern|Zaakvertrouwelijk|Vertrouwelijk|onfidentieel|Geheim|Zeer geheim)'::text))
);


--
-- Name: file_metadata_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE file_metadata_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: file_metadata_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE file_metadata_id_seq OWNED BY file_metadata.id;


--
-- Name: filestore; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE filestore (
    id integer NOT NULL,
    uuid uuid NOT NULL,
    thumbnail_uuid uuid,
    original_name text NOT NULL,
    size integer NOT NULL,
    mimetype character varying(160) NOT NULL,
    md5 character varying(100) NOT NULL,
    date_created timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: filestore_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE filestore_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: filestore_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE filestore_id_seq OWNED BY filestore.id;


--
-- Name: gegevensmagazijn_subjecten; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE gegevensmagazijn_subjecten (
    subject_uuid uuid NOT NULL,
    np_uuid uuid,
    nnp_uuid uuid,
    CONSTRAINT gegevensmagazijn_subjecten_xor_uuid CHECK (((np_uuid IS NULL) <> (nnp_uuid IS NULL)))
);


--
-- Name: gm_adres; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE gm_adres (
    id integer NOT NULL,
    straatnaam character varying(80),
    huisnummer smallint,
    huisletter character(1),
    huisnummertoevoeging character varying(4),
    nadere_aanduiding character varying(35),
    postcode character varying(6),
    woonplaats character varying(75),
    gemeentedeel character varying(75),
    functie_adres character(1) NOT NULL,
    datum_aanvang_bewoning date,
    woonplaats_id character varying(32),
    gemeente_code smallint,
    hash character varying(32),
    import_datum timestamp(6) without time zone,
    adres_buitenland1 text,
    adres_buitenland2 text,
    adres_buitenland3 text,
    landcode integer DEFAULT 6030,
    natuurlijk_persoon_id integer,
    deleted_on timestamp(6) without time zone
);


--
-- Name: gm_adres_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE gm_adres_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: gm_adres_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE gm_adres_id_seq OWNED BY gm_adres.id;


--
-- Name: gm_bedrijf; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE gm_bedrijf (
    id integer NOT NULL,
    gegevens_magazijn_id integer,
    dossiernummer character varying(8),
    subdossiernummer character varying(4),
    hoofdvestiging_dossiernummer character varying(8),
    hoofdvestiging_subdossiernummer character varying(4),
    vorig_dossiernummer character varying(8),
    vorig_subdossiernummer character varying(4),
    handelsnaam text,
    rechtsvorm smallint,
    kamernummer smallint,
    faillisement smallint,
    surseance smallint,
    telefoonnummer character varying(15),
    email character varying(128),
    vestiging_adres text,
    vestiging_straatnaam text,
    vestiging_huisnummer character varying(6),
    vestiging_huisnummertoevoeging character varying(12),
    vestiging_postcodewoonplaats text,
    vestiging_postcode character varying(6),
    vestiging_woonplaats text,
    correspondentie_adres text,
    correspondentie_straatnaam text,
    correspondentie_huisnummer character varying(6),
    correspondentie_huisnummertoevoeging character varying(12),
    correspondentie_postcodewoonplaats text,
    correspondentie_postcode character varying(6),
    correspondentie_woonplaats text,
    hoofdactiviteitencode integer,
    nevenactiviteitencode1 integer,
    nevenactiviteitencode2 integer,
    werkzamepersonen integer,
    contact_naam character varying(64),
    contact_aanspreektitel character varying(45),
    contact_voorletters character varying(19),
    contact_voorvoegsel character varying(8),
    contact_geslachtsnaam character varying(95),
    contact_geslachtsaanduiding character varying(1),
    authenticated smallint,
    authenticatedby text,
    import_datum timestamp(6) without time zone,
    verblijfsobject_id character varying(16),
    vestigingsnummer bigint,
    vestiging_huisletter text,
    correspondentie_huisletter text,
    vestiging_adres_buitenland1 text,
    vestiging_adres_buitenland2 text,
    vestiging_adres_buitenland3 text,
    vestiging_landcode integer DEFAULT 6030,
    correspondentie_adres_buitenland1 text,
    correspondentie_adres_buitenland2 text,
    correspondentie_adres_buitenland3 text,
    correspondentie_landcode integer DEFAULT 6030
);


--
-- Name: gm_bedrijf_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE gm_bedrijf_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: gm_bedrijf_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE gm_bedrijf_id_seq OWNED BY gm_bedrijf.id;


--
-- Name: gm_natuurlijk_persoon; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE gm_natuurlijk_persoon (
    id integer NOT NULL,
    gegevens_magazijn_id integer,
    betrokkene_type integer,
    burgerservicenummer character varying(9),
    a_nummer character varying(10),
    voorletters character varying(50),
    voornamen character varying(200),
    geslachtsnaam character varying(200),
    voorvoegsel character varying(50),
    geslachtsaanduiding character varying(3),
    nationaliteitscode1 smallint,
    nationaliteitscode2 smallint,
    nationaliteitscode3 smallint,
    geboorteplaats character varying(75),
    geboorteland character varying(75),
    geboortedatum timestamp without time zone,
    aanhef_aanschrijving character varying(10),
    voorletters_aanschrijving character varying(20),
    voornamen_aanschrijving character varying(200),
    naam_aanschrijving character varying(200),
    voorvoegsel_aanschrijving character varying(50),
    burgerlijke_staat character(1),
    indicatie_geheim character(1),
    import_datum timestamp(6) without time zone,
    adres_id integer,
    authenticatedby text,
    authenticated smallint,
    datum_overlijden timestamp(6) without time zone,
    verblijfsobject_id character varying(16),
    aanduiding_naamgebruik character varying(1),
    onderzoek_persoon boolean,
    onderzoek_huwelijk boolean,
    onderzoek_overlijden boolean,
    onderzoek_verblijfplaats boolean,
    partner_a_nummer character varying(50),
    partner_burgerservicenummer character varying(50),
    partner_voorvoegsel character varying(50),
    partner_geslachtsnaam character varying(50),
    datum_huwelijk timestamp without time zone,
    datum_huwelijk_ontbinding timestamp without time zone,
    landcode integer DEFAULT 6030 NOT NULL,
    naamgebruik text
);


--
-- Name: gm_natuurlijk_persoon_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE gm_natuurlijk_persoon_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: gm_natuurlijk_persoon_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE gm_natuurlijk_persoon_id_seq OWNED BY gm_natuurlijk_persoon.id;


--
-- Name: groups; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE groups (
    id integer NOT NULL,
    path integer[] NOT NULL,
    name text,
    description text,
    date_created timestamp without time zone,
    date_modified timestamp without time zone
);


--
-- Name: groups_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE groups_id_seq OWNED BY groups.id;


--
-- Name: interface; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE interface (
    id integer NOT NULL,
    name text NOT NULL,
    active boolean NOT NULL,
    case_type_id integer,
    max_retries integer NOT NULL,
    interface_config text NOT NULL,
    multiple boolean DEFAULT false NOT NULL,
    module text NOT NULL,
    date_deleted timestamp without time zone,
    uuid uuid DEFAULT uuid_generate_v4() NOT NULL,
    objecttype_id uuid
);


--
-- Name: interface_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE interface_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: interface_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE interface_id_seq OWNED BY interface.id;


--
-- Name: logging; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE logging (
    id integer NOT NULL,
    loglevel character varying(32),
    zaak_id integer,
    betrokkene_id character varying(128),
    aanvrager_id character varying(128),
    is_bericht integer,
    component character varying(64),
    component_id integer,
    seen integer,
    onderwerp text,
    bericht text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    deleted_on timestamp without time zone,
    event_type text,
    event_data text,
    created_by text,
    modified_by text,
    deleted_by text,
    created_for text,
    created_by_name_cache character varying,
    object_uuid uuid,
    restricted boolean DEFAULT false NOT NULL
);


--
-- Name: logging_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE logging_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: logging_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE logging_id_seq OWNED BY logging.id;


--
-- Name: message; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE message (
    id integer NOT NULL,
    message text NOT NULL,
    subject_id character varying,
    logging_id integer NOT NULL,
    is_read boolean DEFAULT false,
    is_archived boolean DEFAULT false NOT NULL
);


--
-- Name: message_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: message_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE message_id_seq OWNED BY message.id;


--
-- Name: natuurlijk_persoon; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE natuurlijk_persoon (
    search_index tsvector,
    search_term text,
    object_type text DEFAULT 'natuurlijk_persoon'::character varying,
    id integer NOT NULL,
    burgerservicenummer character varying(9),
    a_nummer character varying(10),
    voorletters character varying(50),
    voornamen text,
    geslachtsnaam text,
    voorvoegsel character varying(50),
    geslachtsaanduiding character varying(3),
    nationaliteitscode1 smallint,
    nationaliteitscode2 smallint,
    nationaliteitscode3 smallint,
    geboorteplaats text,
    geboorteland text,
    geboortedatum timestamp without time zone,
    aanhef_aanschrijving character varying(10),
    voorletters_aanschrijving character varying(20),
    voornamen_aanschrijving character varying(200),
    naam_aanschrijving text,
    voorvoegsel_aanschrijving character varying(50),
    burgerlijke_staat character(1),
    indicatie_geheim character(1),
    land_waarnaar_vertrokken smallint,
    import_datum timestamp(6) without time zone,
    adres_id integer,
    authenticated boolean,
    authenticatedby text,
    deleted_on timestamp(6) without time zone,
    verblijfsobject_id character varying(16),
    datum_overlijden timestamp without time zone,
    aanduiding_naamgebruik character varying(1),
    onderzoek_persoon boolean,
    onderzoek_huwelijk boolean,
    onderzoek_overlijden boolean,
    onderzoek_verblijfplaats boolean,
    partner_a_nummer character varying(50),
    partner_burgerservicenummer character varying(50),
    partner_voorvoegsel character varying(50),
    partner_geslachtsnaam character varying(50),
    datum_huwelijk timestamp without time zone,
    datum_huwelijk_ontbinding timestamp without time zone,
    in_gemeente boolean,
    landcode integer DEFAULT 6030 NOT NULL,
    naamgebruik text,
    uuid uuid DEFAULT uuid_generate_v4(),
    active boolean DEFAULT true NOT NULL
)
INHERITS (searchable);


--
-- Name: natuurlijk_persoon_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE natuurlijk_persoon_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: natuurlijk_persoon_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE natuurlijk_persoon_id_seq OWNED BY natuurlijk_persoon.id;


--
-- Name: object_acl_entry; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE object_acl_entry (
    uuid uuid DEFAULT uuid_generate_v4() NOT NULL,
    object_uuid uuid NOT NULL,
    entity_type text NOT NULL,
    entity_id text NOT NULL,
    capability text NOT NULL,
    scope text DEFAULT 'instance'::text NOT NULL,
    groupname text,
    CONSTRAINT object_acl_entry_groupname_check CHECK (((groupname IS NULL) OR (character_length(groupname) > 0))),
    CONSTRAINT object_acl_entry_scope_check CHECK ((scope = ANY (ARRAY['instance'::text, 'type'::text])))
);


--
-- Name: object_bibliotheek_entry; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE object_bibliotheek_entry (
    search_index tsvector,
    search_term text NOT NULL,
    object_type text NOT NULL,
    searchable_id integer DEFAULT nextval('searchable_searchable_id_seq'::regclass),
    id integer NOT NULL,
    bibliotheek_categorie_id integer NOT NULL,
    object_uuid uuid NOT NULL
)
INHERITS (searchable);


--
-- Name: object_bibliotheek_entry_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE object_bibliotheek_entry_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: object_bibliotheek_entry_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE object_bibliotheek_entry_id_seq OWNED BY object_bibliotheek_entry.id;


--
-- Name: object_data; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE object_data (
    uuid uuid DEFAULT uuid_generate_v4() NOT NULL,
    object_id integer,
    object_class text NOT NULL,
    properties text DEFAULT '{}'::text NOT NULL,
    index_hstore hstore,
    date_created timestamp without time zone,
    date_modified timestamp without time zone,
    text_vector tsvector,
    class_uuid uuid,
    acl_groupname text,
    invalid boolean DEFAULT false,
    CONSTRAINT object_data_acl_groupname_check CHECK (((acl_groupname IS NULL) OR (character_length(acl_groupname) > 0)))
);


--
-- Name: object_mutation; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE object_mutation (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    object_uuid uuid,
    object_type text DEFAULT 'object'::text,
    lock_object_uuid uuid,
    type text NOT NULL,
    "values" text DEFAULT '{}'::text NOT NULL,
    date_created timestamp without time zone DEFAULT now(),
    subject_id integer NOT NULL,
    executed boolean DEFAULT false NOT NULL,
    CONSTRAINT object_mutation_type_check CHECK ((type = ANY (ARRAY['create'::text, 'update'::text, 'delete'::text, 'relate'::text])))
);


--
-- Name: object_relation; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE object_relation (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    name text NOT NULL,
    object_type text NOT NULL,
    object_uuid uuid,
    object_embedding text,
    object_id uuid,
    CONSTRAINT object_relation_ref_xor_embed CHECK (((object_uuid IS NULL) <> (object_embedding IS NULL)))
);


--
-- Name: object_relationships; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE object_relationships (
    uuid uuid DEFAULT uuid_generate_v4() NOT NULL,
    object1_uuid uuid NOT NULL,
    object2_uuid uuid NOT NULL,
    type1 text NOT NULL,
    type2 text NOT NULL,
    object1_type text NOT NULL,
    object2_type text NOT NULL,
    blocks_deletion boolean DEFAULT false NOT NULL,
    title1 text,
    title2 text,
    owner_object_uuid uuid
);


--
-- Name: object_subscription; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE object_subscription (
    id integer NOT NULL,
    interface_id integer NOT NULL,
    external_id character varying(255) NOT NULL,
    local_table character varying(100) NOT NULL,
    local_id character varying(255) NOT NULL,
    date_created timestamp without time zone DEFAULT now(),
    date_deleted timestamp without time zone,
    object_preview text,
    config_interface_id integer
);


--
-- Name: object_subscription_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE object_subscription_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: object_subscription_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE object_subscription_id_seq OWNED BY object_subscription.id;


--
-- Name: parkeergebied; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE parkeergebied (
    id integer NOT NULL,
    bag_hoofdadres bigint,
    postcode character varying(6) NOT NULL,
    straatnaam character varying(255) NOT NULL,
    huisnummer integer,
    huisletter character varying(8),
    huisnummertoevoeging character varying(4),
    parkeergebied_id integer,
    parkeergebied character varying(255),
    created timestamp without time zone,
    last_modified timestamp without time zone,
    woonplaats character varying(255)
);


--
-- Name: parkeergebied_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE parkeergebied_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: parkeergebied_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE parkeergebied_id_seq OWNED BY parkeergebied.id;


--
-- Name: queue; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE queue (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    object_id uuid,
    status text DEFAULT 'pending'::text NOT NULL,
    type text NOT NULL,
    label text NOT NULL,
    data text DEFAULT '{}'::text NOT NULL,
    date_created timestamp without time zone DEFAULT statement_timestamp() NOT NULL,
    date_started timestamp without time zone,
    date_finished timestamp without time zone,
    parent_id uuid,
    priority integer DEFAULT 1000 NOT NULL,
    CONSTRAINT queue_status_check CHECK ((status = ANY (ARRAY['pending'::text, 'running'::text, 'finished'::text, 'failed'::text, 'waiting'::text])))
);


--
-- Name: remote_api_keys; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE remote_api_keys (
    id integer NOT NULL,
    key character varying(60) NOT NULL,
    permissions text NOT NULL
);


--
-- Name: remote_api_keys_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE remote_api_keys_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: remote_api_keys_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE remote_api_keys_id_seq OWNED BY remote_api_keys.id;


--
-- Name: roles; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE roles (
    id integer NOT NULL,
    parent_group_id integer,
    name text,
    description text,
    system_role boolean,
    date_created timestamp without time zone,
    date_modified timestamp without time zone
);


--
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE roles_id_seq OWNED BY roles.id;


--
-- Name: sbus_logging; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE sbus_logging (
    id integer NOT NULL,
    sbus_traffic_id integer,
    pid integer,
    mutatie_type text,
    object text,
    params text,
    kerngegeven text,
    label text,
    changes text,
    error boolean,
    error_message text,
    created timestamp without time zone,
    modified timestamp without time zone
);


--
-- Name: sbus_logging_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE sbus_logging_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sbus_logging_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE sbus_logging_id_seq OWNED BY sbus_logging.id;


--
-- Name: sbus_traffic; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE sbus_traffic (
    id integer NOT NULL,
    sbus_type text,
    object text,
    operation text,
    input text,
    input_raw text,
    output text,
    output_raw text,
    error boolean,
    error_message text,
    created timestamp without time zone,
    modified timestamp without time zone
);


--
-- Name: sbus_traffic_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE sbus_traffic_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sbus_traffic_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE sbus_traffic_id_seq OWNED BY sbus_traffic.id;


--
-- Name: scheduled_jobs; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE scheduled_jobs (
    id integer NOT NULL,
    task character varying(255) NOT NULL,
    scheduled_for timestamp without time zone,
    parameters text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    deleted timestamp without time zone,
    schedule_type character varying(16),
    case_id integer,
    uuid uuid DEFAULT uuid_generate_v4(),
    CONSTRAINT scheduled_jobs_schedule_type_check CHECK (((schedule_type)::text ~ '(manual|time)'::text))
);


--
-- Name: scheduled_jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE scheduled_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: scheduled_jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE scheduled_jobs_id_seq OWNED BY scheduled_jobs.id;


--
-- Name: search_query; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE search_query (
    id integer NOT NULL,
    settings text,
    ldap_id integer,
    name character varying(256),
    sort_index integer
);


--
-- Name: search_query_delen; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE search_query_delen (
    id integer NOT NULL,
    search_query_id integer,
    ou_id integer,
    role_id integer
);


--
-- Name: search_query_delen_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE search_query_delen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: search_query_delen_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE search_query_delen_id_seq OWNED BY search_query_delen.id;


--
-- Name: search_query_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE search_query_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: search_query_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE search_query_id_seq OWNED BY search_query.id;


--
-- Name: settings; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE settings (
    id integer NOT NULL,
    key character varying(255),
    value text
);


--
-- Name: settings_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE settings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: settings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE settings_id_seq OWNED BY settings.id;


--
-- Name: subject; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE subject (
    id integer NOT NULL,
    uuid uuid DEFAULT uuid_generate_v4(),
    subject_type text NOT NULL,
    properties text DEFAULT '{}'::text NOT NULL,
    settings text DEFAULT '{}'::text NOT NULL,
    username character varying NOT NULL,
    last_modified timestamp without time zone DEFAULT now(),
    role_ids integer[],
    group_ids integer[],
    nobody boolean DEFAULT false NOT NULL,
    system boolean DEFAULT false NOT NULL,
    CONSTRAINT subject_subject_type_check CHECK ((subject_type = ANY (ARRAY['person'::text, 'company'::text, 'employee'::text])))
);


--
-- Name: subject_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE subject_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: subject_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE subject_id_seq OWNED BY subject.id;


--
-- Name: transaction; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE transaction (
    id integer NOT NULL,
    interface_id integer NOT NULL,
    external_transaction_id character varying(250),
    input_data text,
    input_file integer,
    automated_retry_count integer,
    date_created timestamp without time zone DEFAULT now() NOT NULL,
    date_last_retry timestamp without time zone,
    date_next_retry timestamp without time zone,
    processed boolean DEFAULT false,
    date_deleted timestamp without time zone,
    error_count integer DEFAULT 0,
    direction character varying(255) DEFAULT 'incoming'::character varying NOT NULL,
    success_count integer DEFAULT 0,
    total_count integer DEFAULT 0,
    processor_params text,
    error_fatal boolean,
    preview_data text DEFAULT '{}'::text NOT NULL,
    error_message text,
    text_vector tsvector,
    uuid uuid DEFAULT uuid_generate_v4() NOT NULL,
    CONSTRAINT input_data_or_input_file CHECK (((input_data IS NOT NULL) OR (input_file IS NOT NULL))),
    CONSTRAINT transaction_direction_check CHECK ((((direction)::text = 'incoming'::text) OR ((direction)::text = 'outgoing'::text)))
);


--
-- Name: transaction_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE transaction_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: transaction_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE transaction_id_seq OWNED BY transaction.id;


--
-- Name: transaction_record; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE transaction_record (
    id integer NOT NULL,
    transaction_id integer,
    input text NOT NULL,
    output text NOT NULL,
    is_error boolean DEFAULT false NOT NULL,
    date_executed timestamp without time zone DEFAULT now() NOT NULL,
    date_deleted timestamp without time zone,
    preview_string character varying(200),
    last_error text,
    uuid uuid DEFAULT uuid_generate_v4() NOT NULL
);


--
-- Name: transaction_record_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE transaction_record_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: transaction_record_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE transaction_record_id_seq OWNED BY transaction_record.id;


--
-- Name: transaction_record_to_object; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE transaction_record_to_object (
    id integer NOT NULL,
    transaction_record_id integer,
    local_table character varying(100),
    local_id character varying(255),
    mutations text,
    date_deleted timestamp without time zone,
    mutation_type character varying(100)
);


--
-- Name: transaction_record_to_object_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE transaction_record_to_object_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: transaction_record_to_object_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE transaction_record_to_object_id_seq OWNED BY transaction_record_to_object.id;


--
-- Name: user_app_lock; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE user_app_lock (
    type character(40) NOT NULL,
    type_id character(20) NOT NULL,
    create_unixtime integer NOT NULL,
    session_id character(40) NOT NULL,
    uidnumber integer NOT NULL
);


--
-- Name: user_entity; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE user_entity (
    id integer NOT NULL,
    uuid uuid DEFAULT uuid_generate_v4(),
    source_interface_id integer,
    source_identifier text NOT NULL,
    subject_id integer,
    date_created timestamp without time zone,
    date_deleted timestamp without time zone,
    properties text DEFAULT '{}'::text,
    password character varying(255)
);


--
-- Name: user_entity_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE user_entity_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_entity_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE user_entity_id_seq OWNED BY user_entity.id;


--
-- Name: woz_objects; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE woz_objects (
    id integer NOT NULL,
    object_data text,
    owner character varying(255) NOT NULL,
    object_id character varying(32) NOT NULL
);


--
-- Name: woz_objects_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE woz_objects_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: woz_objects_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE woz_objects_id_seq OWNED BY woz_objects.id;


--
-- Name: zaak; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE zaak (
    search_index tsvector,
    search_term text,
    object_type text DEFAULT 'zaak'::character varying,
    id integer NOT NULL,
    pid integer,
    relates_to integer,
    zaaktype_id integer NOT NULL,
    zaaktype_node_id integer NOT NULL,
    milestone integer NOT NULL,
    contactkanaal character varying(128) NOT NULL,
    aanvraag_trigger zaaksysteem_trigger NOT NULL,
    onderwerp text,
    resultaat text,
    besluit text,
    coordinator integer,
    behandelaar integer,
    aanvrager integer NOT NULL,
    route_ou integer,
    route_role integer,
    locatie_zaak integer,
    locatie_correspondentie integer,
    streefafhandeldatum timestamp(6) without time zone,
    registratiedatum timestamp(6) without time zone NOT NULL,
    afhandeldatum timestamp(6) without time zone,
    vernietigingsdatum timestamp(6) without time zone,
    created timestamp(6) without time zone NOT NULL,
    last_modified timestamp(6) without time zone NOT NULL,
    deleted timestamp(6) without time zone,
    vervolg_van integer,
    aanvrager_gm_id integer,
    behandelaar_gm_id integer,
    coordinator_gm_id integer,
    uuid uuid,
    payment_status text,
    payment_amount numeric(100,2),
    hstore_properties hstore,
    confidentiality confidentiality DEFAULT 'public'::confidentiality NOT NULL,
    stalled_until timestamp without time zone,
    onderwerp_extern text,
    archival_state text,
    status text NOT NULL,
    duplicate_prevention_token uuid DEFAULT uuid_generate_v4() NOT NULL,
    CONSTRAINT archival_state_value CHECK ((archival_state = ANY (ARRAY['overdragen'::text, 'vernietigen'::text]))),
    CONSTRAINT status_value CHECK ((status = ANY (ARRAY['new'::text, 'open'::text, 'stalled'::text, 'resolved'::text, 'deleted'::text])))
)
INHERITS (searchable);


--
-- Name: zaak_authorisation; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE zaak_authorisation (
    id integer NOT NULL,
    zaak_id integer NOT NULL,
    capability text NOT NULL,
    entity_id text NOT NULL,
    entity_type text NOT NULL,
    scope text NOT NULL
);


--
-- Name: zaak_authorisation_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE zaak_authorisation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaak_authorisation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE zaak_authorisation_id_seq OWNED BY zaak_authorisation.id;


--
-- Name: zaak_bag; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE zaak_bag (
    id integer NOT NULL,
    pid integer,
    zaak_id integer,
    bag_type zaaksysteem_bag_types,
    bag_id character varying(255),
    bag_verblijfsobject_id character varying(255),
    bag_openbareruimte_id character varying(255),
    bag_nummeraanduiding_id character varying(255),
    bag_pand_id character varying(255),
    bag_standplaats_id character varying(255),
    bag_ligplaats_id character varying(255),
    bag_coordinates_wsg point
);


--
-- Name: zaak_bag_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE zaak_bag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaak_bag_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE zaak_bag_id_seq OWNED BY zaak_bag.id;


--
-- Name: zaak_betrokkenen; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE zaak_betrokkenen (
    id integer NOT NULL,
    zaak_id integer,
    betrokkene_type character varying(128),
    betrokkene_id integer,
    gegevens_magazijn_id integer,
    verificatie character varying(128),
    naam character varying(255),
    rol text,
    magic_string_prefix text,
    deleted timestamp without time zone,
    uuid uuid DEFAULT uuid_generate_v4(),
    pip_authorized boolean DEFAULT false NOT NULL
);


--
-- Name: zaak_betrokkenen_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE zaak_betrokkenen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaak_betrokkenen_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE zaak_betrokkenen_id_seq OWNED BY zaak_betrokkenen.id;


--
-- Name: zaak_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE zaak_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaak_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE zaak_id_seq OWNED BY zaak.id;


--
-- Name: zaak_kenmerk; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE zaak_kenmerk (
    zaak_id integer NOT NULL,
    bibliotheek_kenmerken_id integer NOT NULL,
    value text NOT NULL,
    id integer NOT NULL
);


--
-- Name: zaak_kenmerk_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE zaak_kenmerk_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaak_kenmerk_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE zaak_kenmerk_id_seq OWNED BY zaak_kenmerk.id;


--
-- Name: zaak_meta; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE zaak_meta (
    id integer NOT NULL,
    zaak_id integer,
    verlenging character varying(255),
    opschorten character varying(255),
    deel character varying(255),
    gerelateerd character varying(255),
    vervolg character varying(255),
    afhandeling character varying(255),
    stalled_since timestamp without time zone
);


--
-- Name: zaak_meta_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE zaak_meta_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaak_meta_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE zaak_meta_id_seq OWNED BY zaak_meta.id;


--
-- Name: zaak_onafgerond; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE zaak_onafgerond (
    zaaktype_id integer NOT NULL,
    betrokkene character(50) NOT NULL,
    json_string text NOT NULL,
    afronden boolean,
    create_unixtime integer
);


--
-- Name: zaak_subcase; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE zaak_subcase (
    id integer NOT NULL,
    zaak_id integer NOT NULL,
    relation_zaak_id integer,
    required integer,
    parent_advance_results text
);


--
-- Name: zaak_subcase_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE zaak_subcase_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaak_subcase_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE zaak_subcase_id_seq OWNED BY zaak_subcase.id;


--
-- Name: zaaktype; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE zaaktype (
    search_index tsvector,
    search_term text,
    object_type text DEFAULT 'zaaktype'::character varying,
    id integer NOT NULL,
    zaaktype_node_id integer,
    version integer,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    deleted timestamp without time zone,
    bibliotheek_categorie_id integer,
    active boolean DEFAULT true NOT NULL
)
INHERITS (searchable);


--
-- Name: zaaktype_authorisation; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE zaaktype_authorisation (
    id integer NOT NULL,
    zaaktype_node_id integer,
    recht text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    deleted timestamp without time zone,
    role_id integer,
    ou_id integer,
    zaaktype_id integer,
    confidential boolean DEFAULT false NOT NULL
);


--
-- Name: zaaktype_authorisation_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE zaaktype_authorisation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_authorisation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE zaaktype_authorisation_id_seq OWNED BY zaaktype_authorisation.id;


--
-- Name: zaaktype_betrokkenen; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE zaaktype_betrokkenen (
    id integer NOT NULL,
    zaaktype_node_id integer,
    betrokkene_type text,
    created timestamp without time zone,
    last_modified timestamp without time zone
);


--
-- Name: zaaktype_betrokkenen_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE zaaktype_betrokkenen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_betrokkenen_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE zaaktype_betrokkenen_id_seq OWNED BY zaaktype_betrokkenen.id;


--
-- Name: zaaktype_definitie; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE zaaktype_definitie (
    id integer NOT NULL,
    openbaarheid character varying(255),
    handelingsinitiator character varying(255),
    grondslag text,
    procesbeschrijving character varying(255),
    afhandeltermijn character varying(255),
    afhandeltermijn_type character varying(255),
    selectielijst character varying(255),
    servicenorm character varying(255),
    servicenorm_type character varying(255),
    pdc_voorwaarden text,
    pdc_description text,
    pdc_meenemen text,
    pdc_tarief text,
    omschrijving_upl character varying(255),
    aard character varying(255),
    extra_informatie text,
    preset_client character varying(255),
    extra_informatie_extern text
);


--
-- Name: zaaktype_definitie_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE zaaktype_definitie_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_definitie_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE zaaktype_definitie_id_seq OWNED BY zaaktype_definitie.id;


--
-- Name: zaaktype_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE zaaktype_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE zaaktype_id_seq OWNED BY zaaktype.id;


--
-- Name: zaaktype_kenmerken; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE zaaktype_kenmerken (
    id integer NOT NULL,
    bibliotheek_kenmerken_id integer,
    value_mandatory integer,
    label text,
    help text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    zaaktype_node_id integer,
    zaak_status_id integer,
    pip integer,
    zaakinformatie_view integer DEFAULT 1,
    bag_zaakadres integer,
    is_group integer,
    date_fromcurrentdate integer DEFAULT 0,
    value_default text,
    pip_can_change boolean,
    publish_public integer,
    referential character varying(12),
    is_systeemkenmerk boolean DEFAULT false,
    required_permissions text,
    version integer,
    help_extern text,
    object_id uuid,
    object_metadata text DEFAULT '{}'::text NOT NULL,
    label_multiple text,
    properties text DEFAULT '{}'::text
);


--
-- Name: zaaktype_kenmerken_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE zaaktype_kenmerken_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_kenmerken_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE zaaktype_kenmerken_id_seq OWNED BY zaaktype_kenmerken.id;


--
-- Name: zaaktype_node; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE zaaktype_node (
    id integer NOT NULL,
    zaaktype_id integer,
    zaaktype_rt_queue text,
    code text,
    trigger text,
    titel character varying(128),
    version integer,
    active integer,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    deleted timestamp without time zone,
    webform_toegang integer,
    webform_authenticatie text,
    adres_relatie text,
    aanvrager_hergebruik integer,
    automatisch_aanvragen integer,
    automatisch_behandelen integer,
    toewijzing_zaakintake integer,
    toelichting character varying(128),
    online_betaling integer,
    zaaktype_definitie_id integer,
    adres_andere_locatie integer,
    adres_aanvrager integer,
    bedrijfid_wijzigen integer,
    zaaktype_vertrouwelijk integer,
    zaaktype_trefwoorden text,
    zaaktype_omschrijving text,
    extra_relaties_in_aanvraag boolean,
    properties text DEFAULT '{}'::text NOT NULL,
    contact_info_intake boolean DEFAULT true,
    is_public boolean DEFAULT false,
    prevent_pip boolean DEFAULT false,
    contact_info_email_required boolean DEFAULT false,
    contact_info_phone_required boolean DEFAULT false,
    contact_info_mobile_phone_required boolean DEFAULT false
);


--
-- Name: zaaktype_node_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE zaaktype_node_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_node_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE zaaktype_node_id_seq OWNED BY zaaktype_node.id;


--
-- Name: zaaktype_notificatie; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE zaaktype_notificatie (
    id integer NOT NULL,
    zaaktype_node_id integer,
    zaak_status_id integer,
    label text,
    rcpt text,
    onderwerp text,
    bericht text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    intern_block integer,
    email text,
    bibliotheek_notificaties_id integer,
    behandelaar character varying(255),
    automatic integer,
    cc text,
    bcc text
);


--
-- Name: zaaktype_notificatie_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE zaaktype_notificatie_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_notificatie_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE zaaktype_notificatie_id_seq OWNED BY zaaktype_notificatie.id;


--
-- Name: zaaktype_regel; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE zaaktype_regel (
    id integer NOT NULL,
    zaaktype_node_id integer,
    zaak_status_id integer,
    naam text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    settings text,
    active boolean DEFAULT true,
    is_group boolean DEFAULT false
);


--
-- Name: zaaktype_regel_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE zaaktype_regel_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_regel_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE zaaktype_regel_id_seq OWNED BY zaaktype_regel.id;


--
-- Name: zaaktype_relatie; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE zaaktype_relatie (
    id integer NOT NULL,
    zaaktype_node_id integer,
    relatie_zaaktype_id integer,
    zaaktype_status_id integer,
    relatie_type text,
    eigenaar_type text DEFAULT 'aanvrager'::text NOT NULL,
    start_delay character varying(255),
    created timestamp without time zone,
    last_modified timestamp without time zone,
    status integer,
    kopieren_kenmerken integer,
    ou_id integer,
    role_id integer,
    automatisch_behandelen boolean,
    required character varying(12),
    parent_advance_results text,
    betrokkene_authorized boolean,
    betrokkene_notify boolean,
    betrokkene_id text,
    betrokkene_role text,
    betrokkene_role_set text,
    betrokkene_prefix text,
    eigenaar_id text
);


--
-- Name: zaaktype_relatie_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE zaaktype_relatie_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_relatie_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE zaaktype_relatie_id_seq OWNED BY zaaktype_relatie.id;


--
-- Name: zaaktype_resultaten; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE zaaktype_resultaten (
    id integer NOT NULL,
    zaaktype_node_id integer,
    zaaktype_status_id integer,
    resultaat text,
    ingang text,
    bewaartermijn integer,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    dossiertype character varying(50),
    label text,
    selectielijst text,
    archiefnominatie text,
    comments text,
    external_reference text,
    trigger_archival boolean DEFAULT true NOT NULL,
    selectielijst_brondatum date,
    selectielijst_einddatum date
);


--
-- Name: zaaktype_resultaten_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE zaaktype_resultaten_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_resultaten_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE zaaktype_resultaten_id_seq OWNED BY zaaktype_resultaten.id;


--
-- Name: zaaktype_sjablonen; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE zaaktype_sjablonen (
    id integer NOT NULL,
    zaaktype_node_id integer,
    bibliotheek_sjablonen_id integer,
    help text,
    zaak_status_id integer,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    automatisch_genereren integer,
    bibliotheek_kenmerken_id integer,
    target_format character varying(5)
);


--
-- Name: zaaktype_sjablonen_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE zaaktype_sjablonen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_sjablonen_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE zaaktype_sjablonen_id_seq OWNED BY zaaktype_sjablonen.id;


--
-- Name: zaaktype_standaard_betrokkenen; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE zaaktype_standaard_betrokkenen (
    id integer NOT NULL,
    zaaktype_node_id integer NOT NULL,
    zaak_status_id integer NOT NULL,
    betrokkene_type text NOT NULL,
    betrokkene_identifier text NOT NULL,
    naam text NOT NULL,
    rol text NOT NULL,
    magic_string_prefix text,
    gemachtigd boolean DEFAULT false NOT NULL,
    notify boolean DEFAULT false NOT NULL,
    uuid uuid DEFAULT uuid_generate_v4()
);


--
-- Name: zaaktype_standaard_betrokkenen_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE zaaktype_standaard_betrokkenen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_standaard_betrokkenen_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE zaaktype_standaard_betrokkenen_id_seq OWNED BY zaaktype_standaard_betrokkenen.id;


--
-- Name: zaaktype_status; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE zaaktype_status (
    id integer NOT NULL,
    zaaktype_node_id integer,
    status integer,
    status_type text,
    naam text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    ou_id integer,
    role_id integer,
    checklist integer,
    fase character varying(255),
    role_set integer
);


--
-- Name: zaaktype_status_checklist_item; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE zaaktype_status_checklist_item (
    id integer NOT NULL,
    casetype_status_id integer NOT NULL,
    label text,
    external_reference text
);


--
-- Name: zaaktype_status_checklist_item_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE zaaktype_status_checklist_item_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_status_checklist_item_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE zaaktype_status_checklist_item_id_seq OWNED BY zaaktype_status_checklist_item.id;


--
-- Name: zaaktype_status_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE zaaktype_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_status_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE zaaktype_status_id_seq OWNED BY zaaktype_status.id;


--
-- Name: zorginstituut_identificatie_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE zorginstituut_identificatie_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY adres ALTER COLUMN id SET DEFAULT nextval('adres_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY bag_ligplaats ALTER COLUMN id SET DEFAULT nextval('bag_ligplaats_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY bag_nummeraanduiding ALTER COLUMN id SET DEFAULT nextval('bag_nummeraanduiding_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY bag_openbareruimte ALTER COLUMN id SET DEFAULT nextval('bag_openbareruimte_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY bag_pand ALTER COLUMN id SET DEFAULT nextval('bag_pand_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY bag_standplaats ALTER COLUMN id SET DEFAULT nextval('bag_standplaats_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY bag_verblijfsobject ALTER COLUMN id SET DEFAULT nextval('bag_verblijfsobject_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY bag_verblijfsobject_gebruiksdoel ALTER COLUMN id SET DEFAULT nextval('bag_verblijfsobject_gebruiksdoel_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY bag_verblijfsobject_pand ALTER COLUMN id SET DEFAULT nextval('bag_verblijfsobject_pand_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY bag_woonplaats ALTER COLUMN id SET DEFAULT nextval('bag_woonplaats_id_seq'::regclass);


--
-- Name: searchable_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY bedrijf ALTER COLUMN searchable_id SET DEFAULT nextval('searchable_searchable_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY bedrijf ALTER COLUMN id SET DEFAULT nextval('bedrijf_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY bedrijf_authenticatie ALTER COLUMN id SET DEFAULT nextval('bedrijf_authenticatie_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY beheer_import ALTER COLUMN id SET DEFAULT nextval('beheer_import_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY beheer_import_log ALTER COLUMN id SET DEFAULT nextval('beheer_import_log_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY betrokkene_notes ALTER COLUMN id SET DEFAULT nextval('betrokkene_notes_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY betrokkenen ALTER COLUMN id SET DEFAULT nextval('betrokkenen_id_seq'::regclass);


--
-- Name: searchable_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY bibliotheek_categorie ALTER COLUMN searchable_id SET DEFAULT nextval('searchable_searchable_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY bibliotheek_categorie ALTER COLUMN id SET DEFAULT nextval('bibliotheek_categorie_id_seq'::regclass);


--
-- Name: searchable_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY bibliotheek_kenmerken ALTER COLUMN searchable_id SET DEFAULT nextval('searchable_searchable_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY bibliotheek_kenmerken ALTER COLUMN id SET DEFAULT nextval('bibliotheek_kenmerken_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY bibliotheek_kenmerken_values ALTER COLUMN id SET DEFAULT nextval('bibliotheek_kenmerken_values_id_seq'::regclass);


--
-- Name: sort_order; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY bibliotheek_kenmerken_values ALTER COLUMN sort_order SET DEFAULT nextval('bibliotheek_kenmerken_values_sort_order_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY bibliotheek_notificatie_kenmerk ALTER COLUMN id SET DEFAULT nextval('bibliotheek_notificatie_kenmerk_id_seq'::regclass);


--
-- Name: searchable_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY bibliotheek_notificaties ALTER COLUMN searchable_id SET DEFAULT nextval('searchable_searchable_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY bibliotheek_notificaties ALTER COLUMN id SET DEFAULT nextval('bibliotheek_notificaties_id_seq'::regclass);


--
-- Name: searchable_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY bibliotheek_sjablonen ALTER COLUMN searchable_id SET DEFAULT nextval('searchable_searchable_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY bibliotheek_sjablonen ALTER COLUMN id SET DEFAULT nextval('bibliotheek_sjablonen_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY bibliotheek_sjablonen_magic_string ALTER COLUMN id SET DEFAULT nextval('bibliotheek_sjablonen_magic_string_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY case_action ALTER COLUMN id SET DEFAULT nextval('case_action_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY case_relation ALTER COLUMN id SET DEFAULT nextval('case_relation_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY checklist ALTER COLUMN id SET DEFAULT nextval('checklist_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY checklist_item ALTER COLUMN id SET DEFAULT nextval('checklist_item_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY config ALTER COLUMN id SET DEFAULT nextval('config_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY contact_data ALTER COLUMN id SET DEFAULT nextval('contact_data_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY contactmoment ALTER COLUMN id SET DEFAULT nextval('contactmoment_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY contactmoment_email ALTER COLUMN id SET DEFAULT nextval('contactmoment_email_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY contactmoment_note ALTER COLUMN id SET DEFAULT nextval('contactmoment_note_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY directory ALTER COLUMN id SET DEFAULT nextval('directory_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY file ALTER COLUMN id SET DEFAULT nextval('file_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY file_case_document ALTER COLUMN id SET DEFAULT nextval('file_case_document_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY file_metadata ALTER COLUMN id SET DEFAULT nextval('file_metadata_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY filestore ALTER COLUMN id SET DEFAULT nextval('filestore_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY gm_adres ALTER COLUMN id SET DEFAULT nextval('gm_adres_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY gm_bedrijf ALTER COLUMN id SET DEFAULT nextval('gm_bedrijf_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY gm_natuurlijk_persoon ALTER COLUMN id SET DEFAULT nextval('gm_natuurlijk_persoon_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY interface ALTER COLUMN id SET DEFAULT nextval('interface_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY logging ALTER COLUMN id SET DEFAULT nextval('logging_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY message ALTER COLUMN id SET DEFAULT nextval('message_id_seq'::regclass);


--
-- Name: searchable_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY natuurlijk_persoon ALTER COLUMN searchable_id SET DEFAULT nextval('searchable_searchable_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY natuurlijk_persoon ALTER COLUMN id SET DEFAULT nextval('natuurlijk_persoon_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY object_bibliotheek_entry ALTER COLUMN id SET DEFAULT nextval('object_bibliotheek_entry_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY object_subscription ALTER COLUMN id SET DEFAULT nextval('object_subscription_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY parkeergebied ALTER COLUMN id SET DEFAULT nextval('parkeergebied_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY remote_api_keys ALTER COLUMN id SET DEFAULT nextval('remote_api_keys_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY roles ALTER COLUMN id SET DEFAULT nextval('roles_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY sbus_logging ALTER COLUMN id SET DEFAULT nextval('sbus_logging_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY sbus_traffic ALTER COLUMN id SET DEFAULT nextval('sbus_traffic_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY scheduled_jobs ALTER COLUMN id SET DEFAULT nextval('scheduled_jobs_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY search_query ALTER COLUMN id SET DEFAULT nextval('search_query_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY search_query_delen ALTER COLUMN id SET DEFAULT nextval('search_query_delen_id_seq'::regclass);


--
-- Name: searchable_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY searchable ALTER COLUMN searchable_id SET DEFAULT nextval('searchable_searchable_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY settings ALTER COLUMN id SET DEFAULT nextval('settings_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY subject ALTER COLUMN id SET DEFAULT nextval('subject_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY transaction ALTER COLUMN id SET DEFAULT nextval('transaction_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY transaction_record ALTER COLUMN id SET DEFAULT nextval('transaction_record_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY transaction_record_to_object ALTER COLUMN id SET DEFAULT nextval('transaction_record_to_object_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_entity ALTER COLUMN id SET DEFAULT nextval('user_entity_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY woz_objects ALTER COLUMN id SET DEFAULT nextval('woz_objects_id_seq'::regclass);


--
-- Name: searchable_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaak ALTER COLUMN searchable_id SET DEFAULT nextval('searchable_searchable_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaak ALTER COLUMN id SET DEFAULT nextval('zaak_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaak_authorisation ALTER COLUMN id SET DEFAULT nextval('zaak_authorisation_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaak_bag ALTER COLUMN id SET DEFAULT nextval('zaak_bag_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaak_betrokkenen ALTER COLUMN id SET DEFAULT nextval('zaak_betrokkenen_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaak_kenmerk ALTER COLUMN id SET DEFAULT nextval('zaak_kenmerk_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaak_meta ALTER COLUMN id SET DEFAULT nextval('zaak_meta_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaak_subcase ALTER COLUMN id SET DEFAULT nextval('zaak_subcase_id_seq'::regclass);


--
-- Name: searchable_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaaktype ALTER COLUMN searchable_id SET DEFAULT nextval('searchable_searchable_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaaktype ALTER COLUMN id SET DEFAULT nextval('zaaktype_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaaktype_authorisation ALTER COLUMN id SET DEFAULT nextval('zaaktype_authorisation_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaaktype_betrokkenen ALTER COLUMN id SET DEFAULT nextval('zaaktype_betrokkenen_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaaktype_definitie ALTER COLUMN id SET DEFAULT nextval('zaaktype_definitie_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaaktype_kenmerken ALTER COLUMN id SET DEFAULT nextval('zaaktype_kenmerken_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaaktype_node ALTER COLUMN id SET DEFAULT nextval('zaaktype_node_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaaktype_notificatie ALTER COLUMN id SET DEFAULT nextval('zaaktype_notificatie_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaaktype_regel ALTER COLUMN id SET DEFAULT nextval('zaaktype_regel_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaaktype_relatie ALTER COLUMN id SET DEFAULT nextval('zaaktype_relatie_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaaktype_resultaten ALTER COLUMN id SET DEFAULT nextval('zaaktype_resultaten_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaaktype_sjablonen ALTER COLUMN id SET DEFAULT nextval('zaaktype_sjablonen_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaaktype_standaard_betrokkenen ALTER COLUMN id SET DEFAULT nextval('zaaktype_standaard_betrokkenen_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaaktype_status ALTER COLUMN id SET DEFAULT nextval('zaaktype_status_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaaktype_status_checklist_item ALTER COLUMN id SET DEFAULT nextval('zaaktype_status_checklist_item_id_seq'::regclass);


--
-- Name: adres_id; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY natuurlijk_persoon
    ADD CONSTRAINT adres_id UNIQUE (id);


--
-- Name: adres_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY adres
    ADD CONSTRAINT adres_pkey PRIMARY KEY (id);


--
-- Name: alternative_authentication_activation_link_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY alternative_authentication_activation_link
    ADD CONSTRAINT alternative_authentication_activation_link_pkey PRIMARY KEY (token);


--
-- Name: bag_ligplaats_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY bag_ligplaats
    ADD CONSTRAINT bag_ligplaats_pkey PRIMARY KEY (id);


--
-- Name: bag_nummeraanduiding_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY bag_nummeraanduiding
    ADD CONSTRAINT bag_nummeraanduiding_pkey PRIMARY KEY (id);


--
-- Name: bag_openbareruimte_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY bag_openbareruimte
    ADD CONSTRAINT bag_openbareruimte_pkey PRIMARY KEY (id);


--
-- Name: bag_pand_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY bag_pand
    ADD CONSTRAINT bag_pand_pkey PRIMARY KEY (id);


--
-- Name: bag_standplaats_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY bag_standplaats
    ADD CONSTRAINT bag_standplaats_pkey PRIMARY KEY (id);


--
-- Name: bag_verblijfsobject_gebruiksdoel_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY bag_verblijfsobject_gebruiksdoel
    ADD CONSTRAINT bag_verblijfsobject_gebruiksdoel_pkey PRIMARY KEY (id);


--
-- Name: bag_verblijfsobject_pand_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY bag_verblijfsobject_pand
    ADD CONSTRAINT bag_verblijfsobject_pand_pkey PRIMARY KEY (id);


--
-- Name: bag_verblijfsobject_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY bag_verblijfsobject
    ADD CONSTRAINT bag_verblijfsobject_pkey PRIMARY KEY (id);


--
-- Name: bag_woonplaats_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY bag_woonplaats
    ADD CONSTRAINT bag_woonplaats_pkey PRIMARY KEY (id);


--
-- Name: bedrijf_authenticatie_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY bedrijf_authenticatie
    ADD CONSTRAINT bedrijf_authenticatie_pkey PRIMARY KEY (id);


--
-- Name: bedrijf_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY bedrijf
    ADD CONSTRAINT bedrijf_pkey PRIMARY KEY (id);


--
-- Name: bedrijf_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY bedrijf
    ADD CONSTRAINT bedrijf_uuid_key UNIQUE (uuid);


--
-- Name: beheer_import_log_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY beheer_import_log
    ADD CONSTRAINT beheer_import_log_pkey PRIMARY KEY (id);


--
-- Name: beheer_import_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY beheer_import
    ADD CONSTRAINT beheer_import_pkey PRIMARY KEY (id);


--
-- Name: betrokkene_notes_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY betrokkene_notes
    ADD CONSTRAINT betrokkene_notes_pkey PRIMARY KEY (id);


--
-- Name: betrokkenen_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY betrokkenen
    ADD CONSTRAINT betrokkenen_pkey PRIMARY KEY (id);


--
-- Name: bibliotheek_categorie_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY bibliotheek_categorie
    ADD CONSTRAINT bibliotheek_categorie_pkey PRIMARY KEY (id);


--
-- Name: bibliotheek_kenmerken_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY bibliotheek_kenmerken
    ADD CONSTRAINT bibliotheek_kenmerken_pkey PRIMARY KEY (id);


--
-- Name: bibliotheek_kenmerken_values_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY bibliotheek_kenmerken_values
    ADD CONSTRAINT bibliotheek_kenmerken_values_pkey PRIMARY KEY (id);


--
-- Name: bibliotheek_notificatie_kenmerk_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY bibliotheek_notificatie_kenmerk
    ADD CONSTRAINT bibliotheek_notificatie_kenmerk_pkey PRIMARY KEY (id);


--
-- Name: bibliotheek_notificaties_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY bibliotheek_notificaties
    ADD CONSTRAINT bibliotheek_notificaties_pkey PRIMARY KEY (id);


--
-- Name: bibliotheek_sjablonen_magic_string_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY bibliotheek_sjablonen_magic_string
    ADD CONSTRAINT bibliotheek_sjablonen_magic_string_pkey PRIMARY KEY (id);


--
-- Name: bibliotheek_sjablonen_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY bibliotheek_sjablonen
    ADD CONSTRAINT bibliotheek_sjablonen_pkey PRIMARY KEY (id);


--
-- Name: case_actions_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY case_action
    ADD CONSTRAINT case_actions_pkey PRIMARY KEY (id);


--
-- Name: case_relation_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY case_relation
    ADD CONSTRAINT case_relation_pkey PRIMARY KEY (id);


--
-- Name: checklist_item_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY checklist_item
    ADD CONSTRAINT checklist_item_pkey PRIMARY KEY (id);


--
-- Name: checklist_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY checklist
    ADD CONSTRAINT checklist_pkey PRIMARY KEY (id);


--
-- Name: config_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY config
    ADD CONSTRAINT config_pkey PRIMARY KEY (id);


--
-- Name: contact_data_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY contact_data
    ADD CONSTRAINT contact_data_pkey PRIMARY KEY (id);


--
-- Name: contactmoment_email_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY contactmoment_email
    ADD CONSTRAINT contactmoment_email_pkey PRIMARY KEY (id);


--
-- Name: contactmoment_note_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY contactmoment_note
    ADD CONSTRAINT contactmoment_note_pkey PRIMARY KEY (id);


--
-- Name: contactmoment_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY contactmoment
    ADD CONSTRAINT contactmoment_pkey PRIMARY KEY (id);


--
-- Name: directory_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY directory
    ADD CONSTRAINT directory_pkey PRIMARY KEY (id);


--
-- Name: file_annotation_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY file_annotation
    ADD CONSTRAINT file_annotation_pkey PRIMARY KEY (id);


--
-- Name: file_case_document_file_id_case_document_id_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY file_case_document
    ADD CONSTRAINT file_case_document_file_id_case_document_id_key UNIQUE (file_id, case_document_id);


--
-- Name: file_case_document_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY file_case_document
    ADD CONSTRAINT file_case_document_pkey PRIMARY KEY (id);


--
-- Name: file_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY file_metadata
    ADD CONSTRAINT file_metadata_pkey PRIMARY KEY (id);


--
-- Name: file_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY file
    ADD CONSTRAINT file_pkey PRIMARY KEY (id);


--
-- Name: file_thumbnail_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY file_derivative
    ADD CONSTRAINT file_thumbnail_pkey PRIMARY KEY (id);


--
-- Name: filestore_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY filestore
    ADD CONSTRAINT filestore_pkey PRIMARY KEY (id);


--
-- Name: gegevensmagazijn_subjecten_subject_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY gegevensmagazijn_subjecten
    ADD CONSTRAINT gegevensmagazijn_subjecten_subject_uuid_key UNIQUE (subject_uuid);


--
-- Name: gm_adres_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY gm_adres
    ADD CONSTRAINT gm_adres_pkey PRIMARY KEY (id);


--
-- Name: gm_bedrijf_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY gm_bedrijf
    ADD CONSTRAINT gm_bedrijf_pkey PRIMARY KEY (id);


--
-- Name: gm_natuurlijk_persoon_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY gm_natuurlijk_persoon
    ADD CONSTRAINT gm_natuurlijk_persoon_pkey PRIMARY KEY (id);


--
-- Name: groups_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY groups
    ADD CONSTRAINT groups_pkey PRIMARY KEY (id);


--
-- Name: interface_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY interface
    ADD CONSTRAINT interface_pkey PRIMARY KEY (id);


--
-- Name: logging_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY logging
    ADD CONSTRAINT logging_pkey PRIMARY KEY (id);


--
-- Name: message_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY message
    ADD CONSTRAINT message_pkey PRIMARY KEY (id);


--
-- Name: name_case_id; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY directory
    ADD CONSTRAINT name_case_id UNIQUE (name, case_id);


--
-- Name: natuurlijk_persoon_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY natuurlijk_persoon
    ADD CONSTRAINT natuurlijk_persoon_pkey PRIMARY KEY (id);


--
-- Name: natuurlijk_persoon_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY natuurlijk_persoon
    ADD CONSTRAINT natuurlijk_persoon_uuid_key UNIQUE (uuid);


--
-- Name: object_acl_entry_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY object_acl_entry
    ADD CONSTRAINT object_acl_entry_pkey PRIMARY KEY (uuid);


--
-- Name: object_data_object_class_object_id_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY object_data
    ADD CONSTRAINT object_data_object_class_object_id_key UNIQUE (object_class, object_id);


--
-- Name: object_data_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY object_data
    ADD CONSTRAINT object_data_pkey PRIMARY KEY (uuid);


--
-- Name: object_mutation_object_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY object_mutation
    ADD CONSTRAINT object_mutation_object_uuid_key UNIQUE (object_uuid);


--
-- Name: object_mutation_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY object_mutation
    ADD CONSTRAINT object_mutation_pkey PRIMARY KEY (id);


--
-- Name: object_relation_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY object_relation
    ADD CONSTRAINT object_relation_pkey PRIMARY KEY (id);


--
-- Name: object_relationships_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY object_relationships
    ADD CONSTRAINT object_relationships_pkey PRIMARY KEY (uuid);


--
-- Name: object_subscription_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY object_subscription
    ADD CONSTRAINT object_subscription_pkey PRIMARY KEY (id);


--
-- Name: object_type_bibliotheek_entry_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY object_bibliotheek_entry
    ADD CONSTRAINT object_type_bibliotheek_entry_pkey PRIMARY KEY (id);


--
-- Name: parameter_unique; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY config
    ADD CONSTRAINT parameter_unique UNIQUE (parameter);


--
-- Name: parkeergebied_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY parkeergebied
    ADD CONSTRAINT parkeergebied_pkey PRIMARY KEY (id);


--
-- Name: pk_ligplaats_nevenadres; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY bag_ligplaats_nevenadres
    ADD CONSTRAINT pk_ligplaats_nevenadres PRIMARY KEY (identificatie, begindatum, correctie, nevenadres);


--
-- Name: queue_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY queue
    ADD CONSTRAINT queue_pkey PRIMARY KEY (id);


--
-- Name: remote_api_keys_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY remote_api_keys
    ADD CONSTRAINT remote_api_keys_pkey PRIMARY KEY (id);


--
-- Name: roles_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- Name: sbus_logging_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY sbus_logging
    ADD CONSTRAINT sbus_logging_pkey PRIMARY KEY (id);


--
-- Name: sbus_traffic_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY sbus_traffic
    ADD CONSTRAINT sbus_traffic_pkey PRIMARY KEY (id);


--
-- Name: scheduled_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY scheduled_jobs
    ADD CONSTRAINT scheduled_jobs_pkey PRIMARY KEY (id);


--
-- Name: scheduled_jobs_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY scheduled_jobs
    ADD CONSTRAINT scheduled_jobs_uuid_key UNIQUE (uuid);


--
-- Name: search_query_delen_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY search_query_delen
    ADD CONSTRAINT search_query_delen_pkey PRIMARY KEY (id);


--
-- Name: search_query_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY search_query
    ADD CONSTRAINT search_query_pkey PRIMARY KEY (id);


--
-- Name: settings_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY settings
    ADD CONSTRAINT settings_pkey PRIMARY KEY (id);


--
-- Name: sort_order_unique; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY bibliotheek_kenmerken_values
    ADD CONSTRAINT sort_order_unique UNIQUE (sort_order, bibliotheek_kenmerken_id);


--
-- Name: subject_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY subject
    ADD CONSTRAINT subject_pkey PRIMARY KEY (id);


--
-- Name: subject_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY subject
    ADD CONSTRAINT subject_uuid_key UNIQUE (uuid);


--
-- Name: transaction_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY transaction
    ADD CONSTRAINT transaction_pkey PRIMARY KEY (id);


--
-- Name: transaction_record_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY transaction_record
    ADD CONSTRAINT transaction_record_pkey PRIMARY KEY (id);


--
-- Name: transaction_record_to_object_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY transaction_record_to_object
    ADD CONSTRAINT transaction_record_to_object_pkey PRIMARY KEY (id);


--
-- Name: user_app_lock_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY user_app_lock
    ADD CONSTRAINT user_app_lock_pkey PRIMARY KEY (uidnumber, type, type_id);


--
-- Name: user_entity_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY user_entity
    ADD CONSTRAINT user_entity_pkey PRIMARY KEY (id);


--
-- Name: woz_objects_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY woz_objects
    ADD CONSTRAINT woz_objects_pkey PRIMARY KEY (id);


--
-- Name: zaak_authorisation_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY zaak_authorisation
    ADD CONSTRAINT zaak_authorisation_pkey PRIMARY KEY (id);


--
-- Name: zaak_bag_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY zaak_bag
    ADD CONSTRAINT zaak_bag_pkey PRIMARY KEY (id);


--
-- Name: zaak_betrokkenen_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY zaak_betrokkenen
    ADD CONSTRAINT zaak_betrokkenen_pkey PRIMARY KEY (id);


--
-- Name: zaak_duplicate_prevention_token_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY zaak
    ADD CONSTRAINT zaak_duplicate_prevention_token_key UNIQUE (duplicate_prevention_token);


--
-- Name: zaak_kenmerk_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY zaak_kenmerk
    ADD CONSTRAINT zaak_kenmerk_pkey PRIMARY KEY (id);


--
-- Name: zaak_meta_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY zaak_meta
    ADD CONSTRAINT zaak_meta_pkey PRIMARY KEY (id);


--
-- Name: zaak_onafgerond_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY zaak_onafgerond
    ADD CONSTRAINT zaak_onafgerond_pkey PRIMARY KEY (zaaktype_id, betrokkene);


--
-- Name: zaak_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY zaak
    ADD CONSTRAINT zaak_pkey PRIMARY KEY (id);


--
-- Name: zaak_subcase_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY zaak_subcase
    ADD CONSTRAINT zaak_subcase_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_authorisation_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY zaaktype_authorisation
    ADD CONSTRAINT zaaktype_authorisation_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_betrokkenen_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY zaaktype_betrokkenen
    ADD CONSTRAINT zaaktype_betrokkenen_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_checklist_item_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY zaaktype_status_checklist_item
    ADD CONSTRAINT zaaktype_checklist_item_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_definitie_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY zaaktype_definitie
    ADD CONSTRAINT zaaktype_definitie_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_kenmerken_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY zaaktype_kenmerken
    ADD CONSTRAINT zaaktype_kenmerken_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_node_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY zaaktype_node
    ADD CONSTRAINT zaaktype_node_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_notificatie_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY zaaktype_notificatie
    ADD CONSTRAINT zaaktype_notificatie_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY zaaktype
    ADD CONSTRAINT zaaktype_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_regel_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY zaaktype_regel
    ADD CONSTRAINT zaaktype_regel_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_relatie_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY zaaktype_relatie
    ADD CONSTRAINT zaaktype_relatie_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_resultaten_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY zaaktype_resultaten
    ADD CONSTRAINT zaaktype_resultaten_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_sjablonen_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY zaaktype_sjablonen
    ADD CONSTRAINT zaaktype_sjablonen_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_standaard_betrokkenen_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY zaaktype_standaard_betrokkenen
    ADD CONSTRAINT zaaktype_standaard_betrokkenen_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_status_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY zaaktype_status
    ADD CONSTRAINT zaaktype_status_pkey PRIMARY KEY (id);


--
-- Name: beheer_import_log_idx_import_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX beheer_import_log_idx_import_id ON beheer_import_log USING btree (import_id);


--
-- Name: bibliotheek_kenmerken_values_idx_bibliotheek_kenmerken_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX bibliotheek_kenmerken_values_idx_bibliotheek_kenmerken_id ON bibliotheek_kenmerken_values USING btree (bibliotheek_kenmerken_id);


--
-- Name: bibliotheek_sjablonen_interface_id_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX bibliotheek_sjablonen_interface_id_idx ON bibliotheek_sjablonen USING btree (interface_id);


--
-- Name: bibliotheek_sjablonen_template_uuid_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX bibliotheek_sjablonen_template_uuid_idx ON bibliotheek_sjablonen USING btree (template_external_name);


--
-- Name: contactmoment_uuid_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX contactmoment_uuid_idx ON contactmoment USING btree (uuid);


--
-- Name: gm_natuurlijk_persoon_idx_adres_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX gm_natuurlijk_persoon_idx_adres_id ON gm_natuurlijk_persoon USING btree (adres_id);


--
-- Name: interface_uuid_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX interface_uuid_idx ON interface USING btree (uuid);


--
-- Name: logging_component_id_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX logging_component_id_idx ON logging USING btree (component_id) WHERE (component_id IS NOT NULL);


--
-- Name: logging_component_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX logging_component_idx ON logging USING btree (component) WHERE (component IS NOT NULL);


--
-- Name: logging_restricted_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX logging_restricted_idx ON logging USING btree (restricted);


--
-- Name: logging_zaak_id_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX logging_zaak_id_idx ON logging USING btree (zaak_id);


--
-- Name: message_subject_archived_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX message_subject_archived_idx ON message USING btree (subject_id, is_archived);


--
-- Name: message_subject_read_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX message_subject_read_idx ON message USING btree (subject_id, is_read);


--
-- Name: natuurlijk_persoon_burgerservicenummer; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX natuurlijk_persoon_burgerservicenummer ON natuurlijk_persoon USING btree (burgerservicenummer);


--
-- Name: natuurlijk_persoon_idx_adres_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX natuurlijk_persoon_idx_adres_id ON natuurlijk_persoon USING btree (adres_id);


--
-- Name: object_acl_entity_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX object_acl_entity_idx ON object_acl_entry USING btree (entity_type, entity_id);


--
-- Name: object_acl_entry_groupname_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX object_acl_entry_groupname_idx ON object_acl_entry USING btree (groupname);


--
-- Name: object_acl_entry_permission_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX object_acl_entry_permission_idx ON object_acl_entry USING btree (capability);


--
-- Name: object_acl_object_uuid_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX object_acl_object_uuid_idx ON object_acl_entry USING btree (object_uuid);


--
-- Name: object_data_assignee_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX object_data_assignee_idx ON object_data USING btree (((index_hstore -> 'case.assignee'::text)));


--
-- Name: object_data_case_casetype_id_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX object_data_case_casetype_id_idx ON object_data USING btree (((index_hstore -> 'case.casetype.id'::text)));


--
-- Name: object_data_case_number_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX object_data_case_number_idx ON object_data USING btree (((index_hstore -> 'case.number'::text)));


--
-- Name: object_data_case_phase_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX object_data_case_phase_idx ON object_data USING btree (((index_hstore -> 'case.phase'::text)));


--
-- Name: object_data_case_requestor_id_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX object_data_case_requestor_id_idx ON object_data USING btree (((index_hstore -> 'case.requestor.id'::text)));


--
-- Name: object_data_casetype_description_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX object_data_casetype_description_idx ON object_data USING btree (((index_hstore -> 'casetype.description'::text)));


--
-- Name: object_data_casetype_name_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX object_data_casetype_name_idx ON object_data USING btree (((index_hstore -> 'casetype.name'::text)));


--
-- Name: object_data_completion_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX object_data_completion_idx ON object_data USING btree (((index_hstore -> 'case.date_of_completion'::text)));


--
-- Name: object_data_hstore_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX object_data_hstore_idx ON object_data USING gin (index_hstore);


--
-- Name: object_data_recipient_name_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX object_data_recipient_name_idx ON object_data USING btree (((index_hstore -> 'recipient.full_name'::text)));


--
-- Name: object_data_regdate_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX object_data_regdate_idx ON object_data USING btree (hstore_to_timestamp(((index_hstore -> 'case.date_of_registration'::text))::character varying));


--
-- Name: object_data_target_date_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX object_data_target_date_idx ON object_data USING btree (hstore_to_timestamp(((index_hstore -> 'case.date_target'::text))::character varying));


--
-- Name: object_text_vector_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX object_text_vector_idx ON object_data USING gin (text_vector);


--
-- Name: scheduled_jobs_case_id_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX scheduled_jobs_case_id_idx ON scheduled_jobs USING btree (case_id);


--
-- Name: scheduled_jobs_task_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX scheduled_jobs_task_idx ON scheduled_jobs USING btree (task);


--
-- Name: transaction_external_transaction_id_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX transaction_external_transaction_id_idx ON transaction USING btree (external_transaction_id);


--
-- Name: transaction_record_uuid_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX transaction_record_uuid_idx ON transaction_record USING btree (uuid);


--
-- Name: transaction_text_vector_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX transaction_text_vector_idx ON transaction USING gist (text_vector);


--
-- Name: transaction_uuid_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX transaction_uuid_idx ON transaction USING btree (uuid);


--
-- Name: zaak_betrokkenen_gegevens_magazijn_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX zaak_betrokkenen_gegevens_magazijn_index ON zaak_betrokkenen USING btree (gegevens_magazijn_id);


--
-- Name: zaaktype_authorisation_idx_zaaktype_node_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX zaaktype_authorisation_idx_zaaktype_node_id ON zaaktype_authorisation USING btree (zaaktype_node_id);


--
-- Name: zaaktype_betrokkenen_idx_zaaktype_node_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX zaaktype_betrokkenen_idx_zaaktype_node_id ON zaaktype_betrokkenen USING btree (zaaktype_node_id);


--
-- Name: zaaktype_idx_zaaktype_node_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX zaaktype_idx_zaaktype_node_id ON zaaktype USING btree (zaaktype_node_id);


--
-- Name: zaaktype_node_idx_zaaktype_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX zaaktype_node_idx_zaaktype_id ON zaaktype_node USING btree (zaaktype_id);


--
-- Name: zaaktype_relatie_idx_relatie_zaaktype_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX zaaktype_relatie_idx_relatie_zaaktype_id ON zaaktype_relatie USING btree (relatie_zaaktype_id);


--
-- Name: zaaktype_relatie_idx_zaaktype_node_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX zaaktype_relatie_idx_zaaktype_node_id ON zaaktype_relatie USING btree (zaaktype_node_id);


--
-- Name: zaaktype_relatie_idx_zaaktype_status_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX zaaktype_relatie_idx_zaaktype_status_id ON zaaktype_relatie USING btree (zaaktype_status_id);


--
-- Name: zaaktype_resultaten_idx_zaaktype_node_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX zaaktype_resultaten_idx_zaaktype_node_id ON zaaktype_resultaten USING btree (zaaktype_node_id);


--
-- Name: zaaktype_resultaten_idx_zaaktype_status_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX zaaktype_resultaten_idx_zaaktype_status_id ON zaaktype_resultaten USING btree (zaaktype_status_id);


--
-- Name: zaaktype_status_idx_zaaktype_node_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX zaaktype_status_idx_zaaktype_node_id ON zaaktype_status USING btree (zaaktype_node_id);


--
-- Name: adres_natuurlijk_persoon_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY adres
    ADD CONSTRAINT adres_natuurlijk_persoon_id_fkey FOREIGN KEY (natuurlijk_persoon_id) REFERENCES natuurlijk_persoon(id);


--
-- Name: alternative_authentication_activation_link_subject_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY alternative_authentication_activation_link
    ADD CONSTRAINT alternative_authentication_activation_link_subject_id_fkey FOREIGN KEY (subject_id) REFERENCES subject(uuid);


--
-- Name: beheer_import_log_import_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY beheer_import_log
    ADD CONSTRAINT beheer_import_log_import_id_fkey FOREIGN KEY (import_id) REFERENCES beheer_import(id);


--
-- Name: bibliotheek_categorie_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY object_bibliotheek_entry
    ADD CONSTRAINT bibliotheek_categorie_fkey FOREIGN KEY (bibliotheek_categorie_id) REFERENCES bibliotheek_categorie(id) ON DELETE CASCADE;


--
-- Name: bibliotheek_categorie_pid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY bibliotheek_categorie
    ADD CONSTRAINT bibliotheek_categorie_pid_fkey FOREIGN KEY (pid) REFERENCES bibliotheek_categorie(id);


--
-- Name: bibliotheek_kenmerken_bibliotheek_categorie_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY bibliotheek_kenmerken
    ADD CONSTRAINT bibliotheek_kenmerken_bibliotheek_categorie_id_fkey FOREIGN KEY (bibliotheek_categorie_id) REFERENCES bibliotheek_categorie(id);


--
-- Name: bibliotheek_kenmerken_file_metadata_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY bibliotheek_kenmerken
    ADD CONSTRAINT bibliotheek_kenmerken_file_metadata_id_fkey FOREIGN KEY (file_metadata_id) REFERENCES file_metadata(id);


--
-- Name: bibliotheek_kenmerken_values_bibliotheek_kenmerken_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY bibliotheek_kenmerken_values
    ADD CONSTRAINT bibliotheek_kenmerken_values_bibliotheek_kenmerken_id_fkey FOREIGN KEY (bibliotheek_kenmerken_id) REFERENCES bibliotheek_kenmerken(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: bibliotheek_notificatie_kenmerk_bibliotheek_kenmerken_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY bibliotheek_notificatie_kenmerk
    ADD CONSTRAINT bibliotheek_notificatie_kenmerk_bibliotheek_kenmerken_id_fkey FOREIGN KEY (bibliotheek_kenmerken_id) REFERENCES bibliotheek_kenmerken(id);


--
-- Name: bibliotheek_notificatie_kenmerk_bibliotheek_notificatie_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY bibliotheek_notificatie_kenmerk
    ADD CONSTRAINT bibliotheek_notificatie_kenmerk_bibliotheek_notificatie_id_fkey FOREIGN KEY (bibliotheek_notificatie_id) REFERENCES bibliotheek_notificaties(id);


--
-- Name: bibliotheek_notificaties_bibliotheek_categorie_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY bibliotheek_notificaties
    ADD CONSTRAINT bibliotheek_notificaties_bibliotheek_categorie_id_fkey FOREIGN KEY (bibliotheek_categorie_id) REFERENCES bibliotheek_categorie(id);


--
-- Name: bibliotheek_sjablonen_bibliotheek_categorie_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY bibliotheek_sjablonen
    ADD CONSTRAINT bibliotheek_sjablonen_bibliotheek_categorie_id_fkey FOREIGN KEY (bibliotheek_categorie_id) REFERENCES bibliotheek_categorie(id);


--
-- Name: bibliotheek_sjablonen_filestore_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY bibliotheek_sjablonen
    ADD CONSTRAINT bibliotheek_sjablonen_filestore_id_fkey FOREIGN KEY (filestore_id) REFERENCES filestore(id);


--
-- Name: bibliotheek_sjablonen_interface_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY bibliotheek_sjablonen
    ADD CONSTRAINT bibliotheek_sjablonen_interface_id_fkey FOREIGN KEY (interface_id) REFERENCES interface(id);


--
-- Name: bibliotheek_sjablonen_magic_strin_bibliotheek_sjablonen_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY bibliotheek_sjablonen_magic_string
    ADD CONSTRAINT bibliotheek_sjablonen_magic_strin_bibliotheek_sjablonen_id_fkey FOREIGN KEY (bibliotheek_sjablonen_id) REFERENCES bibliotheek_sjablonen(id);


--
-- Name: case_actions_case_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY case_action
    ADD CONSTRAINT case_actions_case_id_fkey FOREIGN KEY (case_id) REFERENCES zaak(id);


--
-- Name: case_actions_casetype_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY case_action
    ADD CONSTRAINT case_actions_casetype_status_id_fkey FOREIGN KEY (casetype_status_id) REFERENCES zaaktype_status(id);


--
-- Name: case_relation_case_id_a_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY case_relation
    ADD CONSTRAINT case_relation_case_id_a_fkey FOREIGN KEY (case_id_a) REFERENCES zaak(id);


--
-- Name: case_relation_case_id_b_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY case_relation
    ADD CONSTRAINT case_relation_case_id_b_fkey FOREIGN KEY (case_id_b) REFERENCES zaak(id);


--
-- Name: checklist_case_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY checklist
    ADD CONSTRAINT checklist_case_id_fkey FOREIGN KEY (case_id) REFERENCES zaak(id);


--
-- Name: checklist_item_checklist_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY checklist_item
    ADD CONSTRAINT checklist_item_checklist_id_fkey FOREIGN KEY (checklist_id) REFERENCES checklist(id);


--
-- Name: contactmoment_case_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY contactmoment
    ADD CONSTRAINT contactmoment_case_id_fkey FOREIGN KEY (case_id) REFERENCES zaak(id);


--
-- Name: contactmoment_email_contactmoment_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY contactmoment_email
    ADD CONSTRAINT contactmoment_email_contactmoment_id_fkey FOREIGN KEY (contactmoment_id) REFERENCES contactmoment(id);


--
-- Name: contactmoment_email_filestore_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY contactmoment_email
    ADD CONSTRAINT contactmoment_email_filestore_id_fkey FOREIGN KEY (filestore_id) REFERENCES filestore(id);


--
-- Name: contactmoment_note_contactmoment_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY contactmoment_note
    ADD CONSTRAINT contactmoment_note_contactmoment_id_fkey FOREIGN KEY (contactmoment_id) REFERENCES contactmoment(id);


--
-- Name: directory_case_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY directory
    ADD CONSTRAINT directory_case_id_fkey FOREIGN KEY (case_id) REFERENCES zaak(id);


--
-- Name: file_annotation_file_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY file_annotation
    ADD CONSTRAINT file_annotation_file_id_fkey FOREIGN KEY (file_id) REFERENCES file(id);


--
-- Name: file_case_document_case_document_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY file_case_document
    ADD CONSTRAINT file_case_document_case_document_id_fkey FOREIGN KEY (case_document_id) REFERENCES zaaktype_kenmerken(id);


--
-- Name: file_case_document_file_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY file_case_document
    ADD CONSTRAINT file_case_document_file_id_fkey FOREIGN KEY (file_id) REFERENCES file(id);


--
-- Name: file_case_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY file
    ADD CONSTRAINT file_case_id_fkey FOREIGN KEY (case_id) REFERENCES zaak(id);


--
-- Name: file_directory_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY file
    ADD CONSTRAINT file_directory_id_fkey FOREIGN KEY (directory_id) REFERENCES directory(id);


--
-- Name: file_filestore_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY file
    ADD CONSTRAINT file_filestore_id_fkey FOREIGN KEY (filestore_id) REFERENCES filestore(id);


--
-- Name: file_is_duplicate_of_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY file
    ADD CONSTRAINT file_is_duplicate_of_fkey FOREIGN KEY (is_duplicate_of) REFERENCES file(id) ON DELETE SET NULL;


--
-- Name: file_metadata_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY file
    ADD CONSTRAINT file_metadata_id_fkey FOREIGN KEY (metadata_id) REFERENCES file_metadata(id);


--
-- Name: file_root_file_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY file
    ADD CONSTRAINT file_root_file_id_fkey FOREIGN KEY (root_file_id) REFERENCES file(id);


--
-- Name: file_scheduled_jobs_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY file
    ADD CONSTRAINT file_scheduled_jobs_id_fkey FOREIGN KEY (scheduled_jobs_id) REFERENCES scheduled_jobs(id);


--
-- Name: file_thumbnail_file_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY file_derivative
    ADD CONSTRAINT file_thumbnail_file_id_fkey FOREIGN KEY (file_id) REFERENCES file(id);


--
-- Name: file_thumbnail_filestore_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY file_derivative
    ADD CONSTRAINT file_thumbnail_filestore_id_fkey FOREIGN KEY (filestore_id) REFERENCES filestore(id);


--
-- Name: gegevensmagazijn_subjecten_nnp_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY gegevensmagazijn_subjecten
    ADD CONSTRAINT gegevensmagazijn_subjecten_nnp_uuid_fkey FOREIGN KEY (nnp_uuid) REFERENCES bedrijf(uuid);


--
-- Name: gegevensmagazijn_subjecten_np_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY gegevensmagazijn_subjecten
    ADD CONSTRAINT gegevensmagazijn_subjecten_np_uuid_fkey FOREIGN KEY (np_uuid) REFERENCES natuurlijk_persoon(uuid);


--
-- Name: gegevensmagazijn_subjecten_subject_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY gegevensmagazijn_subjecten
    ADD CONSTRAINT gegevensmagazijn_subjecten_subject_uuid_fkey FOREIGN KEY (subject_uuid) REFERENCES subject(uuid);


--
-- Name: gm_adres_natuurlijk_persoon_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY gm_adres
    ADD CONSTRAINT gm_adres_natuurlijk_persoon_id_fkey FOREIGN KEY (natuurlijk_persoon_id) REFERENCES gm_natuurlijk_persoon(id);


--
-- Name: gm_natuurlijk_persoon_adres_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY gm_natuurlijk_persoon
    ADD CONSTRAINT gm_natuurlijk_persoon_adres_id_fkey FOREIGN KEY (adres_id) REFERENCES gm_adres(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: interface_case_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY interface
    ADD CONSTRAINT interface_case_type_id_fkey FOREIGN KEY (case_type_id) REFERENCES zaaktype(id);


--
-- Name: interface_objecttype_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY interface
    ADD CONSTRAINT interface_objecttype_id_fkey FOREIGN KEY (objecttype_id) REFERENCES object_data(uuid) ON DELETE SET NULL;


--
-- Name: logging_object_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY logging
    ADD CONSTRAINT logging_object_uuid_fkey FOREIGN KEY (object_uuid) REFERENCES object_data(uuid) ON DELETE SET NULL;


--
-- Name: message_logging_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY message
    ADD CONSTRAINT message_logging_id_fkey FOREIGN KEY (logging_id) REFERENCES logging(id);


--
-- Name: natuurlijk_persoon_adres_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY natuurlijk_persoon
    ADD CONSTRAINT natuurlijk_persoon_adres_id_fk FOREIGN KEY (adres_id) REFERENCES adres(id);


--
-- Name: object_data; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY object_bibliotheek_entry
    ADD CONSTRAINT object_data FOREIGN KEY (object_uuid) REFERENCES object_data(uuid) ON DELETE CASCADE;


--
-- Name: object_data_class_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY object_data
    ADD CONSTRAINT object_data_class_uuid_fkey FOREIGN KEY (class_uuid) REFERENCES object_data(uuid);


--
-- Name: object_data_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY object_acl_entry
    ADD CONSTRAINT object_data_fkey FOREIGN KEY (object_uuid) REFERENCES object_data(uuid) ON DELETE CASCADE;


--
-- Name: object_mutation_lock_object_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY object_mutation
    ADD CONSTRAINT object_mutation_lock_object_uuid_fkey FOREIGN KEY (lock_object_uuid) REFERENCES object_data(uuid);


--
-- Name: object_mutation_object_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY object_mutation
    ADD CONSTRAINT object_mutation_object_uuid_fkey FOREIGN KEY (object_uuid) REFERENCES object_data(uuid) ON DELETE CASCADE;


--
-- Name: object_mutation_subject_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY object_mutation
    ADD CONSTRAINT object_mutation_subject_id_fkey FOREIGN KEY (subject_id) REFERENCES subject(id);


--
-- Name: object_relation_object_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY object_relation
    ADD CONSTRAINT object_relation_object_id_fkey FOREIGN KEY (object_id) REFERENCES object_data(uuid);


--
-- Name: object_relation_object_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY object_relation
    ADD CONSTRAINT object_relation_object_uuid_fkey FOREIGN KEY (object_uuid) REFERENCES object_data(uuid);


--
-- Name: object_relationships_object1_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY object_relationships
    ADD CONSTRAINT object_relationships_object1_uuid_fkey FOREIGN KEY (object1_uuid) REFERENCES object_data(uuid) ON DELETE CASCADE;


--
-- Name: object_relationships_object2_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY object_relationships
    ADD CONSTRAINT object_relationships_object2_uuid_fkey FOREIGN KEY (object2_uuid) REFERENCES object_data(uuid) ON DELETE CASCADE;


--
-- Name: object_subscription_config_interface_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY object_subscription
    ADD CONSTRAINT object_subscription_config_interface_id_fkey FOREIGN KEY (config_interface_id) REFERENCES interface(id);


--
-- Name: object_subscription_interface_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY object_subscription
    ADD CONSTRAINT object_subscription_interface_id_fkey FOREIGN KEY (interface_id) REFERENCES interface(id);


--
-- Name: queue_object_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY queue
    ADD CONSTRAINT queue_object_id_fkey FOREIGN KEY (object_id) REFERENCES object_data(uuid) ON DELETE CASCADE DEFERRABLE;


--
-- Name: queue_parent_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY queue
    ADD CONSTRAINT queue_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES queue(id);


--
-- Name: roles_parent_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY roles
    ADD CONSTRAINT roles_parent_group_id_fkey FOREIGN KEY (parent_group_id) REFERENCES groups(id);


--
-- Name: sbus_logging_pid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY sbus_logging
    ADD CONSTRAINT sbus_logging_pid_fkey FOREIGN KEY (pid) REFERENCES sbus_logging(id);


--
-- Name: sbus_logging_sbus_traffic_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY sbus_logging
    ADD CONSTRAINT sbus_logging_sbus_traffic_id_fkey FOREIGN KEY (sbus_traffic_id) REFERENCES sbus_traffic(id);


--
-- Name: scheduled_jobs_case_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY scheduled_jobs
    ADD CONSTRAINT scheduled_jobs_case_id_fkey FOREIGN KEY (case_id) REFERENCES zaak(id);


--
-- Name: search_query_delen_search_query_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY search_query_delen
    ADD CONSTRAINT search_query_delen_search_query_id_fkey FOREIGN KEY (search_query_id) REFERENCES search_query(id);


--
-- Name: transaction_input_file_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY transaction
    ADD CONSTRAINT transaction_input_file_fkey FOREIGN KEY (input_file) REFERENCES filestore(id);


--
-- Name: transaction_interface_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY transaction
    ADD CONSTRAINT transaction_interface_id_fkey FOREIGN KEY (interface_id) REFERENCES interface(id);


--
-- Name: transaction_record_to_object_transaction_record_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY transaction_record_to_object
    ADD CONSTRAINT transaction_record_to_object_transaction_record_id_fkey FOREIGN KEY (transaction_record_id) REFERENCES transaction_record(id);


--
-- Name: transaction_record_transaction_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY transaction_record
    ADD CONSTRAINT transaction_record_transaction_id_fkey FOREIGN KEY (transaction_id) REFERENCES transaction(id);


--
-- Name: user_entity_source_interface_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_entity
    ADD CONSTRAINT user_entity_source_interface_id_fkey FOREIGN KEY (source_interface_id) REFERENCES interface(id);


--
-- Name: user_entity_subject_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_entity
    ADD CONSTRAINT user_entity_subject_id_fkey FOREIGN KEY (subject_id) REFERENCES subject(id);


--
-- Name: zaak_aanvrager_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaak
    ADD CONSTRAINT zaak_aanvrager_fkey FOREIGN KEY (aanvrager) REFERENCES zaak_betrokkenen(id);


--
-- Name: zaak_authorisation_zaak_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaak_authorisation
    ADD CONSTRAINT zaak_authorisation_zaak_id_fkey FOREIGN KEY (zaak_id) REFERENCES zaak(id);


--
-- Name: zaak_bag_pid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaak_bag
    ADD CONSTRAINT zaak_bag_pid_fkey FOREIGN KEY (pid) REFERENCES zaak_bag(id);


--
-- Name: zaak_bag_zaak_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaak_bag
    ADD CONSTRAINT zaak_bag_zaak_id_fkey FOREIGN KEY (zaak_id) REFERENCES zaak(id);


--
-- Name: zaak_behandelaar_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaak
    ADD CONSTRAINT zaak_behandelaar_fkey FOREIGN KEY (behandelaar) REFERENCES zaak_betrokkenen(id);


--
-- Name: zaak_betrokkenen_zaak_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaak_betrokkenen
    ADD CONSTRAINT zaak_betrokkenen_zaak_id_fkey FOREIGN KEY (zaak_id) REFERENCES zaak(id);


--
-- Name: zaak_coordinator_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaak
    ADD CONSTRAINT zaak_coordinator_fkey FOREIGN KEY (coordinator) REFERENCES zaak_betrokkenen(id);


--
-- Name: zaak_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY logging
    ADD CONSTRAINT zaak_id_fkey FOREIGN KEY (zaak_id) REFERENCES zaak(id);


--
-- Name: zaak_kenmerk_bibliotheek_kenmerken_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaak_kenmerk
    ADD CONSTRAINT zaak_kenmerk_bibliotheek_kenmerken_id_fkey FOREIGN KEY (bibliotheek_kenmerken_id) REFERENCES bibliotheek_kenmerken(id);


--
-- Name: zaak_kenmerk_zaak_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaak_kenmerk
    ADD CONSTRAINT zaak_kenmerk_zaak_id_fkey FOREIGN KEY (zaak_id) REFERENCES zaak(id);


--
-- Name: zaak_locatie_correspondentie_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaak
    ADD CONSTRAINT zaak_locatie_correspondentie_fkey FOREIGN KEY (locatie_correspondentie) REFERENCES zaak_bag(id);


--
-- Name: zaak_locatie_zaak_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaak
    ADD CONSTRAINT zaak_locatie_zaak_fkey FOREIGN KEY (locatie_zaak) REFERENCES zaak_bag(id);


--
-- Name: zaak_meta_zaak_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaak_meta
    ADD CONSTRAINT zaak_meta_zaak_id_fkey FOREIGN KEY (zaak_id) REFERENCES zaak(id);


--
-- Name: zaak_pid_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaak
    ADD CONSTRAINT zaak_pid_fk FOREIGN KEY (pid) REFERENCES zaak(id);


--
-- Name: zaak_pid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaak
    ADD CONSTRAINT zaak_pid_fkey FOREIGN KEY (pid) REFERENCES zaak(id);


--
-- Name: zaak_relates_to_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaak
    ADD CONSTRAINT zaak_relates_to_fkey FOREIGN KEY (relates_to) REFERENCES zaak(id);


--
-- Name: zaak_subcase_relation_zaak_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaak_subcase
    ADD CONSTRAINT zaak_subcase_relation_zaak_id_fkey FOREIGN KEY (relation_zaak_id) REFERENCES zaak(id);


--
-- Name: zaak_subcase_zaak_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaak_subcase
    ADD CONSTRAINT zaak_subcase_zaak_id_fkey FOREIGN KEY (zaak_id) REFERENCES zaak(id);


--
-- Name: zaak_vervolg_van_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaak
    ADD CONSTRAINT zaak_vervolg_van_fkey FOREIGN KEY (vervolg_van) REFERENCES zaak(id);


--
-- Name: zaak_zaaktype_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaak
    ADD CONSTRAINT zaak_zaaktype_id_fkey FOREIGN KEY (zaaktype_id) REFERENCES zaaktype(id);


--
-- Name: zaak_zaaktype_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaak_onafgerond
    ADD CONSTRAINT zaak_zaaktype_id_fkey FOREIGN KEY (zaaktype_id) REFERENCES zaaktype(id);


--
-- Name: zaak_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaak
    ADD CONSTRAINT zaak_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES zaaktype_node(id);


--
-- Name: zaaktype_authorisation_zaaktype_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaaktype_authorisation
    ADD CONSTRAINT zaaktype_authorisation_zaaktype_id_fkey FOREIGN KEY (zaaktype_id) REFERENCES zaaktype(id);


--
-- Name: zaaktype_authorisation_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaaktype_authorisation
    ADD CONSTRAINT zaaktype_authorisation_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES zaaktype_node(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: zaaktype_betrokkenen_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaaktype_betrokkenen
    ADD CONSTRAINT zaaktype_betrokkenen_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES zaaktype_node(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: zaaktype_bibliotheek_categorie_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaaktype
    ADD CONSTRAINT zaaktype_bibliotheek_categorie_id_fkey FOREIGN KEY (bibliotheek_categorie_id) REFERENCES bibliotheek_categorie(id) DEFERRABLE;


--
-- Name: zaaktype_kenmerken_bibliotheek_kenmerken_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaaktype_kenmerken
    ADD CONSTRAINT zaaktype_kenmerken_bibliotheek_kenmerken_id_fkey FOREIGN KEY (bibliotheek_kenmerken_id) REFERENCES bibliotheek_kenmerken(id);


--
-- Name: zaaktype_kenmerken_object_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaaktype_kenmerken
    ADD CONSTRAINT zaaktype_kenmerken_object_id_fkey FOREIGN KEY (object_id) REFERENCES object_data(uuid) ON DELETE RESTRICT;


--
-- Name: zaaktype_kenmerken_zaak_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaaktype_kenmerken
    ADD CONSTRAINT zaaktype_kenmerken_zaak_status_id_fkey FOREIGN KEY (zaak_status_id) REFERENCES zaaktype_status(id);


--
-- Name: zaaktype_kenmerken_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaaktype_kenmerken
    ADD CONSTRAINT zaaktype_kenmerken_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES zaaktype_node(id);


--
-- Name: zaaktype_node_zaaktype_definitie_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaaktype_node
    ADD CONSTRAINT zaaktype_node_zaaktype_definitie_id_fkey FOREIGN KEY (zaaktype_definitie_id) REFERENCES zaaktype_definitie(id) DEFERRABLE;


--
-- Name: zaaktype_node_zaaktype_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaaktype_node
    ADD CONSTRAINT zaaktype_node_zaaktype_id_fkey FOREIGN KEY (zaaktype_id) REFERENCES zaaktype(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: zaaktype_notificatie_bibliotheek_notificaties_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaaktype_notificatie
    ADD CONSTRAINT zaaktype_notificatie_bibliotheek_notificaties_id_fkey FOREIGN KEY (bibliotheek_notificaties_id) REFERENCES bibliotheek_notificaties(id);


--
-- Name: zaaktype_notificatie_zaak_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaaktype_notificatie
    ADD CONSTRAINT zaaktype_notificatie_zaak_status_id_fkey FOREIGN KEY (zaak_status_id) REFERENCES zaaktype_status(id);


--
-- Name: zaaktype_notificatie_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaaktype_notificatie
    ADD CONSTRAINT zaaktype_notificatie_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES zaaktype_node(id);


--
-- Name: zaaktype_regel_zaak_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaaktype_regel
    ADD CONSTRAINT zaaktype_regel_zaak_status_id_fkey FOREIGN KEY (zaak_status_id) REFERENCES zaaktype_status(id);


--
-- Name: zaaktype_regel_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaaktype_regel
    ADD CONSTRAINT zaaktype_regel_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES zaaktype_node(id);


--
-- Name: zaaktype_relatie_relatie_zaaktype_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaaktype_relatie
    ADD CONSTRAINT zaaktype_relatie_relatie_zaaktype_id_fkey FOREIGN KEY (relatie_zaaktype_id) REFERENCES zaaktype(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: zaaktype_relatie_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaaktype_relatie
    ADD CONSTRAINT zaaktype_relatie_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES zaaktype_node(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: zaaktype_relatie_zaaktype_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaaktype_relatie
    ADD CONSTRAINT zaaktype_relatie_zaaktype_status_id_fkey FOREIGN KEY (zaaktype_status_id) REFERENCES zaaktype_status(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: zaaktype_resultaten_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaaktype_resultaten
    ADD CONSTRAINT zaaktype_resultaten_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES zaaktype_node(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: zaaktype_resultaten_zaaktype_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaaktype_resultaten
    ADD CONSTRAINT zaaktype_resultaten_zaaktype_status_id_fkey FOREIGN KEY (zaaktype_status_id) REFERENCES zaaktype_status(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: zaaktype_sjablonen_bibliotheek_kenmerken_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaaktype_sjablonen
    ADD CONSTRAINT zaaktype_sjablonen_bibliotheek_kenmerken_id_fkey FOREIGN KEY (bibliotheek_kenmerken_id) REFERENCES bibliotheek_kenmerken(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: zaaktype_sjablonen_bibliotheek_sjablonen_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaaktype_sjablonen
    ADD CONSTRAINT zaaktype_sjablonen_bibliotheek_sjablonen_id_fkey FOREIGN KEY (bibliotheek_sjablonen_id) REFERENCES bibliotheek_sjablonen(id);


--
-- Name: zaaktype_sjablonen_zaak_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaaktype_sjablonen
    ADD CONSTRAINT zaaktype_sjablonen_zaak_status_id_fkey FOREIGN KEY (zaak_status_id) REFERENCES zaaktype_status(id);


--
-- Name: zaaktype_sjablonen_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaaktype_sjablonen
    ADD CONSTRAINT zaaktype_sjablonen_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES zaaktype_node(id);


--
-- Name: zaaktype_standaard_betrokkenen_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaaktype_standaard_betrokkenen
    ADD CONSTRAINT zaaktype_standaard_betrokkenen_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES zaaktype_node(id);


--
-- Name: zaaktype_standaard_betrokkenen_zaaktype_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaaktype_standaard_betrokkenen
    ADD CONSTRAINT zaaktype_standaard_betrokkenen_zaaktype_status_id_fkey FOREIGN KEY (zaak_status_id) REFERENCES zaaktype_status(id);


--
-- Name: zaaktype_status_checklist_item_zaaktype_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaaktype_status_checklist_item
    ADD CONSTRAINT zaaktype_status_checklist_item_zaaktype_status_id_fkey FOREIGN KEY (casetype_status_id) REFERENCES zaaktype_status(id);


--
-- Name: zaaktype_status_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaaktype_status
    ADD CONSTRAINT zaaktype_status_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES zaaktype_node(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: zaaktype_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zaaktype
    ADD CONSTRAINT zaaktype_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES zaaktype_node(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: public; Type: ACL; Schema: -; Owner: -
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

