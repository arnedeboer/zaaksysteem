#! perl
use warnings;
use strict;

# Test header
use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
use Test::Moose;

# /Test header

$zs->zs_transaction_ok(sub {
    my $kenmerk = $zs->create_bibliotheek_kenmerk_ok;

    throws_ok(sub {
        $kenmerk->save_options;
    }, qr/Input not a HashRef, unable to assert profile/, 'Fails when called without params');

    throws_ok(sub {
        $kenmerk->save_options({
            options => 1,
            reason => []
        });
    }, qr/Een optie zonder waarde is niet toegestaan/, 'Fails when called with bad params');

    throws_ok(sub {
        $kenmerk->save_options({
            options => [{
                not_value => 1,
            }, {
                value => 'dsdsds',
            }],
        });
    }, qr/Een optie zonder waarde is niet toegestaan/, 'Fails when not all options have a value member');

}, 'Kenmerk with bad options');


$zs->zs_transaction_ok(sub {
    my $kenmerk = $zs->create_bibliotheek_kenmerk_ok(value_type => 'select');
    my $options = [
        { value => 'boter', active => 0 },
        { value => 'kaas', active => 1 },
        { value => 'eieren', active => 0 },
    ];

    $kenmerk->save_options({ options => $options });

    my $stored = $kenmerk->options;
    is scalar @$stored, 3, "Three options stored";
    for (0..2) {
        is $options->[$_]->{value}, $stored->[$_]->{value}, "Value has been stored correctly";
        is $options->[$_]->{active}, $stored->[$_]->{active}, "Active state has been stored correctly";
    }

}, 'BibliotheekKenmerken');

zs_done_testing();
