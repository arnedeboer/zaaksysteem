package TestFor::General::API::v1::Object::Session;
use base qw(ZSTest);

use TestSetup;

use Test::MockObject;
use URI;

use Zaaksysteem::API::v1::Object::Session;

=head1 NAME

TestFor::General:API::v1::Object::Session - Session information for this zaaksysteem

=head1 SYNOPSIS

    See USAGE tests

    Quick test of this single file from within vagrant
    ./zs_prove -v t/lib/TestFor/General/API/v1/Object/Session.pm

=head1 DESCRIPTION

Proves the Session object.

=head1 IMPLEMENTATION

Implemenation tests, public API of this object can be found in C<t/lib/TestFor/Catalyst/Controller/API/V1/Session.pm>

=head2

=cut

sub api_v1_session_construct : Tests {
    my $self    = shift;

    $zs->zs_transaction_ok(sub {
        my $c = Test::MockObject->new();

        $c->mock('user' => sub { $zs->set_current_user(); });
        $c->mock('user_exists' => sub { 1; });
        $c->mock('req'  => sub { 
            my $req = Test::MockObject->new();

            $req->mock('uri' => sub { URI->new('http://localhost/api/v1') });
        });
        $c->mock('config'  => sub { 
            return { gemeente_id => 'template'};
        });

        $c->mock('get_customer_info' => sub {
            return {
                naam        => 'Mintlab B.V.',
                naam_lang   => 'Mintlab B.V.',
                naam_kort   => 'mintlab',
                email       => 'info@mintlab.nl',
            }
        });

        my $session = Zaaksysteem::API::v1::Object::Session->new_from_catalyst($c);

        ok($session->logged_in_user, 'Found a logged in user');
        is($session->hostname, 'localhost', 'Found correct hostname');

        isa_ok($session->account, 'Zaaksysteem::API::v1::Object::Session::Account');

        ok ($session->account->$_, "Got value for $_") for qw/company email/;
    });
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
