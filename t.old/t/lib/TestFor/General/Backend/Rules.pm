package TestFor::General::Backend::Rules;
use base qw(ZSTest);

use TestSetup;

use Zaaksysteem::Backend::Rules;
use Zaaksysteem::Zaaktypen;
use JSON qw/decode_json encode_json/;


=head1 NAME

TestFor::General::Backend::Rules - Zaaksysteem Rules API Tests

=head1 SYNOPSIS

    ./zs_prove -v t/lib/TestFor/General/Backend/Rules.pm

=head1 LAYOUT

This file implements all USAGE tests, in other words: if you would like
to use the rules engine, here is your inspiration.

The files below this module, e.g.
L<TestFor::General::Backend::Rules::Rule::Condition::Zipcode>, will demonstrate
the specific usage of a condition or action, and also implements the backend
tests for the different internal functions.

=head1 USAGE (or end-to-end)

Usage tests: use the below tests when you would like to give the new
rules engine a spin.

=head2 new_from_case

Helper function for instantiating the engine with a case object (old style)

=cut

sub rules_usage_new_from_case : Tests {
    my $self            = shift;

    $zs->zs_transaction_ok(sub {
        my ($casetype, $zaaktype_status) = $self->_generate_rule_casetype;

        my $case     = $zs->create_case_ok(zaaktype => $casetype);

        # SleurEnPleur
        my $engine   = Zaaksysteem::Backend::Rules->new_from_case(
            $case,
        );
        # /SleurEnPleur

        is(@{ $engine->rules }, 3, 'Got correct amount of rules');

        my $first_rule = $engine->rules->[0];

        is(@{ $first_rule->conditions }, 1, 'Got correct amount of conditions');
        is(@{ $first_rule->then }, 1, 'Got correct amount of actions (then)');
        is(@{ $first_rule->else }, 0, 'Got correct amount of actions (else)');
    }, 'new_from_case: get rules');
}

=head2 new_from_casetype

Helper function for instantiating the engine with a casetype object

=cut

sub rules_usage_new_from_casetype : Tests {
    my $self            = shift;

    $zs->zs_transaction_ok(sub {
        my ($casetype, $zaaktype_status) = $self->_generate_rule_casetype;

        my $case     = $zs->create_case_ok(zaaktype => $casetype);

        # SleurEnPleur
        my $engine   = Zaaksysteem::Backend::Rules->new_from_casetype(
            $case->zaaktype_node_id,
            {
                'case.channel_of_contact'       => 'email',
                'case.confidentiality'          => 'public',
                'case.payment_status'           => undef,
                'case.requestor.house_number'   => 42,
                'case.requestor.subject_type'   => 'natuurlijk_persoon',
                'case.requestor.zipcode'        => '1011PZ'
            }
        );
        # /SleurEnPleur

        is(@{ $engine->rules }, 3, 'Got correct amount of rules');

        my $first_rule = $engine->rules->[0];

        is(@{ $first_rule->conditions }, 1, 'Got correct amount of conditions');
        is(@{ $first_rule->then }, 1, 'Got correct amount of actions (then)');
        is(@{ $first_rule->else }, 0, 'Got correct amount of actions (else)');
    }, 'new_from_casetype: get rules');
}

=head2 generate_object_params

Helper function for generating the helper params for the javascript frontend,
e.g., it will turn a case object into something like

    {
        'case.channel_of_contact'       => 'email',
        'case.confidentiality'          => 'public',
        'case.payment_status'           => undef,
        'case.requestor.house_number'   => 42,
        'case.requestor.subject_type'   => 'natuurlijk_persoon',
        'case.requestor.zipcode'        => '1011PZ'
    }

=cut

sub rules_usage_generate_object_params : Tests {
    my $self            = shift;
    my $valid_result    = {
        'case.casetype.designation_of_confidentiality' => '-',
        'case.casetype.extension' => 'Nee',
        'case.casetype.lex_silencio_positivo' => 'Nee',
        'case.casetype.node.id' => 626,
        'case.casetype.penalty' => 'Nee',
        'case.casetype.publication' => 'Nee',
        'case.casetype.registration_bag' => 'Nee',
        'case.casetype.suspension' => 'Nee',
        'case.casetype.wkpb' => 'Nee',,
        'case.casetype.objection_and_appeal' => 'Nee',
        'case.channel_of_contact' => 'email',
        'case.confidentiality' => 'public',
        'case.number_status' => undef,
        'case.payment_status' => undef,
        'case.requestor.house_number' => 42,
        'case.requestor.subject_type' => 'natuurlijk_persoon',
        'case.requestor.zipcode' => '1011PZ'
    };

    ### When you do not have a case object yet.
    $zs->zs_transaction_ok(sub {
        my ($casetype, $zaaktype_status) = $self->_generate_rule_casetype;

        my $case        = $zs->create_case_ok(zaaktype => $casetype);

        # SleurEnPleur
        my $params      = Zaaksysteem::Backend::Rules->generate_object_params(
            {
                'case.casetype.node'        => $case->zaaktype_node_id,
                'case.channel_of_contact'   => 'email',
                'case.requestor'            => $case->aanvrager_object
            }
        );
        # /SleurEnPleur

        local $valid_result->{'case.casetype.node.id'} = $case->zaaktype_node_id->id;
        is_deeply($params->{rule_params}, { %$valid_result, 'case.confidentiality' => undef }, 'Got correct params');


        # my $first_rule = $engine->rules->[0];
    }, 'generate_object_params: get params with casetype');

    ### When you got your case object.
    $zs->zs_transaction_ok(sub {
        my ($casetype, $zaaktype_status) = $self->_generate_rule_casetype;

        my $case        = $zs->create_case_ok(zaaktype => $casetype);

        # SleurEnPleur
        my $params      = Zaaksysteem::Backend::Rules->generate_object_params(
            {
                'case'                      => $case,
            }
        );
        # /SleurEnPleur

        local $valid_result->{'case.casetype.node.id'} = $case->zaaktype_node_id->id;
        is_deeply($params->{rule_params}, $valid_result, 'Got correct params');

        # my $first_rule = $engine->rules->[0];
    }, 'generate_object_params: get params with case');
}

=head2 integrity

Generates an integrity object according to the given rule set, and in the future from
the given object.

=cut

sub rules_usage_integrity : Tests {
    my $self        = shift;

    throws_ok(
        sub {
            Zaaksysteem::Backend::Rules->integrity();
        },
        qr/from_session missing or/,
        'Correct parameter checking'
    );


    $zs->zs_transaction_ok(sub {
        my ($casetype, $zaaktype_status) = $self->_generate_rule_invalid_casetype;

        my $ztmodel     = Zaaksysteem::Zaaktypen->new(dbic => $schema, object_model => $zs->object_model);
        my $session     = $ztmodel->retrieve_session($casetype->id);

        # diag(explain($session));

        my $i_obj       = Zaaksysteem::Backend::Rules->integrity(
            {
                from_session    => $session
            },
            $schema
        );


        isa_ok($i_obj, 'Zaaksysteem::Backend::Rules::Integrity');
        can_ok($i_obj, $_) for qw/success problems/;

        ok(!$i_obj->success, 'Incorrect rules');
        is(@{ $i_obj->problems }, 8, 'Got 8 problems');

        ok($i_obj->problems->[0]->{$_}, 'Got value for: ' . $_) for qw/message id rule_number naam status_number/;

        # diag(explain($i_obj->problems));
        my $status_i = $i_obj->for_status_number(1);

        isa_ok($status_i, 'Zaaksysteem::Backend::Rules::Integrity');

        ok(!$status_i->success, 'No success for status number 1');
        ok($i_obj->for_status_number(2)->success, 'Success for status 2');

    }, 'Tested invalid casetype');

}

=head2 validate

Validates a given set of params against rules, and returns a validation object containing
the "active" actions, visible (and hidden) fields, visibile (and hidden) groups

=cut

sub rules_usage_validate : Tests {
    my $self        = shift;

    $zs->zs_transaction_ok(sub {
        my ($casetype, $zaaktype_status) = $self->_generate_rule_casetype;

        my $case        = $zs->create_case_ok(zaaktype => $casetype);

        # SleurEnPleur
        my $params      = Zaaksysteem::Backend::Rules->generate_object_params(
            {
                'case'                      => $case,
            }
        );

        my $rules       = $params->{rules};

        my $validation  = $rules->validate(
            {
                %{ $params->{rule_params} },
                'attribute.checkbox_hide' => 'hide',
                'attribute.checkbox_set_value' => 'set_value',
            }
        );
        # /SleurEnPleur


        isa_ok($validation, 'Zaaksysteem::Backend::Rules::ValidationResults');

        is_deeply(
            [ sort @{ $validation->active_attributes }],
            [
                'attribute.checkbox_hide',
                'attribute.checkbox_set_value',
                'attribute.checkbox_show',
                'attribute.text_set_value',
                'attribute.text_show'
            ],
        );

        is(@{ $validation->actions }, 2, 'Found exactly 2 actions');
        is($validation->changes->{'attribute.text_set_value'}, 'set_value Value', 'Found correct value for attribute.text_set_value');

    }, 'Tested basic validation');

    $zs->zs_transaction_ok(sub {
        my ($casetype, $zaaktype_status) = $self->_generate_rule_casetype;

        my $case        = $zs->create_case_ok(zaaktype => $casetype);

        # SleurEnPleur
        my $params      = Zaaksysteem::Backend::Rules->generate_object_params(
            {
                'case'                      => $case,
            }
        );

        my $rules       = $params->{rules};

        my $validation  = $rules->validate_from_case($case);

        # /SleurEnPleur

        isa_ok($validation, 'Zaaksysteem::Backend::Rules::ValidationResults');

    }, 'Tested basic validation');

}

=head1 CODE TESTS

Code tests, testing the implementation itself

=cut

sub rules_basics_is_deeply : Tests {
    my $self        = shift;

    $zs->zs_transaction_ok(sub {
        my $definition = [
          {
            'conditions' => {
              'conditions' => [
                {
                  'attribute_name' => 'attribute.checkbox_hide',
                  'validation_type' => 'immediate',
                  'values' => [
                    'hide'
                  ]
                }
              ],
              'type' => 'and'
            },
            'else' => [],
            'then' => [
              {
                'data' => {
                  'attribute_name'  => 'attribute.text_hide',
                },
                'type' => 'hide_attribute'
              }
            ]
          },
          {
            'conditions' => {
              'conditions' => [
                {
                  'attribute_name' => 'attribute.checkbox_show',
                  'validation_type' => 'immediate',
                  'values' => [
                    'show'
                  ]
                }
              ],
              'type' => 'and'
            },
            'else' => [],
            'then' => [
              {
                'data' => {
                  'attribute_name' => 'attribute.text_show',
                },
                'type' => 'show_attribute'
              }
            ]
          },
          {
            'conditions' => {
              'conditions' => [
                {
                  'attribute_name' => 'attribute.checkbox_set_value',
                  'validation_type' => 'immediate',
                  'values' => [
                    'set_value'
                  ]
                }
              ],
              'type' => 'and'
            },
            'else' => [],
            'then' => [
              {
                'data' => {
                  'attribute_name' => 'attribute.text_set_value',
                  'can_change'      => undef,
                  'attribute_type' => 'text',
                  'value' => 'set_value Value'
                },
                'type' => 'set_value'
              }
            ]
          }
        ];

        my ($casetype, $zaaktype_status) = $self->_generate_rule_casetype;

        my $rengine  = Zaaksysteem::Backend::Rules->new(
            source          => $casetype->zaaktype_node_id,
        );

        my $generated_definition = decode_json(JSON->new->utf8->convert_blessed(1)->encode($rengine));

        is_deeply($generated_definition, $definition, 'Generated definition matches defined definition');
    }, 'Tested basic rules');
}

sub rules_rule__convert_casetype_rule_to_array : Tests {
    my $self        = shift;

    $zs->zs_transaction_ok(sub {
        my ($casetype, $zaaktype_status) = $self->_generate_rule_casetype;

        my $case     = $zs->create_case_ok(zaaktype => $casetype);

        my $rengine  = Zaaksysteem::Backend::Rules->new(
            source          => $casetype->zaaktype_node_id,
        );

        my $converted = $rengine->rules->[0]->_convert_casetype_rule_to_array('voorwaarde');
        isa_ok($converted, 'ARRAY', '_convert_casetype_rule_to_array: voorwaarde');
        is(scalar(@{ $converted }), 1, '_convert_casetype_rule_to_array: voorwaarde: exactly one result');
        ok($converted->[0]->{$_}, '_convert_casetype_rule_to_array: voorwaarde: found key ' . $_) for qw/kenmerk value value_checkbox/;

        $converted = $rengine->rules->[0]->_convert_casetype_rule_to_array('actie');
        isa_ok($converted, 'ARRAY', '_convert_casetype_rule_to_array: actie');
        is(scalar(@{ $converted }), 1, '_convert_casetype_rule_to_array: actie: exactly one result');
        ok($converted->[0]->{$_}, '_convert_casetype_rule_to_array: actie: found key ' . $_) for qw/kenmerk value type/;
    }, 'Tested _convert_casetype_rule_to_array');
}

=head1 INTERNAL FUNCTIONS

Internal test functions below

=cut

use constant BASIC_CHECKS => [qw/
    hide
    show
    set_value
/];

sub _generate_rule_casetype {
    my $self            = shift;

    my $zaaktype_node   = $zs->create_zaaktype_node_ok;
    my $casetype        = $zs->create_zaaktype_ok(node => $zaaktype_node);
    my $zaaktype_status = $zs->create_zaaktype_status_ok(
        status => 1,
        fase   => 'registratiefase',
        node   => $zaaktype_node
    );
    $zs->create_zaaktype_status_ok(
        status => 3,
        fase   => 'afhandelfase',
        node   => $zaaktype_node
    );

    ### Generate zaaktype_kenmerken, checkboxes
    my $zt_kenmerk = $zs->create_zaaktype_kenmerk_ok(
        status              => $zaaktype_status,
        bibliotheek_kenmerk => $zs->create_bibliotheek_kenmerk_ok(
            naam            => 'checkbox_' . $_,
            magic_string    => 'checkbox_' . $_,
            value_type      => 'checkbox',
            values          => [
                {
                    value   => $_,
                    active  => 1,
                }
            ]
        )
    ) for @{ BASIC_CHECKS() };

    ### Generate zaaktype_kenmerken, textboxes
    $zs->create_zaaktype_kenmerk_ok(
        status              => $zaaktype_status,
        bibliotheek_kenmerk => $zs->create_bibliotheek_kenmerk_ok(
            naam            => 'text_' . $_,
            magic_string    => 'text_' . $_,
            value_type      => 'text',
        )
    ) for @{ BASIC_CHECKS() };


    ### Now retrieve those kenmerken
    my @zaaktype_kenmerken = $zaaktype_node->zaaktype_kenmerken->search({},{prefetch => 'bibliotheek_kenmerken_id'})->all;

    ### And create basic rules
    for my $check (@{ BASIC_CHECKS() }) {
        my ($actie_kenmerk) = grep(
            {
                $_->bibliotheek_kenmerken_id->naam eq 'text_' . $check
            }
            @zaaktype_kenmerken
        );
        my ($cond_kenmerk) = grep(
            {
                $_->bibliotheek_kenmerken_id->naam eq 'checkbox_' . $check
            }
            @zaaktype_kenmerken
        );

        my %OLDMAP = (
            set_value   => 'vul_waarde_in',
            hide        => 'verberg_kenmerk',
            show        => 'toon_kenmerk',
        );

        $zs->create_zaaktype_regel_ok(
            status  => $zaaktype_status,
            # node => $zaaktype_node,
            naam    => 'Rule basic check: ' . $check . ':' . $cond_kenmerk->bibliotheek_kenmerken_id->naam . ' eq ' . $check,
            settings => {
                'voorwaarde_1_kenmerk'          => $cond_kenmerk->bibliotheek_kenmerken_id->id,
                'voorwaarde_1_value'            => $check,
                'voorwaarde_1_value_checkbox'   => '1',
                'voorwaarden'                   => '1',


                'actie_1'                       => $OLDMAP{$check},
                'actie_1_kenmerk'               => $actie_kenmerk->bibliotheek_kenmerken_id->id,
                'actie_1_value'                 => $check . ' Value',
                'acties'                        => '1',

                'naam'                          => 'Rule basic check: ' . $check,
            }
        );

        # print STDERR 'Enabled rule: ' . 'Rule basic check: ' . $check . ':' . $cond_kenmerk->bibliotheek_kenmerken_id->naam . ' eq ' . $check . "\n";
    }

    return ($casetype, $zaaktype_status);
}

sub _generate_rule_invalid_casetype {
    my $self            = shift;

    my $zaaktype_node   = $zs->create_zaaktype_node_ok;
    my $casetype        = $zs->create_zaaktype_ok(node => $zaaktype_node);
    my $zaaktype_status = $zs->create_zaaktype_status_ok(
        status => 1,
        fase   => 'registratiefase',
        node   => $zaaktype_node
    );
    $zs->create_zaaktype_status_ok(
        status => 3,
        fase   => 'afhandelfase',
        node   => $zaaktype_node
    );

    ### Generate zaaktype_kenmerken, checkboxes
    my $zt_kenmerk = $zs->create_zaaktype_kenmerk_ok(
        status              => $zaaktype_status,
        bibliotheek_kenmerk => $zs->create_bibliotheek_kenmerk_ok(
            naam            => 'checkbox_' . $_,
            magic_string    => 'checkbox_' . $_,
            value_type      => 'checkbox',
            values          => [
                {
                    value   => $_,
                    active  => 1,
                }
            ]
        )
    ) for @{ BASIC_CHECKS() };

    ### Generate zaaktype_kenmerken, textboxes
    $zs->create_zaaktype_kenmerk_ok(
        status              => $zaaktype_status,
        bibliotheek_kenmerk => $zs->create_bibliotheek_kenmerk_ok(
            naam            => 'text_' . $_,
            magic_string    => 'text_' . $_,
            value_type      => 'text',
        )
    ) for @{ BASIC_CHECKS() };


    ### Now retrieve those kenmerken
    my @zaaktype_kenmerken = $zaaktype_node->zaaktype_kenmerken->search({},{prefetch => 'bibliotheek_kenmerken_id'})->all;

    ### And create basic rules
    for my $check (@{ BASIC_CHECKS() }) {
        my ($actie_kenmerk) = grep(
            {
                $_->bibliotheek_kenmerken_id->naam eq 'text_' . $check
            }
            @zaaktype_kenmerken
        );
        my ($cond_kenmerk) = grep(
            {
                $_->bibliotheek_kenmerken_id->naam eq 'checkbox_' . $check
            }
            @zaaktype_kenmerken
        );

        my %OLDMAP = (
            set_value   => 'vul_waarde_in',
            hide        => 'verberg_kenmerk',
            show        => 'toon_kenmerk',
        );

        $zs->create_zaaktype_regel_ok(
            status  => $zaaktype_status,
            # node => $zaaktype_node,
            naam    => 'Rule basic check: ' . $check,
            settings => {
                'voorwaarde_1_kenmerk'          => $cond_kenmerk->bibliotheek_kenmerken_id->id,
                'voorwaarde_1_value'            => $check,
                'voorwaarde_1_value_checkbox'   => '1',
                'voorwaarden'                   => '1',


                'actie_1'                       => $OLDMAP{$check},
                'actie_1_kenmerk'               => $actie_kenmerk->bibliotheek_kenmerken_id->id,
                'actie_1_value'                 => $check . ' Value',
                'acties'                        => '1',

                'naam'                          => 'Rule basic check: ' . $check,
            }
        );

        $zs->create_zaaktype_regel_ok(
            status  => $zaaktype_status,
            # node => $zaaktype_node,
            naam    => 'Rule basic fsckup: ' . $check,
            settings => {
                'voorwaarde_1_kenmerk'          => '8923729323',
                'voorwaarde_1_value'            => $check,
                'voorwaarde_1_value_checkbox'   => '1',
                'voorwaarden'                   => '1',


                'actie_1'                       => $OLDMAP{$check},
                'actie_1_kenmerk'               => '8998892389',
                'actie_1_value'                 => $check . ' Value',
                'acties'                        => '1',

                'naam'                          => 'Rule basic fsckup: ' . $check,
            }
        );
    }

    $zs->create_zaaktype_regel_ok(
        status  => $zaaktype_status,
        # node => $zaaktype_node,
        naam    => 'Rule fsckup: hide_group',
        settings => {
            'voorwaarde_1_kenmerk'          => '920935234',
            'voorwaarde_1_value'            => 'Zuid',
            'voorwaarde_1_value_checkbox'   => '1',
            'voorwaarden'                   => '1',


            'actie_1'                       => 'hide_group',
            'actie_1_kenmerk'               => 'Group2',
            'actie_1_value'                 => undef,
            'acties'                        => '1',

            'naam'                          => 'Rule fsckup: hide_group',
        }
    );

    return ($casetype, $zaaktype_status);
}



sub results_as_rules : Tests {
    $zs->txn_ok(
        sub {
            my $name = "Result rule";
            my $rule = {
                active => '1',
                naam   => $name,

                acties => '1',
                actie_1          => 'sjabloon_genereren',
                actie_1_sjabloon => '1',

                bibliotheek_kenmerken_id => undef,

                condition_type => 'and',
                mijlsort       => '5',

                voorwaarden => '1',
                voorwaarde_1_kenmerk        => 'case_result',
                voorwaarde_1_value_checkbox => '1',
                voorwaarde_1_value          => 'Totaal onbruikbaar',
            };

            my $zt = $zs->create_zaaktype_predefined_ok();
            my $ztn = $zt->zaaktype_node_id;

            my $r = $zs->create_zaaktype_resultaat_ok(
                node      => $ztn,
                label     => 'Totaal onbruikbaar',
            );

            use Zaaksysteem::Tools qw(dump_terse);

            my $zts = $zs->schema->resultset("ZaaktypeStatus")
                ->search({ zaaktype_node_id => $ztn->id },
                { order_by => { -desc => 'status' } })->first;

            my $zs_rule = $zs->create_zaaktype_regel_ok(
                status   => $zts,
                naam     => $name,
                settings => $rule,
            );

            $zt->discard_changes;
            $ztn->discard_changes;

            my $case = $zs->create_case_ok(zaaktype => $zt);

            $case->set_result_by_id({id =>$r->id});
            $case->discard_changes;

            use Zaaksysteem::Tools qw(dump_terse);
            my $result = {};

            my $options = {
                kenmerken =>
                    $case->field_values({ fase => $zts->status }),
                aanvrager       => $case->aanvrager_object,
                contactchannel  => $case->contactkanaal,
                payment_status  => $case->payment_status || 'Paid',
                casetype        => $case->zaaktype_node_id,
                confidentiality => $case->confidentiality,
                case_result     => $case->resultaat,
                result          => $result,
            };

            ok($zs_rule->_check_conditions($options), "Rule for results applies");
            lives_ok(
                sub {
                    $zs_rule->execute($options);
                },
                'rule->execute works as intended'
            );

        },
        "Epic: Rules for results"
    );
}

=head1 SEE ALSO

L<Zaaksysteem::Backend::Rules> L<Zaaksysteem::Manual>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


1;
