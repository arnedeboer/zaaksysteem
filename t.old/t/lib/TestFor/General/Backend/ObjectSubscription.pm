package TestFor::General::Backend::ObjectSubscription;

# ./zs_prove -v t/lib/TestFor/General/Backend/ObjectSubscription.pm
use base qw(ZSTest);

use Moose;
use TestSetup;

=head1 NAME

TestFor::General::Backend::ObjectSubscription - ObjectSubscription specific tests

=head1 LAYOUT

This file implements all USAGE tests, in other words: if you would like
to use this module, and the belonging ResultSet and Component classes.

=head1 USAGE (or end-to-end)

Usage tests: use the below tests when you would like to give the new
rules engine a spin.

=head2 search_filtered

Helper function for instantiating the engine with a case object (old style)

=cut

sub objectsubscription_usage_search_filtered : Tests {
    my $self            = shift;

    $zs->zs_transaction_ok(sub {
        my $interface   = $self->_create_case_and_entries;
        my $ors         = $schema->resultset('ObjectSubscription');

        ### Freeform
        TODO: {
            local $TODO = 'AUTUMN2015BREAK';
            is($ors->search_filtered({ freeform_filter => 'Anton'})->count, 3, 'Found two entries filtering to "Anton"');
        };
        
        is($ors->search_filtered({ freeform_filter => 'Zaaksysteem'})->count, 7, 'Found five entries filtering to "Zaaksysteem"');
        is($ors->search_filtered({ freeform_filter => 'Mintlab'})->count, 3, 'Found three entries filtering to "Mintlab"');

        ### Per table or interface
        is($ors->search_filtered({ local_table => 'Bedrijf'})->count, 3, 'Found three entries of table "Bedrijf"');
        is($ors->search_filtered({ interface_id => $interface->id})->count, 10, 'Found ten entries for interface_id: ' . $interface->id);

        ### Per special view
        is($ors->search_filtered({ view_id => 'subscriptions_with_cases'})->count, 3, 'Found two subscriptions with a case');
        is($ors->search_filtered({ view_id => 'subscriptions_without_cases'})->count, 4, 'Found four subscriptions without a case');
        is($ors->search_filtered({ view_id => 'subscriptions_without_cases_since_1d'})->count, 1, 'Found one subscriptions without a case and more than one day old');


        my $nps = $schema->resultset('NatuurlijkPersoon')->search();
        while (my $np = $nps->next) {
            $np->in_gemeente(1);
            $np->update;
        }

        is($ors->search_filtered({ view_id => 'subscriptions_outside_municipality_with_cases'})->count, 0, 'Found no subscriptions for people outside municipality, with cases');
        is($ors->search_filtered({ view_id => 'subscriptions_outside_municipality_without_cases'})->count, 0, 'Found no subscription for people outside municipality, without cases');

        $nps = $schema->resultset('NatuurlijkPersoon')->search();
        while (my $np = $nps->next) {
            $np->in_gemeente(0);
            $np->update;
        }

        is($ors->search_filtered({ view_id => 'subscriptions_outside_municipality_with_cases'})->count, 3, 'Found three subscriptions for people outside municipality, with cases');
        is($ors->search_filtered({ view_id => 'subscriptions_outside_municipality_without_cases'})->count, 4, 'Found four subscriptions for people outside municipality, without cases');

    }, 'search_filtered');

    $zs->zs_transaction_ok(sub {
        $zs->create_stuf_interfaces_ok;
        my ($prs)       = $schema->resultset('Interface')->search({module => 'stufprs'});

        my $ors         = $schema->resultset('ObjectSubscription');

        my $cold_case   = $zs->create_case_ok;

        $schema->resultset('ObjectSubscription')->create(
            {
                interface_id    => $prs->id,
                external_id     => ((rand(3)*10) * 242442),
                local_table     => 'NatuurlijkPersoon',
                local_id        => $cold_case->aanvrager->gegevens_magazijn_id,
                object_preview  => $cold_case->aanvrager->naam,
                date_created    => DateTime->now()->subtract(days => 2),
            }
        );

        is($ors->search_filtered({ view_id => 'subscriptions_with_cases'})->count, 1, 'Found single case, because it is not destroyed');

        $cold_case->_delete_zaak();
        $cold_case->update;

        is($ors->search_filtered({ view_id => 'subscriptions_with_cases'})->count, 0, 'Found no subscriptions, because there are no active cases');

        ### Loop over aanvragers, and set in_gemeente to true;
    }, 'search_filtered: destroyed cases (ZS-4972)');
}

sub _create_case_and_entries {
    my $self            = shift;

    ### Find StUF interface for prs
    $zs->create_stuf_interfaces_ok;
    my ($prs)           = $schema->resultset('Interface')->search({module => 'stufprs'});


    ### Create two cases with two different aanvragers and two subscriptions
    for (1..2) {
        my $case            = $zs->create_case_ok;

        $schema->resultset('ObjectSubscription')->create(
            {
                interface_id    => $prs->id,
                external_id     => ((rand(3)*10) * 242442),
                local_table     => 'NatuurlijkPersoon',
                local_id        => $case->aanvrager->gegevens_magazijn_id,
                object_preview  => $case->aanvrager->naam,
            }
        );
    }

    my $cold_case            = $zs->create_case_ok;

    $schema->resultset('ObjectSubscription')->create(
        {
            interface_id    => $prs->id,
            external_id     => ((rand(3)*10) * 242442),
            local_table     => 'NatuurlijkPersoon',
            local_id        => $cold_case->aanvrager->gegevens_magazijn_id,
            object_preview  => $cold_case->aanvrager->naam,
            date_created    => DateTime->now()->subtract(days => 2),
        }
    );

    ### Create 3 extra persons without a case
    for (1..3) {
        my $aanvrager           = $zs->create_natuurlijk_persoon_ok();
        $schema->resultset('ObjectSubscription')->create(
            {
                interface_id    => $prs->id,
                external_id     => ((rand(3)*10) * 242442),
                local_table     => 'NatuurlijkPersoon',
                local_id        => $aanvrager->id,
                object_preview  => $aanvrager->geslachtsnaam,
            }
        );
    }

    ### Create one subscription, older than a day, without
    my $cold_aanvrager           = $zs->create_natuurlijk_persoon_ok();
    $schema->resultset('ObjectSubscription')->create(
        {
            interface_id    => $prs->id,
            external_id     => ((rand(3)*10) * 242442),
            local_table     => 'NatuurlijkPersoon',
            local_id        => $cold_aanvrager->id,
            object_preview  => $cold_aanvrager->geslachtsnaam,
            date_created    => DateTime->now()->subtract(days => 2),
        }
    );

    ### And 3 as a company
    for (1..3) {
        my $aanvrager           = $zs->create_bedrijf_ok();
        $schema->resultset('ObjectSubscription')->create(
            {
                interface_id    => $prs->id,
                external_id     => ((rand(3)*10) * 242442),
                local_table     => 'Bedrijf',
                local_id        => $aanvrager->id,
                object_preview  => $aanvrager->handelsnaam,
            }
        );
    }

    return $prs;
}


=head1 SEE ALSO

L<Zaaksysteem::Backend::Rules> L<Zaaksysteem::Manual>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


1;
