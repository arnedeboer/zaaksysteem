package TestFor::General::DB::Sjablonen;
use base qw(ZSTest);

=head1 NAME

TestFor::General::DB::Sjablonen - A test package for Zaaksysteem Templates

=head1 USAGE

    ./zs_prove -v t/lib/TestFor/General/DB/Sjablonen.pm

=cut

use TestSetup;

use Zaaksysteem::DocumentValidator;

sub assert_target_format : Tests {
    $zs->zs_transaction_ok(
        sub {
            my $sjabloon = $zs->create_sjabloon_ok;
            isa_ok($sjabloon, "Zaaksysteem::Schema::BibliotheekSjablonen");

            my $tf = $sjabloon->_assert_target_format('ODT');
            is($tf, 'odt', "ODT is correct");

            $tf = $sjabloon->_assert_target_format('pdf');
            is($tf, 'pdf', "PDF is correct");

            throws_ok(
                sub {
                    my $tf = $sjabloon->_assert_target_format('jpg');
                },
                qr/target_format 'jpg' is unsupported/,
                "Unsupported format specified"
            );

            throws_ok(
                sub {
                    my $tf = $sjabloon->_assert_target_format();
                },
                qr/target_format is undefined./,
                "No format specified"
            );
        },
        '_assert_target_format'
    );
}

sub filestore_operations : Tests {
    $zs->zs_transaction_ok(
        sub {

            my $sjabloon = $zs->create_sjabloon_ok;
            my $case     = $zs->create_case_ok;

            my $dir  = 't/inc/Documents';
            my $file = 'openoffice_document.odt';

            my $filestore = $zs->create_filestore_ok(
                file_path     => "$dir/$file",
                original_name => $file,
            );
            $sjabloon->filestore($filestore);

            my $document
                = $sjabloon->_get_odf_document_handle({ tmpdir => '/tmp' });
            isa_ok($document, "OpenOffice::OODoc::Document");

            my $x = $sjabloon->_filestore_operations(
                {
                    file   => "/tmp/$file",
                    dir    => $dir,
                    format => 'odt',
                    case   => $case,
                }
            );
            ok($x, '_filestore_operations returns 1');

            my $ok = Zaaksysteem::DocumentValidator->validate_path(
                { filepath => "/tmp/$file" });
            ok($ok, "Filename and MIME-type are OK");

            $x = $sjabloon->_filestore_operations(
                {
                    file   => "/tmp/$file.pdf",
                    dir    => $dir,
                    format => 'pdf',
                    case   => $case,
                }
            );
            ok($x, '_filestore_operations returns 1');
            $ok = Zaaksysteem::DocumentValidator->validate_path(
                { filepath => "/tmp/$file" });
            ok($ok, "Filename and MIME-type are OK");

            throws_ok(
                sub {
                    my $x = $sjabloon->_filestore_operations(
                        {
                            file   => 'file.name',
                            dir    => '/tmp/',
                            format => 'pdx',
                            case   => $case,
                        }
                    );
                },
                qr/invalid: format/,
                "Incorrect target_format"
            );

        },
        '_filestore_operations'
    );
}

sub sjablonen_tests : Tests {
    $zs->zs_transaction_ok(
        sub {

            my $subject  = $zs->create_subject_ok();
            my $case     = $zs->create_case_ok();
            my $sjabloon = $zs->create_sjabloon_ok();

            ok($case->is_in_phase('registratie_fase'),
                "Zit in registratiefase");

            my $betrokkene_id = 'betrokkene-medewerker-' . $subject->id;

            my $doc_name = Zaaksysteem::TestUtils::generate_random_string;
            my %file_create_opts = (
                name          => $doc_name,
                case          => $case,
                subject       => $betrokkene_id,
                target_format => 'pdf',
            );

            my $file   = $sjabloon->file_create(\%file_create_opts);
            my $expect = {
                name              => $doc_name,
                accepted          => 1,
                is_duplicate_name => 0,
            };

            is_file_ok($file, $expect, "Creation went well");

            $file   = $sjabloon->file_create(\%file_create_opts);
            $expect = {
                name              => $doc_name,
                accepted          => 0,
                is_duplicate_name => 1,
            };
            is_file_ok($file, $expect, "Not accepted because duplicate");

            {
                note("Afhandelfase tests");

                my $cs = $zs->create_subject_ok();
                $case = $zs->create_case_ok();
                ok($case->is_in_phase('registratie_fase'),
                    "Zit in registratiefase");

                my $doc_name = Zaaksysteem::TestUtils::generate_random_string;
                my %file_create_opts = (
                    name          => $doc_name,
                    case          => $case,
                    subject       => 'betrokkene-medewerker-' . $cs->id,
                    target_format => 'pdf',
                );

                $file   = $sjabloon->file_create(\%file_create_opts);
                $expect = {
                    name              => $doc_name,
                    accepted          => 1,
                    is_duplicate_name => 0,
                };
                is_file_ok($file, $expect, "Accepted in reg_fase");

                $case->wijzig_behandelaar(
                    { betrokkene_identifier => $betrokkene_id });
                $case->resultaat
                    (Zaaksysteem::TestUtils::DEFAULT_ZAAKTYPE_RESULTAAT);
                $case->advance(
                    object_model     => $zs->object_model,
                    betrokkene_model => $zs->betrokkene_model,
                    current_user     => $zs->set_current_user,
                );

                $file   = $sjabloon->file_create(\%file_create_opts);
                $expect = {
                    name              => $doc_name,
                    accepted          => 0,
                    is_duplicate_name => 1,
                };
                is_file_ok($file, $expect,
                    "Not accepted in between open/close");
                $file->delete;

                $case->wijzig_behandelaar(
                    { betrokkene_identifier => $betrokkene_id });

                my %opts = %file_create_opts;
                $opts{subject}       = $betrokkene_id;
                $opts{target_format} = 'odt',
                    my $result = $sjabloon->file_create(\%opts);
                $expect = {
                    name              => $doc_name,
                    accepted          => 1,
                    is_duplicate_name => 0,
                };
                is_file_ok($result, $expect,
                    "Accepted because created by behandelaar");

                # Afhandelfase
                $case->advance(
                    object_model     => $zs->object_model,
                    betrokkene_model => $zs->betrokkene_model,
                    current_user     => $zs->set_current_user,
                );
                ok($case->is_in_phase('afhandel_fase'), "Zit in afhandelfase");

                $file   = $sjabloon->file_create(\%file_create_opts);
                $expect = {
                    name              => "$doc_name (1)",
                    accepted          => 1,
                    is_duplicate_name => 0,
                };
                is_file_ok($file, $expect, "Accepted in close_fase");

            }

            {
                local *Zaaksysteem::Schema::BibliotheekSjablonen::_tmp_dir
                    = sub {
                    return "/tmp/doesnot";
                    };
                throws_ok(
                    sub {
                        $sjabloon->file_create(\%file_create_opts);
                    },
                    qr/could not write to/,
                    "Unable to create file"
                );
            }
        },
        "Sjablonen behaviour defined",
    );
}

sub external_templates : Tests {
    $zs->txn_ok(
        sub {

            my $interface = $zs->create_xential_interface_ok(spoofmode => 0);
            my $uuid = $zs->generate_uuid;

            my $template = $zs->create_sjabloon_ok(
                template_uuid => $uuid,
                interface     => $interface,
            );
            isa_ok($template, "Zaaksysteem::Schema::BibliotheekSjablonen");

            is($template->template_uuid, $uuid, "Correct UUID");
            is($template->interface_id->id, $interface->id, "Correct interface_id");

            my $filename = Zaaksysteem::TestUtils::generate_random_string;
            my $case     = $zs->create_case_ok;
            my $subject  = $zs->create_subject_ok;

            {
                no warnings qw(redefine once);
                local *Zaaksysteem::Xential::create_document_from_template = sub {
                    return { res => "You called Xential" };
                };
                my $result = $template->create_file_from_external_template(
                    {
                        name          => $filename,
                        case          => $case,
                        subject       => $subject,
                    }
                );

                cmp_deeply(
                    $result,
                    {
                        document => {},
                        res => "You called Xential"
                    },
                    "Create template from Xential"
                );

#                # TODO: add case documents
#                my %optional = (
#                    case_document_ids => [1];
#                );
#                my $result = $template->file_create(
#                    {
#                        name          => $params->{name},
#                        case          => $case,
#                        subject       => $subject,
#                        target_format => 'odt',
#                        %optional,
#                    }
#                );

            }



        },
        "External templates"
    );
}

sub is_file_ok {
    my ($file, $expect, $desc) = @_;

    my $result = { map { $_ => $file->$_ } keys %$expect };
    is_deeply($result, $expect, $desc);
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
