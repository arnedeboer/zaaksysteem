package TestFor::General::Scheduler::Job::CreateCase;
use base qw(Test::Class);

use DateTime;
use Moose::Util qw(ensure_all_roles);
use TestSetup;
use Zaaksysteem::Tools;
use Zaaksysteem::Object::Types::ScheduledJob;

sub setup : Test(startup) {
    my $self = shift;

    my $mock_db = Test::MockObject->new();
    $mock_db->mock(schema    => sub { return $zs->schema });
    $mock_db->mock(resultset => sub { return $zs->schema->resultset($_[1]) });

    $self->{model_object} = Zaaksysteem::Object::Model->new(schema => $zs->schema);
    $self->{model_db} = $mock_db;

    return;
}

sub zs_scheduler_job_create_case : Tests {
    my $self = shift;

    my $now = DateTime->now();
    my $dummy_context = Test::MockObject->new();
    $dummy_context->mock(
        'model',
        sub {
            my $mock = shift;
            my ($requested_model) = @_;
            return $self->{ 'model_' . lc($requested_model) };
        }
    );

    {
        my $sj = Zaaksysteem::Object::Types::ScheduledJob->new(
            job             => 'CreateCase',
            next_run        => $now->clone,
            interval_period => 'weeks',
            interval_value  => 2,
            runs_left       => 2,
        );
        ensure_all_roles($sj, 'Zaaksysteem::Scheduler::Job::CreateCase');

        throws_ok(
            sub { $sj->run($dummy_context); },
            qr{scheduler/job/create_case/relations},
            'Running a CreateCase job without an attached case type goes boom',
        );
    }

    $zs->zs_transaction_ok(sub {
        my $sj = Zaaksysteem::Object::Types::ScheduledJob->new(
            job             => 'CreateCase',
            next_run        => $now->clone,
            interval_period => 'weeks',
            interval_value  => 2,
            runs_left       => 2,
        );
        ensure_all_roles($sj, 'Zaaksysteem::Scheduler::Job::CreateCase');

        my $casetype = $zs->create_zaaktype_predefined_ok();
        my $root_case = $zs->create_case_ok();
        $root_case->_touch();

        my $cto = $casetype->_object;
        my $co = $root_case->object_data;

        $sj->add_relation(
            Zaaksysteem::Object::Relation->new(
                related_object_id   => $cto->uuid,
                related_object_type => $cto->object_class,
                relationship_name_a => 'job_dependency',
                relationship_name_b => 'scheduled_by',
                blocks_deletion     => 1,
            ),
        );
        $sj->add_relation(
            Zaaksysteem::Object::Relation->new(
                related_object_id   => $co->uuid,
                related_object_type => $co->object_class,
                relationship_name_a => 'job_dependency',
                relationship_name_b => 'scheduled_by',
                blocks_deletion     => 1,
            ),
        );

        # Run the actual job
        my $case = $sj->run($dummy_context);
        isa_ok($case, 'Zaaksysteem::Schema::Zaak', 'A case was created');

        my $od = $case->object_data;
        is(
            $od->get_object_attribute('case.related_cases')->value,
            $root_case->id,
            "Original case set as a relation"
        );
    });

    $zs->zs_transaction_ok(sub {

        my $sj = Zaaksysteem::Object::Types::ScheduledJob->new(
            job             => 'CreateCase',
            next_run        => $now->clone,
            interval_period => 'weeks',
            interval_value  => 2,
            runs_left       => 2,
        );
        ensure_all_roles($sj, 'Zaaksysteem::Scheduler::Job::CreateCase');

        my $casetype = $zs->create_zaaktype_predefined_ok();
        my $root_case = $zs->create_case_ok();
        $root_case->_touch();

        require Zaaksysteem::Object::Types::TestObject;
        my $dummy_object = $dummy_context->model('Object')->save(object => Zaaksysteem::Object::Types::TestObject->new());

        my $cto = $casetype->_object;
        my $co = $root_case->object_data;

        $sj->add_relation(
            Zaaksysteem::Object::Relation->new(
                related_object_id   => $cto->uuid,
                related_object_type => $cto->object_class,
                relationship_name_a => 'job_dependency',
                relationship_name_b => 'scheduled_by',
                blocks_deletion     => 1,
            ),
        );
        $sj->add_relation(
            Zaaksysteem::Object::Relation->new(
                related_object_id   => $co->uuid,
                related_object_type => $co->object_class,
                relationship_name_a => 'job_dependency',
                relationship_name_b => 'scheduled_by',
                blocks_deletion     => 1,
            ),
        );
        $sj->add_relation(
            Zaaksysteem::Object::Relation->new(
                related_object_id   => $dummy_object->id,
                related_object_type => $dummy_object->type,
                relationship_name_a => 'job_dependency',
                relationship_name_b => 'scheduled_by',
                blocks_deletion     => 1,
            ),
        );
        my $saved_sj = $dummy_context->model('Object')->save(object => $sj);

        # Run the actual job
        my $case = $sj->run($dummy_context);
        isa_ok($case, 'Zaaksysteem::Schema::Zaak', 'A case was created');

        my $od = $case->object_data;
        is(
            $od->get_object_attribute('case.related_cases')->value,
            $root_case->id,
            "Original case set as a relation"
        );

        # Re-retrieve our "dummy" object
        $dummy_object = $dummy_context->model('Object')->retrieve(uuid => $dummy_object->id);

        my @embedded = $dummy_object->filter_relations(sub { $_->relationship_name_a eq 'embedded' });
        is(@embedded, 1, "Dummy object got embedded in a case");
        is($embedded[0]->related_object_id, $od->uuid, "Dummy object got embedded in the right case");
    });

}

1;


__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

