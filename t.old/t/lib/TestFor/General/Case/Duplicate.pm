package Zaaksysteem::TestFor::General::Case::Duplicate;
use base qw(Test::Class);

use TestSetup;

sub zs_case_duplicate : Tests {

    $zs->zs_transaction_ok(sub {
        my $case = $zs->create_case_ok;
        my $new_case = $zs->create_case_ok();

        my $rs_checklist = $schema->resultset('Checklist')->search({ case_milestone => 2 });
        my $rs_item = $schema->resultset('ChecklistItem')->search;

        is($rs_checklist->search({case_id => $case->id})->count, 1, "Checklist present in the test case");
        my $old_checklist = $rs_checklist->first;

        my $max = 3;

        for (1 .. $max) {
            $zs->create_case_checklist_item_ok(
                state        => 1,
                sequence     => $_,
                user_defined => 0,
                checklist    => $old_checklist,
            );
        }

        my $count = $rs_item->search({checklist_id => $old_checklist->id })->count;
        is($count, $max, "All checklist items found");

        throws_ok(sub {
            $case->duplicate_checklist_items({bla => 1});
        }, qr/Validation of profile failed/, "Should bail out when called without proper params");

        $case->duplicate_checklist_items({ target => $new_case });

        is($rs_checklist->search({case_id => $new_case->id})->count, 1, "Checklist present in the new case");
        my $new_checklist = $rs_checklist->first;
        $count = $rs_item->search({checklist_id => $new_checklist->id })->count;
        is($count, $max, "All checklist items found");

    }, 'Check duplicate_checklist_items');

    $zs->zs_transaction_ok(
        sub {
            $zs->set_current_user;
            my $case = $zs->create_case_ok(onderwerp => "x" x 10 . "\r\n" . 'y' x 244);
            my $dupe = $case->duplicate({dont_copy_log => 1});

            isa_ok($dupe, ref $case, "Got a case");
            isnt($dupe->id, $case->id, "Case id is the same");
            is($dupe->onderwerp, $case->onderwerp, "Case subject is the same");

        },
        '$case->duplicate'
    );

}

1;


__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
