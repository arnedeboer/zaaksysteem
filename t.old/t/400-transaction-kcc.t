#! perl
use warnings;
use strict;

# Test header
use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
use Test::Moose;

# /Test header

use JSON;

$zs->zs_transaction_ok(sub {
    my $contact_data = $zs->create_contact_data_ok(
        {
            telefoonnummer => '0123456789'
        },
    );
    my $interface = $zs->create_named_interface_ok(
        {
            module => 'kcc',
            name => "KCC koppeling met PBX",
            interface_config => {},
        }
    );
    my $transaction = $schema->resultset('Transaction')->create(
        {
            interface => $interface,
            input_data => 'foo',
            processor_params => to_json(
                {
                    phonenumber => '0123456789'
                }
            ),
        }
    );

    ok(
        !$transaction->can('betrokkene'),
        'Transaction does not have the "betrokkene" method'
    );

    my $with_roles = $transaction->apply_roles();
    is($transaction, $with_roles, 'apply_roles() returned itself.');
    does_ok($transaction, 'Zaaksysteem::Backend::Sysin::Transaction::Role::KCC');

    ok(
        $transaction->can('betrokkene'),
        'Transaction now has the "betrokkene" method'
    );

    my ($betrokkene) = @{ $transaction->betrokkene };
    is(
        $betrokkene->gmid,
        $contact_data->gegevens_magazijn_id,
        "Transaction betrokkene is found correctly"
    );
}, 'Basic KCC transaction stuff');

zs_done_testing();
