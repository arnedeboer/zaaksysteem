#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';

use TestSetup;
initialize_test_globals_ok;
### Test header end

$zs->zs_transaction_ok(sub {
    my $i       = $zs->create_interface_ok(name => 'Kaboom');
    my $result  = $schema->resultset('Transaction')->transaction_create({
        input_data               => 'test',
        interface_id             => $i->id,
        external_transaction_id  => 'bla-1234',
    });

    my @result = $schema->resultset('Transaction')->search_filtered;

    is @result, 1, 'Found 1 transaction';
    is $result[0]->interface->name, 'Kaboom', 'Got correct interface';
}, 'transaction_read');


$zs->zs_transaction_ok(sub {
    my $t  = $zs->create_transaction_ok({interface_id => $zs->create_interface_ok->id});
    my $tr = $zs->create_transaction_record_ok({
        transaction_id => $t->id,
    });
    my @trs = $t->transaction_records;
    is @trs, 1, 'Bound transaction record';

    my @result = $schema->resultset('Transaction')->search_filtered({
        interface_id => $t->interface->id,
    });

    is @result, 1, 'Got a single transaction';
    is $result[0]->transaction_records, 1, 'Found expected transaction_record';
}, 'transaction_read with transaction_records');


$zs->zs_transaction_ok(sub {
    my $t = $zs->create_transaction_ok({interface_id => $zs->create_interface_ok->id});

    # Create some random other entries
    my $num = 0;
    while ($num < 5) {
        $zs->create_transaction_ok({
            interface_id => $zs->create_interface_ok()->id,
        });
        $num++;
    }

    my @result = $schema->resultset('Transaction')->search_filtered({
        interface_id => $t->interface->id,
    });

    is @result, 1, 'Got 1 transaction';
    is $result[0]->id, $t->id, 'Is the expected transaction';
}, 'transaction_read with specific interface_id');

# $zs->zs_transaction_ok(sub {
#     my $t = $zs->create_transaction_ok;
#     my $t_success = $zs->create_transaction_record_ok({
#         is_error => 0,
#         transaction_id => $t->id,
#     });
#     my $t_error = $zs->create_transaction_record_ok({
#         is_error => 1,
#         transaction_id => $t->id,
#     });

#     # Prove that we can use scalar context for the search
#     my @all = $schema->resultset('Transaction')->search_filtered;
#     is @all, 1, 'Found one transaction';
#     is $all[0]->records, 2, 'Has two records';

#     my ($result_error) = $schema->resultset('Transaction')->search_filtered({
#         'records.is_error' => 1,
#     });
#     is $result_error->records, 1, 'Got 1 transaction_record with is_error => 1';
#     is $result_error->id, $t_error->transaction->id, 'Is the expected record';

#     my ($result_success) = $schema->resultset('Transaction')->search_filtered({
#         'records.is_error' => 0,
#     });
#     is $result_success->records, 1, 'Got 1 transaction_record with is_error => 0';
#     is $result_success->id, $t_success->transaction->id, 'Is the expected record';

# }, 'transaction_read with specific error state');

$zs->zs_transaction_ok(sub {
    my $t        = $zs->create_transaction_ok({interface_id => $zs->create_interface_ok->id});
    my @result = $schema->resultset('Transaction')->search_filtered({
        interface_id => $t->interface->id,
    });
    is $result[0]->id, $t->id, 'Got expected transaction';

    my $to_json = $result[0]->TO_JSON;
    my @expected_keys = sort(qw/
        automated_retry_count
        date_created
        date_deleted
        date_last_retry
        date_next_retry
        direction
        error_count
        error_fatal
        error_message
        external_transaction_id
        id
        input_data
        input_file
        interface_id
        processed
        success_count
        result_preview
        state
        total_count
        processor_params
    /);
    my @gotten_keys = sort(keys %$to_json);
    is_deeply \@gotten_keys, \@expected_keys, 'Got expected hash keys';
}, 'transaction TO_JSON');

zs_done_testing;
