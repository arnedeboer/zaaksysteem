#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';

use TestSetup;
initialize_test_globals_ok;
### Test header end

$zs->zs_transaction_ok(sub {
    my $i = $zs->create_interface_ok;
    my $result = $schema->resultset('Transaction')->transaction_create({
        input_data               => 'test',
        interface_id             => $i->id,
        external_transaction_id  => 'bla-1234',
    });
    ok $result, 'Created transaction';
    my @set = ('input_data', 'interface_id', 'external_transaction_id');
    for my $s (@set) {
        ok $result->$s, "$s is set";
    }
}, 'transaction_create');

$zs->zs_transaction_ok(sub {
    throws_ok sub {
        $schema->resultset('Transaction')->transaction_create({});
    }, qr/Validation of profile/, 'No parameters dies';
}, 'transaction_create no params');

zs_done_testing;