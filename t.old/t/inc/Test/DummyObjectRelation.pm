package Test::DummyObjectRelation;

use Moose;

extends 'Zaaksysteem::Object';

has attr => (
    is => 'rw',
    isa => 'Str',
    traits => [qw[OA]]
);

has object_ref => (
    is => 'rw',
    traits => [qw[OR]]
);

has object_typed_ref => (
    is => 'rw',
    type => 'dummy_object_relation',
    traits => [qw[OR]]
);

has object_other_ref => (
    is => 'rw',
    type => 'derp',
    traits => [qw[OR]]
);

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
