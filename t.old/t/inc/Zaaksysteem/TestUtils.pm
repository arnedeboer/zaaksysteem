package Zaaksysteem::TestUtils;
use Moose;

=head1 NAME

Zaaksysteem::TestUtils - This is the Test Framework of Zaaksysteem

=head1 SYNOPSIS

    use warnings;
    use strict;

    use lib 't/inc';
    use TestSetup;
    use Zaaksysteem::TestUtils;

    my $zs = Zaaksysteem::TestUtils->new();

    $zs->zs_transaction_ok(
        sub {
            my $case = $zs->create_case_ok();

            # Do something with $case
            ok($case->can_advance, "A case can advance");

            # Failure with case
            throws_ok($case->dies, qr/die/, "I died");
        },
        "Description"
    );

=cut

use lib 't/inc';

use Data::UUID;
use Data::Serializer;
use DateTime::Format::Strptime;
use File::Basename;
use File::Spec::Functions qw(catfile catdir);
use Log::Log4perl;
# use Net::LDAP::Entry;
# use Net::LDAP;
use Params::Check qw(check);
use String::Random;
use Test::More;
use Test::Deep qw(cmp_deeply);
use Test::Exception;
use Module::Load::Conditional qw(can_load);
use Time::HiRes qw( gettimeofday tv_interval );
use IO::All;

use TestSetup;
use FakeLogger;

use Zaaksysteem::Betrokkene::Object::NatuurlijkPersoon;
use Zaaksysteem::Cache;
use Zaaksysteem::Constants qw(
    PROFILE_BAG_TYPES_OK
    ZAAKTYPE_TRIGGER
    ZAAK_CREATE_PROFILE
    ZAAK_CREATE_PROFILE_BETROKKENE
    SERVICE_NORM_TYPES_OK
    SERVICE_NORM_TYPES
    SJABLONEN_TARGET_FORMATS
    OBJECT_CLASS_DOMAIN
    VALID_FQDN
);

use Zaaksysteem::Betrokkene::Object::NatuurlijkPersoon;
use Zaaksysteem::Exception;
use Zaaksysteem::Object::Attribute;
use Zaaksysteem::Object::Model;
use Zaaksysteem::Profile qw[define_profile assert_profile build_profile];
use Zaaksysteem::Schema;
use Zaaksysteem::Types qw(UUID Betrokkene SubjectType);
use Zaaksysteem::Users;
use Zaaksysteem::XML::Compile;
use Zaaksysteem::XML::Generator::iWMO::2_0;
use Zaaksysteem::Zaaktypen::Import;

use constant DEFAULT_SUBJECTS   => [
    {
        'username'  => 'admin',
        'role'      => 'Administrator',
        'group'     => 'Backoffice',
    },
    {
        'username'  => 'beheerder',
        'role'      => 'Zaaksysteembeheerder',
        'group'     => 'Backoffice',
    },
    {
        'username'  => 'gebruiker',
        'group'     => 'Frontoffice',
    },
];

use constant DEFAULT_ZAAKTYPE_RESULTAAT => 'verleend';
use constant AANVRAGER_NP_PROFILE => {
    optional => {
        voornamen                            => 'Str',
        voorletters                          => 'Str',
        geslachtsnaam                        => 'Str',
        huisnummer                           => 'Int',
        huisnummertoevoeging                 => 'Str',
        huisletter                           => 'Str',
        postcode                             => 'Str',
        straatnaam                           => 'Str',
        correspondentie_straatnaam           => 'Str',
        correspondentie_huisnummer           => 'Str',
        correspondentie_postcode             => 'Str',
        correspondentie_woonplaats           => 'Str',
        correspondentie_huisnummertoevoeging => 'Str',
        correspondentie_huisletter           => 'Str',
        woonplaats                           => 'Str',
        geslachtsaanduiding                  => 'Str',
        contactgegevens                      => 'HashRef',
        geboorteplaats                       => 'Str',
        voorvoegsel                          => 'Str',
        a_nummer                             => 'Str',
        burgerservicenummer                  => 'Str',
        functie_adres                        => 'Str',
        geboortedatum                        => 'Any',
        aanduiding_naamgebruik               => 'Str',
    },
    defaults => {
        voorletters          => 'A A',
        voornamen            => 'Anton Ano',
        geslachtsnaam        => 'Zaaksysteem',
        huisnummer           => 42,
        postcode             => '1011PZ',
        straatnaam           => 'Muiderstraat',
        woonplaats           => 'Amsterdam',
        geslachtsaanduiding  => 'M',
        geboorteplaats       => 'Polis Massa',
        voorvoegsel          => 'van de',
        huisnummertoevoeging => '521',
        huisletter           => 'a',
        voorvoegsel          => 'van de',
        burgerservicenummer  => sub { return sprintf("%09d", generate_bsn()) },
        a_nummer             => sub { return sprintf("1%09d", generate_random_string('nnnnnnnnn')) },
        functie_adres        => 'W',
        geboortedatum        => sub { DateTime->now->subtract('years' => 21) },
    },
};

use constant AANVRAGER_BEDRIJF_PROFILE => {
    optional => {
        contact_aanspreektitel               => 'Str',
        contact_geslachtsaanduiding          => 'Str',
        contact_geslachtsnaam                => 'Str',
        contact_naam                         => 'Str',
        contact_voorletters                  => 'Str',
        contact_voorvoegsel                  => 'Str',
        correspondentie_adres                => 'Str',
        correspondentie_huisnummer           => 'Str',
        correspondentie_huisnummertoevoeging => 'Str',
        correspondentie_postcode             => 'Str',
        correspondentie_postcodewoonplaats   => 'Str',
        correspondentie_straatnaam           => 'Str',
        correspondentie_woonplaats           => 'Str',
        dossiernummer                        => 'Str',
        email                                => 'Str',
        fulldossiernummer                    => 'Str',
        handelsnaam                          => 'Str',
        hoofdactiviteitencode                => 'Str',
        hoofdvestiging_dossiernummer         => 'Str',
        hoofdvestiging_subdossiernummer      => 'Str',
        kamernummer                          => 'Str',
        nevenactiviteitencode1               => 'Str',
        nevenactiviteitencode2               => 'Str',
        rechtsvorm                           => 'Str',
        subdossiernummer                     => 'Str',
        surseance                            => 'Str',
        system_of_record_id                  => 'Str',
        telefoonnummer                       => 'Str',
        vestiging_adres                      => 'Str',
        vestiging_huisnummer                 => 'Str',
        vestiging_huisnummertoevoeging       => 'Str',
        vestiging_postcode                   => 'Str',
        vestiging_postcodewoonplaats         => 'Str',
        vestiging_straatnaam                 => 'Str',
        vestiging_woonplaats                 => 'Str',
        vestigingsnummer                     => 'Str',
        vorig_dossiernummer                  => 'Str',
        vorig_subdossiernummer               => 'Str',
        werkzamepersonen                     => 'Str',
    },
    defaults => {
        dossiernummer              => sub { generate_random_string('nnnnnnnn') },
        vestigingsnummer           => sub { generate_random_string('nnnnnnnnnnnn') },
        handelsnaam                => 'Mintlab',
        vestiging_landcode         => '6030',
        vestiging_postcode         => '1051 JL',
        vestiging_straatnaam       => 'Donker Curtiusstraat',
        vestiging_woonplaats       => 'Amsterdam',
        vestiging_huisnummer       => 7,
        vestiging_huisletter       => 'a',
        vestiging_huisnummertoevoeging => '521',
        rechtsvorm                 => '41',
        correspondentie_landcode   => '6030',
        correspondentie_postcode   => '1051 JL',
        correspondentie_straatnaam => 'Donker Curtiusstraat',
        correspondentie_woonplaats => 'Amsterdam',
        correspondentie_huisnummer => 7,
        correspondentie_huisletter       => 'a',
        correspondentie_huisnummertoevoeging => '521',
    },
};

my $strp = DateTime::Format::Strptime->new(
    pattern   => '%F',
    locale    => 'nl_NL',
    time_zone => 'Europe/Amsterdam',
);

use base 'Test::Builder::Module';
my $tb = __PACKAGE__->builder;

=head1 ATTRIBUTES

=head2 schema

A L<Zaaksysteem::Schema>

=cut

has basedir => (
    is => 'ro',
    isa => 'Str',
    lazy => 1,
    default => sub {
        return catdir(
            dirname(__FILE__),    # us
            '..',                 # ZS/
            '..',                 # inc/
            '..',                 # t/
        );
    },
);

has schema => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Schema',
    default => sub { return TestSetup::connect_test_db_ok() },
);

=head2 object_model

A default instance of L<Zaaksysteem::Object::Model>.

Complements L</create_object_model_ok>, this attribute should be used in a
'singleton'/global fashion, conserving configuration and state. Use
C<create_object_model_ok> when a fresh instance is required.

=cut

has object_model => (
    is => 'ro',
    isa => 'Zaaksysteem::Object::Model',
    lazy => 1,
    default => sub {
        my $self = shift;

        return Zaaksysteem::Object::Model->new(
            schema => $self->schema,
        );
    },
);

has betrokkene_model => (
    is      => 'ro',
    isa     => 'Zaaksysteem::Betrokkene',
    lazy    => 1,
    default => sub {
        my $self = shift;
        Zaaksysteem::Betrokkene->new(
            'prod'  => 1,
            'dbic'  => $self->schema,
            'stash' => {},
            'log'   => Log::Log4perl->get_logger(ref($self->schema)),
        );
    },
);

use MockModel;
has c => (
    is => 'ro',
    #isa => 'Zaaksysteem::Object::Model',
    lazy => 1,
    default => sub { my $self = shift; return MockModel->new(zs => $self) },
);

has xmlcompile => (
    is      => 'ro',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return Zaaksysteem::XML::Compile->xml_compile;
    },
);

=head2 zaaktype_importer

A Zaaksysteem zaaktype importer object

=cut

has zaaktype_importer => (
    is      => 'ro',
    isa     => 'Zaaksysteem::Zaaktypen::Import',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return Zaaksysteem::Zaaktypen::Import->new(dbic => $self->schema, session => {});
    },
);

has mech            => (
    is      => 'ro',
    lazy    => 1,
    default => sub {
        my $self        = shift;

        die('Cannot start mechanizer, are you sure you ran test t/testclass/800-catalyst.pl?')
            unless $ENV{ZAAKSYSTEEM_SCHEMA};

        return Zaaksysteem::TestMechanize->new;
    }
);

=head2 config

A configuration as read by L<TestSetup::read_test_config>

=cut

has config => (
    is      => 'ro',
    lazy    => 1,
    default => sub { return TestSetup::read_test_config() },
);

=head1 METHODS

=head2 zs_transaction_ok

Wrapper for a database-transaction so you can add a description and
don't have to type rollback every time.

=head3 SYNOPSIS

    $zs->zs_transaction_ok(
        sub {
            ok(1, "I'm good");
            throws_ok(sub {die}, qr/die/, "I died");
        },
        "Description"
    );

=cut

sub zs_transaction_ok {
    my ($self, $coderef, $description, $commit) = @_;

    ok $self->schema->storage->txn_do(sub {
        $coderef->();
        if (defined $commit && $commit) {
            return 1;
        }
        $self->schema->txn_rollback();
        return 1;
    }), $description;
}

=head2 txn_ok

This method is an alias for L</zs_transaction_ok>.

=cut

sub txn_ok {
    return shift->zs_transaction_ok(@_);
}

=head2 BUILD

Set default values on the schema, as would be done by Catalyst.

=head3 ARGUMENTS

Nothing.

=head3 RETURNS

Nothing.

=cut

sub BUILD {
    my $self = shift;

    my $basedn          = 'o=zaaksysteem,dc=zaaksysteem,dc=nl';
    my $customer_config = {
        start_config => {
            template    => 'zaak_v1',
            customer_id => 100,
            files       => 't/inc/files',
            publish     => 1,
            LDAP        => {basedn => $basedn}
        },
    };

    my $config = {
        LDAP => {
            hostname => 'localhost',
            port     => '389',
            admin    => 'cn=admin,dc=zaaksysteem,dc=nl',
            password => 'zaaksysteem123',
        },
        # this should go under customer_config, however
        # weak_ref constructs clean this up. needs some attention
        'gemeente' => {
            'adres' => 'Donker Curtiusstraat 7 - 521',
            'email' => 'servicedesk@mintlab.nl',
            'faxnummer' => '',
            'gemeente_id_url' => 'http://www.mintlab.nl/',
            'gemeente_portal' => 'http://www.mintlab.nl/',
            'huisnummer' => '7-521',
            'latitude' => '52.278',
            'longitude' => '5.163',
            'naam' => 'Mintlab',
            'naam_kort' => 'Mintlab',
            'naam_lang' => 'Mintlab B.V.',
            'postbus' => '',
            'postbus_postcode' => '',
            'postcode' => '1051 JL',
            'straatnaam' => 'Donker Curtiusstraat',
            'telefoonnummer' => '020 - 737 000 5',
            'website' => 'http://www.mintlab.nl/',
            'woonplaats' => 'Amsterdam',
            'zaak_email' => 'servicedesk@mintlab.nl'
        },
        home => "./../",
    };

    my $logger = Log::Log4perl->get_logger(ref $self->schema);
    my $cache  = Zaaksysteem::Cache->new(storage => {});

    $self->schema->catalyst_config($config);

    $self->schema->cache($cache);
    $self->schema->default_resultset_attributes->{cache}  = $cache;
    $self->schema->default_resultset_attributes->{log}    = $logger;
    $self->schema->default_resultset_attributes->{config} = $config;

    $self->_load_primary_user_and_groups;
}

=head2 _load_primary_user_and_groups

Arguments: none

Return value: $TRUE_OR_FALSE

    $self->_load_primary_user_and_groups;

Will load groups and roles when they do not exist yet, will also create a list of primary users.

=cut

sub _load_primary_user_and_groups {
    my $self        = shift;

    ### Check if we already have roles, groups, users
    $self->create_default_roles_ok unless $self->schema->resultset('Roles')->search->count;
    $self->create_default_groups_ok unless $self->schema->resultset('Groups')->search->count;

    ### Create default employees.
    for my $user (@{ DEFAULT_SUBJECTS() }) {
        ## Do not create user when it already exists
        next if $self->schema->resultset('Subject')->search({ username => $user->{username} })->count;

        ### Get interface, group and role
        my $interface   = $self->schema->resultset('Interface')->find_by_module_name('authldap');

        $tb->BAIL_OUT('Cannot find interface: authldap') unless $interface;

        my $group       = $self->schema->resultset('Groups')->search(
            {
                name    => $user->{group},
            }
        )->first;

        my $role        = $self->schema->resultset('Roles')->search(
            {
                name    => $user->{role},
            }
        )->first;

        my $user_entity = $self->schema->resultset('Subject')->create_user(
            {
                username        => $user->{username},
                interface       => $interface,
                sn              => 'User',
                givenname       => ucfirst($user->{username}),
                initials        => substr($user->{username},0,1) . '.',
                mail            => 'info@mintlab.nl',
                telephonenumber => '0207370005',
                displayname     => $user->{username} . ' User',
                group_id        => $group->id,
                $role ? (role_ids        => [$role->id]) : (),
                set_behandelaar => 1,
            }
        );

        my $subject = $self->schema->resultset('Subject')->find($user_entity->get_column('subject_id'));

        ### Make sure displayname is filled with long string:
        $subject->update_user({displayname => 'betrokkene-medewerker-' . $subject->id, password        => $user->{username}}, $user_entity);
        # $subject->update;
    }
}

=head2 generate_random_string

Generate a random string

=head3 SYNOPSIS

    my $random_stuff = Zaaksysteem::TestUtils::generate_random_string();

=head3 ARGUMENTS

A pattern as described by L<String::Random>, defaults to C<CCccnCCccn>

=head3 RETURNS

A random string, generated by L<String::Random>

=cut

sub generate_random_string {
    my $rnd = String::Random->new;
    my $pattern = shift || "CCccnCCccn";
    return $rnd->randpattern($pattern);
}

sub generate_bsn {
    my $bsn = generate_random_string('n' x 9);

    require Zaaksysteem::Tools;
    Zaaksysteem::Tools->import('elfproef');
    if (elfproef($bsn, 1)) {
        return $bsn;
    }
    return generate_bsn();
}

sub generate_uuid {
    my $self = shift;
    return Data::UUID->new->create_str();
}

=head2 open_testsuite_mail

Helper function to open and read mail files which we can then use to insert into parsers and whatnot

=cut

sub open_testsuite_mail {
    my ($self, $file) = @_;
    return $self->slurp_file(catfile(qw(t data mails), $file));
}

=head2 open_testsuite_json

Helper function to open JSON files for use in the testsuite. We only look into the directory t/inc/json, if you want to open other json files, you might want to alter this sub.

=cut

sub open_testsuite_json {
    my ($self, $file) = @_;
    return $self->slurp_file(catfile(qw(t inc json), $file));
}

sub open_stuf_xml {
    my ($self, $version, $file) = @_;
    return $self->slurp_file(catfile(TestSetup::STUF_TEST_XML_PATH(), $version, $file));
}

=head2 slurp_file

Helper function to slurp files

=cut

sub slurp_file {
    my ($self, $path) = @_;
    use autodie;
    open my $fh, '<', $path;
    my $f;
    { local $/; $f = <$fh>; };
    close($fh);
    return $f;
}

sub slurp {
    my $self = shift;
    use autodie;
    return scalar io->catfile(@_)->slurp;
}

=head2 connect_ldap_ok

Get an instance of a mocked L<Zaaksysteem::Backend::LDAP::Model> instance.

=cut

sub connect_ldap_ok {
    my $self = shift;

    die "Deprecated function used";

    my $model = Zaaksysteem::Backend::LDAP::Model->new(
        ldap => Net::LDAP->new('localhost'),
        base_dn => 'o=zaaksysteem,dc=zaaksysteem,dc=nl',
        ldapconfig => { rolefilter => '(&(objectClass=posixGroup)(memberUid=%s))' },
        ldapcache => {}
    );

    isa_ok $model, 'Zaaksysteem::Backend::LDAP::Model', 'LDAP Model constructs';

    return $model;
}

=head2 _create_zaaksysteem_ok

Create database objects for the testsuite.

=head3 SYNOPSIS

    my $result = $zs->_create_from_zaaksysteem_ok(
        resultset => 'Foo',
        arguments => { bar => 'baz' },
    );

=head3 ARGUMENTS

=over

=item resultset

The resultset you want to use

=item arguments

The arguments you want to pass to the create call

=back

=head3 RETURNS

The object you wanted.

=cut

define_profile _create_zaaksysteem_ok => (
    required => [qw/resultset arguments/],
    typed    => {
        resultset => 'Str',
        arguments => 'HashRef',
    },
);

sub _create_zaaksysteem_ok {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    my $res =
      $self->schema->resultset($opts->{resultset})->create($opts->{arguments});
    return $res;
}

=head2 create_bibliotheek_categorie_ok

Creates a library category in Zaaksysteem.
All arguments are optional

=head3 SYNOPSIS

    my $cat = $zs->create_bibliotheek_categorie_ok();
    # or
    my $cat = $zs->create_bibliotheek_categorie_ok(
        naam        => 'My name',
        label       => 'My label',
        help        => 'My help',
        description => 'My description',
        parent      => Zaaksysteem::Schema::BibliotheekCategorie,
        system      => an integer,
    );

=head3 ARGUMENTS

=over

=item naam

Defaults to something random

=item label

Defaults to something random

=item help

Defaults to something random

=item description

Defaults to something random

=item parent

An L<Zaaksysteem::Schema::BibliotheekCategorie> object

=item description

Defaults to something random

=back

=head3 RETURNS

A L<Zaaksysteem::Schema::BibliotheekCategorie> object.

=cut

define_profile create_bibliotheek_categorie_ok => (
    optional => [qw/naam label description help parent system/],
    typed    => {
        naam        => 'Str',
        label       => 'Str',
        description => 'Str',
        help        => 'Str',
        parent      => 'Zaaksysteem::Schema::BibliotheekCategorie',
        system      => 'Int',
    },
    defaults => {
        naam        => sub { generate_random_string },
        label       => sub { generate_random_string },
        description => sub { generate_random_string },
        help        => sub { generate_random_string },
    },
);

sub create_bibliotheek_categorie_ok {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    if (defined $opts->{parent}) {
        my $c = delete $opts->{parent};
        $opts->{pid} = $c->id;
    }

    return $self->_create_zaaksysteem_ok(
        resultset => 'BibliotheekCategorie',
        arguments => $opts,
    );
}

=head2 create_logging_ok

Creates a logging entry in Zaaksysteem.


=head3 ARGUMENTS

=over

=item event_type

=item event_data

=back

=head3 RETURNS

A L<Zaaksysteem::Schema::Logging> object.

=cut

define_profile create_logging_ok => (
    optional => {
        event_type => 'Str',
        event_data => 'HashRef',
    },
    defaults => {
        event_type => 'foo/bar',
        event_data => sub { return { some => 'data' } },
    }
);

sub create_logging_ok {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    $opts->{event_data} = JSON->new->canonical->encode($opts->{event_data});

    return $self->_create_zaaksysteem_ok(
        resultset => 'Logging',
        arguments => $opts,
    );
}

=head2 create_sjabloon_ok

Creates a library template in Zaaksysteem.

=head3 SYNOPSIS

    my $subject = $zs->create_sjabloon_ok();
    # or
    my $subject = $zs->create_sjabloon_ok(
        naam                  => 'My name',
        label                 => 'My label',
        help                  => 'My help',
        description           => 'My description',
        bibliotheek_categorie => Zaaksysteem::Schema::BibliotheekCategorie,
        filestore             => Zaaksysteem::Schema::Filestore,
    );


=head3 ARGUMENTS

=over

=item naam

Defaults to something random

=item label

Defaults to something random

=item help

Defaults to something random

=item description

Defaults to something random

=item bibliotheek_categorie

An L<Zaaksysteem::Schema::BibliotheekCategorie> object

=item filestore

An L<Zaaksysteem::Schema::Filestore> object

=back

=head3 RETURNS

A L<Zaaksysteem::Schema::BibliotheekSjablonen> object.

=cut

define_profile create_sjabloon_ok => (
    optional => {
        naam                  => 'Str',
        label                 => 'Str',
        description           => 'Str',
        help                  => 'Str',
        bibliotheek_categorie => 'Zaaksysteem::Schema::BibliotheekCategorie',
        filestore             => 'Zaaksysteem::Schema::Filestore',
        template_uuid         => UUID,
        interface             => 'Zaaksysteem::Model::DB::Interface',
    },
    defaults => {
        naam        => sub { generate_random_string },
        label       => sub { generate_random_string },
        description => sub { generate_random_string },
        help        => sub { generate_random_string },
    });

sub create_sjabloon_ok {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    if (!defined $opts->{bibliotheek_categorie}) {
        $opts->{bibliotheek_categorie} = $self->create_bibliotheek_categorie_ok();
    }
    my $c = delete $opts->{bibliotheek_categorie};
    $opts->{bibliotheek_categorie_id} = $c->id;

    if (defined $opts->{template_uuid}|| defined $opts->{interface}) {
        $opts->{template_uuid} //= $self->generate_uuid;
        my $interface = delete $opts->{interface} || $self->create_interface_ok();
        $opts->{interface_id} = $interface->id;
    }
    elsif (!defined $opts->{filestore}) {
        my $dir  = 't/inc/Documents';
        my $file = 'openoffice_document.odt';
        $opts->{filestore} = $self->create_filestore_ok(
            file_path     => catfile($dir, $file),
            original_name => $file,
        );
    }

    my $fs = delete $opts->{filestore};
    $opts->{filestore_id} = $fs->id if $fs;

    return $self->_create_zaaksysteem_ok(
        resultset => 'BibliotheekSjablonen',
        arguments => $opts
    );
}

=head2 create_notificatie_ok

Creates a bibliotheek notificatie

=head3 SYNOPSIS

    my $bn = create_notificatie_ok(
        meuk
    );

=head3 ARGUMENTS

=over

=item object_type

iets

=item bibliotheek_categorie

L<Zaaksysteem::Model::DB::BibliotheekCategorie>

=item label

String

=item subject

String

=item message

String

=item created

L<DateTime>

=item last_modified

L<DateTime>

=item deleted

L<DateTime>

=back

=head3 RETURNS

An L<Zaaksysteem::Model::DB::BibliotheekNotificaties> object

=cut

define_profile create_notificatie_ok => (
    optional => [qw(
        bibliotheek_categorie
        object_type
        label
        subject
        message
        created
        last_modified
        deleted
    )],
    typed => {
        bibliotheek_categorie => 'Zaaksysteem::Model::DB::BibliotheekCategorie',
        object_type           => 'Str',
        label                 => 'Str',
        subject               => 'Str',
        message               => 'Str',
        created               => 'DateTime',
        last_modified         => 'DateTime',
        deleted               => 'DateTime',
    },
    defaults => {
        label         => sub { generate_random_string },
        subject       => sub { generate_random_string },
        message       => sub { generate_random_string },
        created       => DateTime->now(),
        last_modified => DateTime->now(),
    },
);

sub create_notificatie_ok {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    my $cat = delete $args->{bibliotheek_categorie} || $self->create_bibliotheek_categorie_ok();
    $args->{bibliotheek_categorie_id} = $cat->id;

    return $self->_create_zaaksysteem_ok(
        resultset => 'BibliotheekNotificaties',
        arguments => $args
    );
}

=head2 create_zaaktype_notificatie_ok

Creates a zaaktype notificatie

=head3 SYNOPSIS

    my $ztn = create_zaaktype_notificatie_ok();

=head3 ARGUMENTS

=over

=item object_type

=item bibliotheek_notificaties

L<Zaaksysteem::Model::DB::BibliotheekNotificaties>

=item label

String

=item subject

String

=item message

String

=item created

L<DateTime>

=item last_modified

L<DateTime>

=item deleted

L<DateTime>

=back

=head3 RETURNS

An L<Zaaksysteem::Model::DB::ZaaktypeNotificatie> object

=cut

define_profile create_zaaktype_notificatie_ok => (
    optional => [qw(
        automatic
        behandelaar
        bericht
        bibliotheek_notificatie
        created
        email
        intern_block
        label
        last_modified
        node
        onderwerp
        rcpt
        status
    )],
    typed => {
        bibliotheek_notificatie => 'Zaaksysteem::Model::DB::BibliotheekNotificaties',
        node          => 'Zaaksysteem::Schema::ZaaktypeNode',
        status        => 'Zaaksysteem::Schema::ZaaktypeStatus',
        automatic     => 'Int',
        behandelaar   => 'Str',
        bericht       => 'Str',
        email         => 'Str',
        intern_block  => 'Int',
        label         => 'Str',
        onderwerp     => 'Str',
        rcpt          => 'Str',
        created       => 'DateTime',
        last_modified => 'DateTime',
      },
    defaults => {
        created       => DateTime->now(),
        last_modified => DateTime->now(),
        automatic     => 1,
        behandelaar   => sub { generate_random_string },
        bericht       => sub { generate_random_string },
        email         => 'devnull@zaaksysteem.nl',
        intern_block  => 1,
        label         => sub { generate_random_string },
        onderwerp     => sub { generate_random_string },
        rcpt          => sub { generate_random_string },
    },
);

sub create_zaaktype_notificatie_ok {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    my $notif = delete $args->{bibliotheek_notificatie} || $self->create_notificatie_ok();
    $args->{bibliotheek_notificaties_id} = $notif->id;

    my $status = $self->_create_from_node_or_status(
        node => delete $args->{node},
        status => delete $args->{status}
    );
    $args->{zaak_status_id} = $status->id;
    $args->{zaaktype_node_id} = $status->zaaktype_node_id->id;

    return $self->_create_zaaksysteem_ok(
        resultset => 'ZaaktypeNotificatie',
        arguments => $args
    );
}

=head2 create_zaaktype_betrokkene_ok

=over 4

=item Arguments: %params

=item Return Value: $zaaktype_betrokkene_row_id

=back

    my $ztn = create_zaaktype_betrokkene_ok();


Creates a zaaktype betrokkene.

B<%Params>

=over 4

=item node

Type: Zaaksysteem::Schema::ZaaktypeNode

=item betrokkene_type [optional]

Type: ENUM (natuurlijk_persoon, niet_natuurlijk_persoon, medewerker

=item preset_client [optional]

The client to preset this casetype to

=back

=cut

define_profile create_zaaktype_betrokkene_ok => (
    required    => {
        node            => 'Zaaksysteem::Schema::ZaaktypeNode',
    },
    optional => {
        betrokkene_type => SubjectType,
        preset_client   => Betrokkene
    },
);

sub create_zaaktype_betrokkene_ok {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    my @gen_betrokkene_types = qw/natuurlijk_persoon niet_natuurlijk_persoon medewerker/;

    my $node            = $args->{node};
    my $betrokkene_type = $args->{betrokkene_type};

    if ($args->{preset_client}) {
        $betrokkene_type = 'preset_client';

        $node->zaaktype_definitie_id->preset_client($args->{preset_client});
        $node->zaaktype_definitie_id->update;
    } else {
        if (!$betrokkene_type) {
            my @existing_betrokkenen = $node->zaaktype_betrokkenen->search->get_column('betrokkene_type')->all;

            for my $gen_betrokkene_type (@gen_betrokkene_types) {
                ### Leave this loop when it is not already set in database
                if (!grep { $gen_betrokkene_type eq $_ } @existing_betrokkenen) {
                    $betrokkene_type = $gen_betrokkene_type;
                    last;
                }
            }
        }

        if (!$betrokkene_type) {
            throw('ZS/Testsuite/create_zaaktype_betrokkene_ok', "All possible betrokkene_types are already set on this casetype");
        }
    }

    return $self->_create_zaaksysteem_ok(
        resultset => 'ZaaktypeBetrokkenen',
        arguments => {
            zaaktype_node_id    => $node->id,
            betrokkene_type     => $betrokkene_type
        }
    );
}




=head2 create_bibliotheek_kenmerk_ok

Create a kenmerk (label?) in zaaksysteem.
All arguments are optional.

=head3 SYNOPSIS

    my $kenmerk = $zs->create_bibliotheek_kenmerk_ok();

=head3 ARGUMENTS

=over

=item naam

=item label

=item description

=item help

=item magic_string

=item bibliotheek_categorie

An L<Zaaksysteem::Schema::BibliotheekCategorie> object

=item document_categorie

=item file_metadata

=item system

=item type_multiple

=item value_default

=item value_type

=back

=head3 RETURNS

A L<Zaaksysteem::Schema::Kenmerk> object.

=cut

define_profile create_bibliotheek_kenmerk_ok => (
    optional => {
        bibliotheek_categorie => 'Zaaksysteem::Schema::BibliotheekCategorie',
        description           => 'Str',
        document_categorie    => 'Str',
        file_metadata         => 'Str',
        help                  => 'Str',
        label                 => 'Str',
        magic_string          => 'Str',
        naam                  => 'Str',
        system                => 'Bool',
        type_multiple         => 'Bool',
        value_default         => 'Str',
        value_type            => 'Str',
        values                => 'Defined',
        version               => 'Int'
    },
    defaults => {
        naam          => sub { generate_random_string },
        value_type    => 'text',
        help          => 'Some help',
        description   => 'Some description',
        type_multiple => 0,
        system        => 0,

    },
    constraint_methods => {
        value_type => sub {
            my $val = pop;
            return 0 unless defined $val;
            return $val
                =~ /^(text_uc|checkbox|richtext|date|file|bag_straat_adres|email|valutaex|bag_openbareruimte|text|bag_openbareruimtes|url|valuta|option|bag_adres|select|valutain6|valutaex6|valutaex21|image_from_url|bag_adressen|valutain|calendar|calendar_supersaas|subject|bag_straat_adressen|googlemaps|numeric|valutain21|textarea|bankaccount)$/;
        },
    }
);

sub create_bibliotheek_kenmerk_ok {
    my ($self, %params) = @_;
    my $opts = assert_profile(\%params)->valid;

    unless (exists $opts->{ bibliotheek_categorie }) {
        $opts->{ bibliotheek_categorie } = $self->create_bibliotheek_categorie_ok();
    }

    $opts->{magic_string} //= $opts->{naam};

    $opts->{ bibliotheek_categorie_id } = delete($opts->{ bibliotheek_categorie })->id;
    my $values = delete $opts->{ values };

    my $bk = $self->_create_zaaksysteem_ok(
        resultset => 'BibliotheekKenmerken',
        arguments => $opts
    );

    return $bk unless defined $values;

    my $value_profile = build_profile(
        required => { value => 'Str' },
        optional => {
            active => 'Bool',
            sort_order => 'Int'
        }
    );

    for my $value (@{ $values }) {
        my $create_args = assert_profile($value, profile => $value_profile)->valid;

        $bk->bibliotheek_kenmerken_values->create($create_args);
    }

    return $bk;
}


=head2 create_zaaktype_regel_ok

Create a zaaktype regel

=head3 SYNOPSIS

    my $regel = $zs->create_zaaktype_regel_ok(
        status   => $zaaktype_status,
        node     => $zaaktype_node,
        naam     => 'test regel',
        settings => {%settings}
    );

=head3 ARGUMENTS

=over

=item node

L<Zaaksysteem::Schema::ZaaktypeNode>

=item status

L<Zaaksysteem::Schema::ZaaktypeStatus>

=item settings

HashRef

=item naam

String

=back

=head3 RETURNS

A L<Zaaksysteem::Model::DB::ZaaktypeRegel> object

=cut

define_profile create_zaaktype_regel_ok => (
    optional => {
        status                => 'Zaaksysteem::Schema::ZaaktypeStatus',
        node                  => 'Zaaksysteem::Schema::ZaaktypeNode',
        naam                  => 'Str',
        settings              => 'HashRef',
        created               => 'DateTime',
        last_modified         => 'DateTime',
    },
    defaults => {
        naam => sub { generate_random_string },
        settings => sub { return {} },
    },
);

sub create_zaaktype_regel_ok {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    use Zaaksysteem::Backend::Rules::Serializer;
    my $serializer = Zaaksysteem::Backend::Rules::Serializer->new;

    my $status = $self->_create_from_node_or_status(
        node   => delete $args->{node},
        status => delete $args->{status}
    );
    $args->{zaak_status_id} = $status->id;
    $args->{zaaktype_node_id} = $status->zaaktype_node_id->id;

    $args->{settings} = $serializer->encode($args->{settings});

    return $self->_create_zaaksysteem_ok(
        resultset => 'ZaaktypeRegel',
        arguments => $args
    );
}

=head2 create_zaaktype_sjabloon_ok

Creates a case type template

=head3 SYNOPSIS

    my $zt_sjabloon = $zs->create_zaaktype_sjabloon_ok();

=head3 ARGUMENTS

=over

=item automatisch_genereren

Boolean

=item bibliotheek_kenmerk

L<Zaaksysteem::Model::DB::BibliotheekKenmerken>

=item bibliotheek_sjabloon

L<Zaaksysteem::Model::DB::BibliotheekSjablonen>

=item created

L<DateTime> Object

=item help

Defaults to a random string

=item last_modified

L<DateTime> Object

=item node

L<Zaaksysteem::Schema::ZaaktypeNode>, conflicts with status

=item status

L<Zaaksysteem::Schema::ZaaktypeStatus>, conflicts with node.

=item target_format

'pdf' or 'odt', defaults to 'odt'

=back

=head3 RETURNS

A L<Zaaksysteem::Model::DB::ZaaktypeSjablonen> object

=cut

define_profile create_zaaktype_sjabloon_ok => (
    optional => [qw(
          automatisch_genereren
          bibliotheek_kenmerk
          bibliotheek_sjabloon
          created
          help
          last_modified
          node
          status
          target_format
          )
    ],
    constraint_methods => {
        target_format => SJABLONEN_TARGET_FORMATS,
    },
    typed => {
        bibliotheek_sjabloon  => 'Zaaksysteem::Model::DB::BibliotheekSjablonen',
        bibliotheek_kenmerk   => 'Zaaksysteem::Model::DB::BibliotheekKenmerken',
        status                => 'Zaaksysteem::Schema::ZaaktypeStatus',
        node                  => 'Zaaksysteem::Schema::ZaaktypeNode',
        help                  => 'Str',
        created               => 'DateTime',
        last_modified         => 'DateTime',
        automatisch_genereren => 'Bool',
        target_format         => 'Str',
    },
    defaults => {
        help                  => sub { generate_random_string },
        created               => DateTime->now(),
        last_modified         => DateTime->now(),
        automatisch_genereren => 1,
        target_format         => 'odt',
    },
);

sub create_zaaktype_sjabloon_ok {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    if ($args->{node} && $args->{status}) {
        throw('ZS/Testsuite', "Conflicting arguments: node and status");
    }

    my $bk = delete $args->{bibliotheek_kenmerk} || $self->create_bibliotheek_kenmerk_ok();
    $args->{bibliotheek_kenmerken_id} = $bk->id;

    my $bs = delete $args->{bibliotheek_sjabloon} || $self->create_sjabloon_ok();
    $args->{bibliotheek_sjablonen_id} = $bs->id;

    my $node = delete $args->{node} || $self->create_zaaktype_node_ok();
    my $status = delete $args->{status} || $self->create_zaaktype_status_ok(node => $node);
    $args->{zaak_status_id}   = $status->id;
    $args->{zaaktype_node_id} = $status->zaaktype_node_id->id;

    return $self->_create_zaaksysteem_ok(
        resultset => 'ZaaktypeSjablonen',
        arguments => $args
    );
}

=head2 create_zaaktype_kenmerk_ok

Creates a zaaktype kenmerk

=head3 SYNOPSIS

    $zs->create_zaaktype_kenmerk_ok;

=head3 ARGUMENTS

=over

=item bag_zaakadres

=item bibliotheek_kenmerk

L<Zaaksysteem::Schema::BibliotheekKenmerken>

=item date_fromcurrentdate

=item help

=item help_extern

=item is_group

=item is_systeemkenmerk

=item label

=item pip

=item pip_can_change

=item publish_public

=item referential

=item required_permissions

=item value_default

=item value_mandatory

=item version

=item status

L<Zaaksysteem::Schema::ZaaktypeStatus>

=item zaakinformatie_view

=item node

L<Zaaksysteem::Schema::ZaaktypeNode>

=back

=head3 RETURNS

An L<Zaaksysteem::Model::DB::ZaaktypeKenmerken> object

=cut

define_profile create_zaaktype_kenmerk_ok => (
    optional => [qw(
        bag_zaakadres
        bibliotheek_kenmerk
        date_fromcurrentdate
        help
        help_extern
        is_group
        is_systeemkenmerk
        label
        pip
        pip_can_change
        publish_public
        referential
        required_permissions
        value_default
        value_mandatory
        version
        status
        zaakinformatie_view
        node
        object_type
        object_metadata
    )],
    typed => {
        bag_zaakadres        => 'Int',
        bibliotheek_kenmerk  => 'Zaaksysteem::Schema::BibliotheekKenmerken',
        date_fromcurrentdate => 'Int',
        help                 => 'Str',
        help_extern          => 'Str',
        is_group             => 'Int',
        is_systeemkenmerk    => 'Bool',
        label                => 'Str',
        node                 => 'Zaaksysteem::Schema::ZaaktypeNode',
        pip                  => 'Int',
        pip_can_change       => 'Bool',
        publish_public       => 'Int',
        referential          => 'Str',
        required_permissions => 'HashRef',
        status               => 'Zaaksysteem::Schema::ZaaktypeStatus',
        value_default        => 'Str',
        value_mandatory      => 'Int',
        version              => 'Int',
        zaakinformatie_view  => 'Int',
        object_type          => 'Zaaksysteem::Object::Types::Type',
        object_metadata      => 'HashRef',
      },
    constraint_methods => {},
    defaults           => {
        help              => sub { generate_random_string },
        help_extern       => sub { generate_random_string },
        is_systeemkenmerk => 0,
        label             => sub { generate_random_string },
        pip_can_change    => 1,
        publish_public    => 1,
        version           => 1,
    },
);

sub create_zaaktype_kenmerk_ok {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    my $bk = delete $args->{bibliotheek_kenmerk} || $self->create_bibliotheek_kenmerk_ok();
    $args->{bibliotheek_kenmerken_id} = $bk->id;

    if ($args->{node} && $args->{status}) {
        throw('ZS/Testsuite', "Conflicting arguments: node and status");
    }

    my $node = delete $args->{node} || $self->create_zaaktype_node_ok();
    my $status = delete $args->{status} || $self->create_zaaktype_status_ok(node => $node);

    $args->{zaak_status_id}   = $status->id;
    $args->{zaaktype_node_id} = $status->zaaktype_node_id->id;

    $args->{required_permissions} = JSON::encode_json($args->{required_permissions}) if $args->{required_permissions};

    my $object_type = delete $args->{ object_type };

    if ($object_type) {
        delete $args->{bibliotheek_kenmerken_id};
        $args->{ object_id } = $object_type->id;
    }

    return $self->_create_zaaksysteem_ok(
        resultset => 'ZaaktypeKenmerken',
        arguments => $args
    );
}

=head2 create_subject_ok

Create a subject (user) in zaaksysteem.

=head3 SYNOPSIS

    my $subject = $zs->create_subject_ok();
    # or
    my $subject = $zs->create_subject_ok(
        username     => 'myusername',
        subject_type => 'employee',
        properties   => [ 'my properties' ],
        settings     => [ 'my settings' ],
    );

=head3 ARGUMENTS

=over

=item username

=item subject_type

Scalar value, currently only 'employee' is accepted.

=back

=head3 RETURNS

A L<Zaaksysteem::Schema::Subject> object.

=cut

define_profile create_subject_ok => (
    optional => {
        subject_type => 'Str',
        username     => 'Str',
        role_ids     => 'Any', # Because arrayrefs don't work
        group_ids    => 'Any', # Because arrayrefs don't work
        properties   => 'HashRef',
    },
    constraint_methods => {
        subject_type => qr/^employee$/,
    },
    defaults => {
        username     => sub { generate_random_string },
        subject_type => "employee",
        group_ids    => sub { return [3] },  # Devops
        role_ids     => sub { return [12] }, # Behandelaar
    },
);

sub create_subject_ok {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    ### Return user when already exists
    if ($opts->{username}) {
        my $subject = $self->schema->resultset('Subject')->find({username => $opts->{username}});

        return $subject if $subject;
    }

    my $ldap_data = ($opts->{properties} || {
        cn              => $opts->{username},
        sn              => $opts->{username},
        displayname     => $opts->{username},
        givenname       => $opts->{username},
        initials        => $opts->{username},
        mail            => 'devnull@zaaksysteem.nl',
        telephonenumber => '0612345678',
    });

    my $interface   = $self->schema->resultset('Interface')->find_by_module_name('authldap');

    return $self->schema->resultset('Subject')->create(
        {
            subject_type        => 'employee',
            properties          => $ldap_data,
            username            => $opts->{username},
            role_ids            => $opts->{role_ids},
            group_ids           => $opts->{group_ids},
        }
    );

}

sub create_default_groups_ok {
    my $self        = shift;

    my $parent      = $self->schema->resultset('Groups')->create_group(
        {
            name        => "Mintlab",
            description => "Mintlab Test Organization",
        }
    );

    my $backoffice  = $self->schema->resultset('Groups')->create_group(
        {
            name            => "Backoffice",
            description     => "Where all the magic is created",
            parent_group_id => $parent->id,
        }
    );


    my $devops_in_backoffice = $self->schema->resultset('Groups')->create_group(
        {
            name            => "Devops",
            description     => "Mintlab Backoffice - Devops",
            parent_group_id => $backoffice->id,
        }
    );

    my $frontoffice = $self->schema->resultset('Groups')->create_group(
        {
            name            => "Frontoffice",
            description     => "Where all the magic is sold",
            parent_group_id => $parent->id,
        }
    );

    my $next      = $self->schema->resultset('Groups')->create_group(
        {
            name        => "Zaaksysteem",
            description => "Zaaksysteem Test Organization",
        }
    );
    my $kantine = $self->schema->resultset('Groups')->create_group(
        {
            name            => "Kantine",
            description     => "Where all the magic is sold",
            parent_group_id => $next->id,
        }
    );


    return [$parent, $backoffice, $devops_in_backoffice, $frontoffice];
}

sub create_default_roles_ok {
    my $self        = shift;
    my @roles;

    push(@roles,
        $self->schema->resultset('Roles')->create_role(
            {
                name        => $_,
                description => "Systeemgroep: " . $_,
                system_role => 1,
            }
        )
    ) for qw/
        Administrator
        Zaaksysteembeheerder
        Zaaktypebeheerder
        Zaakbeheerder
        Contactbeheerder
        Basisregistratiebeheerder
        Wethouder
        Directielid
        Afdelingshoofd
        Kcc-medewerker
        Zaakverdeler
        Behandelaar
    /;

    return \@roles;
}

=head2 create_userentity_ok

Create a subject (user) in zaaksysteem.

=head3 SYNOPSIS

    my $ue = $zs->create_userentity_ok();
    # or
    my $ue = $zs->create_userentity_ok(
        subject   => $subject,
        interface => $interface,
    );

=head3 ARGUMENTS

=over

=item subject

L<Zaaksysteem::Schema::Subject>

=item interface

L<Zaaksysteem::Schema::Interface>

=back

=head3 RETURNS

A L<Zaaksysteem::Schema::UserEntity> object.

=cut

define_profile create_userentity_ok => (
    optional => {
        subject    => 'Zaaksysteem::Schema::Subject',
        interface  => 'Zaaksysteem::Schema::Interface',
        active     => 'Bool',
        properties => 'HashRef',
    },
    defaults => {
        active => 1,
    }
);

sub create_userentity_ok {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    my $subject = delete $opts->{subject} || $self->create_subject_ok();
    $opts->{subject_id}        = $subject->id;
    $opts->{source_identifier} = $subject->username;

    my $interface = delete $opts->{interface} || $self->schema->resultset('Interface')->find_by_module_name('authldap');;
    $opts->{source_interface_id} = $interface->id;

    my $active = delete $opts->{active};
    if (!$active) {
        $opts->{date_deleted} = DateTime->now();
    }

    return $self->_create_zaaksysteem_ok(
        resultset => 'UserEntity',
        arguments => $opts
    );
}

=head2 create_medewerker_ok

Create a medewerker (employee) in zaaksysteem.

=head3 SYNOPSIS

    my $ue = $zs->create_medewerker_ok();
    # or
    my $ue = $zs->create_medewerker_ok(
        username   => $username,
        subject    => $subject,
        interface  => $interface,
        active     => [0, 1],
        properties => {key => 'value'},
    );

=head3 ARGUMENTS

=over

=item username

Conflicts with subject

=item subject

L<Zaaksysteem::Schema::Subject>, conflicts with username

=item interface

L<Zaaksysteem::Schema::Interface>

=item active

=item properties

=back

=head3 RETURNS

A L<Zaaksysteem::Schema::Subject> object.

=cut

define_profile create_medewerker_ok => (
    optional => {
        username   => 'Str',
        subject    => 'Zaaksysteem::Schema::Subject',
        interface  => 'Zaaksysteem::Schema::Interface',
        active     => 'Bool',
        properties => 'HashRef',
        group_ids  => 'Any',
        role_ids   => 'Any',
    },
    defaults => {
        active => 1,
    }
);

sub create_medewerker_ok {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    if (defined $opts->{username} && defined $opts->{subject}) {
        throw('/Zaaksysteem/Testsuite',
            "You cannot define a username and a subject");
    }

    my %subject_args;
    foreach (qw(group_ids role_ids username)) {
        $subject_args{$_} = delete $opts->{$_} if defined $opts->{$_};
    }

    if (keys %subject_args) {
        $opts->{subject} = $self->create_subject_ok(%subject_args);
    }
    return $self->create_userentity_ok(%$opts);
}

=head2 create_zaaktype_node_ok

Creates a node for a zaaktype

=head3 SYNOPSIS

    $zs->create_zaaktype_node_ok();

=head3 ARGUMENTS

=over

=item code

=item titel

=item trigger

=item toelichting

=item automatisch_behandelen

=item webform_toegang

=item webform_authenticatie

=item toewijzing_zaakintake

=item adres_relatie

=item aanvrager_hergebruik

=item active

=item version

=item zaaktype_definitie

L<Zaaksysteem::Schema::ZaaktypeDefinitie>

=back

=head3 RETURNS

=cut

define_profile create_zaaktype_node_ok => (
    optional => [
        qw/
            code
            titel
            trigger
            toelichting
            automatisch_behandelen
            webform_toegang
            webform_authenticatie
            toewijzing_zaakintake
            adres_relatie
            aanvrager_hergebruik
            active
            version
            zaaktype_definitie
            properties
            /
    ],
    typed => {
        aanvrager_hergebruik   => 'Bool',
        active                 => 'Int',
        adres_relatie          => 'Str',
        automatisch_behandelen => 'Bool',
        zaaktype_definitie     => 'Zaaksysteem::Schema::ZaaktypeDefinitie',
        code                   => 'Str',
        titel                  => 'Str',
        toelichting            => 'Str',
        toewijzing_zaakintake  => 'Bool',
        version                => 'Int',
        webform_authenticatie  => 'Bool',
        webform_toegang        => 'Bool',
        trigger                => 'Str',
    },
    constraint_methods => { trigger => ZAAKTYPE_TRIGGER, },
    defaults           => {
        aanvrager_hergebruik   => 0,
        active                 => 1,
        adres_relatie          => sub { return generate_random_string },
        automatisch_behandelen => 0,
        code                   => sub { return generate_random_string },
        titel                  => sub { return generate_random_string },
        toelichting            => sub { return generate_random_string },
        toewijzing_zaakintake  => 0,
        version                => 1,
        webform_authenticatie  => 0,
        webform_toegang        => 0,
        trigger                => 'extern',
        properties             => {
            aanleiding                   => '',
            archiefclassicatiecode       => '',
            bag                          => 'Nee',
            beroep_mogelijk              => 'Nee',
            doel                         => '',
            e_formulier                  => '',
            lex_silencio_positivo        => 'Nee',
            lock_registration_phase      => '1',
            lokale_grondslag             => '',
            opschorten_mogelijk          => 'Nee',
            pdc_tarief_balie             => '',
            pdc_tarief_behandelaar       => '',
            pdc_tarief_email             => '',
            pdc_tarief_post              => '',
            pdc_tarief_telefoon          => '',
            publicatie                   => 'Nee',
            publicatietekst              => '',
            queue_coworker_changes       => '1',
            verantwoordelijke            => '',
            verantwoordingsrelatie       => '',
            verdagingstermijn            => '',
            verlenging_mogelijk          => 'Nee',
            verlengingstermijn           => '',
            vertrouwelijkheidsaanduiding => '-',
            wet_dwangsom                 => 'Nee',
            wkpb                         => 'Nee'
        },
        zaaktype_trefwoorden  => 'Dit zijn trefwoorden test',
        zaaktype_omschrijving => 'Dit is een omschrijving',
    },
);

sub create_zaaktype_node_ok {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    my $zt = delete $opts->{zaaktype_definitie} || $self->create_zaaktype_definitie_ok();
    $opts->{zaaktype_definitie_id} = $zt->id;

    return $self->_create_zaaksysteem_ok(
        resultset => 'ZaaktypeNode',
        arguments => $opts
    );
}

=head2 create_zaaktype_status_ok

Creates a status for a zaaktype

=head3 SYNOPSIS

    $zs->create_zaaktype_status_ok

=head3 ARGUMENTS

=over

=item status

=item status_type

=item naam

=item ou_id

=item role_id

=item checklist

=item fase

=item role_set

=item node

L<Zaaksysteem::Schema::ZaaktypeNode>

=back

=head3 RETURNS

L<Zaaksysteem::Schema::ZaaktypeStatus>

=cut

define_profile create_zaaktype_status_ok => (
    optional => [qw(
        status
        status_type
        naam
        ou_id
        role_id
        checklist
        fase
        role_set
        node
    )],
    typed => {
        node        => 'Zaaksysteem::Schema::ZaaktypeNode',
        status      => 'Int',
        status_type => 'Str',
        naam        => 'Str',
        ou_id       => 'Int',
        role_id     => 'Int',
        fase        => 'Str',
        role_set    => 'Int',
      },
    defaults => {
        status      => 1,
        naam        => sub { return generate_random_string },
        fase        => sub { return generate_random_string },
        ou_id       => 1,
        role_id     => 1,
      },
);

sub create_zaaktype_status_ok {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    my $n = delete $opts->{node} || $self->create_zaaktype_node_ok();
    $opts->{zaaktype_node_id} = $n->id;

    return $self->_create_zaaksysteem_ok(
        resultset => 'ZaaktypeStatus',
        arguments => $opts
    );
}

=head2 create_zaaktype_definitie_ok

Creates a zaaktype definitie

=head3 SYNOPSIS

    $zs->create_zaaktype_definitie_ok

=head3 ARGUMENTS

=over

=item openbaarheid

=item handelingsinitiator

=item grondslag

=item procesbeschrijving

=item afhandeltermijn

=item afhandeltermijn_type

=item selectielijst

=item servicenorm

=item servicenorm_type

=item pdc_voorwaarden

=item pdc_description

=item pdc_meenemen

=item pdc_tarief

=item omschrijving_upl

=item aard

=item extra_informatie

=item preset_client

=back

=head3 RETURNS

L<Zaaksysteem::Schema::ZaaktypeDefinitie>

=cut

define_profile create_zaaktype_definitie_ok => (
    optional => [qw(
          openbaarheid
          handelingsinitiator
          grondslag
          procesbeschrijving
          afhandeltermijn
          afhandeltermijn_type
          selectielijst
          servicenorm
          servicenorm_type
          pdc_voorwaarden
          pdc_description
          pdc_meenemen
          pdc_tarief
          omschrijving_upl
          aard
          extra_informatie
          preset_client
          )
    ],
    typed => {
        openbaarheid         => 'Str',
        handelingsinitiator  => 'Str',
        grondslag            => 'Str',
        procesbeschrijving   => 'Str',
        afhandeltermijn      => 'Str',
        afhandeltermijn_type => 'Str',
        selectielijst        => 'Str',
        servicenorm          => 'Str',
        servicenorm_type     => 'Str',
        pdc_voorwaarden      => 'Str',
        pdc_description      => 'Str',
        pdc_meenemen         => 'Str',
        pdc_tarief           => 'Str',
        omschrijving_upl     => 'Str',
        aard                 => 'Str',
        extra_informatie     => 'Str',
        preset_client        => 'Str',
    },
    constraint_methods => {
        servicenorm_type     => SERVICE_NORM_TYPES_OK,
        afhandeltermijn_type => SERVICE_NORM_TYPES_OK,
    },
    defaults => {
        grondslag            => sub { return generate_random_string },
        servicenorm          => 5,
        servicenorm_type     => 'kalenderdagen',
        afhandeltermijn      => 10,
        afhandeltermijn_type => 'kalenderdagen',
        handelingsinitiator  => sub { generate_random_string },
        procesbeschrijving   => sub { generate_random_string },
        selectielijst        => sub { generate_random_string },
        openbaarheid         => sub { generate_random_string },
    },
);

sub create_zaaktype_definitie_ok {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    return $self->_create_zaaksysteem_ok(
        resultset => 'ZaaktypeDefinitie',
        arguments => $opts
    );
}

=head2 create_zaaktype_authorisation_ok

Get an instance of L<Zaaksysteem::DB::Component::ZaaktypeAuthorisation>.

C<_sync_object> will be called on the instance in order to build a
L<Zaaksysteem::Backend::Object::Data::Component> reflecting this permission.

=head3 ARGUMENTS

=over 4

=item zaaktype (optional)

Must be an instance of L<Zaaksysteem::DB::Component::Zaaktype>. If not provided
it will be implicitly created via C<< $self->create_zaaktype_ok >>.

=item ou_id (optional)

ou_id for the department / organisational unit that defines which ou gets this
permission. Defaults to C<10001>.

=item role_id (optional)

role_id for the role that defines which role gets this permission. Defaults to
C<20002>.

=item recht (optional)

Textual string representing the permission to be added. Defaults to
C<zaak_read>.

=back

=cut

define_profile create_zaaktype_authorisation_ok => (
    optional => {
        zaaktype     => 'Zaaksysteem::Model::DB::Zaaktype',
        ou_id        => 'Int',
        role_id      => 'Int',
        recht        => 'Str',
        confidential => 'Bool',
    },
    defaults => {
        ou_id        => 3,             # devops
        role_id      => 12,            # behandelaar
        recht        => 'zaak_read',
        confidential => 0,
    },
);

sub create_zaaktype_authorisation_ok {
    my ($self, %params) = @_;

    my $opts = assert_profile(\%params)->valid;

    my $zaaktype = delete $opts->{ zaaktype } || $self->create_zaaktype_ok;

    my $auth = $self->_create_zaaksysteem_ok(
        resultset => 'ZaaktypeAuthorisation',
        arguments => {
            zaaktype_id  => $zaaktype->id,
            ou_id        => $opts->{ou_id},
            role_id      => $opts->{role_id},
            recht        => $opts->{recht},
            confidential => $opts->{confidential},
        }
    );

    $zaaktype->_sync_object($self->object_model);

    return $auth;
}

=head2 create_zaaktype_ok

Creates a zaaktype

=head3 SYNOPSIS

    $zs->create_zaaktype_ok

=head3 ARGUMENTS

=over

=item version

Conflicts with node

=item active

Conflicts with node

=item category

L<Zaaksysteem::Schema::BibliotheekCategorie>

=item node

L<Zaaksysteem::Schema::ZaaktypeNode>, conflicts with version and active

=back

=head3 RETURNS

L<Zaaksysteem::Schema::Zaaktype>

=cut

define_profile create_zaaktype_ok => (
    optional => [
        qw/
            version
            active
            category
            node
            search_term
            /
    ],
    typed => {
        version     => 'Int',
        active      => 'Bool',
        category    => 'Zaaksysteem::Schema::BibliotheekCategorie',
        node        => 'Zaaksysteem::Schema::ZaaktypeNode',
        search_term => 'Str',
    },

    #conflicts => {
    #  node => [ active, version ],
    #  active => [ node ],
    #  version => [ node ],
    #},
);

sub create_zaaktype_ok {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    if ($opts->{node} and defined $opts->{active} || defined $opts->{version}) {
        throw('/Zaaksysteem/Testsuite',
            "Conflicts! You cannot pass a node and active/version params");
    }

    my $n = delete $opts->{node} || $self->create_zaaktype_node_ok(
        active  => defined $opts->{active}  ? $opts->{active}  : 1,
        version => defined $opts->{version} ? $opts->{version} : 1,
    );
    $opts->{zaaktype_node_id} = $n->id;

    $opts->{active}  = $n->active;
    $opts->{version} = $n->version;

    my $c = delete $opts->{category} || $self->create_bibliotheek_categorie_ok();
    $opts->{bibliotheek_categorie_id} = $c->id;

    my $zt = $self->_create_zaaksysteem_ok(
        resultset => 'Zaaktype',
        arguments => $opts
    );
    $n->update({zaaktype_id => $zt->id});
    return $zt;
}

=head2 create_case_ok

=head3 SYNOPSIS

    my $case = $zs->create_case_ok();
    my $case = $zs->create_case_ok(%args);

=head3 ARGUMENTS

=over

=item aanvragers

=item registratiedatum

=item aanvraag_trigger

=item contactkanaal

=item zaaktype

L<Zaaksysteem::Schema::Zaaktype>

=item zaaktype_node

L<Zaaksysteem::Schema::ZaaktypeNode>

=item locatie_zaak

=item locatie_correspondentie

=back

=head3 RETURNS

L<Zaaksysteem::Zaak>

=head3 TODO

We need to expand this and/or create a function which creates a case with more defaults

=cut

define_profile create_case_ok => (
    optional => [
        qw/
          aanvragers
          registratiedatum
          aanvraag_trigger
          contactkanaal
          zaaktype
          zaaktype_node
          locatie_zaak
          locatie_correspondentie
          kenmerken
          confidentiality
          ontvanger
          delayed_touch
          onderwerp
          assignee_id
          ou_id
          role_id
          /
    ],
    typed => {
        zaaktype      => 'Zaaksysteem::Schema::Zaaktype',
        zaaktype_node => 'Zaaksysteem::Schema::ZaaktypeNode',
    },
    constraint_methods => {},
    defaults           => {
        registratiedatum => DateTime->now(),
        aanvraag_trigger => 'extern',
        contactkanaal    => 'email',
        delayed_touch    => 0,
        onderwerp        => sub { generate_random_string },
    },
);

sub create_case_ok {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    my $delayed_touch = delete $args->{delayed_touch};
    if ($delayed_touch) {
        $self->schema->default_resultset_attributes->{delayed_touch} = Zaaksysteem::Zaken::DelayedTouch->new;
    }
    else {
        $self->schema->default_resultset_attributes->{delayed_touch} = undef;
    }

    if (!$args->{aanvragers}) {
        my $aanvrager = $self->create_aanvrager_np_ok();
        $args->{aanvragers} = [$aanvrager];
    }

    my $zt = delete $args->{zaaktype} || $self->create_zaaktype_predefined_ok();;
    $args->{zaaktype_id} = $zt->id;

    if (!$args->{locatie_zaak}) {
        my $bag = $self->create_bag_openbareruimte_ok();
        $args->{locatie_zaak} = {
            bag_type => 'openbareruimte',
            bag_id   => $bag->id
        };
    }

    my $case = $self->schema->resultset('Zaak')->create_zaak($args);
    if ($delayed_touch) {
        return $case;
    }
    else {
        $case->update();
        return $case->discard_changes;
    }

}

=head2 run_delayed_touch

Run a delayed touch on the schema

=cut

sub run_delayed_touch {
    my $self = shift;

    my $dt = $self->schema->default_resultset_attributes->{delayed_touch};
    if (!$dt) {
        $self->log->info("Nothing to run");
        return;
    }

    $dt->execute($self->schema);
    return 1;
}

=head2 create_aanvrager_np_ok

Creates an natural person requestor, used by create_case_ok.

=head3 SYNOPSIS

    my $req = $zs->create_aanvrager_np_ok();
    my $req = $zs->create_aanvrager_np_ok(%args);

=head3 ARGUMENTS

=over

=item voornamen

=item voorletters

=item geslachtsnaam

=item huisnummer

=item postcode

=item straatnaam

=item woonplaats

=item geslachtsaanduiding

=back

=head3 RETURNS

An HashRef:

    {
        create          => { np-voornamen => "Voornamen", .. },
        betrokkene_type => 'natuurlijk_persoon',
        verificatie     => 'medewerker',
    }

=cut

define_profile create_aanvrager_np_ok => %{AANVRAGER_NP_PROFILE()};

sub create_aanvrager_np_ok {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    # For some weird reason the constant won't allow me to have contactgegevens in the defaults
    my $contact_gegevens = delete $args->{contactgegevens} || { email => 'devnull@zaaksysteem.nl'};

    my %info = map { 'np-' . $_ => $args->{$_} } keys %$args;

    foreach (keys %$contact_gegevens) {
        $info{"npc-$_"} = $contact_gegevens->{$_};
    }

    return {
        create          => \%info,
        betrokkene_type => 'natuurlijk_persoon',
        verificatie     => 'medewerker',
    };
}

=head2 create_natuurlijk_persoon_ok

Creates an natural person inside the database

=head3 SYNOPSIS

    my $np = $zs->create_natuurlijk_persoon_ok();
    my $np = $zs->create_natuurlijk_persoon_ok(%args);

=head3 ARGUMENTS

=over

=item voornamen

=item voorletters

=item geslachtsnaam

=item huisnummer

=item postcode

=item straatnaam

=item woonplaats

=item geslachtsaanduiding

=back

=head3 RETURNS


=cut

define_profile create_natuurlijk_persoon_ok => %{AANVRAGER_NP_PROFILE()};

sub create_natuurlijk_persoon_ok {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    my $npa = $self->create_aanvrager_np_ok(%$args);
    my $do  = { dbic => $self->schema, log => FakeLogger->new() };

    my $id = Zaaksysteem::Betrokkene::Object::NatuurlijkPersoon->new(dbic => $self->schema, log => FakeLogger->new())->create(
        $do, $npa->{create},
    );
    return $self->get_from_zaaksysteem(
        resultset => 'NatuurlijkPersoon',
        search    => {id => $id},
        options   => {rows => 1},
    )->single;
}

define_profile create_aanvrager_bedrijf_ok => %{AANVRAGER_BEDRIJF_PROFILE()};

sub create_aanvrager_bedrijf_ok {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    return {
        create          => $args,
        betrokkene_type => 'bedrijf',
        verificatie     => 'medewerker',
    };
}

define_profile create_bedrijf_ok => %{AANVRAGER_BEDRIJF_PROFILE()};

sub create_bedrijf_ok {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    my $do = { dbic => $self->schema, log => FakeLogger->new() };

    my $id = Zaaksysteem::Betrokkene::Object::Bedrijf->new(dbic => $self->schema, log => FakeLogger->new())->create(
        $do, $args,
    );
    return $self->get_from_zaaksysteem(
        resultset => 'Bedrijf',
        search    => {id => $id},
        options   => {rows => 1},
    )->single;
}

define_profile create_zaaktype_predefined_ok => (
    optional => [qw(resultaat naam node)],
    typed    => {
        resultaat => 'Str',
        naam      => 'Str',
        node      => 'Zaaksysteem::Schema::ZaaktypeNode',
    },
);

sub create_zaaktype_predefined_ok {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    my $zt  = $self->create_zaaktype_ok(node => delete $args->{node});
    my $ztn = $zt->zaaktype_node_id;

    $ztn->update({titel => $args->{naam}}) if $args->{naam};

    my $status = 1;
    foreach my $f (qw(Registratie Beoordeling)) {
        $self->create_zaaktype_status_ok(
            naam   => $f,
            status => $status,
            fase   => $f,
            node   => $ztn,
        );
        $status++;
    }
    my $final_status = $self->create_zaaktype_status_ok(
        status   => $status,
        fase     => 'Afhandeling',
        naam     => 'Afhandeling',
        node     => $ztn,
        role_set => 0,
        role_id  => 0,
    );

    $self->create_zaaktype_resultaat_ok(
        node      => $ztn,
        resultaat => $args->{resultaat}
    );

    $self->create_zaaktype_authorisation_ok(
        zaaktype => $zt,
    );
    $self->create_zaaktype_authorisation_ok(
        zaaktype => $zt,
        recht    => 'zaak_edit',
    );
    $self->create_zaaktype_authorisation_ok(
        zaaktype => $zt,
        recht    => 'zaak_beheer',
    );
    $self->create_zaaktype_authorisation_ok(
        zaaktype => $zt,
        recht    => 'zaak_search',
    );

    return $zt->discard_changes;
}

=head2 create_gm_natuurlijk_persoon_ok

Creates a gegevensmagazijn natuurlijk persoon

=head3 SYNOPSIS

    $zs->create_gm_natuurlijk_persoon_ok

=head3 ARGUMENTS

=over

=item gegevens_magazijn_id

=item betrokkene_type

=item burgerservicenummer

=item a_nummer

=item voorletters

=item voornamen

=item geslachtsnaam

=item voorvoegsel

=item geslachtsaanduiding

=item nationaliteitscode1

=item nationaliteitscode2

=item nationaliteitscode3

=item geboortegemeente

=item geboorteplaats

=item geboortegemeente_omschrijving

=item geboorteregio

=item geboorteland

=item aanhef_aanschrijving

=item voorletters_aanschrijving

=item voornamen_aanschrijving

=item naam_aanschrijving

=item voorvoegsel_aanschrijving

=item burgerlijke_staat

=item indicatie_gezag

=item indicatie_curatele

=item indicatie_geheim

=item aanduiding_verblijfsrecht

=item aanduiding_soort_vreemdeling

=item land_vanwaar_ingeschreven

=item land_waarnaar_vertrokken

=item adres_buitenland1

=item adres_buitenland2

=item adres_buitenland3

=item nnp_ts

=item hash

=item onderzoek_verblijfplaats_ingang

=item onderzoek_verblijfplaats_einde

=item onderzoek_overlijden_ingang

=item onderzoek_overlijden_einde

=item datum_huwelijk

=item datum_huwelijk_ontbinding

=item datum_aanvang_verblijfsrecht

=item datum_einde_verblijfsrecht

=item geboortedatum

=item import_datum

=item datum_overlijden

=item onderzoek_persoon_ingang

=item onderzoek_persoon_einde

=item onderzoek_huwelijk_ingang

=item onderzoek_huwelijk_einde

=item adres_id

=item authenticatedby

=item authenticated

=item verblijfsobject_id

=item aanduiding_naamgebruik

=item onderzoek_persoon

=item onderzoek_persoon_onjuist

=item onderzoek_huwelijk

=item onderzoek_huwelijk_onjuist

=item onderzoek_overlijden

=item onderzoek_overlijden_onjuist

=item onderzoek_verblijfplaats

=item onderzoek_verblijfplaats_onjuist

=item partner_a_nummer

=item partner_burgerservicenummer

=item partner_voorvoegsel

=item partner_geslachtsnaam

=back

=head3 RETURNS

L<Zaaksysteem::Model::DB::GmNatuurlijkPersoon>

=cut

define_profile create_gm_natuurlijk_persoon_ok => (
    optional => [qw(
          gegevens_magazijn_id
          betrokkene_type
          burgerservicenummer
          a_nummer
          voorletters
          voornamen
          geslachtsnaam
          voorvoegsel
          geslachtsaanduiding
          nationaliteitscode1
          nationaliteitscode2
          nationaliteitscode3
          geboortegemeente
          geboorteplaats
          geboortegemeente_omschrijving
          geboorteregio
          geboorteland
          aanhef_aanschrijving
          voorletters_aanschrijving
          voornamen_aanschrijving
          naam_aanschrijving
          voorvoegsel_aanschrijving
          burgerlijke_staat
          indicatie_gezag
          indicatie_curatele
          indicatie_geheim
          aanduiding_verblijfsrecht
          aanduiding_soort_vreemdeling
          land_vanwaar_ingeschreven
          land_waarnaar_vertrokken
          adres_buitenland1
          adres_buitenland2
          adres_buitenland3
          nnp_ts
          hash
          onderzoek_verblijfplaats_ingang
          onderzoek_verblijfplaats_einde
          onderzoek_overlijden_ingang
          onderzoek_overlijden_einde
          datum_huwelijk
          datum_huwelijk_ontbinding
          datum_aanvang_verblijfsrecht
          datum_einde_verblijfsrecht
          geboortedatum
          import_datum
          datum_overlijden
          onderzoek_persoon_ingang
          onderzoek_persoon_einde
          onderzoek_huwelijk_ingang
          onderzoek_huwelijk_einde
          adres_id
          authenticatedby
          authenticated
          verblijfsobject_id
          aanduiding_naamgebruik
          onderzoek_persoon
          onderzoek_persoon_onjuist
          onderzoek_huwelijk
          onderzoek_huwelijk_onjuist
          onderzoek_overlijden
          onderzoek_overlijden_onjuist
          onderzoek_verblijfplaats
          onderzoek_verblijfplaats_onjuist
          partner_a_nummer
          partner_burgerservicenummer
          partner_voorvoegsel
          partner_geslachtsnaam
    )],
    typed    => {
     gegevens_magazijn_id              =>  'Int',
     betrokkene_type                   =>  'Int',
     burgerservicenummer               =>  'Str',
     a_nummer                          =>  'Str',
     voorletters                       =>  'Str',
     voornamen                         =>  'Str',
     geslachtsnaam                     =>  'Str',
     voorvoegsel                       =>  'Str',
     geslachtsaanduiding               =>  'Str',
     nationaliteitscode1               =>  'Int',
     nationaliteitscode2               =>  'Int',
     nationaliteitscode3               =>  'Int',
     geboortegemeente                  =>  'Str',
     geboorteplaats                    =>  'Str',
     geboortegemeente_omschrijving     =>  'Str',
     geboorteregio                     =>  'Str',
     geboorteland                      =>  'Str',
     aanhef_aanschrijving              =>  'Str',
     voorletters_aanschrijving         =>  'Str',
     voornamen_aanschrijving           =>  'Str',
     naam_aanschrijving                =>  'Str',
     voorvoegsel_aanschrijving         =>  'Str',
     burgerlijke_staat                 =>  'Str',
     indicatie_gezag                   =>  'Str',
     indicatie_curatele                =>  'Str',
     indicatie_geheim                  =>  'Str',
     aanduiding_verblijfsrecht         =>  'Int',
     aanduiding_soort_vreemdeling      =>  'Str',
     land_vanwaar_ingeschreven         =>  'Int',
     land_waarnaar_vertrokken          =>  'Int',
     adres_buitenland1                 =>  'Str',
     adres_buitenland2                 =>  'Str',
     adres_buitenland3                 =>  'Str',
     nnp_ts                            =>  'Str',
     hash                              =>  'Str',
     adres_id                          =>  'Int',
     authenticatedby                   =>  'Str',
     authenticated                     =>  'Int',
     verblijfsobject_id                =>  'Str',
     aanduiding_naamgebruik            =>  'Str',
     onderzoek_persoon                 =>  'Bool',
     onderzoek_persoon_onjuist         =>  'Str',
     onderzoek_huwelijk                =>  'Bool',
     onderzoek_huwelijk_onjuist        =>  'Str',
     onderzoek_overlijden              =>  'Bool',
     onderzoek_overlijden_onjuist      =>  'Str',
     onderzoek_verblijfplaats          =>  'Bool',
     onderzoek_verblijfplaats_onjuist  =>  'Str',
     partner_a_nummer                  =>  'Str',
     partner_burgerservicenummer       =>  'Str',
     partner_voorvoegsel               =>  'Str',
     partner_geslachtsnaam             =>  'Str',
    },
    constraint_methods => {
    },
    defaults => {
    },
);

sub create_gm_natuurlijk_persoon_ok {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    my $res = $self->schema->resultset('GmNatuurlijkPersoon')->create($args);
    return $res;
}

=head2 _create_bag_ok

Creates a BAG entry in the database

=head3 SYNOPSIS

    $zs->_create_bag_ok(
        _type         => 'ligplaats',
        identificatie => 'Ergens',
        inonderzoek   => 1,
        status        => 'Zonnig',
    );

=head3 ARGUMENTS

=over

=item _type

Limited by L<PROFILE_BAG_TYPES_OK|Zaaksysteem::Constants/PROFILE_BAG_TYPES_OK>

=item identificatie

=item begindatum

=item inonderzoek

=item status

=item hoofdadres

=item huisnummer

=item openbareruimte

=item type

=item naam

=back

=head3 RETURNS

A BAG object, depending on the input

=cut

define_profile _create_bag_ok => (
    required => [qw(_type)],
    optional => [qw(identificatie begindatum inonderzoek status hoofdadres huisnummer huisletter huisnummertoevoeging postcode openbareruimte woonplaats type naam)],
    typed    => {
        _type         => 'Str',
        identificatie => 'Str',
        naam          => 'Str',
        inonderzoek   => 'Str',
        status        => 'Str',
    },
    constraint_methods => {
        _type => PROFILE_BAG_TYPES_OK,
    },
    defaults => {
        begindatum    => $strp->format_datetime(DateTime->now()),
        identificatie => sub { generate_random_string },
        inonderzoek   => 'N',
        status        => sub { generate_random_string },
    },
);

sub _create_bag_ok {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    my $rs = 'Bag' . ucfirst(lc(delete $args->{_type}));
    my $res = $self->schema->resultset($rs)->create($args);
    return $res;
}

=head2 create_bag_ligplaats_ok

Creates a ligplaats

=head3 SYNOPSIS

    $zs->create_bag_ligplaats_ok

=head3 ARGUMENTS

=over

=item identificatie

=item begindatum

=item inonderzoek

=item status

=item hoofdadres

=back

=head3 RETURNS

L<Zaaksysteem::Schema::BagLigplaats>

=cut

define_profile create_bag_ligplaats_ok => (
    optional => [qw(identificatie begindatum inonderzoek status hoofdadres)],
    typed    => {
        hoofdadres => 'Str',
    },
    defaults => {
        hoofdadres => sub { generate_random_string },
    },
);
sub create_bag_ligplaats_ok {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    return $self->_create_bag_ok(_type => 'ligplaats', %$args);
}

=head2 create_bag_nummeraanduiding_ok

Creates a nummeraanduiding

=head3 SYNOPSIS

    $zs->create_bag_nummeraanduiding_ok

=head3 ARGUMENTS

=over

=item identificatie

=item begindatum

=item inonderzoek

=item status

=item huisnummer

=item openbareruimte

=back

=head3 RETURNS

L<Zaaksysteem::Schema::BagNummeraanduiding>

=cut

define_profile create_bag_nummeraanduiding_ok => (
    optional => [qw(identificatie begindatum inonderzoek status huisnummer huisletter huisnummertoevoeging postcode openbareruimte woonplaats)],
    typed    => {
        huisnummer => 'Int',
        openbareruimte => 'Str',
    },
    defaults => {
        huisnummer     => '42',
        openbareruimte => sub { generate_random_string },
    },
);

sub create_bag_nummeraanduiding_ok {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    my $type = 'nummeraanduiding';
    return $self->_create_bag_ok(_type => $type, type => $type, %$args);
}

=head2 create_bag_openbareruimte_ok

Creates a openbare ruimte

=head3 SYNOPSIS

    $zs->create_bag_openbareruimte_ok

=head3 ARGUMENTS

=over

=item identificatie

=item begindatum

=item inonderzoek

=item status

=item naam

=back

=head3 RETURNS

L<Zaaksysteem::Schema::BagOpenbareruimte>

=cut

define_profile create_bag_openbareruimte_ok => (
    optional => [qw(identificatie begindatum inonderzoek status naam woonplaats)],
    typed    => {
        naam => 'Str',
    },
    defaults => {
        naam => sub { generate_random_string },
    },
);
sub create_bag_openbareruimte_ok {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    my $type = 'openbareruimte';
    return $self->_create_bag_ok(_type => $type, type => $type, %$args);
}

=head2 create_bag_pand_ok

Creates a pand

=head3 SYNOPSIS

    $zs->create_bag_pand_ok

=head3 ARGUMENTS

=over

=item identificatie

=item begindatum

=item inonderzoek

=item status

=back

=head3 RETURNS

L<Zaaksysteem::Schema::BagPand>

=cut

define_profile create_bag_pand_ok => (
    optional => [qw(identificatie begindatum inonderzoek status)],
);
sub create_bag_pand_ok {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    return $self->_create_bag_ok(_type => 'pand', %$args);
}

=head2 create_bag_standplaats_ok

Creates a standplaats

=head3 SYNOPSIS

    $zs->create_bag_standplaats_ok

=head3 ARGUMENTS

=over

=item identificatie

=item begindatum

=item inonderzoek

=item status

=item hoofdadres

=back

=head3 RETURNS

L<Zaaksysteem::Schema::BagStandplaats>

=cut

define_profile create_bag_standplaats_ok => (
    optional => [qw(identificatie begindatum inonderzoek status hoofdadres)],
    typed    => {
        hoofdadres => 'Str',
    },
    defaults => {
        hoofdadres => sub { generate_random_string },
    },
);

sub create_bag_standplaats_ok {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    return $self->_create_bag_ok(_type => 'standplaats', %$args);
}

=head2 create_bag_verblijfsobject_ok

Creates a verblijfsobject

=head3 SYNOPSIS

    $zs->create_bag_verblijfsobject_ok

=head3 ARGUMENTS

=over

=item identificatie

=item begindatum

=item inonderzoek

=item status

=item hoofdadres

=back

=head3 RETURNS

L<Zaaksysteem::Schema::BagVerblijfsobject>

=cut

define_profile create_bag_verblijfsobject_ok => (
    optional => [qw(identificatie begindatum inonderzoek status hoofdadres)],
    typed    => {
        hoofdadres => 'Str',
    },
    defaults => {
        hoofdadres => sub { generate_random_string },
    },
);

sub create_bag_verblijfsobject_ok {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    return $self->_create_bag_ok(_type => 'verblijfsobject', %$args);
}

=head2 create_bag_woonplaats_ok

Creates a woonplaats

=head3 SYNOPSIS

    $zs->create_bag_woonplaats_ok

=head3 ARGUMENTS

=over

=item identificatie

=item begindatum

=item inonderzoek

=item status

=item naam

=back

=head3 RETURNS

L<Zaaksysteem::Schema::BagWoonplaats>


=cut

define_profile create_bag_woonplaats_ok => (
    optional => [qw(identificatie begindatum inonderzoek status naam)],
    typed    => {
        naam => 'Str',
    },
    defaults => {
        naam => sub { generate_random_string },
    },
);

sub create_bag_woonplaats_ok {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    return $self->_create_bag_ok(_type => 'woonplaats', %$args);
}

=head2 _get_from_zaaksysteem_ok

Get some object back from Zaaksysteem

=head3 SYNOPSIS

    $zs->_get_from_zaaksysteem_ok('MyResultSet');

=head3 ARGUMENTS

=over

=item scalar

Some resultset (a scalar)

=back

=head3 RETURNS

An object you were looking for or dies.

=cut

sub _get_from_zaaksysteem_ok {
    my ($self, $s) = @_;
    if ($s) {
        my $result = $self->get_from_zaaksysteem(resultset => $s, options => { rows => 1 })->single;
        $tb->ok($result, sprintf("Found %s: %s", $s, ref $result));
        return $result;
    }
    else {
        throw(
            '/Zaaksysteem/Testsuite/_get_from_zaaksysteem',
            "You are not looking for something"
        );
    }
}

=head2 get_from_zaaksysteem

Get something from zaaksysteem

=head3 SYNOPSIS

    $zs->get_from_zaaksysteem(
        resultset => 'MyResultSet',
        search    => { case => $case->id },
        options   => { rows => 1 },
    );

=head3 ARGUMENTS

=over

=item resultset

Your resultset

=item search

Your search

=item options

Your options

=back

=head3 RETURNS

Whatever you asked for

=cut

define_profile get_from_zaaksysteem => (
    required => [qw(resultset)],
    optional => [qw(search options)],
    typed    => {
        resultset => 'Str',
    },
    defaults => {
        search  => sub {return {}},
        options => sub {return {}},
    },
);

sub get_from_zaaksysteem {
    my $self = shift;
    my $args = assert_profile({@_})->valid;
    return $self->schema->resultset($args->{resultset})->search_rs($args->{search}, $args->{options});
}

=head2 get_sjabloon_ok

Get a static BibliotheekSjabloon from the database.

=cut

sub get_sjabloon_ok {
    my $self = shift;
    return $self->_get_from_zaaksysteem_ok('BibliotheekSjablonen');
}

=head2 get_betrokkene_ok

Get a static Betrokkene from the database.

=cut

sub get_betrokkene_ok {
    my $self = shift;
    return $self->_get_from_zaaksysteem_ok('ZaakBetrokkenen');
}

=head2 get_bibliotheek_kenmerk_ok

Get a static library 'kenmerk' from the database.

=cut

sub get_bibliotheek_kenmerk_ok {
    my $self = shift;
    return $self->create_bibliotheek_kenmerk_ok();
}

=head2 get_case_type_ok

Gets a static database case type. If no case type is found, create one.

=cut

sub get_case_type_ok {
    my $self = shift;
    $self->_get_from_zaaksysteem_ok('Zaaktype');
}

=head2 get_subject_ok

Arguments: \%options

Generates a random subject string.

B<options>

=over 4

=item subject_id [optional]

ID of subject

=item username [optional]

username of subject

=back

=cut

sub get_subject_ok {
    my $self        = shift;
    my $search_opts = shift || {};

    if ($search_opts->{subject_id}) {
        $search_opts->{id} = delete($search_opts->{subject_id});
    }

    if (!scalar keys %$search_opts) {
        $search_opts->{username} = 'admin';
    }

    my $subject             = $self->schema->resultset('Subject')->find($search_opts);

    $subject = "betrokkene-medewerker-" . $subject->id;
    $tb->ok($subject, "Generated subject: $subject");
    return $subject;
}

=head2 empty_filestore_ok

Clean up the file store, deleting all files within it.

=cut

sub empty_filestore_ok {
    my $self = shift;
    foreach (qw(tmp storage)) {
        $self->empty_dir_ok(catdir($self->config->{'filestore_location'}, $_));
    }
}


=head2 empty_dir_ok

=cut

sub empty_dir_ok {
    my ($self, $dir) = @_;
    my $err;
    require File::Path;
    File::Path::remove_tree($dir, {keep_root => 1, error => \$err});
    my $ok = @$err ? 0 : 1;
    if (@$err) {
        diag explain $err;
    }
    $tb->ok($ok, "Emptied $dir");
    my $gitignore = catfile($dir, '.gitignore');
    my $fh;
    if (open my $fh, '>', $gitignore) {
        print $fh "*\n";
        close($fh);
    } else {
        diag("Error in setting $dir/.gitignore: $!");
    }

    return $ok;
}



=head2 create_file_ok

Create a file entry.

=head3 SYNOPSIS

    $zs->create_file_ok();

=head3 ARGUMENTS

=head3 RETURNS

=cut

define_profile create_file_ok => (
    optional => {
        name              => 'Str',
        file_path         => 'Str',
        db_params         => 'HashRef',
        metadata          => 'HashRef',
        ignore_extension  => 'Bool',
        case_document_ids => 'Any',
    },
    defaults => {
        db_params => sub {return {}},
    },
);

sub create_file_ok {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    $opts->{name}       //= $self->config->{filestore_test_file_name};
    $opts->{file_path}  //= $self->config->{filestore_test_file_path};

    $opts->{db_params}{created_by} = delete $opts->{betrokkene} || $self->get_subject_ok;

    if (exists $opts->{db_params}{case} && $opts->{db_params}{case}) {
        my $case = delete $opts->{db_params}{case};
        $opts->{db_params}{case_id} = $case->id;
    }

    my $result = $self->schema->resultset('File')->file_create($opts);
    $tb->ok($result, "Created file: $result");
    return $result;
}

=head2 create_filestore_ok

Create a filestore entry.

=cut

define_profile create_filestore_ok => (
    optional    => {
        file_path     => 'Str',
        original_name => 'Str',
    },
);

sub create_filestore_ok {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    $args->{file_path} //= $self->config->{filestore_test_file_path};
    $args->{original_name} //= $self->config->{filestore_test_file_name};

    my $result = $self->schema->resultset('Filestore')->filestore_create($args);
    $tb->ok($result, "Created filestore: $result");
    return $result;
}

=head2 create_directory_ok

Create a directory entry.

=cut

define_profile create_directory_ok => (
    optional  => {
        case => 'Zaaksysteem::Model::DB::Zaak',
        name => 'Str',
    },
    defaults => {
        name => sub { return generate_random_string }
    },
);

sub create_directory_ok {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    my $case = delete $args->{case} || $self->create_case_ok();

    return $self->_create_zaaksysteem_ok(
        resultset => 'Directory',
        arguments => {
            case_id       => $case->id,
            original_name => $args->{name},
            name          => $args->{name},
        }
    );
}

=head2 get_zaaktype()

Get a zaaktype.

=cut

sub get_zaaktype_ok {
    my $self = shift;
    return $self->_get_from_zaaksysteem_ok('Zaaktype');
}

=head2 get_zaaktype_status()

Get a zaaktype status.

=cut

sub get_zaaktype_status_ok {
    my $self = shift;
    return $self->_get_from_zaaksysteem_ok('ZaaktypeStatus');
}

=head2 get_zaaktype_kenmerk_ok($schema)

Get a zaaktype kenmerk.

=cut

sub get_zaaktype_kenmerk_ok {
    my $self = shift;
    return $self->_get_from_zaaksysteem_ok('ZaaktypeKenmerken');
}

=head2 get_ustore_ok

Get a Ustore object for testing.

=cut

sub get_ustore_ok {
    my $self  = shift;
    my $store = File::UStore->new(
        path   => $self->schema->storage_path,
        prefix => 'zs_',
        depth  => 5,
    );
    $tb->ok($store, "Created store: $store");
    return $store;
}

=head2 create_case_document_ok

Create a case document

=cut

define_profile create_case_document_ok => (
    optional  => {
        case => 'Zaaksysteem::Model::DB::Zaak',
    },
);

sub create_case_document_ok {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    my $case = delete $args->{case} || $self->create_case_ok();

    my ($kenmerk) = grep { $_->value_type eq 'file' } $case->zaak_kenmerken;
    return $kenmerk if $kenmerk;
    return $self->create_zaaktype_kenmerk_ok(
        value_type => 'file',
        node       => $case->zaaktype_node_id
    );

}

=head2 create_contactmoment_note_ok($schema)

Create a contactmoment entry of type note.

=cut

sub create_contactmoment_note_ok {
    my $self = shift;

    my @valid_chars = ('A' .. 'z', 'a' .. 'z', 0 .. 9, ' ');
    my $message;
    $message .= $valid_chars[rand @valid_chars] for 20 .. 60;

    my $result =
      $self->schema->resultset('Contactmoment')->contactmoment_create({
            type       => 'note',
            subject_id => $self->get_subject_ok,
            created_by => $self->get_subject_ok,
            medium     => 'balie',
            case_id    => $self->get_case_ok,
            message    => $message,
      });
    $tb->ok($result, "Created note contactmoment: $result");
    return $result;
}

=head2 create_interface_ok

Create a new interface

=head3 ARGUMENTS

=over

=item name

Name of the interface, defaults to something random

=item zaaktype

A Zaaksysteem::Schema::Zaaktype object, created for you if you don't supply one.

=item max_retries

Integer, defaults to 10

=item multiple

Integer, defaults to 1

=item module

String, defaults to 'bagcsv'

=item interface_config

HashRef, defaults to { url => undef },

=item active

Boolean, defaults to true

=back

=head3 RETURNS

An interface object: "Zaaksysteem::Meuk::Interface"

=cut

define_profile create_interface_ok => (
    optional => [qw(
          name
          zaaktype
          max_retries
          multiple
          module
          interface_config
          active
          )
    ],
    typed => {
        name             => 'Str',
        zaaktype         => 'Zaaksysteem::Schema::Zaaktype',
        max_retries      => 'Int',
        multiple         => 'Bool',
        module           => 'Str',
        interface_config => 'HashRef',
        active           => 'Bool',
    },
    defaults => {
        name             => sub { generate_random_string },
        max_retries      => 10,
        module           => 'bagcsv',
        interface_config => {url => undef},
        active           => 1,
    },
);

sub create_interface_ok {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    if (exists $opts->{zaaktype}) {
        my $c = delete $opts->{zaaktype} || $self->create_zaaktype_ok;
        $opts->{case_type_id} = $c->id;
    }

    return $self->schema->resultset('Interface')->interface_create($opts);
}

=head2 create_transaction_ok

Create a new transaction.

=head3 ARGUMENTS

=head3 RETURNS

=cut

sub create_transaction_ok {
    my $self = shift;
    my $opts = $_[0];

    my $default = {
        input_data              => 'test',
        interface_id            => $self->create_interface_ok->id,
        external_transaction_id => 'bla-1234',
    };
    my $create_opts = _merge_hash($opts, $default);

    my $result =
      $self->schema->resultset('Transaction')->transaction_create($create_opts);
    $tb->ok($result, "Created transaction: $result");

    return $result;
}

=head2 $self->create_transaction_ok

Create a new transaction.

=cut

sub create_transaction_record_ok {
    my $self = shift;
    my $opts = $_[0];

    my $default = {
        transaction_id => $opts->{transaction_id}
          || $self->create_transaction_ok->id,
        input  => 'test',
        output => 'bla-1234',
    };
    my $create_opts = _merge_hash($opts, $default);

    my $result =
      $self->schema->resultset('TransactionRecord')->create($create_opts);
    $tb->ok($result, "Created transaction record: $result");

    return $result;
}

=head2 _merge_hash

Simple helper to merge two hashes. Useful for default parameter overrides.

The first given hash is leading when duplicate keys are found. (This should generally be the user-defined parameters)

Expects hashrefs for input.

=cut

sub _merge_hash {
    my ($hash_1, $hash_2) = @_;
    foreach my $k (keys %$hash_2) {
        if (exists $hash_1->{$k}) {
            next;
        }
        else {
            $hash_1->{$k} = $hash_2->{$k};
        }
    }
    return $hash_1;
}

=head2 create_contact_data_ok(\%contact_data, \%np_data)

Create a ContactData record, linked to the relevant NatuurlijkPersoon and
with the specified in the C<\%data> hash reference.

If no data is specified, something random will be used.

=head3 SYNOPSIS

    $zs->create_contact_data_ok(\%contact_data, \%np_data);

=head3 TODO

Refactor this bit of code

=cut

sub create_contact_data_ok {
    my $self = shift;
    my ($contact_data, $np_data) = @_;

    my $np =
      $self->schema->resultset('NatuurlijkPersoon')->create($np_data // {});

    return $self->schema->resultset('ContactData')->create({
            gegevens_magazijn_id => $np->id,
            betrokkene_type      => 1,
            mobiel               => $contact_data->{mobiel},
            telefoonnummer       => $contact_data->{telefoonnummer},
            email                => $contact_data->{email},
    });
}

=head2 create_named_interface_ok

The same as L</create_interface_ok>, but you must use a hashref as an argument.

=head3 SYNOPSIS

    my $interface = create_named_interface_ok({name => MyInterface});

=cut

sub create_named_interface_ok {
    my $self = shift;
    my $options = shift || {};
    return $self->create_interface_ok(%$options);
}

=head2 get_file_contents_as_string

=cut

sub get_file_contents_as_string {
    my $self     = shift;
    my $filename = shift;

    # Get file contents
    open(my $fh, "<:encoding(UTF-8)", $filename)
      or die('Cannot find test file: ' . $filename);

    my $contents = '';
    while (<$fh>) {
        $contents .= $_;
    }

    close($fh);

    $tb->ok($contents, 'Got file contents from file: ' . $filename);

    return $contents;
}

=head2 set_current_user

Set the current user and returns it to you. With no arguments it creates a employee for you.

=head3 SYNOPSIS

    $zs->set_current_user();
    # or
    $zs->set_current_user(
        username => 'marty',
    );
    # or
    $zs->set_current_user(
        subject => Zaaksysteem::Schema::Subject,
    );
    # or
    $zs->set_current_user(
        userentity => Zaaksysteem::Schema::UserEntity,
    );

=head3 ARGUMENTS

=over

=item username

A username for your current user

=item subject

A L<Zaaksysteem::Schema::Subject> object.

=item userentity

A L<Zaaksysteem::Schema::UserEntity> object.

=back

=head3 RETURNS

A L<Zaaksysteem::Schema::Subject> object.

=cut

define_profile set_current_user => (
    optional    => {
        username   => 'Str',
        subject    => 'Zaaksysteem::Schema::Subject',
        userentity => 'Zaaksysteem::Schema::UserEntity',
    },
);

sub set_current_user {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    my $subject = $args->{subject};
    if (!$subject) {
        my $ue = $args->{userentity} || $self->create_medewerker_ok(%$args);
        $subject = $ue->subject_id;
    }
    $self->schema->default_resultset_attributes->{current_user} = $subject;
    $self->schema->current_user($subject);
    return $self->schema->default_resultset_attributes->{current_user};
}

=head2 create_zaaktype_status_checklist_item_ok

Creates a checklist item for a zaaktype

=head3 SYNOPSIS

    $zs->create_zaaktype_status_checklist_item_ok(
        status             => $zs->create_zaaktype_status_ok(),
        label              => "Some label",
        external_reference => "Some reference",
    );

=head3 ARGUMENTS

=over

=item status

L<Zaaksysteem::Schema::ZaaktypeStatus>

=item label

String

=item external_reference

String

=back

=head3 RETURNS

A L<Zaaksysteem::Model::DB::ZaaktypeStatusChecklistItem> object

=cut

define_profile create_zaaktype_status_checklist_item_ok => (
    optional => [qw(
          status
          label
          external_reference
    )],
    typed => {
        status             => 'Zaaksysteem::Schema::ZaaktypeStatus',
        label              => 'Str',
        external_reference => 'Str',
    },
    constraint_methods => {},
    defaults           => {
        label              => sub { generate_random_string },
        external_reference => sub { generate_random_string },
    },
);

sub create_zaaktype_status_checklist_item_ok {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    my $status = delete $args->{status} || $self->create_zaaktype_status_ok();
    $args->{casetype_status_id} = $status->id;

    return $self->_create_zaaksysteem_ok(
        resultset => 'ZaaktypeStatusChecklistItem',
        arguments => $args
    );
}

=head2 create_case_checklist_ok

Creates a checklist for a case

=head3 SYNOPSIS

    $zs->create_case_checklist_ok(
        zaak      => $zs->create_case_ok(),
        milestone => 42,
    );

=head3 ARGUMENTS

=over

=item zaak

L<Zaaksysteem::Schema::Zaak>

=item milestone

Integer

=back

=head3 RETURNS

A L<Zaaksysteem::Model::DB::Checklist> object

=cut

define_profile create_case_checklist_ok => (
    optional => [qw(zaak milestone)],
    typed    => {
        zaak      => 'Zaaksysteem::Schema::Zaak',
        milestone => 'Int',
    },
    defaults => {
        milestone => 1,
    },
);

sub create_case_checklist_ok {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    my $zaak = delete $args->{zaak} || $self->create_case_ok();

    return $self->_create_zaaksysteem_ok(
        resultset => 'Checklist',
        arguments => {
            case_id        => $zaak->id,
            case_milestone => $args->{milestone},
        }
    );
}

=head2 create_case_checklist_item_ok

Creates a checklist for a case

=head3 SYNOPSIS

    $zs->create_case_checklist_item_ok(
        checklist         => $zs->create_case_checklist_ok(),
        label             => "Some label",
        state             => 1,
        user_defined      => 1,
        deprecated_answer => "Whoops",
    );

=head3 ARGUMENTS

=over

=item checklist

L<Zaaksysteem::Model::DB::Checklist>

=item label

String

=item state

Boolean

=item user_defined

Boolean

=item deprecated_answer

String

=back

=head3 RETURNS

A L<Zaaksysteem::Model::DB::Checklist> object

=cut

define_profile create_case_checklist_item_ok => (
    optional => [qw(
        checklist
        label
        state
        user_defined
        deprecated_answer
    )],
    typed => {
        checklist         => 'Zaaksysteem::Model::DB::Checklist',
        label             => 'Str',
        state             => 'Bool',
        user_defined      => 'Bool',
        deprecated_answer => 'Str'
    },
    defaults => {
        label             => sub { generate_random_string },
        state             => 0,
        user_defined      => 1,
        deprecated_answer => 'Blub'
    },

);

sub create_case_checklist_item_ok {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    my $cl = delete $args->{checklist} || $self->create_case_checklist_ok();
    $args->{checklist_id} = $cl->id;

    return $self->_create_zaaksysteem_ok(
        resultset => 'ChecklistItem',
        arguments => $args,
    );
}

=head2 create_zaaktype_resultaat_ok

Create a zaaktype resultaat

=head3 SYNOPSIS

    $zs->create_zaaktype_resultaat_ok();

=head3 ARGUMENTS

=over

=item status

L<Zaaksysteem::Schema::ZaaktypeStatus>

=item node

L<Zaaksysteem::Schema::ZaaktypeNode>

=item resultaat

=item ingang

=item bewaartermijn

=item created

L<DateTime>

=item last_modified

L<DateTime>

=item dossiertype

=item label

=item selectielijst

=item archiefnominatie

=item comments

=back

=head3 RETURNS

A L<Zaaksysteem::Model::DB::ZaaktypeResultaten> object

=cut

define_profile create_zaaktype_resultaat_ok => (
    optional => [qw(
          status
          node
          resultaat
          ingang
          bewaartermijn
          created
          last_modified
          dossiertype
          label
          selectielijst
          archiefnominatie
          comments
          )
    ],
    typed => {
        bewaartermijn => 'Int',
        label         => 'Str',
        comments      => 'Str',
        selectielijst => 'Str',
        dossiertype   => 'Str',
        ingang        => 'Str',
        node          => 'Zaaksysteem::Schema::ZaaktypeNode',
        status        => 'Zaaksysteem::Schema::ZaaktypeStatus',
        created       => 'DateTime',
        last_modified => 'DateTime',
    },
    constraint_methods => {},
    defaults           => {
        archiefnominatie => sub { return 'Bewaren (B)' },
        comments         => sub { return generate_random_string },
        created          => DateTime->now(),
        dossiertype      => sub { return 'digitaal' },
        ingang           => sub { return 'verleend' },
        label            => sub { return generate_random_string },
        last_modified    => DateTime->now(),
        resultaat        => DEFAULT_ZAAKTYPE_RESULTAAT,
        selectielijst    => sub { return generate_random_string },
        bewaartermijn    => 365,
    },
);

define_profile _create_from_node_or_status => (
    optional => [qw(node status)],
    typed => {
        node          => 'Zaaksysteem::Schema::ZaaktypeNode',
        status        => 'Zaaksysteem::Schema::ZaaktypeStatus',
    },
);

sub _create_from_node_or_status {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    if ($args->{node} && $args->{status}) {
        throw('ZS/Testsuite', "Conflicting arguments: node and status");
    }

    my $status = $args->{status} || $self->create_zaaktype_status_ok(
          node => $args->{node} || $self->create_zaaktype_node_ok()
    );

    return $status;
}

sub create_zaaktype_resultaat_ok {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    my $status = $self->_create_from_node_or_status(
        node   => delete $args->{node},
        status => delete $args->{status}
    );
    $args->{zaaktype_status_id} = $status->id;
    $args->{zaaktype_node_id} = $status->zaaktype_node_id->id;

    return $self->_create_zaaksysteem_ok(
        resultset => 'ZaaktypeResultaten',
        arguments => $args
    );
}

sub create_bag_records {
    my $self      = shift;

    $self->create_bag_woonplaats_ok(
        identificatie   => '9001',
        begindatum      => '201301010000',
        naam            => 'Amsterdam',
        officieel       => 'N',
        inonderzoek     => 'N',
        documentdatum   => '',
        documentnummer  => '',
        correctie       => 'N',
        status          => 'Naamgeving uitgegeven',
    );

    $self->create_bag_openbareruimte_ok(
        identificatie   => '1234567890123456',
        begindatum      => '201301010000',
        naam            => 'Donker Curtiusstraat',
        officieel       => 'N',
        woonplaats      => '9001',
        inonderzoek     => 'N',
        type            => 'Weg',
        documentdatum   => '',
        documentnummer  => '',
        status          => 'Naamgeving uitgegeven',
        correctie       => 'N'
    );

    $self->create_bag_nummeraanduiding_ok(
        identificatie   => '9876543218375842',
        begindatum      => '201301010000',
        huisnummer      => 23,
        officieel       => 'N',
        huisletter      => 'A',
        huisnummertoevoeging => '1rec',
        postcode        => '1051JL',
        woonplaats      => '9001',
        inonderzoek     => 'N',
        openbareruimte  => '1234567890123456',
        type            => 'verblijfsobject',
        documentdatum   => '',
        documentnummer  => '',
        status          => 'Naamgeving uitgegeven',
        correctie       => 'N'
    );
}


=head2 create_zaaktype_relatie_ok

Creates a L<Zaaksysteem::Model::DB::ZaaktypeRelatie> object for  you with prefilled values.
All parameters are optional.

=head3 SYNOPSIS

    $zs->create_zaaktype_relatie_ok();

=head3 ARGUMENTS

=over

=item zt_status

L<Zaaksysteem::Schema::ZaaktypeStatus>

=item zt_node

L<Zaaksysteem::Schema::ZaaktypeNode>

=item zaaktype

L<Zaaksysteem::Schema::Zaaktype>

=item relatie_type

String

=item eigenaar_type

String

=item created

DateTime

=item last_modified

DateTime

=item start_delay

Str

=item status

Integer

=item kopieren_kenmerken

Integer / Boolean

=item ou_id

Integer

=item role_id

Integer

=item automatisch_behandelen

Boolean

=item required

String

=item parent_advance_result

String

=back

=head3 RETURNS

A L<Zaaksysteem::Model::DB::ZaaktypeRelatie> object

=cut

define_profile create_zaaktype_relatie_ok => (
    optional => [qw(
        zt_status
        zt_node
        zaaktype
        relatie_type
        eigenaar_type
        created
        last_modified
        start_delay
        status
        kopieren_kenmerken
        ou_id
        role_id
        automatisch_behandelen
        required
        parent_advance_results
    )],
    typed => {
        zt_node                => 'Zaaksysteem::Schema::ZaaktypeNode',
        zt_status              => 'Zaaksysteem::Schema::ZaaktypeStatus',
        zaaktype               => 'Zaaksysteem::Schema::Zaaktype',
        relatie_type           => 'Str',
        eigenaar_type          => 'Str',
        created                => 'DateTime',
        last_modified          => 'DateTime',
        start_delay            => 'Str',
        status                 => 'Int',
        kopieren_kenmerken     => 'Int',
        ou_id                  => 'Int',
        role_id                => 'Int',
        automatisch_behandelen => 'Bool',
        required               => 'Str',
        parent_advance_results => 'Str',
    },
    constraint_methods => {
    },
    defaults => {
        relatie_type           => sub { generate_random_string },
        eigenaar_type          => sub { generate_random_string },
        created                => DateTime->now(),
        last_modified          => DateTime->now(),
        start_delay            => sub { generate_random_string },
        kopieren_kenmerken     => 1,
        ou_id                  => 1,
        role_id                => 1,
        automatisch_behandelen => 1,
        required               => sub { generate_random_string },
        parent_advance_results => sub { generate_random_string },
    },
);

sub create_zaaktype_relatie_ok {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    if (($args->{zt_status} || $args->{zt_node}) && $args->{zaaktype}) {
        throw('ZS/Testsuite', "Conflicting arguments: zt_node, zt_status and zaaktype");
    }
    if ($args->{zt_node} && $args->{zt_status}) {
        throw('ZS/Testsuite', "Conflicting arguments: zt_node and zt_status");
    }

    my ($node, $status, $zt);
    if ($args->{zaaktype}) {
        $zt = delete $args->{zaaktype};
        $node = $zt->zaaktype_node_id;
    }

    $node ||= delete $args->{zt_node} || $self->create_zaaktype_node_ok();
    $status = delete $args->{zt_status} || $self->create_zaaktype_status_ok(node => $node);
    $node = $status->zaaktype_node_id;
    $zt ||= $self->create_zaaktype_ok(node => $node);

    $args->{zaaktype_status_id}  = $status->id;
    $args->{zaaktype_node_id}    = $node->id;
    $args->{relatie_zaaktype_id} = $zt->id;

    return $self->_create_zaaksysteem_ok(
        resultset => 'ZaaktypeRelatie',
        arguments => $args
    );
}

=head2 create_object_data_ok

=head3 SYNOPSIS

Creates a dummy object_data row in the database. Supports most permutations
of objects out of the box. For specific case objects, use C<create_case_ok>
and have it sync to the object_data table.

=head3 USAGE

    $zs->create_object_data_ok;

=head3 ARGUMENTS

=over 4

=item object_class

The string slug identification of the object's type.

Default: C<object>

=item object_id

Integer representing the originating object's identifier

Default: C<0..31337>

=item class_uuid

UUID of the object that should act as the to-be-created instance's parent
class, in an object-oriented sort of way.

Default: C<undef>

=item attributes

HashRef of L<attributes|Zaaksysteem::Object::Attribute> to be
included in this object.

Default: C<number>, C<name>, C<null>, and C<timestamp> attributes
with their values set to respective randomized ranges of sensible data.

=back

=cut

define_profile create_object_data_ok => (
    optional => {
        object_class => 'Str',
        object_id => 'Int',
        class_uuid => 'Str', # TODO Needs UUID type!
        attributes => 'HashRef', # Subtyped hashrefs fail to validate.. wha?
    },
    defaults => {
        object_class => 'object',
        object_id => sub { return int(rand(31337)) } ,
        attributes => sub {
            {
                number => Zaaksysteem::Object::Attribute->new(
                    name => 'number',
                    value => int(rand(31337)),
                    attribute_type => 'integer'
                ),
                name => Zaaksysteem::Object::Attribute->new(
                    name => 'name',
                    value => generate_random_string(),
                    attribute_type => 'text'
                ),
                null => Zaaksysteem::Object::Attribute->new(
                    name => 'null',
                    value => undef,
                    attribute_type => 'text' # TODO Yeah, what?
                ),
                timestamp => Zaaksysteem::Object::Attribute->new(
                    name => 'timestamp',
                    attribute_type => 'timestamp',
                    value => DateTime->now->add(
                        # Randomly select any date between epoch and now + epoch
                        seconds => int(rand(time() * 2) - time())
                    )
                )
            }
        }
    }
);

sub create_object_data_ok {
    my ($self, %args) = @_;

    # Only we need this module
    use Pg::hstore;

    my $opts = assert_profile(\%args)->valid;

    my $attrs = delete $opts->{ attributes };

    for my $attr (values %{ $attrs }) {
        $opts->{ properties }{ values }{ $attr->name } = $attr->TO_JSON;
    }

    $opts->{ index_hstore } = Pg::hstore::encode({
        map { $_->name => $_->human_value } values %{ $attrs }
    });

    return $self->_create_zaaksysteem_ok(
        resultset => 'ObjectData',
        arguments => $opts
    );
}

=head2 create_object_type_ok

This helper quickly creates a L<Zaaksysteem::Object::Types::Type>.

    $zs->create_object_type_ok();

Parameters are expected to be a bare hash, of which only one key is consumed
by this method, C<model>. If not provided, a model will be created ad-hoc.

All other parameters are passed as-is to the constructor of
L<Zaaksysteem::Object::Types::Type>.

=cut

define_profile create_object_type_ok => (
    optional => {
        category => 'Zaaksysteem::Schema::BibliotheekCategorie',
        name     => 'Str',
        prefix   => 'Str'
    },
    defaults => {
        name => sub { generate_random_string },
    }
);

sub create_object_type_ok {
    my ($self, %params) = @_;

    my $opts = assert_profile(\%params)->valid;

    use Zaaksysteem::Object::Types::Type;

    my $model = delete $opts->{ model } || $self->create_object_model_ok;
    my $category = delete $opts->{ category } || $self->create_bibliotheek_categorie_ok;

    $opts->{ category_id } = $category->id;

    my $object_type = Zaaksysteem::Object::Types::Type->new(%{ $opts });

    $object_type->prefix;

    return $model->save(object => $object_type);
}

=head2 create_object_model_ok

This helper builds a L<Zaaksysteem::Object::Model> instance.

    $zs->create_object_model_ok;

It optionally takes the following arguments:

=over 4

=item schema

A reference to a L<Zaaksysteem::Schema> object. Defaults to the current schema
in use by the TestUtils instance.

=item table

The string name of the DBIx::Class table name. Defaults to I<ObjectData>.

=back

=cut

define_profile create_object_model_ok => (
    optional => {
        schema => 'Zaaksysteem::Schema',
        table => 'Str'
    },
    defaults => {
        table => 'ObjectData'
    }
);

sub create_object_model_ok {
    my ($self, %params) = @_;

    my $opts = assert_profile(\%params)->valid;

    $opts->{ schema } //= $self->schema;

    return Zaaksysteem::Object::Model->new(%{ $opts });
}

define_profile create_instance_ok => (
    optional => {
        owner                 => 'Str',
        fqdn                  => 'Str',
        label                 => 'Str',
        disabled              => 'Bool',
        deleted               => 'Bool',
        protected             => 'Bool',
        template              => 'Str',
        provisioned_on        => 'DateTime',
        delete_on             => 'DateTime',
        filestore_provisioned => 'DateTime',
        database_provisioned  => 'DateTime',
        network_acl           => 'Any', # Because ArrayRef will not work with dfv
    },
    defaults => {
        fqdn                  => sub { return generate_random_string . ".zaaksysteem.nl" },
        label                 => sub { return generate_random_string . ".zaaksysteem.nl label" },
        owner                 => sub { return 'betrokkene-bedrijf-' . generate_random_string('nnn') },
        disabled              => 0,
        template              => 'some_template',
        customer_type         => 'government',
    }
);

sub create_instance_ok {
    my ($self, %args) = @_;
    my $args = assert_profile(\%args)->valid;
    return $self->object_model->save_object(
        object => $self->create_object('Instance', %$args)
    );
}

=head2 create_object_ok

Create and test object creation

=cut

sub create_object_ok {
    my ($self, $type, $args, $values) = @_;

    $values //= {};

    my $obj = $self->create_object($type, %$args);

    is $obj->id, undef, "Unsaved object does not have an id";

    my $saved = $self->object_model->save_object(object => $obj);

    is_deeply(
        $obj->TO_JSON->{values},
        {
            %$args,
            %$values,
        },
        'Object values matches input'
    );
    is_deeply($saved->TO_JSON->{values}, $obj->TO_JSON->{values}, "Saved object matches unsaved object");

    isnt $saved->id, undef, "Saved object has an id";
    return ($obj, $saved);
}

=head2 create_object

Create an object and test if the ISA is ok.

=cut

sub create_object {
    my ($self, $type, %args) = @_;
    my $module = "Zaaksysteem::Object::Types::$type";
    if (can_load(modules =>  {$module => undef})) {
        my $obj = $module->new(%args);
        isa_ok($obj, $module);
        return $obj;
    }

    fail("Loaded module: $module");
    return;
}

define_profile create_customer_config_ok => (
    optional => {
        owner         => 'Str',
        customer_id   => 'Str',
        domain        => 'Str',
        template      => 'Str',
        customer_type => 'Str',
        shortname     => 'Str',
    },
    defaults => {
        owner => sub { return 'betrokkene-bedrijf-' . generate_random_string('nnn') },
        customer_type => 'overheid',
        customer_id   => sub { generate_random_string },
        domain        => sub { generate_random_string . ".nl" },
        template      => sub { generate_random_string },
        shortname     => sub { generate_random_string },
    }
);

sub create_customer_config_ok {
    my ($self, %args) = @_;
    my $args = assert_profile(\%args)->valid;
    return $self->object_model->save_object(
        object => $self->create_object('Controlpanel', %$args)
    );
}


define_profile create_naw_ok => (
    optional => {
        owner                 => 'Str',
        naam                  => 'Str',
        straatnaam            => 'Str',
        huisnummer            => 'Int',
        huisnummer_letter     => 'Str',
        huisnummer_toevoeging => 'Str',
        postcode              => 'Str',
        woonplaats            => 'Str',
        email                 => 'Str',
        telefoonnummer        => 'Str',
    },
    defaults => {
        owner => sub { return 'betrokkene-bedrijf-' . generate_random_string('nnn') },
        naam       => sub { return generate_random_string() },
        straatnaam => sub { return generate_random_string() },
        huisnummer => sub { return generate_random_string('nnn') },
        huisnummer_letter     => sub { return generate_random_string('C') },
        huisnummer_toevoeging => sub { return generate_random_string('nnn') },
        postcode   => sub { return generate_random_string('nnnncc') },
        woonplaats => sub { return generate_random_string() },
        email       => sub { return generate_random_string() . '@' . generate_random_string() . '.nl' },
        telefoonnummer => sub { return generate_random_string('nnnnnnnnnn') },
    }
);

sub create_naw_ok {
    my ($self, %args) = @_;
    my $args = assert_profile(\%args)->valid;
    return $self->object_model->save_object(
        object => $self->create_object('Naw', %$args)
    );
}

define_profile create_scheduled_job_ok => (
    optional => {
        data            => 'HashRef',
        interval_period => 'Str',
        interval_value  => 'Str',
        job             => 'Str',
        next_run        => 'DateTime',
        runs_left       => 'Int',
    },
    defaults => {
        next_run => sub { return DateTime->now() },
        job      => 'SomeJob',
    },
);

sub create_scheduled_job_ok {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    my $scheduled_job = $self->object_model->save_object(
        object => $self->create_object('ScheduledJob', %$args));

}
=head2 create_omgevingsloket_interface_ok

Simpel omgevingsloketje

=cut

sub create_interface_omgevingsloket_ok {
    my $self = shift;

    my $interface = $self->schema->resultset('Interface')->search_active({module => 'omgevingsloket'})->first;
    if ($interface) {
        diag "Omgevingsloket interface bestaat al, wrap het binnen een transactie svp!";
        return $interface;
    }

    my $zaaktype_definitie = $self->create_zaaktype_definitie_ok(
        extra_informatie => 'extra info [[omg_locatie]]'
    );
    my $zaaktype_node = $self->create_zaaktype_node_ok(
        zaaktype_definitie => $zaaktype_definitie
    );

    my $casetype = $self->create_zaaktype_predefined_ok(node => $zaaktype_node);

    require Zaaksysteem::Backend::Sysin::Modules::Omgevingsloket;
    my @attributes = Zaaksysteem::Backend::Sysin::Modules::Omgevingsloket::ATTRIBUTES;
    my $zaaktype_status = $casetype->zaaktype_node_id->zaaktype_statuses->search->first;
    my @mapping;

    my $value_types = {
        aanvraagdatum => 'date'
    };

    for my $attribute (@attributes) {
        next if $attribute eq 'bijlagen';

        my $magic_string = $attribute . "_magic_string";
        my $bieb1 = $self->create_bibliotheek_kenmerk_ok(
            magic_string => $magic_string,
            value_type => $value_types->{$attribute} || 'text'
        );

        $self->create_zaaktype_kenmerk_ok(
            status => $zaaktype_status,
            bibliotheek_kenmerk => $bieb1,
        );

        push @mapping, {
            external_name => $attribute,
            internal_name => {
                searchable_object_id => $magic_string
            }
        };
    }

    return $self->create_interface_ok(
        name => 'Omgevingsloket',
        module => 'omgevingsloket',
        zaaktype => $casetype,
        interface_config => {
            host         => 'not a real host',
            username     => 'testusername',
            password     => 'testpassword',
            attribute_mapping => [@mapping]
        }
    );
}

sub set_fallback_requestors {
    my ($self, $interface) = @_;
    my $config = $interface->get_interface_config;
    my $natuurlijk_persoon = $self->create_natuurlijk_persoon_ok;
    my $bedrijf = $self->create_bedrijf_ok;
    $config->{fallback_bedrijf}            = { $bedrijf->get_columns };
    $config->{fallback_natuurlijk_persoon} = { $natuurlijk_persoon->get_columns };
    $interface->update_interface_config($config);
}

=head2 create_stuf_interface_ok



=cut

sub create_stuf_interfaces_ok {
    my $self        = shift;
    my $opts        = shift || {};
    $opts->{config} ||= {};

    $self->create_named_interface_ok(
        {
            module              => 'stufconfig',
            name                => 'STUF PRS Parsing',
            interface_config    => {
                stuf_supplier           => 'pink',
                synchronization_type    => 'question',
                gemeentecode            => '363',
                mk_sender               => 'ZSNL',
                mk_ontvanger            => 'CGS',
                gbav_applicatie         => 'CML',
                mk_ontvanger_afnemer    => 'CMODIS',
                mk_async_url            => 'https://localhost/stuf/async',
                mk_sync_url             => 'https://localhost/stuf/sync',
                gbav_pink_url           => 'https://localhost/stuf/pink',
                gbav_search             => 1,

                mk_spoof                => 1,

                %{ $opts->{config} }
            }
        },
    );

    return $self->create_named_interface_ok(
        {
            module          => 'stufprs',
            name            => 'STUF PRS Parsing',
            active          => 1,
        }
    );
}

sub create_xential_interface_ok {
    my ($self, %opts) = @_;

    my $base_host = delete $opts{base_host} || 'https://10.44.0.11/foo/bar';

    return $self->create_interface_ok(
        name             => 'Xential',
        module           => 'xential',
        max_retries      => 1,
        interface_config => {
            infoservice             => "$base_host/InfoService",
            buildservice            => "$base_host/BuildService",
            contextservice          => "$base_host/ContextService",
            editservice             => "$base_host/EditService?frits=baas",
            contact_service_base_id => 'zs_testsuite',
            contextservice_username => 'zaaksysteem',
            contextservice_password => 'testsuite',
            uri                     => $base_host,
            timeout                 => '10',
            spoofmode               => 1,
            %opts,
        },
    );
}


=head2 create_map_interface_ok

=cut

sub create_map_interface_ok {
    my ($self, %opts) = @_;

    return $self->create_named_interface_ok(
        {
            name             => 'Map configuration',
            module           => 'map',
            interface_config => {
                wms_layers      => [
                    {
                        id      => 'hoogtenl',
                        label   => 'Hoogtebestand Nederland',
                        url     => 'https://geodata.nationaalgeoregister.nl/ahn1/wms?&request=GetCapabilities',
                        active  => 1,
                    },
                    {
                        id      => 'adrnl',
                        label   => 'Adressen Nederland',
                        url     => 'https://geodata.nationaalgeoregister.nl/inspireadressen/wms?&request=GetCapabilities',
                        active  => 0,
                    }
                ],
                %opts,
            },
        }
    );


}

=head2 stuf_0204

Get the XML Compiles StUF parser for StUF 0204

=cut

sub stuf_0204 {
    my $self = shift;
    return $self->xmlcompile->add_class('Zaaksysteem::StUF::0204::Instance')->stuf0204;
}

=head2 stuf_0301

Get the XML Compiles StUF parser for StUF 0301

=cut

sub stuf_0301 {
    my $self = shift;
    return $self->xmlcompile->add_class('Zaaksysteem::StUF::0301::Instance')->stuf0301;
}

=head2 stuf_0310

Get the XML Compiles StUF parser for StUF 0310

=cut

sub stuf_0310 {
    my $self = shift;
    return $self->xmlcompile->add_class('Zaaksysteem::XML::Generator::StUF0310')->stuf0310;
}

=head2 stuf_0312

Get the XML Compiles StUF parser for StUF 0312

=cut

sub stuf_0312 {
    my $self = shift;
    return $self->xmlcompile->add_class('Zaaksysteem::StUF::0312::Instance')->stuf0312;
}

=head2 mijnoverheid

Get the XML Compile parser for MijnOverheid

=cut

sub mijnoverheid {
    my $self = shift;
    return $self->xmlcompile->add_class('Zaaksysteem::XML::MijnOverheid::Instance')->mijnoverheid;
}

=head2 iwmo

Get the XML Compiles StUF parser for StUF 0310

=cut

sub iwmo_2_0 {
    my $self = shift;
    return $self->xmlcompile->add_class('Zaaksysteem::XML::Generator::iWMO::2_0')->iwmo2_0;
}

sub ijw_2_0 {
    my $self = shift;
    return $self->xmlcompile->add_class('Zaaksysteem::XML::Generator::iJW::2_0')->ijw2_0;
}


=head2 get_json_as_perl

Returns a given json string as a perl hash.

=cut

sub get_json_as_perl {
    my $self    = shift;
    my $json    = shift;

    my $jo      = JSON->new->utf8->pretty->canonical;

    return $jo->decode($json);
}

=head2 xential

Get the XML Compile parser for Xential

=cut

sub xential {
    my $self = shift;
    return $self->xmlcompile->add_class('Zaaksysteem::XML::Xential::Instance')->xential;
}

=head2 zaaksysteem

Get the XML Compile parser for Zaaksysteem

=cut

sub zaaksysteem {
    my $self = shift;
    return $self->xmlcompile->add_class('Zaaksysteem::XML::Zaaksysteem::Instance')->zaaksysteem;
}

=head2 num_queries_ok

=over 4

=item Arguments: $CODEREF, $NUMBER_ALLOWED

=item Return Value: 1

=back

    ### Allow one query
    $zs->num_queries_ok(
        sub {
            $schema->resultset('Message')->search()->first;
        },
        1
    );

Tests whether the query stays within the given number of queries

=cut

sub num_queries_ok {
    my $self    = shift;
    my $sub     = shift;
    my $num     = shift;
    my $msg     = shift;

    my $die;

    $self->schema->storage->debug(1);
    $self->schema->storage->debugobj->in_test_count(1);
    $self->schema->storage->debugobj->reset_test_count;

    ### Calculate time
    my $t0      = [gettimeofday];
    eval {
        $sub->();
    };
    my $elapsed = tv_interval ( $t0, [gettimeofday]);


    if ($@) {
        $die = $@;
    }

    my $testcount = $self->schema->storage->debugobj->test_count;

    ok($testcount <= $num, ($msg || "Number of queries below or equal to $num, got ") . $testcount . " in $elapsed seconds.");

    $self->schema->storage->debugobj->in_test_count(0);
    $self->schema->storage->debugobj->reset_test_count;
    $self->schema->storage->debug(0);

    if ($die) {
        die($die);
    }

    return 1;
}

define_profile set_attribute_mapping_on_interface_ok => (
    required => {
        interface  => 'Zaaksysteem::Model::DB::Interface',
        casetype   => 'Any',
        attributes => 'HashRef',
    },
    optional => {},

);

sub set_attribute_mapping_on_interface_ok {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    my %kenmerken = %{ $opts->{attributes} };
    my %kenmerken_mapping;

    my $lib_cat = $self->create_bibliotheek_categorie_ok;
    my $node    = $opts->{casetype}->zaaktype_node_id;
    my $zt_id   = $opts->{casetype}->id;

    my @attributes;
    foreach (keys %kenmerken) {
        my $val = $kenmerken{$_};
        push(
            @attributes,
            {
                attribute_type => 'magic_string',
                case_type_id   => $zt_id,
                checked        => 1,
                external_name  => $_,
                internal_name  => {
                    searchable_object_id    => $val,
                    searchable_object_label => $val,
                    searchable_object_type  => 'magicstring'
                },
                optional => 0
            }
        );

        my $bk = $self->create_bibliotheek_kenmerk_ok(
            magic_string          => $val,
            bibliotheek_categorie => $lib_cat,
        );

        my $ztk = $self->create_zaaktype_kenmerk_ok(
            bibliotheek_kenmerk => $bk,
            node                => $node,
        );

        $kenmerken_mapping{$ztk} = $bk;
    }

    $opts->{interface}->set_attribute_mapping({ attributes => \@attributes });
    return \%kenmerken_mapping;
}

define_profile process_interface_trigger_ok => (
    required => {
        interface => 'Zaaksysteem::Model::DB::Interface',
        params    => 'HashRef',
    },
    optional => { trigger => 'Str', },
    defaults => { trigger => 'spoofmode', }
);

sub process_interface_trigger_ok {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    my $pt = $opts->{interface}->process_trigger($opts->{trigger}, $opts->{params});
    my $rs = $pt->records({}, { order => '-desc' });
    my $ok = is($rs->count, 1, "Got one record");

    my $record;
    if ($ok) {
        $record = $rs->first;
        ok(!$record->is_error, "No error found in transaction record");
    }
    return ($pt, $record);
}

define_profile test_generated_xml_ok => (
    required => {
        reader => 'Defined',
        xml    => 'Str',
    },
    optional => {
        expect  => 'Defined',
        msg     => 'Str',
        no_diag => 'Bool',
    },
    defaults => {
        msg => 'XML::Compile can parse the generated XML correctly',
    }
);

sub test_generated_xml_ok {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    my $parsed_xml;
    my $ok = lives_ok(sub {
        $parsed_xml = $opts->{reader}->($opts->{xml});
    }, "lives_ok: $opts->{msg}");

    if ($ok) {
        if (exists $opts->{expect}) {
            return cmp_deeply($parsed_xml, $opts->{expect}, "cmp_deeply: $opts->{msg}");
        }
    }
    else {
        diag $opts->{xml} unless $opts->{no_diag};
    }
    return $ok;

}

has wmo2_0_generator => (
    is      => 'ro',
    isa     => 'Zaaksysteem::XML::Generator::iWMO::2_0',
    lazy    => 1,
    default => sub {
        return Zaaksysteem::XML::Generator::iWMO::2_0->new();
    },
);

has wmo301_schema => (
    is      => 'ro',
    isa     => 'XML::Compile::Schema',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return XML::Compile::Schema->new(
                [
                    map { catfile($self->basedir, qw(share xsd iwmo), $_) }
                        qw(
                            Basisschema.xsd
                            WMO301.xsd
                            WMO302.xsd
                        )
                ]
        );
    },
);

has jw_schema => (
    is      => 'ro',
    isa     => 'XML::Compile::Schema',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return XML::Compile::Schema->new(
                [
                    map { catfile($self->basedir, qw(share xsd ijw), $_) }
                        qw(
                            Basisschema.xsd
                            JW301.xsd
                            JW302.xsd
                        )
                ]
        );
    },
);

sub api_v1_datetime {
    my $self = shift;
    my $dt = shift || DateTime->now();
    return $dt->set_time_zone("UTC")->iso8601 . "Z";
}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
