#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use JSON;

use TestSetup;
initialize_test_globals_ok;
use Test::Deep;
use Data::FormValidator;
use File::Spec::Functions;


BEGIN { use_ok('Zaaksysteem::StUF') };
BEGIN { use_ok('Zaaksysteem::StUF::Stuurgegevens') };

my $VALIDATION_MAP      = {
    'ADR'   => {
        'postcode'                  => '1015XK',
        'woonplaatsnaam'            => 'Amsterdam',
        'straatnaam'                => 'Donker Curtiusstraat',
        'huisnummer'                => '7',
    }
};

$zs->zs_transaction_ok(sub {
    my $stuf    = Zaaksysteem::StUF->from_file(
        catfile(STUF_TEST_XML_PATH, '0204', 'adr/101-adr-create.xml'),
    );

    is($stuf->entiteittype, 'ADR', 'Found entiteittype ADR');

    my $params  = $stuf->as_params;
    #note(explain($stuf->as_params));

    for my $key (keys %{ $VALIDATION_MAP->{ADR} }) {
        my $givenvalue = $params->{ADR}->{ $key };
        my $wantedvalue = $VALIDATION_MAP->{ADR}->{ $key };

        is($givenvalue, $wantedvalue, 'Correct value for: ' . $key);
    }

    my $adr_params = $stuf->get_params_for_adr;

    ### We have to poke this resultset, else the profile wont get loaded
    $schema->resultset('BagWoonplaats');

    my $rv = Data::FormValidator->check(
        $adr_params,
        Params::Profile->get_profile('method' => 'Zaaksysteem::Backend::BagWoonplaats::ResultSet::bag_create_or_update')
    );

    ok($rv->success, 'Valid data for BagWoonplaats profile');

}, 'Check ADR native params');

$zs->zs_transaction_ok(sub {
    my $stuf    = Zaaksysteem::StUF->from_file(
        catfile(STUF_TEST_XML_PATH, '0204', 'adr/101-adr-create.xml'),
    );

    my $adr_params = $stuf->get_params_for_adr;

    $schema->resultset('BagWoonplaats')->bag_create_or_update(
        $adr_params,
    );

}, 'Import ADR in database');


zs_done_testing;
