#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';

use TestSetup;
initialize_test_globals_ok;### Test header end

use Zaaksysteem::Search::Term::Set;
use Zaaksysteem::Search::Term::Function;

my $completion   = "CAST(index_hstore->'case.date_of_completion' AS timestamp)";
my $registration = "CAST(index_hstore->'case.date_of_registration' AS timestamp)";
my $target       = "CAST(index_hstore->'case.date_target' AS timestamp)";

{
    my $term = Zaaksysteem::Search::Term::Function->new(
        object_type => 'case',
        function    => 'urgency',
        arguments   => Zaaksysteem::Search::Term::Set->new(values => []),
    );

    my @rv = $term->evaluate();
    is(@rv, 1, "Evaluating 'case->urgency' returns one element");

    $rv[0] =~ s/^\s+|\s+$//g;

    like(
        $rv[0],
        qr/CASE.+WHEN.+normal.+medium.+high.+late/ms,
        "The returned query part looks OK",
    );
}

{
    throws_ok(
        sub {
            my $term = Zaaksysteem::Search::Term::Function->new(
                object_type => 'case',
                function    => 'non-existent',
                arguments   => Zaaksysteem::Search::Term::Set->new(values => []),
            );
            $term->evaluate();
        },
        'Zaaksysteem::Exception::Base',
        'Unknown function throws exception',
    );
    throws_ok(
        sub {
            my $term = Zaaksysteem::Search::Term::Function->new(
                object_type => 'global',
                function    => 'urgency',
                arguments   => Zaaksysteem::Search::Term::Set->new(values => []),
            );
            $term->evaluate();
        },
        'Zaaksysteem::Exception::Base',
        'Unknown function (in known namespace) throws exception',
    );
    throws_ok(
        sub {
            my $term = Zaaksysteem::Search::Term::Function->new(
                object_type => 'global',
                function    => 'non-existent',
                arguments   => Zaaksysteem::Search::Term::Set->new(values => []),
            );
            $term->evaluate();
        },
        'Zaaksysteem::Exception::Base',
        'Unknown function (in unknown namespace) throws exception',
    );
}

zs_done_testing();
