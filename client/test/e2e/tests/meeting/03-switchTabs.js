describe('when opening the app', ( ) => {

	beforeAll(( ) => {

		browser.get('/vergadering/bbv');

	});

	it('should contain a header', ( ) => {

		let nav = element.all(by.css('meeting-nav'));

		expect(nav.count()).toBeGreaterThan(0);

	});

	it('should contain tabs', ( ) => {

		let tabs = element.all(by.css('.meeting-nav__tabs a'));

		expect(tabs.count()).toBe(2);

	});

	it('when clicking on a tab it should navigate to that tab', ( ) => {

		let tab = element.all(by.css('.meeting-nav__tabs a')).last(),
			closedProposal = $('.proposal-item-table tbody .closed');

		tab.click();

		expect(closedProposal.isPresent()).toBe(true);

	});

	afterAll(( ) => {

		browser.get('/intern');

	});

});
