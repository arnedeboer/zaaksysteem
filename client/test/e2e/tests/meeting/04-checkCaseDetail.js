import login from './../../functions/common/auth/login';
import logout from './../../functions/common/auth/logout';
import loginAs from './../../functions/common/auth/loginAs';

describe('when clicking on a case in the caseList', ( ) => {

	beforeAll(( ) => {

		logout();

		loginAs('burgemeester');

		browser.get('/vergadering/bbv');

	});

	it('should navigate to the detailView', ( ) => {

		let firstProposal = element.all(by.css('.proposal-item-table tbody tr')).last();

		firstProposal.click();

		expect(browser.getCurrentUrl()).toContain('/voorstel/');

	});

	it('should contain a detailView', ( ) => {

		let proposalDetailView = element.all(by.css('proposal-detail-view'));

		expect(proposalDetailView.count()).toBeGreaterThan(0);

	});

	it('should have a detailView that contains a proposal note bar', ( ) => {

		let noteBar = $('proposal-note-bar');

		expect(noteBar.isPresent()).toBe(true);

	});

	it('should have a detailView that contains a proposal vote bar', ( ) => {

		let voteBar = $('proposal-vote-bar');

		expect(voteBar.isPresent()).toBe(true);

	});

	it('should display the information of the case properly', ( ) => {

		let caseNumber = $('.proposal-detail-view__list li:nth-child(1) .proposal-detail-view__value'),
			caseType = $('.proposal-detail-view__list li:nth-child(2) .proposal-detail-view__value'),
			subject = $('.proposal-detail-view__list li:nth-child(3) .proposal-detail-view__value'),
			caseConfidentiality = $('.proposal-detail-view__list li:nth-child(4) .proposal-detail-view__value'),
			meetingAgendaType = $('.proposal-detail-view__list li:nth-child(5) .proposal-detail-view__value'),
			caseResult = $('.proposal-detail-view__list li:nth-child(6) .proposal-detail-view__value');

		expect(caseNumber.getText()).toEqual('41');
		expect(caseType.getText()).toEqual('Voorstel 2');
		expect(subject.getText()).toEqual('Openstaand voorstel');
		expect(caseConfidentiality.getText()).toEqual('Openbaar');
		expect(meetingAgendaType.getText()).toEqual('Paraafstukken');
		expect(caseResult.getText()).toEqual('aangehouden');

	});

	afterAll(( ) => {

		logout();

		login();

		browser.get('/intern');

	});

});
