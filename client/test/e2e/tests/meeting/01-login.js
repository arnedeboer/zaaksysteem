import login from './../../functions/common/auth/login';
import logout from './../../functions/common/auth/logout';
import attemptLogin from './../../functions/common/auth/attemptLogin';

describe('when logging in', ( ) => {

	beforeEach(( ) => {

		logout();

		browser.get('/vergadering/');

	});

	describe('with valid credentials', ( ) => {

		beforeEach(() => {

			login();

		});

		it('should redirect to the application', ( ) => {

			expect(browser.getCurrentUrl()).toMatch(/vergadering/);

		});

	});

	describe('with invalid credentials', ( ) => {

		beforeEach(() => {

			attemptLogin('admin', 'wrong password');

		});

		it('should redirect to the login form', ( ) => {

			expect(browser.getCurrentUrl()).toMatch('/auth/page');

		});

		afterAll(() => {

			login();

		});

	});

	afterAll(( ) => {

		browser.get('/intern/');

	});

});
