import login from './../../../../functions/common/auth/login';
import loginAs from './../../../../functions/common/auth/loginAs';
import logout from './../../../../functions/common/auth/logout';
import openPlusAction from './../../../../functions/intern/plusActions/openPlusAction';
import inputSelectFirst from './../../../../functions/common/input/inputSelectFirst';
import getFieldSelector from './../../../../functions/common/getFieldSelector';

describe('when logging in as behandelaar', ( ) => {

	beforeAll(( ) => {

		logout();
		loginAs('behandelaar');

	});

	describe('and open a registration form', ( ) => {

		beforeAll(( ) => {

			let form = $('zs-contextual-action-menu form');

			openPlusAction('zaak');

			inputSelectFirst(form, 'casetype', 'basic casetype');

			form.$(getFieldSelector('requestor_type'))
				.$('input[type=radio][value=natuurlijk_persoon')
				.click();

			inputSelectFirst(form, 'requestor', '123456789');

			form.submit();

		});

		it('should not have the skip required option present', ( ) => {

			expect($('.cheating').isPresent()).toBe(false);

		});

	});

	afterAll(( ) => {

		logout();
		login();

	});

});

describe('when logging in as zaakbeheerder', ( ) => {

	beforeAll(( ) => {

		logout();
		loginAs('zaakbeheerder');

	});

	describe('and open a registration form', ( ) => {

		beforeAll(( ) => {

			let form = $('zs-contextual-action-menu form');

			openPlusAction('zaak');

			inputSelectFirst(form, 'casetype', 'basic casetype');

			form.$(getFieldSelector('requestor_type'))
				.$('input[type=radio][value=natuurlijk_persoon')
				.click();

			inputSelectFirst(form, 'requestor', '123456789');

			form.submit();

		});

		it('should have the skip required option present', ( ) => {

			expect($('.cheating').isPresent()).toBe(true);

		});

	});

	afterAll(( ) => {

		logout();
		login();

	});

});
