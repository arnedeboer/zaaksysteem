import login from './../../../../functions/common/auth/login';
import loginAs from './../../../../functions/common/auth/loginAs';
import logout from './../../../../functions/common/auth/logout';

describe('when logging in as behandelaar', ( ) => {

	beforeAll(( ) => {

		logout();
		loginAs('behandelaar');

	});

	describe('and opening the documentintake', ( ) => {

		beforeAll(( ) => {

			browser.get('/zaak/intake?scope=documents');

		});

		it('should not have the type selector present', ( ) => {

			expect($('.document-list-visibility-select').isDisplayed()).toBe(false);
				
		});

	});

	afterAll(( ) => {

		logout();
		login();

	});

});

describe('when logging in as documentintaker', ( ) => {

	beforeAll(( ) => {

		logout();
		loginAs('documentintaker');

	});

	describe('and opening the documentintake', ( ) => {

		beforeAll(( ) => {

			browser.get('/zaak/intake?scope=documents');

		});

		it('should have the type selector present', ( ) => {

			expect($('.document-list-visibility-select').isDisplayed()).toBe(true);
				
		});

	});

	afterAll(( ) => {

		logout();
		login();


	});

});
