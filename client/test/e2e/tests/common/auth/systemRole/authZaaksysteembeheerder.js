import login from './../../../../functions/common/auth/login';
import loginAs from './../../../../functions/common/auth/loginAs';
import logout from './../../../../functions/common/auth/logout';
import verifyRestricted from './../../../../functions/common/auth/verifyRestricted';

describe('when logging in as behandelaar', ( ) => {

	beforeAll(( ) => {

		logout();
		loginAs('behandelaar');

	});

	describe('and opening the user page', ( ) => {

		it('should show the no access page', ( ) => {

			expect(verifyRestricted('/medewerker')).toBe(true);

		});

	});

	describe('and opening the log page', ( ) => {

		it('should show the no access page', ( ) => {

			expect(verifyRestricted('/beheer/logging')).toBe(true);

		});

	});

	describe('and opening the transaction page', ( ) => {

		it('should show the no access page', ( ) => {

			expect(verifyRestricted('/beheer/sysin/transactions')).toBe(true);

		});

	});

	describe('and opening the configuration page', ( ) => {

		it('should show the no access page', ( ) => {

			expect(verifyRestricted('/beheer/configuration')).toBe(true);

		});

	});

	afterAll(( ) => {

		logout();
		login();

	});

});

describe('when logging in as zaaksysteembeheerder', ( ) => {

	beforeAll(( ) => {

		logout();
		loginAs('zaaksysteembeheerder');

	});

	describe('and opening the user page', ( ) => {

		it('should not show the no access page', ( ) => {

			expect(verifyRestricted('/medewerker')).toBe(false);

		});

	});

	describe('and opening the log page', ( ) => {

		it('should not show the no access page', ( ) => {

			expect(verifyRestricted('/beheer/logging')).toBe(false);

		});

	});

	describe('and opening the transaction page', ( ) => {

		it('should not show the no access page', ( ) => {

			expect(verifyRestricted('/beheer/sysin/transactions')).toBe(false);

		});

	});

	describe('and opening the configuration page', ( ) => {

		it('should not show the no access page', ( ) => {

			expect(verifyRestricted('/beheer/configuration')).toBe(false);

		});

	});

	afterAll(( ) => {

		logout();
		login();

	});

});
