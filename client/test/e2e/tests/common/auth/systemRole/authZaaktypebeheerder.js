import login from './../../../../functions/common/auth/login';
import loginAs from './../../../../functions/common/auth/loginAs';
import logout from './../../../../functions/common/auth/logout';
import verifyRestricted from './../../../../functions/common/auth/verifyRestricted';
import getCaseUrl from './../../../../functions/common/getValue/getCaseUrl';

describe('when logging in as behandelaar', ( ) => {

	beforeAll(( ) => {

		logout();
		loginAs('behandelaar');

	});

	describe('and opening the catalog', ( ) => {

		it('should show the no access page', ( ) => {

			expect(verifyRestricted('/beheer/bibliotheek')).toBe(true);

		});

	});

	describe('and opening a case', ( ) => {

		beforeAll(( ) => {

			browser.get(getCaseUrl('24'));

		});

		it('should not have the rule mode button present', ( ) => {

			expect($('[data-name="debug_rules"]').isPresent()).toBe(false);

		});

	});

	afterAll(( ) => {

		logout();
		login();

	});

});

describe('when logging in as zaaktypebeheerder', ( ) => {

	beforeAll(( ) => {

		logout();
		loginAs('zaaktypebeheerder');

	});

	describe('and opening the catalog', ( ) => {

		it('should not show the no access page', ( ) => {

			expect(verifyRestricted('/beheer/bibliotheek')).toBe(false);

		});

	});

	describe('and opening a case', ( ) => {

		beforeAll(( ) => {

			logout();
			loginAs('zaaktypebeheerder');

			browser.get(getCaseUrl('24'));

		});

		it('should have the rule mode button present', ( ) => {

			expect($('[data-name="debug_rules"]').isPresent()).toBe(true);

		});

	});

	afterAll(( ) => {

		logout();
		login();

	});

});
