import login from './../../../../functions/common/auth/login';
import loginAs from './../../../../functions/common/auth/loginAs';
import logout from './../../../../functions/common/auth/logout';
import searchFor from './../../../../functions/intern/topbar/spotEnlighter/searchFor';
import closeSpotEnlighter from './../../../../functions/intern/topbar/spotEnlighter/closeSpotEnlighter';
import getContactUrl from './../../../../functions/common/getValue/getContactUrl';

describe('when logging in as user with authorisation to search case 25', ( ) => {

	beforeAll(( ) => {

		logout();
		loginAs('metzaakoverzichtrechten');

	});

	describe('and arriving on the dashboard', ( ) => {

		it('should have case 25 present', ( ) => {

			expect($('[href="/intern/zaak/25"]').isPresent()).toBe(true);

		});

	});

	describe('and searching for case 25 in general search', ( ) => {

		beforeAll(( ) => {

			searchFor('zaakoverzichtrechten');

		});

		it('should have case 25 present', ( ) => {

			expect($('[href="/intern/zaak/25"]').isPresent()).toBe(true);

		});

		afterAll(( ) => {

			closeSpotEnlighter();

		});

	});

	describe('and opening advanced search', ( ) => {

		beforeAll(( ) => {

			browser.get('/search/all');

		});

		it('should have case 25 present', ( ) => {

			expect($('[href="/intern/zaak/25"]').isPresent()).toBe(true);

		});

		afterAll(( ) => {

			browser.get('');

		});

	});

	describe('and opening contact overview', ( ) => {

		beforeAll(( ) => {

			browser.ignoreSynchronization = true;

			browser.get(getContactUrl('np', '3'));

		});

		it('should have case 25 present', ( ) => {

			expect($('#zaak25').isPresent()).toBe(true);

		});

		afterAll(( ) => {

			browser.ignoreSynchronization = false;

			browser.get('');

		});

	});

	afterAll(( ) => {

		logout();
		login();

	});

});

describe('when logging in as user without authorisation to search case 25', ( ) => {

	beforeAll(( ) => {

		logout();
		loginAs('zonderzaakoverzichtrechten');

	});

	describe('and arriving on the dashboard', ( ) => {

		it('should not have case 25 present', ( ) => {

			expect($('[href="/intern/zaak/25"]').isPresent()).toBe(false);

		});

	});

	describe('and searching for case 25 in general search', ( ) => {

		beforeAll(( ) => {

			searchFor('zaakoverzichtrechten');

		});

		it('should not have case 25 present', ( ) => {

			expect($('[href="/intern/zaak/25"]').isPresent()).toBe(false);

		});

		afterAll(( ) => {

			closeSpotEnlighter();

		});

	});

	describe('and opening advanced search', ( ) => {

		beforeAll(( ) => {

			browser.get('/search/all');

		});

		it('should not have case 25 present', ( ) => {

			expect($('[href="/intern/zaak/25"]').isPresent()).toBe(false);

		});

		afterAll(( ) => {

			browser.get('');

		});

	});

	describe('and opening contact overview', ( ) => {

		beforeAll(( ) => {

			browser.ignoreSynchronization = true;

			browser.get(getContactUrl('np', '3'));

		});

		it('should not have case 25 present', ( ) => {

			expect($('#zaak25').isPresent()).toBe(false);

		});

		afterAll(( ) => {

			browser.ignoreSynchronization = false;

			browser.get('');

		});

	});

	afterAll(( ) => {

		logout();
		login();

	});

});
