describe('when opening the app', ( ) => {

	beforeAll(( ) => {

		browser.get('/pdc');

	});

	it('should contain a header', ( ) => {

		let nav = element.all(by.css('pdc-nav'));
		
		expect(nav.count()).toBeGreaterThan(0);

	});


	it('should contain tabs', ( ) => {

		let tabs = element.all(by.css('.pdc-nav__tabs a'));
		
		expect(tabs.count()).toBeGreaterThan(0);

	});

	it('when clicking on a tab it should navigate to that tab', ( ) => {

		let tab = element.all(by.css('.pdc-nav__tabs a')).last(),
			href = tab.getAttribute('href');

		tab.click();

		expect(browser.getCurrentUrl()).toContain(href);
		
	});

	afterAll(( ) => {

		browser.get('/intern');

	});

});
