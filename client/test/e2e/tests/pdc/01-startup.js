describe('when opening the pdc app', ( ) => {

	beforeAll(( ) => {

		browser.get('/pdc/');

	});

	it('should redirect to the PDC app without logging in', ( ) => {

		expect(browser.getCurrentUrl()).toContain('/pdc/');

	});

	afterAll(( ) => {

		browser.get('/intern');

	});

});
