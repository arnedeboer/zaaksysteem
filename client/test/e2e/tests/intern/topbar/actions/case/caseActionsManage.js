import getCaseUrl from './../../../../../functions/common/getValue/getCaseUrl';
import openAction from './../../../../../functions/intern/topbar/actions/openAction';
import toggleActionMenu from './../../../../../functions/intern/topbar/actions/toggleActionMenu';
import openAdditionalInformation from './../../../../../functions/intern/caseView/additionalInformation/openAdditionalInformation';
import closeDialog from './../../../../../functions/common/closeDialog';
import caseUpdateRequestor from './../../../../../functions/intern/topbar/actions/actionsCase/caseUpdateRequestor';
import caseUpdateCoordinator from './../../../../../functions/intern/topbar/actions/actionsCase/caseUpdateCoordinator';
import caseUpdateDepartmentRole from './../../../../../functions/intern/topbar/actions/actionsCase/caseUpdateDepartmentRole';
import caseUpdateRegistrationDate from './../../../../../functions/intern/topbar/actions/actionsCase/caseUpdateRegistrationDate';
import caseUpdateTargetDate from './../../../../../functions/intern/topbar/actions/actionsCase/caseUpdateTargetDate';
import caseUpdateCompletionDate from './../../../../../functions/intern/topbar/actions/actionsCase/caseUpdateCompletionDate';
import caseUpdateDestructionDate from './../../../../../functions/intern/topbar/actions/actionsCase/caseUpdateDestructionDate';
import caseUpdateAttribute from './../../../../../functions/intern/topbar/actions/actionsCase/caseUpdateAttribute';
import caseUpdatePhase from './../../../../../functions/intern/topbar/actions/actionsCase/caseUpdatePhase';
import caseUpdateStatus from './../../../../../functions/intern/topbar/actions/actionsCase/caseUpdateStatus';
import caseUpdateAuthentication from './../../../../../functions/intern/topbar/actions/actionsCase/caseUpdateAuthentication';
import caseUpdateCasetype from './../../../../../functions/intern/topbar/actions/actionsCase/caseUpdateCasetype';
import caseCloseEarly from './../../../../../functions/intern/topbar/actions/actionsCase/caseCloseEarly';
import getAboutValue from './../../../../../functions/intern/caseView/additionalInformation/getAboutValue';

describe('when opening case 26', ( ) => {

	beforeAll(( ) => {

		browser.get(getCaseUrl('26'));

	});

	describe('when updating the requestor of a case', ( ) => {

		beforeAll(( ) => {

			caseUpdateRequestor('natuurlijk_persoon', 'zaakacties');

			openAdditionalInformation();

		});

		it('the case should have a different requestor', ( ) => {

			expect(getAboutValue('Aanvrager')).toEqual('z. zaakacties');

		});

		afterAll(( ) => {

			closeDialog();

		});

	});

	describe('when updating the coordinator of a case', ( ) => {

		beforeAll(( ) => {

			caseUpdateCoordinator('coordinatorwijzigen');

			openAdditionalInformation();

		});

		it('the case should have a different coordinator', ( ) => {

			expect(getAboutValue('Coordinator')).toEqual('Coordinator wijzigen');

		});

		afterAll(( ) => {

			closeDialog();

		});

	});

	describe('when updating the department and role of a case', ( ) => {


		beforeAll(( ) => {

			caseUpdateDepartmentRole('-Zaakacties', 'Behandelaar');

			openAdditionalInformation();

		});

		it('the case should have a different department and role', ( ) => {

			expect(getAboutValue('Afdeling')).toEqual('Zaakacties');

		});

		afterAll(( ) => {

			closeDialog();

		});

	});

	describe('when updating the registration date of a case', ( ) => {

		beforeAll(( ) => {

			caseUpdateRegistrationDate('13', 'Jan', '2010');

			openAdditionalInformation();

		});

		it('the case should have a different registration date', ( ) => {

			expect(getAboutValue('Registratiedatum')).toEqual('13-01-2010');

		});

		afterAll(( ) => {

			closeDialog();

		});

	});

	describe('when updating the target date of a case', ( ) => {

		beforeAll(( ) => {

			caseUpdateTargetDate('14', 'Jan', '2010');

			openAdditionalInformation();

		});

		it('the case should have a different target date', ( ) => {

			expect(getAboutValue('Streefafhandeldatum')).toEqual('14-01-2010');

		});

		afterAll(( ) => {

			closeDialog();

		});

	});

	describe('when updating the completion date of a case', ( ) => {

		beforeAll(( ) => {

			caseCloseEarly('aangehouden');

			caseUpdateCompletionDate('15', 'Jan', '2010');

			openAdditionalInformation();

		});

		it('the case should have a different completion date', ( ) => {

			expect(getAboutValue('Afhandeldatum')).toEqual('15-01-2010');

		});

		afterAll(( ) => {

			closeDialog();

			caseUpdateStatus('In behandeling');

			toggleActionMenu();

		});

	});

	describe('when updating the destruction date of a case', ( ) => {

		beforeAll(( ) => {

			caseCloseEarly('aangehouden');

			caseUpdateDestructionDate('16', '01', '2010');

			openAdditionalInformation();

		});

		it('the case should have a different destruction date', ( ) => {

			expect(getAboutValue('Uiterste vernietigingsdatum')).toEqual('16-01-2010');

		});

		afterAll(( ) => {

			closeDialog();

			caseUpdateStatus('In behandeling');

			toggleActionMenu();

		});

	});

	describe('when updating the attribute of a case', ( ) => {

		beforeAll(( ) => {

			caseUpdateAttribute('omschrijving', 'Update attribute');

			openAdditionalInformation();

		});

		it('the case should have a different value in the attribute', ( ) => {

			expect(getAboutValue('Extra informatie')).toEqual('Update attribute');

		});

		afterAll(( ) => {

			closeDialog();

		});

	});

	describe('when updating the phase of a case', ( ) => {

		beforeAll(( ) => {

			$('.phase-advance-button').click();

			caseUpdatePhase('Behandelen');

		});

		it('the case should have be in a different phase', ( ) => {

			expect($('.phase-list .active [href="/intern/zaak/26/fase/behandelen/"]').isPresent()).toBe(true);

		});

		afterAll(( ) => {

			toggleActionMenu();

		});

	});

	describe('when updating the status of a case', ( ) => {

		beforeAll(( ) => {

			caseUpdateStatus('Afgehandeld');

		});

		it('the case should have a different status', ( ) => {

			expect($('[data-name="resolved"]').isPresent()).toBe(true);

		});

		afterAll(( ) => {

			toggleActionMenu();

			caseUpdateStatus('In behandeling');

			toggleActionMenu();

		});

	});

	describe('when updating the authentication settings of a case', ( ) => {

		beforeAll(( ) => {

			caseUpdateAuthentication('-Systeemrollen', 'Behandelaar', 'handle');

			openAction('Rechten wijzigen');

		});

		it('the case should have different authentication settings', ( ) => {

			expect($('zs-case-admin-view [checked="checked"]').isPresent()).toBe(true);

		});

		afterAll(( ) => {

			closeDialog();

		});


	});

	describe('when updating the casetype of a case', ( ) => {

		beforeAll(( ) => {

			caseUpdateCasetype('Zaakacties zaaktype wijzigen');

		});

		it('the case should be of a different casetype', ( ) => {

			expect($('[data-name="zaakacties"]').isPresent()).toBe(true);

		});

		afterAll(( ) => {

			caseUpdateCasetype('Zaakacties');

		});

	});

	afterAll(( ) => {

		browser.waitForAngular();

		browser.get('');

	});

});
