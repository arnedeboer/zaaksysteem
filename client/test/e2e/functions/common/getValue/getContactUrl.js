export default ( type, number ) => {

	let contactType;

	switch (type) {
		case 'np':
			contactType = 'natuurlijk_persoon';
			break;
		case 'nnp':
			contactType = 'bedrijf';
			break;
		default:
			contactType = 'medewerker';
			break;
	}

	return `/betrokkene/${number}/?gm=1&type=${contactType}`;

};
