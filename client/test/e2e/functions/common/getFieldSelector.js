export default ( name ) => {
	return `.vorm-field-item[data-name=${name}]`;
};
