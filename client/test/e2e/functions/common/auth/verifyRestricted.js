export default ( page ) => {

	let restrictedPage = element(by.css( '.no-access' ));

	browser.get(page);

	return restrictedPage.isPresent();

};
