export default ( attribute, selectToInput ) => {

	attribute.getAttribute('class').then((attributeClass) => {

		let inputSelect,
			i;

		if (attributeClass.includes('multiple')) {

			inputSelect = selectToInput ? selectToInput : [1, 2, 3, 4];

		} else {

			inputSelect = selectToInput ? selectToInput : [1];

		}

		for (i = 0; i < inputSelect.length; i++) {

			attribute.$(`li:nth-child(${i + 1}) select`).click();

			attribute.$(`li:nth-child(${i + 1}) select option:nth-child(${inputSelect[i] + 1})`).click();

			if ( inputSelect.length - 1 > i ) {

				attribute.$('.vorm-field-add-button').click();

			}

		}
		
	});

};
