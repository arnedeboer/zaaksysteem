export default ( attribute, fileToInput ) => {

	attribute.$('.file-upload-zone button').getAttribute('name').then((name) => {

		let path = require('path'),
		inputFile = fileToInput ? fileToInput : './../../../../utilities/text.txt',
		absolutePath = path.resolve(__dirname, inputFile);

		$(`input[name="${name}"]`).sendKeys(absolutePath);

	});

};
