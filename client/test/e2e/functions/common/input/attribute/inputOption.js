import random from 'lodash/random';

export default ( attribute, optionToClick ) => {

	let options = attribute.all(by.css('[type="radio"]')),
		inputOption,
		randomOption;

		options.count().then((optionCount) => {

			randomOption = random(1, optionCount);

			inputOption = optionToClick ? optionToClick : randomOption;

			attribute.$(`label:nth-child(${inputOption}) input`).click();

		});

};
