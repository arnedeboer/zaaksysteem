import inputSelectFirst from './../inputSelectFirst';

export default ( attribute, addressToInput ) => {

	let inputAddress,
		i;

	attribute.getAttribute('class').then((attributeClass) => {

		if (attributeClass.includes('multiple')) {

			inputAddress = addressToInput ? addressToInput : ['straat', 'laan'];

		} else {

			inputAddress = addressToInput ? addressToInput : ['straat'];

		}

		attribute.element(by.xpath('../..')).getAttribute('data-name').then((dataname) => {

			for (i = 0; i < inputAddress.length; i++) {

				inputSelectFirst($('.vorm-field-list'), dataname, inputAddress[i]);

			}

		});

	});

};
