export default ( attribute, addressToInput ) => {

	let inputAddress = addressToInput ? addressToInput : 'straat',
		results = attribute.all(by.css('.suggestion-list button'));

	attribute.$('input').sendKeys(inputAddress);

	browser.waitForAngular();

	results.first().click();

};
