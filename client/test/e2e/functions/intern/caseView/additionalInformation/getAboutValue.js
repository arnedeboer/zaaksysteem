export default ( valueName ) => {

	let aboutInformationNames = element.all(by.css('.case-info-group .case-info-group-field')),
		result,
		fieldValue;

	result = aboutInformationNames.filter((elm) => {
		return elm.$('.case-info-group-field-label').getText().then((text) => {
			return text === `${valueName}`;
		});
	}).first();

	fieldValue = result.$('.case-info-group-field-value').getText();

	return fieldValue;

};
