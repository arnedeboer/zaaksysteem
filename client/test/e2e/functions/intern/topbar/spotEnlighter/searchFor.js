export default ( query, objectType = 'all' ) => {
		
	let spotEnlighter = element(by.css('zs-universal-search')),
		openButton = spotEnlighter.element(by.css('[data-name="open-button"]')),
		objectTypeSelector = spotEnlighter.element(by.css('zs-universal-search [data-name="object-type-select"]')),
		option,
		input = spotEnlighter.element(by.css('[data-name="query-input"]'));

	openButton.click();
	objectTypeSelector.click();

	option = spotEnlighter.element(by.css(`[option-name=${objectType}]`));
	option.click();

	input.sendKeys(query);

};
