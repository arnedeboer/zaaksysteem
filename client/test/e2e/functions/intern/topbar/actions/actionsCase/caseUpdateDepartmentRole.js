import openAction from './../openAction';

export default ( department, role ) => {

	let form = $('zs-case-admin-view form');

	openAction('Afdeling en rol wijzigen');

	$('[data-name="department-picker"] select').sendKeys(department);
	$('[data-name="role-picker"] select').sendKeys(role);

	form.submit();

};
