import openAction from './../openAction';

export default ( department, role, authType ) => {

	let form = $('zs-case-admin-view form'),
		authLevel,
		i;

	switch (authType) {
		case 'manage':
			authLevel = '4';
			break;
		case 'handle':
			authLevel = '3';
			break;
		case 'access':
			authLevel = '2';
			break;
		case 'search':
			authLevel = '1';
			break;
		default:
			authLevel = '0';
			break;
	}

	openAction('Rechten wijzigen');

	$('[data-name="department-picker"] select').sendKeys(department);
	$('[data-name="role-picker"] select').sendKeys(role);

	for (i = 1; i <= authLevel; i++) {
		$(`.capabilities-list li:nth-child(${i})`).click();
	}

	form.submit();

};
