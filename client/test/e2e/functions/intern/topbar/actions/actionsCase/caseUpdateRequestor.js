import openAction from './../openAction';
import inputSelectFirst from './../../../../common/input/inputSelectFirst';

export default ( type, requestor ) => {

	let form = $('zs-case-admin-view form');

	openAction('Aanvrager wijzigen');

	$('zs-case-admin-view [data-name="requestor"] .vorm-clear').click();
	$(`zs-case-admin-view [data-name="type"] [value=${type}]`).click();
	
	inputSelectFirst(form, 'requestor', requestor);

	form.submit();

};
