import openAction from './../openAction';
import inputDate from './../../../../common/input/attribute/inputDate';

export default ( day, month, year ) => {

	let form = $('zs-case-admin-view form'),
		dateField = $('[data-name="streefafhandeldatum"]');

	openAction('Streefafhandeldatum wijzigen');

	inputDate(dateField, day, month, year);

	form.submit();

};
