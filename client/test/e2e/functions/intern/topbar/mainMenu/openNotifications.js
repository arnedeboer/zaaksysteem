import openMenu from './openMenu';

export default ( ) => {

	let notificationButton = '[zs-tooltip="Notificaties"]',
		notifications = '.notifications';

	openMenu();

	$(notifications).isDisplayed()
		.then(

			isDisplayed => {
				return isDisplayed ?
					isDisplayed
					: Promise.reject();
			},
			( ) => $(notificationButton).click()
			
		);

};
