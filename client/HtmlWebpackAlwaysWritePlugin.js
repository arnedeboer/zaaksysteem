var find = require('lodash/find'),
	last = require('lodash/last'),
	path = require('path'),
	endsWith = require('lodash/endsWith'),
	async = require('async'),
	fs = require('fs-extra'),
	identity = require('lodash/identity');

function HtmlWebpackAlwaysWritePlugin ( settings ) {
	this.settings = settings || {};
}

HtmlWebpackAlwaysWritePlugin.prototype.apply = function ( compiler ) {

	var stripPrefix	= this.settings.stripPrefix,
		root = this.settings.root || '.',
		entries = this.settings.entries || [];

	compiler.plugin('after-emit', ( compilation, callback ) => {

		var files =
			entries.map(( entryName ) => {

				var file = find(compilation.assets, ( asset, filename ) => endsWith(filename, path.join(entryName, 'index.html')));

				return file ?
					{
						filename: path.join(root, compilation.outputOptions.publicPath.replace(stripPrefix, ''), entryName, 'index.html'),
						source: file.source()
					}
					: null;

			})
				.filter(identity);


		async.parallel(
			files.map(
				( file ) => {
					return ( cb ) => fs.outputFile(file.filename, file.source, cb);
				}
			),
		callback);

	});
};

module.exports = HtmlWebpackAlwaysWritePlugin;
