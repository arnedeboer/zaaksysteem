import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import ocLazyLoadModule from 'oclazyload/dist/ocLazyLoad';
import assign from 'lodash/assign';
import get from 'lodash/get';
import find from 'lodash/find';
import first from 'lodash/head';
import keyBy from 'lodash/keyBy';
import keys from 'lodash/keys';
import mapKeys from 'lodash/mapKeys';
import isArray from 'lodash/isArray';
import mapValues from 'lodash/mapValues';
import identity from 'lodash/identity';
import flatten from 'lodash/flatten';
import _values from 'lodash/values';
import uniq from 'lodash/uniq';
import map from 'lodash/map';
import pickBy from 'lodash/pickBy';
import seamlessImmutable from 'seamless-immutable';
import resourceModule from './../../../shared/api/resource';
import composedReducerModule from './../../../shared/api/resource/composedReducer';
import observableStateParamsModule from './../../../shared/util/route/observableStateParams';
import shouldReloadModule from './../../../shared/util/route/shouldReload';
import auxiliaryRouteModule from './../../../shared/util/route/auxiliaryRoute';
import onRouteActivateModule from './../../../shared/util/route/onRouteActivate';
import zsModalModule from './../../../shared/ui/zsModal';
import snackbarServiceModule from './../../../shared/ui/zsSnackbar/snackbarService';
import convertv0Case from './../../../shared/api/mock/convertv0Case';
import getRelatableObjectTypesFromCasetype from './../../../shared/case/caseActions/getRelatableObjectTypesFromCasetype';
import normalizePhaseName from './zsCaseView/zsCasePhaseView/normalizePhaseName';
import template from './template.html';

let assignToScope = ( resourcesToDestroy, ...rest ) => {

	// This is not pretty. But we have to attach the resources to $rootScope,
	// and we want to destroy them along with the view scope

	let indexes,
		toDestroy = resourcesToDestroy,
		injectables = rest;

	if (!isArray(toDestroy)) {
		injectables = [ resourcesToDestroy, ...injectables ];
		toDestroy = [];
	}

	indexes = toDestroy.map( ( name ) => injectables.indexOf(name));

	return [ '$scope', '$stateParams', 'observableStateParams', ...injectables, ( $scope, $stateParams, observableStateParams, ...injected ) => {

		let resources = indexes.map(index => injected[index]);

		assign(
			$scope,
			$stateParams,
			observableStateParams,
			mapKeys(injected, (obj, index) => injectables[index])
		);

		$scope.$on('$destroy', ( ) => {

			resources.forEach( ( resource ) => {
				resource.destroy();
			});

		});

	}];

};

export default
	angular.module('Zaaksysteem.intern.case.route',
		[
			angularUiRouterModule,
			ocLazyLoadModule,
			resourceModule,
			composedReducerModule,
			observableStateParamsModule,
			shouldReloadModule,
			auxiliaryRouteModule,
			onRouteActivateModule,
			zsModalModule,
			snackbarServiceModule
		]
	)
		.config([ '$stateProvider', '$urlMatcherFactoryProvider', ( $stateProvider, $urlMatcherFactoryProvider ) => {

			$urlMatcherFactoryProvider.strictMode(false);

			$stateProvider
				.state('case', {
					url: '/zaak/{caseId:int}',
					resolve: {
						timeout: [ '$timeout', ( $timeout ) => {
							return $timeout(angular.noop, 50);
						}],
						case: [ '$rootScope', '$q', '$state', '$stateParams', 'snackbarService', 'resource', ( $rootScope, $q, $state, $stateParams, snackbarService, resource ) => {

							let caseResource =
								resource({
									url: `/api/case/${$stateParams.caseId}`
								},
								{ scope: $rootScope })
									.reduce( ( requestOptions, data ) => {

										let caseObj = first(data);

										// reducers are called with error data,
										// gotta check if it's an error first
										if (!caseObj || !caseObj.id) {
											return null;
										}

										return convertv0Case(caseObj);

										
									});

							return caseResource.asPromise()
								.then(( ) => caseResource)
								.catch( ( response ) => {

									let type = get(first(response.data.result), 'type'),
										msg;

									if (type === 'api/case/find_case') {
										msg = 'De zaak kon niet worden gevonden. Mogelijk is de zaak verwijderd.';
									} else if (type === 'api/case/authorization') {
										msg = 'U heeft niet voldoende rechten om deze zaak te bekijken.';
									} else {
										msg = 'Er ging iets mis bij het laden van de zaak. Neem contact op met uw beheerder voor meer informatie';
									}

									snackbarService.error(msg, {
										actions: [
											{
												type: 'link',
												label: 'Terug naar dashboard',
												link: $state.href('home')
											}
										]
									});

									return $q.reject(response);

								});

						}],
						casetype: [ '$rootScope', '$q', '$state', '$stateParams', 'resource', 'case', 'snackbarService', ( $rootScope, $q, $state, $stateParams, resource, caseResource, snackbarService ) => {

							let reference = caseResource.data().instance.casetype.reference,
								version = caseResource.data().instance.casetype.instance.version,
								casetypeResource = resource({
								url: `/api/v1/casetype/${reference}`,
								params: {
									version
								}
							}, { scope: $rootScope })
								.reduce(( requestOptions, data) => first(data));

							return casetypeResource.asPromise()
								.then(( ) => casetypeResource)
								.catch( ( response ) => {

									snackbarService.error(
										'Het zaaktype kan niet worden geladen.',
										{
											actions: [
												{
													type: 'link',
													label: 'Terug naar dashboard',
													link: $state.href('home')
												}
											]
										}
									);

									return $q.reject(response);

								});

						}],
						jobs: [ '$rootScope', 'resource', 'case', ( $rootScope, resource, caseResource ) => {

							let jobsResource = resource(
									( ) => {
										let reference = get(caseResource.data(), 'reference');

										return reference ?
											{
												url: `/api/v1/case/${reference}/queue`,
												params: {
													paging: 100
												}
											}
											: null;
									},
									{
										scope: $rootScope
									}
								)
									.reduce( ( requestOptions, data ) => {

										return (data || []).filter(
											job => (job.instance.status !== 'finished' && job.instance.status !== 'failed')
										);

									});

							return jobsResource.asPromise()
								.then( ( ) => {
									return jobsResource;
								})
								.catch( ( ) => {
									// not important enough to bug the user w/ a snack
								});


						}],
						module: [ '$rootScope', '$ocLazyLoad', '$q', ( $rootScope, $ocLazyLoad, $q ) => {

							return $q(( resolve/*, reject*/ ) => {

								require([ './', './controller' ], ( ...names ) => {

									$rootScope.$evalAsync( ( ) => {
										$ocLazyLoad.load(names.map(name => {
											return { name };
										}));

										resolve();
									});

								});

							});
						}],
						rules: [ '$rootScope', '$q', '$stateParams', 'resource', ( $rootScope, $q, $stateParams, resource ) => {

							let ruleResource;

							ruleResource = resource(
								{ url: '/api/rules', params: { case_id: $stateParams.caseId, zapi_no_pager: 1 } },
								{
									scope: $rootScope
								}
							);

							return ruleResource.asPromise()
								.then( ( ) => ruleResource);

						}]
					},
					template,
					controller: 'CaseController',
					controllerAs: 'vm',
					onActivate: [ '$state', '$location', 'auxiliaryRouteService', 'case', ( $state, $location, auxiliaryRouteService, caseResource ) => {

						let names = auxiliaryRouteService.parseNames($state.current.name, $location.url()),
							state = auxiliaryRouteService.state('case.phase', $state.current.name, $location.url()),
							params = { phaseName: normalizePhaseName(get(caseResource.data(), 'instance.phase') || '') };

						if (!params.phaseName) {
							params.phaseName = 'afhandelen';
						}

						if (names.base === 'case') {
							$state.go(state, params, { location: false });
						}

					}],
					title: [ 'case', ( caseResource ) => {

						let caseObj = caseResource.data(),
							caseId = caseObj.instance.number,
							casetypeName = caseObj.instance.casetype.instance.name,
							title;

						title = `Zaak ${caseId}: ${casetypeName}`;

						return title;
					}],
					settings:
						'<zs-case-settings case-resource="viewController.getCaseResource()" casetype-resource="viewController.getCasetypeResource()" user="viewController.getUser()"></zs-case-settings>',
					actions: [
						{
							name: 'upload',
							label: 'Upload document',
							iconClass: 'upload',
							template:
								`<zs-case-file-upload
									on-batch-start="close($promise)"
									case-id="viewController.getCaseNumber()"
								></zs-case-file-upload>`,
							when: [ 'viewController', ( viewController ) => {

								return viewController.canUpload();
							}]
						},
						{
							name: 'sjabloon',
							label: 'Sjabloon gebruiken',
							iconClass: 'file',
							template:
								`<zs-case-template-generate
									on-close="close(viewController.createTemplate($values))"
									case-id="viewController.getCaseNumber()"
									data-templates="viewController.getCaseTemplates()"
									case-documents="viewController.getCaseDocuments()"
									data-requestor="viewController.getRequestor()"
								></zs-case-template-generate>`,
							when: [ 'viewController', ( viewController ) => !viewController.isResolved() && viewController.canEdit() && get(viewController.getCaseTemplates(), 'length', 0) > 0 ]
						},
						{
							name: 'betrokkene',
							label: 'Voeg betrokkene toe',
							iconClass: 'account',
							template:
								`<zs-case-add-subject
									on-subject-add="close($promise)"
									case-id="viewController.getCaseNumber()"
								>
								</zs-case-add-subject>`,
							when: [ 'viewController', ( viewController ) => viewController.canEdit() && !viewController.isResolved() && !!viewController.getRequestor() ]
						},
						{
							name: 'email',
							label: 'Verstuur e-mail',
							iconClass: 'email',
							template:
								`<zs-case-send-email
									case-id="viewController.getCaseNumber()"
									data-templates="viewController.getEmailTemplates()"
									requestor-name="viewController.getRequestorName()"
									on-email-send="close($promise)"
								>
								</zs-case-send-email>`,
							when: [ 'viewController', ( viewController ) => viewController.canEdit() && !viewController.isResolved() && !!viewController.getRequestor() ]
						},
						{
							name: 'geplande-zaak',
							label: 'Voeg geplande zaak toe',
							iconClass: 'calendar-plus',
							template:
								`<zs-case-plan
									case-id="viewController.getCaseNumber()"
									case-uuid="viewController.getCase().reference"
									on-submit="close($promise)"
								></zs-case-plan>`,
							when: [ 'viewController', ( viewController ) => viewController.canEdit() && !viewController.isResolved() ]
						}
					]
				})
				.state('case.phase', {
					url: '/fase/:phaseName/:tab/',
					template:
						`<zs-case-phase-view
							on-phase-advance="vm.handlePhaseAdvance($taskIds)"
							phase-name="phaseName"
							data-case="case"
							data-casetype="casetype"
							data-rules="rules"
							data-tab="get('tab')"
							data-templates="vm.getCaseTemplates()"
							case-documents="vm.getCaseDocuments()"
							data-requestor="vm.getRequestor()"
							can-advance="vm.canAdvance()"
							can-edit="vm.canEdit()&&!vm.isResolved()"
							is-resolved="vm.isResolved()"
							can-self-assign="!vm.isResolved()"
							data-user="vm.getUser()"
						></zs-case-phase-view>`,
					title: [ '$stateParams', 'case', 'casetype', ( $stateParams, caseObj, casetype ) => {
						
						let phase = $stateParams.phaseName,
							isCurrent = (get(caseObj, 'values[\'case.phase\']') || '').toLowerCase() === phase,
							phaseObj = find(get(casetype, 'instance[\'phases\']', []), ( p ) => {
								return p.name.toLowerCase() === phase;
							}),
							label;

						if (phaseObj) {
							label = phaseObj.name;

							if (isCurrent) {
								label += ' - Actieve fase';
							}
						}

						return label;
					}],
					controller: assignToScope([ 'rules' ], 'case', 'casetype', 'rules'),
					params: {
						tab: { squash: true, value: null }
					},
					shouldReload: ( current, currentParams, to, toParams ) => {

						let sameName = get(current, 'name') === get(to, 'name'),
							samePhase = sameName && get(currentParams, 'phaseName') === get(toParams, 'phaseName');
						
						return !samePhase;
					}
				})
				.state('case.docs', {
					url: '/documenten/:documentId/',
					title: 'Documenten',
					template:
						`<zs-case-document-view
							case-id="vm.getCaseNumber()"
							read-only="!vm.canEdit()||vm.isResolved()"
							can-upload="vm.canUpload()"
							can-delete="vm.canDelete()"
							on-file-accept="vm.reloadCaseResource()"
							on-file-reject="vm.reloadCaseResource()"
							on-file-update="vm.reloadCaseResource()"
						></zs-case-document-view>`,
					params: {
						documentId: { squash: true, value: null }
					},
					resolve: {
						docModule:
							[ '$rootScope', '$ocLazyLoad', '$q', ( $rootScope, $ocLazyLoad, $q ) => {

								return $q(( resolve/*, reject*/ ) => {

									require([ './zsCaseDocumentView' ], ( ...names ) => {

										$rootScope.$evalAsync( ( ) => {
											$ocLazyLoad.load(names.map(name => {
												return { name };
											}));

											resolve();
										});

									});

								});
							}]
					}
				})
				.state('case.timeline', {
					url: '/timeline/:eventId/',
					title: 'Timeline',
					template:
						`<zs-case-timeline-view
							case-id="vm.getCaseNumber()"
							data-requestor="vm.getRequestor()"
							event-id="vm.getEventId()"
						></zs-case-timeline-view>`,
					params: {
						eventId: { squash: true, value: null }
					},
					resolve: {
						timelineModule:
							[ '$rootScope', '$ocLazyLoad', '$q', ( $rootScope, $ocLazyLoad, $q ) => {

								return $q(( resolve/*, reject*/ ) => {

									require([ './zsCaseTimelineView' ], ( ...names ) => {

										$rootScope.$evalAsync( ( ) => {
											$ocLazyLoad.load(names.map(name => {
												return { name };
											}));

											resolve();
										});

									});

								});
							}]
					}
				})
				.state('case.location', {
					url: '/locatie/:more/',
					title: 'Kaart',
					params: {
						more: { squash: true, value: null }
					},
					template:
						`<zs-case-map-view
							case-resource="vm.getCaseResource()"
							show-more="more()"
						></zs-case-map-view>`,
					shouldReload: ( current, currentParams, to ) => {

						let sameName = get(current, 'name') === get(to, 'name');
						
						return !sameName;
					},
					controller: [ '$scope', 'observableStateParams', ( $scope, observableStateParams ) => {

						$scope.more = ( ) => observableStateParams.get('more');


					}]
				})
				.state('case.relations', {
					url: '/relaties',
					title: 'Relaties',
					template:
						`<zs-case-relation-view
							related-cases="relationVm.relations.data().related"
							child-cases="relationVm.relations.data().children"
							parent-case="relationVm.relations.data().parent"
							sibling-cases="relationVm.relations.data().siblings"
							planned-cases="relationVm.relations.data().plannedCases"
							related-subjects="relationVm.relations.data().relatedSubjects"
							related-objects="relationVm.relations.data().relatedObjects"
							planned-emails="relationVm.relations.data().plannedEmails"
							supports-planned-emails="relationVm.supportsPlannedEmails()"
							has-relatable-object-types="relationVm.hasRelatableObjectTypes()"
							can-relate="vm.canRelate()"
							on-case-relate="relationVm.handleCaseRelate($reference)"
							on-case-unrelate="relationVm.handleCaseUnrelate($reference)"
							on-case-reorder="relationVm.handleCaseReorder($reference, $after)"
							on-planned-case-update="relationVm.handlePlannedCaseUpdate($id, $values)"
							on-planned-case-remove="relationVm.handlePlannedCaseRemove($id)"
							on-related-subject-update="relationVm.handleRelatedSubjectUpdate($id, $values)"
							on-related-subject-remove="relationVm.handleRelatedSubjectRemove($id)"
							on-object-relate="relationVm.handleObjectRelate($object)"
							on-email-reschedule="relationVm.handleEmailReschedule()"
							current-case="vm.getCase()"
							data-disabled="!vm.canEdit()||vm.isResolved()"
						>
						</zs-case-on-view>`,
					resolve: {
						emailInterfaceResource: [ '$rootScope', '$q', 'resource', ( $rootScope, $q, resource ) => {

							let res =
									resource(
										'/api/v1/sysin/interface/get_by_module_name/email',
										{ scope: $rootScope }
									);

							return res.asPromise()
								.then(( ) => res)
								.catch( err => {

									console.error(err);
									
									return $q.resolve(res);
								});

						}],
						relatedSubjectResource: [ '$rootScope', 'resource', 'snackbarService', '$stateParams', ( $rootScope, resource, snackbarService, $stateParams ) => {

							let res =
								resource(
									`/api/case/${$stateParams.caseId}/subjects`,
									{ scope: $rootScope }
								);

							return res.asPromise()
								.then(( ) => res)
								.catch(( ) => {

									snackbarService.emit('De betrokkenen konden niet worden geladen. Neem contact op met uw beheerder voor meer informatie.');

									res.destroy();

								});

						}],
						relatedObjectResource: [ '$q', '$rootScope', 'resource', 'snackbarService', '$stateParams', ( $q, $rootScope, resource, snackbarService, $stateParams ) => {

							let res =
								resource(
									( ) => {

										return {
											url: `/api/case/${$stateParams.caseId}`,
											params: {
												deep_relations: true
											}
										};

									}, { scope: $rootScope }
								)
									.reduce( ( requestOptions, data ) => {

										let relatedObjects =
												data ?
													seamlessImmutable(get(convertv0Case(first(data)), 'instance.relations.instance.rows', []))
													: [];

										return relatedObjects;

									});

							return res.asPromise()
								.then( ( ) => res)
								.catch( ( err ) => {

									snackbarService.emit('De geplande zaken konden niet worden geladen. Neem contact op met uw beheerder voor meer informatie.');

									res.destroy();

									return $q.reject(err);
								});

						}],
						plannedEmailsResource: [ '$q', '$rootScope', 'resource', 'snackbarService', '$stateParams', 'emailInterfaceResource', ( $q, $rootScope, resource, snackbarService, $stateParams, emailInterfaceResource ) => {

							let res = resource(
									( ) => {
										return emailInterfaceResource.data() && emailInterfaceResource.data().length ?
											`/zaak/${$stateParams.caseId}/scheduled_jobs`
											: null;
									},
									{ scope: $rootScope }
								);

							return res.asPromise()
								.then(( ) => res)
								.catch( err => {

									res.destroy();

									snackbarService.error('De geplande emails konden niet worden geladen. Neem contact op met uw beheerder voor meer informatie.');

									return $q.reject(err);

								});

						}],
						objectTypeResource: [ '$q', '$rootScope', 'resource', 'snackbarService', 'relatedObjectResource', ( $q, $rootScope, resource, snackbarService, relatedObjectResource ) => {

							let res =
								resource(
									( ) => {

										let objectTypes =
												uniq(
													(relatedObjectResource.data() || [])
														.map(relation => relation.type)
												);

										return objectTypes.length ?
											{
												url: '/api/object/search',
												params: {
													zql: `SELECT {} FROM type WHERE prefix in ("${objectTypes.join('","')}")`
												}
											}
											: null;

									}, { scope: $rootScope }
								);

							return res.asPromise()
								.then( ( ) => res)
								.catch( ( err ) => {

									snackbarService.emit('De objecttypes konden niet worden geladen. Neem contact op met uw beheerder voor meer informatie.');
									res.destroy();

									return $q.reject(err);
								});

						}],
						relationsResources: [ '$rootScope', '$stateParams', '$q', 'case', 'resource', 'composedReducer', 'snackbarService', ( $rootScope, $stateParams, $q, caseResource, resource, composedReducer, snackbarService ) => {

							let getReferences = ( data ) => {
								return assign(
									{
										child: [],
										parent: [],
										plain: [],
										initiator: [],
										continuation: []
									},
									mapValues(
										get(data, 'instance.case_relationships', {}),
										( value ) => {

											let set;

											set = flatten(
													get(value, 'instance.rows', [ value ])
												)
													.filter(identity);

											return set.map(row => row.reference);
										}
									)
								);

							};

							let currentResource,
								parentResource,
								childrenResource,
								resources;

							currentResource =
								resource(
									{
										url: `/api/v1/case/${caseResource.data().reference}`
									},
									{
										scope: $rootScope
									}
								)
									.reduce(( requestOptions, data ) => first(data));

							parentResource =
								resource(
									( ) => {

										let references = getReferences(currentResource.data());

										if (references.parent.length) {
											return `/api/v1/case/${references.parent[0]}`;
										}

										return null;

									},
									{
										scope: $rootScope
									}
								)
									.reduce(( requestOptions, data ) => first(data));

							childrenResource =
								resource(
									( ) => {

										let currentRefs = getReferences(currentResource.data()),
											parentRefs = parentResource.data() ? getReferences(parentResource.data()) : {},
											uuids;

										uuids =
											uniq(
													flatten(_values(currentRefs))
														.concat(parentRefs.child)
														.concat(
															[ currentResource.data(), parentResource.data() ]
																.filter(identity)
																.map(c => c.reference)
														)
														.filter(identity)
											);

										if (uuids.length) {
											return {
												url: '/api/object/search',
												params: {
													zql: `SELECT {} FROM case WHERE object.uuid IN ("${uuids.join('","')}")`,
													zapi_num_rows: 100
												}
											};
										}

										return null;
									},
									{ scope: $rootScope }
								)
									.reduce(( requestOptions, data ) => (data || []).map(convertv0Case));

							resources = [ currentResource, parentResource, childrenResource ];

							return $q.all(resources.map(res => res.asPromise()))
								.then( ( ) => {
									return $q.resolve(resources);
								})
								.catch( ( err ) => {

									snackbarService.emit('De relaties konden niet worden geladen. Neem contact op met uw beheerder voor meer informatie.');

									resources.forEach(( res ) => {

										res.destroy();

									});

									return $q.reject(err);

								});

						}],
						relations:
							[ '$rootScope', 'composedReducer', 'case', 'relationsResources', 'relatedSubjectResource', 'relatedObjectResource', 'objectTypeResource', 'plannedEmailsResource',
							( $rootScope, composedReducer, caseResource, relationsResources, relatedSubjectResource, relatedObjectResource, objectTypeResource, plannedEmailsResource ) => {

							return composedReducer({ scope: $rootScope }, caseResource, ...relationsResources, relatedSubjectResource, relatedObjectResource, objectTypeResource, plannedEmailsResource)
								.reduce( ( caseObj, currentCase, parentCase, childCases, relatedSubjects, relatedObjects, objectTypes, plannedEmails ) => {

									let cases = keyBy(
											[ currentCase ].concat(parentCase).concat(childCases).filter(identity),
											'reference'
										),
										caseRelations,
										relations;

									caseRelations =
										mapValues(
											assign(
												{
													sibling: get(parentCase, 'instance.case_relationships.child')
												},
												currentCase.instance.case_relationships
											),
											( value ) => {

												let set;

												set = flatten(
														get(value, 'instance.rows', [ value ])
													)
														.filter(identity);

												return set.map(row => cases[row.reference]);
											}
										);

									relations = {
										self: caseObj,
										parent: first(caseRelations.parent),
										siblings: uniq(caseRelations.sibling, 'reference').filter(relation => relation.reference !== currentCase.reference),
										children: caseRelations.child,
										related: currentCase.instance.relations.instance.rows.map(
											row => {

												let type =
														keys(
															pickBy(
																caseRelations,
																( relatedCases ) => !!find(relatedCases, { reference: row.reference })
															)
														)[0],
													relatedCase = cases[row.reference];

												return relatedCase ?
													relatedCase.merge({
														relation_type: type
													})
													: null;
											}
										).filter(identity),
										plannedCases: relatedObjects.filter(
											obj => obj.type === 'scheduled_job'
										),
										relatedSubjects,
										relatedObjects:
											map(
												relatedObjects.filter(obj => obj.type !== 'scheduled_job' && obj.type !== 'case'),
												( obj ) => {

													let objectType =
															find(objectTypes, ( type ) => {
																return type.values.prefix === obj.type;
															});

													return obj.merge({
														related_object_type_label: get(objectType, 'values.name', obj.related_object_type)
													});

												}
											),
										plannedEmails
									};

									return relations;

								});

						}],
						relationModule:
							[ '$rootScope', '$ocLazyLoad', '$q', ( $rootScope, $ocLazyLoad, $q ) => {

								return $q(( resolve/*, reject*/ ) => {

									require([ './zsCaseRelationView' ], ( ...names ) => {

										$rootScope.$evalAsync( ( ) => {

											$ocLazyLoad.load(
												names.map(name => {
													return { name };
												})
											)
												.catch( err => {

													console.error(err);

													return $q.reject(err);

												});

											resolve();
										});

									});

								});
							}]
						},
						controller: [
						'$scope', 'composedReducer', 'case', 'casetype', 'relationsResources', 'relations', 'relatedSubjectResource', 'relatedObjectResource', 'objectTypeResource', 'plannedEmailsResource', 'emailInterfaceResource',
						function ( $scope, composedReducer, caseResource, casetypeResource, relationsResources, relations, relatedSubjectResource, relatedObjectResource, objectTypeResource, plannedEmailsResource, emailInterfaceResource ) {

							let ctrl = this,
								resourcesToDestroy = relationsResources.concat(relatedSubjectResource, relatedObjectResource, objectTypeResource, plannedEmailsResource, emailInterfaceResource),
								hasRelatableObjectTypesReducer =
									composedReducer({ scope: $scope }, casetypeResource)
										.reduce(( casetype => {
											return getRelatableObjectTypesFromCasetype(casetype).length;
										}));

							ctrl.relations = relations;

							ctrl.currentResource = relationsResources[0];

							ctrl.handleCaseRelate = ( reference ) => {

								return ctrl.currentResource.mutate(
									'case/relation/add',
									{
										caseReference: caseResource.data().reference,
										relatedCaseReference: reference
									}
								).asPromise();
							};

							ctrl.handleCaseUnrelate = ( reference ) => {
								return ctrl.currentResource.mutate(
									'case/relation/remove',
									{
										caseReference: caseResource.data().reference,
										relatedCaseReference: reference
									}
								).asPromise();
							};

							ctrl.handleCaseReorder = ( reference, after ) => {
								return ctrl.currentResource.mutate(
									'case/relation/move',
									{
										caseReference: caseResource.data().reference,
										relatedCaseReference: reference,
										afterCaseReference: after
									}
								).asPromise();
							};

							ctrl.handlePlannedCaseUpdate = ( reference, values ) => {
								return relatedObjectResource.mutate(
									'case/relation/scheduled_job/update',
									{
										reference,
										values
									}
								).asPromise();
							};

							ctrl.handlePlannedCaseRemove = ( reference ) => {
								return relatedObjectResource.mutate(
									'case/relation/scheduled_job/remove',
									{
										reference
									}
								);
							};

							ctrl.handleRelatedSubjectUpdate = ( id, values ) => {
								return relatedSubjectResource.mutate(
									'case/relation/subject/update',
									assign({
										caseId: caseResource.data().instance.number,
										subjectId: id
									}, values)
								);
							};

							ctrl.handleRelatedSubjectRemove = ( id ) => {
								return relatedSubjectResource.mutate(
									'case/relation/subject/remove',
									{
										caseId: caseResource.data().instance.number,
										subjectId: id
									}
								);
							};

							ctrl.handleObjectRelate = ( object ) => {

								return relatedObjectResource.mutate(
									'case/relation/add/object',
									{
										caseReference: caseResource.data().reference,
										relatedObjectReference: object.id,
										relatedObjectType: object.type,
										relatedObjectLabel: object.label
									}
								).asPromise();
							};

							ctrl.handleEmailReschedule = ( ) => {

								return plannedEmailsResource.mutate(
									'case/relation/reschedule',
									{
										caseId: caseResource.data().instance.number
									}
								).asPromise();

							};

							ctrl.supportsPlannedEmails = ( ) => get(emailInterfaceResource.data(), 'length', 0) > 0;

							ctrl.hasRelatableObjectTypes = hasRelatableObjectTypesReducer.data;

							$scope.$on('$destroy', ( ) => {
								
								resourcesToDestroy.forEach(res => {
									res.destroy();
								});
							});

						}],
						controllerAs: 'relationVm'
				})
				.state('case.admin', {
					url: '/beheer/:action',
					auxiliary: true,
					resolve: {
						acls: [
							'$rootScope', '$q', 'resource', '$stateParams', 'case',
							( $rootScope, $q, resource, $stateParams, caseResource ) => {

								let res = resource(
									$stateParams.action === 'rechten' ?
										`/api/v1/case/${caseResource.data().reference}/acl`
										: null,
									{
										scope: $rootScope,
										cache: {
											disabled: true
										}
									}
								)
									.reduce(( requestOptions, data ) => {
										return data ?
											seamlessImmutable(data.asMutable().reverse())
											: data;
									});

								// immediately resolve to keep UI responsive (modal opens immediately)

								return $q.resolve(res);

							}

						],
						actions: [
							'$rootScope', '$q', '$state', '$ocLazyLoad', 'composedReducer', 'case', 'casetype', 'acls',
							( $rootScope, $q, $ocLazyLoad, $state, composedReducer, caseResource, casetypeResource ) => {

							return $q( ( resolve/*, reject*/ ) => {

								require([ '../../../shared/case/caseActions' ], ( actions ) => {

									$rootScope.$evalAsync( ( ) => {
										resolve(
											composedReducer( { scope: $rootScope }, caseResource, casetypeResource)
												.reduce(( caseObj, casetype, acls ) => {
													return actions({ caseObj, casetype, $state, acls });
												})
										);
									});

								});

							});

						}]
					},
					onExit: [ 'actions', ( actions ) => {

						actions.destroy();

					}],
					onEnter: [
					'$rootScope', '$compile', '$state', 'user', 'case', 'casetype', 'acls', 'actions', '$stateParams', 'zsModal',
					( $rootScope, $compile, $state, user, caseResource, casetypeResource, aclsResource, actions, $stateParams, zsModal ) => {
						
						let action = find(actions.data(), { name: $stateParams.action }),
							scope,
							modal,
							unregister;

						let closeModal = ( ) => {

							modal.close();
							
							scope.$destroy();

							unregister();

						};

						scope = $rootScope.$new(true);

						scope.action = action;
						scope.caseResource = caseResource;
						scope.casetypeResource = casetypeResource;
						scope.aclsResource = aclsResource;
						scope.user = user.data().instance.logged_in_user;

						scope.onSubmit = ( promise, reload, willRedirect ) => {
							modal.hide();

							promise.then(( ) => {
								modal.close();

								if (!willRedirect) {
									$state.go('^', null, { reload });
								}

							})
								.catch( ( ) => {
									modal.show();
								});
							
						};

						modal = zsModal({
							title: action.label,
							scope,
							el: $compile(`
								<zs-case-admin-view
									data-action="action"
									case-resource="caseResource"
									data-user="user"
									casetype-resource="casetypeResource"
									acl-resource="aclsResource"
									on-submit="onSubmit($promise, $reload, $willRedirect)"
								></zs-case-admin-view>`)(scope)
						});

						modal.open();

						modal.onClose( ( ) => {

							$state.go('^');

							return false;

						});

						unregister = $rootScope.$on('$stateChangeStart', closeModal);

					}]
				})
				.state('case.info', {
					url: '/informatie',
					auxiliary: true,
					onEnter: [ '$rootScope', '$compile', '$state', 'case', 'zsModal', ( $rootScope, $compile, $state, caseResource, zsModal ) => {

						let modal,
							scope,
							unregister;

						scope = $rootScope.$new(true);

						scope.caseResource = caseResource;

						let closeModal = ( ) => {

							modal.close();
							scope.$destroy();
							unregister();
						};

						modal = zsModal({
							title: 'Meer informatie over de zaak',
							el: $compile('<zs-case-about-view case-resource="caseResource"></zs-case-about-view>')(scope)
						});

						modal.open();

						modal.onClose( ( ) => {

							$state.go('^');

							return false;

						});

						unregister = $rootScope.$on('$stateChangeStart', closeModal);

					}]
				});

		}])
		.name;
