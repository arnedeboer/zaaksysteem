import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import resourceModule from './../../../shared/api/resource';
import composedReducerModule from './../../../shared/api/resource/composedReducer';
import rwdServiceModule from './../../../shared/util/rwdService';
import actionsModule from './actions';
import queueRunner from './zsCaseView/zsCasePhaseView/queueRunner';
import get from 'lodash/get';
import pickBy from 'lodash/pickBy';
import keyBy from 'lodash/keyBy';
import mapValues from 'lodash/mapValues';
import identity from 'lodash/identity';
import map from 'lodash/map';
import assign from 'lodash/assign';
import includes from 'lodash/includes';
import find from 'lodash/find';
import shortid from 'shortid';
import snackbarServiceModule from './../../../shared/ui/zsSnackbar/snackbarService';

export default
	angular.module('CaseController', [
		angularUiRouterModule,
		resourceModule,
		rwdServiceModule,
		actionsModule,
		snackbarServiceModule,
		composedReducerModule
	])
		.controller('CaseController', [ '$scope', '$http', '$q', '$timeout', '$state', 'resource', 'case', 'casetype', 'jobs', 'user', 'rwdService', 'snackbarService', 'composedReducer', 'dateFilter', function ( $scope, $http, $q, $timeout, $state, resource, caseResource, casetypeResource, jobsResource, userResource, rwdService, snackbarService, composedReducer, dateFilter ) {

			let ctrl = this,
				userCollapsed,
				collapsedReducer,
				templateResource,
				emailTemplateResource,
				rightsReducer,
				messageReducer,
				hiddenByUser = [],
				runner;

			let getCaseNumber = ( ) => get(caseResource.data(), 'instance.number');

			templateResource = resource(
				( ) => {
					return `/zaak/${getCaseNumber()}/get_sjablonen`;
				},
				{ scope: $scope }
			);

			emailTemplateResource = resource(
				( ) => {
					return `/zaak/${getCaseNumber()}/get_sjablonen?type=notifications`;
				},
				{ scope: $scope }
			);

			collapsedReducer = composedReducer({ scope: $scope }, ( ) => userCollapsed, ( ) => !rwdService.isActive('large-only'))
				.reduce( ( isUserCollapsed, collapsedDefault ) => {

					return isUserCollapsed === undefined ?
						collapsedDefault
						: userCollapsed;
				});

			rightsReducer = composedReducer( { scope: $scope }, caseResource)
				.reduce( ( caseObj ) => {
					let rights = mapValues(
						keyBy(get(caseObj, 'instance.permissions', {})),
						( value ) => !!value
					);

					return rights;
				});

			messageReducer = composedReducer({ scope: $scope }, caseResource, userResource, jobsResource, ( ) => hiddenByUser, ( ) => runner)
				.reduce(( caseObj, user, jobs, hidden, rnnr ) => {

					let unassigned = !caseObj.instance.assignee && caseObj.instance.status !== 'resolved',
						userNotRequestor =
							caseObj.instance.assignee
							&& caseObj.instance.status !== 'resolved'
							&& caseObj.instance.assignee.instance.id !== user.instance.logged_in_user.id,
						messages;

					messages =
						map(
							pickBy({
								payment_pending: get(caseObj.instance.payment_status, 'original') === 'pending',
								payment_failed: get(caseObj.instance.payment_status, 'original') === 'failed',
								unassigned,
								user_not_requestor: userNotRequestor,
								intake: caseObj.instance.assignee
									&& caseObj.instance.assignee.instance.id === get(user, 'instance.logged_in_user.id')
									&& caseObj.instance.status === 'new',
								stalled: caseObj.instance.status === 'stalled',
								resolved: caseObj.instance.status === 'resolved',
								investigated: caseObj.instance.requestor.instance.investigation === 'In onderzoek',
								deceased: caseObj.instance.requestor.instance.date_of_death,
								briefadres: !!caseObj.instance.requestor.instance.has_correspondence_address,
								moved: !!caseObj.instance.requestor.instance.status === 'Verhuisd',
								geheimhouding: !!caseObj.instance.requestor.instance.is_secret,
								jobs: !rnnr && jobs && jobs.length > 0
							}, identity),
							( value, key ) => {

								let message;

								message =
									{
										payment_pending: 'De betaling van de zaak is (nog) niet afgerond',
										payment_failed: 'De betaling van de zaak is mislukt',
										unassigned: {
											message: 'Deze zaak heeft nog geen behandelaar',
											actions: [
												{
													label: 'In behandeling nemen',
													click: ctrl.handleSelfAssign
												}
											]
										},
										user_not_requestor: 'U bent niet de behandelaar van de zaak',
										intake: {
											message: 'Deze zaak is aan u toegewezen, maar u heeft hem nog niet in behandeling genomen',
											actions: [
												{
													label: 'In behandeling nemen',
													click: ctrl.handleSelfAssign
												}
											]
										},
										stalled: `De zaak is opgeschort ${
											caseObj.instance.stalled_until ?
											`tot ${dateFilter(caseObj.instance.stalled_until, 'dd-MM-yyyy')}`
											: 'voor onbepaalde tijd'}`,
										resolved: 'Deze zaak is afgehandeld. U kunt geen wijzigingen meer aanbrengen',
										investigated: 'Aanvrager staat in onderzoek',
										deceased: 'Aanvrager is overleden',
										briefadres: 'Aanvrager heeft een briefadres',
										geheimhouding: 'Aanvrager heeft geheimhouding',
										moved: 'Aanvrager is verhuisd',
										jobs: {
											message: 'Er worden op de achtergrond nog taken uitgevoerd voor deze zaak',
											actions: [
												{
													label: 'Bekijk voortgang',
													click: ctrl.showQueueProgress
												}
											]
										}
									}[key];

								return assign(
									{ name: key, id: shortid() },
									typeof message === 'string' ?
										{ message }
										: message
								);

							}
						);

					messages = messages.filter(message => !includes(hidden, message.name));

					return messages;

				});
			ctrl.getCase = caseResource.data;

			ctrl.getCasetype = casetypeResource.data;

			ctrl.getCaseResource = ( ) => caseResource;

			ctrl.getCasetypeResource = ( ) => casetypeResource;

			ctrl.getCaseNumber = getCaseNumber;

			ctrl.getCaseTitle = ( ) => get(ctrl.getCasetype(), 'name');

			ctrl.getCaseLink = ( ) => $state.href('case');

			ctrl.isSidebarCollapsed = collapsedReducer.data;

			ctrl.handleSidebarToggleClick = ( ) => {
				userCollapsed = !ctrl.isSidebarCollapsed();
			};

			ctrl.getCaseDocuments = ( ) => get(ctrl.getCase(), 'instance.case_documents');

			ctrl.getCaseTemplates = templateResource.data;

			ctrl.getEmailTemplates = emailTemplateResource.data;

			ctrl.handleConfidentialityChange = ( confidentiality ) => {
				
				caseResource.mutate(
					'CASE_CONFIDENTIALITY_CHANGE',
					{
						caseId: ctrl.getCaseNumber(),
						confidentiality
					}
				);
			};

			ctrl.getRequestor = ( ) => get(ctrl.getCase(), 'instance.requestor');
			ctrl.getAssignee = ( ) => get(ctrl.getCase(), 'instance.assignee');
			ctrl.getCoordinator = ( ) => get(ctrl.getCase(), 'instance.coordinator');
			ctrl.getRequestorName = ( ) => get(ctrl.getCase(), 'instance.requestor.instance.name', 'Onbekend');

			ctrl.createTemplate = ( values ) => {

				let promise,
					reloadDocs = $state.current.name.indexOf('case.docs') === 0,
					template = find(ctrl.getCaseTemplates(), tpl => {
						return tpl.bibliotheek_sjablonen_id.id === values.template;
					});

				promise = $http({
					url: '/file/file_create_sjabloon',
					method: 'POST',
					data: pickBy(
						{
							case_id: ctrl.getCaseNumber(),
							name: values.name,
							target_format: values.filetype,
							zaaktype_sjabloon_id: template.id,
							case_document_ids: values.case_document
						},
						identity
					)
				});

				snackbarService.wait('Uw document wordt aangemaakt', {
					promise,
					then: ( resp ) => {

						let type = resp.data.type,
							message,
							actions;

						switch (type) {

							case 'created':
							message = 'Uw document is aangemaakt';
							actions = reloadDocs ?
								[]
								: [
									{
										type: 'link',
										link: $state.href('case.docs', { documentId: get(resp, 'data.data.id') }, { inherit: true }),
										label: 'Document bekijken'
									}
								];
							break;

							case 'pending':
							message = 'Uw document is in de wachtrij geplaatst.<br>U krijgt een notificatie op het moment dat deze is verwerkt.';
							break;

							case 'redirect':
							message = 'Voor het genereren van het document moeten er acties uitgevoerd worden op een externe website.';
							actions = [
								{
									type: 'link',
									link: resp.data.data.document.resume_url,
									label: 'Externe website openen'
								}
							];
							break;

						}

						return {
							message,
							actions
						};
					},
					catch: ( ) => 'Uw document kon niet worden aangemaakt'
				})
					.then(( ) => {

						if (reloadDocs) {
							$state.reload();
						}

					});

				return promise;
			};

			ctrl.handleSelfAssign = ( ) => {

				let promise =
					$http({
						url: `/zaak/${ctrl.getCaseNumber()}/open`,
						method: 'POST',
						data: {
							no_redirect: 1
						}
					})
						.success( ( /*data*/ ) => {
							caseResource.reload();
						});

				snackbarService.wait('De zaak wordt in behandeling genomen', {
					promise,
					then: ( ) => 'De zaak is door u in behandeling genomen',
					catch: ( ) => {
						return 'De zaak kon niet in behandeling worden genomen. Neem contact op met uw beheerder voor meer informatie.';
					}
				});

				return promise;
			};

			ctrl.hideMessage = ( message ) => {
				hiddenByUser = hiddenByUser.concat(message.name);
			};

			ctrl.canEdit = ( ) => {
				return !!rightsReducer.data().zaak_edit;
			};

			ctrl.canManage = ( ) => {
				return !!rightsReducer.data().zaak_beheer;
			};

			ctrl.canDelete = ( ) => {
				return !!rightsReducer.data().zaak_beheer;
			};

			ctrl.canAdvance = ( ) => {
				return !!(ctrl.canEdit() && ctrl.getCoordinator() && ctrl.getAssignee());
			};

			ctrl.isResolved = ( ) => {
				return get(ctrl.getCase(), 'instance.status', 'resolved') === 'resolved';
			};

			ctrl.canUpload = ( ) => {
				return !ctrl.isResolved();
			};

			ctrl.canRelate = ( ) => {
				return !!( rightsReducer.data().zaak_beheer || (rightsReducer.data().zaak_edit && get(ctrl.getCase(), 'instance.coordinator')) );
			};

			ctrl.getUser = ( ) => userResource.data().instance.logged_in_user;

			ctrl.canSelfAssign = ( ) => !ctrl.isResolved();

			ctrl.handlePhaseAdvance = ( taskIds ) => {
				if (taskIds.length) {
					ctrl.showQueueProgress(taskIds);
				}
			};

			ctrl.showQueueProgress = ( ids ) => {

				runner = queueRunner({
						$http,
						$timeout,
						$q,
						opts: {
							url: `/api/v1/case/${caseResource.data().reference}/queue`
						},
						ids
					})
					.then( ( promises ) => {

						if (promises.length) {
							return snackbarService.wait(
								'Achtergrondtaken worden uitgevoerd',
								{
									promise: promises,
									collapse: true,
									then: ( ) => 'Alle achtergrondtaken zijn succesvol uitgevoerd',
									catch: ( ) => {
										return {
											message: 'Niet alle achtergrondtaken konden worden uitgevoerd.',
											actions: [
												{
													type: 'link',
													label: 'Ga naar timeline',
													link: $state.href('case.timeline', { caseId: ctrl.getCaseNumber() }, { reload: true })
												}
											]
										};
									}
								}
							);
						}

						return $q.all(promises);

					})
					.finally( ( ) => {
						jobsResource.reload()
							.finally(( ) => {
								runner = null;
							});
					});

			};

			ctrl.getMessages = messageReducer.data;

			$scope.$on('$destroy', ( ) => {

				[ caseResource, casetypeResource, jobsResource ].forEach(res => {
					res.destroy();
				});

			});
			
			ctrl.reloadCaseResource = caseResource.reload;

		}])
		.name;
