import angular from 'angular';
import zsSuggestionListModule from './../../../../../../shared/ui/zsSuggestionList';
import composedReducerModule from './../../../../../../shared/api/resource/composedReducer';
import attrToVorm from './../../../../../../shared/zs/vorm/attrToVorm';
import seamlessImmutable from 'seamless-immutable';
import shortid from 'shortid';
import includes from 'lodash/includes';
import uniqBy from 'lodash/uniq';
import find from 'lodash/find';
import mapValues from 'lodash/mapValues';
import isArray from 'lodash/isArray';
import first from 'lodash/head';
import pick from 'lodash/pick';
import template from './template.html';

export default
	angular.module('zsCaseAdminAttributeList', [
		zsSuggestionListModule,
		composedReducerModule
	])
		.directive('zsCaseAdminAttributeList', [ 'composedReducer', ( composedReducer ) => {

			return {
				restrict: 'E',
				template,
				require: [ 'zsCaseAdminAttributeList', 'ngModel' ],
				scope: {
					delegate: '&',
					templateData: '&',
					compiler: '&'
				},
				bindToController: true,
				controller: [ '$scope', '$element', function ( scope, element ) {

					let ctrl = this,
						fields = [],
						ngModel,
						inputDelegate = { input: element.find('input') },
						suggestionReducer = composedReducer({ scope }, ( ) => ctrl.templateData().available, ( ) => ctrl.query)
							.reduce( ( availableAttributes, query ) => {

								let lq = query.toLowerCase();

								return lq ?
									availableAttributes.filter(
										attr => {

											let isMatch =
												!attr.is_group
													&& (attr.type !== 'object' && attr.type !== 'file')
													&& (
														includes(attr.label.toLowerCase(), lq)
														|| includes(attr.magic_string, lq)
													);

											return isMatch;
										}
									)
										.map(attr => {

											return {
												id: shortid(),
												label: attr.label,
												data: {
													attribute: attr
												}
											};
										})
									: [];

							});

					let transformValues = ( type, vals ) => {

						let reduce = ( field, value ) => {

							return field[type] ?
								field[type].reduce(( current, reducer ) => {
									return reducer(current);
								}, value)
								: value;

						};

						let values = seamlessImmutable(mapValues(
								vals,
								( value, key ) => {

									let field = find(fields, { name: key }),
										val = value;

									if (!field) {
										return null;
									}

									val = isArray(val) && field.template !== 'checkbox-list' ? val.map(v => reduce(field, v)) : reduce(field, val);

									if (field.limit === 1 && field.template !== 'checkbox-list' && isArray(val)) {
										val = first(val);
									}

									return val;

								}
						));

						return values;

					};

					ctrl.query = '';

					ctrl.link = ( controllers ) => {

						[ ngModel ] = controllers;

						ngModel.$parsers.push( val => {
							return transformValues('formatters', val);
						});

						ngModel.$formatters.push(val => {
							return transformValues('parsers', val);
						});

					};

					ctrl.getFields = ( ) => fields;

					ctrl.getSuggestions = suggestionReducer.data;

					ctrl.getInputDelegate = ( ) => inputDelegate;

					ctrl.getValues = ( ) => ngModel && ngModel.$modelValue;

					ctrl.handleChange = ( name, value ) => {

						let values = ngModel.$modelValue || seamlessImmutable({});

						values = values.merge(
							transformValues('parsers', { [name]: value })
						);

						ngModel.$setViewValue(values);
					};

					ctrl.handleSuggestionSelect = ( suggestion ) => {

						let field = attrToVorm(suggestion.data.attribute),
							values = ngModel.$modelValue || seamlessImmutable({});

						fields = uniqBy(
							fields.concat(field),
							'name'
						);

						values = values.merge(
							pick(ctrl.templateData().defaults, field.name)
						);

						ngModel.$setViewValue(values);

						ctrl.query = '';

					};

				}],
				controllerAs: 'vm',
				link: ( scope, element, attrs, controllers ) => {

					controllers.shift().link(controllers);

				}
			};

		}])
		.name;
