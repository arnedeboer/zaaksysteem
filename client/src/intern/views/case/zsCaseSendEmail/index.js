import angular from 'angular';
import vormFieldsetModule from './../../../../shared/vorm/vormFieldset';
import composedReducerModule from './../../../../shared/api/resource/composedReducer';
import vormValidatorModule from './../../../../shared/vorm/util/vormValidator';
import snackbarServiceModule from './../../../../shared/ui/zsSnackbar/snackbarService';
import seamlessImmutable from 'seamless-immutable';
import email from './../forms/email';
import get from 'lodash/get';
import template from './template.html';
import './styles.scss';

export default
	angular.module('zsCaseSendEmail', [
		vormFieldsetModule,
		composedReducerModule,
		vormValidatorModule,
		snackbarServiceModule
	])
		.directive('zsCaseSendEmail', [ '$http', 'composedReducer', 'vormValidator', 'snackbarService', ( $http, composedReducer, vormValidator, snackbarService ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					caseId: '&',
					templates: '&',
					onEmailSend: '&',
					requestorName: '&'
				},
				bindToController: true,
				controller: [ '$scope', function ( scope ) {

					let ctrl = this,
						formReducer,
						fieldReducer,
						values = seamlessImmutable({}),
						validityReducer;

					formReducer = composedReducer( { scope }, ctrl.templates, ctrl.requestorName)
						.reduce( ( templates, requestorName ) => {

							if (templates && requestorName) {
								return email({
									templates: seamlessImmutable(templates).asMutable({ deep: true }),
									requestorName
								});
							}

							return null;

						});

					formReducer.onUpdate(( ) => {

						let form = formReducer.data();

						if (form) {
							values =
								values.merge(
									seamlessImmutable(form.getDefaults())
								).merge(values);
						}

					});

					fieldReducer = composedReducer( { scope }, formReducer)
						.reduce( form => {
							return form ? form.fields() : [];
						});

					validityReducer = composedReducer( { scope }, fieldReducer, ( ) => values)
						.reduce(vormValidator);

					ctrl.getValues = ( ) => values;

					ctrl.handleChange = ( name, value ) => {

						values = formReducer.data().processChange(name, value, values);

					};

					ctrl.getValidity = ( ) => get(validityReducer.data(), 'validations');

					ctrl.isDisabled = ( ) => !get(validityReducer.data(), 'valid', false);

					ctrl.getFields = fieldReducer.data;

					ctrl.handleSubmit = ( ) => {

						let data = {
								recipient_type: values.recipient_type,
								log_error: 0,
								notificatie_cc: values.recipient_cc,
								notificatie_bcc: values.recipient_bcc,
								notificatie_onderwerp: values.email_subject,
								notificatie_bericht: values.email_content,
								update: 1,
								no_redirect: 1,
								mailtype: values.template ?
									'bibliotheek_notificatie'
									: 'specific_mail'
							},
							promise;

						if (values.recipient_type === 'behandelaar') {
							data.medewerker_betrokkene_id = `betrokkene-medewerker-${get(values.recipient_medewerker, 'data.id')}`;
						} else if (values.recipient_type === 'overig') {
							data.notificatie_email = values.recipient_address;
						}

						if (!values.template) {
							data.subject = values.email_subject;
							data.body = values.email_content;
						} else {
							data.zaaktype_notificatie_id = Number(values.template);
						}

						promise = $http({
							url: `/zaak/${ctrl.caseId()}/send_email`,
							method: 'POST',
							data
						});

						snackbarService.wait('Uw e-mail wordt verstuurd.', {
							promise,
							then: ( ) => 'Uw e-mail is verstuurd.',
							catch: ( ) => 'De e-mail kon niet worden verstuurd.'
						});

						ctrl.onEmailSend( { $promise: promise });

					};

				}],
				controllerAs: 'vm'
			};

		}])
		.name;
