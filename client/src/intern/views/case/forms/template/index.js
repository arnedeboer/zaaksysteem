import angular from 'angular';
import propCheck from './../../../../../shared/util/propCheck';
import defaults from 'lodash/defaults';
import find from 'lodash/find';

export default ( data, options ) => {

	let opts = defaults(
			{},
			options,
			{
				showTemplateSelect: true,
				showCaseDocumentSelect: true
			}
		),
		capabilities = {};

	let isExternal = ( id ) => {

		let tpl = id ?
				find(data.templates, t => t.bibliotheek_sjablonen_id.id === Number(id))
				: null;

		return tpl && !!tpl.bibliotheek_sjablonen_id.interface_id;

	};

	propCheck.throw(
		propCheck.shape({
			templates: propCheck.array,
			caseDocuments: propCheck.array
		}),
		data
	);

	return {
		getDefaults: ( ) => {
			return {};
		},
		processChange: ( name, value, values ) => {
			let vals = values;

			return vals.merge({ [name]: value });
		},
		getCapabilities: ( ) => capabilities,
		fields: ( ) => {
			return [
				{
					name: 'template',
					label: 'Sjabloon',
					template: {
						inherits: 'select',
						control: ( el ) => {

							let wrapper = angular.element('<div></div>');

							wrapper.append(el);

							wrapper.append(
								angular.element(
									`<div class="template-external-warning" ng-show="vm.templateData().isExternal(delegate.value)">
										Let op! Dit sjabloon wordt gegenereerd met een externe documentgenerator.
									</div>`
								)
							);

							return wrapper;

						}
					},
					data: {
						options: (data.templates || []).map(
							tpl => {
								return {
									label: tpl.bibliotheek_sjablonen_id.naam,
									value: tpl.bibliotheek_sjablonen_id.id
								};
							}
						),
						isExternal
					},
					required: true,
					when: opts.showTemplateSelect
				},
				{
					name: 'name',
					label: 'Naam',
					template: {
						inherits: 'text',
						control: ( el ) => {

							return angular.element(
								`<div class="vorm-input-wrapper">
									${el[0].outerHTML}
									<span>{{vm.invokeData('filetype')}}</span>
								</div>`
							);

						}
					},
					data: {
						filetype: [ '$values', ( values ) => values.filetype ]
					},
					required: true
				},
				{
					name: 'filetype',
					label: 'Opslagformaat',
					template: 'radio',
					data: {
						options: [ 'odt', 'pdf' ]
							.map(extension => {
								return {
									value: extension,
									label: extension
								};
							})
					},
					required: true,
					when: [ '$values', vals => !vals.template || !isExternal(vals.template) ]
				},
				{
					name: 'case_document',
					label: 'Zaakdocument',
					template: 'select',
					data: {
						notSelectedLabel: 'Geen',
						options:
							(data.caseDocuments || []).map(
								caseDoc => {

									return {
										value: caseDoc.bibliotheek_kenmerken_id.id,
										label: caseDoc.label || caseDoc.bibliotheek_kenmerken_id.naam
									};
								}
							)
					}
				}
			];
		}
	};

};
