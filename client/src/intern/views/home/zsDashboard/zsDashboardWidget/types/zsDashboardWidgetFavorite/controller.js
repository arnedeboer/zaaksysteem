import angular from 'angular';
import immutable from 'seamless-immutable';
import sortBy from 'lodash/sortBy';
import find from 'lodash/find';

export default
	function ( resource, dragulaService, snackbarService, scope ) {

		let ctrl = this,
			favorites = resource(`/api/v1/dashboard/favourite/${ctrl.type()}`, { scope, cache: { every: 60 * 5 * 10000 } }),
			dragulaBagId = 'favorites';

		ctrl.getFavorites = favorites.data;

		ctrl.getDragulaBagId = ( ) => dragulaBagId;

		ctrl.handleObjectSuggest = ( object ) => {

			let hasObject =
				!!find(
					ctrl.getFavorites(),
					( favorite ) => favorite.reference_id === object.id
				),
				limitReached = (favorites.limit() || 10) <= favorites.data().length;

			if (hasObject) {
				snackbarService.error('Dit object staat al tussen uw favorieten.');
			} else if (limitReached) {
				snackbarService.error(`U kunt maximaal ${favorites.limit()} favorieten in uw lijst hebben.`);
			} else {

				favorites.mutate('add_favorite', {
					reference_type: ctrl.type(),
					reference_id: object.id,
					label: object.label
				});

			}

		};

		ctrl.removeFavorite = ( favorite ) => {

			favorites.mutate('remove_favorite', {
				uuid: favorite.id,
				reference_type: ctrl.type()
			});

		};

		ctrl.reorderFavorite = ( favorite, order ) => {

			favorites.mutate('update_favorite', {
				uuid: favorite.id,
				reference_id: favorite.reference_id,
				reference_type: favorite.reference_type,
				order
			});

		};

		ctrl.canMove = ( el, container, handle) => handle.classList.contains('widget-favorite-drag-handle');

		ctrl.getObjectType = ctrl.type;

		ctrl.favorites = favorites;

		dragulaService.options(scope, ctrl.getDragulaBagId(), {
			moves: ctrl.canMove
		});

		favorites.reduce( ( requestOptions, data ) => {
			let result =
				(data || [])
					.map( ( item ) => {
					return immutable({
						id: item.reference
					}).merge(item.instance);
				});

			result = sortBy(result, 'order');

			return immutable(result);
		});

		scope.$on(`${dragulaBagId}.drop`, ( event, draggedElement ) => {
			
			scope.$apply( ( ) => {
				let before = draggedElement[0].nextElementSibling,
					order = 0;

				if (before && before.nodeName !== '#comment') {
					order = angular.element(before).scope().favorite.order - 5;
				} else {
					order = Math.max(...ctrl.getFavorites().map( favorite => favorite.order)) + 5;
				}

				ctrl.reorderFavorite(draggedElement.scope().favorite, order);
			});

		});

	}
