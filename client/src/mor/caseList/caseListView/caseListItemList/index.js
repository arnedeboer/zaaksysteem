import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import template from './template.html';
import composedReducerModule from '../../../../shared/api/resource/composedReducer';
import caseListItemListItemModule from './caseListItemListItem';
import configServiceModule from '../../../shared/configService';
import sessionServiceModule from '../../../../shared/user/sessionService';
import auxiliaryRouteModule from '../../../../shared/util/route/auxiliaryRoute';
import shortid from 'shortid';
import find from 'lodash/find';
import get from 'lodash/get';
import assign from 'lodash/assign';
import trim from 'lodash/trim';
import isString from 'lodash/isString';
import isArray from 'lodash/isArray';
import isObject from 'lodash/isObject';
import first from 'lodash/first';
import omit from 'lodash/omit';
import isNull from 'lodash/isNull';
import appServiceModule from '../../../shared/appService';
import './styles.scss';

export default
		angular.module('Zaaksysteem.mor.caseListItemList', [
			composedReducerModule,
			caseListItemListItemModule,
			configServiceModule,
			sessionServiceModule,
			auxiliaryRouteModule,
			angularUiRouterModule,
			appServiceModule
		])
		.directive('caseListItemList', [ '$sce', '$state', 'configService', 'composedReducer', 'auxiliaryRouteService', ( $sce, $state, configService, composedReducer, auxiliaryRouteService ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					caseGroups: '&',
					onLoadMore: '&'
				},
				bindToController: true,
				controller: [ '$scope', function ( $scope ) {

					let ctrl = this,
						groupReducer;

					let getDetailState = ( ) => {
						return auxiliaryRouteService.append($state.current, 'caseDetail');
					};

					groupReducer = composedReducer({ scope: $scope }, ctrl.caseGroups, configService.getCaseSpecificAttributes(), getDetailState )
						.reduce(( groups, attributes, detailState ) => {
							return groups
								.asMutable({ deep: true })
								.map(
									group => {

										let cases = group.cases.map( ( caseItem ) => {

											let caseSpecificAttributes =
												find(attributes, ( attribute ) => {
													return attribute.casetype_reference === caseItem.instance.casetype.reference;
												}),
												locationTemplate,
												label,
												dateCurrent = new Date(),
												dateTarget = new Date(get(caseItem, 'instance.date_target')),
												days = Math.floor( (dateTarget.getTime() - dateCurrent.getTime() ) / (24 * 60 * 60 * 1000)),
												locationValue;

											if (caseSpecificAttributes.subject_attribute !== null) {
												label = caseItem.instance.attributes[caseSpecificAttributes.subject_attribute.object.column_name.replace('attribute.', '')] || trim(caseItem.instance.subject_external) || caseItem.instance.casetype.instance.name;
											} else {
												label = trim(caseItem.instance.subject_external) || caseItem.instance.casetype.instance.name;
											}

											locationValue = get(caseItem, 'instance.case_location.nummeraanduiding') || get(caseItem, 'instance.case_location.openbareruimte');

											if ( isObject(locationValue) && locationValue.hasOwnProperty('address_data')) {
												locationValue = omit(get(locationValue, 'address_data'), isNull);
												locationTemplate = `${get(locationValue, 'straat')} ${get(locationValue, 'huisnummer', '')} ${get(locationValue, 'huisletter', '')} ${get(locationValue, 'huisnummertoevoeging', '')}`;
											} else if ( isString(locationValue)) {
												locationTemplate = locationValue || '';
											}

											if (isArray(label)) {
												label = first(label);
											}

											return {
												$id: shortid(),
												number: caseItem.instance.number,
												label,
												location: locationTemplate,
												days,
												link: $state.href(detailState, { caseID:  caseItem.instance.number }),
												style: {
													urgentie_high: days < 0,
													urgentie_medium: days >= 0 && days < 5,
													urgentie_low: days >= 5
												}
											};

										});

										return assign({}, group, {
											cases,
											empty: assign({}, group.empty, {
												message: $sce.trustAsHtml(group.empty.message)
											})
										});

									}
								);

						});

					ctrl.getGroups = groupReducer.data;

				}],
				controllerAs: 'vm'
			};

		}
		])
		.name;
