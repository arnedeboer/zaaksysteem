import angular from 'angular';
import 'angular-mocks';
import proposalDetailViewModule from '.';
import configData from '../../shared/mock/config.json';
import documentsData from '../../shared/mock/documents.json';
import proposalData from '../../shared/mock/proposal.json';
import cacherModule from '../../../shared/api/cacher';
import configServiceModule from '../../shared/configService';
import resourceModule from '../../../shared/api/resource';

describe('proposalDetailView', ( ) => {

	let proposalDetailView,
		$httpBackend,
		apiCacher,
		el;

	beforeEach(angular.mock.module(proposalDetailViewModule, cacherModule, configServiceModule, resourceModule, [ ( ) => { }]));

	beforeEach(angular.mock.inject([ '$httpBackend', 'apiCacher', ( ...rest ) => {

		[ $httpBackend, apiCacher ] = rest;

		$httpBackend.expectGET(/\/api\/v1\/app\/bbvapp/)
			.respond([ configData ]);

		apiCacher.clear();

	}]));

	beforeEach(angular.mock.inject([ '$rootScope', '$compile', '$document', 'configService', ( ...rest ) => {

		let $rootScope, $compile, $document;

		[ $rootScope, $compile, $document ] = rest;

		let scope = $rootScope.$new();

		$httpBackend.flush();

		scope.proposal = proposalData;

		scope.documents = documentsData;

		el = angular.element(`<proposal-detail-view
			data-proposal=proposal
			data-documents=documents
		/>`);

		$document.find('body').append(el);

		$compile(el)(scope);

		proposalDetailView = el.controller('proposalDetailView');

	}]));


	it('should have a controller', ( ) => {

		expect(proposalDetailView).toBeDefined();

	});

	it('should have a function to return fields', ( ) => {

		expect( proposalDetailView.getFields ).toBeDefined();

	});

	it('should return relevant fields', ( ) => {

		expect( proposalDetailView.getFields().length ).toBeGreaterThan(0);

	});

	it('should have a function to return attachments to a proposal', ( ) => {

		expect( proposalDetailView.getAttachments ).toBeDefined();

	});

	it('should return attachments when attachments are provided', ( ) => {

		expect( proposalDetailView.getAttachments().length ).not.toEqual(0);

	});

	it('should have a function that returns a download link provided with the document uuid', ( ) => {

		expect( proposalDetailView.getFileUrl ).toBeDefined();

	});

	it('should have a function that returns a download link provided with the document uuid', ( ) => {

		expect( proposalDetailView.getFileUrl('f0cdc823-f80f-47ef-b40b-6a40bf19ed90') ).toEqual('/api/v1/case/f02e22d4-00ed-4903-943f-a6935458ac8c/document/f0cdc823-f80f-47ef-b40b-6a40bf19ed90/download');

	});

	it('should have a function that returns a style object', ( ) => {

		expect( proposalDetailView.getStyle ).toBeDefined();

	});

	it('should have a function that returns a style object', ( ) => {

		expect( proposalDetailView.getStyle() ).toEqual(jasmine.objectContaining({
			'background-color': configData.instance.interface_config.header_bgcolor
		}));

	});

	it('should have a function that returns the title', ( ) => {

		expect( proposalDetailView.getTitle ).toBeDefined();

	});

	it('should have a function that returns the title, given data', ( ) => {

		expect( proposalDetailView.getTitle() ).not.toEqual('');

	});

	it('should have a function that goes back to a previous state', ( ) => {

		expect( proposalDetailView.goBack ).toBeDefined();

	});

});
