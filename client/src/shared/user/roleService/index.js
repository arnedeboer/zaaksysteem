import angular from 'angular';
import resourceModule from './../../api/resource';
import composedReducerModule from './../../api/resource/composedReducer';
import first from 'lodash/head';
import get from 'lodash/get';
import assign from 'lodash/assign';
import find from 'lodash/find';

export default
	angular.module('roleService', [
		resourceModule,
		composedReducerModule
	])
		.factory('roleService', [ '$rootScope', 'resource', 'composedReducer', ( $rootScope, resource, composedReducer ) => {

			let roleResource;

			let createResource = ( scope, options ) => {

				return resource(
					{
						url: '/api/authorization/org_unit',
						params: {
							zapi_no_pager: 1
						}
					},
					assign({ scope }, options)
				);

			};

			roleResource = createResource($rootScope);

			return {
				createResource,
				defaults: composedReducer({ scope: $rootScope }, roleResource)
					.reduce(( units ) => {
						let unit = first(units),
							roles = get(unit, 'roles'),
							role = find(roles, { name: 'Behandelaar' })
								|| first(roles);

						return {
							unit: get(unit, 'org_unit_id'),
							role: get(role, 'role_id')
						};
					})
					.data
			};

		}])
		.name;
