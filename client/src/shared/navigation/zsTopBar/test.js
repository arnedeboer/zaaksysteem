import angular from 'angular';
import 'angular-mocks';
import zsTopBar from './index.js';

describe('zsTopBar', ( ) => {

	let $rootScope,
		$compile,
		$httpBackend,
		scope,
		topBarEl,
		ctrl;

	beforeEach(angular.mock.module([ '$provide', ( $provide ) => {

		$provide.constant('HOME', '/');

	}]));

	beforeEach(angular.mock.module(zsTopBar));

	beforeEach(angular.mock.inject([ '$rootScope', '$compile', '$httpBackend', ( ...rest ) => {
		
		[ $rootScope, $compile, $httpBackend ] = rest;

		$httpBackend.whenGET(/.*/)
			.respond(200, []);

		topBarEl = angular.element('<zs-top-bar/>');
		
		scope = $rootScope.$new();

		$compile(topBarEl)(scope);

		ctrl = topBarEl.controller('zsTopBar');

	}]));

	afterEach( ( ) => {

		$httpBackend.flush();

	});

	it('should have a controller', () => {

		expect(ctrl).toBeDefined();

	});

});
