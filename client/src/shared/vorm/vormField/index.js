import angular from 'angular';
import vormTemplateServiceModule from './../vormTemplateService';
import vormInvokeModule from './../vormInvoke';
import composedReducerModule from './../../api/resource/composedReducer';
import VormField from './VormField';

export default
	angular.module('vormField', [
		vormTemplateServiceModule,
		vormInvokeModule,
		composedReducerModule
	])
		.directive('vormField', [ ( ) => {

			return {
				restrict: 'E',
				scope: {
					onChange: '&',
					value: '&',
					template: '&',
					compiler: '&',
					limit: '&',
					valid: '&',
					required: '&',
					disabled: '&',
					label: '@',
					description: '@',
					addLabel: '@',
					editMode: '&',
					templateData: '&',
					modelOptions: '&',
					locals: '&',
					autofocus: '&',
					hideLabel: '&'
				},
				bindToController: true,
				controller: VormField,
				controllerAs: 'vm'
			};

		}])
		.name;
