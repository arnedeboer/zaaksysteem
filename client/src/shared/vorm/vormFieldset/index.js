import angular from 'angular';
import vormFieldModule from './../vormField';
import vormInvokeModule from './../vormInvoke';
import composedReducerModule from './../../api/resource/composedReducer';
import template from './template.html';
import find from 'lodash/find';
import assign from 'lodash/assign';
import createReducerFromFunction from './../../api/resource/composedReducer/createReducerFromFunction';

export default
	angular.module('vormFieldset', [
		vormFieldModule,
		vormInvokeModule,
		composedReducerModule
	])
		.directive('vormFieldset', [ '$sce', 'vormInvoke', 'composedReducer', ( $sce, vormInvoke, composedReducer ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					label: '@',
					description: '@',
					fields: '&',
					onChange: '&',
					values: '&',
					valid: '&',
					modelOptions: '&',
					locals: '&',
					compiler: '&',
					hideLabel: '&'
				},
				bindToController: true,
				controller: [ '$scope', function ( scope ) {

					let ctrl = this,
						description,
						fieldReducer,
						localsReducer,
						optionsReducer;

					localsReducer = composedReducer( { scope }, ctrl.values, ctrl.locals)
						.reduce( ( values, locals ) => {
							return assign({ $values: values }, locals);
						});

					fieldReducer = composedReducer( { scope }, localsReducer, ctrl.fields)
						.reduce( ( locals, fields = [] ) => {

							let visible =
									fields.filter(
										field => field.when === undefined || field.when === null || !!vormInvoke(field.when, locals)
									);

							return visible;
						});

					optionsReducer = composedReducer( { scope }, createReducerFromFunction(scope, ctrl.modelOptions, { objectEquality: true }))
						.reduce(( preferredOptions ) => {
							let opts =
									preferredOptions
									|| {};

							return opts;
						});

					ctrl.getDescription = ( ) => description;

					ctrl.getLocals = ( ) => localsReducer.value();

					ctrl.invoke = ( invokable ) => {
						return vormInvoke(invokable, ctrl.getLocals());
					};

					ctrl.handleChange = ( name, value ) => {

						let field = find(ctrl.fields(), { name });

						if (field.onChange) {
							vormInvoke(field.onChange, { $value: value });
						}

						ctrl.onChange({
							$name: name,
							$value: value
						});
					};

					ctrl.getModelOptions = optionsReducer.data;

					ctrl.getFields = fieldReducer.data;

					scope.$watch(( ) => ctrl.description, ( desc ) => {
						description = $sce.trustAsHtml(desc);
					});

				}],
				controllerAs: 'vm'
			};

		}])
		.name;
