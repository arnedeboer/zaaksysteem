import angular from 'angular';
import vormTemplateServiceModule from './../../vormTemplateService';
import vormInvokeModule from './../../vormInvoke';
import template from './template.html';
 
export default
	angular.module('vorm.types.radio', [
			vormTemplateServiceModule,
			vormInvokeModule
		])
			.directive('vormRadioGroup', [ 'vormInvoke', ( vormInvoke ) => {

				return {
					template,
					restrict: 'E',
					require: [ 'vormRadioGroup', 'ngModel' ],
					scope: {
						templateData: '&',
						inputId: '&'
					},
					bindToController: true,
					controller: [ function ( ) {

						let ctrl = this,
							ngModel;

						ctrl.link = ( controllers ) => {
							[ ngModel ] = controllers;

							ctrl.value = ngModel.$modelValue;

							ngModel.$formatters.push( val => {

								ctrl.value = val;
								return val;
								
							});

						};

						ctrl.handleChange = ( ) => {
							ngModel.$setViewValue(ctrl.value, 'click');
						};

						ctrl.getOptions = ( ) => {
							let options = vormInvoke(ctrl.templateData().options);

							return options;
						};

					}],
					controllerAs: 'vm',
					link: ( scope, element, attrs, controllers ) => {

						controllers.shift().link(controllers);

					}
				};

			}])
			.run([ 'vormTemplateService', function ( vormTemplateService ) {
				
				const el = angular.element(
					'<vorm-radio-group ng-model></vorm-radio-group>'
				);
				
				vormTemplateService.registerType('radio', {
					control: el
				});
				
			}])
			.name;
