import angular from 'angular';
import template from './template.html';
import get from 'lodash/get';
import zsIconModule from './../../../../ui/zsIcon';

export default
	angular.module('vormFileDisplay', [
		zsIconModule
	])
		.directive('vormFileDisplay', [ ( ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					formatter: '&',
					file: '&'
				},
				bindToController: true,
				controller: [ function ( ) {

					let ctrl = this;

					ctrl.getFilename = ( ) => {

						let file = ctrl.file(),
							formatter = ctrl.formatter();

						return formatter ?
							formatter(file) || ''
							: get(file, 'filename') || '';
					};

				}],
				controllerAs: 'vm'
			};

		}])
		.name;
