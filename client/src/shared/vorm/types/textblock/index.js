import angular from 'angular';
import vormTemplateServiceModule from './../../vormTemplateService';
import zsTruncateHtmlModule from './../../../ui/zsTruncate/zsTruncateHtml';
 
export default
	angular.module('vorm.types.textblock', [
			vormTemplateServiceModule,
			zsTruncateHtmlModule
		])
			.run([ 'vormTemplateService', function ( vormTemplateService ) {

				vormTemplateService.registerType('text_block', {
					control:
						angular.element(
							'<zs-truncate-html ng-model data-value="delegate.value"></zs-truncate-html>'
						),
					display:
						angular.element(
							'<zs-truncate-html data-value="delegate.value"></zs-truncate-html>'
						)
				});
				
			}])
			.name;
