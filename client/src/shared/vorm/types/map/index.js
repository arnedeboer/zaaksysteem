import angular from 'angular';
import vormTemplateServiceModule from './../../vormTemplateService';
import zsMapModule from './../../../ui/zsMap';
import zsMapSearchModule from './../../../ui/zsMap/zsMapSearch';
import snackbarServiceModule from './../../../ui/zsSnackbar/snackbarService';
import resourceModule from './../../../api/resource';
import composedReducerModule from './../../../api/resource/composedReducer';
import wickedGoodXpath from 'wicked-good-xpath';
import first from 'lodash/head';
import get from 'lodash/get';
import isArray from 'lodash/isArray';
import identity from 'lodash/identity';
import './styles.scss';

export default
	angular.module('vorm.types.map', [
		vormTemplateServiceModule,
		zsMapModule,
		zsMapSearchModule,
		resourceModule,
		composedReducerModule,
		snackbarServiceModule
	])
		.directive('vormMap', [ ( ) => {

			return {
				restrict: 'E',
				scope: {
					addressType: '&',
					featureLayers: '&'
				},
				bindToController: true,
				template:
					`<zs-map-search
						on-change="vm.handleSearchChange($location, $lat, $lng)"
						data-location="vm.getLocation()"
						address-type="vm.addressType()"
					></zs-map-search>`,
				require: [ 'vormMap', 'ngModel' ],
				controller: [ '$scope', function ( ) {

					let ctrl = this,
						ngModel;

					ctrl.link = ( ...rest ) => {
						[ ngModel ] = rest;
					};

					ctrl.handleSearchChange = ( location ) => {
						ngModel.$setViewValue(location, 'click');
					};

					ctrl.getLocation = ( ) => {
						return ngModel ? ngModel.$modelValue : null;
					};

				}],
				controllerAs: 'vm',
				link: ( scope, element, attrs, controllers ) => {

					controllers.shift().link(...controllers);
					
				}
			};

		}])
		.directive('vormMapComponent', [
				'$http', '$q', '$window', '$document', 'resource', 'snackbarService', 'composedReducer',
				( $http, $q, $window, $document, resource, snackbarService, composedReducer ) => {

			return {
				restrict: 'E',
				scope: {
					values: '&',
					onChange: '&',
					disabled: '&',
					addressType: '&',
					featureLayers: '&',
					onFeaturesSelect: '&'
				},
				template:
					`<button class="map-expand-button btn btn-secondary" type="button" ng-click="vm.handleMapButtonClick()">
						<span class="btn-inner">
							<span>{{vm.isMapVisible() ? 'Verberg' : 'Toon'}} kaart</span>
							<zs-icon icon-type="{{ vm.isMapVisible() ? 'chevron-up' : 'chevron-down' }}"></zs-icon>
						</span>
					</button>

					<zs-map
						markers="vm.getMarkers()"
						on-map-click="vm.handleMapClick($lat, $lng, $features)"
						ng-if="vm.isMapVisible()"
						feature-layers="vm.featureLayers()"
					>
					</zs-map>`,
				bindToController: true,
				controller: [ '$scope', function ( scope ) {

					let ctrl = this,
						mapVisible = false,
						locationResource = resource(
							( ) => {

								let value = isArray(ctrl.values()) ? first(ctrl.values()) : ctrl.values(),
									opts =
										value && ctrl.addressType() !== 'coordinate' ?
											{
												url: '/plugins/maps',
												params: {
													term: value
												}
											}
											: null;

								return opts;

							}, { scope, cache: { disabled: true } }
						)
							.reduce( ( requestOptions, data ) => {

								let address = first(get(data, 'json.addresses')),
									markers;

								markers = address ?
									[
										{
											coordinates: address.coordinates,
											title: address.identification,
											description:
												`<b>Stad</b>: ${address.city}<br/>`
												+ `<b>Postcode</b>: ${address.zipcode || '-'}<br/>`
												+ `<b>Land</b>: ${address.country}<br/>`
												+ `<b>Provincie</b>: ${address.province}<br/>`

										}
									]
									: null;

								return markers;

							}),
						markerReducer = composedReducer( { scope }, locationResource, ctrl.addressType, ctrl.values)
							.reduce(( locations, addressType, values ) => {

								let markers = [],
									vals = isArray(values) ? values : [ values ];

								if (addressType === 'coordinate') {
									markers = (vals || [])
										.filter(identity)
										.map(value => {
											let coordinate = value.split(',');

											return {
												coordinates: {
													lat: coordinate[0],
													lng: coordinate[1]
												},
												title: coordinate.join(', ')
											};

										});
								} else {
									markers = locations;
								}

								return markers;

							});

					ctrl.isMapVisible = ( ) => mapVisible;

					ctrl.getMarkers = markerReducer.data;

					ctrl.handleMapButtonClick = ( ) => {
						mapVisible = !mapVisible;
					};

					ctrl.handleMapClick = ( lat, lng, features ) => {

						let promises = [];

						if (ctrl.disabled()) {
							return;
						}

						promises = promises.concat(
							ctrl.addressType() === 'coordinate' ?
								$q.resolve([ lat, lng ].join(','))
								: $http({
										url: '/plugins/maps',
										params: {
											term: `${lat} ${lng}`
										}
									})
									.then( response => {

										let data = response.data.json,
											address = first(data.addresses);

										if (!data.success || !address) {
											return $q.reject(response);
										}

										return $q.resolve(address.identification);

									})
						);


						// get names of clicked features

						if (features && features.length) {
							promises = promises.concat(
								features.map(
									feature => $http({ url: feature.url, method: 'GET', withCredentials: false, headers: { 'X-Client-Type': undefined } }).then( data => {

										let parser = new DOMParser(),
											xml = parser.parseFromString(data.data, 'text/xml'),
											path = feature.layer.instance.feature_info_xpath,
											featureName;

										// IE11 doesn't support XPath query, so we use a helper library
										if (!document.evaluate) {
											wickedGoodXpath.install();
										}

										try {
											featureName = xml.evaluate(path, xml, null, XPathResult.ANY_TYPE, null).iterateNext().value;
										} catch ( error ) {
											console.warn(`xpath ${path} did not return a value, resolving value as empty`);
											return $q.resolve(null);
										}

										return $q.resolve(featureName);
									})
								)
							);
						}

						snackbarService.wait('Bezig met ophalen van locatie', {
							promise: promises,
							catch: ( err ) => {

								console.error('Can\'t get location info', err);

								return 'Er kon geen locatie gevonden worden voor deze coordinaat';
							}
						})
							.then( ( values ) => {

								let [ value, ...ftrs ] = values;

								ctrl.onChange({
									$value: value
								});

								if (features && features.length) {
									ctrl.onFeaturesSelect({
										$features: ftrs.filter(identity)
									});
								}

							});

					};

				}],
				controllerAs: 'vm'
			};

		}])
		.run([ 'vormTemplateService', ( vormTemplateService ) => {

			vormTemplateService.registerType(
				'map',
				{
					control:
						angular.element(
							'<vorm-map address-type="vm.invokeData(\'addressType\')" feature-layers="vm.invokeData(\'featureLayers\')" ng-model></vorm-map>'
						),
					display:
						angular.element('<div class="object-list-item">{{delegate.value}}</div>'),
					wrapper: ( el ) => {

						angular.element(
							el[1]
						).append(
							`<vorm-map-component
								data-disabled="vm.disabled()"
								data-values="vm.value()"
								address-type="vm.invokeData('addressType')"
								feature-layers="vm.invokeData('featureLayers')"
								on-change="vm.onChange({ $value: $value })"
								on-features-select="vm.templateData().onFeaturesSelect($features)"
							>
							</vorm-map-component>`
						);

						return el;

					},
					defaults: {
						editMode: 'empty'
					}
				}
				
			);

		}])
		.name;
