import angular from 'angular';
import 'angular-mocks';
import mutationServiceModule from '.';
import cacherModule from './../../cacher';
import SomaEvents from 'soma-events';
import assign from 'lodash/assign';
import map from 'lodash/map';
import get from 'lodash/get';
import zsStorageModule from './../../../util/zsStorage';
import snackbarServiceModule from './../../../ui/zsSnackbar/snackbarService';

describe('mutationService', ( ) => {

	let mutationService,
		apiCacher,
		zsStorage,
		snackbarService,
		$window,
		$httpBackend,
		$timeout;

	beforeEach(angular.mock.module(mutationServiceModule, cacherModule, zsStorageModule, snackbarServiceModule));

	beforeEach(angular.mock.inject([ '$window', '$httpBackend', '$timeout', 'apiCacher', 'mutationService', 'zsStorage', 'snackbarService', ( ...rest ) => {

		[ $window, $httpBackend, $timeout, apiCacher, mutationService, zsStorage, snackbarService ] = rest;

	}]));

	afterEach( ( ) => {

		mutationService.clear();
		zsStorage.clear();
		apiCacher.clear();

	});

	describe('without cached data', ( ) => {

		let requestOptions = { url: '/foo' };

		it('should have no running mutations', ( ) => {

			expect(mutationService.list().length).toBe(0);

		});

		describe('when a mutation is added', ( ) => {

			let mutationData = { foo: 'bar' },
				type = 'foo';

			let addMutation = ( ) => {
				return mutationService.add({
					type,
					data: mutationData,
					request: requestOptions
				});
			};

			describe('without action', ( ) => {

				it('should throw', ( ) => {

					expect(addMutation).toThrow();

				});

			});

			describe('with action', ( ) => {

				let mutation,
					mutateRequestOptions = { url: '/bar' },
					reduce,
					request,
					success,
					error,
					data = [ 1, 2, 3 ],
					reduced = [ 1, 2, 3, 4 ],
					onAdd,
					onRemove;

				let flushWithSuccess = ( ) => {

					$httpBackend.expectPOST(mutateRequestOptions.url)
						.respond(200, data);

					$timeout.flush();

					$httpBackend.flush(1);
				};

				let flushWithError = ( ) => {

					$httpBackend.expectPOST(mutateRequestOptions.url)
						.respond(400, mutationData);

					$timeout.flush();

					$httpBackend.flush(1);

				};

				let registerAction = ( options ) => {

					return mutationService.register({
						type,
						reduce,
						request,
						success,
						error,
						options
					});

				};

				beforeEach( ( ) => {

					reduce = jasmine.createSpy('reduce').and.returnValue(reduced);
					request = jasmine.createSpy('request').and.returnValue(mutateRequestOptions);
					success = jasmine.createSpy('success');
					error = jasmine.createSpy('error');

					onAdd = jasmine.createSpy('added');
					onRemove = jasmine.createSpy('removed');

					mutationService.onAdd.push(onAdd);
					mutationService.onRemove.push(onRemove);

				});

				describe('without options', ( ) => {

					beforeEach( ( ) => {
						registerAction();

						mutation = addMutation();
					});

					it('should return a mutation object when added', ( ) => {

						expect(mutation.id).toBeDefined();

						expect(mutation.created).toBeDefined();

					});

					it('should return a mutation object with an asPromise() method', ( ) => {

						let promise = mutation.asPromise();

						expect(promise).toBeDefined();

						expect(typeof promise.then).toBe('function');

					});

					it('should throw an error when an action is already registered', ( ) => {

						expect( ( ) => {
							mutationService.register({
								type,
								reduce,
								request
							});
						}).toThrow();

					});

					it('should return the mutation when asked', ( ) => {

						expect(mutationService.filter(requestOptions).length).toBe(1);

						expect(mutationService.filter({ url: '/bar' }).length).toBe(0);

					});

					it('should execute the mutations for the given request options', ( ) => {

						let result = mutationService.exec(requestOptions, data);

						expect(reduce).toHaveBeenCalledWith(data, mutation.data);

						expect(result).toEqual(reduced);

					});

					it('should not flush a mutation twice', ( ) => {

						flushWithSuccess();

						addMutation();

						$httpBackend.whenPOST(mutateRequestOptions.url)
							.respond(200, data);

						$timeout.flush();

						addMutation();

						$timeout.flush();

						$httpBackend.flush(2);

						$httpBackend.verifyNoOutstandingRequest();
						$httpBackend.verifyNoOutstandingExpectation();


					});

					describe('with success', ( ) => {

						let resolved;

						beforeEach( ( ) => {

							resolved = jasmine.createSpy('resolved');

							mutation.asPromise().then(resolved);

							flushWithSuccess();
						});


						it('should update the cached response for that request', ( ) => {

							expect(apiCacher.get(requestOptions).$data).toEqual(data);

						});

						it('should call the handlers', ( ) => {

							expect(request).toHaveBeenCalledWith(jasmine.objectContaining(mutationData));

							expect(success).toHaveBeenCalledWith(data);

							expect(error).not.toHaveBeenCalled();

							expect(reduce).not.toHaveBeenCalled();

						});

						it('should remove the mutation', ( ) => {

							expect(mutationService.list().length).toBe(0);

						});

						it('should call the onAdd listener', ( ) => {

							expect(onAdd).toHaveBeenCalled();

							expect(onAdd.calls.argsFor(0)[0]).toEqual([ mutation.id ]);

						});

						fit('should resolve the mutation promise', ( ) => {

							expect(resolved).toHaveBeenCalled();

						});

					});

					describe('with error', ( ) => {

						let rejected;

						beforeEach(( ) => {

							rejected = jasmine.createSpy('rejected');

							mutation.asPromise().catch(rejected);

							flushWithError();

						});

						it('should call the error handlers', ( ) => {

							expect(error).toHaveBeenCalled();

							expect(success).not.toHaveBeenCalled();

						});

						it('should remove the mutation', ( ) => {

							expect(mutationService.list().length).toBe(0);

						});

						fit('should reject the promise', ( ) => {

							expect(rejected).toHaveBeenCalled();

						});

					});

					describe('when removed', ( ) => {

						let rejected;

						beforeEach( ( ) => {

							rejected = jasmine.createSpy('rejected');

							mutation.asPromise().catch(rejected);

						});

						it('should no longer be in the list', ( ) => {

							expect(mutationService.list().length).toBe(1);

							mutationService.remove(mutation.id);

							expect(mutationService.list().length).toBe(0);

						});

						it('should call the onRemove listener', ( ) => {

							expect(onRemove).not.toHaveBeenCalled();

							mutationService.remove(mutation.id);

							expect(onRemove).toHaveBeenCalled();

							expect(onRemove.calls.argsFor(0)[0]).toEqual([ mutation.id ]);

						});

						it('should cancel running requests', ( ) => {

							$httpBackend.expectPOST(mutateRequestOptions.url)
								.respond(200, data);

							$timeout.flush();

							mutationService.remove(mutation.id);

							expect($httpBackend.flush).toThrow();

							expect(success).not.toHaveBeenCalled();
							expect(error).not.toHaveBeenCalled();

						});

						xit('should reject the promise', ( ) => {

							expect(rejected).toHaveBeenCalled();

						});

					});

				});

				describe('with reloadOnComplete is true', ( ) => {

					beforeEach(( ) => {

						registerAction({
							reloadOnComplete: true
						});

						mutation = addMutation();

					});

					it('should reload the data', ( ) => {

						let url = get(mutation.request, 'url');

						$httpBackend.expectPOST(mutateRequestOptions.url)
							.respond(200, data);

						$httpBackend.whenGET(url)
							.respond(data);

						$timeout.flush();

						$httpBackend.flush(1);

						$httpBackend.flush(1);

					});

				});

			});

			describe('with action without error handler', ( ) => {

				beforeEach( ( ) => {

					mutationService.register({
						type: 'foo',
						request: ( ) => ({ url: '/foo' }),
						reduce: ( data ) => data
					});

					$httpBackend.expectPOST('/foo')
						.respond(400);

					mutationService.add({
						type: 'foo',
						request: { url: '/foo' },
						data: { id: 'foo' }
					});

				});

				it('should add an error snack', ( ) => {

					expect(snackbarService.getSnacks().length).toBe(0);

					$timeout.flush();

					$httpBackend.flush();

					expect(snackbarService.getSnacks().length).toBe(1);

				});

			});

			describe('with action that throws on .request', ( ) => {

				let onError;

				beforeEach( ( ) => {

					onError = jasmine.createSpy('error');

					mutationService.register({
						type: 'foo',
						request: jasmine.createSpy('request').and.throwError(),
						reduce: ( data ) => data,
						error: onError
					});

					mutationService.add({
						type: 'foo',
						request: { url: '/foo' },
						data: { foo: 'bar' }
					});

				});

				it('should remove the mutation', ( ) => {

					expect(mutationService.list().length).toBe(1);

					$timeout.flush();

					expect(mutationService.list().length).toBe(0);

					expect(onError).toHaveBeenCalled();

				});

			});

			describe('when told what mutations are owned', ( ) => {

				let mutations = [];


				beforeEach( ( ) => {

					mutationService.register({
						type,
						reduce: angular.noop,
						request: angular.noop
					});

					mutations.push(addMutation());

				});

				it('should only claim the unowned mutations', ( ) => {

					let event = new SomaEvents.Event('message');

					assign(event, {
						message: {
							type: 'ownedMutationsResponse',
							data: map(mutations, 'id')
						},
						origin: $window.location.origin
					});

					$window.dispatchEvent(event);

					$timeout.flush();

				});

			});
			

		});

		
		describe('when queried for owned mutations', ( ) => {

			let message,
				event;

			let dispatch = ( ) => {
				$window.dispatchEvent(event);
			};

			beforeEach(( ) => {

				message = jasmine.createSpy('message');
				event = new SomaEvents.Event('message');

				assign(event, {
					message: {
						type: 'ownedMutationsRequest'
					},
					origin: $window.location.origin,
					source: {
						postMessage: jasmine.createSpy('postMessage')
					}
				});

				$window.addEventListener('message', message);
			});

			afterEach( ( ) => {

				$window.removeEventListener('message', message);
			});

			it('should respond', ( ) => {

				dispatch();

				let evt = message.calls.argsFor(0)[0],
					msg = evt.message;

				expect(message.calls.count()).toBe(1);

				expect(msg).toEqual(jasmine.objectContaining({
					type: 'ownedMutationsRequest'
				}));

				expect(event.source.postMessage.calls.count()).toBe(1);

			});

			describe('with mutations registered', ( ) => {

				let mutation,
					type = 'foo';

				beforeEach( ( ) => {

					mutationService.register({
						type,
						reduce: angular.noop,
						request: jasmine.createSpy('requester').and.returnValue({ url: '/bar' })
					});

					mutation = mutationService.add({
						type,
						request: {
							url: '/foo'
						}
					});

				});

				it('should return the id of the mutation', ( ) => {

					dispatch();

					let spy = event.source.postMessage,
						msg = spy.calls.argsFor(0)[0];

					expect(spy.calls.count()).toBe(1);

					expect(msg.type).toBe('ownedMutationsResponse');

					expect(msg.data).toEqual([ mutation.id ]);

					
				});

			});

		});


	});

});
