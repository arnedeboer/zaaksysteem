import angular from 'angular';
import 'angular-mocks';
import resourceModule from '.';
import apiCacherModule from '../cacher';
import mutationServiceModule from './mutationService';
import mapValues from 'lodash/mapValues';
import keyBy from 'lodash/keyBy';
import each from 'lodash/each';
import zsStorageModule from './../../util/zsStorage';

describe('resource', ( ) => {

	let $httpBackend,
		$rootScope,
		scope,
		spies,
		resourceFactory,
		mutationService,
		cacher,
		zsStorage;

	beforeEach(angular.mock.module(resourceModule, [ 'resourceProvider', ( resourceProvider ) => {

		spies =
			mapValues(
				keyBy('next prev cursor limit'.split(' ')),
				( /*key*/ ) => {
					return ( requestOptions/*, data, ...rest*/ ) => {
						return requestOptions;
					};
				}
			);

		each(spies, ( fn, key ) => {
			resourceProvider.configure[key](fn);
		});

	}]));

	beforeEach(angular.mock.module(apiCacherModule, mutationServiceModule, zsStorageModule));

	beforeEach(angular.mock.inject([ '$httpBackend', '$rootScope', 'resource', 'apiCacher', 'zsStorage', 'mutationService', ( ...rest ) => {
		
		[ $httpBackend, $rootScope, resourceFactory, cacher, zsStorage, mutationService ] = rest;

		scope = $rootScope.$new();

	}]));

	afterEach( ( ) => {

		cacher.clear();

		mutationService.clear();

		zsStorage.clear();

	});

	describe('without cache', ( ) => {

		let resource,
			requestOptions = { url: '/foo' },
			data = [ { foo: 'bar' } ];

		beforeEach(( ) => {

			resource = resourceFactory(requestOptions.url, { scope });

		});

		describe('before resolving', ( ) => {

			beforeEach( ( ) => {

				$httpBackend.whenGET('/foo')
					.respond(data);

			});

			describe('when fulfilled', ( ) => {

				it('should return a resource object', ( ) => {

					expect(resource).toBeDefined();

				});

				it('should be pending', ( ) => {

					expect(resource.state()).toBe('pending');

				});

				it('should be loading', ( ) => {

					expect(resource.loading()).toBe(true);

				});

				it('should return undefined as data', ( ) => {

					expect(resource.data()).toBeNull();

				});

				afterEach(( ) => {

					$httpBackend.flush();

				});

			});

			describe('when cancelled', ( ) => {

				it('should destroy itself along with the scope', ( ) => {

					let spy = jasmine.createSpy('destroy'),
						removeFn;

					expect(mutationService.onAdd.length).toBe(1);

					removeFn = resource.onDestroy(spy);

					scope.$destroy();

					expect(spy).toHaveBeenCalled();

					removeFn();

					expect(mutationService.onAdd.length).toBe(0);

					$httpBackend.verifyNoOutstandingExpectation();
					$httpBackend.verifyNoOutstandingRequest();

				});

			});
		});

		describe('when converting to a promise', ( ) => {

			let promise,
				resolved,
				reducedData = 'foo',
				reducedSpy = jasmine.createSpy('reduce').and.returnValue(reducedData);

			beforeEach(( ) => {

				$httpBackend.expectGET('/foo')
					.respond(data);

				promise = resource.asPromise();

				resolved = jasmine.createSpy('resolved');

				promise.then(resolved);

			});

			it('should resolve with the data from the resource ran through the reducers', ( ) => {

				resource.reduce(reducedSpy);

				$httpBackend.flush();

				expect(resolved).toHaveBeenCalled();

				expect(resolved).toHaveBeenCalledWith(reducedData);

			});
			
		});

		describe('when resolved', ( ) => {

			beforeEach(( ) => {

				$httpBackend.expectGET('/foo')
					.respond(data);

				$httpBackend.flush();

			});

			it('should be resolved', ( ) => {

				expect(resource.state()).toBe('resolved');

			});

			it('should return the data', ( ) => {

				expect(resource.data()).toEqual(data);

			});

			it('should add a mutation', ( ) => {

				let type = 'foo',
					reduce = jasmine.createSpy('reduce').and.returnValue({ foo: 'bar' }),
					request = jasmine.createSpy('request').and.returnValue({ url: '/bar' });

				mutationService.register({
					type,
					reduce,
					request
				});

				expect( ( ) => {
					resource.mutate(type, { param: 'foo' });
				}).not.toThrow();

				expect(resource.mutations().length).toBe(1);

				expect(mutationService.list().length).toBe(1);

			});

			describe('when invalidated', ( ) => {

				it('should call the update listeners', ( ) => {

					let spy = jasmine.createSpy('update');

					resource.onUpdate(spy);

					spy.calls.reset();

					resource.invalidate();

					expect(spy).toHaveBeenCalled();

				});

				it('should reset the cache', ( ) => {

					let spy = jasmine.createSpy('reduce');

					resource.reduce(spy);

					spy.calls.reset();

					resource.invalidate();

					resource.data();

					expect(spy).toHaveBeenCalled();

				});

			});

		});

		describe('when rejected', ( ) => {

			let err = 'foo';

			beforeEach( ( ) => {

				$httpBackend.expectGET('/foo')
					.respond(400, err);

				$httpBackend.flush();

			});

			it('should be rejected', ( ) => {

				expect(resource.state()).toBe('rejected');

			});

			it('should return the error', ( ) => {

				expect(resource.error()).toEqual(err);

			});


		});

		describe('when changing the request', ( ) => {

			let nwOptions = { url: '/bar' },
				nwData = { bar: 'foo' },
				onUpdate;

			beforeEach(( ) => {
				
				$httpBackend.expectGET(requestOptions.url)
					.respond(data);

				$httpBackend.flush();

				$httpBackend.expectGET(nwOptions.url)
					.respond(nwData);

				onUpdate = jasmine.createSpy('update');

				resource.onUpdate(onUpdate);

				resource.request(nwOptions);

			});

			describe('and keeping it', ( ) => {

				it('should reset the state to pending', ( ) => {

					expect(resource.state()).toBe('pending');

					$httpBackend.flush();

				});

				it('should invoke the update listeners', ( ) => {

					expect(onUpdate).toHaveBeenCalled();

					onUpdate.calls.reset();

					$httpBackend.flush();

					expect(onUpdate).toHaveBeenCalled();

				});

				it('should keep the data from the previous request in cache', ( ) => {

					expect(resource.data()).toEqual(data);

					$httpBackend.flush();

					expect(resource.data()).toEqual(nwData);

				});

			});

			
			describe('when set to null', ( ) => {

				beforeEach( ( ) => {
					resource.request(null);
				});

				it('should immediately set the data to null', ( ) => {

					expect(resource.data()).toBe(null);

				});
			
				afterEach( ( ) => {

					$httpBackend.verifyNoOutstandingExpectation();
					$httpBackend.verifyNoOutstandingRequest();

				});

			});

		});

	});

	describe('with cache', ( ) => {

		let resource,
			data = [ { foo: 'bar' } ],
			url = '/foo',
			requestOptions = { url },
			nwData = [ { bar: 'foo ' } ];

		describe('and a successful reduce', ( ) => {

			beforeEach(( ) => {

				cacher.store({ url }, data);

				resource = resourceFactory(
					requestOptions,
					{ scope: $rootScope }
				);
			});

			describe('and a successful request', ( ) => {

				beforeEach(( ) => {

					$httpBackend.expectGET(url)
						.respond(nwData);

				});

				it('should return a resource object', ( ) => {

					expect(resource).toBeDefined();

				});

				it('should return the data from the cache', ( ) => {

					expect(resource.data()).toEqual(data);

				});

				it('should invoke the update listeners', ( ) => {

					let spy = jasmine.createSpy('onUpdate');

					resource.onUpdate(spy);

					expect(spy).toHaveBeenCalled();

				});

				it('should add/remove an update listener', ( ) => {

					let spy = jasmine.createSpy('update'),
						removeFn;

					removeFn = resource.onUpdate(spy);

					cacher.store(requestOptions, { foo: 'bar' } );

					removeFn();

					spy.calls.reset();

					cacher.store(requestOptions, { bar: 'foo' });

					expect(spy).not.toHaveBeenCalled();

				});

				afterEach(( ) => {

					$httpBackend.flush();

				});

				describe('when stateless', ( ) => {

					let type = 'foo',
						reduce,
						request,
						spy;

					beforeEach( ( ) => {

						reduce = jasmine.createSpy('reduce');
						request = jasmine.createSpy('request').and.returnValue({ url: '/foo' });
						spy = jasmine.createSpy('foo');

						resource.reduce(spy);

						mutationService.register({
							type,
							reduce,
							request
						});

					});

					it('should call the spy once', ( ) => {

						resource.data();

						expect(spy).toHaveBeenCalled();

						spy.calls.reset();

						resource.data();

						expect(spy).not.toHaveBeenCalled();

					});

					it('should call the reducer when the cache is updated', ( ) => {

						resource.data();

						spy.calls.reset();

						cacher.store(requestOptions, { foo: 'bar' });

						resource.data();

						expect(spy).toHaveBeenCalled();

					});

					it('should call the reducer when a mutation is added', ( ) => {

						resource.data();

						spy.calls.reset();

						mutationService.add({
							type: 'foo',
							request: requestOptions
						});

						resource.data();

						expect(spy).toHaveBeenCalled();

						spy.calls.reset();

						resource.data();

						expect(spy).not.toHaveBeenCalled();


					});

					it('should call the reducer when a mutation is removed', ( ) => {

						let mutation;

						resource.data();

						spy.calls.reset();

						mutationService.add({
							type: 'foo',
							request: requestOptions
						});

						resource.data();

						spy.calls.reset();

						mutation = mutationService.list()[0];

						mutationService.remove(mutation.id);

						resource.data();

						expect(spy).toHaveBeenCalled();

					});

				});

			});

			describe('and a failed request', ( ) => {


				describe('with a status of 0', ( ) => {

					beforeEach( ( ) => {

						$httpBackend.expectGET(url)
							.respond(0);

						$httpBackend.flush();

					});

					it('should ignore the result of the request', ( ) => {

						expect(resource.data()).toEqual(data);

					});

					it('should keep the state resolved', ( ) => {

						expect(resource.state()).toBe('resolved');

					});

				});

				describe('with a status of other than 0', ( ) => {

					let err = { error: 'foo' };

					beforeEach( ( ) => {

						$httpBackend.expectGET(url)
							.respond(400, err);

						$httpBackend.flush();

					});

					it('should update the data of the resource with the error', ( ) => {

						expect(resource.data()).toEqual(err);

					});

					it('should set the state to rejected', ( ) => {

						expect(resource.state()).toBe('rejected');

					});

				});


			});

		});


		describe('and a reducer which throws', ( ) => {

			let createResource = ( ) => {
				resource = resourceFactory(url, { scope: $rootScope });
			};

			beforeEach( ( ) => {

				spyOn(mutationService, 'exec').and.throwError('foo');

				$httpBackend.expectGET(url)
					.respond(nwData);

			});

			it('should still fetch', ( ) => {

				createResource();

				expect($httpBackend.flush).not.toThrow();

			});

			it('should not err when created', ( ) => {

				expect(createResource).not.toThrow();

				resource.data();

				expect(mutationService.exec).toHaveBeenCalled();

				$httpBackend.flush();

			});


		});

		
	});

	describe('when request is a getter', ( ) => {

		let getOpts,
			resource;

		beforeEach( ( ) => {

			getOpts = jasmine.createSpy();

			scope = $rootScope.$new();

		});

		describe('with no initial url', ( ) => {

			beforeEach(( ) => {

				resource = resourceFactory(getOpts, { scope });

			});

			it('should change the request when the return value changes', ( ) => {

				getOpts.and.returnValue({ url: '/foo' });

				$httpBackend.expectGET('/foo')
					.respond([]);

				scope.$digest();

				$httpBackend.flush();

			});

			it('should not change the request when the return value is the same', ( ) => {

				getOpts.and.returnValue({ url: '/foo' });

				$httpBackend.expectGET('/foo')
					.respond([]);

				scope.$digest();

				$httpBackend.flush();

				getOpts.and.returnValue({ url: '/foo' });

				scope.$digest();

				$httpBackend.verifyNoOutstandingRequest();

			});
		});

		describe('with no initial url', ( ) => {

			beforeEach(( ) => {

				getOpts.and.returnValue({ url: '/foo' });

				$httpBackend.expectGET('/foo')
					.respond([]);

				resource = resourceFactory(getOpts, { scope });

			});
		

			it('should initially be pending', ( ) => {

				expect(resource.state()).toBe('pending');

				scope.$digest();

				$httpBackend.flush();

				expect(resource.state()).toBe('resolved');

			});

		});

	});
	
});
