import angular from 'angular';
import invoke from 'lodash/invokeMap';

export default
	angular.module('shared.util.appUnload', [
	])
		.factory('appUnload', [ ( ) => {

			let listeners = [];

			return {
				onUnload: ( fn ) => {
					listeners.push(fn);
				},
				triggerUnload: ( ) => {
					invoke(listeners, 'call', null);
				}
			};

		}])
		.name;
