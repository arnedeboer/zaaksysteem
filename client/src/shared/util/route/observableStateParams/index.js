import angular from 'angular';
import angularUiRouter from 'angular-ui-router';

export default
	angular.module('observableStateParams', [
		angularUiRouter,
		'ui.router.util'
	])
		.factory('observableStateParams', [ '$rootScope', '$location', '$state', ( $rootScope, $location, $state ) => {

			let getParamsFromUrl = ( ) => {
				return ($state && $state.$current && $state.$current.url.exec($location.url(), $location.search())) || {};
			};

			getParamsFromUrl();

			return {
				get: ( key ) => {

					let params = getParamsFromUrl();

					return key ? params[key] : params;
				}
			};

		}])
		.name;
