import zsDropdownMenu from './index.js';
import angular from 'angular';
import 'angular-mocks';
import soma from 'soma-events';

describe('zsDropdownMenu', ( ) => {

	let $rootScope,
		$compile,
		$timeout,
		$q,
		$document,
		scope,
		el,
		ctrl;

	beforeEach(angular.mock.module(zsDropdownMenu));

	beforeEach(angular.mock.inject(['$rootScope', '$compile', '$q', '$timeout', '$document', ( ...rest ) => {
		[ $rootScope, $compile, $q, $timeout, $document ] = rest;

		scope = $rootScope.$new();

		el = angular.element(`
			<zs-dropdown-menu
				options="options"
				on-open="handleOpen($event)"
				on-close="handleClose($event)"
			>
			</zs-dropdown-menu>
		`);

		$document.find('body').append(el);

		scope.options = [
			{
				name: 'foo',
				label: 'Foo',
				click: angular.noop
			},
			{
				name: 'bar',
				label: 'bar',
				click: angular.noop
			}
		];

		scope.handleOpen = jasmine.createSpy('open');
		scope.handleClose = jasmine.createSpy('close');

	}]));

	describe('with mode click', ( ) => {

		beforeEach(( ) => {

			$compile(el)(scope);

			ctrl = el.controller('zsDropdownMenu');

		});

		it('should have a controller', ( ) => {

			expect(ctrl).toBeDefined();

		});

		it('should start closed', ( ) => {

			expect(ctrl.isMenuOpen()).toBe(false);

		});

		it('should toggle the menu', () => {

			let isOpen = ctrl.isMenuOpen();

			ctrl.toggleMenu();

			expect(ctrl.isMenuOpen()).not.toBe(isOpen);

			ctrl.toggleMenu();

			expect(ctrl.isMenuOpen()).toBe(isOpen);

		});

		it('should open the menu', ( ) => {

			ctrl.openMenu();

			expect(ctrl.isMenuOpen()).toBe(true);

		});

		it('should close the menu', ( ) => {

			ctrl.openMenu();

			expect(ctrl.isMenuOpen()).toBe(true);

			ctrl.closeMenu();

			expect(ctrl.isMenuOpen()).toBe(false);

		});

		it('should toggle when the button is clicked', ( ) => {

			let isOpen = ctrl.isMenuOpen();

			el.find('button').triggerHandler('click');

			expect(ctrl.isMenuOpen()).not.toBe(isOpen);

		});

		it('should return the options', ( ) => {

			expect(ctrl.options()).toBe(scope.options);

		});

		it('should call the click handler of the option', ( ) => {

			let spy = jasmine.createSpy('clickHandler');

			scope.options[0].click = spy;

			scope.$digest();

			el.find('ul').find('button').eq(0).triggerHandler('click');

			expect(spy).toHaveBeenCalled();

		});

		it('should close after an option click', ( ) => {

			ctrl.openMenu();

			ctrl.handleOptionClick(scope.options[0]);

			expect(ctrl.isMenuOpen()).toBe(false);

		});

		it('should only close after a returned promise is settled', ( ) => {

			let promise = $q((resolve/*, reject*/) => {
				$timeout(resolve, 500);
			});

			ctrl.openMenu();

			scope.options[0].click = ( ) => promise;

			ctrl.handleOptionClick(scope.options[0]);

			expect(ctrl.isMenuOpen()).toBe(true);

			$timeout.flush();

			expect(ctrl.isMenuOpen()).toBe(false);

		});

		it('should close when opened and the document is clicked', ( ) => {

			let event;

			ctrl.openMenu();

			event = $document[0].createEvent('MouseEvent');

			event.initMouseEvent('click', true,	true);

			$document[0].dispatchEvent(event);

			expect(ctrl.isMenuOpen()).toBe(false);

		});

		it('should close when escape is pressed', ( ) => {

			let event = new soma.Event('keyup');

			event.keyCode = 27;

			ctrl.openMenu();

			$document[0].dispatchEvent(event);

			expect(ctrl.isMenuOpen()).toBe(false);

		});

		it('should use anchor tags if type is link', ( ) => {

			scope.options[0].type = 'link';

			expect(el.find('a').length).toBe(0);

			scope.$digest();

			expect(el.find('a').length).toBe(1);

		});

		it('should dispatch an event when the menu is opened', ( ) => {

			let event = new soma.Event('click');

			ctrl.openMenu(event);

			expect(scope.handleOpen).toHaveBeenCalled();
			expect(scope.handleOpen.calls.argsFor(0).length).toBe(1);

		});

		it('should dispatch an event when the menu is closed', ( ) => {

			let event = new soma.Event('click');

			ctrl.openMenu();

			ctrl.closeMenu(event);

			expect(scope.handleClose).toHaveBeenCalled();
			expect(scope.handleClose.calls.argsFor(0).length).toBe(1);

		});

	});

	describe('with mode hover', ( ) => {

		beforeEach(( ) => {

			el.attr('mode', '\'hover\'');

			$compile(el)(scope);

			ctrl = el.controller('zsDropdownMenu');

		});

		it('should open and close on mouseenter/leave', ( ) => {

			expect(ctrl.isMenuOpen()).toBe(false);

			el.triggerHandler('mouseenter');

			expect(ctrl.isMenuOpen()).toBe(true);

			el.triggerHandler('mouseleave');

			expect(ctrl.isMenuOpen()).toBe(false);

		});

	});

});
