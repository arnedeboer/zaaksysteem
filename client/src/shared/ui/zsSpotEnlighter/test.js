import angular from 'angular';
import 'angular-mocks';
import SomaEvents from 'soma-events';
import zsSpotEnlighter from '.';
import cacherModule from './../../api/cacher';

describe('zsSpotEnlighter', ( ) => {

	let el,
		input,
		$rootScope,
		$httpBackend,
		$compile,
		apiCacher,
		scope,
		savedSearchesService,
		ctrl;

	let compileWithResponse = ( searchResponse, objectTypeResponse = []) => {

		$httpBackend.expectGET(/\/api\/object\/search/)
			.respond(searchResponse);

		$httpBackend.expectGET(/\/objectsearch\/objecttypes/)
			.respond(objectTypeResponse);

		$compile(el)(scope);

		$httpBackend.flush();

		ctrl = el.controller('vm');

		input = el.find('input');

	};

	beforeEach(angular.mock.module(zsSpotEnlighter, cacherModule));

	beforeEach(angular.mock.inject([ '$compile', '$rootScope', '$httpBackend', 'apiCacher', 'savedSearchesService', ( ...rest ) => {

		[ $compile, $rootScope, $httpBackend, apiCacher, savedSearchesService ] = rest;

		el = angular.element(`
			<zs-universal-search
				use-location="true"
			/>
		`);

		scope = $rootScope.$new();

	}]));

	describe('without saved filters', ( ) => {

		beforeEach( ( ) => {

			compileWithResponse([]);

		});
		

		it('should have a controller', ( ) => {

			expect(ctrl).toBeDefined();

		});

		it('should return the input as the key delegate', ( ) => {

			expect(ctrl.getKeyInputDelegate().input[0]).toBe(input[0]);

		});

		describe('when focused', ( ) => {

			beforeEach( ( ) => {

				ctrl.query = 'foo';

				$httpBackend.expectGET(/\/api\/object\/search/)
					.respond([]);

				$httpBackend.expectGET(/objectsearch/)
					.respond([]);

				el.find('input').triggerHandler('focus');

				$httpBackend.flush();

			});

			it('should be open', ( ) => {

				expect(ctrl.isOpen()).toBe(true);

			});

			it('should fetch data', ( ) => {

				$httpBackend.verifyNoOutstandingExpectation();

			});

			it('should close after escape when input is focused', ( ) => {

				let event = new SomaEvents.Event('keyup', null, true);

				event.keyCode = 27;

				input[0].ownerDocument.dispatchEvent(event);

				scope.$digest();

				expect(ctrl.isOpen()).toBe(false);

			});

			it('should close on back click', ( ) => {

				ctrl.handleBackClick();

				expect(ctrl.isOpen()).toBe(false);

			});

		});

	});

	describe('with saved searches', ( ) => {

		let savedSearches =
			[
				{
					id: 'foo',
					label: 'foo',
					type: 'filter',
					data: {
						id: 'foo'
					}
				}
			];

		beforeEach( ( ) => {

			compileWithResponse(savedSearches);

		});

		describe('when opened', ( ) => {

			let suggestions =
				[
					{
						id: 'bar',
						label: 'Bar'
					}
				];


			describe('without a query', ( ) => {

				beforeEach( ( ) => {

					el.find('input').triggerHandler('focus');

				});

				it('should return the saved searches plus the predefined searches plus the all contacts link as suggestions', ( ) => {

					let numSavedSearches = savedSearchesService.getPredefinedSearches().length;

					expect(ctrl.getSuggestions().length).toBe(savedSearches.length + numSavedSearches + 1);
					expect(ctrl.getSuggestions()[0].id).toBe('search_contacts');
					expect(ctrl.getSuggestions()[numSavedSearches + 1].id).toBe(savedSearches[0].id);

				});

			});

			describe('with a query', ( ) => {

				beforeEach( ( ) => {

					ctrl.query = 'foo';

					scope.$digest();

					$httpBackend.expectGET(/api\/object\/search/)
						.respond([]);

					$httpBackend.expectGET(/objectsearch/)
						.respond(suggestions);

					el.find('input').triggerHandler('focus');

					$httpBackend.flush();

				});

				it('should return the suggestions w/o the search_contacts link and predefined searches if the query is not null', ( ) => {

					let numSavedSearches = savedSearchesService.getPredefinedSearches().length;

					expect(ctrl.getSuggestions()[0].id).not.toBe('search_contacts');

					expect(ctrl.getSuggestions()[0].id).toBe(suggestions[0].id);

					ctrl.query = '';

					scope.$digest();

					expect(ctrl.getSuggestions()[0].id).toBe('search_contacts');
					expect(ctrl.getSuggestions()[numSavedSearches + 1].id).toBe(savedSearches[0].id);

				});

				it('should fetch new data when the query changes', ( ) => {

					$httpBackend.expectGET(/api\/object\/search/)
						.respond([]);

					$httpBackend.expectGET(/objectsearch/)
						.respond(suggestions);

					ctrl.query = 'bar';

					scope.$digest();

					$httpBackend.flush();

					$httpBackend.verifyNoOutstandingExpectation();

				});

			});

		});

	});

	afterEach( ( ) => {

		apiCacher.clear();
		
	});

});
