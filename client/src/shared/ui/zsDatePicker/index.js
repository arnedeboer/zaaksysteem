import angular from 'angular';
import once from 'lodash/once';

export default
	angular.module('zsDatePicker', [
	])
		.directive('zsDatePicker', [ '$q', 'dateFilter', ( $q, dateFilter ) => {

			let load = once(
				( ) => {
					return $q( ( resolve/*, reject*/ ) => {
						require([ 'moment', 'pikaday', './styles.scss' ], ( ...rest ) => {
							resolve(rest);
						});
					});
				}
			);

			return {
				restrict: 'A',
				require: 'ngModel',
				link: ( scope, element, attrs, ngModel ) => {

					let picker;

					let setDatePicker = ( value ) => {

						let val = value || null;

						if (picker && dateFilter(picker.getDate(), 'dd-MM-yyyy') !== dateFilter(val, 'dd-MM-yyyy')) {
							picker.setDate(val, true);
						}

					};

					ngModel.$parsers.unshift(( val ) => {
						let value = val;

						if (val && val.match(/^\d{1,2}-\d{1,2}-\d{4}$/)) {

							let values = val.split('-');

							value = new Date(values[2], values[1] - 1, values[0]);

							value = dateFilter(value, 'yyyy-MM-dd');
						}

						setDatePicker(value);

						return value;
					});

					ngModel.$formatters.unshift(( val ) => {
						let value = val ?
							dateFilter(val, 'dd-MM-yyyy')
							: val;

						return value;
					});

					load().then(( libs ) => {

						let Pikaday = libs[1];

						picker = new Pikaday({
							field: element[0],
							format: 'DD-MM-YYYY',
							i18n: {
								previousMonth: 'Vorige maand',
								nextMonth: 'Volgende maand',
								months: [ 'Januari', 'Februari', 'Maart', 'April', 'Mei', 'Juni', 'Juli', 'Augustus', 'September', 'Oktober', 'November', 'December' ],
								weekdays: [ 'Zondag', 'Maandag', 'Dinsdag', 'Woensdag', 'Donderdag', 'Vrijdag', 'Zaterdag' ],
								weekdaysShort: [ 'Zo', 'Ma', 'Di', 'Wo', 'Do', 'Vr', 'Zat']
							}
						});

						if (ngModel.$modelValue) {
							setDatePicker(ngModel.$modelValue);
						}

					});

				}
			};

		}])
		.name;
