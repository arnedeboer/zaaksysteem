import angular from 'angular';
import roleServiceModule from './../../user/roleService';
import composedReducerModule from './../../api/resource/composedReducer';
import template from './template.html';
import fill from 'lodash/fill';
import range from 'lodash/range';
import find from 'lodash/find';
import get from 'lodash/get';
import includes from 'lodash/includes';
import sortBy from 'lodash/sortBy';
import seamlessImmutable from 'seamless-immutable';
import shortid from 'shortid';
import './role-picker.scss';

export default
	angular.module('zsRolePicker', [
		roleServiceModule,
		composedReducerModule
	])
		.directive('zsRolePicker', [ '$rootScope', 'roleService', 'composedReducer', ( $rootScope, roleService, composedReducer ) => {

			let roleResource = roleService.createResource($rootScope);

			return {
				restrict: 'E',
				template,
				scope: {
					unit: '&',
					role: '&',
					depth: '&',
					onChange: '&'
				},
				bindToController: true,
				controller: [ '$scope', '$element', function ( scope, element ) {

					let ctrl = this,
						unitReducer = composedReducer( { scope }, roleResource, ctrl.depth)
							.reduce(( units, preferredDepth ) => {

								let depth = preferredDepth || 0;

								return units.filter(unit => unit.depth >= depth);
							}),
						roleReducer = composedReducer( { scope }, unitReducer, ( ) => ctrl.selectedUnit)
							.reduce( ( units, selectedUnit ) => {

								let unit = find(units, { org_unit_id: selectedUnit })
									|| find(units, { depth: 0 });

								return seamlessImmutable(
									sortBy(get(unit, 'roles'), role => {
										return role.system ? 1 : 0;
									})
								);
									
							});

					ctrl.roleQuery = '';

					ctrl.isFilterVisible = ( ) => {

						let numRoles = get(roleReducer.data(), 'length', 0);

						return numRoles > 30;
					};

					ctrl.handleUnitChange = ( ) => {

						let unitObj = find(unitReducer.data(), { org_unit_id: ctrl.selectedUnit }),
							roles = unitObj.roles,
							roleId =
								get(
									find(roles, { role_id: ctrl.role() })
									|| find(roles, { name: 'Behandelaar' }),
									'role_id'
								);

						ctrl.onChange( {
							$unit: ctrl.selectedUnit,
							$role: roleId
						});

					};

					ctrl.handleRoleChange = ( ) => {

						ctrl.onChange({
							$unit: ctrl.unit(),
							$role: ctrl.selectedRole
						});

					};

					ctrl.getUnitOptions = composedReducer( { scope }, unitReducer, ctrl.unit)
						.reduce( ( units, selectedUnit ) => {

							let options =
								units.map(
									unit => {
										return {
											id: shortid(),
											value: unit.org_unit_id,
											label:
												fill(
													range(Number(unit.depth)),
													'-'
												).join('') + unit.name,
											selected: selectedUnit === unit.org_unit_id
										};
									}
								);

							return options.asMutable({ deep: true });
						})
						.data;

					ctrl.getRoleOptions = composedReducer( { scope }, roleReducer, ctrl.role, ( ) => ctrl.roleQuery)
						.reduce( ( roles, selectedRole, roleQuery ) => {

							let options =
								roles.map(
									role => {
										return {
											id: shortid(),
											value: role.role_id,
											label: role.system === 0 ?
													`* ${role.name}`
													: role.name,
											selected: role.role_id === selectedRole
										};
									}
								)
									.filter(option => {
										return option.selected || includes(option.label.toLowerCase(), roleQuery.toLowerCase());
									});

							return options.asMutable({ deep: true });

						})
						.data;

					roleReducer.onUpdate( ( ) => {

						let size = ctrl.isFilterVisible() ? 10 : 0;

						element.find('select').eq(1).attr('size', size);

					});

					scope.$watch(( ) => ctrl.unit(), ( unit ) => {
						ctrl.selectedUnit = unit;
					});

					scope.$watch(( ) => ctrl.role(), ( role ) => {
						ctrl.selectedRole = role;
					});

				}],
				controllerAs: 'vm'
			};

		}])
		.name;
