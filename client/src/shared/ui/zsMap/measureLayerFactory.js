import angular from 'angular';
import each from 'lodash/each';
import keyBy from 'lodash/keyBy';
import mapValues from 'lodash/mapValues';

const MSG_HELP = 'Klik om te beginnen met meten';
const MSG_LINE_CONTINUE = 'Klik om een afstand te meten';
const MSG_AREA_CONTINUE = 'Klik om een oppervlak te meten';

export default ( OpenLayers ) => {

	let layer,
		source = new OpenLayers.source.Vector(),
		enabled = false,
		measureMode = 'area',
		mapComponent,
		listenerKey,
		interaction,
		sketch,
		tooltips = null;

	let clear = ( ) => {

		source.clear();

		tooltips.measure.element.classList.add('hidden');

	};

	let handlePointerMove = ( event ) => {

		let helpMsg = MSG_HELP;

		if (event.dragging) {
			return;
		}

		if (sketch) {
			let geom = (sketch.getGeometry());

			if (geom instanceof OpenLayers.geom.Polygon) {
				helpMsg = MSG_AREA_CONTINUE;
			} else if (geom instanceof OpenLayers.geom.LineString) {
				helpMsg = MSG_LINE_CONTINUE;
			}
		}

		tooltips.help.element.innerHTML = helpMsg;
		tooltips.help.layer.setPosition(event.coordinate);
		tooltips.help.element.classList.remove('hidden');

	};

	let formatArea = ( polygon ) => {
		let area = polygon.getArea(),
			output;

		if (area > 10000) {
			output = `${(Math.round(area / 1000000 * 100) / 100)} km<sup>2</sup>`;
		} else {
			output = `${(Math.round(area * 100) / 100)} m<sup>2</sup>`;
		}
		return output;
	};

	let formatLength = ( line ) => {

		let length = Math.round(line.getLength() * 100) / 100,
			output;

		if (length > 100) {
			output = `${(Math.round(length / 1000 * 100) / 100)}  km`;
		} else {
			output = `${(Math.round(length * 100) / 100)} m`;
		}

		return output;
	};

	let addInteraction = ( ) => {

		let drawListener;

		if (interaction) {
			mapComponent.removeInteraction(interaction);
		}

		interaction = new OpenLayers.interaction.Draw({
			source,
			type: measureMode === 'line' ? 'LineString' : 'Polygon',
			style: new OpenLayers.style.Style({
				fill: new OpenLayers.style.Fill({
					color: 'rgba(255, 255, 255, 0.2)'
				}),
				stroke: new OpenLayers.style.Stroke({
					color: 'rgba(0, 0, 0, 0.5)',
					lineDash: [10, 10],
					width: 3
				}),
				image: new OpenLayers.style.Circle({
					radius: 5,
					stroke: new OpenLayers.style.Stroke({
						color: 'rgba(0, 0, 0, 0.7)',
						width: 2
					}),
					fill: new OpenLayers.style.Fill({
						color: 'rgba(255, 255, 255, 0.2)'
					})
				})
			})
		});

		mapComponent.addInteraction(interaction);

		interaction.on('drawstart', ( evt ) => {

			let coord;
			
			sketch = evt.feature;

			coord = evt.coordinate;

			source.clear();

			tooltips.measure.element.classList.remove('hidden');

			drawListener =
				sketch.getGeometry().on('change', ( event ) => {

					let geom = event.target,
						output;

					if (geom instanceof OpenLayers.geom.Polygon) {
						output = formatArea(geom);
						coord = geom.getInteriorPoint().getCoordinates();
					} else {
						output = formatLength(geom);
						coord = geom.getLastCoordinate();
					}

					tooltips.measure.element.innerHTML = output;
					tooltips.measure.layer.setPosition(coord);

				});
		});

		interaction.on('drawend', ( ) => {

			sketch = null;

			OpenLayers.Observable.unByKey(drawListener);

		});

	};
	
	layer = new OpenLayers.layer.Vector({
		source,
		style: new OpenLayers.style.Style({
			fill: new OpenLayers.style.Fill({
				color: 'rgba(255, 255, 255, 0.2)'
			}),
			stroke: new OpenLayers.style.Stroke({
				color: '#000000',
				width: 3
			}),
			image: new OpenLayers.style.Circle({
				radius: 7,
				fill: new OpenLayers.style.Fill({
					color: '#ffffff'
				})
			})
		})
	});

	tooltips =
		mapValues(
			keyBy('help measure'.split(' ')),
			( value, key ) => {

				let element = angular.element(
						'<div></div>'
					)[0];

				element.className = `map-measure-tooltip map-measure-tooltip-${key}`;

				return {
					element,
					layer: new OpenLayers.Overlay({
						element,
						offset: [ 15, 0 ],
						positioning: 'center-left'
					})
				};
			}
		);

	return {
		enable: ( ) => {

			if (enabled) {
				return;
			}

			enabled = true;

			listenerKey = mapComponent.on('pointermove', handlePointerMove);

			each(tooltips, ( tooltip ) => {
				mapComponent.addOverlay(tooltip.layer);
			});

			addInteraction();

		},
		disable: ( ) => {

			if (!enabled) {
				return;
			}

			clear();

			enabled = false;

			each(tooltips, ( tooltip ) => {
				mapComponent.removeOverlay(tooltip.layer);
			});

			mapComponent.unByKey(listenerKey);

			if (interaction) {
				mapComponent.removeInteraction(interaction);
			}

		},
		isEnabled: ( ) => enabled,
		clear,
		setMeasureMode: ( mode ) => {

			measureMode = mode;

			clear();

			if (enabled) {
				addInteraction();
			}

		},
		getMeasureMode: ( ) => measureMode,
		getLayer: ( ) => layer,
		setMapComponent: ( component ) => {
			mapComponent = component;
		}
	};

};
