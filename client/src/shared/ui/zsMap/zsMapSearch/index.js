import angular from 'angular';
import resourceModule from './../../../api/resource';
import zsSuggestionListModule from './../../zsSuggestionList';
import zsClickOutsideModule from './../../zsClickOutside';
import sessionServiceModule from './../../../user/sessionService';
import composedReducerModule from './../../../api/resource/composedReducer';
import shortid from 'shortid';
import get from 'lodash/get';
import template from './template.html';

export default
	angular.module('zsMapSearch', [
		resourceModule,
		zsSuggestionListModule,
		zsClickOutsideModule,
		sessionServiceModule,
		composedReducerModule
	])
		.directive('zsMapSearch', [ '$document', 'resource', 'composedReducer', 'sessionService', ( $document, resource, composedReducer, sessionService ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					onChange: '&',
					addressType: '&'
				},
				bindToController: true,
				controller: [ '$scope', '$element', function ( scope, element ) {

					let ctrl = this,
						suggestionResource,
						placeholderReducer,
						inputDelegate = { input: element.find('input').eq(0) },
						isOpen = false;

					placeholderReducer = composedReducer( { scope }, sessionService.createResource(scope))
						.reduce( session => {

							return session ?
								`Zoek op adres (bv. ${get(session, 'instance.account.instance.place')}, ${get(session, 'instance.account.instance.address')})`
								: '';

						});

					ctrl.query = '';

					ctrl.getInputDelegate = ( ) => inputDelegate;

					ctrl.handleFocus = ( ) => {
						isOpen = true;
					};

					ctrl.handleSelect = ( suggestion ) => {

						let location =
							ctrl.addressType() === 'coordinate' ?
								[ suggestion.data.coordinates.lat, suggestion.data.coordinates.lng ].join(',')
								: suggestion.data.identification;

						ctrl.onChange({
							$location: location
						});

						isOpen = false;
					};

					ctrl.handleClickOutside = ( ) => {

						if (ctrl.getSuggestions()) {
							scope.$evalAsync(( ) => {
								isOpen = false;
							});
						}

						return false;

					};

					ctrl.getPlaceholder = placeholderReducer.data;

					suggestionResource =
						resource(( ) => {
							return isOpen && ctrl.query ?
								{
									url: '/plugins/maps',
									params: {
										term: ctrl.query
									}
								}
								: null;
						}, { scope, cache: { disabled: true } } )
							.reduce( ( requestOptions, data ) => {

								let addresses = get(data, 'json.addresses', []),
									suggestions =
										addresses.map(
											suggestion => {
												return {
													id: shortid(),
													label: suggestion.identification,
													data: suggestion
												};
											}
										);

								return suggestions;

							});

					ctrl.isOpen = ( ) => isOpen;

					ctrl.getSuggestions = suggestionResource.data;

				}],
				controllerAs: 'vm'
			};

		}])
		.name;
