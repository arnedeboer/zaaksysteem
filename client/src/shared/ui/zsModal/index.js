import angular from 'angular';
import propCheck from './../../util/propCheck';
import defaults from 'lodash/defaults';
import template from './template.html';
import each from 'lodash/each';
import assign from 'lodash/assign';
import './styles.scss';

export default
	angular.module('zsModal', [
	])
		.factory('zsModal', [ '$interpolate', '$rootScope', '$compile', '$timeout', '$animate', '$document', '$window', ( $interpolate, $rootScope, $compile, $timeout, $animate, $document, $window ) => {

			let tpl = template,
				compiler;

			if ($interpolate.startSymbol() !== '{{') {
				tpl = tpl.replace(/{{/g, $interpolate.startSymbol())
					.replace(/}}/g, $interpolate.endSymbol());
			}

			compiler = $compile(tpl);

			return ( preferredOptions ) => {

				let options =
						defaults(preferredOptions, {
							title: '',
							parent: $document.find('body'),
							position: ( ) => {
								return {
									top: '0px',
									left: '0px',
									right: '0px',
									bottom: '0px'
								};
							},
							classes: '',
							repositionOnScroll: false
						}),
					scope,
					modalEl,
					modal,
					closeListeners = [],
					isClosed = false;

				let attemptClose = ( ) => {
					if (!isClosed && closeListeners.filter(fn => fn()).length === 0) {
						modal.close();
					}
				};

				let onKeyUp = ( event ) => {
					if (event.keyCode === 27) {
						event.stopPropagation();
						scope.$evalAsync(attemptClose);
					}
				};

				let resetDimensions = ( ) => {
					modalEl.css(options.position());
				};

				propCheck.throw(
					propCheck.shape({
						title: propCheck.string.optional
					}),
					options
				);

				scope = $rootScope.$new(true);

				if (options.before) {
					options = assign({}, options, {
						before: null,
						after: null,
						parent: options.before.parent()
					});
				}

				modal = {
					open: ( ) => {

						if (options.openFrom) {

							let bounds = options.openFrom[0].getBoundingClientRect(),
								viewport = { width: $window.innerWidth, height: $window.innerHeight },
								dimensions = {
									top: `${bounds.top}px`,
									left: `${bounds.left}px`,
									right: `${viewport.width - bounds.right}px`,
									bottom: `${viewport.height - bounds.bottom}px`
								};

							modalEl.css(dimensions);

							$timeout(resetDimensions, 0, false);

						}
						
						$document[0].addEventListener('keyup', onKeyUp, true);

						return $animate.enter(modalEl, options.parent, options.after, { addClass: 'modal-open' });
					},
					close: ( ) => {
						
						isClosed = true;

						return $animate.leave(modalEl, { removeClass: 'modal-open' } )
							.then(( ) => {
								$document[0].removeEventListener('keyup', onKeyUp, true);
								scope.$destroy();
							});
					},
					hide: ( ) => {

						return $animate.addClass(modalEl, 'ng-hide');

					},
					show: ( ) => {

						return $animate.removeClass(modalEl, 'ng-hide');

					},
					onClose: ( fn ) => {
						closeListeners = closeListeners.concat(fn);
					}
				};

				scope.title = options.title;

				scope.onCloseClick = attemptClose;

				if (options.repositionOnScroll) {
					$window.addEventListener('scroll', resetDimensions);

					modal.onClose( ( ) => {
						$window.removeEventListener('scroll', resetDimensions);
					});
				}

				compiler(scope, ( el ) => {

					modalEl = el;

					each(options.classes.split(' '), ( cl ) => {
						modalEl.addClass(cl);
					});

					modalEl.find('zs-transclude').replaceWith(options.el);
				});

				return modal;

			};

		}])
		.name;
