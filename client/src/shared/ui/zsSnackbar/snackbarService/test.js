import angular from 'angular';
import 'angular-mocks';

import snackbarServiceModule from '.';

describe('snackbarService', ( ) => {

	let $rootScope, $timeout,
		$q,
		snackbarService;

	beforeEach(angular.mock.module(snackbarServiceModule));

	beforeEach(angular.mock.inject( [ '$rootScope', '$timeout', '$q', 'snackbarService', ( ...rest ) => {

		[ $rootScope, $timeout, $q, snackbarService ] = rest;

	}]));

	it('should throw if message is undefined', ( ) => {

		expect( ( ) => {

			snackbarService.add();

		}).toThrow();

	});

	it('should add an info snack', ( ) => {

		snackbarService.info('foo');

		expect(snackbarService.getSnacks().length).toBe(1);

		expect(snackbarService.getSnacks()[0].type).toBe('info');

	});

	it('should add an error snack', ( ) => {

		snackbarService.error('err');

		expect(snackbarService.getSnacks().length).toBe(1);

		expect(snackbarService.getSnacks()[0].type).toBe('error');

	});

	it('should remove a snack', ( ) => {

		let snack = snackbarService.info('foo');

		expect(snackbarService.getSnacks().length).toBe(1);

		snackbarService.remove(snack);

		expect(snackbarService.getSnacks().length).toBe(0);

	});

	it('should set a default timeout if undefined', ( ) => {

		let snack = snackbarService.info('foo');

		expect(snack.timeout).toBeGreaterThan(0);

	});

	it('should persist error snacks', ( ) => {

		let snack = snackbarService.error('foo', { timeout: 1 });

		expect(snack.timeout).toBe(0);

	});

	it('should remove a snack after a timeout', ( ) => {

		snackbarService.info('foo');

		expect(snackbarService.getSnacks().length).toBe(1);

		$timeout.flush();

		expect(snackbarService.getSnacks().length).toBe(0);

	});

	it('should cancel the timeout if the snack is removed earlier', ( ) => {

		let snack = snackbarService.info('foo');

		snackbarService.remove(snack);

		expect($timeout.flush).toThrow();

	});

	describe('when adding a wait snack', ( ) => {

		let deferred,
			label = 'foo',
			then,
			catchHandler,
			resolved,
			rejected,
			returnValue,
			data = { foo: 'bar' },
			snack;

		let resolvePromise = ( ) => {
			deferred.resolve(data);

			$rootScope.$digest();
		};

		let rejectPromise = ( ) => {

			deferred.reject(data);

			$rootScope.$digest();

		};

		beforeEach(( ) => {

			deferred = $q.defer();

			then = jasmine.createSpy('then');
			catchHandler = jasmine.createSpy('catch');

			resolved = jasmine.createSpy('resolve');
			rejected = jasmine.createSpy('reject');

			returnValue = snackbarService.wait(label, { promise: deferred.promise, then, catch: catchHandler })
				.then(resolved)
				.catch(rejected);

			snack = snackbarService.getSnacks()[0];

		});

		it('should return a promise', ( ) => {

			expect(returnValue).toBeDefined();

			expect('then' in returnValue).toBe(true);

		});

		it('should add a wait snack', ( ) => {

			expect(snackbarService.getSnacks().length).toBe(1);
		});

		describe('when returning a label', ( ) => {

			let returnedLabel = 'bar';

			beforeEach( ( ) => {

				then.and.returnValue(returnedLabel);
				catchHandler.and.returnValue(returnedLabel);

			});

			describe('and it resolves', ( ) => {

				beforeEach(resolvePromise);

				it('should remove the snack when the promise is fulfilled', ( ) => {

					expect(snackbarService.getSnacks()[0]).not.toBe(snack);

				});

				it('should call the then handler with the resolve value from the promise', ( ) => {

					expect(then).toHaveBeenCalledWith(data);

				});

				it('should add an info snack with the label returned', ( ) => {

					expect(snackbarService.getSnacks().length).toBe(1);

					expect(snackbarService.getSnacks()[0].message).toBe(returnedLabel);

					expect(snackbarService.getSnacks()[0].type).toBe('info');

				});

				it('should resolve the promise with the created snack', ( ) => {

					expect(resolved).toHaveBeenCalledWith(snackbarService.getSnacks()[0]);

				});

			});

			describe('and it rejects', ( ) => {

				beforeEach(rejectPromise);

				it('should remove the snack when the promise is fulfilled', ( ) => {

					expect(snackbarService.getSnacks()[0]).not.toBe(snack);

				});

				it('should call the then handler with the resolve value from the promise', ( ) => {

					expect(catchHandler).toHaveBeenCalledWith(data);

				});

				it('should add an info snack with the label returned', ( ) => {

					expect(snackbarService.getSnacks().length).toBe(1);

					expect(snackbarService.getSnacks()[0].message).toBe(returnedLabel);

					expect(snackbarService.getSnacks()[0].type).toBe('error');

				});

				it('should resolve the promise with the created snack', ( ) => {

					expect(rejected).toHaveBeenCalledWith(snackbarService.getSnacks()[0]);

				});

			});

		});

		describe('without returning a label', ( ) => {

			beforeEach(resolvePromise);

			it('shouldn\'t add a snack', ( ) => {

				expect(snackbarService.getSnacks().length).toBe(0);

			});

			it('should resolve the promise with undefined', ( ) => {

				expect(resolved).toHaveBeenCalledWith(undefined);

			});

		});
		
	});

});
