import executeRule from '.';
import difference from 'lodash/difference';
import keys from 'lodash/keys';
import evaluateFormula from './evaluateFormula';

describe('executeRule', ( ) => {

	it('should return an object with the keys hidden, disabled, values', ( ) => {

		let rule = {
				conditions: {
					conditions: []
				},
				then: [],
				else: []
			},
			result = executeRule(rule, { values: { } });

		expect(
			difference(
				['hidden', 'disabled', 'values' ],
				keys(result)
			).length
		).toBe(0);

	});

	describe('when rule matches', ( ) => {

		it('should execute the "then" actions', ( ) => {

			let rule = {
					conditions: {
						conditions: []
					},
					then: [
						{
							data: {
								attribute_name: 'foo',
								value: 'foo'
							},
							type: 'set_value'
						}
					],
					else: [
						{
							data: {
								attribute_name: 'bar',
								value: 'bar'
							},
							type: 'set_value'
						}
					]
				},
				result = executeRule(rule, { values: { } });

			expect(result.values.foo).toBe('foo');

			expect(result.values.bar).not.toBe('bar');

		});

	});

	describe('when rule doesn\'t match', ( ) => {

		it('should execute the "else" actions', ( ) => {

			let rule = {
					conditions: {
						conditions: [
							{
								attribute_name: 'foo',
								validation_type: 'immediate',
								values: [ 'foo' ]
							}
						],
						type: 'and'
					},
					then: [
						{
							data: {
								attribute_name: 'foo',
								value: 'foo'
							},
							type: 'set_value'
						}
					],
					else: [
						{
							data: {
								attribute_name: 'bar',
								value: 'bar'
							},
							type: 'set_value'
						}
					]
				},
				result = executeRule(rule, { values: { } });

			expect(result.values.foo).not.toBe('foo');

			expect(result.values.bar).toBe('bar');

		});

	});

	describe('when executing actions', ( ) => {

		let createRuleWithAction = ( action ) => {

			return {
				conditions: {
					conditions: [],
					type: 'and'
				},
				then: [ action ]
			};
		};

		describe('of type set_value', ( ) => {

			describe('and can_change is truthy', ( ) => {

				let rule =
					createRuleWithAction({
						type: 'set_value',
						data: {
							can_change: true,
							attribute_name: 'foo',
							value: 'foo'
						}
					});

				it('should set the value only when it is empty', ( ) => {

					let result =
						executeRule(
							rule,
							{ values: { foo: 'bar' } }
						);

					expect(result.values.foo).toBe('bar');
					expect(result.disabled.foo).toBeFalsy();

					result = executeRule(
						rule,
						{ values: { foo: '' } }
					);

					expect(result.values.foo).toBe('foo');
					expect(result.disabled.foo).toBeFalsy();

				});

			});

			describe('and can_change is falsy', ( ) => {

				it('should set the value and disabled to true', ( ) => {

					let rule =
							createRuleWithAction({
								type: 'set_value',
								data: {
									can_change: false,
									attribute_name: 'foo',
									value: 'foo'
								}
							}),
						result =
							executeRule(
								rule,
								{ values: { foo: 'bar' } }
							);

					expect(result.values.foo).toBe('foo');
					expect(result.disabled.foo).toBeTruthy();

				});

			});

		});

		describe('of type hide_attribute', ( ) => {

			it('should hide the attribute', ( ) => {

				let rule =
						createRuleWithAction({
							type: 'hide_attribute',
							data: {
								attribute_name: 'foo'
							}
						}),
					result =
						executeRule(
							rule,
							{ values: { } }
						);

				expect(result.hidden.foo).toBe(true);

			});

		});

		describe('of type show_attribute', ( ) => {

			it('should unhide the attribute', ( ) => {

				let rule =
						createRuleWithAction({
							type: 'show_attribute',
							data: {
								attribute_name: 'foo'
							}
						}),
					result =
						executeRule(
							rule,
							{ values: { } }
						);

				expect(result.hidden.foo).toBeFalsy();

			});

		});

		describe('of type hide_group', ( ) => {

			it('should hide all the related attributes', ( ) => {

				let rule =
						createRuleWithAction({
							type: 'hide_group',
							data: {
								attribute_name: 1,
								related_attributes: [ 'foo', 'bar' ]
							}
						}),
					result =
						executeRule(
							rule,
							{ values: { } }
						);

				expect(result.hidden.foo).toBe(true);
				expect(result.hidden.bar).toBe(true);

			});

		});

		describe('of type show_group', ( ) => {

			it('should show all the related attributes', ( ) => {

				let rule =
						createRuleWithAction({
							type: 'show_group',
							data: {
								attribute_name: 1,
								related_attributes: [ 'foo', 'bar' ]
							}
						}),
					result =
						executeRule(
							rule,
							{ values: { } }
						);

				expect(result.hidden.foo).toBeFalsy();
				expect(result.hidden.bar).toBeFalsy();

			});

		});

		describe('of type set_value_formula', ( ) => {

			it('should set the value of the attribute to the result of the formula and disable the attribute', ( ) => {

				let formula = '3 * 5',
					rule =
						createRuleWithAction({
							type: 'set_value_formula',
							data: {
								attribute_name: 'foo',
								formula
							}
						}),
					result =
						executeRule(
							rule,
							{ values: { } }
						);

				expect(result.values.foo).toBe(evaluateFormula(formula, {}));
				expect(result.disabled.foo).toBe(true);

			});

		});

	});

});
