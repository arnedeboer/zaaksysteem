import angular from 'angular';
import resourceModule from '../../shared/api/resource';
import proposalDetailViewModule from './proposalDetailView';
import first from 'lodash/head';
import snackbarServiceModule from './../../shared/ui/zsSnackbar/snackbarService';
import auxiliaryRouteModule from './../../shared/util/route/auxiliaryRoute';
import onRouteActivateModule from './../../shared/util/route/onRouteActivate';
import zsModalModule from './../../shared/ui/zsModal';
import viewTitleModule from './../../shared/util/route/viewTitle';
import template from './index.html';
import seamlessImmutable from 'seamless-immutable';
import flatten from 'lodash/flatten';
import find from 'lodash/find';
import getAttributes from '../shared/getAttributes';
import './styles.scss';

export default {

	moduleName:
		angular.module('proposalDetail', [
			resourceModule,
			proposalDetailViewModule,
			snackbarServiceModule,
			auxiliaryRouteModule,
			onRouteActivateModule,
			zsModalModule,
			viewTitleModule
		])
		.name,
	config: [
		{
			route: {
				url: 'voorstel/:zaakID',
				title: [ 'proposal', 'currentAppConfig', ( proposal, appConfig ) => {

					let findDescription = ( ) => find( getAttributes(appConfig.data()).voorstel, ( attribute ) => attribute.external_name === 'voorstelonderwerp');

					return `${proposal.data().instance.number}: ${proposal.data().instance.attributes[findDescription().internal_name.searchable_object_id] || proposal.data().instance.casetype.name}`;

				}],
				resolve: {
					proposal: [ '$rootScope', '$stateParams', '$q', 'resource', 'snackbarService', ( $rootScope, $stateParams, $q, resource, snackbarService ) => {

						let proposalResource = resource(
							{ url: '/api/v1/case', params: { zql: `SELECT {} FROM case WHERE case.number = ${$stateParams.zaakID}` } },
							{ scope: $rootScope }
						)
							.reduce( ( requestOptions, data ) => {

								return first(data || seamlessImmutable([]));

							});

						return proposalResource
							.asPromise()
							.then( ( ) => proposalResource)
							.catch( ( err ) => {

								snackbarService.error('Het voorstel kon niet worden geladen. Neem contact op met uw beheerder voor meer informatie.');

								return $q.reject(err);
							});
					}],
					documents: [ '$rootScope', '$stateParams', '$q', 'resource', 'snackbarService', 'proposal', 'currentAppConfig', ( $rootScope, $stateParams, $q, resource, snackbarService, proposal, currentAppConfig ) => {

						let attachmentAttributes,
							fileUuids;

						// Get the attributes magic strings that have been set as attachment attributes in the koppelprofiel
						attachmentAttributes = currentAppConfig.data().instance.interface_config.attribute_mapping
							.filter( attribute => attribute.external_name.indexOf('voorstelbijlage') !== -1 && attribute.internal_name )
							.map( attribute => attribute.internal_name.searchable_object_id);

						// Get the uuids of files that have that document label
						fileUuids =
							flatten(
								attachmentAttributes.map( attribute => proposal.data().instance.attributes[attribute])
							);

						return resource(
							{ url: `/api/v1/case/${proposal.data().reference}/document` },
							{ scope: $rootScope }
						)
							.asPromise()
							.then( data => {

								// When data is returned, filter all the attachments of the case with the uuids that have been found in previous step
								return data.filter( attachment => find(fileUuids, ( fileUuid ) => fileUuid === attachment.reference)) || seamlessImmutable([]);

							})
							.catch( ( err ) => {

								snackbarService.error('De voorsteldocumenten konden niet worden geladen. Neem contact op met uw beheerder voor meer informatie.');

								return $q.reject(err);

							});

					}],
					casetype: [ '$rootScope', '$q', 'resource', 'proposal', 'currentAppConfig', 'snackbarService', ( $rootScope, $q, resource, proposal, currentAppConfig, snackbarService ) => {

						let casetypeResource = resource(( ) => {
	
							return currentAppConfig.data().instance.interface_config.access === 'rw' ?
								{
									url: `/api/v1/casetype/${proposal.data().instance.casetype.reference}`,
									params: {
										version: proposal.data().instance.casetype.instance.version
									}
								}
								: null;

						}, { scope: $rootScope });

						return casetypeResource
							.asPromise()
							.then( (data) => first(data) )
							.catch( ( err ) => {

								snackbarService.error('Het zaaktype kon niet worden geladen. Neem contact op met uw beheerder voor meer informatie.');

								return $q.reject(err);

							});

					}]
				},
				auxiliary: true,
				onActivate: [ '$timeout', '$rootScope', '$window', '$document', '$compile', 'viewTitle', '$stateParams', 'zsModal', 'proposal', 'documents', 'casetype', 'currentAppConfig', ( $timeout, $rootScope, $window, $document, $compile, viewTitle, $stateParams, zsModal, proposal, documents, casetype, currentAppConfig ) => {

					let scrollEl = angular.element($document[0].querySelector('.body-scroll-container'));

					scrollEl.css({ overflow: 'hidden' });

					let openModal = ( ) => {

						let modal,
							unregister,
							scope = $rootScope.$new(true);

						scope.proposal = proposal;
						scope.documents = documents;
						scope.casetype = casetype;
						scope.appConfig = currentAppConfig.data;

						modal = zsModal({
							el: $compile(angular.element(template))(scope),
							title: viewTitle.get(),
							classes: 'detail-modal'
						});

						modal.open();

						modal.onClose( ( ) => {
							$window.history.back();
							return true;
						});

						unregister = $rootScope.$on('$stateChangeStart', ( ) => {

							$window.requestAnimationFrame( ( ) => {

								$rootScope.$evalAsync(( ) => {
									modal.close()
										.then(( ) => {

											scope.$destroy();

											scrollEl.css({
												overflow: '',
												height: ''
											});

										});

									unregister();

								});

							});
							
						});

					};


					$window.requestAnimationFrame( ( ) => {
						$rootScope.$evalAsync(openModal);
					});

				}]

			},
			state: 'proposalDetail'
		}
	]
};
