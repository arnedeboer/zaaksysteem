import angular from 'angular';
import template from './index.html';
import resourceModule from '../../shared/api/resource';
import composedReducerModule from '../../shared/api/resource/composedReducer';
import meetingListViewModule from './proposalsListView';
import meetingListItemModule from '../shared/meetingListItem';
import snackbarServiceModule from '../../shared/ui/zsSnackbar/snackbarService';
import seamlessImmutable from 'seamless-immutable';
import flattenAttrs from '../shared/flattenAttrs';
import find from 'lodash/find';

export default {

	moduleName:
		angular.module('Zaaksysteem.meeting.proposalsList', [
			resourceModule,
			meetingListViewModule,
            meetingListItemModule,
			snackbarServiceModule,
			composedReducerModule
		])
		.controller('proposalsListController', [ '$stateParams', '$scope', 'proposalsResource', 'currentAppConfig', ( $stateParams, $scope, proposalsResource, currentAppConfig ) => {

			$scope.proposalsResource = proposalsResource;
			$scope.meetingType = $stateParams.meetingType;
			$scope.appConfig = currentAppConfig.data;

			$scope.$on('$destroy', ( ) => {
				
				[ proposalsResource ].forEach( resource => {
					resource.destroy();
				});
			});

		}])
		.name,
	config: [
		{
			route: {
				url: '/:group/voorstellen',
				scope: {
					appConfig: '&'
				},
				params: {
					meetingType: { squash: true, value: 'open' },
					paging: { squash: true, value: 50 }
				},
				template,
				resolve: {
					currentAppConfig: [ '$rootScope', 'composedReducer', '$stateParams', 'appConfig', ( $rootScope, composedReducer, $stateParams, appConfig ) => {

						return composedReducer( { scope: $rootScope }, appConfig, $stateParams )
							.reduce( (config, state ) => find(config, ( configItem ) => configItem.instance.interface_config.short_name === state.group));

					}],
					proposalsResource: [ '$rootScope', '$q', '$stateParams', 'currentAppConfig', 'resource', 'snackbarService', ( $rootScope, $q, $stateParams, currentAppConfig, resource, snackbarService ) => {

						let proposalsResource = resource(( ) => {
								
							let statuses =
								$stateParams.meetingType === 'open' ?
									[ 'open', 'new' ]
									: [ 'resolved' ],
                                publicationAttribute = currentAppConfig.data().instance.interface_config.proposal_publication_filter_attribute,
								publicationAttributeValue = currentAppConfig.data().instance.interface_config.proposal_publication_filter_attribute_value,
								publicationQueryPartial = '';

							if (publicationAttribute && publicationAttributeValue) {
								publicationQueryPartial = `AND (${publicationAttribute.object.column_name} = "${publicationAttributeValue}")`;
							}

							return {
								url: '/api/v1/case',
								params: {
									zql: `SELECT {} FROM case WHERE (case.casetype.id IN (${currentAppConfig.data().instance.interface_config.casetype_voorstel_item.join()})) ${publicationQueryPartial} AND (case.status IN ("${statuses.join('","')}") )`,
									rows_per_page: $stateParams.paging
								}
							};

						}, { scope: $rootScope })

							// grab first value of array for every attribute to reduce
							// boilerplate
							
							.reduce( ( requestOptions, data ) => {
								return (data || seamlessImmutable([])).map(flattenAttrs);
							});


						return proposalsResource.asPromise()
							.then(( ) => proposalsResource)
							.catch( err => {
								snackbarService.error('Er ging iets fout bij het ophalen van de voorstellen. Neem contact op met uw beheerder voor meer informatie.');
								return $q.reject(err);
							});

					}]
				},
				controller: 'proposalsListController'
			},
			state: 'proposalsList'
		}
	]
};
