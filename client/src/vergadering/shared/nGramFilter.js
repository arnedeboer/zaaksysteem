import get from 'lodash/get';
import identity from 'lodash/identity';
import isArray from 'lodash/isArray';
import take from 'lodash/take';
import fuzzy from 'fuzzy';
import uniq from 'lodash/uniq';
import flattenDeep from 'lodash/flattenDeep';
import seamlessImmutable from 'seamless-immutable';
import wordNgrams from 'word-ngrams';
import getAttributes from './getAttributes';
import getCaseValueFromAttribute from './getCaseValueFromAttribute';

const NGRAM_RANGE = [ 1, 2, 3 ];

let index = seamlessImmutable([]),
	results,
	matches,
	attributes;

export default {
	init: ( config ) => {

		attributes = getAttributes(config)
			.voorstel
			.filter(attr => attr.external_name !== 'voorsteldocumenten' )
			.map( attr => get(attr, 'internal_name.searchable_object_id'));

	},
	getAttributesToIndex: ( ) => attributes,
	addToIndex: ( elements ) => {

		let getNGramsFromElement = ( el ) => {

			return attributes ?
				attributes.map(attrName => getCaseValueFromAttribute(attrName, el))
				.filter(identity)
				.map(elValue => {

					let stringValue = isArray(elValue) ? elValue.join() : elValue;

					return NGRAM_RANGE.map(nGramLength => {
						return wordNgrams.listAllNGrams(
							wordNgrams.buildNGrams( stringValue, nGramLength, { caseSensitive: false, includePunctuation: false })
						);
					});
				}) : null;
		};

		index = seamlessImmutable(
			uniq(flattenDeep(elements.map(getNGramsFromElement)))
		);
		
	},
	getFromIndex: ( query ) => {

		results = take(fuzzy.filter(query, index.asMutable()), 15);
		matches = results.map( (el) => el.string );

		return matches;
	}
};
