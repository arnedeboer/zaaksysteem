export default ( attribute, caseItem ) => {

	let value;

	switch (attribute) {
		case 'vertrouwelijkheid':
		value = caseItem.instance.confidentiality.mapped;
		break;
		case 'aanvrager_naam':
		value = `${caseItem.instance.requestor.instance.subject.instance.first_names} ${caseItem.instance.requestor.instance.subject.instance.surname}`;
		break;
		case 'zaaktype':
		value = caseItem.instance.casetype.instance.name;
		break;
		case 'zaaknummer':
		value = String(caseItem.instance.number);
		break;
		default:
		value = caseItem.instance.attributes[attribute];
	}

	return String(value).toLowerCase();
};
