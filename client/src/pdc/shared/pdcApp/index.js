import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import uiViewTransitionDirectionModule from './../../../shared/util/route/uiViewTransitionDirection';
import pdcNavModule from './../pdcNav';
import zsSnackbarModule from './../../../shared/ui/zsSnackbar';
import template from './template.html';
import FTScroller from 'ftscroller';
import each from 'lodash/each';
import composedReducerModule from '../../../shared/api/resource/composedReducer';
import createReducerFromScopeEvent from './../../../shared/api/resource/resourceReducer/createReducerFromScopeEvent';
import './styles.scss';

export default
	angular.module('Zaaksysteem.pdc.pdcApp', [
		angularUiRouterModule,
		uiViewTransitionDirectionModule,
		pdcNavModule,
		zsSnackbarModule,
		composedReducerModule
	])
		.directive('pdcApp', [
				'$q', '$window', '$timeout', '$rootScope', '$state', '$stateParams', '$compile', 'composedReducer',
				( $q, $window, $timeout, $rootScope, $state, $stateParams, $compile, composedReducer ) => {

			const X_BOUNDS = 30;

			return {
				restrict: 'E',
				template,
				scope: {
					appConfig: '&'
				},
				bindToController: true,
				controller: [ '$scope', '$element', function ( scope, element ) {

					let ctrl = this,
						viewReducer,
						viewPlaceholder = angular.element('<div class="view-placeholder"><zs-spinner is-loading="true"></zs-spinner></div>'),
						indexBy = 'objectType',
						scrollerDeferred = $q.defer(),
						parent = element[0].querySelector('.scroll-container'),
						render,
						scroller;

					let getStateValue = ( params = $stateParams ) => params[indexBy];

					let getPlaceholderElForState = ( value ) => element[0].querySelector(`[view-name="${value}"]`);

					let isRelevantStateChange = ( toState, toParams, fromState/*, fromParams*/ ) => {
						return (toState.name === 'objectList' && (fromState.name === 'objectList' || !fromState.name));
					};

					let setScroller = ( initial ) => {

						let isCorrectSegment = false,
							currentSegment = scroller.currentSegment.x,
							expectedSegment = viewReducer.data().indexOf(getStateValue());
						
						isCorrectSegment = currentSegment === expectedSegment;

						$timeout(( ) => {

							if (!isCorrectSegment) {
								let scroll = expectedSegment * parent.offsetWidth;

								scroller.scrollTo(scroll, 0, !initial ? 250 : 0);
							}
							
						}, 0, false);

					};

					let dispatchProgress = ( ) => {
						let style = parent.querySelector('.ftscroller_x').style,
							transform = style.transform || style.webkitTransform,
							translateX = transform ? transform.match(/(\d+(\.\d+)?)px/)[1] : 0,
							percentage = Math.max(0, Math.min(1, Number(translateX) / ( parent.childNodes.length * parent.offsetWidth))),
							transitionTimingFunction = style.transitionTimingFunction,
							transitionDuration = style.transitionDuration;

						$rootScope.$emit('pdc.view.scroll', { percentage, transitionTimingFunction, transitionDuration });

						if (render) {
							$window.requestAnimationFrame(dispatchProgress);
						}
					};

					let startRender = ( ) => {
						render = true;
						$window.requestAnimationFrame(dispatchProgress);
					};

					let stopRender = ( ) => {
						render = false;
					};

					let handleStateChangeStart = ( ) => {

						// copy HTML from current ui view,
						// hide UI view,
						// show placeholder

						let el = getPlaceholderElForState(getStateValue()),
							view = element.find('ui-view')[0];

						el.classList.add('initialized');
						el.innerHTML = view.innerHTML;

						el.style.display = '';
						view.style.display = 'none';

					};

					let handleStateChangeSuccess = ( initial ) => {

						let el = getPlaceholderElForState(getStateValue()),
							view = element.find('ui-view')[0];

						el.style.display = 'none';

						angular.element(el).after(angular.element(view));

						view.style.display = '';

						setScroller(initial);

					};

					let initScroller = ( ) => {

						let lastX;

						scroller = new FTScroller.FTScroller(
							parent,
							{
								alwaysScroll: false,
								bouncing: false,
								flinging: true,
								scrollbars: false,
								singlePageScrolls: false,
								snapping: true,
								updateOnWindowResize: true,
								updateOnChanges: false,
								scrollingY: false,
								scrollBoundary: X_BOUNDS,
								scrollResponseBoundary: X_BOUNDS,
								disabledInputMethods: { scroll: true },
								maxFlingDuration: 350
							}
						);

						scroller.addEventListener('scrollstart', startRender);

						scroller.addEventListener('scrollend', stopRender);

						scroller.addEventListener('segmentdidchange', ( segment ) => {

							let stateVal = viewReducer.data()[segment.segmentX];

							if (stateVal !== getStateValue()) {

								scope.$evalAsync(( ) => {
									$state.go('objectList', { [indexBy]: stateVal });
								});

							}

						});

						// FTScroller cancels the event if x < X_BOUNDS,
						// which results in vertical scrolls being canceled,
						// so we just stop propagation if the difference

						parent.addEventListener('touchstart', ( event ) => {
							
							lastX = event.touches[0].clientX;

						}, true);

						parent.addEventListener('touchmove', ( event ) => {

							if (Math.abs(lastX - event.touches[0].clientX) < X_BOUNDS) {
								event.stopPropagation();
							}
							
						}, true);

						parent.addEventListener('touchend', ( ) => {

							lastX = NaN;

						});

						let unwatcher = scope.$watch(( ) => $stateParams[indexBy], val => {

							if (val) {
								scrollerDeferred.resolve();
								unwatcher();
							}
						});

						return scrollerDeferred.promise;

					};

					viewReducer = composedReducer( { scope, mode: 'hot' }, ctrl.appConfig)
						.reduce(( config ) => {
							return config.instance.interface_config.objects.map(object => object.objecttype.object_type);
						});

					ctrl.getViewOrder = viewReducer.data;

					$compile(viewPlaceholder)(scope);

					// add placeholders based on views

					composedReducer( { scope, mode: 'hot' }, viewReducer, scrollerDeferred.promise)
						.subscribe(views => {

								let viewContainer = element.find('ui-view').parent()[0];

								each(
									element[0].querySelectorAll('.ftscroller_x,.ftscroller_y'),
									( el ) => {

										el.style.width = `${views.length * 100}%`;
									}
								);

								for (let i = 0, l = viewContainer.childNodes.length; i < l; ++i) {

									let el = viewContainer.childNodes[i];

									if (el.classList && el.classList.contains('view-placeholder')) {
										viewContainer.removeChild(el);
									}

								}

								for (let i = 0, l = views.length; i < l; ++i) {
									let clone = viewPlaceholder.clone();

									clone.attr('view-name', views[i]);
									viewContainer.appendChild(clone[0]);
								}

								scroller.updateDimensions();

						});

					composedReducer( { scope, mode: 'hot' }, createReducerFromScopeEvent(scope, '$stateChangeSuccess'), viewReducer, scrollerDeferred.promise)
						.subscribe(( rest ) => {
							if (isRelevantStateChange(...rest)) {
								
								let initial = !rest[2].name;

								handleStateChangeSuccess(initial);

								if (initial) {
									dispatchProgress();
								}
							}

						});

					scope.$on('$stateChangeStart', ( event, ...rest ) => {
						if (isRelevantStateChange(...rest)) {
							handleStateChangeStart();
						}
					});
					
					scope.$applyAsync(initScroller);

				}],
				controllerAs: 'vm'
			};

		}])
		.name;
