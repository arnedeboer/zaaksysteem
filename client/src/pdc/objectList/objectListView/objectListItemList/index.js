import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import template from './template.html';
import composedReducerModule from '../../../../shared/api/resource/composedReducer';
import objectListItemListItemModule from './objectListItemListItem';
import sessionServiceModule from '../../../../shared/user/sessionService';
import auxiliaryRouteModule from '../../../../shared/util/route/auxiliaryRoute';
import shortid from 'shortid';
import './styles.scss';

export default
		angular.module('Zaaksysteem.pdc.objectListItemList', [
			composedReducerModule,
			objectListItemListItemModule,
			sessionServiceModule,
			auxiliaryRouteModule,
			angularUiRouterModule
		])
		.directive('objectListItemList', [ '$sce', '$state', 'composedReducer', 'auxiliaryRouteService', ( $sce, $state, composedReducer, auxiliaryRouteService ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					objectItems: '&',
					onLoadMore: '&'
				},
				bindToController: true,
				controller: [ '$scope', function ( $scope ) {

					let ctrl = this,
						objectReducer;

					let getDetailState = ( ) => {
						return auxiliaryRouteService.append($state.current, 'objectDetail');
					};

					objectReducer = composedReducer( { scope: $scope }, ctrl.objectItems, getDetailState )
						.reduce( ( objects, detailState ) => {

							return objects.map( ( object => {

								return {
									$id: shortid(),
									label: object.label,
									link: $state.href(detailState, { objectID:  object.id })
								};

							}));

						});

					ctrl.getItems = objectReducer.data;

				}],
				controllerAs: 'vm'
			};

		}
		])
		.name;
