import angular from 'angular';
import template from './template.html';
import composedReducerModule from '../../../shared/api/resource/composedReducer';
import angularUiRouterModule from 'angular-ui-router';
import snackbarServiceModule from '../../../shared/ui/zsSnackbar/snackbarService';
import get from 'lodash/get';
import shortid from 'shortid';
import startsWith from 'lodash/startsWith';
import isArray from 'lodash/isArray';
import isNull from 'lodash/isNull';
import omitBy from 'lodash/omitBy';
import identity from 'lodash/identity';
import './styles.scss';

export default
		angular.module('Zaaksysteem.pdc.objectDetailView', [
			composedReducerModule,
			angularUiRouterModule,
			snackbarServiceModule
		])
		.directive('objectDetailView', [ '$sce', '$state', 'dateFilter', 'composedReducer', ( $sce, $state, dateFilter, composedReducer ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					objectItem: '&',
					objectType: '&',
					relatedCaseTypes: '&',
					appConfig: '&'
				},
				bindToController: true,
				controller: [ '$scope', function ( $scope ) {

					let ctrl = this,
						styleReducer,
						fieldReducer,
						buttonReducer,
						titleReducer;

					titleReducer = composedReducer( { scope: $scope }, ctrl.objectItem() )
						.reduce( ( object ) => {

							return object.label;

						});

					styleReducer = composedReducer( { scope: $scope }, ctrl.appConfig() )
						.reduce( config => {
							return {
								'background-color': get(config, 'instance.interface_config.header_bgcolor', '#FFF')
							};
						});

					fieldReducer = composedReducer( { scope: $scope }, ctrl.objectItem(), ctrl.objectType() )
						.reduce( ( item, type ) => {

							return type.values.attributes
								.asMutable()
								.map( attribute => {

									let label = attribute.label || attribute.attribute_label,
										value = item.values[attribute.name.replace('attribute.', '')] || '-',
										tpl = String(value);

									if (attribute.attribute_type === 'image_from_url') {
										tpl = `<img src="${value}" alt="" />`;
									}

									if (attribute.attribute_type === 'subject') {

										if ( !isArray(value) && value !== '-') {
											value = [value];
											tpl = '';

											tpl = value.map( subject => {
												return `${get(subject, 'voorletters')} ${get(subject, 'naamgebruik')}`;
											}).join(', ');
										}
									}

									if (attribute.attribute_type === 'date') {
										tpl = dateFilter( value, 'dd MMMM yyyy');
									}

									if (attribute.attribute_type === 'url') {
										tpl = `<a class="link" href="${value}" target="_blank">${value}</a>`;
									}

									if (startsWith(attribute.attribute_type, 'valuta')) {
										value = Number(value) ? value.toFixed(2) : value;
										tpl = `&euro; ${value}`;
									}

									if (attribute.attribute_type === 'file') {
										tpl = null;
									}

									if (startsWith(attribute.attribute_type, 'bag_')) {

										if ( !isArray(value) ) {
											value = [value];
											tpl = '';
										}
							
										value = value.map( ( address ) => {
											let addressValue = omitBy(get(address, 'address_data'), isNull);
											
											tpl += `${get(addressValue, 'straat')} ${get(addressValue, 'huisnummer', '')}${get(addressValue, 'huisletter', '')}${get(addressValue, 'huisnummertoevoeging', '')} ${get(addressValue, 'postcode', '')} ${get(addressValue, 'woonplaats', '')}<br />`;
										});

									}

									if (value === '-') {
										tpl = null;
									}

									return tpl !== null ?
										{
											id: shortid(),
											name: attribute.name.replace('attribute.', ''),
											label,
											value,
											template: $sce.trustAsHtml(tpl)
										} : null;

								}).filter(identity);
						});

					buttonReducer = composedReducer( { scope: $scope }, ctrl.relatedCaseTypes() )
						.reduce( ( relatedCaseTypes ) => {
							return relatedCaseTypes.map( casetype => {

								let buttons = [],
									casetypeName = casetype.instance.title,
									buttonOrder = [ 'person', 'organisation', 'unknown' ];

								if (get(casetype, 'instance.settings.public_registration_form')) {

									buttons = buttonOrder.map( key => {

										let label,
											link = get(casetype.instance.public_url_path, key);

										if (key === 'person') {
											label = `${casetypeName} aanvragen als burger`;
										} else if (key === 'organisation') {
											label = `${casetypeName} aanvragen als organisatie`;
										} else if (key === 'unknown') {
											label = `${casetypeName} anoniem aanvragen`;
										}

										return link ? {
											id: shortid(),
											link,
											label
										} : null;

									}).filter(identity);

								}

								return buttons.length ? {
									id: shortid(),
									name: casetype.id,
									label: casetype.label,
									buttons
								} : null;

							}).filter(identity);

						});

					ctrl.getTitle = titleReducer.data;

					ctrl.getFields = fieldReducer.data;

					ctrl.getStyle = styleReducer.data;

					ctrl.getButtons = buttonReducer.data;

					ctrl.goBack = ( ) => {
						$state.go('^');
					};

				}],
				controllerAs: 'objectDetailView'

			};
		}
		])
		.name;
