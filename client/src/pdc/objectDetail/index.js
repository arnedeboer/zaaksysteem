import angular from 'angular';
import resourceModule from '../../shared/api/resource';
import objectDetailViewModule from './objectDetailView';
import snackbarServiceModule from './../../shared/ui/zsSnackbar/snackbarService';
import auxiliaryRouteModule from './../../shared/util/route/auxiliaryRoute';
import onRouteActivateModule from './../../shared/util/route/onRouteActivate';
import zsModalModule from './../../shared/ui/zsModal';
import viewTitleModule from './../../shared/util/route/viewTitle';
import composedReducerModule from '../../shared/api/resource/composedReducer';
import template from './index.html';
import seamlessImmutable from 'seamless-immutable';
import first from 'lodash/first';
import './styles.scss';

export default {

	moduleName:
		angular.module('Zaaksysteem.pdc.objectDetail', [
			resourceModule,
			objectDetailViewModule,
			snackbarServiceModule,
			auxiliaryRouteModule,
			onRouteActivateModule,
			zsModalModule,
			viewTitleModule,
			composedReducerModule
		])
		.name,
	config: [
		{
			route: {
				url: '/:objectID/',
				resolve: {
					objectItem: [ '$rootScope', '$stateParams', '$q', 'resource', 'snackbarService', ( $rootScope, $stateParams, $q, resource, snackbarService ) => {

						let objectItemResource = resource(
							{ url: `/api/object/${$stateParams.objectID}` },
							{ scope: $rootScope }
						)
							.reduce( ( requestOptions, data ) => {

								return ( first(data) || seamlessImmutable({}) );

							});

						return objectItemResource
							.asPromise()
							.then( ( ) => objectItemResource)
							.catch( ( err ) => {

								snackbarService.error('Het object kon niet worden geladen. Neem contact op met uw beheerder voor meer informatie.');

								return $q.reject(err);
							});
					}],
					objectType: [ '$rootScope', '$stateParams', '$q', 'resource', 'snackbarService', ( $rootScope, $stateParams, $q, resource, snackbarService ) => {

						let objectTypeResource = resource(
							{
								url: '/api/object/search',
								params: {
									zql: `SELECT {} FROM type WHERE prefix = "${$stateParams.objectType}"`
								}
							},
							{ scope: $rootScope }
						)
							.reduce( ( requestOptions, data ) => {
								return ( first(data) || seamlessImmutable({}) );
							});

						return objectTypeResource
							.asPromise()
							.then( ( ) => objectTypeResource)
							.catch( ( err ) => {

								snackbarService.error('Het objecttype kon niet worden geladen. Neem contact op met uw beheerder voor meer informatie.');

								return $q.reject(err);
							});

					}],
					relatedCaseTypes: [ '$rootScope', '$stateParams', '$q', 'resource', 'objectItem', 'composedReducer', 'snackbarService', ( $rootScope, $stateParams, $q, resource, objectItem, composedReducer, snackbarService ) => {

						let uuidsReducer = composedReducer( { scope: $rootScope }, objectItem.data )
							.reduce( ( object ) => {

								return object.related_objects.map( casetype => {
									return casetype.related_object_id;
								});

							});

						let relatedCaseTypesResource = resource(
							{
								url: '/api/v1/casetype',
								params: {
									zql: `SELECT {} FROM casetype WHERE object.uuid IN ("${uuidsReducer.data().join('","')}")`,
									zapi_num_rows: 100
								}
							},
							{ scope: $rootScope }
						)
							.reduce( ( requestOptions, data ) => {
								return ( data || seamlessImmutable([]));
							});

						return relatedCaseTypesResource
							.asPromise()
							.then( ( ) => relatedCaseTypesResource)
							.catch( ( err ) => {

								snackbarService.error('De gerelateerde zaaktypes konden niet geladen worden. Neem contact op met uw beheerder voor meer informatie.');

								return $q.reject(err);
							});
					}]
				},
				auxiliary: true,
				onActivate: [ '$state', '$timeout', '$rootScope', '$window', '$document', '$compile', 'viewTitle', '$stateParams', 'zsModal', 'objectItem', 'objectType', 'relatedCaseTypes', 'config', ( $state, $timeout, $rootScope, $window, $document, $compile, viewTitle, $stateParams, zsModal, objectItem, objectType, relatedCaseTypes, config ) => {

					let scrollEl = angular.element($document[0].querySelector('.body-scroll-container'));

					scrollEl.css({ overflow: 'hidden' });

					let openModal = ( ) => {

						let modal,
							unregister,
							scope = $rootScope.$new(true);

						scope.objectItem = objectItem;
						scope.objectType = objectType;
						scope.relatedCaseTypes = relatedCaseTypes;
						scope.config = config;

						modal = zsModal({
							el: $compile(angular.element(template))(scope),
							title: viewTitle.get(),
							classes: 'detail-modal'
						});

						modal.open();

						modal.onClose( ( ) => {
							$window.history.back();
							return true;
						});

						unregister = $rootScope.$on('$stateChangeStart', ( ) => {

							$window.requestAnimationFrame( ( ) => {

								$rootScope.$evalAsync(( ) => {

									if ($state.current.name === 'objectList') {

										modal.close()
											.then(( ) => {

												scope.$destroy();

												scrollEl.css({
													overflow: '',
													height: ''
												});

											});

										unregister();

									}

								});

							});
							
						});

					};

					$window.requestAnimationFrame( ( ) => {
						$rootScope.$evalAsync(openModal);
					});

				}]

			},
			state: 'objectDetail'
		}
	]
};
