var path = require('path'),
	fs = require('fs'),
	swPrecache = require('sw-precache'),
	async = require('async'),
	uniq = require('lodash/uniq'),
	endsWith = require('lodash/endsWith'),
	contains = require('lodash/includes'),
	assign = require('lodash/assign'),
	first = require('lodash/first'),
	flatten = require('lodash/flatten'),
	mapValues = require('lodash/mapValues'),
	indexBy = require('lodash/keyBy');

function ServiceWorkerCachePlugin ( settings ) {

	this.settings = settings;

}

ServiceWorkerCachePlugin.prototype.apply = function ( compiler ) {

	var settings = this.settings,
		apps = settings.entries || [],
		filesPerApp = mapValues(indexBy(apps), ( ) => []),
		root = settings.root || '.',
		publicPath = settings.publicPath || '';

	compiler.plugin('compilation', ( compilation ) => {

		compilation.plugin('module-asset', function ( ) {

			var filename = arguments[1],
				entry = this.entries[0],
				paths = entry.context.split(path.sep),
				appName = first(entry.context.replace(path.join(process.cwd(), 'src'), '').replace(path.sep, '').split(path.sep));

			if (!filesPerApp[appName]) {
				return;
			}

			filesPerApp[appName].push(filename);

		});

	});

	compiler.plugin('after-emit', ( compilation, callback ) => {

		async.parallel(apps.map(( app ) => {

			var chunk = compilation.namedChunks[app],
				chunkId = chunk.id,
				files = chunk.files,
				assets = uniq(
					files
						.concat(filesPerApp[app])
						.concat(
							flatten(
								compilation.chunks.filter(( c ) => {
									return c.entry === false && contains(c.parents.map(parent => parent.id), chunkId);
								})
								.map( c => c.files)
							)
						)
						.filter( filename => !endsWith(filename, '.map'))
				),
				globs = assets.map( ( asset ) => path.join(root, publicPath, asset));

			if (!fs.existsSync(path.join(root, app))) {
				fs.mkdirSync(path.join(root, app), ( err ) => {
					if (err) {
						console.err(err);
					}
				});
			}

			return function ( cb ) {

				globs = globs.concat(
					(settings.globs || [])
						.map(glob => {
							return path.join(root, publicPath, glob);
						})
				);

				swPrecache.write(
					path.join(root, publicPath, app, 'service-worker.js'),
					assign({
						staticFileGlobs: globs,
						stripPrefix: root.replace(path.sep, '/'),
						maximumFileSizeToCacheInBytes: 8 * 1024 * 1024
					}, settings.options),
				cb);
			};

		}), callback);

	});

};

module.exports = ServiceWorkerCachePlugin;
