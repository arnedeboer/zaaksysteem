#! /usr/bin/perl
use TestSetup;
initialize_test_globals_ok;

use Test::WWW::Mechanize::Catalyst;
ensure_test_server_ok();

sub confabulate {
    my ($title) = @_;

    $title =~ s| +|-|gis;
    $title = lc($title);
}

sub check_link {
    my ($mech, $links, $match) = @_;

    # check person link
    my ($link) = grep { $_->url =~ m|/$match/| } @$links;

    ok $link, "Found link for $match";

    $mech->get($link->url);#, 'Person link retrieved succesfully');

    is $mech->status, 302, "$match link yielded a redirect";
    ok $mech->response->header('location') =~ m|zaak/create/webformulier|,
        "$match link redirected to zaak/create url";
}


my $oldskool_title = 'Ik ben een MIXED case TITEL';
my $zaaktype_definitie = $zs->create_zaaktype_definitie_ok(
    preset_client => 'betrokkene-natuurlijk_persoon-1'
);
my $zaaktype_node = $zs->create_zaaktype_node_ok(
    zaaktype_definitie => $zaaktype_definitie,
    webform_toegang => 1,
    titel => $oldskool_title,
);

my $casetype = $zs->create_zaaktype_ok(node => $zaaktype_node);
my $title = $casetype->zaaktype_node_id->titel;

my %SUBJECT_TYPES = (
    person => 'persoon',
    organisation => 'organisatie',
    unknown => 'onbekend'
);

my $public_url = $casetype->TO_JSON->{public_url};

my $seo_friendly_title = $casetype->seo_friendly_title;
ok $seo_friendly_title =~ m|^[a-z0-9-]+$|, 'Seo friendly title contains only a-z, 0-9 and dashes';


for my $subject_type (keys %SUBJECT_TYPES) {

    my $result = $public_url->{$subject_type};

    my $expected = '/aanvragen/' . $casetype->id . '/' . $SUBJECT_TYPES{$subject_type} . '/';

    ok $result && $result =~ m|^$expected|, 'Generated url starts with expected payload';

    my ($seo_friendly_title) = $result =~ m|$expected(.*)$|;
    ok $seo_friendly_title =~ m|^[a-z0-9-]+$|, 'Seo friendly title contains only a-z, 0-9 and dashes';
}

ok !$schema->resultset('Zaaktype')->find_by_lowercase_title($title), "Not found casetype using uppercase legacy search";
ok $schema->resultset('Zaaktype')->find_by_lowercase_title(lc($title)), "Found casetype using legacy search";


### TODO: Needs to be moved to Test::Class
#
# my $mech = Test::WWW::Mechanize::Catalyst->new(catalyst_app => 'Zaaksysteem');
# $mech->{catalyst_debug} = 1;
# $mech->get_ok('/form/list', '/form/list available without logging in');

# $mech->requests_redirectable([]);

# diag('mech -> Find_all_linkgs');

# my @links = $mech->find_all_links();#url_regex => qr|aanvragen| );

# for my $match (qw/persoon organisatie onbekend/) {
#     check_link($mech, \@links, $match);
# }

# # check oldskool urls
# $mech->get('/aanvraag/' . confabulate($title) . '/niet_natuurlijk_persoon');
# is $mech->status, 302, "Oldskool link yielded a redirect";
# ok $mech->response->header('location') =~ m|zaak/create/webformulier|,
#     "Oldskool link redirected to zaak/create url";

# $casetype->delete;

zs_done_testing();
