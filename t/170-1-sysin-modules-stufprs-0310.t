#! perl

### This TEST script tests the default CSV implementation

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
use File::Spec::Functions qw(catfile);
use constant XML_PATH => 't/inc/API/StUF/0310/prs/';

use Zaaksysteem::Backend::Sysin::Modules::STUFPRS;

sub create_config_interface {
    my $params                  = shift || {};

    return $zs->create_named_interface_ok(
        {
            module              => 'stufconfig',
            name                => 'STUF PRS Parsing',
            interface_config    => {
                gemeentecode    => '363',
                mk_async_url    => 'http://localhost:3331/stuf',
                mk_sync_url     => 'http://localhost:3331/stuf',
                mk_ontvanger    => 'TESM',
                stuf_supplier   => 'centric',
                %{ $params }
            }
        },
    );
}

sub _create_stuf_interface {
    create_config_interface();

    $zs->create_named_interface_ok(
        {
            module          => 'stufnps',
            name            => 'STUF NPS Parsing',
        },
    );
}


$zs->zs_transaction_ok(sub {
    create_config_interface();

    $zs->create_bag_woonplaats_ok(naam => 'Amsterdam');

    my $VALIDATION_MAP = {
        'NatuurlijkPersoon'     => {
            'a_nummer'                  => '1234567890',
            'burgerservicenummer'       => '987654321',
            'voornamen'                 => 'Tinus',
            'voorletters'               => 'T',
            'geslachtsnaam'             => 'Testpersoon',
            'geslachtsaanduiding'       => 'M',
        },
        'Adres'                 => {
            'postcode'                  => '1015JL',
            'woonplaats'                => 'Amsterdam',
            'straatnaam'                => 'Donker Curtiusstraat',
            'huisnummer'                => '7',
            'huisnummertoevoeging'      => '521',
        }
    };

    my $interface = $zs->create_named_interface_ok(
        {
            module          => 'stufnps',
            name            => 'STUF PRS Parsing',
        },
    );

    ### Last created object_subscription
    my $transaction = $interface->process({
        input_data              => $zs->get_file_contents_as_string(
                                    catfile(XML_PATH, '101-prs-create-tinus.xml')
                                ),
    });

    ok($transaction, 'PRS Transaction completed');
    ok(!$transaction->error_count, 'PRS Transaction: no errors');
    is($transaction->success_count, 1, 'PRS Transaction: 1 success');
    is($transaction->external_transaction_id, 'MK0000229154', 'PRS Transaction: external transaction id');
    is($transaction->automated_retry_count, undef, 'PRS Transaction: automated retry count');
    ok($transaction->date_created, 'PRS Transaction: date created');
    ok(!$transaction->date_next_retry, 'PRS Transaction: no date next_retry');
    ok($transaction->date_last_retry, 'PRS Transaction: date last retry');


    is($transaction->records->count, 1, 'Got single transaction record');

    my $record      = $transaction->records->first;

    is($record->transaction_record_to_objects, 2, 'Got two mutations');

    my $npms        = $record->transaction_record_to_objects->search(
        {
            'local_table'           => 'NatuurlijkPersoon',
            'transaction_record_id' => $record->id,
        }
    );

    is($npms->count, 1, 'Got single NP mutation record');

    my $npm         = $npms->first;

    my $np          = $schema->resultset('NatuurlijkPersoon')->find(
        $npm->local_id
    );

    ok($np->authenticated, 'Record is authenticated');
    is($np->authenticatedby, 'gba', 'Record is authenticated by GBA');

    is(
        $schema->resultset('ObjectSubscription')->search(
            {
                'local_table'   => 'NatuurlijkPersoon',
                'local_id'      => $np->id,
            }
        )->count,
        1,
        'Got single subscription'
    );

    for my $key (keys %{ $VALIDATION_MAP->{NatuurlijkPersoon} }) {
        my $givenvalue = $np->$key;
        my $wantedvalue = $VALIDATION_MAP->{NatuurlijkPersoon}->{ $key };

        is($givenvalue, $wantedvalue, 'Correct value for: ' . $key);
    }

    for my $key (keys %{ $VALIDATION_MAP->{Adres} }) {
        my $givenvalue = $np->adres_id->$key;
        my $wantedvalue = $VALIDATION_MAP->{Adres}->{ $key };

        is($givenvalue, $wantedvalue, 'Correct value for: ' . $key);
    }

    ok($np->in_gemeente, 'in_gemeente: true - binnen gemeentelijk');


}, 'Tested: Create single PRS mutation');

$zs->zs_transaction_ok(sub {
    my $interface   = _create_stuf_interface();

    ### Last created object_subscription
    my $transaction = $interface->process({
        input_data              => $zs->get_file_contents_as_string(
                                    catfile(XML_PATH, '107-prs-create-tinus-foreign.xml')
                                ),
    });

    ok(!$transaction->error_count, 'PRS Transaction: no errors');
    is($transaction->success_count, 1, 'PRS Transaction: 1 success');
    is($transaction->records->count, 1, 'Got single transaction record');

    my $record      = $transaction->records->first;
    is($record->transaction_record_to_objects, 2, 'Got two mutations');

    my $npm        = $record->transaction_record_to_objects->search(
        {
            'local_table'           => 'NatuurlijkPersoon',
            'transaction_record_id' => $record->id,
        }
    )->first;
    my $np          = $schema->resultset('NatuurlijkPersoon')->find(
        $npm->local_id
    );

    ok($np->authenticated, 'Record is authenticated');
    is($np->authenticatedby, 'gba', 'Record is authenticated by GBA');

    is(
        $schema->resultset('ObjectSubscription')->search(
            {
                'local_table'   => 'NatuurlijkPersoon',
                'local_id'      => $np->id,
            }
        )->count,
        1,
        'Got single subscription'
    );

    is($np->adres_id->landcode, 5010, 'Got correct landcode for foreigner');

    ok(!$np->in_gemeente, 'in_gemeente: false - buiten gemeentelijk persoon');

    is($np->adres_id->adres_buitenland1, 'Teststraat 6B', 'Found adres_buitenland1');
    is($np->adres_id->adres_buitenland2, 'Bilzen', 'Found adres_buitenland2');
    is($np->adres_id->adres_buitenland3, 'Foreigncountry', 'Found adres_buitenland3');

}, 'Tested: Create single PRS FOREIGNER mutation');


$zs->zs_transaction_ok(sub {
    create_config_interface();

    my $interface = $zs->create_named_interface_ok(
        {
            module          => 'stufnps',
            name            => 'STUF PRS Parsing',
        },
    );

    ### Last created object_subscription
    my $transaction = $interface->process({
        input_data              => $zs->get_file_contents_as_string(
                                    catfile(XML_PATH, '101-prs-create-tinus.xml')
                                ),
    });

    ok($transaction, 'PRS Transaction completed');
    ok(!$transaction->error_count, 'PRS Transaction: no errors');
    is($transaction->success_count, 1, 'PRS Transaction: 1 success');

    my $record      = $transaction->records->first;

    my $npms        = $record->transaction_record_to_objects->search(
        {
            'local_table'           => 'NatuurlijkPersoon',
            'transaction_record_id' => $record->id,
        }
    );

    is($npms->count, 1, 'Got single NP mutation record');

    my $npm         = $npms->first;

    my $np          = $schema->resultset('NatuurlijkPersoon')->find(
        $npm->local_id
    );

    is($np->partner_geslachtsnaam, 'TestpartnernaamGOOD', 'Found correct partner');
    is($np->aanduiding_naamgebruik, 'P', 'Found correct naamgebruik');


}, 'Tested: Create single PRS mutation with partner');

$zs->zs_transaction_ok(sub {
    create_config_interface();

    my $interface = $zs->create_named_interface_ok(
        {
            module          => 'stufnps',
            name            => 'STUF PRS Parsing',
        },
    );

    my $transaction = $interface->process({
        input_data              => $zs->get_file_contents_as_string(
                                    catfile(XML_PATH, '121-prs-update-huwelijk.xml')
                                ),
    });

    like($transaction->records->first->output, qr/no_entry_found/, 'Missing subscription');
}, 'Tested: PRS Update Mutation, missing object subscription');

$zs->zs_transaction_ok(
    sub {
        create_config_interface();

        my $VALIDATION_MAP = {
            'NatuurlijkPersoon' => {
                'a_nummer'            => '1234567890',
                'burgerservicenummer' => '987654321',
                'voornamen'           => 'Minus',
                'voorletters'         => 'M',
                'geslachtsnaam'       => 'Mestpersoon',
                'geslachtsaanduiding' => 'M',
            },
            'Adres' => {
                'postcode'             => '1015JL',
                'woonplaats'           => 'Amsterdam',
                'straatnaam'           => 'Donker Curtiusstraat',
                'huisnummer'           => '70',
                'huisnummertoevoeging' => undef,
            }
        };

        my $interface = $zs->create_named_interface_ok(
            {
                module => 'stufnps',
                name   => 'STUF PRS Parsing',
            },
        );

        my $transaction = $interface->process(
            {
                input_data => $zs->get_file_contents_as_string(
                    catfile(XML_PATH, '101-prs-create-tinus.xml')
                ),
            }
        );

        sub get_np_from_transaction {
            my $t      = shift;
            ok($t->success_count, 'Succesful transaction');

            my $record = $t->records->first;
            ok($record, "Got a transaction_record");

            my $rs = $record->transaction_record_to_objects;
            ok($rs->count, "has transaction_record_to_objects");

            my $mutation = $record->transaction_record_to_objects(
                { 'local_table' => 'NatuurlijkPersoon', })->first;
            ok($mutation, 'Found NatuurlijkPersoon mutation');

            my $np = $schema->resultset('NatuurlijkPersoon')
                ->find($mutation->local_id);
            isa_ok($np, "Zaaksysteem::Model::DB::NatuurlijkPersoon");

            note(sprintf("Transaction ID %d has NatuurlijkPersoon %s", $t->id, $np->id));

            return $np;
        }

        my $np = get_np_from_transaction($transaction);
        my $second_transaction = $interface->process(
            {
                input_data => $zs->get_file_contents_as_string(
                    catfile(XML_PATH, '121-prs-update-huwelijk.xml')
                ),
            }
        );

        $np = get_np_from_transaction($second_transaction);

        for my $key (keys %{ $VALIDATION_MAP->{NatuurlijkPersoon} }) {
            my $givenvalue  = $np->$key;
            my $wantedvalue = $VALIDATION_MAP->{NatuurlijkPersoon}->{$key};

            is($givenvalue, $wantedvalue, 'Correct value for: ' . $key);
        }

        for my $key (keys %{ $VALIDATION_MAP->{Adres} }) {
            my $givenvalue  = $np->adres_id->$key;
            my $wantedvalue = $VALIDATION_MAP->{Adres}->{$key};

            is($givenvalue, $wantedvalue, 'Correct value for: ' . $key);
        }

    },
    'Tested: PRS Update Mutation'
);



$zs->zs_transaction_ok(sub {
    create_config_interface();

    my $interface = $zs->create_named_interface_ok(
        {
            module          => 'stufnps',
            name            => 'STUF PRS Parsing',
        },
    );

    ###
    ### CREATE FIRST ENTRY
    ###
    my $transaction = $interface->process({
        input_data              => $zs->get_file_contents_as_string(
                                    catfile(XML_PATH, '101-prs-create-tinus.xml')
                                ),
    });

    my $record     = $transaction->records->first;

    is(
        $schema->resultset('ObjectSubscription')->search_active->count,
        1,
        'Single object subscription'
    );

    my $mutation   = $record->transaction_record_to_objects->search(
        {
            'local_table' => 'NatuurlijkPersoon',
            'transaction_record_id' => $record->id,
        }
    )->first;

    ok($mutation, 'Found NatuurlijkPersoon mutation');

    ###
    ### DELETE CREATED ENTRY
    ###
    my $second_transaction = $interface->process({
        input_data              => $zs->get_file_contents_as_string(
                                    catfile(XML_PATH, '131-prs-delete-tinus.xml')
                                ),
    });

    my $np          = $schema->resultset('NatuurlijkPersoon')->find(
        $mutation->local_id
    );

    ok(!$second_transaction->error_count, 'Succesfully deleted PRS entry');
    ok($np->deleted_on, 'NatuurlijkPersoon is deleted');
    ok($np->adres_id->deleted_on, 'NatuurlijkPersoon->Adres is deleted');

    ok(
        !$schema->resultset('ObjectSubscription')->search_active->count,
        'Object subscription removed'
    );
}, 'Tested: PRS Delete');

$zs->zs_transaction_ok(sub {
    create_config_interface();

    my $interface = $zs->create_named_interface_ok(
        {
            module          => 'stufnps',
            name            => 'STUF PRS Parsing',
        },
    );

    my $inserted_transaction = $interface->process({
        input_data              => $zs->get_file_contents_as_string(
                                    catfile(XML_PATH, '101-prs-create-tinus.xml')
                                ),
    });

    my $transaction = $interface->process_trigger(
        'disable_subscription',
        {
            subscription_id => $interface->object_subscriptions->first->id,
        }
    );

    #note(explain({ $transaction->get_columns }));

}, 'Tested: PRS remove afnemerindicatie');

$zs->zs_transaction_ok(sub {
    create_config_interface({
        synchronization_type   => 'hybrid',
    });

    my $interface = $zs->create_named_interface_ok(
        {
            module          => 'stufnps',
            name            => 'STUF PRS Parsing',
            interface_config    => {
                mk_async_url    => 'http://localhost/async',
                mk_sync_url     => 'http://localhost/sync',
                mk_ontvanger    => 'TESM',
            }
        },
    );

    my $inserted_transaction = $interface->process({
        input_data              => $zs->get_file_contents_as_string(
                                    catfile(XML_PATH, '101-prs-create-tinus.xml')
                                ),
    });

    my $transaction = $interface->process_trigger(
        'disable_subscription',
        {
            subscription_id => $interface->object_subscriptions->first->id,
        }
    );

    my $now         = DateTime->now();

    ok($transaction->error_count, 'Got transaction errors');
    ok(
        $transaction->date_last_retry,
        'Got last retry: ' . $transaction->date_last_retry->hms
    );

    ok(
        $transaction->date_last_retry <= $now,
        'Last retry in the past'
    );
    ok(
        $transaction->date_next_retry,
        'Got next retry: ' . $transaction->date_next_retry->hms
    );
    ok(
        $transaction->date_next_retry > $now,
        'Last retry in the future'
    );
    is(
        $transaction->automated_retry_count, 1, 'Automated retry count set to 1'
    );

    ### Reset last retry to now
    $transaction->date_next_retry(DateTime->now());
    $transaction->update;

    ok(sleep(1), 'Slept for 1 second');

    my $date_next_retry = $transaction->date_next_retry;

    ok(
        $schema->resultset('Transaction')->process_pending,
        'Processed transactions pending'
    );

    ### Get new transaction id
    my $new_transaction     = $schema->resultset('Transaction')->find(
        $transaction->id
    );

    ok(
        $new_transaction->date_last_retry > $date_next_retry,
        'Transaction reprocessed, new retry date: ' . $new_transaction->date_next_retry->hms
    );
    is(
        $new_transaction->automated_retry_count, 2, 'Automated retry count set to 2'
    );

}, 'Tested: PRS remove afnemerindicatie RETRY');

# SKIP: {
#     skip "Set ENVIRONMENT VARIABLE: WITH_STUF_SERVER=1 to test", 2 unless $ENV{WITH_STUF_SERVER};

#     $zs->zs_transaction_ok(sub {
#         create_config_interface();

#         my $interface   = $zs->create_named_interface_ok(
#             {
#                 module              => 'stufnps',
#                 name                => 'STUF PRS Parsing',
#             },
#         );

#         my $stufnps     = $schema
#                         ->resultset('Interface')
#                         ->find_by_module_name('stufnps');

#         my $entries     = $stufnps->process_trigger(
#             'search',
#             {
#                 'bsn-nummer'        => '987654321',
#             }
#         );

#     }, 'Tested: Search via PRS');



#     $zs->zs_transaction_ok(sub {
#         create_config_interface();

#         my $interface   = $zs->create_named_interface_ok(
#             {
#                 module              => 'stufnps',
#                 name                => 'STUF PRS Parsing',
#             },
#         );

#         my $stufnps     = $schema
#                         ->resultset('Interface')
#                         ->find_by_module_name('stufnps');

#         my $transaction = $stufnps->process_trigger(
#             'import',
#             {
#                 'sleutelGegevensbeheer'        => '2000000',
#             }
#         );

#         my $record      = $transaction->records->first;

#         my $npm         = $record->transaction_record_to_objects->search(
#             {
#                 'local_table'           => 'NatuurlijkPersoon',
#                 'transaction_record_id' => $record->id,
#             }
#         )->first;

#         my $np          = $schema->resultset('NatuurlijkPersoon')->find(
#             $npm->local_id
#         );

#         my $subscription    = $schema
#                             ->resultset('ObjectSubscription')
#                             ->search({
#                                 'local_table'   => 'NatuurlijkPersoon',
#                                 'local_id'      => $np->id,
#                             })->first;
#         is($subscription->external_id, '2000000', 'Correct sleutelGegevensbeheer');

#         #note(explain($entries));
#     }, 'Tested: Import PRS');
# };

$zs->zs_transaction_ok(sub {
    create_config_interface();

    my $interface = $zs->create_named_interface_ok(
        {
            module          => 'stufnps',
            name            => 'STUF PRS Parsing',
        },
    );

    ###
    ### CREATE FIRST ENTRY
    ###
    my $transaction = $interface->process({
        input_data              => $zs->get_file_contents_as_string(
                                    catfile(XML_PATH, '101-prs-create-tinus.xml')
                                ),
    });


    ###
    ### DECEASE CREATED ENTRY
    ###

    my $second_transaction = $interface->process({
        input_data              => $zs->get_file_contents_as_string(
                                    catfile(XML_PATH, '141-prs-decease-tinus.xml')
                                ),
    });

    my $record     = $transaction->records->first;

    my $mutation   = $record->transaction_record_to_objects->search(
        {
            'local_table' => 'NatuurlijkPersoon',
            'transaction_record_id' => $record->id,
        }
    )->first;

    ok($mutation, 'Found NatuurlijkPersoon mutation');

    my $np          = $schema->resultset('NatuurlijkPersoon')->find(
        $mutation->local_id
    );

    ok($np->deleted_on, 'Person deleted');
    ok($np->datum_overlijden, 'Got overlijdensdate: ' . $np->datum_overlijden);


    ### Process last transaction
    my $subscription_delete = $schema->resultset('Transaction')->search(
        {},{order_by => { '-desc' => 'id' } }
    )->first;

    #$subscription_delete->process;
    #note(explain({ $subscription_delete->get_columns }));

}, 'Tested: Decease person');

$zs->zs_transaction_ok(sub {
    create_config_interface({
        automatic_desubscription_moved_prs => 1,
    });

    my $interface = $zs->create_named_interface_ok(
        {
            module          => 'stufnps',
            name            => 'STUF PRS Parsing',
        },
    );

    ###
    ### CREATE FIRST ENTRY
    ###
    my $transaction = $interface->process({
        input_data              => $zs->get_file_contents_as_string(
                                    catfile(XML_PATH, '101-prs-create-tinus.xml')
                                ),
    });


    ###
    ### DECEASE CREATED ENTRY
    ###

    my $second_transaction = $interface->process({
        input_data              => $zs->get_file_contents_as_string(
                                    catfile(XML_PATH, '142-prs-verhuis-tinus.xml')
                                ),
    });

    my $record     = $transaction->records->first;

    my $mutation   = $record->transaction_record_to_objects->search(
        {
            'local_table' => 'NatuurlijkPersoon',
            'transaction_record_id' => $record->id,
        }
    )->first;

    ok($mutation, 'Found NatuurlijkPersoon mutation');

    my $np          = $schema->resultset('NatuurlijkPersoon')->find(
        $mutation->local_id
    );

    ok(!$np->in_gemeente, 'in_gemeente: false - buiten gemeentelijk');

    ### Process last transaction
    my $subscription_delete = $schema->resultset('Transaction')->search(
        {},{order_by => { '-desc' => 'id' } }
    )->first;

    #$subscription_delete->process;
    #note(explain({ $subscription_delete->get_columns }));

}, 'Tested: Move (verhuis) person');

$zs->zs_transaction_ok(sub {
    create_config_interface({
        automatic_desubscription_moved_prs => 1,
    });

    $schema->resultset('BagWoonplaats')->create({
        'identificatie'     => '0360',
        'begindatum'        => '20140101',
        'naam'              => 'Amsterdam',
        'status'            => 'Aangewezen',
        'inonderzoek'       => 'N',
    });

    my $interface = $zs->create_named_interface_ok(
        {
            module          => 'stufnps',
            name            => 'STUF PRS Parsing',
        },
    );

    ###
    ### CREATE FIRST ENTRY
    ###
    my $transaction = $interface->process({
        input_data              => $zs->get_file_contents_as_string(
                                    catfile(XML_PATH, '101-prs-create-tinus.xml')
                                ),
    });

    my $second_transaction = $interface->process({
        input_data              => $zs->get_file_contents_as_string(
                                    catfile(XML_PATH, '142-prs-verhuis-tinus.xml')
                                ),
    });

    my $record     = $transaction->records->first;

    my $mutation   = $record->transaction_record_to_objects->search(
        {
            'local_table' => 'NatuurlijkPersoon',
            'transaction_record_id' => $record->id,
        }
    )->first;

    ok($mutation, 'Found NatuurlijkPersoon mutation');

    my $np          = $schema->resultset('NatuurlijkPersoon')->find(
        $mutation->local_id
    );

    ok(!$np->deleted_on, 'Person not deleted');
    #ok($np->datum_overlijden, 'Got overlijdensdate: ' . $np->datum_overlijden);

    #$subscription_delete->process;
    #note(explain({ $subscription_delete->get_columns }));

}, 'Tested: Move (verhuis) person binnen eigen kernen');

$zs->zs_transaction_ok(sub {
    create_config_interface();

    my $interface = $zs->create_named_interface_ok(
        {
            module          => 'stufnps',
            name            => 'STUF PRS Parsing',
        },
    );

    ### Last created object_subscription
    my $transaction = $interface->process({
        input_data              => $zs->get_file_contents_as_string(
                                    catfile(XML_PATH, '106-prs-bug-large_number_of_voorletters-zs-3259.xml')
                                ),
    });

    my $record      = $transaction->records->first;

    my $npms        = $record->transaction_record_to_objects->search(
        {
            'local_table'           => 'NatuurlijkPersoon',
            'transaction_record_id' => $record->id,
        }
    );

    my $npm         = $npms->first;

    my $np          = $schema->resultset('NatuurlijkPersoon')->find(
        $npm->local_id
    );

    is($np->voorletters, 'T.A.B.C.D.E.F.G.H.I.', 'Found voorletters in DB: T.A.B.C.D.E.F.G.H.I.');

}, 'ZS-3259 Person with large amount of voorletters');

zs_done_testing();
