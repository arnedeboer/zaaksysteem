#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
### Test header end

use Zaaksysteem::Betrokkene::Object::NatuurlijkPersoon;

$zs->zs_transaction_ok(
    sub {

        my $dispatch_options = {
            dbic => $schema,
        };

        my $np_info = {
            'np-voornamen'           => 'Anton Ano',
            'np-geslachtsnaam'       => 'Zaaksysteem',
            'np-huisnummer'          => 42,
            'np-postcode'            => '1011PZ',
            'np-straatnaam'          => 'Muiderstraat',
            'np-woonplaats'          => 'Amsterdam',
            'np-geslachtsaanduiding' => 'M',
            'np-voornamen'           => 'Anton Ano',
        };

        my $np_id = Zaaksysteem::Betrokkene::Object::NatuurlijkPersoon->create(
            $dispatch_options, $np_info,
        );
        ok($np_id, "Natuurlijk persoon aangemaakt");

        my $np = $zs->get_from_zaaksysteem(
            resultset => 'NatuurlijkPersoon',
            search    => {id => $np_id},
            options   => {rows => 1},
        )->single;
        ok($np, "Found the DB entry");

        is($np->voorletters, "", "Geen voorletters");

        my $adres_id = $np->adres_id->id;
        ok($adres_id, "Adres ID found");

        my $adres = $zs->get_from_zaaksysteem(
            resultset => 'Adres',
            search    => {id => $adres_id},
            options   => {rows => 1},
        )->single;
        ok($adres, "Found address data");


        $np_info->{'np-voorletters'} = "A.A";
        $np_id = Zaaksysteem::Betrokkene::Object::NatuurlijkPersoon->create(
            $dispatch_options, $np_info,
        );

        $np = $zs->get_from_zaaksysteem(
            resultset => 'NatuurlijkPersoon',
            search    => {id => $np_id},
            options   => {rows => 1},
        )->single;
        is($np->voorletters, "A.A", "Wel voorletters");


    },
    'Natuurlijk Persoon create tested',
);

zs_done_testing();
