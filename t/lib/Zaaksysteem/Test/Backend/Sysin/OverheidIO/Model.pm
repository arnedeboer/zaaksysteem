package Zaaksysteem::Test::Backend::Sysin::OverheidIO::Model;

=head1 NAME

Zaaksysteem::Test::Backend::Sysin::OverheidIO::Model - Test OverheidIO model

=head2 SYNOPSIS

    prove -l :: Zaaksysteem::Test::Backend::Sysin::OverheidIO::Model

=cut

use Zaaksysteem::Test;
use Zaaksysteem::Backend::Sysin::OverheidIO::Model;
use URI;
use IO::All;

sub test_setup {
    my $test = shift;

    my $test_method = $test->test_report->current_method->name;

    if ('test_overheid_io_live' eq $test_method
        && !$ENV{ZS_OVERHEID_IO_API_KEY})
    {
        $test->test_skip("Skipping $test_method: set ZS_OVERHEID_IO_API_KEY");
    }
}

sub _create_overheid_io_ok {
    my $options = {@_};
    my $model   = Zaaksysteem::Backend::Sysin::OverheidIO::Model->new(
        base_uri => URI->new(
            $options->{base_uri}
                // 'https://overheidio.zaaksysteem.nl/api/kvk'
        ),
        key => $options->{key} // 'zs-testsuite',
    );
    isa_ok($model, 'Zaaksysteem::Backend::Sysin::OverheidIO::Model');
    return $model;
}

sub test_overheid_io_model {
    my $oio = Zaaksysteem::Backend::Sysin::OverheidIO::Model->new(
        type => 'kvk',
        key  => 'foo',
    );

    my $base_uri = $oio->base_uri;
    is("$base_uri", "https://overheid.io/api/kvk", "Correct base URI");
}

sub test_overheid_io_live {

    my $oio = _create_overheid_io_ok(
        base_uri => 'https://overheid.io/api/kvk/',
        key      => $ENV{ZS_OVERHEID_IO_API_KEY},
    );

    {
        my $answer        = $oio->search("Euronet Com*");
        my $company       = $answer->{_embedded}{rechtspersoon}[0];
        my $expected_data = {
            subdossiernummer     => "0000",
            handelsnaam          => "Euronet Communications B.V.",
            vestigingsnummer     => "15999696",
            dossiernummer        => "33301540",
            huisnummer           => "21",
            huisnummertoevoeging => "",
            plaats               => "Hilversum",
            postcode             => "1211RH",
            straat               => "Wilhelminastraat",
            _links => { self => { href => "/api/kvk/33301540/0000" }, },
        };

        is_deeply($company, $expected_data,
            "EuroNet Communications found via wild card search");
    }

    {
        my $answer
            = $oio->search("Euronet Com*", filters => { huisnummer => 21 });
        is($answer->{totalItemCount}, 1, "One company found");
    }

    {
        my $answer
            = $oio->search("Euronet Com*", filter => { huisnummer => 22 });
        is($answer->{totalItemCount}, 0, "No company found, filter denies");
    }

    {
        my $answer = $oio->search("Euronet Com");
        is($answer->{totalItemCount}, 0, "No company found, exact search");
    }

    {
        my $answer = $oio->search(
            "51902672",
            filter => {
                dossiernummer    => "51902672",
                vestigingsnummer => "21881022",
            }
        );
        my $company       = $answer->{_embedded}{rechtspersoon}[0];
        my $expected_data = {
            subdossiernummer     => "0000",
            handelsnaam          => "Mintlab B.V.",
            vestigingsnummer     => "21881022",
            dossiernummer        => "51902672",
            huisnummer           => "7",
            huisnummertoevoeging => "UNIT 521-522",
            plaats               => "Amsterdam",
            postcode             => "1051JL",
            straat               => "Donker Curtiusstraat",
            _links => { self => { href => "/api/kvk/51902672/0000" }, },
        };

        if (
            !is_deeply(
                $company, $expected_data,
                "Mintlab found with default filters"
            )
            )
        {
            diag explain [$company, $expected_data];
        }
    }
    {
        my $answer = $oio->search(
            undef,
            filter => {
                dossiernummer    => "51902672",
                vestigingsnummer => "21881022",
            }
        );
        my $company       = $answer->{_embedded}{rechtspersoon}[0];
        my $expected_data = {
            subdossiernummer     => "0000",
            handelsnaam          => "Mintlab B.V.",
            vestigingsnummer     => "21881022",
            dossiernummer        => "51902672",
            huisnummer           => "7",
            huisnummertoevoeging => "UNIT 521-522",
            plaats               => "Amsterdam",
            postcode             => "1051JL",
            straat               => "Donker Curtiusstraat",
            _links => { self => { href => "/api/kvk/51902672/0000" }, },
        };

        is_deeply($company, $expected_data,
            "Mintlab found by dossier/vestigingsnummer filters");
    }

}

sub _overheid_io {

    my $oio = _create_overheid_io_ok();
    no warnings qw(redefine once);
    use HTTP::Response;
    my $answer = io->catfile('t/data/overheid.io/search.json')->slurp;
    local *LWP::UserAgent::request = sub {
        my $self = shift;
        return HTTP::Response->new(200, undef, undef, $answer);
    };
    use warnings;

    $answer = $oio->search("foo");

    my $company = $answer->{_embedded}{rechtspersoon}[0];

    my $expected_data = {
        subdossiernummer => "0000",
        handelsnaam      => "Euronet Communications B.V.",
        vestigingsnummer => "15999696",
        dossiernummer    => "33301540",
        _links           => { self => { href => "/api/kvk/33301540/0000" }, },
    };

    is_deeply($company, $expected_data, "EuroNet Communications found");
}

sub test_overheid_io_uri {
    my $oio = _create_overheid_io_ok();

    _test_overheid_io_uri_query("Simple search", $oio, "foo");
    _test_overheid_io_uri_query("Filter search",
        $oio, "foo", filter => { foo => 'bar' });

}

sub _test_overheid_io_uri_query {
    my ($test_name, $model, $search, %params) = @_;

    my $calling_uri;
    no warnings qw(redefine once);
    local *Zaaksysteem::Backend::Sysin::OverheidIO::Model::_call_overheid_io = sub {
        my $self = shift;
        $calling_uri = shift;
    };
    use warnings;

    $model->search($search, %params);

    my @query = $calling_uri->query_form;
    my @expected_query = ("size" => 30,);

    $params{filter}{actief} = "true";

    foreach (keys %{ $params{filter} }) {
        push(@expected_query, "filters[$_]" => $params{filter}{$_});
    }

    foreach (@{ $model->fieldnames }) {
        push(@expected_query, 'fields[]' => $_);
    }

    foreach (@{ $model->queryfields }) {
        push(@expected_query, 'queryfields[]' => $_);
    }
    push(@expected_query, query => "foo");

    @query          = sort { $a cmp $b } @query;
    @expected_query = sort { $a cmp $b } @expected_query;

    is_deeply(\@query, \@expected_query, "Correct query params: $test_name");
    is($calling_uri->path, "/api/kvk", "API path is correct: $test_name");
}

1;

__END__

=head1 NAME

Zaaksysteem::Test::Backend::Sysin::OverheidIO::Model - A OverheidIO test package

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
