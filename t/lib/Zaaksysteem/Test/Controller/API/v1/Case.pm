package Zaaksysteem::Test::Controller::API::v1::Case;

use Zaaksysteem::Test;

use Zaaksysteem::Controller::API::v1::Case;

sub _mock_controller {
    return qclass(
        -with_new => 1,

        -subclass => ['Zaaksysteem::Controller::API::v1::Case'],

    )->package->new;
}

sub test__create_files_with_metadata {
    my $self    = shift;

    ### Mocking
    my $ctrl    = $self->_mock_controller;
    my $case    = qclass(-implement => 'Zaaksysteem::Schema::Zaak', id => 1337, -with_new => 1)->package->new;
    my $files   = [{
            reference => 'd1cf69a6-a7f8-4816-96df-aed05c51b7f6',
            metadata  => {
                origin              => 'Inkomend',
                description         => 'Besluit van de gemeente over de melding',
                document_category   => 'Besluit',
                trust_level         => 'Zaakvertrouwelijk',
                origin_date         => '2016-05-05T00:00:00'
            },
        },
        {
            reference => '6e9fe213-fad3-42e2-b739-0256330ba6fd',
            metadata  => {
                origin              => 'Uitgaand',
                description         => 'Besluit van de gemeente over de aanvraag',
                document_category   => 'Besluit',
                trust_level         => 'Zaakvertrouwelijk',
                origin_date         => '2016-05-06T00:00:00'
            }
        }
    ];

    my $user    = qclass(-implement => 'Zaaksysteem::Schema::Subject', betrokkene_identifier => 'betrokkene-natuurlijk_persoon-55', -with_new => 1)->package->new;

    my $schema_t = {};
    my $schema  = qclass(
        -implement  => 'Zaaksysteem::Schema',
        -with_new   => 1,
        resultset   => qmeth {
            if ($_[1] eq 'Filestore') {
                return qobj(
                    find => qmeth {
                        $schema_t->{resultset}->{filestore}->{find} //= [];
                        push(@{ $schema_t->{resultset}->{filestore}->{find} }, $_[1]);

                        return qobj(
                            original_name   => 'Testdocument.pdf',
                            id              => (
                                @{ $schema_t->{resultset}->{filestore}->{find} } == 1
                                    ? 'd1cf69a6-a7f8-4816-96df-aed05c51b7f6'
                                    : '6e9fe213-fad3-42e2-b739-0256330ba6fd'
                            ),
                            files => qobj(
                                first   => qmeth { undef }
                            )
                        );
                    },
                ),
            } elsif ($_[1] eq 'File') {
                return qobj(
                    find => qmeth {
                        $schema_t->{resultset}->{file}->{find} //= [];
                        push(@{ $schema_t->{resultset}->{file}->{find} }, $_[1]);

                        return qobj(
                            update_metadata => qmeth {
                                $schema_t->{resultset}->{file}->{update_metadata} //= [];
                                push(@{ $schema_t->{resultset}->{file}->{update_metadata} }, $_[1]);

                                return 1;
                            }
                        )
                    },
                    file_create => qmeth {
                        $schema_t->{resultset}->{file}->{file_create} //= [];
                        push(@{ $schema_t->{resultset}->{file}->{file_create} }, $_[1]);

                        return qobj(
                            update_metadata => qmeth {
                                $schema_t->{resultset}->{file}->{update_metadata} //= [];
                                push(@{ $schema_t->{resultset}->{file}->{update_metadata} }, $_[1]);

                                return 1;
                            }
                        )

                    }
                ),
            }
        },
    )->package->new;

    throws_ok(
        sub { $ctrl->_create_files_with_metadata },
        qr/missing:/,
        'Validation'
    );

    ok($ctrl->_create_files_with_metadata(
        {
            files       => $files,
            user        => $user,
            case        => $case,
            schema      => $schema,
        }
    ), "Succesfully called: _create_files_with_metadata");

    is($schema_t->{resultset}->{file}->{file_create}->[0]->{db_params}->{case_id}, 1337, "file_create: Correct case_id");
    is($schema_t->{resultset}->{file}->{file_create}->[0]->{db_params}->{created_by}, "betrokkene-natuurlijk_persoon-55", "file_create: correct betrokkene");
    is($schema_t->{resultset}->{file}->{file_create}->[0]->{db_params}->{filestore_id}, "d1cf69a6-a7f8-4816-96df-aed05c51b7f6", "file_create: correct filestore_id");

    is($schema_t->{resultset}->{file}->{update_metadata}->[0]->{description}, 'Besluit van de gemeente over de melding', "update_metadata: description");
    is($schema_t->{resultset}->{file}->{update_metadata}->[0]->{document_category}, 'Besluit', "update_metadata: document_category");
    is($schema_t->{resultset}->{file}->{update_metadata}->[0]->{origin}, "Inkomend", "update_metadata: origin");
    is($schema_t->{resultset}->{file}->{update_metadata}->[0]->{origin_date}, "2016-05-05", "update_metadata: origin_date");
    is($schema_t->{resultset}->{file}->{update_metadata}->[0]->{trust_level}, "Zaakvertrouwelijk", "update_metadata: trust_level");

    is($schema_t->{resultset}->{file}->{update_metadata}->[1]->{description}, 'Besluit van de gemeente over de aanvraag', "update_metadata: description");
    is($schema_t->{resultset}->{file}->{update_metadata}->[1]->{document_category}, 'Besluit', "update_metadata: document_category");
    is($schema_t->{resultset}->{file}->{update_metadata}->[1]->{origin}, "Uitgaand", "update_metadata: origin");
    is($schema_t->{resultset}->{file}->{update_metadata}->[1]->{origin_date}, "2016-05-06", "update_metadata: origin_date");
    is($schema_t->{resultset}->{file}->{update_metadata}->[1]->{trust_level}, "Zaakvertrouwelijk", "update_metadata: trust_level");



}

1;

__END__

=head1 NAME

Zaaksysteem::Test::Controller::API::v1::Case - Test Zaaksysteem::Test::Controller::API::v1::Case

=head1 DESCRIPTION

Test email backend code

=head1 SYNOPSIS

    prove -l -v t t/test-class-moose.t :: Zaaksysteem::Test::Controller::API::v1::Case;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.


