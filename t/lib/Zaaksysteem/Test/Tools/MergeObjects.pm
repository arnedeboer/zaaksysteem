package Zaaksysteem::Test::Tools::MergeObjects;

use Zaaksysteem::Test;
use Zaaksysteem::Tools::MergeObject qw(:all);

sub _get_anon_meta_class {

    my $meta = Moose::Meta::Class->create_anon_class();
    $meta->add_attribute("foo",  is => 'rw', isa => 'Str' );
    $meta->add_attribute("bar",  is => 'rw', isa => 'Str' );
    $meta->add_attribute("baz",  is => 'rw', isa => 'Str' );
    return $meta;

}

sub _get_anon_moose {
    my %opts = @_;
    my $meta = _get_anon_meta_class();
    return $meta->new_object(%opts);
}

sub _get_anon_dbix {
    my %opts = @_;
    my $meta = _get_anon_meta_class();
    $meta->add_method('get_columns', sub {
            my $self = shift;
            return  [map { $_->name } $self->meta->get_all_attributes]
    });

    $meta->add_attribute("_update_hash", is => 'rw', isa => 'HashRef');
    $meta->add_method('update', sub {
            my $self = shift;
            return $self->_update_hash(shift);
    });
    return $meta->new_object(%opts);
}

sub test_merge_objects {

    my $object = _get_anon_moose(
        foo => 'foo',
        bar => 'bar',
    );

    {
        my %hash = (baz => 'baz');
        my $diff = get_diff($object, \%hash);
        cmp_deeply($diff, { foo => undef, bar => undef, baz => 'baz' }, "get_diff() removes non-existent values");
    }

    {
        my %hash = (foo => 'foo', bar => 'bar');
        my $diff = get_diff($object, \%hash);
        cmp_deeply($diff, { }, "get_diff() found no diffs");
    }

    {
        my %hash = (bar => 'baz');
        $object = merge_moose_obj($object, \%hash);

        is($object->foo, 'foo', "Stays foo, because we can't clear it");
        is($object->baz, undef, "Unset value cannot be set");
        is($object->bar, 'baz', "bar is set correctly");
    }

}

sub test_merge_dbix {

    my $object = _get_anon_dbix(
        foo => 'foo',
        bar => 'bar',
    );

    {
        my %hash = (baz => 'baz');
        my $diff = get_diff($object, \%hash);
        cmp_deeply($diff, { foo => undef, bar => undef, baz => 'baz' }, "get_diff() removes non-existent values");
    }

    {
        my %hash = (foo => 'foo', bar => 'bar');
        my $diff = get_diff($object, \%hash);
        cmp_deeply($diff, { }, "get_diff() found no diffs");
    }

    {
        my %hash = (bar => 'baz');
        $object = merge_dbix_row($object, \%hash);

        cmp_deeply($object->_update_hash, { foo => undef, bar => 'baz' }, "update call succeeds");
    }
}

1;

__END__

=head1 NAME

Zaaksysteem::Test::Tools::MergeObjects - Test ZS::Tools::MergeObjects

=head1 DESCRIPTION

=head1 SYNOPSIS

    prove -l :: Zaaksysteem::Test::Tools::MergeObjects


=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
