package Zaaksysteem::Test::Mocks;
use Exporter 'import';

use Mock::Quick;

use warnings;
use strict;
use feature 'state';
use Scalar::Util qw/blessed/;

our @EXPORT = qw(
    mock_case
    mock_moose_class
    mock_dbix_schema
    mock_dbix_resultset
    mock_dbix_component

    mock_transaction
    mock_transaction_record

    mock_interface
);

=head1 NAME

Zaaksysteem::Test::Mocks - Basic test mocks for Zaaksysteem-specific objects/classes

=head1 DESCRIPTION


=head1 SYNOPSIS

    my $fake = mock_case();
    $fake->id;

=head1 EXPORTED FUNCTIONS

=head2 mock_case

Returns a mock case, that subclasses C<Zaaksysteem::Schema::Zaak> (so ISA
checks work)

=cut

sub mock_case {
    state $case_id_counter = 0;

    my $class = qclass(
        -subclass => 'Zaaksysteem::Schema::Zaak',
        -with_new => 1,

        id => ++$case_id_counter,

        # Add attributes and methods here, as required.
        #-attributes => [
        #],
        #method_name => sub {},
    );

    my $obj = $class->new(@_);

    return $obj;
}

=head2 mock_moose_class

    my $role = mock_moose_class(
        {
            roles   => ['Zaaksysteem::BR::Subject::Types::Person'],
        },
        {
            constructor_param1 => 'value1',
        }
    );

Mocks a Moose Role

=cut

sub mock_moose_class {
    my ($opts, $constructor_params)    = @_;
    $constructor_params //= {};

    return Moose::Meta::Class->create_anon_class(
        %$opts,
    )->new_object(%$constructor_params);
}

=head2 mock_dbix_schema

    my $schema = mock_dbix_schema();

Returns a mock schema, that has the following capabilities

=over

=item method: resultset

    $schema->resultset('Something');

=back

=cut

sub mock_dbix_schema {
    my $class = qclass(
        -with_new => 1,
        -subclass => 'Zaaksysteem::Schema',
        resultset => sub {
            my ($self, $name) = @_;

            return mock_dbix_resultset(resultset => $name);
        },
        default_resultset_attributes => {},
    );

    my $obj = $class->package->new(@_);

    return $obj;
}

=head2 mock_dbix_resultset

    my $rs  = mock_dbix_resultset(resultset => 'Queue');

Mocks a resultset, and loads the resultset_class in it.

=cut

sub mock_dbix_resultset {
    my %opts        = @_;
    my $name        = $opts{resultset};

    my $class   = 'Zaaksysteem::Schema::' . $name;
    eval  "use $class;";

    my $zschema         = $class->new;
    my $resultset_class = $zschema->result_source->resultset_class;
    my $result_class    = $zschema->result_source->result_class;

    $zschema->result_source->schema(mock_dbix_schema);

    my @controls;
    push(@controls, qclass(
        -takeover       => $result_class,
        discard_changes => sub { return shift },        # Make sure we leave alone the database...
        update          => sub {
            my ($self, $params) = @_;
            return 1 unless $params;

            $self->$_($params->{$_}) for keys %$params;

            return $self;
        }
    ));

    my $rsclass     = qclass(
        -subclass       => $resultset_class,
        -with_new       => 1,
        result_source   => $zschema->result_source,
        result_class    => $result_class,
        control         => \@controls,                    # Keep the Mock override alive, as long as this rs lives
        create          => sub {
            return mock_dbix_component(@_);
        },
    );

    return $rsclass->package->new();
}

=head2 mock_dbix_component

    my $row  = mock_dbix_component('Queue', { id => 55, label => 'blabla' });

Mocks a row, makes sure the component is loaded on it

=cut

sub mock_dbix_component {
    my ($rs, $params)   = @_;
    $rs                 = blessed($rs) ? $rs : mock_dbix_resultset(resultset => $rs);

    my $component       = $rs->new_result($params);
    $component->{_inflated_column} = $params;       ### When inflating, DBIx tries to connect to a DB. This skips it.

    return $component;
}

sub mock_natuurlijk_persoon {
    state $natuurlijk_persoon_id = 0;

    my $class = qclass(
        -subclass => 'Zaaksysteem::Schema::NatuurlijkPersoon',
        -with_new => 1,

        id => ++$natuurlijk_persoon_id,

        # Add attributes and methods here, as required.
        #-attributes => [
        #],
        #method_name => sub {},
    );

    return $class->new(@_);
}

sub mock_transaction {
    state $transaction_id_counter = 0;

    my $class = qclass(
        -subclass => 'Zaaksysteem::Schema::Transaction',
        -with_new => 1,

        id => ++$transaction_id_counter,
    );

    my $obj = $class->new(@_);
    return $obj;
}

sub mock_transaction_record {
    state $transaction_record_counter = 0;

    my $class = qclass(
        -subclass => 'Zaaksysteem::Schema::TransactionRecord',
        -with_new => 1,

        id => ++$transaction_record_counter,
        -attributes => [ qw(input output) ],
    );

    my $obj = $class->new(@_);
    return $obj;
}

sub mock_interface {
    state $interface_id = 0;

    my $class = qclass(
        -subclass => 'Zaaksysteem::Model::DB::Interface',
        -with_new => 1,

        id => ++$interface_id,
        -attributes => [ qw(input output) ],
    );

    my $obj = $class->new(@_);
    return $obj;
}


1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
