package Zaaksysteem::Test::Pod;

=head1 NAME

Zaaksysteem::Test::Pod - Test if all files have a EUPL license block

=head2 SYNOPSIS

    prove -l :: Zaaksysteem::Test::Pod

=cut

use Zaaksysteem::Test;
use Test::Pod;
use Pod::Elemental;

my @poddirs = qw(lib/Zaaksysteem lib/Mail t/lib script dev-bin);

sub test_pod : Tags(pod) {
    all_pod_files_ok(@poddirs);
}

sub test_eupl : Tags(pod license) {
    my @podfiles = sort { $a cmp $b } all_pod_files(@poddirs);

    foreach (@podfiles) {
        next if $_ eq 'lib/Zaaksysteem/CONTRIBUTORS.pod';
        if ((stat($_))[7] == 0) {
            note("$_ is empty!");
            next;
        }
        eupl_ok($_);
    }
}

=head2 eupl_ok

Helper function, checks whether a file has a license block (preferably EUPL).

=cut

sub eupl_ok {
    my $file     = shift;
    my $document = Pod::Elemental->read_file($file);

    my $ok = 0;
    foreach my $child (@ {  $document->children } ) {
        if (ref $child eq 'Pod::Elemental::Element::Generic::Command') {
            my $content = $child->content;
            chomp($content);
            if ($content eq 'COPYRIGHT and LICENSE') {
                $ok = 1;
            }
            elsif ($content eq 'COPYRIGHT' || $content eq 'LICENSE') {
                note("Old skool license found in $file, please update");
                $ok = 1;
            }
        }
    }
    return ok($ok, "$file: EUPL license found");
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
