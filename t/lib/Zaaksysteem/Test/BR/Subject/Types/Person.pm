package Zaaksysteem::Test::BR::Subject::Types::Person;

use Zaaksysteem::Test;
use Mock::Quick;

=head1 NAME

Zaaksysteem::Test::Backend::Email - Test ZS::Backend::Email

=head1 DESCRIPTION

Test email backend code

=head1 SYNOPSIS

    prove -l -v :: Zaaksysteem::Test::BR::Subject::Types::Person

=head2 test_run_post_triggers

=cut

sub test_run_post_triggers {
    my $self    = shift;

    ### Mocking:
    my ($person, $row)  = $self->_mock_person_and_role(
        id               => 44,
        datum_overlijden => '2016-08-08',
    );

    ### Run
    $person->run_post_triggers($row);

    ### Check resulting queue item
    my $item = shift (@{ $row->result_source->schema->default_resultset_attributes->{queue_items} });

    is($item->[1]->{'data'}->{natuurlijk_persoon_id}, 44, "Item got correct natuurlijk_persoon_id");
    is($item->[0], 'update_cases_of_deceased', "Loaded item: update_case_of_deceased");
}


=head2 _mock_person_and_role

Mocks a role of this class

=cut

sub _mock_person_and_role {
    my $self    = shift;

    my $schema = qobj(
        default_resultset_attributes => {
            queue_items => [],
        },
        resultset => qmeth {
            my $rsname = $_[1];
            die("Test not implemented for: $rsname") unless $rsname eq 'Queue';

            return qobj(
                create_item => qmeth {
                    my $obj = shift;
                    return [@_];
                }
            );
        }
    );

    # my $schema  = mock_dbix_schema();
    my $person  = mock_moose_class({
        roles => ['Zaaksysteem::BR::Subject::Types::Person'],
    });

    my $row     = qobj(
        @_,
        result_source => qobj(schema  => $schema)
    );

    return ($person, $row);
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.