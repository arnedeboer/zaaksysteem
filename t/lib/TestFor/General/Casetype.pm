package TestFor::General::Casetype;
use base qw(Test::Class);

use TestSetup;

sub create_phased_casetype {
    my $zaaktype_definitie = $zs->create_zaaktype_definitie_ok(
        preset_client => 'betrokkene-natuurlijk_persoon-1'
    );
    my $zaaktype_node = $zs->create_zaaktype_node_ok(
        zaaktype_definitie => $zaaktype_definitie
    );

    my $zaaktype = $zs->create_zaaktype_predefined_ok(node => $zaaktype_node);

    my $bieb1   = $zs->create_bibliotheek_kenmerk_ok(
        magic_string => 'kenmerk1',
        value_type => 'text'
    );

    my $bieb2   = $zs->create_bibliotheek_kenmerk_ok(
        magic_string => 'kenmerk2',
        value_type => 'richtext'
    );

    my $zaaktype_status = $zaaktype_node->zaaktype_statuses->search->first;

    $zs->create_zaaktype_kenmerk_ok(
        status => $zaaktype_status,
        bibliotheek_kenmerk => $bieb1,
    );
    $zs->create_zaaktype_kenmerk_ok(
        status => $zaaktype_status,
        bibliotheek_kenmerk => $bieb2,
    );

    return $zaaktype;
}


sub casetype_attributes : Tests {
    $zs->txn_ok(sub {
        my $empty_casetype = $zs->create_zaaktype_ok;
        is_deeply $empty_casetype->attributes_by_magic_string, {}, 'Empty has if no kenmerken';

        my $casetype = create_phased_casetype;

        my $lookup = $casetype->attributes_by_magic_string;

        is scalar keys %$lookup, 2, 'Two keys in lookup';
        my $kenmerk1 = $lookup->{kenmerk1};
        is $kenmerk1->magic_string, 'kenmerk1', 'Kenmerk1 has correct magic string';
        is $kenmerk1->value_type, 'text', 'Kenmerk1 has correct value_type';

        my $kenmerk2 = $lookup->{kenmerk2};
        is $kenmerk2->magic_string, 'kenmerk2', 'Kenmerk2 has correct magic string';
        is $kenmerk2->value_type, 'richtext', 'Kenmerk2 has correct value_type';
    });
}

sub zs_casetype_authorisation : Tests {
    $zs->zs_transaction_ok(sub {
        my $auth = $zs->create_zaaktype_authorisation_ok;

        my $ok = can_ok($auth, 'security_identity');
        diag('zaaktype/object does not implements security identity method') unless $ok;

        my $casetype_object = $zs->object_model->rs->first;
        my $casetype_acl    = $casetype_object->object_acl_entries->first;
        isa_ok($casetype_acl, "Zaaksysteem::Model::DB::ObjectAclEntry");

        my %id = $auth->security_identity;

        ok exists $id{ position }, 'zaaktype/object security_identity returns a position';

        is $casetype_acl->entity_type, 'position', 'zaaktype/object position is set';
        is $casetype_acl->entity_id, $id{ position }, 'zaaktype/object position ident correct';
        is $casetype_acl->get_column('object_uuid'), $casetype_object->uuid, 'zaaktype/object object_uuid matches casetype';

        # this last check is for the sanity of the testsuite itself, this file
        # must be reviewed since the underlying TestUtils API (or something else)
        # has broken. Also it checks that the 'recht' zaak_edit is mapped to 'read'
        is $casetype_acl->capability, 'read', 'zaaktype/object default permission is read';

    }, 'zaaktype/object object_data sync ok');

    $zs->zs_transaction_ok(sub {
        my (@auths) = $zs->create_zaaktype_authorisation_ok( recht => 'zaak_edit' );

        my $casetype_acl = $zs->object_model->rs->first->object_acl_entries->first;

        is $casetype_acl->capability, 'write', 'zaaktype/object zaak_edit permission mapped to write';
    });
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

