package TestFor::General::Tools;
use base qw(Test::Class);

use TestSetup;

use FakeLogger;
use Zaaksysteem::Tools;

sub import_tests : Tests {
    lives_ok(
        sub { define_profile("test" => ()); },
        "define_profile() was imported",
    );

    throws_ok(
        sub { throw("aap", "noot") },
        "Zaaksysteem::Exception::Base",
        "throw() was imported"
    );
}

sub generate_message_tests : Tests {
    my $ref_msg = Zaaksysteem::Tools::_generate_message({ a => "reference" });
    is($ref_msg, qq[{ a => "reference" }$/], "Reference is dumped properly");

    my $filtered_msg = Zaaksysteem::Tools::_generate_message({ schema => $zs->schema });
    is(
        $filtered_msg,
        sprintf(
            qq[{\n  schema => <Zaaksysteem::Schema database handle connected to %s>,\n}$/],
            $zs->schema->storage->connect_info->[0]{dsn}
        ),
        "Database object was properly flattened"
    );

    my $simple_msg = Zaaksysteem::Tools::_generate_message("foo");
    is($simple_msg, "foo$/", "Simple string is returned as-is");

    my $sprintf_msg = Zaaksysteem::Tools::_generate_message("foo %d", 42);
    is($sprintf_msg, "foo 42$/", "Multiple arguments are passed through sprintf");
}

sub log_tests : Tests {
    my $log = Zaaksysteem::Tools::_log("a", "b", "c");
    is($log, "a$/b$/c", "In non-void context, the to-be-logged data is returned");

    with_stopped_clock {
        my $warnings = "";
        local $SIG{__WARN__} = sub { $warnings .= join("", @_) };

        Zaaksysteem::Tools::_log("a", "b", "c");
        is(
            $warnings,
            localtime . " a$/b$/c$/",
            "In void context with no logger object, data is warned.",
        );
    };

    my $logger = FakeLogger->new();
    Zaaksysteem::Tools::set_logger($logger);
    Zaaksysteem::Tools::_log("a", "b", "c");

    is_deeply(
        $logger->log->{debug},
        ["a$/b$/c"],
        "Log object is set, and _log is called in void context",
    );
}

sub toplevel_tests : Tests {
    my $msg_burp = burp("Ooh, %s-flavoured!", "lemon");
    is($msg_burp, "Ooh, lemon-flavoured!\n", "burp() works");

    my $msg_barf = barf("Ooh, %s-flavoured!", "pistachio");
    like($msg_barf, qr/^Ooh, pistachio-flavoured!$/m, "barf() output contains the message");
    like($msg_barf, qr/^Trace begun at/m, "barf() output contains a stack trace");
}

sub sanitize_filename_tests : Tests {
    {
        my $evil_filename = "C:\\Program Files\\<b>To boldly go where no man has gone before</b>.odt";
        is(sanitize_filename($evil_filename), "b_.odt", 'Filename containing all kinds of bad stuff');
    }

    {
        my $evil_filename = qq{<>"&};
        is(sanitize_filename($evil_filename), "____", "Filename composed of all 'bad' characters'");
    }

    {
        my $evil_filename = qq{C:\\Windows\\System32\\TEST.exe};
        is(sanitize_filename($evil_filename), "TEST.exe", "Normal Windows/MSIE full-path filename");
    }

    {
        my $evil_filename = qq{VerySafe.odt};
        is(sanitize_filename($evil_filename), "VerySafe.odt", "Normal filename");
    }
}

sub signature_tests : Tests {
    my $obj = SignatureTest->new;

    lives_ok {
        is $obj->test_simple(1), 1, 'retval checks out'
    } 'simple positive test signature is ok';

    dies_ok {
        $obj->test_simple('Str')
    } 'simple negative test signature is ok';

    lives_ok {
        my $ok = $obj->test_noargs
    } 'noargs positive test signature ok';

    lives_ok {
        my $ok = $obj->test_noargs(1, 'str', {})
    } 'noargs positive test is lenient about additional args';

    lives_ok {
        my $ok = $obj->test_noret('')
    } 'noret positive test ok';

    lives_ok {
        is $obj->test_slurp_array(20, qw[a b c]), 3, 'filled list retval'
    } 'slurp_array positive test ok';

    lives_ok {
        is $obj->test_slurp_array(20), 0, 'empty list retval'
    } 'slurp_array positive, no elements, test ok';

    dies_ok {
        $obj->test_slurp_array(20, {}, [])
    } 'slurp_array negative, wrong type ok';

    lives_ok {
        $obj->test_slurp_typed_array({ foo => 'bar' }, { baz => 'quux' })
    } 'slurp_typed_array positive with hashref args';

    dies_ok {
        $obj->test_slurp_typed_array({ foo => \1 }, { bar => \2 })
    } 'slurp_typed_array negative with hashref args';

    lives_ok {
        my $ok = $obj->test_slurp_hash(20, a => 1, b => 2);
    } 'slurp_hash positive with defined vals';

    dies_ok {
        $obj->test_slurp_typed_hash(a => \1, b => \2);
    } 'slurp_typed_hash negative with nested type mismatch';
}

package SignatureTest;

use Moose;
use Zaaksysteem::Tools qw[sig];

has fail => (
    is => 'rw'
);

sig test_simple => 'Num => Num';

sub test_simple {
    my ($self, $num) = @_;

    return $self->fail ? 'string' : $num;
}

sig test_noargs => '=> Str';

sub test_noargs {
    my ($self) = @_;

    return $self->fail ? undef : 'string';
}

sig test_noret => 'Defined';

sub test_noret {
    my ($self, $defined) = @_;

    return unless $self->fail;
    return $defined;
}

sig test_slurp_array => 'Num, @Str => Num';

sub test_slurp_array {
    my ($self, $num, @rest) = @_;

    return $self->fail ? '' : scalar @rest;
}

sig test_slurp_typed_array => '@HashRef[Str]';

sub test_slurp_typed_array {
    my ($self, @rest) = @_;

    return scalar @rest;
}

sig test_slurp_hash => 'Num, %Defined => Num';

sub test_slurp_hash {
    my ($self, $num, %hash) = @_;

    return $self->fail ? '' : scalar keys %hash;
}

sig test_slurp_typed_hash => '%ArrayRef[Str]';

sub test_slurp_typed_hash {
    my ($self, %hash) = @_;

    return scalar keys %hash;
}

sig test_optional => 'Num, ?Str, @Num => Num';

sub test_optional {
    my ($self, $num, @rest) = @_;

    return scalar @rest;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

