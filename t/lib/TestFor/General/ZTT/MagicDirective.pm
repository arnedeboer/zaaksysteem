package TestFor::General::ZTT::MagicDirective;

use base qw[Test::Class];

use TestSetup;

use Zaaksysteem::ZTT::MagicDirective;

sub ztt_magic_directive_parser : Tests {
    my $directive_parser = Zaaksysteem::ZTT::MagicDirective->new;

    is_deeply $directive_parser->parse('case.id'), {
        attribute => 'case.id'
    }, 'simple attribute is "parsed"';

    is_deeply $directive_parser->parse('case.startdate | date'), {
        attribute => 'case.startdate',
        filter => {
            name => 'date',
            args => []
        }
    }, 'simple attribute + filter parses';

    is_deeply $directive_parser->parse('case.startdate | date(short)'), {
        attribute => 'case.startdate',
        filter => {
            name => 'date',
            args => [qw[short]]
        }
    }, 'simple attribute + filter with bareword argument parses';

    is_deeply $directive_parser->parse('case.startdate | date("ABC")'), {
        attribute => 'case.startdate',
        filter => {
            name => 'date',
            args => [qw[ABC]]
        }
    }, 'simple attribute + filter with string argument parses';

    is_deeply $directive_parser->parse('case.startdate | date("ABC", case, "herp")'), {
        attribute => 'case.startdate',
        filter => {
            name => 'date',
            args => [qw[ABC case herp]]
        }
    }, 'simple attribute + filter with mixed arguments parses';

    is_deeply $directive_parser->parse('itereer:case_relations:case.id'), {
        attribute => 'case.id',
        iterate_context => 'case_relations'
    }, 'simple attribute + inline iteration syntax ok';

    is_deeply $directive_parser->parse('itereer:case_relations:case.id | link'), {
        attribute => 'case.id',
        iterate_context => 'case_relations',
        filter => {
            name => 'link',
            args => []
        }
    }, 'simple attribute + inline iteration syntax + arg-less filter parses';

    is_deeply $directive_parser->parse('itereer:case_relations:case.id | link(case, herp)'), {
        attribute => 'case.id',
        iterate_context => 'case_relations',
        filter => {
            name => 'link',
            args => [qw[case herp]]
        }
    }, 'simple attribute + inline iteration syntax + filter + filter_args parses';

    is_deeply $directive_parser->parse('ITerEER:case_RELATIONS:case.ID | LiNK(CaSe, "AbC")'), {
        attribute => 'case.id',
        iterate_context => 'case_relations',
        filter => {
            name => 'link',
            args => [qw[CaSe AbC]]
        }
    }, 'mixed-case attribute name, iterate keyword, iterate context and filtername parses and returns lower-cased';

    is $directive_parser->parse('magic_string_with_ space'), undef, 'syntax error in attribute results in undef parse';
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
