package TestFor::General::Tools::Text;

use base qw[Test::Class];

use TestSetup;

use Zaaksysteem::Tools::Text;
use Zaaksysteem::Tools::Text::PDFHack;

sub simple_text_en : Tests {
    my $text = Zaaksysteem::Tools::Text->new(source => 'Here I am, building some English text. I sure hope the new library can identify me.');

    is $text->language, 'en', 'English correctly detected';

    # Base tests
    ok grep { $_ eq 'english' } $text->stemmed_words;
    ok grep { $_ eq 'text' } $text->stemmed_words;
    ok grep { $_ eq 'identifi' } $text->stemmed_words;
    ok grep { $_ eq 'librari' } $text->stemmed_words;

    # Invert test, verify stoplist was used
    ok not grep { $_ eq 'i' } $text->stemmed_words;
    ok not grep { $_ eq 'am' } $text->stemmed_words;
}

sub simple_text_nl : Tests {
    my $text = Zaaksysteem::Tools::Text->new(source => 'Dit is een stukje tekst met hopelijk genoeg informatie om herkend te kunnen worden als Nederlands. Gebrabbel.');

    is $text->language, 'nl', 'Dutch correctly detected';

    # Base tests
    ok grep { $_ eq 'nederland' } $text->stemmed_words;
    ok grep { $_ eq 'tekst' } $text->stemmed_words;
    ok grep { $_ eq 'stukj' } $text->stemmed_words;
    ok grep { $_ eq 'herkend' } $text->stemmed_words;

    # Invert test, verify stoplist was used
    ok not grep { $_ eq 'is' } $text->stemmed_words;
    ok not grep { $_ eq 'met' } $text->stemmed_words;
}

sub pdf_text_en : Tests {
    my $pdf = Zaaksysteem::Tools::Text::PDFHack->new(file => 't/inc/Documents/openoffice_document.pdf');

    my $text = Zaaksysteem::Tools::Text->new(source => $pdf);

    is $text->language, 'en', 'English correctly detected from PDF content';

    ok grep { $_ eq 'free' } $text->stemmed_words;
    ok grep { $_ eq 'manipul' } $text->stemmed_words;

    ok not grep { $_ eq 'is' } $text->stemmed_words;
}

sub odt_text_en : Tests {
    my $odt = Zaaksysteem::Tools::Text::OpenDocument->new(file => 't/inc/Documents/openoffice_document.odt');

    my $text = Zaaksysteem::Tools::Text->new(source => $odt);

    is $text->language, 'en', 'English correctly detected from ODT content';

    #diag explain [ $text->stemmed_words ];

    ok grep { $_ eq 'break' } $text->stemmed_words;
    ok grep { $_ eq 'zaaksysteem' } $text->stemmed_words;

    ok not grep { $_ eq 'is' } $text->stemmed_words;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
