package TestFor::General::StUF::0204::NatuurlijkPersoon;
use base qw(Test::Class);

use Moose;
use TestSetup;
use File::Spec::Functions qw(catfile);

use Storable qw/dclone/;

#use Zaaksysteem::XML::Compile::Backend;

has 'xmlcompile'    => (is => 'rw');

sub setup : Test(startup) {
    my $self = shift;

    $self->xmlcompile(Zaaksysteem::XML::Compile->xml_compile);

    $self->xmlcompile->add_class('Zaaksysteem::StUF::0204::Instance');
}

sub stuf_0204_helpers_natuurlijk_persoon : Tests {
    my $self            = shift;

    my $raw_period      = {
      einddatum => {
        _ => 'NIL',
        exact => 1,
        indOnvolledigeDatum => 'V',
        noValue => 'geenWaarde'
      },
      ingangsdatum => {
        _ => bless( {
          _e => [
            0
          ],
          _es => '+',
          _m => [
            19511115
          ],
          sign => '+'
        }, 'Math::BigFloat' ),
        exact => 1,
        indOnvolledigeDatum => 'V'
      },
    };

    ok($self->xmlcompile->stuf0204->_is_active_period($raw_period), 'is_active_period: With startdate: true');

    my $no_ingang = dclone($raw_period);
    $no_ingang->{ingangsdatum}->{_} = 'NIL';
    $no_ingang->{einddatum}->{_} = '19991201';


    ok(!$self->xmlcompile->stuf0204->_is_active_period($no_ingang), 'is_active_period: No startdate: false');

    my $end_in_passed = dclone($raw_period);
    $end_in_passed->{einddatum}->{_}             = '20120501';

    ok(!$self->xmlcompile->stuf0204->_is_active_period($end_in_passed), 'is_active_period: Enddate in past: false');

    my $start_in_future = dclone($raw_period);

    $start_in_future->{ingangsdatum}->{_}->{_m} = ['20400101'];

    ok(!$self->xmlcompile->stuf0204->_is_active_period($start_in_future), 'is_active_period: Future startdate: false');

    my $no_period = dclone($raw_period);
    delete $no_period->{ingangsdatum};
    delete $no_period->{einddatum};

    ok($self->xmlcompile->stuf0204->_is_active_period($no_period), 'is_active_period: no dates: true');

    my @raw_partners      = (
        {
          id => 1,
          datumSluiting => {
            _ => bless( {
              _e => [
                0
              ],
              _es => '+',
              _m => [
                19980101
              ],
              sign => '+'
            }, 'Math::BigFloat' ),
            exact => 1,
            indOnvolledigeDatum => 'V'
          },
          datumOntbinding => {
            _ => bless( {
              _e => [
                0
              ],
              _es => '+',
              _m => [
                19991231
              ],
              sign => '+'
            }, 'Math::BigFloat' ),
            exact => 1,
            indOnvolledigeDatum => 'V'
          },
        },
        {
          id => 2,
          datumSluiting => {
            _ => bless( {
              _e => [
                0
              ],
              _es => '+',
              _m => [
                20070101
              ],
              sign => '+'
            }, 'Math::BigFloat' ),
            exact => 1,
            indOnvolledigeDatum => 'V'
          },
          datumOntbinding => {
            _ => bless( {
              _e => [
                0
              ],
              _es => '+',
              _m => [
                20121231
              ],
              sign => '+'
            }, 'Math::BigFloat' ),
            exact => 1,
            indOnvolledigeDatum => 'V'
          },
        },
        {
          id => 3,
          datumSluiting => {
            _ => bless( {
              _e => [
                0
              ],
              _es => '+',
              _m => [
                20010101
              ],
              sign => '+'
            }, 'Math::BigFloat' ),
            exact => 1,
            indOnvolledigeDatum => 'V'
          },
          datumOntbinding => {
            _ => bless( {
              _e => [
                0
              ],
              _es => '+',
              _m => [
                20021231
              ],
              sign => '+'
            }, 'Math::BigFloat' ),
            exact => 1,
            indOnvolledigeDatum => 'V'
          },
        }
    );


    my $partner = $self->xmlcompile->stuf0204->_get_active_partner(\@raw_partners);
    is($partner->{id}, 2, '_get_active_partner: laatste Ontbinding');

    push(
        @raw_partners,
        {
          id => 4,
          datumSluiting => {
            _ => bless( {
              _e => [
                0
              ],
              _es => '+',
              _m => [
                20130101
              ],
              sign => '+'
            }, 'Math::BigFloat' ),
            exact => 1,
            indOnvolledigeDatum => 'V'
          },
          datumOntbinding => {
            _ => 'NIL',
            exact => 1,
            indOnvolledigeDatum => 'V',
            noValue => 'geenWaarde'
          },
        }
    );

    $partner = $self->xmlcompile->stuf0204->_get_active_partner(\@raw_partners);

    is($partner->{id}, 4, '_get_active_partner: geen Ontbinding');
}

# TODO: better faults
# sub stuf_0204_foutbericht : Tests {
#     my $self            = shift;

#     my $xml             = $self->xmlcompile->stuf0204->foutbericht();

#     diag(explain($xml));
# }

sub stuf_0204_get_natuurlijk_persoon : Tests {
    my $self            = shift;

    my $VALIDATION_MAP = {
        'a_nummer'                  => '1234567890',
        'burgerservicenummer'       => '987654321',
        'voornamen'                 => 'Tinus',
        'voorletters'               => 'T',
        'geslachtsnaam'             => 'Testpersoon',
        'geslachtsaanduiding'       => 'M',
        'postcode'                  => '1015JL',
        'woonplaats'                => 'Amsterdam',
        'straatnaam'                => 'Donker Curtiusstraat',
        'huisnummer'                => '7',
        'huisnummertoevoeging'      => '521',
    };

    my $perl = $self->xmlcompile->stuf0204->get_natuurlijkpersoon(
        {
            filename => catfile(STUF_TEST_XML_PATH, '0204', 'prs/101-prs-create-tinus.xml')
        }
    );

    is($perl->{mutation_type}, 'create', 'Mutation type "create" found');

    for my $key (keys %{ $VALIDATION_MAP }) {
        is($perl->{new}->{$key}, $VALIDATION_MAP->{$key}, 'Found valid value for: ' . $key);
    }

    ### Huwelijk
    $VALIDATION_MAP = {
        'a_nummer'                      => '1234567891',
        'burgerservicenummer'           => '987654322',
        'voornamen'                     => 'TinusHUW',
        'voorletters'                   => 'V',
        'geslachtsnaam'                 => 'Testpersoon',
        'geslachtsaanduiding'           => 'M',
        'geboortedatum'                 => '19620529',
        'postcode'                      => '1015JL',
        'woonplaats'                    => 'Amsterdam',
        'straatnaam'                    => 'Donker Curtiusstraat',
        'huisnummer'                    => '7',
        'huisnummertoevoeging'          => '521',
        'partner_a_nummer'              => '5654321023',
        'partner_burgerservicenummer'   => '568316589',
        'partner_voorvoegsel'           => 'de',
        'partner_geslachtsnaam'         => 'TestpartnernaamGOOD'
    };

    $perl = $self->xmlcompile->stuf0204->get_natuurlijkpersoon(
        {
            filename => catfile(STUF_TEST_XML_PATH, '0204', 'prs/102-prs-create-huwelijk.xml')
        }
    );

    is($perl->{mutation_type}, 'create', 'Mutation type "create" found');

    for my $key (keys %{ $VALIDATION_MAP }) {
        is($perl->{new}->{$key}, $VALIDATION_MAP->{$key}, 'Found valid value for: ' . $key);
    }
}

sub stuf_0204_set_natuurlijk_persoon : Tests {
    my $self            = shift;

    my $xml             = $self->xmlcompile->stuf0204->set_natuurlijkpersoon(
        {
            a_nummer                => '1234567890',
            burgerservicenummer     => '987654321',
            voornamen               => 'Tinus',
            voorletters             => 'TV',
            voorvoegsel             => 'vander',
            geslachtsnaam           => 'Testpersoon',
            geboortedatum           => '19830609',
            datum_overlijden        => '19830610',
            indicatie_geheim        => undef,
            burgerlijkestaat        => undef,
            aanduiding_naamgebruik  => 'P',
            sleutel_gegevensbeheer  => '234234234',
            sleutel_verzendend      => '23423423424234',
        },
        {
            sender                  => 'ZSNL',
            receiver                => 'CMODIS',
            reference               => 'ZS0000229154',
            datetime                => '2014030209011458',
            mutation_type           => 'create',
            follow_subscription     => 1,
        },
    );

    like($xml, qr/$_/, 'Found string in returned XML: ' . $_) for qw/
        a-nummer>1234567890
        bsn-nummer>987654321
        voornamen>Tinus
        voorletters>TV
        voorvoegselGeslachtsnaam>vander
        geslachtsnaam>Testpersoon
        geboortedatum>19830609
        datumOverlijden>19830610
        Naamgebruik>P
        sleutelGegevensbeheer="234234234"
        sleutelVerzendend="23423423424234"
    /;

    #diag($xml);
}

sub stuf_0204_search_natuurlijk_persoon : Tests {
    my $self            = shift;

    my $xml             = $self->xmlcompile->stuf0204->search_natuurlijkpersoon(
        {
            a_nummer                => '1234567890',
            burgerservicenummer     => '987654321',
            voornamen               => 'Tinus',
            voorletters             => 'TV',
            voorvoegsel             => 'vander',
            geslachtsnaam           => 'Testpersoon',
            geboortedatum           => '19830609',
        },
        {
            sender                  => 'ZSNL',
            receiver                => 'CMODIS',
            reference               => 'ZS0000229154',
            datetime                => '2014030209011458',
            mutation_type           => 'create',
            follow_subscription     => 1,
        },
    );

    like($xml, qr/$_/, 'Found string in returned XML: ' . $_) for qw/
        a-nummer>1234567890
        bsn-nummer>987654321
        voornamen>Tinus
        voorletters>TV
        voorvoegselGeslachtsnaam>vander
        geslachtsnaam>Testpersoon
    /;

    my $trace;
    throws_ok(
        sub {
            $self->xmlcompile->stuf0204->search_natuurlijkpersoon(
                    {
                        a_nummer                => '1234567890',
                        burgerservicenummer     => '987654321',
                        voornamen               => 'Tinus',
                        voorletters             => 'TV',
                        voorvoegsel             => 'vander',
                        geslachtsnaam           => 'Testpersoon',
                        geboortedatum           => '19830609',
                    },
                    {
                        sender                  => 'ZSNL',
                        receiver                => 'CMODIS',
                        reference               => 'ZS0000229154',
                        datetime                => '2014030209011458',
                        mutation_type           => 'create',
                        follow_subscription     => 1,
                        dispatch                => {
                            transport_hook          => sub {
                                return HTTP::Response->new(200, 'answer manually created',
                                    [ 'Content-Type' => 'text/xml' ], '<xml><my /><custom /></xml>'
                                );
                            }
                        }
                    },
                );
        },
        qr/my.*custom.*xml/,
        'Tested transport_hook: Found custom XML'
    );
    # ($xml, $trace)   = $self->xmlcompile->stuf0204->search_natuurlijkpersoon(
    #     {
    #         a_nummer                => '1234567890',
    #         burgerservicenummer     => '987654321',
    #         voornamen               => 'Tinus',
    #         voorletters             => 'TV',
    #         voorvoegsel             => 'vander',
    #         geslachtsnaam           => 'Testpersoon',
    #         geboortedatum           => '19830609',
    #     },
    #     {
    #         sender                  => 'ZSNL',
    #         receiver                => 'CMODIS',
    #         reference               => 'ZS0000229154',
    #         datetime                => '2014030209011458',
    #         mutation_type           => 'create',
    #         follow_subscription     => 1,
    #         dispatch                => {
    #             transport_hook          => sub {
    #                 return HTTP::Response->new(200, 'answer manually created',
    #                     [ 'Content-Type' => 'text/xml' ], '<xml><my /><custom /></xml>'
    #                 );
    #             }
    #         }
    #     },
    # );

    # like($trace->response->content, qr/my.*custom.*xml/, 'Tested transport_hook: Found custom XML');

    #diag($xml);
}

sub stuf_0204_set_pink_voa : Tests {
    my $self            = shift;

    throws_ok(
        sub {
            $self->xmlcompile->stuf0204->set_pink_voa({ bla => 'not'});
        },
        qr/Validation of profile failed/,
        'Succesfull validation of parameter profile'
    );

    my $xml             = $self->xmlcompile->stuf0204->set_pink_voa(
        {
            a_nummer                => '1234567890',
        },
    );

    like($xml, qr/$_/, 'Found string in returned XML: ' . $_) for qw/
        Anummer>1234567890
    /;

    $xml             = $self->xmlcompile->stuf0204->set_pink_voa(
        {
            burgerservicenummer     => '987654321',
        },
    );

    like($xml, qr/$_/, 'Found string in returned XML: ' . $_) for qw/
        BSN>987654321
    /;

    #diag($xml);

}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

