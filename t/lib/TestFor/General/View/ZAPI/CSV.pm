package TestFor::General::View::ZAPI::CSV;
use warnings;
use strict;
use base 'Test::Class';

use TestSetup;

use Zaaksysteem::Object::Model;
use Zaaksysteem::View::ZAPI::CSV;

sub setup : Test(startup) {
    my $self = shift;
    $self->{csv} = Zaaksysteem::View::ZAPI::CSV->new();
    $self->{object_model} = Zaaksysteem::Object::Model->new(schema => $zs->schema);
}


sub get_row_data {
    [
      {
        'A_NUMMER' => '1269458621',
        'HUISLETTER_VBL' => 'X',
        'HUISNUMMER_VBL' => 42,
        'KVK_NUMMER_1' => '',
        'NAAM' => 'Martin',
        'NATUURLIJK_NIET_NATUURLIJK' => 'N',
        'PLAATSNAAM_VBL' => 'Hilversum',
        'POSTKODE_ALFANUMERIEK_VBL' => 'AB',
        'POSTKODE_NUMERIEK_VBL' => '1234',
        'SOFINUMMER' => '164783248',
        'STAMNUMMER_BELASTINGEN' => '17',
        'STRAATNAAM_VBL' => 'Jimlaan',
        'TOEVOEGING_HUISNUMMER_VBL' => undef,
        'VOORLETTERS_VBL' => 'J W',
        'VOORVOEGSEL_VBL' => ''
      },
      {
        'A_NUMMER' => '',
        'HUISLETTER_VBL' => undef,
        'HUISNUMMER_VBL' => '1',
        'KVK_NUMMER_1' => '12345678',
        'NAAM' => 'Leeghe BV',
        'NATUURLIJK_NIET_NATUURLIJK' => 'I',
        'PLAATSNAAM_VBL' => 'Amsterdam',
        'POSTKODE_ALFANUMERIEK_VBL' => 'AB',
        'POSTKODE_NUMERIEK_VBL' => '1234',
        'SOFINUMMER' => '',
        'STAMNUMMER_BELASTINGEN' => '23',
        'STRAATNAAM_VBL' => 'Dam',
        'TOEVOEGING_HUISNUMMER_VBL' => undef,
        'VOORLETTERS_VBL' => '',
        'VOORVOEGSEL_VBL' => ''
      }
    ]
}

sub get_zapi_object {
    my $row_data = shift;

    return Zaaksysteem::ZAPI::Response->new(
        unknown     => $row_data,
        uri_prefix  => URI->new('http://localhost/'),
        no_pager    => 1,
        options     => {
            'csv'   => {
                sep_char        => ';',
                column_order    => [qw/A_NUMMER KVK_NUMMER_1/],
                no_header       => 1
            }
        }
    );
}

sub test_zapi_csv : Tests {
    my $self = shift;

    $zs->zs_transaction_ok(sub {

        my $zapi_object = get_zapi_object();

        my $csv_output  = $self->{csv}->render(undef, undef, $zapi_object, { csv => []});

        is $csv_output, '', 'Empty string returned if empty array supplied';

    }, 'Check empty row_data behaviour');


    $zs->zs_transaction_ok(sub {

        my $zapi_object = get_zapi_object(get_row_data);
        my $column_order    = $self->{csv}->_load_csv_header($zapi_object, $zapi_object->result);

        isa_ok $column_order, 'ARRAY', 'Load CSV header';
        is scalar @$column_order, 2, "Two heading columns";

    }, 'Check load csv header');


    $zs->zs_transaction_ok(sub {

        my $zapi_object = get_zapi_object(get_row_data);

        my $rows        = $self->{csv}->_process_zapi_result($zapi_object, $zapi_object->result);

        foreach my $row (@$rows) {
            is ref $row, 'ARRAY', "Row is an array";
            is scalar @$row, 2, "Row array has two elements";
        }

    }, 'CSV without header');


    ### Test process (until we need $c)
    $zs->zs_transaction_ok(sub {
        throws_ok(
            sub {
                $self->{csv}->process(undef, { format => 'blaat'})
            },
            qr/invalid: format/,
            '$self->{csv}->process: Incorrect formats'
        );
    }, '$self->{csv}->process: Exceptions');

    $zs->zs_transaction_ok(sub {
        my $case    = $zs->create_case_ok();

        ok($case->touch, 'Touched zaak for updated ObjectData');

        my $rs      = $schema->resultset('ObjectData')->search({ object_class => 'case'});

        $rs->object_requested_attributes([qw/case.number case.casetype.id case.casetype.name/]);

        my $zapi_object = Zaaksysteem::ZAPI::Response->new(
            resultset   => $rs,
            uri_prefix  => URI->new('http://localhost/index'),
            no_pager    => 1,
        );

        throws_ok(
            sub {
                $self->{csv}->_process_zapi_result($zapi_object);
            },
            qr/No zapi attribute found/,
            '$self->{csv}->process: Test exception'
        );

        my $rows    = $self->{csv}->_process_zapi_result($zapi_object, $zapi_object->result);

        isa_ok($rows, 'ARRAY', 'Found list of rows');

        is(@{ $rows }, ($rs->count + 1), 'Found same number as rows as in resultset, + one header');

        for my $sample (qw/Zaaknummer Zaaktype/) {
            ok(grep({ $sample eq $_ } @{ $rows->[0]}), 'Found in header: ' . $sample);
        }
    }, '$self->{csv}->_process_zapi_result with ResultSet');

    $zs->zs_transaction_ok(sub {
        my $new_obj = $self->{object_model}->inflate_from_json('{ "object_class": "object" }');
        my $object1 = $self->{object_model}->save(object => $new_obj);
        my $object2 = $self->{object_model}->save(object => $new_obj);

        my $rs = $self->{object_model}->rs;

        my $iter = Zaaksysteem::Object::Iterator->new(
            rs => $rs,
            model => $self->{object_model},
        );

        my $zapi_object = Zaaksysteem::ZAPI::Response->new(
            iterator    => $iter,
            uri_prefix  => URI->new('http://localhost/index'),
            no_pager    => 1,
        );

        my $rows = $self->{csv}->_process_zapi_result($zapi_object, $zapi_object->result);

        isa_ok($rows, 'ARRAY', 'Found list of rows');

        is(@{ $rows }, ($rs->count + 1), 'Found same number as rows as in resultset, + one header');
    }, '$self->{csv}->_process_zapi_result with iterator');

    $zs->zs_transaction_ok(sub {
        my $case    = $zs->create_case_ok();

        ok($case->touch, 'Touched zaak for updated ObjectData');

        my $rs      = $schema->resultset('ObjectData')->search({ object_class => 'case'});

        $rs->object_requested_attributes([qw/case.number casetype.id casetype.name/]);

        my $zapi_object = Zaaksysteem::ZAPI::Response->new(
            resultset   => $rs,
            uri_prefix  => URI->new('http://localhost/index'),
            no_pager    => 1,
        );

        throws_ok(
            sub {
                $self->{csv}->_process_zapi_result($zapi_object);
            },
            qr/No zapi attribute found/,
            '$self->{csv}->process: Test exception'
        );

        my $rows        = $self->{csv}->_process_zapi_result($zapi_object, $zapi_object->result);

        my $csv_output  = $self->{csv}->render(undef, undef, $zapi_object, { csv => $rows});
        like($csv_output, qr/Zaaknummer/, 'Found case.number in text output');

    }, '$self->{csv}->render');

    $zs->zs_transaction_ok(sub {
        my $case    = $zs->create_case_ok();

        ok($case->touch, 'Touched zaak for updated ObjectData');

        my $rs      = $schema->resultset('ObjectData')->search({ object_class => 'case'});

        $rs->object_requested_attributes([qw/case.number casetype.id casetype.name/]);

        my $zapi_object = Zaaksysteem::ZAPI::Response->new(
            resultset   => $rs,
            uri_prefix  => URI->new('http://localhost/index'),
            no_pager    => 1,
        );

        my $rows        = $self->{csv}->_process_zapi_result($zapi_object, $zapi_object->result);

        my $csv_output  = $self->{csv}->render(undef, undef, $zapi_object, { csv => $rows});

        like(
            $self->{csv}->_invoke_jodconvertor('calc', $csv_output),
            qr/urn:oasis:names:tc:opendocument:xmlns:office:1.0/,
            'Found calc output'
        );

    }, '$self->{csv}->_invoke_jodconvertor');
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
