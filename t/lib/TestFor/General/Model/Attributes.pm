package TestFor::General::Model::Attributes;
use base qw(Test::Class);

use TestSetup;
use Zaaksysteem::Model::Attributes;

sub setup_attribute_model : Test(startup) {
    shift->{ model } = Zaaksysteem::Model::Attributes->new(schema => $schema);
}

sub attribute_hydration : Tests {
    my $attributes = shift->{ model };

    $zs->zs_transaction_ok(sub {
        my @attrs = $attributes->search('uname');

        ok scalar(@attrs), 'Search for system attribute returns at least on result';

        my $attr = (grep { $_->{ object }{ source } eq 'sys-attr' } @attrs)[0];

        is_deeply(
            $attr,
            {
                id          => 'system.uname',
                is_multiple => 0,
                label       => 'Uname',
                object_type => 'attribute',
                object      => {
                    column_name => 'system.uname',
                    source      => 'sys-attr',
                    value_type  => 'text'
                }
            },
            'Formatting of returned system attribute is correct'
        );
    }, 'attr uname hydrated properly by model');
}

sub bibliotheek_attribute_hydration : Tests {
    my $self = shift;

    $zs->zs_transaction_ok(sub {
        $zs->create_bibliotheek_kenmerk_ok(
            naam          => 'aaaaa',
            magic_string  => 'non_multi',
            value_type    => 'text',
            type_multiple => 0,
        );
        {
            my @attrs = $self->{model}->search('aaaaa');

            ok scalar(@attrs), 'Search for bibliotheek attribute by name returns at least one result';

            my $attr = $attrs[0];

            is_deeply(
                $attr,
                {
                    id          => $attr->{id},
                    is_multiple => 0,
                    label       => 'aaaaa',
                    object_type => 'attribute',
                    object      => {
                        column_name => 'attribute.non_multi',
                        source      => 'db-attr',
                        value_type  => 'text'
                    }
                },
                'Formatting of returned db attribute is correct'
            );
        }

        $zs->create_bibliotheek_kenmerk_ok(
            naam          => 'bbbbb',
            magic_string  => 'multi',
            value_type    => 'text',
            type_multiple => 1,
        );
        {
            my @attrs = $self->{model}->search('bbbbb');

            ok scalar(@attrs), 'Search for bibliotheek attribute by name returns at least one result';

            my $attr = $attrs[0];

            is_deeply(
                $attr,
                {
                    id          => $attr->{id},
                    is_multiple => 1,
                    label       => 'bbbbb',
                    object_type => 'attribute',
                    object      => {
                        column_name => 'attribute.multi',
                        source      => 'db-attr',
                        value_type  => 'text'
                    }
                },
                'Formatting of returned db attribute (multi-value) is correct'
            );
        }

    }, 'attr uname hydrated properly by model');
}

sub bibliotheek_kenmerken : Tests {
    my $attributes = shift->{ model };

    $zs->zs_transaction_ok(sub {
        my $bibkenmerk = $zs->create_bibliotheek_kenmerk_ok(
            naam => 'somecomplexname',
            magic_string => 'testes123',
            values => [
                { value => 'Optie A', active => 1 },
                { value => 'Optie B', active => 0 }
            ]
        );

        is $bibkenmerk->bibliotheek_kenmerken_values->count, 2, 'attr/values create_bibliotheek_kenmerk creates values';

        my ($attr) = $attributes->search('somecomplexname');

        is ref $attr, 'HASH', 'hydration returns a hashref';
        is ref $attr->{ object }, 'HASH', 'hydration returns hashref object';
        is ref $attr->{ object }{ values }, 'ARRAY', 'hydration returns arrayref for attr values';
        is     $attr->{ object }{ column_name }, 'attribute.testes123';

        my @active_opts = grep { $_->{ active } == 1 } @{ $attr->{ object }{ values } };
        my @inactive_opts = grep { $_->{ active } == 0 } @{ $attr->{ object }{ values } };

        is scalar(@active_opts), 1, 'exactly one active option';
        is scalar(@inactive_opts), 1, 'exactly one inactive option';

        is $active_opts[0]->{ value }, 'Optie A', 'value correctly injected';
        is $inactive_opts[0]->{ value }, 'Optie B', 'value correctly injected';
    }, 'attr/values creating bibliotheek_kenmerken with values hydrate as expected');
}

sub deleted_attributes : Tests {
    my $attributes = shift->{ model };

    # ZS-2145
    $zs->zs_transaction_ok(sub {
        my $bib = $zs->create_bibliotheek_kenmerk_ok(naam => 'testytesty');

        my @attrs = $attributes->search('testytesty');
        my $count = scalar(@attrs);

        ok $count > 0, 'found test attribute';

        $bib->delete;

        @attrs = $attributes->search('testytesty');

        is scalar(@attrs), $count - 1, 'did not find attribute after deletion';
    }, 'attr deleted attributes are not hydrated in objectsearch');
}

sub _create_test_case {
    my $zaaktype_node = $zs->create_zaaktype_node_ok;
    my $casetype = $zs->create_zaaktype_ok(node => $zaaktype_node);

    my $zaaktype_status = $zs->create_zaaktype_status_ok(
        status => 1,
        fase   => 'registratiefase',
        node   => $zaaktype_node
    );

    my $afhandelfase = $zs->create_zaaktype_status_ok(
        status => 2,
        fase   => 'afhandelfase',
        node   => $zaaktype_node
    );

    my $valuta_kenmerk = $zs->create_bibliotheek_kenmerk_ok(
        naam => 'valuta1',
        magic_string => 'magic_string_valuta',
        value_type => 'valuta',
    );
    my $bag_kenmerk = $zs->create_bibliotheek_kenmerk_ok(
        naam => 'bag',
        magic_string => 'magic_string_bag',
        value_type => 'bag_openbareruimte',
    );
    my $multibag_kenmerk = $zs->create_bibliotheek_kenmerk_ok(
        naam => 'bag',
        magic_string => 'magic_string_multibag',
        value_type => 'bag_openbareruimtes',
    );

    my $valuta_zt_kenmerk = $zs->create_zaaktype_kenmerk_ok(
        status              => $zaaktype_status,
        bibliotheek_kenmerk => $valuta_kenmerk,
    );
    my $bag_zt_kenmerk = $zs->create_zaaktype_kenmerk_ok(
        status              => $zaaktype_status,
        bibliotheek_kenmerk => $bag_kenmerk,
    );
    my $multibag_zt_kenmerk = $zs->create_zaaktype_kenmerk_ok(
        status              => $zaaktype_status,
        bibliotheek_kenmerk => $multibag_kenmerk,
    );

    my $case = $zs->create_case_ok(zaaktype => $casetype);

    return (
        $case,
        {
            multibag => $multibag_kenmerk->id,
            bag      => $bag_kenmerk->id,
            valuta   => $valuta_kenmerk->id
        }
    );
}

sub test_typed_casetype_attributes : Tests {
    my $attributes = shift->{ model };

    $zs->zs_transaction_ok(sub {
        my ($case, $kenmerken) = _create_test_case();

        my $bag_openbareruimte = $zs->create_bag_openbareruimte_ok();
        my $bag_openbareruimte2 = $zs->create_bag_openbareruimte_ok();

        $case->zaak_kenmerken->replace_kenmerk({
            bibliotheek_kenmerken_id    => $kenmerken->{valuta},
            zaak_id                     => $case->id,
            values                      => ["3.14"]
        });
        $case->zaak_kenmerken->replace_kenmerk({
            bibliotheek_kenmerken_id    => $kenmerken->{bag},
            zaak_id                     => $case->id,
            values                      => [$bag_openbareruimte->identificatie]
        });
        $case->zaak_kenmerken->replace_kenmerk({
            bibliotheek_kenmerken_id    => $kenmerken->{multibag},
            zaak_id                     => $case->id,
            values                      => [$bag_openbareruimte->identificatie, $bag_openbareruimte2->identificatie]
        });

        my $od = $schema->resultset("ObjectData")->find_or_create_by_object_id(case => $case->id);

        is_deeply(
            $od->get_object_attribute('attribute.magic_string_valuta')->value,
            '3.14',
            "Valuta attribute value was stored correctly"
        );

        is_deeply(
            $od->get_object_attribute('attribute.magic_string_bag')->value,
            {
                bag_id           => $bag_openbareruimte->identificatie,
                human_identifier => undef,
                address_data     => undef,
            },
            "BAG attribute was stored correctly"
        );
        is_deeply(
            $od->get_object_attribute('attribute.magic_string_multibag')->value,
            [
                {
                    bag_id           => $bag_openbareruimte->identificatie,
                    human_identifier => undef,
                    address_data     => undef,
                },
                {
                    bag_id           => $bag_openbareruimte2->identificatie,
                    human_identifier => undef,
                    address_data     => undef,
                },
            ],
            "Multi-value BAG attribute was stored correctly"
        );

        $case->zaak_kenmerken->replace_kenmerk({
            bibliotheek_kenmerken_id    => $kenmerken->{valuta},
            zaak_id                     => $case->id,
            values                      => ["6,28"] # note: ',' not '.'
        });

        $od->discard_changes();
        is_deeply(
            $od->get_object_attribute('attribute.magic_string_valuta')->value,
            '6.28',
            "Valuta attribute value was stored correctly even with , separator"
        );
    }, 'Casetype attribute type mapping');
}

# Further testing requires create_kenmerk_ok!
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

