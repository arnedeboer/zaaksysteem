package TestFor::General::ObjectData;
use base qw(Test::Class);

use TestSetup;
use Zaaksysteem::Object::Model;
use Zaaksysteem::Search::ZQL;



# XXX TODO evict with extreme prejudice
sub create_object_ok {
    my $case_id      = shift || 1;
    my $relationship = shift;

    return $schema->resultset('ObjectData')->create(
        {
            object_class    => 'case',
            object_id       => $case_id,
            properties      => {
                values => {
                    'case.number'           => Zaaksysteem::Object::Attribute->new(
                        name            => 'case.number',
                        value           => $case_id,
                        attribute_type  => 'integer',
                    )->TO_JSON,
                    'case.casetype.name'    => Zaaksysteem::Object::Attribute->new(
                        name            => 'case.casetype.name',
                        value           => 'Casetype title',
                        attribute_type  => 'text',
                    )->TO_JSON,
                    'case.casetype.motivation'    => Zaaksysteem::Object::Attribute->new(
                        name            => 'case.casetype.motivation',
                        value           => undef,
                        attribute_type  => 'text',
                    )->TO_JSON,
                },
            },
            index_hstore => Pg::hstore::encode({
                'case.number'           => Zaaksysteem::Object::Attribute->new(
                    name            => 'case.number',
                    value           => $case_id,
                    attribute_type  => 'integer',
                )->human_value,
                'casetype.name'    => Zaaksysteem::Object::Attribute->new(
                    name            => 'case.casetype.name',
                    value           => 'Casetype title',
                    attribute_type  => 'text',
                )->human_value,
                'case.casetype.motivation' => Zaaksysteem::Object::Attribute->new(
                    name => 'case.casetype.motivation',
                    value => undef,
                    attribute_type => 'text'
                )->human_value
            }),
        }
    );
}

sub test_object_create : Tests {
    my $object = Zaaksysteem::Object::Model->new(schema => $schema);

    ok($object, 'Loaded Zaaksysteem::Object::Model');

    throws_ok(
        sub { Zaaksysteem::Search::ZQL->new('SELECT {} FROM') },
        'Zaaksysteem::Exception::Base',
        'Throws on missing object type'
    );

    # The previous throws_ok that used to be here tested for
    # non-existant object types. This is incorrect, as the object_type
    # is a dynamic field, and could be filled with anything.
    # As objects get loaded into the database, empty resultsets on
    # certain object_type queries are expected to return nothing if no
    # records exist, not die violently.
}

sub retrieve_case_objects : Tests {
    $zs->zs_transaction_ok(sub {
        create_object_ok();

        my $object = Zaaksysteem::Object::Model->new(
            schema  => $schema
        );

        my $rs = $object->rs;

        ok(
            $rs->can('_apply_zql_roles'),
            'ResultSet has role ObjectResultSet'
        );

        ok(
            $rs->count,
            'Succesfully retrieved objects from type "case": ' . $rs->count
        );

        $rs->reset;
        ok(
            $rs->next->can('_is_objectcomponent'),
            'rs->next correctly returned row with ObjectComponent role'
        );

        ok(
            $rs->first->can('_is_objectcomponent'),
            'rs->first correctly returned row with ObjectComponent role'
        );

        ok(
            $rs->single->can('_is_objectcomponent'),
            'rs->single correctly returned row with ObjectComponent role'
        );

        $rs->reset;
        my @rows = $rs->all;
        for (my $i = 0; $i < scalar(@rows); $i++) {
            ok(
                $rows[$i]->can('_is_objectcomponent'),
                'rs->all->[' . $i . '] correctly returned row with ObjectComponent role'
            )
        }

        ok(
            !$schema->resultset('Zaak')->can('_apply_zql_roles'),
            'ResultSet retrieved via normal ways do NOT have role ObjectResultSet'
        );
    }, 'Verified retrieving of case objects');
}

sub verify_json_retrieval : Tests {
    my @REQUIRED_OBJECT_RESULT_PARAMS = qw/
        object_type
        id
        values
    /;

    $zs->zs_transaction_ok(sub {
        create_object_ok();

        my $object = Zaaksysteem::Object::Model->new(
            schema  => $schema
        );

        my $rs = $object->rs;

        my $case    = $rs->first;
        my $json    = $case->TO_JSON;
        ok((exists $json->{ $_ } && $json->{ $_ }), 'Found JSON key: ' . $_)
            for @REQUIRED_OBJECT_RESULT_PARAMS;

        isa_ok($json->{values}, 'HASH', 'JSON->{values}');
        # note(explain($json));
    }, 'Verified retrieving of correct JSON data');
}

sub verify_object_from_zql : Tests {
    $zs->zs_transaction_ok(sub {
        create_object_ok($_) for 1..10;

        my $object = Zaaksysteem::Object::Model->new(
            schema  => $schema
        );

        my $zql = Zaaksysteem::Search::ZQL->new('SELECT {} FROM case');

        my $rs = $zql->apply_to_resultset($object->rs);

        ok(
            $rs->can('_apply_zql_roles'),
            'ResultSet has role ObjectResultSet'
        );

        is($rs->count, 10, 'Retrieved 10 cases');

        my $zql2 = Zaaksysteem::Search::ZQL->new('SELECT {} FROM case WHERE case.number = 4');

        $rs = $zql2->apply_to_resultset($object->rs);

        is($rs->count, 1, 'Retrieved 1 case');

    }, 'Verified retrieving from ZQL query');
}

sub zql_with_limit : Tests {
    $zs->zs_transaction_ok(sub {
        create_object_ok($_) for 1..10;

        my $object = Zaaksysteem::Object::Model->new(
            schema  => $schema
        );

        my $zql = Zaaksysteem::Search::ZQL->new('SELECT {} FROM case LIMIT 5');

        my $rs = $zql->apply_to_resultset($object->rs);

        is($rs->count, 5, 'Limit to 5 results');

        my $zql2 = Zaaksysteem::Search::ZQL->new('SELECT {} FROM case LIMIT 2');

        my $rs2 = $zql2->apply_to_resultset($object->rs);

        is($rs2->count, 2, 'Limit to 2 results');
    }, 'Verified limiting of SQL queries');
}

sub zql_count : Tests {
    $zs->zs_transaction_ok(sub {
        create_object_ok($_) for 1..10;

        my $model = Zaaksysteem::Object::Model->new(
            schema => $schema
        );

        my $zql = Zaaksysteem::Search::ZQL->new('COUNT case');
        my $rs = $zql->apply_to_resultset($model->rs);

        is $rs->count, 10, 'ZQL COUNT applies cleanly';

        my $object = $rs->first;

        my $zql2 = Zaaksysteem::Search::ZQL->new(sprintf(
            'COUNT case WHERE case.number = "%s"',
            $object->object_id
        ));

        my $rs2 = $zql2->apply_to_resultset($model->rs);

        is $rs2->count, 1, 'ZQL COUNT with WHERE clause applies cleanly';

        my $zql3 = Zaaksysteem::Search::ZQL->new(sprintf(
            'COUNT case MATCHING "case"',
            $object->object_id
        ));

        my $rs3 = $zql3->apply_to_resultset($model->rs);

        is $rs3->count, 10, 'ZQL COUNT with MATCHING clause applies cleanly';
    });
}

sub zql_single_column : Tests {
    $zs->zs_transaction_ok(sub {
        create_object_ok();

        my $object = Zaaksysteem::Object::Model->new(
            schema  => $schema
        );

        my $zql = Zaaksysteem::Search::ZQL->new('SELECT WITH DESCRIPTION case.number FROM case');

        my $rs = $zql->apply_to_resultset($object->rs);
        my $row     = $rs->first;

        ok(
            (scalar keys %{ $row->TO_JSON->{values}} == 1),
            'Found exactly one column'
        );
        ok(
            (scalar @{ $row->TO_JSON->{describe}->{attributes}} == 1),
            'Found exactly one colum in describe'
        );
        ok(
            $row->TO_JSON->{values}->{'case.number'},
            'Found column case.number'
        );
    }, 'Verified single column selection ZQL query');
}

sub multicolumn_describe : Tests {
    $zs->zs_transaction_ok(sub {
        create_object_ok();

        my $object = Zaaksysteem::Object::Model->new(schema => $schema);

        my $zql = Zaaksysteem::Search::ZQL->new('SELECT WITH DESCRIPTION {} FROM case');

        my $rs = $zql->apply_to_resultset($object->rs);
        my $row = $rs->first;

        ok(
            (scalar keys %{ $row->TO_JSON->{values}} > 1),
            'Found multiple columns'
        );
        ok(
            (scalar @{ $row->TO_JSON->{describe}->{attributes}} > 1),
            'Found multiple columns in describe'
        );
    }, 'Verified multiple columns in select and describe by default');
}

sub zql_column_selection : Tests {
    $zs->zs_transaction_ok(sub {
        create_object_ok();

        my $object = Zaaksysteem::Object::Model->new(schema => $schema);

        my $zql = Zaaksysteem::Search::ZQL->new('SELECT WITH DESCRIPTION case.number, case.casetype.name FROM case');

        my $rs = $zql->apply_to_resultset($object->rs);
        my $row = $rs->first;

        ok(
            (scalar keys %{ $row->TO_JSON->{values} } == 2),
            'Found exactly two columns'
        );
        ok(
            (scalar @{ $row->TO_JSON->{ describe }{ attributes } } == 2),
            'Found exactly two colums in describe'
        );
        ok(
            $row->TO_JSON->{values}->{$_},
            'Found column ' . $_
        ) for qw/case.number case.casetype.name/;

    }, 'Verified column selection from ZQL query');
}

sub zql_description : Tests {
    $zs->zs_transaction_ok(sub {

        create_object_ok();

        my $object = Zaaksysteem::Object::Model->new(
            schema  => $schema
        );

        my $zql = Zaaksysteem::Search::ZQL->new('SELECT WITH DESCRIPTION {} FROM case LIMIT 1');

        my $rs = $zql->apply_to_resultset($object->rs);
        my $row     = $rs->first;

        ok(exists $row->TO_JSON->{describe}, 'Found TO_JSON->{describe}');
        isa_ok($row->TO_JSON->{describe}, 'HASH', 'TO_JSON->{describe}');
    }, 'Verified existence of describe keys in JSON representation');

    $zs->zs_transaction_ok(sub {
        create_object_ok();

        my $object = Zaaksysteem::Object::Model->new(schema => $schema);

        my $zql = Zaaksysteem::Search::ZQL->new('SELECT {} FROM case LIMIT 1');

        my $rs = $zql->apply_to_resultset($object->rs);

        my $row = $rs->first;

        ok(!$row->TO_JSON->{describe}, 'Did not find TO_JSON->{describe} when not asked');

    }, 'No description requested == no description supplied');
}

sub test_find_or_create_by_object_id : Tests {
    $zs->zs_transaction_ok(sub {
        throws_ok(
            sub {
                $schema->resultset('ObjectData')->find_or_create_by_object_id();
            },
            qr/Need at least object_class/,
            'throws_ok: Needs at least two parameters, missing 2'
        );

        throws_ok(
            sub {
                $schema->resultset('ObjectData')->find_or_create_by_object_id(
                    'case'
                );
            },
            qr/Need at least object_class/,
            'throws_ok: Needs at least two parameters, missing 1'
        );

        throws_ok(
            sub {
                $schema->resultset('ObjectData')->find_or_create_by_object_id(
                    'case', 'blaat'
                );
            },
            qr/Need at least object_class/,
            'throws_ok: Needs at least an id for second parameter, gave word'
        );

        my $case_object = $schema->resultset('ObjectData')->find_or_create_by_object_id(
            'case', 1
        );

        ok($case_object, 'Created case_object with find_or_create_by_object_id');
        is($case_object->object_class, 'case','Objectclass: ' . $case_object->object_class);
        is($case_object->object_id, 1,'Objectid: ' . $case_object->object_id);

        my $case_object2 = $schema->resultset('ObjectData')->find_or_create_by_object_id(
            'case', 1
        );

        is($case_object->uuid, $case_object2->uuid, 'Found existing case object with find_or_create_by_object_id');
    }, 'rs->find_or_create_by_object_id: Create object with object_id');
}

sub textvector_queries : Tests {
    $zs->zs_transaction_ok(sub {
        my $object = create_object_ok;

        my $objects = Zaaksysteem::Object::Model->new(schema => $schema)->rs;

        my $rs = $objects->search_text_vector('Casetype');

        ok $rs->count, 'Simple text vector search results in rows returned';

        my $rs2 = $objects->search_text_vector('not_in_vector');

        ok $rs2->count == 0, 'Simple text vector search with non-matching keyword returns 0 rows';

        my $rs3 = $objects->search_text_vector('CASeTypE');

        ok $rs3->count, 'Text vector search is case-insentitive';

        my $rs4 = $objects->search_text_vector('title casetype');

        ok $rs4->count, 'Dislocated query is executed properly';
    });

    $zs->zs_transaction_ok(sub {
        my $object = create_object_ok;
        my $objects = Zaaksysteem::Object::Model->new(schema => $schema)->rs;

        my $zql = Zaaksysteem::Search::ZQL->new('SELECT {} FROM case MATCHING "Casetype"');

        my $rs = $zql->apply_to_resultset($objects);

        ok($rs->count, 'Found cases');

        my $item = $rs->first;

        is $item->uuid, $object->uuid, 'Select query with matching condition returns expected object';

        my $zql2 = Zaaksysteem::Search::ZQL->new('SELECT {} FROM case MATCHING "bit rot"');

        my $rs2 = $zql2->apply_to_resultset($objects);

        ok $rs2->count == 0, 'Select query with non-matching condition returns 0 rows';
    });

    $zs->zs_transaction_ok(sub {
        my $object = create_object_ok;
        my $objects = Zaaksysteem::Object::Model->new(schema => $schema)->rs;

        my $zql = Zaaksysteem::Search::ZQL->new('SELECT {} FROM case MATCHING "Casetype" WHERE case.number = 1');

        my $rs = $zql->apply_to_resultset($objects);

        ok($rs->count, 'Found exactly one case');

        my $item = $rs->first;

        is $item->uuid, $object->uuid, 'Select query with matching condition and where clause returns expected object';
    });

    $zs->zs_transaction_ok(sub {
        my $object = create_object_ok;
        my $objects = Zaaksysteem::Object::Model->new(schema => $schema)->rs;

        my $zql = Zaaksysteem::Search::ZQL->new('SELECT {} FROM case MATCHING "Casetypee" WHERE case.number = 1');

        my $rs = $zql->apply_to_resultset($objects);

        ok $rs->count == 0, 'Select query with mis-matching condition and true where condition returns 0 rows';
    });

    $zs->zs_transaction_ok(sub {
        my $object = create_object_ok;
        my $objects = Zaaksysteem::Object::Model->new(schema => $schema)->rs;

        my $zql = Zaaksysteem::Search::ZQL->new('SELECT {} FROM case MATCHING "Casetype" WHERE case.number = 2');

        my $rs = $zql->apply_to_resultset($objects);

        ok $rs->count == 0, 'Select query with matching condition and false where condition returns 0 rows';
    });
}

sub order_by : Tests {
    $zs->zs_transaction_ok(sub {
        create_object_ok($_) for 1..20;

        my $objects = Zaaksysteem::Object::Model->new(schema => $schema)->rs;

        my $zql = Zaaksysteem::Search::ZQL->new('SELECT {} FROM case ORDER BY case.number');

        my $rs = $zql->apply_to_resultset($objects);

        is $rs->count, 20, 'Select query with numeric order by has count equal to objects created';

        is_deeply [ map { $_->object_id } $rs->all ],
            [qw[1 10 11 12 13 14 15 16 17 18 19 2 20 3 4 5 6 7 8 9]],
            'select/order_by/alphanumeric returns expected order of objects';

        my $rs2 = Zaaksysteem::Search::ZQL->new('SELECT {} FROM case NUMERIC ORDER BY case.number')->apply_to_resultset($objects);

        is_deeply [ map { $_->object_id } $rs2->all ],
            [ 1..20 ],
            'select/order_by/numeric returns expected order of objects';

        my $rs3 = Zaaksysteem::Search::ZQL->new('SELECT {} FROM case NUMERIC ORDER BY casetype.name')->apply_to_resultset($objects);

        lives_ok { $rs3->first; } 'select/order_by/numeric on non-integer field lives';
    });
}

sub distinct_queries : Tests {
    $zs->zs_transaction_ok(sub {
        create_object_ok $_ for 1..10;

        my $objects = Zaaksysteem::Object::Model->new(schema => $schema)->rs;
        my $zql = Zaaksysteem::Search::ZQL->new('SELECT DISTINCT case.casetype.name FROM case');

        my $rs = $zql->apply_to_resultset($objects);

        my %cols = $rs->first->get_columns;

        is $cols{ count }, 10, 'Select distinct query returns correct count';
        is $cols{ 'case$casetype$name' }, 'Casetype title', 'Select distinct query returns correct column value';
    }, 'Select distinct query returns proper resultset');

    $zs->zs_transaction_ok(sub {
        create_object_ok $_ for 1..10;

        my $objects = Zaaksysteem::Object::Model->new(schema => $schema)->rs;
        my $zql = Zaaksysteem::Search::ZQL->new('SELECT DISTINCT casetype.name, case.number FROM case');

        my $rs = $zql->apply_to_resultset($objects);

        for my $object ($rs->all) {
            is $object->get_column('count'), 1, 'Select distinct with multiple columns returns objects with count == 1';
        }

    }, 'Select distinct query returns proper resultset');
}

sub clear_relations : Tests {
    $zs->zs_transaction_ok(sub {
        create_object_ok 1;
        create_object_ok 2;

        my $objects = Zaaksysteem::Object::Model->new(schema => $schema)->rs;
        my $zql = Zaaksysteem::Search::ZQL->new('SELECT {} FROM case');
        my $relations = $schema->resultset('ObjectRelationships');

        my ($a, $b) = $zql->apply_to_resultset($objects)->all;

        my $rel = $relations->create(
            {
                object1_uuid => $a->uuid,
                object2_uuid => $b->uuid,
                object1_type => $a->object_class,
                object2_type => $b->object_class,
                type1        => 'plain',
                type2        => 'plain'
            }
        );

        is $relations->count, 1, 'exactly 1 relation created';

        $a->delete;

        is $relations->count, 0, 'exactly 0 relations found after object deletion';
    }, 'Relations get cleared during object deletion');
}

sub zql_isnull : Tests {
    $zs->zs_transaction_ok(sub {
        create_object_ok 1;

        my $objects = Zaaksysteem::Object::Model->new( schema => $schema )->rs;
        my $zql = Zaaksysteem::Search::ZQL->new('SELECT case.number FROM case WHERE case.casetype.motivation = NULL');

        my $rs = $zql->apply_to_resultset($objects);

        lives_ok sub { $rs->count }, 'object/zql/is_null query lives';

        is $rs->count, 1, 'object/zql/is_null returns exactly 1 object';
    });
}

sub text_vector_updates : Tests {
    $zs->zs_transaction_ok(sub {
        my $object = create_object_ok;

        my $zql = Zaaksysteem::Search::ZQL->new('SELECT {} FROM case MATCHING "Casetype"');

        my $objects = Zaaksysteem::Object::Model->new(schema => $schema)->rs;

        my $rs = $zql->apply_to_resultset($objects);

        is $rs->count, 1, 'Updating object text_vectors produces matching row in db';

        is $object->uuid, $rs->first->uuid, 'Objects match';
    }, 'Text vector updates executed properly');
}

sub bibliotheek_product_entry_sync : Tests {
    use Zaaksysteem::Object::Attribute;

    $zs->zs_transaction_ok(sub {
        my $category = $zs->create_bibliotheek_categorie_ok;

        my $object = $zs->create_object_data_ok(
            object_class => 'product',
            attributes => {
                name => Zaaksysteem::Object::Attribute->new(
                    name => 'name',
                    value => 'Je suis un produit',
                    attribute_type => 'text'
                ),
                search_terms => Zaaksysteem::Object::Attribute->new(
                    name => 'search_terms',
                    value => 'This is the search term',
                    attribute_type => 'text'
                ),
                category_id => Zaaksysteem::Object::Attribute->new(
                    name => 'category_id',
                    value => $category->id,
                    attribute_type => 'integer'
                )
            }
        );

        my $shim = $zs->schema->resultset('ObjectBibliotheekEntry')->search({},{order_by => {'-desc' => 'me.id'} })->first;

        isa_ok $shim, 'DBIx::Class::Row', 'Got result from BibliotheekEntry table';

        is $shim->get_column('object_uuid'), $object->uuid, 'uuid in shim matches UUID of object_data';
        is $shim->get_column('bibliotheek_categorie_id'), $category->id, 'bibliotheek_categorie_id is hardcoded 2';

        is $shim->search_term, 'Je suis un produit This is the search term', 'search_term in shim is as expected';
        is $shim->object_type, 'product', 'type in shim is hardcoded to "product"';

        is $category->object_bibliotheek_entries->count, 1,
            'getting bibliotheek_entry from the category context works';

    }, 'New object_data with type "product" syncs to catalogue table');
}

sub bibliotheek_type_entry_sync : Tests {
    use Zaaksysteem::Object::Attribute;

    $zs->zs_transaction_ok(sub {
        my $category = $zs->create_bibliotheek_categorie_ok;

        my $object = $zs->create_object_data_ok(
            object_class => 'type',
            attributes => {
                name => Zaaksysteem::Object::Attribute->new(
                    name => 'name',
                    value => 'I am an Object-type instance',
                    attribute_type => 'text'
                ),
                category_id => Zaaksysteem::Object::Attribute->new(
                    name => 'category_id',
                    value => $category->id,
                    attribute_type => 'integer'
                )
            }
        );

        my $shim = $zs->schema->resultset('ObjectBibliotheekEntry')->search({},{order_by => {'-desc' => 'me.id'} })->first;


        isa_ok $shim, 'DBIx::Class::Row', 'Got result from BibliotheekEntry table';

        is $shim->get_column('object_uuid'), $object->uuid, 'uuid in shim matches UUID of object_data';
        is $shim->get_column('bibliotheek_categorie_id'), $category->id, 'bibliotheek_categorie_id is hardcoded 2';

        is $shim->search_term, 'I am an Object-type instance', 'search_term in shim matches UUID of object_data';
        is $shim->object_type, 'type', 'type in shim is hardcoded to "type"';

        is $category->object_bibliotheek_entries->count, 1,
            'getting bibliotheek_entry from the category context works';

    }, 'New object_data with type "type" syncs to catalogue table');
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

