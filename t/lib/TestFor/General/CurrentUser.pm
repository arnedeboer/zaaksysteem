package TestFor::General::CurrentUser;
use base qw(Test::Class);

use TestSetup;

sub zs_current_user_groups : Tests {
    $zs->txn_ok(
        sub {
            my $user = $zs->set_current_user;

            my $groups = $user->all_available_groups;
            is(@$groups, 4, "four groups found");

            my $group = $user->get_group_by_id(2);
            is($group->id, 2, "Got group two");
            is($group->name, "Backoffice", "Group is backoffice");

            {
                my $group = $user->find_group_by_id('42');
                is($group, undef, "Find returns undef when we cannot find it");

                throws_ok(
                    sub {
                        $user->get_group_by_id('42');
                    },
                    qr#zs/group: #,
                    "get dies when it cannot find a group"
                );
            }

            my $roles = $user->all_available_roles;
            is(@$roles, 12, "12 roles found");

            my $role = $user->get_role_by_id(2);
            is($role->id, 2, "Got role two");
            is($role->name, "Zaaksysteembeheerder", "Role is Zaaksysteembeheerder");

            {
                my $role = $user->find_role_by_id('42');
                is($role, undef, "Find returns undef when we cannot find it");

                throws_ok(
                    sub {
                        $user->get_role_by_id('42');
                    },
                    qr#zs/role: #,
                    "get dies when it cannot find a role"
                );
            }

        },
        "Current user"
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
