package TestFor::General::Types;
use base qw(Test::Class);

use TestSetup;
use JSON::XS;

sub zs_types_boolean : Tests {
    my $typo    = XXX::Test::Types->new();
    my ($json, $strjson);

    $typo->active(1);
    $typo->active_json(1);

    $json       = JSON::XS->new->utf8->allow_nonref->allow_blessed->convert_blessed;
    $strjson    = $json->encode($typo);

    like($strjson, qr/"active":"1"/, 'Moose default Bool has correct implementation');
    like($strjson, qr/"active_json":1/, 'Customer Boolean does correct JSON inflaction');

    $typo->active_json(JSON::XS::true);
    $strjson    = $json->encode($typo);
    like($strjson, qr/"active_json":1/, 'JSON::XS::true inflates correctly to 1');

    $typo->active_json(JSON::XS::false);
    $strjson    = $json->encode($typo);
    like($strjson, qr/"active_json":0/, 'JSON::XS::false inflates correctly to 0');

}

sub zs_types_jsonboolean : Tests {
    my $typo    = XXX::Test::Types->new();
    my ($json, $strjson);

    $json       = JSON::XS->new->utf8->allow_nonref->allow_blessed->convert_blessed;

    $typo->true_or_false(JSON::XS::false);
    $strjson    = $json->encode($typo);
    like($strjson, qr/"true_or_false":false/, 'JSON::XS::false inflates correctly to false');

    $typo->true_or_false(JSON::XS::true);
    $strjson    = $json->encode($typo);
    like($strjson, qr/"true_or_false":true/, 'JSON::XS::false inflates correctly to true');

    $typo->true_or_false(1);
    $strjson    = $json->encode($typo);
    like($strjson, qr/"true_or_false":true/, 'JSON::XS::false inflates correctly to true');

    $typo->true_or_false(0);
    $strjson    = $json->encode($typo);
    like($strjson, qr/"true_or_false":false/, 'JSON::XS::false inflates correctly to false');

    $typo->array_params([{ test => 0, test2 => 1, test3 => "test"}]);
    $strjson    = $json->encode($typo);
    like($strjson, qr/"array_params":\[\{"test":0/, 'Array with hash with param: test => 0 inflates correctly to JSON');
    # diag(explain($strjson));

    $typo->hash_params({ test => 0, test2 => 1, test3 => "test", test4 => { one => 1, two => "2"}});
    $strjson    = $json->encode($typo);
    like($strjson, qr/"hash_params":\{"test":0/, 'Hash with param: test => 0 inflates correctly to JSON');

    $zs->zs_transaction_ok(sub {
        my $o = $schema->resultset('ObjectData')->create({
            object_class    => 'custom',
            properties      => $strjson,
        });

        my $object = $schema->resultset('ObjectData')->find($o->uuid);


        $typo->hash_params($object->properties->{hash_params});

        $strjson = $json->encode($typo);

        like($strjson, qr/"hash_params":\{"test":0/, 'Hash with param: test => 0 inflates correctly to JSON');

    }, 'Checked back and forth from database');


}

sub zs_types_networking : Tests {
    my $zs = ZS::Test::Dummy->new();

    my $ipv4  = '193.134.5.5';
    my $ipv6  = '2001:0db8:0000:0000:0000:0000:1428:57ab';

    my $error = qr/Attribute \(network_acl\) does not pass/;
    my $error_array = qr/Attribute \(network_acls\) does not pass/;

    subtest ipv4 => sub {
        lives_ok(
            sub {
                $zs->ipv4($ipv4);
            },
            "Valid IPv4"
        );

        lives_ok(
            sub {
                $zs->ipv4("$ipv4/32");
            },
            "Valid IPv4 CIDR"
        );

        lives_ok(
            sub {
                $zs->ipv4("$ipv4/28");
            },
            "Valid IPv4 CIDR"
        );

        throws_ok(
            sub {
                $zs->ipv4("$ipv4/123");
            },
            qr/Attribute \(ipv4\) does not pass/,
            "Invalid CIDR"
        );

        throws_ok(
            sub {
                $zs->ipv4("194.134.5.555");
            },
            qr/Attribute \(ipv4\) does not pass/,
            "Invalid ipv4"
        );
    };

    subtest ipv6 => sub {
        lives_ok(
            sub {
                $zs->ipv6($ipv6);
            },
            "Valid IPv6"
        );

        lives_ok(
            sub {
                $zs->ipv6("$ipv6/34");
            },
            "Valid IPv6 CIDR"
        );

        lives_ok(
            sub {
                $zs->ipv6("$ipv6/28");
            },
            "Valid IPv6 CIDR"
        );

        throws_ok(
            sub {
                $zs->ipv6("$ipv6/0");
            },
            qr/Attribute \(ipv6\) does not pass/,
            "Invalid CIDR: /0"
        );

        throws_ok(
            sub {
                $zs->ipv6("$ipv6/65");
            },
            qr/Attribute \(ipv6\) does not pass/,
            "Invalid CIDR: /65"
        );

        throws_ok(
            sub {
                $zs->ipv6("196.136.5.555");
            },
            qr/Attribute \(ipv6\) does not pass/,
            "Invalid ipv6: ipv4 is not ipv6"
        );
    };

    foreach ($ipv4, $ipv6) {
        lives_ok(
            sub {
                $zs->network_acl($_);
            },
            "Valid IP: $_"
        );

        lives_ok(
            sub {
                $zs->network_acls([$_]);
            },
            "Valid IP: $_"
        );

        lives_ok(
            sub {
                $zs->network_acl("$_ Some comment");
            },
            "Valid IP $_ + comment"
        );

        lives_ok(
            sub {
                $zs->network_acls(["$_ Some comment"]);
            },
            "Valid IP $_ + comment"
        );

        throws_ok(
            sub {
                $zs->network_acl("${_}Some comment");
            },
            $error,
            "Valid IP $_ and comment with no spaces"
        );

        throws_ok(
            sub {
                $zs->network_acls(["${_}Some comment"]);
            },
            $error_array,
            "Valid IP $_ and comment with no spaces"
        );

        TODO: {
            local $TODO = 'CIDR notation is not yet supported';

            lives_ok(
                sub {
                    $zs->network_acl("$_/32");
                },
                "Valid CIDR $_"
            );

            lives_ok(
                sub {
                    $zs->network_acls(["$_/32"]);
                },
                "Valid CIDR $_"
            );
        }
    }

    throws_ok(
        sub {
            $zs->network_acl('NO IPv6 or IPv4');
        },
        $error,
        "Invalid IP",
    );
    throws_ok(
        sub {
            $zs->network_acls(['NO IPv6 or IPv4']);
        },
        $error_array,
        "Invalid IP",
    );
}

package XXX::Test::Types;

use Moose;
use Zaaksysteem::Types qw/Boolean JSONBoolean/;

has active_json => (
    is     => 'rw',
    isa    => Boolean,
    coerce => 1,
);

has active => (
    is     => 'rw',
    isa    => 'Bool',
);

has true_or_false => (
    is     => 'rw',
    isa    => JSONBoolean,
    coerce => 1,
);

has array_params => (
    is     => 'rw',
    isa    => 'ArrayRef'
);

has hash_params => (
    is     => 'rw',
    isa    => 'HashRef'
);

sub TO_JSON {
    my $self    = shift;

    return {
        active_json  => $self->active_json,
        active => $self->active,
        true_or_false => $self->true_or_false,
        array_params => $self->array_params,
        hash_params => $self->hash_params,
    }
}

1;

package ZS::Test::Dummy;
use Moose;
extends 'Zaaksysteem::Object';

use Zaaksysteem::Types qw(ZSNetworkACL ZSNetworkACLs zs_ipv4 zs_ipv6);

has network_acl => (
    is     => 'rw',
    isa    => ZSNetworkACL,
    traits => [qw[OA]],
    label  => "Testing ZSNetworkACL types",
);

has network_acls => (
    is     => 'rw',
    isa    => ZSNetworkACLs,
    traits => [qw[OA]],
    label  => "Testing ZSNetworkACL types",
);

has ipv4 => (
    is     => 'rw',
    isa    => zs_ipv4,
    traits => [qw[OA]],
    label  => "Testing ipv4 types",
);

has ipv6 => (
    is     => 'rw',
    isa    => zs_ipv6,
    traits => [qw[OA]],
    label  => "Testing ipv6 types",
);

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

1;
