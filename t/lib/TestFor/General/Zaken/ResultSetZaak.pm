package TestFor::General::Zaken::ResultSetZaak;
use base qw(Test::Class);

use TestSetup;
use File::Touch;
use File::Temp;
use File::Spec::Functions;
use Cwd 'abs_path';

sub _publish_dir_ok {
    my ($rs, $dir, $msg) = @_;

    is($rs->_make_publish_dir($dir), $msg, "Correct return value");
    ok(-d $dir, "Found dir $dir");

    my $dh;
    opendir $dh, $dir;
    my @files = readdir($dh);

    is(@files, 2, "Only . and .. are present");
    close($dh);
}

sub zs_rs_zaak_publish_dir : Tests {
    $zs->txn_ok(
        sub {

            my $case = $zs->create_case_ok();
            my $rs   = $zs->schema->resultset('Zaak')->search_rs($case->id);

            my $dir = catdir(
                File::Temp->newdir(
                    DIR    => catdir(qw(t inc tmp)),
                    UNLINK => 1
                ),
                'zs'
            );

            _publish_dir_ok($rs, $dir, "Created publish directory '$dir'.\n");

            touch(catfile($dir, 'meuk'));
            _publish_dir_ok($rs, $dir, "Cleaned publish directory '$dir'.\n");
        },

        "Zaken::ResultSetZaak::_publish_dir"
    );
}

sub zs_rs_zaak_publish : Tests {
    $zs->txn_ok(
        sub {
            my $case = $zs->create_case_ok;
            my $rs   = $zs->schema->resultset('Zaak')->search_rs($case->id);

            my $rd = File::Temp->newdir(
                DIR    => catdir(qw(t inc tmp)),
                UNLINK => 1
            );

            my $fd = File::Temp->newdir(
                DIR    => $rd,
                UNLINK => 1
            );

            my $args = {
                root_dir              => $rd,
                files_dir             => $fd,
                publish_script        => 'bin/iets',
                hostname              => 'jo.jo.nl',
                display_fields        => [ 'meuk'],
                config                => { empty => 'hash'},
                published_file_ids    => [1, 2, 3],
                published_related_ids => [4, 5, 6],
                dry_run               => 1,
            };

            no warnings qw(redefine once);
            local *Zaaksysteem::Zaken::ResultSetZaak::_assert_publish_script = sub { return 1} ;

            my $opts;
            {    # Ede
                my $ede_pub = $zs->create_interface_ok(
                    module           => 'legacy_publicaties',
                    name             => 'vergaderingen_ede',
                    active           => 1,
                    interface_config => {
                        protocol         => 'scp',
                        source_directory => "foo",
                        hostname         => 'foo.bar.nl',
                        port             => 22,
                        internal_name    => 'vergaderingen_ede_mintlab',
                        notify_url       => 'foo.bar.nl',
                        destination_directory  => '/foo',
                        username         => 'bar',
                        password         => 'baz',
                        notify_wait      => 2,
                    },
                );

                no warnings qw(redefine once);
                local *Zaaksysteem::Zaken::ResultSetZaak::export_vergaderingen_ede = sub {
                    my $self = shift;
                    $opts = shift;
                };
                $rs->publish({ %$args, profile => 'vergaderingen_ede' });

                is_deeply(
                    $opts,
                    {
                        files_dir          => catdir($args->{files_dir}, abs_path("foo")),
                        publish_dir        => catdir($args->{files_dir}, abs_path("foo"), 'vergaderingen_ede'),
                        root_dir           => abs_path($rd),
                        published_file_ids => [1, 2, 3],
                        published_related_ids => [4, 5, 6],
                    },
                    "Correct arguments passed through for Ede"
                );
            }
            {    # Tholen
                my $tholen_pub = $zs->create_interface_ok(
                    module           => 'legacy_publicaties',
                    name             => 'perfectview_tholen',
                    active           => 1,
                    interface_config => {
                        protocol         => 'ftp',
                        source_directory => 'meuk',
                        hostname         => 'foo.bar.nl',
                        port             => 22,
                        internal_name    => 'perfectview_tholen',
                        notify_url       => 'foo.bar.nl',
                        destination_directory  => '/foo',
                        username         => 'bar',
                        password         => 'baz',
                        notify_wait      => 2,
                    },
                );
                no warnings qw(redefine once);
                local *Zaaksysteem::Zaken::ResultSetZaak::export_perfectview_tholen = sub {
                    my $self = shift;
                    $opts = shift;
                };
                $rs->publish({ %$args, profile => 'perfectview_tholen' });
                is_deeply(
                    $opts,
                    {
                        files_dir          => catdir($args->{files_dir}, abs_path("meuk")),
                        config             => $tholen_pub->get_interface_config,
                        tmp_publish_dir    => catfile($args->{files_dir}, abs_path("meuk"), 'publish'),
                        display_fields     => ['meuk'],
                    },
                    "Correct arguments passed through for Tholen"
                );
            }

        },
        "Zaken::ResultSetZaak::publish"
    );

}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

