package TestFor::General::Zaken::ComponentZaak;
use base qw(Test::Class);

use TestSetup;

use Zaaksysteem::Object::Model;
use Zaaksysteem::Search::ZQL;

sub setup : Test(startup) {
    my $self = shift;

    $self->{model} = Zaaksysteem::Object::Model->new(
        schema => $zs->schema
    );
}

sub zs_zaken_componentzaak_deletion_errors : Tests {
    my $self = shift;

    $zs->zs_transaction_ok(sub {
        {
            my $case = $zs->create_case_ok();
            $case->update(
                {
                    status => 'resolved',
                    afhandeldatum => DateTime->now(),
                    vernietigingsdatum => DateTime->new(
                        year      => 1955,
                        month     => 11,
                        day       => 12,
                        hour      => 22,
                        minute    => 4,
                        time_zone => 'US/Pacific',
                    ),
                }
            );

            is_deeply(
                $case->deletion_errors(),
                [],
                'No deletion errors for deletable case',
            );
            ok($case->can_delete(), 'API thinks case is deleteable too');
        }
        {
            my $case1 = $zs->create_case_ok();
            my $case2 = $zs->create_case_ok();
            my $case3 = $zs->create_case_ok();

            $case1->update(
                {
                    milestone => 2,
                }
            );
            $case2->update(
                {
                    status => 'resolved',
                    afhandeldatum => DateTime->now(),
                    vernietigingsdatum => DateTime->new(
                        year      => 1955,
                        month     => 11,
                        day       => 12,
                        hour      => 22,
                        minute    => 4,
                        time_zone => 'US/Pacific',
                    ),
                    milestone => 2,
                    pid => $case1->id,
                }
            );
            $case3->update(
                {
                    status => 'resolved',
                    afhandeldatum => DateTime->now(),
                    vernietigingsdatum => DateTime->new(
                        year      => 1955,
                        month     => 11,
                        day       => 12,
                        hour      => 22,
                        minute    => 4,
                        time_zone => 'US/Pacific',
                    ),
                    milestone => 2,
                    pid => $case2->id,
                }
            );

            is_deeply(
                $case1->deletion_errors(),
                [
                    'Zaak is nog niet afgehandeld',
                    'Bewaartermijn is niet verstreken'
                ],
                'Deletion errors for a grandchild with undeleteable grandparent and parent',
            );
            is_deeply(
                $case2->deletion_errors(),
                [
                    'Hoofdzaak ' . $case1->id . ' is nog niet afgehandeld'
                ],
                'Deletion errors for a child with undeleteable parent but deleteable child',
            );
            is_deeply(
                $case3->deletion_errors(),
                [
                    "Bewaartermijn van hoofdzaak " . $case2->id . " niet verstreken",
                    "Hoofdzaak " . $case1->id . " is nog niet afgehandeld",
                ],
                'Deletion errors for a grandchild with undeleteable grandparent and parent',
            );
        }

        {
            my $case1 = $zs->create_case_ok();
            my $case2 = $zs->create_case_ok();
            my $case3 = $zs->create_case_ok();

            $case1->update(
                {
                    status => 'resolved',
                    afhandeldatum => DateTime->now(),
                    vernietigingsdatum => DateTime->new(
                        year      => 1955,
                        month     => 11,
                        day       => 12,
                        hour      => 22,
                        minute    => 4,
                        time_zone => 'US/Pacific',
                    ),
                }
            );
            $case2->update({pid => $case1->id, milestone => 2});
            $case3->update(
                {
                    pid => $case1->id,
                    afhandeldatum => DateTime->now(),
                    vernietigingsdatum => DateTime->now()->add(days => 7),
                    milestone => 3,
                }
            );

            is_deeply(
                [ sort @{$case1->deletion_errors()} ],
                [
                    'Bewaartermijn van deelzaak ' . $case3->id . ' niet verstreken',
                    'Deelzaak ' . $case2->id . ' is nog niet afgehandeld',
                ],
                'Deletion errors for case with undeleteable children',
            );
        }
    }, 'Case deletion errors');
}

sub zs_zaken_componentzaak_deletion_warnings : Tests {
    my $self = shift;

    $zs->zs_transaction_ok(sub {
        my $case1 = $zs->create_case_ok();
        my $case2 = $zs->create_case_ok();
        my $case3 = $zs->create_case_ok();

        $case1->update(
            {
                status => 'resolved',
                afhandeldatum => DateTime->now(),
                vernietigingsdatum => DateTime->new(
                    year      => 1955,
                    month     => 11,
                    day       => 12,
                    hour      => 22,
                    minute    => 4,
                    time_zone => 'US/Pacific',
                ),
            }
        );
        $case2->update({pid => $case1->id, milestone => 2});
        $case3->update(
            {
                pid => $case1->id,
                afhandeldatum => DateTime->now(),
                vernietigingsdatum => DateTime->now()->add(days => 7),
                milestone => 3,
            }
        );

        $schema->resultset('CaseRelation')->create(
            {
                case_id_a   => $case1->id,
                case_id_b   => $case2->id,
                order_seq_a => 1,
                order_seq_b => 1,
                type_a      => 'umpteenth cousin, thrice removed',
                type_b      => 'umpteenth cousin, thrice removed',
            }
        );
        $schema->resultset('CaseRelation')->create(
            {
                case_id_a   => $case1->id,
                case_id_b   => $case3->id,
                order_seq_a => 2,
                order_seq_b => 2,
                type_a      => 'umpteenth cousin, thrice removed',
                type_b      => 'umpteenth cousin, thrice removed',
            }
        );

        TODO: {
            local $TODO = "Object deletion changes may have triggered extra warnings";

            my $ok = is_deeply(
                [sort @{ $case1->deletion_warnings }],
                [
                    'Bewaartermijn voor gerelateerde zaak '
                        . $case3->id
                        . ' is nog niet verstreken',
                    'Gerelateerde zaak '
                        . $case2->id
                        . ' is nog niet afgehandeld',
                ],
                'Deletion warnings'
            );
            if (!$ok) {
                diag explain $case1->deletion_warnings();
            }
        }

        {
            require Zaaksysteem::Object::Types::TestObject;

            my $case1 = $zs->create_case_ok();
            my $object = Zaaksysteem::Object::Types::TestObject->new();

            my $case_obj = $case1->object_data;
            $object->add_relation(
                Zaaksysteem::Object::Relation->new(
                    related_object_id => $case_obj->uuid,
                    related_object_type => 'case',
                    relationship_name_a => 'thief',
                    relationship_name_b => 'victim',
                    blocks_deletion => 1,
                )
            );
            my $saved_object = $self->{model}->save(object => $object);

            $case1->update(
                {
                    status => 'resolved',
                    afhandeldatum => DateTime->now(),
                    vernietigingsdatum => DateTime->new(
                        year      => 1955,
                        month     => 11,
                        day       => 12,
                        hour      => 22,
                        minute    => 4,
                        time_zone => 'US/Pacific',
                    ),
                }
            );

            is_deeply(
                [ sort @{$case1->deletion_warnings()} ],
                [
                    "Object '" .$saved_object->type . "(...". substr($saved_object->id, -6) . ")' is nog aan deze zaak gekoppeld"
                ],
                'Deletion warnings for case with delete-blocking object relation',
            );
        }

        {
            require Zaaksysteem::Object::Types::ScheduledJob;

            my $case1 = $zs->create_case_ok();
            my $object = Zaaksysteem::Object::Types::ScheduledJob->new(
                job             => 'CreateCase',
                next_run        => DateTime->now(),
                interval_period => 'once',
                runs_left       => 1,
            );

            my $case_obj = $case1->object_data;
            $object->add_relation(
                Zaaksysteem::Object::Relation->new(
                    related_object_id => $case_obj->uuid,
                    related_object_type => 'case',
                    relationship_name_a => 'thief',
                    relationship_name_b => 'victim',
                    blocks_deletion => 1,
                )
            );
            my $saved_object = $self->{model}->save(object => $object);

            $case1->update(
                {
                    status => 'resolved',
                    afhandeldatum => DateTime->now(),
                    vernietigingsdatum => DateTime->new(
                        year      => 1955,
                        month     => 11,
                        day       => 12,
                        hour      => 22,
                        minute    => 4,
                        time_zone => 'US/Pacific',
                    ),
                }
            );

            is_deeply(
                [ sort @{$case1->deletion_warnings()} ],
                [
                    "Taak 'scheduled_job(..." . substr($saved_object->id, -6) . ")' is nog niet voltooid"
                ],
                'Deletion warnings for case with delete-blocking object relation to ScheduledJob',
            );
        }
    }, 'Case deletion warnings');
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

