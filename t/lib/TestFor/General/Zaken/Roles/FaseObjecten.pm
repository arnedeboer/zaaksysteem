package TestFor::General::Zaken::Roles::FaseObjecten;
use base qw(Test::Class);

use TestSetup;

sub zs_zaken_roles_faseobjecten_set_vernietigingsdatum : Tests {
    $zs->zs_transaction_ok(
        sub {
            my $case = $zs->create_case_ok();

            $case->afhandeldatum(undef);
            my $r = $case->set_vernietigingsdatum;
            is($r, undef, "No afhandeldatum set");

            # Testsuite only creates one resultaat
            my $resultaat
                = $case->zaaktype_node_id->zaaktype_resultaten->search_rs()
                ->first;

            $case->resultaat($resultaat->resultaat);
            my $now = DateTime->now();
            $case->afhandeldatum($now);
            $r = $case->set_vernietigingsdatum;

            isa_ok($r, "DateTime", "Return value is of correct type");
            is(
                $r,
                $now->clone->add(days => $resultaat->bewaartermijn),
                "Destruction date is correct"
            );

            $case->resultaat(undef);
            $r = $case->set_vernietigingsdatum;
            is(
                $r,
                $now->add(years => 1),
                "No result results in a destruction one year from now"
            );

            throws_ok(
                sub {
                    $case->resultaat("This will break stuff");
                    $case->set_vernietigingsdatum;
                },
                qr/Unable to find resultaat/,
                "DB discrepancy found"
            );
        },
        'set_vernietigingdatum',
    );
}

sub zs_zaken_roles_faseobjecten_is_in_phase : Tests {
    $zs->zs_transaction_ok(
        sub {
            my $case = $zs->create_case_ok();
            while ($case->set_volgende_fase) {
            }
            ok($case->is_in_phase('afhandel_fase'), "Zit in afhandelfase");
        },
        'is_in_phase'
    );
}

1;

__END__

=head1 NAME

TestFor::General::Zaken::Roles::FaseObjecten - A zaak fase objecten role tester

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
