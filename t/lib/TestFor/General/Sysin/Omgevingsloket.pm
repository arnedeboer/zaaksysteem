package TestFor::General::Sysin::Omgevingsloket;
use base qw(Test::Class);

use TestSetup;

use File::Spec::Functions qw/catfile/;
use File::Slurp qw(read_file);

use Zaaksysteem::Backend::Sysin::Modules::Omgevingsloket;

sub setup : Test(startup) {
    my $self = shift;

    $zs->stuf_0312;
    $self->{module} = Zaaksysteem::Backend::Sysin::Modules::Omgevingsloket->new;
}

=head2 get_test_xml_ok

Get the correct XML from the file.

=cut

sub get_test_xml_ok {
    return $zs->open_stuf_xml('0312', shift);
}

=head2 add_zaaktype_kenmerk

Create a new attr and add it to a given casetype phase

=cut

sub add_zaaktype_kenmerk {
    my ($zaaktype_status, $name, $value_type) = @_;

    my $kenmerk2 = $zs->create_bibliotheek_kenmerk_ok(
        naam => $name,
        magic_string => 'magic_string_' . $name,
        value_type => $value_type,
    );

    return $zs->create_zaaktype_kenmerk_ok(
        status              => $zaaktype_status,
        bibliotheek_kenmerk => $kenmerk2,
    );
}

sub add_attribute_to_config {
    my ($interface, $name, $value_type) = @_;

    my $zaaktype_status = $interface->case_type_id->zaaktype_node_id
        ->zaaktype_statuses->search({status => 1})->first;

    my $zt_kenmerk = add_zaaktype_kenmerk($zaaktype_status, $name, $value_type);

    my $config = $interface->get_interface_config;

    push @{ $config->{attribute_mapping} }, {
        external_name => $name,
        internal_name => {
            searchable_object_id => 'magic_string_' . $name
        }
    };

    $interface->update_interface_config($config);

    return $zt_kenmerk;
}

sub omgevingsloket_parse_xml_natuurlijk_persoon : Tests {
    my $self = shift;

    $zs->zs_transaction_ok(sub {

        my $xml = get_test_xml_ok('aanvraag_natuurlijk_persoon.xml');

        my $aanvraag = $self->{module}->parse_xml($xml);

        my $desired = {
            'burgerservicenummer' => '581238850',
            'type'                => 'natuurlijk_persoon'
        };

        is_deeply $aanvraag->{isAangevraagdDoor}, $desired, 'Natuurlijk persoon parsed';
    });
}

sub omgevingsloket_parse_xml : Tests {
    my $self = shift;

    $zs->zs_transaction_ok(sub {

        my $xml = get_test_xml_ok('aanvraag_bedrijf.xml');
        my $aanvraag = $self->{module}->parse_xml($xml);
        # negative scenario is handled below
        my $sub = $self->{module}->_process_selector($aanvraag);
        isa_ok($sub, "CODE", "Process selector returns a sub");

        my $desired = {
          'aanvraagNaam' => 'test3465758',
          'aanvraagProcedure' => 'Reguliere procedure',
          'aanvraagStatus' => 'In behandeling',
          'aanvraagdatum' => '08-04-2014',
          'aanvraagnummer' => '33991',
          'attachments' => [
            '/33991/basisset_aanvraag/33991_1396945003501_Bouwtekening.pdf',
            '/33991/basisset_aanvraag/33991_1397227477521_papierenformulier.pdf',
            '/33991/basisset_aanvraag/33991_1397227477526_publiceerbareaanvraag.pdf'
          ],
          'gedeeltelijkGoedkeurenGewenst' => 'nee',
          'gefaseerdIndienen' => 'niet',
          'isAangevraagdDoor' => {
            'type' => 'bedrijf',
            'vestigingsnummer' => '999888555418'
          },
          'landelijkeFormulierVersie' => '2013.01',
          'omschrijvingNietInTeDienenBijlagen' => 'nmnmn',
          'omschrijvingUitgesteldeBijlagen' => 'jknmn',
          'persoonsgegevensVrijgeven' => 'nee',
          'plichtStatus' => 'Vergunningsplicht',
          'projectKosten' => 4567,
          'projectOmschrijving' => 'jkjkjkjkjkjk',
          'referentieCodeAanvrager' => 'mjkj',
          'stuurgegevens' => {
            'berichtcode' => 'Di01',
            'functie' => 'AanbiedenAanvraag',
            'ontvanger' => {
              'administratie' => 'omvvergunning',
              'applicatie' => 'Gemeente zztestgemeente18',
              'organisatie' => '012345678'
            },
            'referentienummer' => '122485',
            'tijdstipBericht' => '20140411164437',
            'zender' => {
              'administratie' => 'omvvergunning',
              'applicatie' => 'OLO',
              'organisatie' => 'VROM'
            }
          },
          'toelichtingBijIndiening' => '-',
          'uitgesteldeAanleveringBijlagen' => 'ja'
        };

        is_deeply $aanvraag, $desired, "Parse aanvraag satisfactory";

        $aanvraag->{stuurgegevens}{functie} = "Not what module thought it was gonna get";
        is($self->{module}->_process_selector($aanvraag), undef, "process_selector cannot handle this");

    }, 'Omgevingsloket parse_xml: aanvraag');

    $zs->zs_transaction_ok(sub {

        my $xml = get_test_xml_ok('aanvulling_bedrijf.xml');
        my $aanvulling = $self->{module}->parse_xml($xml);
        my $sub = $self->{module}->_process_selector($aanvulling);
        isa_ok($sub, "CODE", "Process selector returns a sub");

        my $desired = {
            aanvraagnummer => 38433,
            attachments    => [
                '/38433/basisset_aanvraag/38433_1418652325424_document_koppelingen_config.png'
            ],
            stuurgegevens => {
                berichtcode => 'Di01',
                functie     => 'IndienenAanvulling',
                ontvanger   => {
                    administratie => 'omvvergunning',
                    applicatie    => 'Gemeente Voerendaal',
                    organisatie   => 'Gemeente Voerendaal'
                },
                referentienummer => '156819',
                tijdstipBericht  => '20141215150701',
                zender           => {
                    administratie => 'omvvergunning',
                    applicatie    => 'OLO',
                    organisatie   => 'VROM',
                }
            }
        };
        is_deeply $aanvulling, $desired, "Parse aanvraag satisfactory";

    }, 'Omgevingsloket parse_xml: aanvulling');
}

sub omgevingsloket_parse_aanvraagdatum : Tests {
    my $self = shift;

    $zs->zs_transaction_ok(sub {

        throws_ok(sub {
            $self->{module}->parse_aanvraagdatum('dfdfdf');
        }, qr/Incorrect datumformaat/, "Fails when given incorrect date");

        is $self->{module}->parse_aanvraagdatum, undef, "Returns undef when given falsy input";

        is $self->{module}->parse_aanvraagdatum("20140312"), '12-03-2014', "Converts date to MM-DD-YYYY";

    }, 'Omgevingsloket aanvraagdatum');
}

sub omgevingsloket_create_case : Tests {
    $zs->zs_transaction_ok(sub {

        my $interface = $zs->create_interface_omgevingsloket_ok;

        my $values = {
            aanvraagNaam => 'value1',
            projectOmschrijving => 'value2',
            aanvraagdatum => '31-12-2014'
        };

        my $medewerker = $zs->create_medewerker_ok;
        my $case = $interface->create_case({
            values => $values,
            requestor => 'betrokkene-medewerker-' . $medewerker->get_column('subject_id')
        });

        isa_ok $case, 'Zaaksysteem::Model::DB::Zaak', 'Case created';

        my @case_values = sort values %{ $case->field_values };
        my @expected_values = sort values %$values;

        is_deeply [@case_values], [@expected_values],
            'Values stored as advertised';

        # several processes are only triggered on touch.
        ok $case->_touch, "Touch still works";

    }, 'Omgevingsloket create case');
}

sub omgevingsloket_process_aanvraag : Tests {
    $zs->zs_transaction_ok(sub {
        my $interface = $zs->create_interface_omgevingsloket_ok;
        $zs->set_fallback_requestors($interface);

        my $xml = get_test_xml_ok('aanvraag_bedrijf.xml');

        my $rs = $schema->resultset('Zaak')->search(undef, {order_by => { -desc => 'me.id'} });
        my $start_count = $rs->count;

        my $transaction = $interface->process({ input_data => $xml });

        my $after_count = $rs->count;
        is $after_count, $start_count + 1, 'New case created';

        isa_ok $transaction, 'Zaaksysteem::Model::DB::Transaction', "Got a transaction";
        ok $transaction->success_count, "Success count truthy";
        ok $transaction->processed, "Processed truthy";

        # check case in context
        my $case = $rs->first;

        my $expected_field_values = [
          '-',
          '08-04-2014',
          '33991',
          '4567',
          'In behandeling',
          'Reguliere procedure',
          'Vergunningsplicht',
          'ja',
          'jkjkjkjkjkjk',
          'jknmn',
          'mjkj',
          'nee',
          'nee',
          'niet',
          'nmnmn',
          'test3465758'
        ];

        my $field_values = [sort values %{ $case->field_values }];
        is_deeply $field_values, $expected_field_values, "Field values set as advertised";

        # check files
        my $case_files = [sort map {$_->name} $case->active_files];

        my $expected_files = [
          '33991_1396945003501_Bouwtekening',
          '33991_1397227477521_papierenformulier',
          '33991_1397227477526_publiceerbareaanvraag'
        ];

        is_deeply $case_files, $expected_files, "Files added as advertised";

    }, 'Omgevingsloket process aanvraag');
}

sub omgevingsloket_indienen_aanvulling : Tests {
    $zs->txn_ok(
        sub {
            my $interface = $zs->create_interface_omgevingsloket_ok;
            $zs->set_fallback_requestors($interface);

            my $case = $zs->create_case_ok();

            my $xml = get_test_xml_ok('aanvulling_bedrijf.xml');

            my $rs = $schema->resultset('Zaak')->search(undef, {order_by => { -desc => 'me.id'} });
            my $start_count = $rs->count;

            my $transaction = $interface->process({ input_data => $xml });

            my $after_count = $rs->count;
            is $after_count, $start_count, 'No new case created';

            isa_ok $transaction, 'Zaaksysteem::Model::DB::Transaction', "Got a transaction";

            ok !$transaction->success_count, "Success count false";
            ok $transaction->processed, "Processed truthy";

            my $case_files = [sort map {$_->name} $case->active_files];

            my $expected_files = [];
            is_deeply $case_files, $expected_files, "No files added, the case isn't a Omgevingsloket case";

            {
                no warnings qw(once redefine);
                local *Zaaksysteem::Object::Model::search_rs = sub { bless({}, "Foo::Rs") };
                local *Foo::Rs::count = sub { return 1 };
                local *Foo::Rs::first = sub { return "Foo::Rs" };
                local *Foo::Rs::get_source_object = sub { return $case };

                my $transaction = $interface->process({ input_data => $xml });

                ok $transaction->success_count, "Success count false";
                ok $transaction->processed, "Processed truthy";

                my $case_files = [sort map {$_->name} $case->active_files];
                my $expected_files = ['38433_1418652325424_document_koppelingen_config'];
                is_deeply $case_files, $expected_files, "Files added as advertised";
            }

        },
        'Omgevingsloket process aanvulling'
    );
}

sub omgevingsloket_store_remote_files : Tests {
    my $self = shift;

    $zs->zs_transaction_ok(sub {

        my $case = $zs->create_case_ok;
        my @remote_files = qw/file1.txt file2.pdf file3.doc/;

        my $interface = $zs->create_interface_omgevingsloket_ok;
        $self->{module}->interface($interface);

        # clear pesky moose cache
        $self->{module}->clear_attachment_case_document_ids;

        my @files = $self->{module}->store_remote_files($case, @remote_files);

        isa_ok $_, 'Zaaksysteem::Model::DB::File', 'Got file object'
            for @files;

        # scenario with case_document_id configured
        my $case_document_id = add_attribute_to_config($interface, 'bijlagen', 'file')->id;

        # clear moose lazy cache
        $self->{module}->clear_attachment_case_document_ids;

        my ($file) = $self->{module}->store_remote_files($case, 'casedoc.pdf');

        isa_ok $file, 'Zaaksysteem::Model::DB::File', 'Got file object';

        my ($first) = $file->case_documents;
        is $first->case_document_id->id, $case_document_id, 'case_document set';

    }, 'Omgevingsloket store_remote_files');
}

sub omgevingsloket_store_remote_files_error : Tests {
    my $self = shift;

    $zs->zs_transaction_ok(sub {

        my $case = $zs->create_case_ok;
        my @remote_files = ('', undef, 'not_existing.txt');

        my $interface = $zs->create_interface_omgevingsloket_ok();
        $self->{module}->interface($interface);

        # clear pesky moose cache
        $self->{module}->clear_attachment_case_document_ids;

        my @files = $self->{module}->store_remote_files($case, @remote_files);

        is scalar @files, 1, "One file returned, rest are falsy";
        my ($file) = @files;

        is $file->name, "FOUT - not_existing.txt", "Filename set as advertised";

        ok(-f $file->filestore->get_path, "File exists");
        my $content = read_file($file->filestore->get_path);

        ok $content =~ m/not_existing.txt/ &&
            $content =~ m/niet ophalen/,
            "Content contains clues as to what happened";

    }, 'Omgevingsloket store_remote_files error scenario');
}

sub omgevingsloket_retrieve_ftp : Tests {
    my $self = shift;

    $zs->zs_transaction_ok(sub {

        my $local_file = '/tmp/' . time;
        my $interface = $zs->create_interface_omgevingsloket_ok;
        my $config = $interface->get_interface_config;

        # real test data for accept. enable this and move t/inc/Net/FTPSSL.pm away:
        # $ mv t/inc/Net/FTPSSL.pm t/inc/Net/FTPSSL.pm.disabled
        # $config->{host} = 'ftps-inr.omgevingsloket.nl';
        # $config->{username} = 'ftp_g1017';
        # $config->{password} = 'ChCLQUTxz8UD';
        # $interface->update_interface_config($config);

        my $remote_file = '/33991/basisset_aanvraag/33991_1397227477526_publiceerbareaanvraag.pdf';

        $self->{module}->interface($interface);

        lives_ok(sub {
            $self->{module}->retrieve_ftp({
                local_file => $local_file,
                remote_file => $remote_file
            });
        }, "FTP retriever plays nice with FTP module");

        ok -e $local_file, "FTP retrieved file to local";

        unlink $local_file;

    }, 'Omgevingsloket retrieve_ftp');
}

sub omgevingsloket_attachment_case_document_id : Tests {
    my $self = shift;

    $zs->zs_transaction_ok(sub {

        my $interface = $zs->create_interface_omgevingsloket_ok;
        $self->{module}->interface($interface);

        is_deeply $self->{module}->attachment_case_document_ids, undef,
            "Returns empty list if no attribute has been configured";

        my $case_document_id = add_attribute_to_config($interface, 'bijlagen', 'file')->id;

        $self->{module}->clear_attachment_case_document_ids;

        is_deeply $self->{module}->attachment_case_document_ids, [$case_document_id],
            "Returns expected case_document_id if configured properly";

    }, 'Omgevingsloket get_attachment_case_document_id');
}

sub omgevingsloket_get_fallback_requestor : Tests {
    my $self = shift;

    $zs->zs_transaction_ok(sub {
        my $interface = $zs->create_interface_omgevingsloket_ok;

        $self->{module}->interface($interface);

        throws_ok(sub {
            $self->{module}->get_fallback_requestor('frmblffd');
        }, qr/wrong_requestor_type/, 'Fails when given wrong requestor type');

        throws_ok(sub {
            $self->{module}->get_fallback_requestor('bedrijf');
        }, qr/fallback_bedrijf_missing/, 'Fails when no fallback configured');

        $zs->set_fallback_requestors($interface);

        my $bedrijf = $self->{module}->get_fallback_requestor('bedrijf');

        ok $bedrijf =~ m/^betrokkene-bedrijf-\d+$/, 'Fallback bedrijf returned';
    });
}

sub omgevingsloket_determine_bedrijf : Tests {
    my $self = shift;

    $zs->zs_transaction_ok(sub {

        my $interface = $zs->create_interface_omgevingsloket_ok;

        $self->{module}->interface($interface);

        throws_ok(sub {
            $self->{module}->determine_bedrijf;
        }, qr/fallback_bedrijf_missing/, 'Fails when no fallback configured');

        $zs->set_fallback_requestors($interface);

        my $bedrijf = $self->{module}->determine_bedrijf;
        ok $bedrijf =~ m/^betrokkene-bedrijf-\d+$/, 'Fallback bedrijf returned';

        my $vestigingsnummer = '123456123456';
        my $db_bedrijf = $zs->create_bedrijf_ok(
            vestigingsnummer => $vestigingsnummer
        );

        $bedrijf = $self->{module}->determine_bedrijf($vestigingsnummer);
        is $bedrijf, 'betrokkene-bedrijf-' . $db_bedrijf->id,
            'Found bedrijf in db by vestigingsnummer';
    });
}

sub omgevingsloket_determine_natuurlijk_persoon : Tests {
    my $self = shift;

    $zs->zs_transaction_ok(sub {

        my $interface = $zs->create_interface_omgevingsloket_ok;
        $zs->set_fallback_requestors($interface);

        $self->{module}->interface($interface);

        my $burgerservicenummer = '987656432';
        my $db_natuurlijk_persoon = $zs->create_natuurlijk_persoon_ok;

        $db_natuurlijk_persoon->burgerservicenummer($burgerservicenummer);
        $db_natuurlijk_persoon->update;

        my $natuurlijk_persoon = $self->{module}->determine_natuurlijk_persoon($burgerservicenummer);
        is $natuurlijk_persoon, 'betrokkene-natuurlijk_persoon-' . $db_natuurlijk_persoon->id,
            'Found natuurlijk_persoon in db by burgerservicenummer';
    });
}

sub omgevingsloket_determine_requestor : Tests {
    my $self = shift;

    $zs->zs_transaction_ok(sub {

        my $interface = $zs->create_interface_omgevingsloket_ok;

        $self->{module}->interface($interface);

        throws_ok(sub {
            $self->{module}->determine_requestor({});
        }, qr/Betrokkene type is niet/, 'Fails when requestor has not been set');

        $zs->set_fallback_requestors($interface);

        my $requestor = $self->{module}->determine_requestor({
            type => 'bedrijf',
            vestigingsnummer => '123'
        });

        my $config = $interface->get_interface_config;
        my $fallback_bedrijf = $config->{fallback_bedrijf};
        is $requestor, 'betrokkene-bedrijf-' . $fallback_bedrijf->{id}, 'Requestor correctly set';

    });
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2014-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
