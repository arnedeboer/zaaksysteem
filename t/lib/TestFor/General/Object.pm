package TestFor::General::Object;
use base qw(Test::Class);

use TestSetup;

use JSON;
use Zaaksysteem::Object;

sub test_object_base : Tests {
    my ($obj, $now);

    with_stopped_clock {
        $now = DateTime->now();
        $obj = Zaaksysteem::Object->new(
            properties     => {},
            authorizations => [],
        );
    };

    isa_ok($obj, 'Zaaksysteem::Object', 'Object instantiates');

    is($obj->type, 'object', 'Type of base object is "object"');
    is($obj->id,   undef,    'id of base object is not defined');
    is($obj->date_modified, $now, 'Default modification date is "now"');
    is($obj->date_created, $now,  'Default creation date is "now"');
    ok($obj->is_deleteable, "By default, objects can be deleted.");

    my $date_created = DateTime->new(year => 1971, month => 11, day => 30);
    my $date_modified = DateTime->new(year => 2525, month => 10, day => 3);
    my $obj_with_time = Zaaksysteem::Object->new(
        properties     => {},
        authorizations => [],
        date_created   => $date_created,
        date_modified  => $date_modified,
    );
    is($obj_with_time->date_created,  $date_created, 'date_created was set by constructor');
    is($obj_with_time->date_modified, $date_modified, 'date_modified was set by constructor');

    is_deeply $obj_with_time->TO_JSON, {
        date_created    => $date_created,
        date_modified   => $date_modified,
        id              => undef,
        type            => 'object',
        actions         => [],
        values          => {},
        related_objects => [],
        relatable_types => [],
        security_rules  => [],
        label           => "$obj_with_time",
    }, 'Object->TO_JSON';
}

sub test_tostring : Tests {
    is(
        Zaaksysteem::Object->new(id => 1)->TO_STRING,
        'object(1)',
        'Zaaksysteem::Object::TO_STRING stringifies a =< 6 char id correctly'
    );

    is(
        Zaaksysteem::Object->new()->TO_STRING,
        'object(unsynched)',
        'Zaaksysteem::Object::TO_STRING stringifies a missing id correctly'
    );

    is(
        Zaaksysteem::Object->new(id => 123456)->TO_STRING,
        'object(123456)',
        'Zaaksysteem::Object::TO_STRING stringifies a == 6 char id correctly'
    );

    is(
        Zaaksysteem::Object->new(id => 'b7f3184d-3413-4688-aab5-4d82d117d2d1')->TO_STRING,
        'object(...17d2d1)',
        'Zaaksysteem::Object::TO_STRING stringifies a > 6 char id correctly'
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

