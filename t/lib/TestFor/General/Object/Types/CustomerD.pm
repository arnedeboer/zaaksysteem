package TestFor::General::Object::Types::CustomerD;
use base qw(Test::Class);

use TestSetup;
use Zaaksysteem::Tools;
use Zaaksysteem::Object::Types::CustomerD;

sub zs_object_types_customer_d : Tests {
    my $model = $zs->object_model;

    $zs->zs_transaction_ok(
        sub {
            my %opts = (
                fqdn  => 'testsuite.zaaksysteem.nl',
                label => 'Zaaksysteem testsuite omgeving',
                owner => 'To be defined'
            );

            my $obj = Zaaksysteem::Object::Types::CustomerD->new(%opts);
            isa_ok($obj, "Zaaksysteem::Object::Types::CustomerD");
            is($obj->type, 'customer_d', "Type is correct");

            my $saved = $model->save_object(object => $obj);

            is_deeply(
                $obj->TO_JSON->{values},
                {
                    release        => 'accept',
                    loadbalancer   => 'accept',
                    cloud          => 'commercial',
                    disabled       => 'true',
                    deleted        => 'false',
                    %opts,
                },
                'TO_JSON->values with minimal object'
            );
            is_deeply($saved->TO_JSON->{values}, $obj->TO_JSON->{values});

            is $obj->id,     undef, "Unsaved object does not have an id";
            isnt $saved->id, undef, "Saved object has an id";

            my $now = DateTime->now();

            %opts = (
                fqdn                  => 'testsuite.zaaksysteem.nl',
                label                 => 'Zaaksysteem testsuite omgeving',
                owner                 => 'To be defined',
                release               => 'master',
                loadbalancer          => 'production',
                cloud                 => 'gov',
                ipv6                  => '2001:0db8:0000:0000:0000:0000:1428:57ab',
                ipv4                  => '194.134.5.5',
                certificate           => 'some file',
                ldap                  => 'ldap',
                key                   => 'some key',
                aliases               => [ 'alias.zaaksysteem.nl' ],
                fallback              => 'http://localhost',
                disabled              => 0,
                deleted               => 1,
                provisioned_on        => $now,
                delete_on             => $now,
                ldap_provisioned      => $now->datetime(),
                filestore_provisioned => $now->datetime(),
                database_provisioned  => $now,
                origin                => "production",
                network_acl           => ['194.134.5.5 # Ye olde employee'],
            );

            $obj = Zaaksysteem::Object::Types::CustomerD->new(%opts);

            $opts{deleted} = 'true';
            $opts{disabled} = 'false';

            is_deeply($obj->TO_JSON->{values},
                \%opts, 'TO_JSON->values with all attributes');

            throws_ok(
                sub {
                    $model->save_object(object => $obj);
                },
                qr#object/unique/constraint: Object unique constraint violation: fqdn#,
                "Is unique"
            );

            lives_ok(
                sub { $model->save_object(object => $saved) },
                "Saved object can be saved if it exists"
            );

            my $object = $model->save(
                json => $zs->open_testsuite_json('customerd.json'));

            my $cc = $zs->create_customer_config_ok;

            my $relation = $object->add_to_customer($cc);
            ok($relation, "Relationship has been created");

            lives_ok(
                sub { $object->add_to_customer($cc) },
                "Creating an existing relationship is allowed"
            );

            throws_ok(
                sub {
                    $object->add_to_customer(
                        $model->save_object(
                            object => $zs->create_customer_config_ok()
                        )
                    );
                },
                qr/Existing relationship, unable to set new relationship/,
                "Existing relationship, polygamy is not allowed here",
            );

        },
        'CustomerD object'
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
