package TestFor::General::Object::Types::Type;
use base qw(Test::Class);

use TestSetup;

use Zaaksysteem::Object::Types::Type;

sub object_type_simple : Tests {
    my ($obj_type, $now);

    $zs->txn_ok(sub {
        my $category = $zs->create_bibliotheek_categorie_ok;

        with_stopped_clock {
            $now      = DateTime->now();
            $obj_type = Zaaksysteem::Object::Types::Type->new(
                name                    => 'UserDefinedType',
                prefix                  => 'udt',
                authorizations          => [],
                attributes              => [],
                instance_authorizations => [],
                related_casetypes       => [],
                category_id             => $category->id
            );
        };

        isa_ok(
            $obj_type,
            'Zaaksysteem::Object::Types::Type',
            'New object instantiates correctly'
        );
        is($obj_type->type, 'type', '->type returns the correct type');

        is_deeply(
            $obj_type->TO_JSON,
            {
                id                      => undef,
                type                    => 'type',
                date_created            => $now,
                date_modified           => $now,
                actions                 => [],
                relatable_types         => [],
                related_objects         => [],
                values                  => {
                    name                    => 'UserDefinedType',
                    prefix                  => 'udt',
                    attributes              => [],
                    related_casetypes       => [],
                    instance_authorizations => [],
                    category_id             => $category->id,
                    modified_sections       => [],
                    modification_rationale  => ''
                },
                security_rules  => [],
                relatable_types => [],
                related_objects => [],
            },
        );
    }, 'object/types/type');
}

sub object_type_meta_class_relatables : Tests {
    my $self = shift;

    my $type = Zaaksysteem::Object::Types::Type->new(
        name => 'Contract',
        prefix => 'contact',
        related_objecttypes => [{ object_type => 'plot' }]
    );

    my $meta = $type->instance_meta_class;

    my $contract = $meta->new_object;

    # Also check for 'case' relatable type, which should bubble up from
    # Zaaksysteem::Object->relatable_types
    is_deeply [ $contract->relatable_types ], [qw[case plot]],
        'Type mangles related_objecttypes into relatable_types method';
}

sub object_type_prefix : Tests {
    my $self = shift;

    my $type = Zaaksysteem::Object::Types::Type->new(
        name => 'Contract',
    );

    is($type->prefix, 'contract', "Prefix generator, basic");

    {
        $type->name('Something space rich');

        $self->_clear_prefix($type);
        is($type->prefix, 'something_space_rich', "Prefix generator, with spaces");
    }

    {
        $type->name('Something "quoted"');

        $self->_clear_prefix($type);
        is($type->prefix, 'something__quoted_', "Prefix generator, with double quotes");
    }
}

sub object_type_simple_stringification : Tests {
    my $self = shift;

    my $type = Zaaksysteem::Object::Types::Type->new(
        name => 'Contract',
    );

    my $meta = $type->instance_meta_class;
    my $contract = $meta->new_object;

    is $contract->TO_STRING, 'contract(unsynched)', 'Stub object type stringification falls back to Zaaksysteem::Object->TO_STRING';
}

sub object_type_stringification : Tests {
    my $self = shift;

    my $type = Zaaksysteem::Object::Types::Type->new(
        name => 'Contract',
        title_template => 'abc'
    );

    my $meta = $type->instance_meta_class;
    my $contract = $meta->new_object;

    is $contract->TO_STRING, 'abc', 'Title template passthru style works';
}

sub object_type_stringification_ztt : Tests {
    my $self = shift;

    $zs->txn_ok(sub {
        my $attr = $zs->create_bibliotheek_kenmerk_ok(magic_string => 'banaan');

        my $type = Zaaksysteem::Object::Types::Type->new(
            name => 'Contract',
            title_template => sprintf('[[ %s ]]', $attr->magic_string),
            attributes => [
                {
                    attribute_id => $attr->id,
                    attribute_label => $attr->magic_string,
                    attribute_object => $attr,
                    attribute_type => $attr->value_type,
                    data => { },
                    label => $attr->magic_string,
                    name => $attr->magic_string,
                }
            ]
        );

        my $meta = $type->instance_meta_class;
        my $contract = $meta->new_object($attr->magic_string => 'xyz');

        is $contract->TO_STRING, 'xyz', 'ZTT interpolated stringification of user-defined attribute';
    });
}

sub _clear_prefix {
    my $self = shift;
    my $instance = shift;

    Zaaksysteem::Object::Types::Type->meta
        ->find_attribute_by_name('prefix')
        ->clear_value($instance);
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

