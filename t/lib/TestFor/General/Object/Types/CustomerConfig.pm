package TestFor::General::Object::Types::CustomerConfig;
use base qw(Test::Class);

use TestSetup;
use Zaaksysteem::Tools;
use Zaaksysteem::Object::Types::CustomerConfig;

sub zs_object_types_customer_config : Tests {

    my $model = $zs->object_model;

    $zs->zs_transaction_ok(
        sub {
            my %opts = (
                customer_id     => 'mintlab',
                naam_lang       => 'Mintlab B.V.',
                naam_kort       => 'Mintlab',
                woonplaats      => 'Amsterdam',
                straatnaam      => 'Donker Curtiusstraat',
                huisnummer      => '7-521',
                postcode        => '1051 JL',
                website         => 'http://www.mintlab.nl',
                email           => 'info@mintlab.nl',
                telefoonnummer  => '020 7370005',
                faxnummer       => '020 7370005',
                latitude        => '',
                longitude       => '',
                gemeente_id_url => 'http://mijn.overheid.nl/gemeenteid',
                gemeente_portal => 'http://mintlab.nl',
                cloud           => 'government',
                ad_key          => '5Rm8cdJz5sJ1azpO',
                production_url  => 'http://mintlab.zaaksysteem.nl',
                production_ip   => '10.44.0.11',
                accept_url      => 'http://mintlab.accept.zaaksysteem.nl',
                accept_ip       => '10.44.0.12',
                domain          => 'zaaksysteem.nl',
                template        => 'mintlab',
            );

            my $obj = Zaaksysteem::Object::Types::CustomerConfig->new(%opts);
            isa_ok($obj, "Zaaksysteem::Object::Types::CustomerConfig");
            is($obj->type, 'customer_config', "Object->type matches");

            my $saved = $model->save_object(object => $obj);

            is_deeply(
                $obj->TO_JSON->{values},
                {
                    postbus          => '',
                    postbus_postcode => '',
                    %opts,
                },
                'TO_JSON->values with minimal object'
            );
            is_deeply($saved->TO_JSON->{values}, $obj->TO_JSON->{values});

            is $obj->id,     undef, "Unsaved object does not have an id";
            isnt $saved->id, undef, "Saved object has an id";

            throws_ok(
                sub {
                    $model->save_object(object => $obj);
                },
                qr#object/unique/constraint: Object unique constraint violation: \w+#,
                "Unique constraint violation",
            );

            lives_ok(
                sub { $model->save_object(object => $saved) },
                "Saved object can be saved if it exists"
            );

            my $object = $model->save(
                json => $zs->open_testsuite_json('customerconfig.json'));

            my $cd = $zs->create_customer_d_ok;
            my $relation = $saved->add_customer_d($cd);
            ok($relation, "Relationship has been created");

            lives_ok(
                sub { $saved->add_customer_d($cd) },
                "Creating an existing relationship is allowed"
            );

            throws_ok(
                sub {
                    my $customer_config = $zs->create_customer_config_ok();
                    my $cd = $zs->create_customer_d_ok();
                    $customer_config->add_customer_d($cd);

                    $model->save_object(object => $customer_config);

                    $cd = $model->retrieve(uuid => $cd->id);
                    $saved->add_customer_d($cd);
                },
                qr/Existing relationship, unable to set new relationship/,
                "Existing relationship, polygamy is not allowed here",
            );

        },
        'CustomerConfig Object'
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
