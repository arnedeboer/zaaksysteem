package TestFor::General::Object::Types::ScheduledJob;
use base qw(Test::Class);

use TestSetup;
use Zaaksysteem::Tools;
use Zaaksysteem::Object::Types::ScheduledJob;

sub setup : Test(startup) {
    shift->{model} = Zaaksysteem::Object::Model->new(schema => $zs->schema);
}

sub zs_object_types_scheduled_job_saving : Tests {
    my $model = shift->{model};

    $zs->zs_transaction_ok(sub {
        my $now = DateTime->now();

        my $sj = Zaaksysteem::Object::Types::ScheduledJob->new(
            job             => 'inside',
            next_run        => $now->clone,
            interval_period => 'weeks',
            interval_value  => 2,
            runs_left       => 2,
        );
        isa_ok($sj, 'Zaaksysteem::Object::Types::ScheduledJob', 'new() returns a ScheduledJob');

        my $saved = $model->save_object(object => $sj);

        ok($saved->id, "Saving a ScheduledJob works");
        is($saved->next_run,        $now,    "next_run is retrieved properly");
        is($saved->runs_left,       2,       "runs_left is retrieved properly");
        is($saved->interval_period, 'weeks', "interval_period is retrieved properly");
        is($saved->interval_value,  2,       "interval_value is retrieved properly");
    });
}

sub zs_object_types_scheduled_job_setup_next : Tests {
    my $now = DateTime->now();

    my $sj = Zaaksysteem::Object::Types::ScheduledJob->new(
        job             => 'italian',
        next_run        => $now->clone,
        interval_period => 'weeks',
        interval_value  => 2,
        runs_left       => 2,
    );
    isa_ok($sj, 'Zaaksysteem::Object::Types::ScheduledJob', 'new() returns a ScheduledJob');

    is($sj->next_run, $now, "Next run is 'now'");

    ok(!$sj->setup_next_run(), "setup_next_run returned false: more runs to do");
    is($sj->next_run, $now->clone->add('weeks' => 2), "Next run date was moved ahead by 2 weeks");
    is($sj->runs_left, 1, "One run left to do");

    ok($sj->setup_next_run(), "setup_next_run returned true: no more runs to do");
}

sub zs_object_types_scheduled_job_new_empty : Tests {
    my $model = shift->{model};

    with_stopped_clock {
        my $sj = Zaaksysteem::Object::Types::ScheduledJob->new_empty();
        isa_ok($sj, 'Zaaksysteem::Object::Types::ScheduledJob', 'new_empty returns a ScheduledJob');

        is(
            $sj->job,
            'DummyJob',
            'Empty ScheduledJob has dummy job type'
        );
        is(
            $sj->next_run,
            DateTime->now(),
            'Empty ScheduledJob\'s next run time is "now"'
        );
        is(
            $sj->interval_period,
            'once',
            'Empty ScheduledJob has an interval period of "once"'
        );
        is(
            $sj->interval_value,
            undef,
            'Empty ScheduledJob has no interval value'
        );
        is(
            $sj->runs_left,
            0,
            'Empty ScheduledJob has no runs left'
        );
    };
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
