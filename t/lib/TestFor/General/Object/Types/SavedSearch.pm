package TestFor::General::Object::Types::SavedSearch;
use base qw(Test::Class);

use TestSetup;
use Zaaksysteem::Object::Types::SavedSearch;

sub zs_object_types_savedsearch : Tests {
    my $model = $zs->object_model;

    $zs->zs_transaction_ok(
        sub {
            my %opts = (
                title => 'foo',
                query => 'bar',
            );

            my $obj = Zaaksysteem::Object::Types::SavedSearch->new(%opts);
            isa_ok($obj, "Zaaksysteem::Object::Types::SavedSearch");
            is($obj->type, 'saved_search', "Type is correct");

            foreach (qw(title query)) {
                throws_ok(
                    sub {
                        Zaaksysteem::Object::Types::SavedSearch->new(%opts,
                            $_ => '');
                    },
                    qr/Attribute \Q($_)\E does not pass the type constraint because.*NonEmptyStr.*/,
                    "Empty $_ is not legit"
                );
            }


        },
        'SavedSearch object'
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
