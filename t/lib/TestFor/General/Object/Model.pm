package TestFor::General::Object::Model;
use base qw(Test::Class);

use TestSetup;
use JSON;
use Zaaksysteem::Exception;

sub test_inflection : Tests {
    my $self = shift;
    my $model = $zs->object_model;

    is $model->inflect_package_by_type('case'),
        'Zaaksysteem::Object::Types::Case',
        'case => Case';

    is $model->inflect_package_by_type('case_meta'),
        'Zaaksysteem::Object::Types::CaseMeta',
        'case_meta => CaseMeta';

    is $model->inflect_package_by_type('case/type'),
        'Zaaksysteem::Object::Types::Case::Type',
        'case/type => Case::Type';
}

sub test_instantiation_from_json : Tests {
    my $self = shift;
    my $model = $zs->object_model;

    $zs->zs_transaction_ok(sub {
        my $object = $model->inflate_from_json('{ "object_class": "object" }');

        isa_ok $object, 'Zaaksysteem::Object', 'empty json object instantiates';

        my $body = {
            id => 55,
            object_class => 'object'
        };

        $object = $model->inflate_from_json(
            encode_json($body)
        );

        is $body->{ id }, $object->id, 'object with id instantiates to Object with id';
    }, 'Zaaksysteem::Object::Model::inflate_from_json');
}

sub test_inflection_integration : Tests {
    my $self = shift;
    my $model = $zs->object_model;

    $zs->zs_transaction_ok(sub {

        my $body = {
            id => 12345,
            object_class => 'case/meta_thing'
        };

        TODO : {
            local $TODO = "foo";
            is_deeply $model->inflate_from_json(encode_json($body)), {},
                'Inflator returns empty object when no type-class can be found';
            };

    }, 'Zaaksysteem::Object::type');
}

sub test_inflation_from_database : Tests {
    my $self = shift;
    my $model = $zs->object_model;

    $zs->zs_transaction_ok(sub {
        with_stopped_clock {
            my $object_data = $zs->create_object_data_ok;
            my $model = Zaaksysteem::Object::Model->new(
                schema => $zs->schema
            );

            my $object = $model->inflate_from_row($object_data);

            is $object_data->id, $object->id, 'IDs match in from_row inflation';
            is $object_data->date_created, $object->date_created, 'Creation dates match in from_row inflation';
            is $object_data->date_modified, $object->date_modified, 'Modification dates match in from_row inflation';
        };
    }, 'Zaaksysteem::Object::Model::inflate_from_row');
}

sub test_delete : Tests {
    my $self = shift;
    my $model = $zs->object_model;

    $zs->zs_transaction_ok(sub {
        my $base = $model->rs->count;

        my $object_data = $zs->create_object_data_ok;
        my $object = $model->inflate_from_row($object_data);

        is $model->rs->count, $base + 1,
            'One object in object_data store after create_object_data_ok';

        $model->delete(object => $object);

        is $model->rs->count, $base,
            'No objects in object_data store after deletion of ZS::Object';

        $object_data = $zs->create_object_data_ok;

        is $model->rs->count, $base + 1,
            'One object in object_data store after create_object_data_ok';

        $model->delete(uuid => $object_data->uuid);

        is $model->rs->count, $base,
            'No objects in object_data store after deletion via UUID';
    }, 'Zaaksysteem::Object::Model::delete');

    $zs->zs_transaction_ok(sub {
        require Zaaksysteem::Object::Types::TestObject;
        my $object = Zaaksysteem::Object::Types::TestObject->new();
        my $object_data2 = $zs->create_object_data_ok;
        my $object2 = $model->inflate_from_row($object_data2);

        $object->add_relation(
            Zaaksysteem::Object::Relation->new(
                related_object_id   => $object2->id,
                related_object_type => $object2->type,
                relationship_name_a => 'host',
                relationship_name_b => 'hostage',
                blocks_deletion     => 1,
            )
        );
        my $saved = $model->save(object => $object);

        throws_ok(
            sub { $model->delete(object => $saved); },
            qr{object_model/delete_disallowed_by_relationship},
            'Deleting an object with delete-blocking relationships does not work.',
        );
    }, 'Zaaksysteem::Object::Model::delete with a blocking relation');
}

sub test_retrieve : Tests {
    my $self = shift;
    my $model = $zs->object_model;

    $zs->zs_transaction_ok(sub {
        my $object_data = $zs->create_object_data_ok;

        my $object = $model->retrieve(uuid => $object_data->id);

        isa_ok $object, 'Zaaksysteem::Object', 'object retrieved by uuid';
    }, 'Zaaksysteem::Object::Model::retrieve');
}

sub test_save : Tests {
    my $self = shift;
    my $model = $zs->object_model;

    $zs->zs_transaction_ok(sub {
        my $object = $model->inflate(json => '{ "object_class": "object" }');

        $object = $model->save(object => $object);

        isa_ok $model->rs->first, 'Zaaksysteem::Backend::Object::Data::Component',
            'Object gets inserted in the database';
    }, 'Zaaksysteem::Object::Model::save');
}

sub test_save_object : Tests {
    my $self = shift;
    my $model = $zs->object_model;

    $zs->zs_transaction_ok(sub {
        my $object = $model->inflate_from_json('{ "object_class": "test_object" }');

        isa_ok $object, 'Zaaksysteem::Object::Types::TestObject', 'empty json object instantiates as the correct class';

        my $saved = $model->save_object(object => $object);

        my $log_entries = $zs->schema->resultset('Logging')->search({'me.object_uuid' => $saved->id});
        is ($log_entries->count, 0, "No log entries were made when calling save_object without 'log' argument");
    });

    $zs->zs_transaction_ok(sub {
        my $object = $model->inflate_from_json('{ "object_class": "test_object" }');

        my $saved = $model->save_object(
            object => $object,
            log    => 1
        );

        my $ok = ok($saved->does('Zaaksysteem::Object::Roles::Log'));
        if (!$ok) {
            diag ("Our testobject does not implement the logger role, skipping log creation tests");
            return $ok;
        }

        TODO : {
            local $TODO = "ZS-XXXX: Save object does not log creation";
            my $log_entries = $zs->schema->resultset('Logging')->search({'me.object_uuid' => $saved->id});

            my $ok = is($log_entries->count, 1, "A single log entry were made when calling save_object with true 'log' argument");
            if (!$ok) {
                diag ("Our log entry was not found, skipping tests");
                return $ok;
            }

            my $entry = $log_entries->first;
            is($entry->event_type, 'object/create', "Event has correct event type: object/create");
        }
    }, 'Zaaksysteem::Object::Model::inflate_from_json');
}

sub test_create_log_entry : Tests {
    my $self = shift;
    my $model = $zs->object_model;

    $zs->txn_ok(sub {
        my $attr = $zs->create_bibliotheek_kenmerk_ok(
            magic_string => 'name',
            value_type => 'text'
        );
        my $type = Zaaksysteem::Object::Types::Type->new(
            name => 'UserDefinedType',
            category_id => $attr->get_column('bibliotheek_categorie_id'),
            prefix => 'udt',
            attributes => [
                {
                    index => 0,
                    required => 0,
                    attribute_type => 'Name',
                    attribute_label => 'Name',
                    attribute_id => $attr->id,
                    attribute_object => $attr
                }
            ]
        );
        $model->type_model(Zaaksysteem::Object::TypeModel->new());
        $model->type_model->cache_object_types($type);

        my $meta = $type->instance_meta_class;
        my $instance = $model->save_object(object => $meta->new_object(name => 'somename'));
        my $instance2 = $model->retrieve(uuid => $instance->id);

        $instance2->name('some_other_name');

        {
            $model->_create_log_entry(
                'update',
                $instance,
                $instance2,
            );

            my $log_entries = $zs->schema->resultset('Logging')->search({'me.object_uuid' => $instance->id});
            is($log_entries->count, 1, "_create_log_entry created a log entry");

            my $entry = $log_entries->first;

            is($entry->event_type, "object/update", "An update event was created");
            is_deeply(
                $entry->data,
                {
                    changes => [
                        {
                            "field" => "name",
                            "new_value" => "some_other_name",
                            "old_value" => "somename",
                        }
                    ],
                    "object_label"     => "some_other_name",
                    "object_type"      => "udt",
                    "object_uuid"      => $instance->id,
                },
                "Event log data is correct"
            );

            $entry->delete();
        }

        {
            $model->_create_log_entry(
                'create',
                undef,
                $instance,
            );

            my $log_entries = $zs->schema->resultset('Logging')->search({'me.object_uuid' => $instance->id});
            is($log_entries->count, 1, "_create_log_entry created a log entry");

            my $entry = $log_entries->first;

            is($entry->event_type, "object/create", "A create event was created");
            is_deeply(
                $entry->data,
                {
                    changes => [
                        {
                            "field" => "name",
                            "old_value" => undef,
                            "new_value" => "somename",
                        }
                    ],
                    "object_label"     => "somename",
                    "object_type"      => "udt",
                    "object_uuid"      => $instance->id,
                },
                "Event log data is correct"
            );

            $entry->delete();
        }
    });
}

sub test_object_from_case : Tests {
    my $self = shift;
    my $model = $zs->object_model;

    $zs->zs_transaction_ok(sub {
        my $case = $zs->create_case_ok;

        # XXX Finish test
    }, 'Zaaksysteem::Object::Types::Case');
}

sub test_export_single : Tests {
    my $self = shift;
    my $model = $zs->object_model;

    with_stopped_clock {
        my $now = DateTime->now();

        my $mock_tar = Test::MockObject->new();
        $mock_tar->set_isa('Archive::Tar::Stream');

        my $object = $model->inflate(json => '{ "object_class": "object" }');

        my @export;

        no warnings 'redefine';
        local *Zaaksysteem::Object::Model::_add_scalar_to_export = sub {
            my $self = shift;
            push @export, { scalar => [@_] };
        };
        local *Zaaksysteem::Object::Model::_add_object_to_export = sub {
            my $self = shift;
            push @export, { object => [@_] };
        };
        use warnings 'redefine';

        $model->export_single(
            tar_handle => $mock_tar,
            object     => $object,
            metadata   => {
                extra => 'data',
            },
        );

        is_deeply(
            $export[0],
            {
                scalar => [
                    $mock_tar,
                    'meta.json',
                    JSON->new->pretty->canonical->encode({
                        version => '1',
                        time => $now->clone->set_time_zone('UTC')->iso8601 . 'Z',
                        export_type => 'single',
                        extra       => 'data',
                    }),

                ]
            },
            "meta.json added correctly"
        );
        is_deeply(
            $export[1],
            {
                object => [
                    $mock_tar,
                    $object,

                ]
            },
            "Single object added correctly"
        );
        is(@export, 2, "Correct number of files added to tar stream");
    };
}

sub test_export_multiple : Tests {
    my $self = shift;
    my $model = $zs->object_model;

    $zs->zs_transaction_ok(sub {
        with_stopped_clock {
            my $now = DateTime->now();

            my $mock_tar = Test::MockObject->new();
            $mock_tar->set_isa('Archive::Tar::Stream');

            my $obj1 = $model->save(json => '{ "object_class": "object" }');
            my $obj2 = $model->save(json => '{ "object_class": "object" }');

            my $rs = $zs->schema->resultset('ObjectData')->search(
                { object_class => 'object' },
                { order_by => 'date_created' }
            );

            my @export;

            no warnings 'redefine';
            local *Zaaksysteem::Object::Model::_add_scalar_to_export = sub {
                my $self = shift;
                push @export, { scalar => [@_] };
            };
            local *Zaaksysteem::Object::Model::_add_object_to_export = sub {
                my $self = shift;
                push @export, { object => [@_] };
            };
            use warnings 'redefine';

            $model->export_multiple(
                tar_handle => $mock_tar,
                resultset  => $rs,
                metadata   => { more => 'meta' },
            );

            is_deeply(
                $export[0],
                {
                    scalar => [
                        $mock_tar,
                        'meta.json',
                        JSON->new->pretty->canonical->encode({
                            version     => '1',
                            export_type => 'query',
                            more        => 'meta',
                            time => $now->clone->set_time_zone('UTC')->iso8601 . 'Z',
                        }),

                    ]
                },
                "meta.json added correctly"
            );
            is_deeply(
                $export[1],
                {
                    object => [
                        $mock_tar,
                        $obj1,
                    ]
                },
                "Single object added correctly"
            );
            is_deeply(
                $export[2],
                {
                    object => [
                        $mock_tar,
                        $obj2,
                    ]
                },
                "Single object added correctly"
            );
            is(@export, 3, "Correct number of files added to tar stream");
        };
    });

}

sub test_export_privates : Tests {
    my $self = shift;
    my $model = $zs->object_model;

    my $mock_tar = Test::MockObject->new();
    $mock_tar->set_isa('Archive::Tar::Stream');

    my (@AddFile, @AddFileData);
    $mock_tar->mock('AddFile', sub {
        my $self = shift;

        push @AddFileData, $_[2]->getline;
        push @AddFile, [@_];
    });

    my $test_string = "test scalar";
    $model->_add_scalar_to_export($mock_tar, "foobar", $test_string);

    is(@AddFile, 1, "tar->AddFile called once");
    is($AddFile[0]->[0], 'foobar', 'AddFile() called with correct filename');
    is($AddFile[0]->[1], length($test_string), 'AddFile() called with correct length');
    is($AddFile[0]->[3], 'mode', 'AddFile() called with correct "mode" argument');
    is($AddFile[0]->[4], 0644, 'AddFile() called with correct "mode" argument');

    is($AddFileData[0], $test_string, 'AddFile() called with correct file handle');
}

sub object_model_search : Tests {
    my $model = $zs->object_model;

    $zs->txn_ok(sub {
        my $row = $zs->create_object_data_ok;

        # Test count-ability
        ok $model->count('object', { name => $row->index_hstore->{ name } }),
            'count works, at least one object in store';

        # Do an actual search
        my ($object) = $model->search('object', {
            name => $row->index_hstore->{ name }
        });

        is $row->id, $object->id, 'Correct object retrieved from database';
    });
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
