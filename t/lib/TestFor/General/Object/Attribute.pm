package TestFor::General::Object::Attribute;
use base 'Test::Class';

use TestSetup;
use JSON;

use DateTime;
use Zaaksysteem::Object::Attribute;

sub zs_oa : Tests {
    my $now = DateTime->now();

    my $oa1 = Zaaksysteem::Object::Attribute->new(
        name           => 'foobar',
        attribute_type => 'timestamp',
        value          => $now,
    );

    is_deeply(
        $oa1->human_value,
        $now->strftime('%d-%m-%Y'),
        "Single-value timestamp works OK"
    );

    my $oa2 = Zaaksysteem::Object::Attribute->new(
        name           => 'foobar',
        attribute_type => 'timestamp',
        value          => undef,
    );

    is_deeply(
        $oa2->human_value,
        undef,
        "Single-value timestamp 'undef' works OK"
    );

    my $oa3 = Zaaksysteem::Object::Attribute->new(
        name           => 'foobar',
        attribute_type => 'timestamp',
        value          => [undef, $now],
    );

    my $ok = is_deeply(
        $oa3->human_value,
        [ undef, $now->strftime('%d-%m-%Y') ],
        "Multivalue timestamps work OK"
    );
    if (!$ok) {
        diag explain $oa3->human_value;
    }
}

sub zs_oa_uniq_trait : Tests {
    my $obj = XXX::Test::OA::Traits->new();

    my $outcome = { solo => 1, duo => 0, no_unique => undef, standaard => undef };

    my $res = { map { $_->name => $_->unique } grep { $_->can('has_unique') } $obj->meta->get_all_attributes };
    is_deeply($res, $outcome, "Moose: has_unique trait");

    $res = { map { $_->name => $_->unique } $obj->attribute_instances };
    is_deeply($res, $outcome, "ZS::Object: has_unique trait");
}

sub zs_oa_defined_attr : Tests {

    my $thing = Zaaksysteem::TestUtils::generate_random_string();
    my %opts = (solo => $thing);
    my $obj = XXX::Test::OA::Traits->new(%opts);

    my %attributes;
    my @attr = $obj->attribute_instances;
    foreach (@attr) {
        $attributes{$_->name} = $_->TO_JSON;
        delete $attributes{$_->name}{human_label};
    }

    is_deeply(
        \%attributes,
        {
            duo => {
                'attribute_type' => 'text',
                'name'           => 'duo'
            },
            no_unique => {
                'attribute_type' => 'text',
                'name'           => 'no_unique'
            },
            solo => {
                'attribute_type' => 'text',
                'human_value'    => $thing,
                'name'           => 'solo',
                'value'          => $thing,
            },
            standaard => {
                'attribute_type' => 'text',
                'human_value'    => "Dope shit ouwe",
                'name'           => 'standaard',
                'value'          => "Dope shit ouwe",
            }
        },
        "Attributes to JSON are correct"
    );

    is_deeply($obj->TO_JSON->{values}, { %opts, standaard => "Dope shit ouwe" }, "Options which are not required won't be shown as values");

    use Zaaksysteem::Object::Model;
    {
        no warnings qw(redefine once);
        local *Zaaksysteem::Object::Model::load_object_package = sub { bless{}, 'XXX::Test::OA::Traits' } ;
        my $model = Zaaksysteem::Object::Model->new(schema => $zs->schema);
        my $saved = $model->save_object(object => $obj);
        isnt($saved->{id}, undef, "Object has an ID");
        my $saved_two = $model->save_object(object => $saved);
        is_deeply($saved, $saved_two, "The same");
    }
}

sub zs_oa_filestore_attr : Tests {
    my $filestore       = $zs->create_filestore_ok();

    # Two, one object, one not, to test coercion
    my $filestore_type  = {
        uuid            => $filestore->uuid,
        thumbnail_uuid  => $filestore->thumbnail_uuid,
        filename        => $filestore->original_name,
        size            => $filestore->size,
        mimetype        => $filestore->mimetype,
        md5             => $filestore->md5,
    };

    my $obj             = XXX::Test::OA::Filestore->new(
        profile_picture => [$filestore_type],
    );

    my $model   = Zaaksysteem::Object::Model->new(
        schema => $zs->schema
    );

    $model->package_type_mapping->{filestore} = 'XXX::Test::OA::Filestore';

    $zs->zs_transaction_ok(sub {
        my $back_and_forth = $model->save(object => $obj);

        my ($save) = $model->search('filestore');

        ### Validate we got a Filestore enty back from db
        isa_ok($save->profile_picture->[0], 'HASH');
        is_deeply(
            [sort keys %{ $save->profile_picture->[0] }],
            [qw(filename md5 mimetype size thumbnail_uuid uuid)],
            "Database contents look like a file store entry"
        );

        ### Stringified it is a human name
        is(
            $save->profile_picture->[0]->{filename},
            'FilestoreTest.txt',
            'Got correct filename from db'
        );
    }, 'Check inflate / deflate from database');
}

package XXX::Test::OA::Traits;
use Moose;

extends 'Zaaksysteem::Object';

has solo => (
    is     => 'rw',
    isa    => 'Str',
    traits => [qw[OA]],
    label  => 'We must be unique',
    unique => 1,
);

has standaard => (
    is      => 'rw',
    isa     => 'Str',
    traits  => [qw[OA]],
    label   => 'Default option test',
    default => "Dope shit ouwe",
);

has duo => (
    is     => 'rw',
    isa    => 'Str',
    traits => [qw[OA]],
    label  => "We aren't unique",
    unique => 0,
);

has no_unique => (
    is     => 'rw',
    isa    => 'Str',
    traits => [qw[OA]],
    label  => "We aren't unique",
);


package XXX::Test::OA::Filestore;

use Moose;

extends 'Zaaksysteem::Object';

has profile_picture => (
    is     => 'rw',
    isa    => 'ArrayRef[HashRef]',
    traits => [qw[OA]],
    label  => "A profile picture of hasselhoff",
    type   => 'file',
);


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
