package TestFor::General::Object::Roles::Relation;
use base qw(Test::Class);

use TestSetup;

sub zs_object_roles_relation : Tests {
    $zs->txn_ok(sub {
        my $shuttlecraft = Zaaksysteem::Test::Object::Cochrane->new();
        my $starship = Zaaksysteem::Test::Object::Voyager->new();

        use Zaaksysteem::Object::Model;
        {
            no warnings qw(redefine once);
            local *Zaaksysteem::Object::Model::load_object_package = sub { bless{}, 'Zaaksysteem::Test::Object::Cochrane' };

            my $model = Zaaksysteem::Object::Model->new(schema => $zs->schema);
            $shuttlecraft = $model->save_object(object => $shuttlecraft);

            local *Zaaksysteem::Object::Model::load_object_package = sub { bless{}, 'Zaaksysteem::Test::Object::Voyager' };
            $starship     = $model->save_object(object => $starship);
        }

        $starship->relate($shuttlecraft);
        ok($starship->is_deleteable, "Basic relationship keeps object deleteable");

        _relationship_ok($starship,
            { relationship_name_a => 'related', relationship_name_b => 'related' });

        my %opts = (
            relationship_name_a => "Mothership",
            relationship_name_b => "Shuttlecraft",
            blocks_deletion     => 1,
        );
        $starship->relate($shuttlecraft, %opts);
        ok(!$starship->is_deleteable, "Relationship with 'block_deletion = 1' makes object undeleteable");

        _relationship_ok($starship, \%opts);
    });
}

sub _relationship_ok {
    my ($obj, $want) = @_;

    my $relations = $obj->relations;
    is(@$relations, 1, "One relationship found");
    my $r = $relations->[-1];

    foreach (keys %$want) {
        is $r->$_, $want->{$_}, "$_ has the correct value";
    }
}

package Zaaksysteem::Test::Object::Cochrane;
use Moose;
extends 'Zaaksysteem::Object';
with qw(Zaaksysteem::Object::Roles::Relation);

sub relatable_types {
    return qw(voyager);
}

1;

package Zaaksysteem::Test::Object::Voyager;
use Moose;
extends 'Zaaksysteem::Object';
with qw(Zaaksysteem::Object::Roles::Relation);

has cpt => (
    is => 'ro',
    isa => 'Str',
    default => sub { return 'Janeway' },
);

sub relatable_types {
    return qw(cochrane);
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
