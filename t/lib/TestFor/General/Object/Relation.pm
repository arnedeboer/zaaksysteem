package TestFor::General::Object::Relation;
use base 'Test::Class';

use Data::UUID;
use TestSetup;
use Zaaksysteem::Object::Relation;

sub test_object_relation : Tests {
    my $du = Data::UUID->new();
    my $uuid = $du->create_str();

    my $or = Zaaksysteem::Object::Relation->new(
        related_object_id   => $uuid,
        related_object_type => 'some_type',
        relationship_name_a => 'mother',
        relationship_name_b => 'child',
        relationship_title_a => 'title a',
        relationship_title_b => 'title b',
    );

    isa_ok($or, 'Zaaksysteem::Object::Relation', "New relation");

    ok(
        $or->meta->find_attribute_by_name('related_object_id')->is_required,
        "related_object_id is required",
    );
    ok(
        $or->meta->find_attribute_by_name('related_object_type')->is_required,
        "related_object_type is required",
    );
    ok(
        $or->meta->find_attribute_by_name('relationship_name_a')->is_required,
        "relationship_name_a is required",
    );
    ok(
        $or->meta->find_attribute_by_name('relationship_name_b')->is_required,
        "relationship_name_b is required",
    );
    ok(
        !$or->meta->find_attribute_by_name('relationship_title_a')->is_required,
        "relationship_title_a is not required",
    );
    ok(
        !$or->meta->find_attribute_by_name('relationship_title_b')->is_required,
        "relationship_title_b is not required",
    );

    throws_ok(
        sub { $or->related_object_id("not a uuid") },
        qr/Validation failed/,
        'related_object_id must be a UUID',
    );

    is_deeply(
        $or->TO_JSON,
        {
            related_object_id   => $uuid,
            related_object_type => 'some_type',
            relationship_name_a => 'mother',
            relationship_name_b => 'child',

            related_object      => undef,
            is_inherited        => \0,
            blocks_deletion     => \0,
            owner_object_id     => undef,
        },
        'JSONification of relation is correct'
    );

    my $obj = Zaaksysteem::Object->new();
    $or->related_object($obj);

    is_deeply(
        $or->TO_JSON,
        {
            related_object_id   => $uuid,
            related_object_type => 'some_type',
            relationship_name_a => 'mother',
            relationship_name_b => 'child',

            related_object      => $obj,
            is_inherited        => \0,
            blocks_deletion     => \0,
            owner_object_id     => undef,
        },
        'JSONification of relation with included object is correct'
    );
}

sub test_object_relation_methods : Tests {
    my $du = Data::UUID->new();
    my $uuid = $du->create_str();
    my $owner_uuid = $du->create_str();
    my $other_uuid = $du->create_str();

    my $or = Zaaksysteem::Object::Relation->new(
        related_object_id    => $uuid,
        related_object_type  => 'some_type',
        relationship_name_a  => 'mother',
        relationship_name_b  => 'child',
        relationship_title_a => 'title a',
        relationship_title_b => 'title b',
        owner_object_id      => $owner_uuid,
    );

    isa_ok($or, 'Zaaksysteem::Object::Relation', "New relation");

    is_deeply(
        $or->TO_JSON,
        {
            related_object_id   => $uuid,
            related_object_type => 'some_type',
            relationship_name_a => 'mother',
            relationship_name_b => 'child',

            related_object  => undef,
            is_inherited    => \0,
            blocks_deletion => \0,
            owner_object_id => $owner_uuid,
        },
        'JSONification of relation with owner object is correct'
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
