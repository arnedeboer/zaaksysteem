package TestFor::General::PDFGenerator;
use base qw(Test::Class);

use TestSetup;

use File::Temp qw(tempfile);
use Zaaksysteem::PDFGenerator;

my $template_result = "Dit is random: " . rand();

sub zs_pdfgenerator : Tests {
    my $self = shift;

    my $dummy_schema = $self->_create_dummy_schema();
    my $dummy_tpl = $self->_create_dummy_tpl();
    my $dummy_ctx = $self->_create_dummy_ctx();

    my $pg = Zaaksysteem::PDFGenerator->new(
        schema => $dummy_schema
    );
    isa_ok($pg, 'Zaaksysteem::PDFGenerator');

    my $result = $pg->generate_pdf(
        template => $dummy_tpl,
        context  => $dummy_ctx,
    );

    is_deeply(
        $result,
        {
            'Accept'       => 'application/pdf',
            'Content'      => $template_result,
            'Content_Type' => 'text/plain',
        },
        "The PDF converter was called correctly, and the template processed."
    );
}

sub _create_dummy_schema {
    my $rs = Test::MockObject->new();
    $rs->mock(
        send_to_converter => sub {
            my $self = shift;
            return {@_};
        }
    );

    my $schema = Test::MockObject->new();
    $schema->mock(
        resultset => sub {
            return $rs;
        }
    );

    $schema->set_isa('DBIx::Class::Schema');

    return $schema;
}

sub _create_dummy_tpl {
    my $fs = Test::MockObject->new();
    $fs->mock(
        save_template_document => sub {
            my ($self, $ztt) = @_;

            my ($fh, $filename) = tempfile('testsuiteXXXX');
            print $fh $ztt->process_template('[[context]]')->string;

            return $filename;
        }
    );
    $fs->mock(original_name => sub { return 'testje.txt' });

    my $tpl = Test::MockObject->new();

    $tpl->mock(filestore_id => sub { return $fs });
    $tpl->set_isa('Zaaksysteem::DB::Component::BibliotheekSjablonen');

    return $tpl;
}

sub _create_dummy_ctx {
    my $ctx = Test::MockObject->new();
    $ctx->mock(
        get_string_fetchers => sub {
            return sub {
                return unless shift->name eq 'context';

                return Zaaksysteem::ZTT::Element->new(value => $template_result);
            };
        }
    );
    $ctx->mock(get_context_iterators => sub { return {} });
    $ctx->set_isa('Zaaksysteem::ZTT::Context::WOZ');

    return $ctx;
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
