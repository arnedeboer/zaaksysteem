package TestFor::General::Case::Create;
use base qw(Test::Class);
use TestSetup;

=head1 NAME

TestFor::General::Case::Create - Create case scenario's for Zaaksysteem

=head1 SYNOPSIS

    TEST_METHOD=zs_create_case_ok.* ./zs_prove -v t/testclass/100*.t

=cut

sub zs_create_case_ok : Tests {

    $zs->zs_transaction_ok(
        sub {
            my $case
                = $zs->create_case_ok(registratiedatum => DateTime->now(),);
            ok($case->id, "Found a case ID");

        },
        "Valid datetime object"
    );

    $zs->zs_transaction_ok(
        sub {
            my $case = $zs->create_case_ok(
                registratiedatum => DateTime->now->datetime());
            ok($case->id, "Found a case ID");
        },
        "DateTime->datetime()"
    );

    $zs->zs_transaction_ok(
        sub {
            my $zt = $zs->create_zaaktype_predefined_ok();
            my $days_running = 2;

            my $case = $zs->create_case_ok(zaaktype => $zt, registratiedatum => DateTime->now->subtract(days => $days_running));

            my $def = $case->zaaktype_node_id->zaaktype_definitie_id;

            my $days_left = $def->servicenorm - $days_running;
            my $days_perc = sprintf("%d", $days_running/5 * 100);

            my $case_json = $case->object_data->TO_JSON->{values};

            is($case->days_left, $days_left, 'Got correct days left');
            is($case_json->{'case.days_left'}, $days_left, "Object data has correct days left");

            is($case->days_running, $days_running, 'Got correct days running');
            TODO : {
                local $TODO = "days running is not implemented in case objects";
                is($case_json->{'case.days_running'}, $days_running, "Object data has correct days running");
            }

            is($case->days_perc, $days_perc, 'Got correct days percentage');
            is($case_json->{'case.progress_days'}, $days_perc, "Object data has correct days perc");

        },
        'Correctly calculated days'
    );
}

sub zs_case_toewijzing : Tests {

    $zs->zs_transaction_ok(
        sub {

            my $medewerker = $zs->create_medewerker_ok()->subject_id;
            my $case       = $zs->create_case_ok(
                assignee_id => 'betrokkene-medewerker-' . $medewerker->id
            );
            my $behandelaar = $case->behandelaar;
            isa_ok($behandelaar, "Zaaksysteem::Schema::ZaakBetrokkenen");
            is($behandelaar->betrokkene_id, $medewerker->id,
                "The correct behandelaar is set");
        },
        "Create case met toewijzing behandelaar"
    );

    $zs->zs_transaction_ok(
        sub {

            my $case       = $zs->create_case_ok(
                ou_id   => 42,
                role_id => 666,
            );

            my $res = { map { $_ => $case->$_ } qw(route_ou route_role) };
            is_deeply(
                $res,
                { route_role => 666, route_ou => 42 },
                "Group and role are set correctly"
            );
        },
        "Create case met toewijzing route"
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
