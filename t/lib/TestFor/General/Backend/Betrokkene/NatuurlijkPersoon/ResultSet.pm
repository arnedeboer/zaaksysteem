package TestFor::General::Backend::Betrokkene::NatuurlijkPersoon::ResultSet;

use base qw(Test::Class);

use Moose;
use TestSetup;

sub betrokkene_natuurlijkpersoon_create_natuurlijk_persoon : Tests {
    my $params  = {
        a_nummer                => '1234567890',
        burgerservicenummer     => '987654321',
        voornamen               => 'Tinus',
        voorletters             => 'TV',
        voorvoegsel             => 'vander',
        geslachtsnaam           => 'Testpersoon',
        geboortedatum           => '19830609',
        datum_overlijden        => '19830610',
        indicatie_geheim        => undef,
        burgerlijkestaat        => undef,
        aanduiding_naamgebruik  => 'E',
        sleutel_gegevensbeheer  => '234234234',
        sleutel_verzendend      => '23423423424234',
        postcode                => '1051JL',
        huisnummer              => '7',
        straatnaam              => 'Donker Curtiusstraat',
        woonplaats              => 'Amsterdam',
        geslachtsaanduiding     => 'M',
        functie_adres           => 'B',
    };

    throws_ok(
        sub {
            $schema->resultset('NatuurlijkPersoon')->create_natuurlijk_persoon(
                $params,
                {
                    authenticated => 'gbaaa'
                }
            );
        },
        qr/Authenticated must match regex/,
        'Valid exception for authenticated flag'
    );

    throws_ok(
        sub {
            $schema->resultset('NatuurlijkPersoon')->create_natuurlijk_persoon(
                {
                    %$params,
                    burgerservicenummer => 'abc'
                }
            );
        },
        qr/Validation of profile failed/,
        'Valid parameter checking for call'
    );

    $zs->zs_transaction_ok(sub {
        my $entry = $schema->resultset('NatuurlijkPersoon')->create_natuurlijk_persoon(
            {
                %$params,
            }
        );

        ok(!$entry->authenticated, 'No authenticated entry created');
        ok(!$entry->authenticatedby, 'No authenticatedby param set');

        for my $key (keys %$params) {
            my $checkentry;
            if ($entry->can($key)) {
                $checkentry = $entry;
            } elsif ($entry->adres_id->can($key)) {
                $checkentry = $entry->adres_id;
            }

            next unless $checkentry;

            my $val = $params->{$key};
            if ($key =~ /geboortedatum|datum_overlijden/) {
                $val =~ s/(\d{4})(\d{2})(\d{2})/$1-$2-$3T00:00:00/;
            }

            is($val, $checkentry->$key, 'Found correct value for: ' . $key);
        }

    }, 'Created non authenticated person');

    $zs->zs_transaction_ok(sub {
        my $entry = $schema->resultset('NatuurlijkPersoon')->create_natuurlijk_persoon(
            {
                %$params,
            },
            {
                authenticated => 'digid',
            }
        );

        ok($entry->authenticated, 'authenticated entry created');
        is($entry->authenticatedby, 'digid', 'authenticatedby param set to digid');

    }, 'Created an authenticated person');

}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

