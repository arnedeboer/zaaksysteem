package TestFor::General::Backend::Groups;
use base qw(Test::Class);

use Moose;
use TestSetup;

# use JSON qw/decode_json encode_json/;


=head1 NAME

TestFor::General::Backend::Groups - Zaaksysteem Subject Groups test

=head1 LAYOUT

This file implements all USAGE tests, in other words: if you would like
to use the engine, here is your inspiration.

=head1 USAGE (or end-to-end)

Usage tests: use the below tests when you would like to give this module
a spin.

=head2 groups_usage_getting_groups_for_subject

Gets the group belonging to this user. Until further notice: this will always
return exactly one entry (or none where there are none)

=cut


sub groups_usage_getting_groups_for_subject : Tests {
    my $self            = shift;

    $zs->zs_transaction_ok(sub {
        my $subject = $self->_generate_subject_and_groups;

        is($subject->primary_groups->[0]->name, 'Devops', 'Subject belongs to backoffice/devops');
        is($subject->inherited_groups->[0]->name, 'Mintlab', 'Subject belongs to backoffice, inherits rights from Mintlab');
        is($subject->inherited_groups->[1]->name, 'Backoffice', 'Subject belongs to backoffice, inherits rights from Mintlab/Backoffice');

        is(@{ $subject->primary_groups }, 1, 'Found exactly one primary group');
        ok(scalar @{ $subject->inherited_groups }, 'Found one or more inherited groups');
    }, 'groups: primary_groups and inherited_groups');
}

sub groups_usage_update_group : Tests {
    my $self            = shift;

    $zs->zs_transaction_ok(sub {
        my $parent = $schema->resultset('Groups')->create_group(
            {
                name            => "Mintlab",
                description     => "Mintlab Test Organization",
            }
        );

        ok($parent, 'Created primary organization group');

        my $child = $schema->resultset('Groups')->create_group(
            {
                name            => "Backoffice",
                description     => "Mintlab Backoffice",
                parent_group_id => $parent->id,
            }
        );

        ok($child, 'Created backoffice group');

        my $newchild = $schema->resultset('Groups')->find($child->id);

        ok($newchild, 'Found backoffice group');

        $newchild   = $newchild->update_group(
            {
                name            => 'Backstoffice',
                description     => 'Backstoffice',
            }
        );

        is($newchild->name, 'Backstoffice', 'Updated name: Backoffice to Backstoffice');
        is($newchild->description, 'Backstoffice', 'Updated description: Backoffice to Backstoffice');

        lives_ok(
            sub {
                $newchild   = $newchild->update_group(
                    {
                        name            => 'Backstoffice with space',
                        description     => 'Backstoffice with space',
                    }
                );
            },
            'Succesfully updated a group with whitespace character in name or description',
        );

    }, 'groups_usage_update_group: Tested updating of groups');
}



=head2 subject_is_member_of

=head2 subject_

=cut

sub _generate_subject_and_groups {
    my $self            = shift;

    my $groups          = $zs->create_default_groups_ok();
    my ($backoffice)    = grep { $_->name eq 'Devops' } @$groups;

    ### Create subject in backoffice
    my $subject         = $zs->create_subject_ok(username => 'gebruiker');

    $subject->set_primary_groups([$backoffice->id]);
}

=head2 IMPLEMENTATION_TESTS

=head2 groups_create_group

Tests creation of groups

=cut

sub groups_create_group : Tests {
    my $self            = shift;

    $zs->zs_transaction_ok(sub {
        throws_ok(
            sub {
                $schema->resultset('Groups')->create_group({name => 'bla', description => 'bla', parent_group_id => 'bla' });
            },
            qr/invalid: parent_group_id/s,
            'Checking parameter validation'
        );

        throws_ok(
            sub {
                $schema->resultset('Groups')->create_group({description => 'bla', parent_group_id => 44 });
            },
            qr/missing: name/,
            'Checking parameter validation'
        );

        my $parent = $schema->resultset('Groups')->create_group(
            {
                name            => "Mintlab",
                description     => "Mintlab Test Organization",
            }
        );

        throws_ok(
            sub {
                $schema->resultset('Groups')->create_group({name => 'valid', description => 'bla', parent_group_id => ($parent->id + 1) });
            },
            qr/Cannot find parent row by given id/,
            'Checking parameter validation'
        );

        my $child = $schema->resultset('Groups')->create_group(
            {
                name            => "Backoffice",
                description     => "Mintlab Backoffice",
                parent_group_id => $parent->id,
            }
        );

        ok($parent->id, 'group row: found id');
        is($parent->name, 'Mintlab', 'group row: found correct name');
        is($parent->description, 'Mintlab Test Organization', 'group row: found correct name');
        is($parent->path->[0], $parent->id, 'group row: Found primary id in path as first value');

        is(scalar @{ $child->path }, 2, 'child row: path contains two entries');
        is($child->path->[0], $parent->id, 'child row: first value of path is parent id');
        is($child->path->[1], $child->id, 'child row: second value of path is child id');
    }, 'groups_create_group: create parent and child group');

    $zs->zs_transaction_ok(sub {
        $schema->resultset('Groups')->create_group({name => 'bla1', description => 'bla1', override_id => 2 });
        $schema->resultset('Groups')->create_group({name => 'bla2', description => 'bla2', override_id => 1 });
        lives_ok(
            sub {
                $schema->resultset('Groups')->create_group({name => 'bla3', description => 'bla3' });
            },
            'No duplicate ID errors'
        );
    });
}

=head2 groups_set_primary_groups_on_subject

Tests creation of groups

=cut

sub groups_set_primary_groups_on_subject : Tests {
    my $self            = shift;

    $zs->zs_transaction_ok(sub {
        my $subject         = $zs->create_subject_ok(username => 'gebruiker');

        my $groups          = $zs->create_default_groups_ok();
        my ($backoffice)    = grep { $_->name eq 'Backoffice' } @$groups;

        throws_ok(
            sub {
                $schema->resultset('Groups')->set_primary_groups_on_subject($subject, ['aa']);
            },
            qr/Only group IDs allowed/,
            'set_primary_groups_on_subject: groups should to be a list of numbers'
        );

        throws_ok(
            sub {
                $schema->resultset('Groups')->set_primary_groups_on_subject($subject, [2424, 234]);
            },
            qr/exactly one group id/,
            'set_primary_groups_on_subject: groups should be exactly one group'
        );

        throws_ok(
            sub {
                $schema->resultset('Groups')->set_primary_groups_on_subject($subject, [($backoffice->id+10)]);
            },
            qr/One or more given group ids not/,
            'set_primary_groups_on_subject: one or more given group ids not found in database'
        );

        $schema->resultset('Groups')->set_primary_groups_on_subject($subject, [ $backoffice->id ]);
        is($subject->group_ids->[0], $backoffice->id, 'set_primary_groups_on_subject: group_ids match given backoffice->id');

    }, 'set_primary_groups_on_subject: Tested function implementation');
}

sub groups_groups_for_subject: Tests {
    my $self            = shift;

    $zs->zs_transaction_ok(sub {
        my $subject         = $self->_generate_subject_and_groups();

        my $rv              = $schema->resultset('Groups')->groups_for_subject($subject);

        ok($rv->{primary_groups} && (scalar(@{ $rv->{primary_groups} }) == 1), 'Found single primary group');
        ok($rv->{inherited_groups} && (scalar(@{ $rv->{inherited_groups} }) == 2), 'Found tree of inherited groups');

        is($rv->{primary_groups}->[0]->name, 'Devops', 'Subject belongs to backoffice');
        is($rv->{inherited_groups}->[0]->name, 'Mintlab', 'Subject belongs to devops, inherits rights from Mintlab');
        is($rv->{inherited_groups}->[1]->name, 'Backoffice', 'Subject belongs to devops, inherits rights from Mintlab/Backoffice');
    }, 'groups_for_subject: Tested function implementation');
}

sub groups_get_all_cached: Tests {
    my $self            = shift;

    $zs->zs_transaction_ok(sub {
        my $subject         = $self->_generate_subject_and_groups();

        my $rv              = $schema->resultset('Groups')->get_all_cached;

    }, 'groups_get_all_cached: Tested function implementation');
}

sub groups_get_as_tree: Tests {
    my $self            = shift;

    $zs->zs_transaction_ok(sub {
        my $subject         = $self->_generate_subject_and_groups();

        my $rv              = $schema->resultset('Groups')->get_as_tree;

    }, 'groups_get_as_tree: Tested function implementation');
}


=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

1;
