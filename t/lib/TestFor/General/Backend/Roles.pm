package TestFor::General::Subject::Roles;

use base qw(Test::Class);

use Moose;
use TestSetup;

# use JSON qw/decode_json encode_json/;


=head1 NAME

TestFor::General::Backend::Roles - Zaaksysteem Subject Groups test

=head1 LAYOUT

This file implements all USAGE tests, in other words: if you would like
to use the engine, here is your inspiration.

=head1 USAGE (or end-to-end)

Usage tests: use the below tests when you would like to give this module
a spin.

=head2 roles_usage_getting_roles_for_subject

Gets the group belonging to this user. Until further notice: this will always
return exactly one entry (or none where there are none)

=cut


sub roles_usage_getting_roles_for_subject : Tests {
    my $self            = shift;

    $zs->zs_transaction_ok(sub {
        my $subject = $schema->resultset('Subject')->search({username => 'admin'})->first;

        is(scalar(@{ $subject->roles }), 1, 'Got one role');

        ok($subject->roles->[0]->name, 'Got a name for this role: ' . $subject->roles->[0]->name )
    }, 'groups: primary_groups and inherited_groups');
}

sub roles_usage_update_role : Tests {
    my $self            = shift;

    $zs->zs_transaction_ok(sub {
        my $parent_group = $schema->resultset('Groups')->create_group(
            {
                name            => "Mintlab",
                description     => "Mintlab Test Organization",
            }
        );

        my $systemrole = $schema->resultset('Roles')->create_role(
            {
                name            => "Behandelaar",
                description     => "System role: administrator",
                parent_group_id => $parent_group->id,
                system_role     => 1,
            }
        );

        throws_ok(
            sub {
                $systemrole->update_role(
                    {
                        name            => 'Bestandelaar',
                        description     => 'Bestandelaar',
                    }
                );
            },
            qr/authorized to alter systemrole/,
            'Valid check for altering system_role'
        );

        my $updated_system_role = $systemrole->update_role(
            {
                name            => 'Bestandelaar',
                description     => 'Bestandelaar',
                change_systemrole => 1,
            }
        );

        is($updated_system_role->name, 'Bestandelaar', 'Updated systemrole name: Behandelaar to Bestandelaar');
        is($updated_system_role->description, 'Bestandelaar', 'Updated systemrole description: Behandelaar to Bestandelaar');


        my $role = $schema->resultset('Roles')->create_role(
            {
                name            => "Behandelaar",
                description     => "role: behandelaar",
                parent_group_id => $parent_group->id,
            }
        );

        ok($role, 'Created behandelaar role');

        my $newrole = $schema->resultset('Roles')->find($role->id);

        ok($newrole, 'Found behandelaar role');

        $newrole   = $newrole->update_role(
            {
                name            => 'Bestandelaar',
                description     => 'Bestandelaar',
            }
        );

        is($newrole->name, 'Bestandelaar', 'Updated name: Behandelaar to Bestandelaar');
        is($newrole->description, 'Bestandelaar', 'Updated description: Behandelaar to Bestandelaar');

        lives_ok(
            sub {
                $newrole->update_role(
                    {
                        name            => 'Bestandelaar met spatie',
                        description     => 'Bestandelaar met spatie',
                    }
                );
            },
            'Succesfully updated a role with whitespace character in name or description',
        );

    }, 'roles_usage_update_role: update a role');
}

sub _generate_subject_groups_and_roles {
    my $self            = shift;
}

=head2 IMPLEMENTATION_TESTS

=head2 roles_create_role

Tests creation of groups

=cut

sub roles_create_role : Tests {
    my $self            = shift;

    $zs->zs_transaction_ok(sub {
        throws_ok(
            sub {
                $schema->resultset('Roles')->create_role({name => 'bla', description => 'bla', parent_group_id => 'bla' });
            },
            qr/invalid: parent_group_id/s,
            'Checking parameter validation'
        );

        my $parent_group = $schema->resultset('Groups')->create_group(
            {
                name            => "Mintlab",
                description     => "Mintlab Test Organization",
            }
        );

        throws_ok(
            sub {
                $schema->resultset('Roles')->create_role({name => 'valid', description => 'bla', parent_group_id => ($parent_group->id + 1) });
            },
            qr/Cannot find group row/,
            'Checking for correct parent_group_id'
        );

        my $role = $schema->resultset('Roles')->create_role(
            {
                name            => "Behandelaar",
                description     => "System role: behandelaar",
                parent_group_id => $parent_group->id,
                system_role     => 1,
            }
        );

        ok($role->id, 'role row: found id');
        is($role->name, 'Behandelaar', 'role row: found correct name');
        is($role->description, 'System role: behandelaar', 'role row: found correct name');
        is($role->parent_group_id->id, $parent_group->id, 'role row: Found group id as parent_group_id');
        ok($role->system_role, 'role row: role created as system row');

        my $customrole = $schema->resultset('Roles')->create_role(
            {
                name            => "Wijkbeheer",
                description     => "Custom role: wijkbeheer",
            }
        );

        is($customrole->name, 'Wijkbeheer', 'role row: found correct name for custom row');
        ok(!$customrole->system_role, 'role row: role not created as system row');
    }, 'groups_create_group: create parent and child group');

    $zs->zs_transaction_ok(sub {
        ### Get max(id) from table
        my $max_id_row  = $schema->resultset('Roles')->search(
            {},
            {
                'select'    => [{ MAX => 'id', '-as' => 'maxid'}],
            }
        )->first;

        my $max_id      = $max_id_row->get_column('maxid');

        $schema->resultset('Roles')->create_role({name => 'bla1', description => 'bla1', override_id => ($max_id + 2) });
        $schema->resultset('Roles')->create_role({name => 'bla2', description => 'bla2', override_id => ($max_id + 1) });
        lives_ok(
            sub {
                $schema->resultset('Roles')->create_role({name => 'bla3', description => 'bla3' });
            },
            'No duplicate ID errors'
        );
    });
}

sub roles_set_roles_on_subject : Tests {
    my $self            = shift;

    $zs->zs_transaction_ok(sub {
        my $subject         = $zs->create_subject_ok(username => 'gebruiker');

        throws_ok(
            sub {
                $schema->resultset('Roles')->set_roles_on_subject($subject, [qw/a b c/]);
            },
            qr/Only role IDs/s,
            'Checking parameter validation'
        );

        throws_ok(
            sub {
                $schema->resultset('Roles')->set_roles_on_subject($subject, [99999999]);
            },
            qr/One or more given role ids not found in database/s,
            'Invalid group ids'
        );

        ### Get parent group
        my $group = $schema->resultset('Groups')->search({name => 'Mintlab'})->first;

        my $role = $schema->resultset('Roles')->create_role(
            {
                name            => "Behandelaarr",
                description     => "System role: behandelaar",
                parent_group_id => $group->id,
                system_role     => 1,
            }
        );

        ok(
            $schema->resultset('Roles')->set_roles_on_subject($subject, [$role->id]),
            'subject roles: set role_id on subject'
        );

        ok(grep({ $role->id eq $_ } @{ $subject->role_ids }), 'subject roles: Found role ids');

    }, 'groups_create_group: create parent and child group');
}

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

1;
