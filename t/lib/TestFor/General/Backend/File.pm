package TestFor::General::Backend::File;
use base qw(Test::Class);

use TestSetup;

use Encode qw(encode_utf8 decode_utf8);
use File::Basename;
use File::Copy qw(cp);
use File::Spec::Functions;
use File::Temp qw(tempfile);

use constant OUTLOOK_DIR => catfile('t', 'inc', 'OutlookTestMails');

sub zs_backend_file_document_status : Tests {
    $zs->txn_ok(
        sub {

            my $file = $zs->create_file_ok();
            is($file->document_status, 'original', 'documentstatus is set correctly to the default');

            $file = $zs->create_file_ok(
                db_params => {
                    document_status => 'copy',
                }
            );
            is($file->document_status, 'copy', 'documentstatus is set correctly via file_create');

            foreach my $status (qw(converted copy replaced original)) {
                $file->update_properties(
                    {
                        document_status => $status,
                        subject         => "testsuite"
                    }
                );
                is($file->document_status, $status, "documentstatus is set correctly to: $status");
            }

            throws_ok(
                sub {
                    $zs->create_file_ok(
                        db_params => { document_status => 'meuk', });
                },
                qr/Invalid options given: document_status/,
                "meuk is not a document_status"
            );

        },
        "Document status"
    );
}

sub zs_backend_file_outlook_attachments_duplicate_mails : Tests {
    $zs->zs_transaction_ok(
        sub {

            my $r = outlook_files_ok(
                mail   => 'Tekst met bijlage - PDF.msg',
                expect => 4,
            );

            my $case  = $r->{case};
            my $files = $r->{files};

            my $mail_one = catfile(OUTLOOK_DIR, 'Tekst met bijlage - PDF.msg');
            my ($fh, $mail_two) = tempfile(UNLINK => 1, SUFFIX => '.msg');
            close($fh);

            cp($mail_one, $mail_two);
            my $file_two = _create_file($case, $mail_two);
            $files = $zs->schema->resultset('File')->search_rs({ case_id => $case->id });

            # Because we now apply more logic a mail which is send twice
            # results in several files which are again dupes of eachother, if
            # the subject is the same and the case is the same.
            # Perhaps we should do something with a message id as well.
            my $accepted = 0;
            my $not_accepted = 0;
            while (my $f = $files->next) {
                if ($f->accepted) {
                    $accepted++;
                }
                else {
                    $not_accepted++;
                }
            }

            is_deeply(
                { accepted => $accepted, not_accepted => $not_accepted },
                { accepted => 5,         not_accepted => 3 },
                "All files are accepted",
            );

            unlink($mail_two);
        },
        "All files accepted from two different mails with similar files"
    );
}

sub zs_backend_file_outlook_attachments_evil_file_names : Tests {
    $zs->zs_transaction_ok(
        sub {
            outlook_files_ok(
                mail   => decode_utf8('Test met diakriet ë en ☃.msg'),
                expect => 3,
            );
        },
        "ZS-5925 - Gemailde bestanden met accenten etc werken naar behoren",
    );
    $zs->zs_transaction_ok(
        sub {
            outlook_files_ok(
                mail   => 'ZS-1210.msg',
                expect => 7,
            );
        },
        "ZS-1210 - Gemailde bestanden met haakjes (and more?) komen niet aan in documentintake",
    );
}

sub zs_backend_file_outlook_attachments_html_mail : Tests {
    $zs->zs_transaction_ok(
        sub {
            outlook_files_ok(
                mail   => 'HTML_mail.msg',
                expect => 4,
                todo   => "Bug in Outlook::Email",
            );
        },
        "ZS-4605 Outlook mails met HTML worden niet goed geconverteerd",
    );
}

sub zs_backend_file_outlook_attachments_eml_mail : Tests {
    my $self = shift;
    if (!$ENV{ZS_DEVELOPER_MAIL}) {
        $self->builder->skip(
            "We don't have dummy mails with this scenario yet"
        );
    }
    return;

    $zs->zs_transaction_ok(
        sub {
                outlook_files_ok(
                    mail   => 'nested_eml.msg',
                    expect => 6,
                );
        },
        "ZS-5498: Nested eml in Outlook mails"
    );
}

sub zs_backend_file_resultset_search_active : Tests {
    $zs->zs_transaction_ok(
        sub {
            my $r = outlook_files_ok(
                mail   => 'Tekst met bijlage - PDF.msg',
                expect => 4,
            );
            my $found = $r->{case}->search_active_files;
            is($found->count, $r->{files}->count, "Active equals files count");
        },
        "Search active files",
    );
}

sub _create_file {
    my ($case, $file) = @_;
    my ($filename, undef, $suffix) = fileparse($file, '\.[^\.]*');
    my $create_opts = {
        db_params => {
            case_id    => $case->id,
            created_by => $zs->get_subject_ok,
            accepted   => 1,
        },
        file_path => $file,
        name      => $filename . $suffix,
    };

    return $zs->schema->resultset('File')->file_create($create_opts);
}

sub outlook_files_ok {
    my %args = @_;

    my $mail = catfile(OUTLOOK_DIR, $args{mail});
    my $case = $zs->create_case_ok();

    my $file = _create_file($case, $mail);
    my $files = $zs->schema->resultset('File')->search_rs({ case_id => $case->id });

    my $msg = encode_utf8("Found $args{expect} file in $args{mail}");

    if (defined $args{todo}) {
        TODO : {
            local $TODO = $args{todo};
            is($files->count, $args{expect}, $msg);
        }
    }
    else {
        is($files->count, $args{expect}, $msg);
    }
    return { case => $case, files => $files };

}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
