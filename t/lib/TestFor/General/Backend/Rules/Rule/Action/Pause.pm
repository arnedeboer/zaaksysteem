package TestFor::General::Backend::Rules::Rule::Action::Pause;
use base qw(Test::Class);

use Moose;
use TestSetup;
use File::Spec::Functions qw(catfile);

use Zaaksysteem::Backend::Rules;
use JSON qw/decode_json encode_json/;


=head1 NAME

TestFor::General::Backend::Rules::Rule::Condition::Pause - Rule condition: Pause

=head1 CODE TESTS

Code tests, testing the implementation itself

=head2 rules_rule_action_pause_result

=cut

sub rules_rule_action_pause_result : Tests {
    my $self            = shift;

    $zs->zs_transaction_ok(sub {
        my ($casetype, $zaaktype_status, $main_attr) = $self->_generate_rules_for_pause;

        my $case     = $zs->create_case_ok(zaaktype => $casetype);

        my $engine   = Zaaksysteem::Backend::Rules->new_from_case(
            $case,
            {
                'case.number_status'   => 1,
            }
        );

        ### Second condition, with undef values, should be validated true
        is_deeply(
            $engine->rules->[0]->then->[0]->TO_JSON,
            {
                data => {
                    message             => 'Gepauzeerd',
                    copy_attributes     => 1,
                    start_case          => {
                        prefill         => {
                            aanvrager_type      => undef,
                            zaaktype        => {
                                titel               => $casetype->zaaktype_node_id->titel,
                                zaaktype_id         => $casetype->id,
                            }
                        }
                    }
                },
                type => 'pause_application'
            }
        );
    }, 'action::hidegroup: checked hide_group for missing group');
}

sub _generate_rules_for_pause {
    my $self            = shift;
    my (%opts)          = @_;

    my $zaaktype_node   = $zs->create_zaaktype_node_ok;
    my $casetype        = $zs->create_zaaktype_ok(node => $zaaktype_node);
    my $zaaktype_status = $zs->create_zaaktype_status_ok(
        status => 1,
        fase   => 'registratiefase',
        node   => $zaaktype_node
    );
    my $afhandelstatus  = $zs->create_zaaktype_status_ok(
        status => 3,
        fase   => 'afhandelfase',
        node   => $zaaktype_node
    );

    $zs->create_zaaktype_resultaat_ok(
        status      => $afhandelstatus,
        resultaat   => 'aangekocht',
    );

    $zs->create_zaaktype_resultaat_ok(
        status      => $afhandelstatus,
        resultaat   => 'verwerkt',
    );

    ### Generate group1
    $zs->create_zaaktype_kenmerk_ok(
        status              => $zaaktype_status,
        is_group            => 1,
        label               => 'Benodigde gegevens'
    );

    ### Generate zaaktype_kenmerken, checkboxes
    my $zt_kenmerk = $zs->create_zaaktype_kenmerk_ok(
        status              => $zaaktype_status,
        bibliotheek_kenmerk => $zs->create_bibliotheek_kenmerk_ok(
            naam            => 'checkbox_wijken',
            magic_string    => 'checkbox_wijken',
            value_type      => 'checkbox',
            values          => [
                {
                    value   => 'Noord',
                    active  => 1,
                },
                {
                    value   => 'Oost',
                    active  => 1,
                },
                {
                    value   => 'Zuid',
                    active  => 1,
                },
                {
                    value   => 'West',
                    active  => 1,
                },
                {
                    value   => 'Geen',
                    active  => 1,
                }
            ]
        )
    );

    ### Generate group2
    my $main_attr = $zs->create_zaaktype_kenmerk_ok(
        status              => $zaaktype_status,
        is_group            => 1,
        label               => 'Group2'
    );

    ### Generate zaaktype_kenmerken, checkboxes
    my $bag_adres = $zs->create_zaaktype_kenmerk_ok(
        status              => $zaaktype_status,
        bibliotheek_kenmerk => $zs->create_bibliotheek_kenmerk_ok(
            naam            => 'bag_adres',
            magic_string    => 'bag_adres',
            value_type      => 'bag_adres',
        )
    );


    $zs->create_zaaktype_regel_ok(
        status  => $zaaktype_status,
        # node => $zaaktype_node,
        naam    => 'Rule check: hide_group',
        settings => {
            'voorwaarde_1_kenmerk'          => $zt_kenmerk->bibliotheek_kenmerken_id->id,
            'voorwaarde_1_value'            => 'Zuid',
            'voorwaarde_1_value_checkbox'   => '1',
            'voorwaarden'                   => '1',


            'actie_1'                       => 'pauzeer_aanvraag',
            'actie_1_copy_properties'       => 'on',
            'actie_1_message'               => 'Gepauzeerd',
            'actie_1_startzaak'             => $zaaktype_node->zaaktype_id->id,
            'actie_1_zaaktype'              => $zaaktype_node->titel,
            'actie_1_zaaktype_id'           => $zaaktype_node->zaaktype_id->id,
            'acties'                        => '1',

            'naam'                          => 'Hide Group',
        }
    );

    return ($casetype, $zaaktype_status, $main_attr);
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
