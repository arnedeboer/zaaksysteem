package TestFor::General::Backend::Rules::Rule::Condition;
use base qw(Test::Class);

use Moose;
use TestSetup;
use File::Spec::Functions qw(catfile);

use Zaaksysteem::Backend::Rules;
use Zaaksysteem::Zaaktypen;
use JSON qw/decode_json encode_json/;


=head1 NAME

TestFor::General::Backend::Rule::Condition - Condition code tests

=head1 LAYOUT

This file implements all USAGE tests, in other words: if you would like
to use the rules engine, here is your inspiration.

The files below this module, e.g.
L<TestFor::General::Backend::Rules::Rule::Condition::Zipcode>, will demonstrate
the specific usage of a condition or action, and also implements the backend
tests for the different internal functions.

=head1 USAGE (or end-to-end)

Usage tests: use the below tests when you would like to give the new
rules engine a spin.

=head2 _find_condition

Validates a given set of params against rules, and returns a validation object containing
the "active" actions, visible (and hidden) fields, visibile (and hidden) groups

=cut

sub rules_rule__conditions_match : Tests {
    my $self        = shift;

    $zs->zs_transaction_ok(sub {
        my ($casetype, $zaaktype_status) = $self->_generate_rule_casetype;

        my $case        = $zs->create_case_ok(zaaktype => $casetype);

        # SleurEnPleur
        my $params      = Zaaksysteem::Backend::Rules->generate_object_params(
            {
                'case'                      => $case,
                'case.number_status'        => 1,
            }
        );

        ### Rule basic check: hide
        my $rule       = $params->{rules}->rules->[0];

        ok(
            !$rule->_conditions_match($params->{rule_params}),
            $rule->label . ': No conditions match with params'
        );

        ok(
            $rule->_conditions_match(
                {
                    %{ $params->{rule_params} },
                    'attribute.checkbox_hide' => 'hide'
                }
            ),
            $rule->label . ': Conditions match with params: hide'
        );

        ok(
            $rule->_conditions_match(
                {
                    %{ $params->{rule_params} },
                    'attribute.checkbox_hide' => ['hide','bogus']
                }
            ),
            $rule->label . ': Conditions match with params: ["hide", "bogus"]'
        );

        ok(
            !$rule->_conditions_match(
                {
                    %{ $params->{rule_params} },
                    'attribute.checkbox_hide' => ['not','valid']
                }
            ),
            $rule->label . ': Conditions NO match with params: ["not","valid"]'
        );

    }, 'Tested basic condition: hide field');

}

=head1 INTERNAL FUNCTIONS

Internal test functions below

=cut

use constant BASIC_CHECKS => [qw/
    hide
    show
    set_value
/];

sub _generate_rule_casetype {
    my $self            = shift;

    my $zaaktype_node   = $zs->create_zaaktype_node_ok;
    my $casetype        = $zs->create_zaaktype_ok(node => $zaaktype_node);
    my $zaaktype_status = $zs->create_zaaktype_status_ok(
        status => 1,
        fase   => 'registratiefase',
        node   => $zaaktype_node
    );
    $zs->create_zaaktype_status_ok(
        status => 3,
        fase   => 'afhandelfase',
        node   => $zaaktype_node
    );

    ### Generate zaaktype_kenmerken, checkboxes
    my $zt_kenmerk = $zs->create_zaaktype_kenmerk_ok(
        status              => $zaaktype_status,
        bibliotheek_kenmerk => $zs->create_bibliotheek_kenmerk_ok(
            naam            => 'checkbox_' . $_,
            magic_string    => 'checkbox_' . $_,
            value_type      => 'checkbox',
            values          => [
                {
                    value   => $_,
                    active  => 1,
                }
            ]
        )
    ) for @{ BASIC_CHECKS() };

    ### Generate zaaktype_kenmerken, textboxes
    $zs->create_zaaktype_kenmerk_ok(
        status              => $zaaktype_status,
        bibliotheek_kenmerk => $zs->create_bibliotheek_kenmerk_ok(
            naam            => 'text_' . $_,
            magic_string    => 'text_' . $_,
            value_type      => 'text',
        )
    ) for @{ BASIC_CHECKS() };


    ### Now retrieve those kenmerken
    my @zaaktype_kenmerken = $zaaktype_node->zaaktype_kenmerken->search({},{prefetch => 'bibliotheek_kenmerken_id'})->all;

    ### And create basic rules
    for my $check (@{ BASIC_CHECKS() }) {
        my ($actie_kenmerk) = grep(
            {
                $_->bibliotheek_kenmerken_id->naam eq 'text_' . $check
            }
            @zaaktype_kenmerken
        );
        my ($cond_kenmerk) = grep(
            {
                $_->bibliotheek_kenmerken_id->naam eq 'checkbox_' . $check
            }
            @zaaktype_kenmerken
        );

        my %OLDMAP = (
            set_value   => 'vul_waarde_in',
            hide        => 'verberg_kenmerk',
            show        => 'toon_kenmerk',
        );

        $zs->create_zaaktype_regel_ok(
            status  => $zaaktype_status,
            # node => $zaaktype_node,
            naam    => 'Rule basic check: ' . $check . ':' . $cond_kenmerk->bibliotheek_kenmerken_id->naam . ' eq ' . $check,
            settings => {
                'voorwaarde_1_kenmerk'          => $cond_kenmerk->bibliotheek_kenmerken_id->id,
                'voorwaarde_1_value'            => $check,
                'voorwaarde_1_value_checkbox'   => '1',
                'voorwaarden'                   => '1',


                'actie_1'                       => $OLDMAP{$check},
                'actie_1_kenmerk'               => $actie_kenmerk->bibliotheek_kenmerken_id->id,
                'actie_1_value'                 => $check . ' Value',
                'acties'                        => '1',

                'naam'                          => 'Rule basic check: ' . $check,
            }
        );
    }

    return ($casetype, $zaaktype_status);
}


=head1 SEE ALSO

L<Zaaksysteem::Backend::Rules> L<Zaaksysteem::Manual>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


1;