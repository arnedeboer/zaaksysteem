package TestFor::General::Backend::Rules::Rule::Condition::RequestorType;
use base qw(Test::Class);

use Moose;
use TestSetup;
use File::Spec::Functions qw(catfile);

use Zaaksysteem::Backend::Rules;
use JSON qw/decode_json encode_json/;


=head1 NAME

TestFor::General::Backend::Rules::Rule::Condition::RequestorType - Rule condition: RequestorType

=head1 CODE TESTS

Code tests, testing the implementation itself

=head2 rules_rule_condition_requestortype_result

=cut

sub rules_rule_condition_requestortype_result : Tests {
    my $self            = shift;

    $zs->zs_transaction_ok(sub {
        my ($casetype, $zaaktype_status) = $self->_generate_rules_for_requestortype(type => 'none');

        my $case     = $zs->create_case_ok(zaaktype => $casetype);

        my $engine   = Zaaksysteem::Backend::Rules->new_from_case(
            $case,
            {
                'case.number_status'   => 1,
            }
        );

        is (@{ $engine->rules }, 1, 'Found one rule');

        is($engine->rules->[0]->conditions->[0]->attribute, 'case.requestor.subject_type', 'Correct attribute name: case.requestor.subject_type');
        ok($engine->rules->[0]->conditions->[0]->validates_true, 'ZS-4494: when no aanvrager is set in ztb, validate true');
        ok(!@{ $engine->rules->[0]->conditions->[0]->values }, 'Found empty values');
    }, 'condition::requestortype: checked requestortype: no values');

    $zs->zs_transaction_ok(sub {
        my ($casetype, $zaaktype_status) = $self->_generate_rules_for_requestortype(type => 'natuurlijk_persoon');

        my $case     = $zs->create_case_ok(zaaktype => $casetype);

        my $engine   = Zaaksysteem::Backend::Rules->new_from_case(
            $case,
            {
                'case.number_status'   => 1,
            }
        );

        is (@{ $engine->rules }, 1, 'Found one rule');

        is($engine->rules->[0]->conditions->[0]->attribute, 'case.requestor.subject_type', 'Correct attribute name: case.requestor.subject_type');
        is($engine->rules->[0]->conditions->[0]->values->[0], 'natuurlijk_persoon', 'Found natuurlijk_persoon as value');
    }, 'condition::requestortype: checked requestortype: natuurlijk_persoon');

    $zs->zs_transaction_ok(sub {
        my ($casetype, $zaaktype_status) = $self->_generate_rules_for_requestortype(type => 'bedrijf');

        my $case     = $zs->create_case_ok(zaaktype => $casetype);

        my $engine   = Zaaksysteem::Backend::Rules->new_from_case(
            $case,
            {
                'case.number_status'   => 1,
            }
        );

        is (@{ $engine->rules }, 1, 'Found one rule');

        is($engine->rules->[0]->conditions->[0]->attribute, 'case.requestor.subject_type', 'Correct attribute name: case.requestor.subject_type');
        is($engine->rules->[0]->conditions->[0]->values->[0], 'bedrijf', 'Found bedrijf as value');
    }, 'condition::requestortype: checked requestortype: bedrijf');
}

sub _generate_rules_for_requestortype {
    my $self            = shift;
    my (%opts)          = @_;

    my $zaaktype_node   = $zs->create_zaaktype_node_ok;
    my $casetype        = $zs->create_zaaktype_ok(node => $zaaktype_node);
    my $zaaktype_status = $zs->create_zaaktype_status_ok(
        status => 1,
        fase   => 'registratiefase',
        node   => $zaaktype_node
    );
    my $afhandelstatus  = $zs->create_zaaktype_status_ok(
        status => 3,
        fase   => 'afhandelfase',
        node   => $zaaktype_node
    );

    $zs->create_zaaktype_resultaat_ok(
        status      => $afhandelstatus,
        resultaat   => 'aangekocht',
    );

    $zs->create_zaaktype_resultaat_ok(
        status      => $afhandelstatus,
        resultaat   => 'verwerkt',
    );

    ### Generate zaaktype_kenmerken, checkboxes
    my $zt_kenmerk = $zs->create_zaaktype_kenmerk_ok(
        status              => $zaaktype_status,
        bibliotheek_kenmerk => $zs->create_bibliotheek_kenmerk_ok(
            naam            => 'richtext',
            magic_string    => 'richtext',
            value_type      => 'richtext',
        )
    );

    if (!$opts{type} || $opts{type} eq 'none') {
        $zs->create_zaaktype_regel_ok(
            status  => $zaaktype_status,
            # node => $zaaktype_node,
            naam    => 'Rule check: no voorwaarde',
            settings => {
                'voorwaarde_1_kenmerk'          => 'aanvrager',
                'voorwaarden'                   => '1',


                'actie_1'                       => 'vul_waarde_in',
                'actie_1_kenmerk'               => 'case_result',
                'actie_1_value'                 => '2',
                'acties'                        => '1',

                'naam'                          => 'Vul waarde in',
            }
        );
    } elsif ($opts{type} && $opts{type} eq 'natuurlijk_persoon') {
        $zs->create_zaaktype_regel_ok(
            status  => $zaaktype_status,
            # node => $zaaktype_node,
            naam    => 'Rule check: voorwaarde Natuurlijk persoon',
            settings => {
                'voorwaarde_1_kenmerk'          => 'aanvrager',
                'voorwaarde_1_value'            => ['Natuurlijk persoon'],
                'voorwaarden'                   => '1',


                'actie_1'                       => 'vul_waarde_in',
                'actie_1_kenmerk'               => 'case_result',
                'actie_1_value'                 => '2',
                'acties'                        => '1',

                'naam'                          => 'Vul waarde in',
            }
        );

    } elsif ($opts{type} && $opts{type} eq 'bedrijf') {
        $zs->create_zaaktype_regel_ok(
            status  => $zaaktype_status,
            # node => $zaaktype_node,
            naam    => 'Rule check: voorwaarde Bedrijf',
            settings => {
                'voorwaarde_1_kenmerk'          => 'aanvrager',
                'voorwaarde_1_value'            => ['Niet natuurlijk persoon'],
                'voorwaarden'                   => '1',


                'actie_1'                       => 'vul_waarde_in',
                'actie_1_kenmerk'               => 'case_result',
                'actie_1_value'                 => '2',
                'acties'                        => '1',

                'naam'                          => 'Vul waarde in',
            }
        );

    }

    return ($casetype, $zaaktype_status);
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
