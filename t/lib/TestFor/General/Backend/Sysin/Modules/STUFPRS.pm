package TestFor::General::Backend::Sysin::Modules::STUFPRS;
use base qw(Test::Class);

use Moose;
use TestSetup;
use File::Spec::Functions qw(catfile);

use constant STUF_PRS_COLUMNS => [qw/
    burgerservicenummer
    voornamen
    voorletters
    geslachtsnaam
    geslachtsaanduiding
    geboortedatum
    straatnaam
    huisnummer
    postcode
    woonplaats
    a_nummer
    voorvoegsel
    huisnummertoevoeging
    huisletter
    landcode
    gemeente_code
/];

use constant XML_PATH => 't/inc/API/StUF/0204/prs/';

=head1 NAME

TestFor::General::Backend::Sysin::Modules::STUFPRS - StUF Koppeling Natuurlijke personen (0204)

=head1 SYNOPSIS

    See USAGE tests

=head1 DESCRIPTION

These tests prove the interactions between the outside servicebus and our zaaksysteem, based
on StUF 0204 and entity PRS (Natuurlijk Persoon). The first sets of tests will demonstrate
the different uses invoked from zaaksysteem. Like searching for a person and importing one.

The second part of these tests are automatic tests, which prove the different scenario's
of the system integration module with the outside. Think of messages which move a person, decease
a person etc.

=head1 USAGE 

Usage tests, use these if you would like to know how to interact with the API, it's also useful
for extended documentation of the StUF API.

=head2 stuf_prs_usage_searching

Searching for a person.

=cut

sub stuf_prs_usage_searching : Tests {
    my $self            = shift;

    $zs->zs_transaction_ok(sub {
        my $interface       = $self->_create_prs_interface();

        my %search_opts     = (
            geslachtsnaam   => 'Jansen',
            geboortedatum   => '19550404',
            postcode        => '1234AA',
            huisnummer      => '44'
        );

        ### USAGE:
        ### Make sure you use at least a BSN or a combination of postcode, huisnummer, geboortedatum
        my $result          = $interface->process_trigger('search_for_single_result', \%search_opts);

        ### END USAGE;

        ok(($result && ref $result eq 'ARRAY' && @$result == 1), 'Got a single result');

        ### 423 defines the ID came from sleutelGegevensbeheer / 234 defines the id came from sleutelVerzendend
        ok($result->[0]->{external_id}, 'Got external transaction_id');
        like($result->[0]->{external_id}, qr/423\d+/, 'Valid transaction_id');

        ok($result->[0]->{ $_ }, 'Found data for column: ' . $_) for @{ STUF_PRS_COLUMNS() };

        is($result->[0]->{ $_ }, $search_opts{ $_ }, "Found correct data for column $_:" . $search_opts{ $_ }) for keys %search_opts;

    }, 'Trigger: "search_for_single_result" via Pink');

    $zs->zs_transaction_ok(sub {
        my $interface       = $self->_create_prs_interface();

        ### USAGE, searching for multiple entries...possible, but do not use, is not really a good idea.
        my $result          = $interface->process_trigger('search', { geslachtsnaam => 'Jansen' });

        ### END USAGE;

        ok(($result && ref $result eq 'ARRAY' && @$result == 10), 'Got a serie of 10 results');

        ### 423 defines the ID came from sleutelGegevensbeheer / 234 defines the id came from sleutelVerzendend
        ok($result->[0]->{external_id}, 'Got external transaction_id');
        like($result->[0]->{external_id}, qr/423\d+/, 'Valid transaction_id');

        ok($result->[0]->{ $_ }, 'Found data for column: ' . $_) for @{ STUF_PRS_COLUMNS() };
    }, 'Trigger: "search" via pink, multiple entries');

    $zs->zs_transaction_ok(sub {
        my $interface       = $self->_create_prs_interface;

        ### USAGE:
        my $result          = $interface->process_trigger('search_for_single_result', { via_gbav => 1, burgerservicenummer => '986565472'});

        ### END USAGE;

        ok(($result && ref $result eq 'ARRAY' && @$result == 1), 'Got a single result');
        ok($result->[0]->{burgerservicenummer}, 'Got external burgerservicenummer');
        is($result->[0]->{burgerservicenummer}, '986565472', 'Retrieved correct burgerservicenumber');
        like($result->[0]->{burgerservicenummer}, qr/\d+/, 'Valid burgerservicenummer');

        ok($result->[0]->{ $_ }, 'Found data for column: ' . $_) for @{ STUF_PRS_COLUMNS() };
    }, 'Validated searching of persons: binnengemeentelijk, via pink, via GBA-V');
}

=head2 stuf_prs_usage_disable_subscription

Removing of a object subscription (verwijderen van afnemerindicatie)

=cut

sub stuf_prs_usage_disable_subscription : Tests {
    my $self            = shift;

    $zs->zs_transaction_ok(sub {
        my $interface       = $self->_create_prs_interface();
        my $search_result   = $interface->process_trigger('search_complete_via_bsn', { burgerservicenummer => '887894321'});
        $interface->process_trigger('import', $search_result->[0]);

        ### USAGE:
        my $np              = $schema->resultset('NatuurlijkPersoon')->search(
            {burgerservicenummer => '887894321'}
        )->first;

        my $subscription    = $np->subscription_id;

        $interface->process_trigger('disable_subscription', { subscription_id => $subscription});

        ### END USAGE;

        $np                 = $schema->resultset('NatuurlijkPersoon')->search(
            {burgerservicenummer => '887894321'}
        )->first;
        $subscription       = $np->subscription_id;

        ok($np->deleted_on, 'disable_subscription: before: found person, deleted.');
        ok($np->adres_id->deleted_on, 'disable_subscription: before: found person, address deleted.');
        ok($subscription->date_deleted, 'disable_subscription: before: found subscription, deleted.');

    }, 'Trigger: "disable_subscription" via Pink');
}

=head2 stuf_prs_usage_search_and_import

Searching for a person, and when found, importing this person

=cut

sub stuf_prs_usage_search_and_import : Tests {
    my $self            = shift;

    $zs->zs_transaction_ok(sub {
        my $interface       = $self->_create_prs_interface;

        ### USAGE:
        my $search_result   = $interface->process_trigger('search', { geslachtsnaam => 'Jansen'});
        my $first_result    = $search_result->[0];

        my $result          = $interface->process_trigger('import', $first_result);

        ### END USAGE;

        ### check for active subscription
        my $object_subscription = $schema->resultset('ObjectSubscription')->search(
            {
                external_id     => $first_result->{external_id}
            }
        )->first;

        is($object_subscription->local_table, 'NatuurlijkPersoon', 'ObjectSubscription: found correct local_table');
        like($object_subscription->external_id, qr/^\d+$/, 'ObjectSubscription: found ID for external_id');
        like($object_subscription->local_id, qr/^\d+$/, 'ObjectSubscription: found ID for local_id');
        ok(!$object_subscription->date_deleted, 'ObjectSubscription: is active');

        my $np                  = $schema->resultset('NatuurlijkPersoon')->find($object_subscription->local_id);

        ### Has the correct values
        for my $column (@{ STUF_PRS_COLUMNS() }) {
            if ($np->can($column) && $np->$column) {
                my $val = $np->$column;
                if (UNIVERSAL::isa($np->$column, 'DateTime')) {
                    $val = $np->$column->ymd;
                    $val =~ s/-//g;
                }
                is($val, $first_result->{$column}, "Found correct imported value for $column: " . $np->$column);
            } elsif ($np->adres_id->can($column) && $np->adres_id->$column) {
                my $val = $np->adres_id->$column;
                if (UNIVERSAL::isa($np->adres_id->$column, 'DateTime')) {
                    $val = $np->adres_id->$column->ymd;
                    $val =~ s/-//g;
                }
                is($val, $first_result->{$column}, "Found correct imported value for $column: " . $np->adres_id->$column);
            }
        }
    }, 'Validated: search and import a single person');

    $zs->zs_transaction_ok(sub {
        my $interface       = $self->_create_prs_interface;

        ### USAGE:
        my $search_result   = $interface->process_trigger('search', { geslachtsnaam => 'Jansen'});
        my $first_result    = $search_result->[0];

        my $result          = $interface->process_trigger('import', $first_result);

        ### END USAGE;

        ### check for active subscription
        my $object_subscription = $schema->resultset('ObjectSubscription')->search(
            {
                external_id     => $first_result->{external_id}
            }
        )->first;

        is($object_subscription->local_table, 'NatuurlijkPersoon', 'ObjectSubscription: found correct local_table');
        like($object_subscription->external_id, qr/^\d+$/, 'ObjectSubscription: found ID for external_id');
        like($object_subscription->local_id, qr/^\d+$/, 'ObjectSubscription: found ID for local_id');
        ok(!$object_subscription->date_deleted, 'ObjectSubscription: is active');

        my $np                  = $schema->resultset('NatuurlijkPersoon')->find($object_subscription->local_id);

        ### Has the correct values
        for my $column (@{ STUF_PRS_COLUMNS() }) {
            if ($np->can($column) && $np->$column) {
                my $val = $np->$column;
                if (UNIVERSAL::isa($np->$column, 'DateTime')) {
                    $val = $np->$column->ymd;
                    $val =~ s/-//g;
                }
                is($val, $first_result->{$column}, "Found correct imported value for $column: " . $np->$column);
            } elsif ($np->adres_id->can($column) && $np->adres_id->$column) {
                my $val = $np->adres_id->$column;
                if (UNIVERSAL::isa($np->adres_id->$column, 'DateTime')) {
                    $val = $np->adres_id->$column->ymd;
                    $val =~ s/-//g;
                }
                is($val, $first_result->{$column}, "Found correct imported value for $column: " . $np->adres_id->$column);
            }
        }
    }, 'Validated: search and import a single person');

}

=head1 INTERNAL TESTS

=head2 stuf_prs_search_situations

A set of possible StUF scenarios. The following situations (and more) are tested

=over 4

=item search on BSN

=item check for "onderzoek" data

=item check for decease date

=item check if person is secret (geheim)

=item check if person has a correspondence address (briefadres)

=item check if geboortedatum is filled

=item check if searching on postalcode, housenumber is possible

=item check if we get a complete PRS set

=back

=cut


sub stuf_prs_search_situations : Tests {
    my $self                = shift;

    my $EVERYTHING  = {
        burgerservicenummer => '887654321',
        voornamen           => 'Frisia Wim',
        voorletters         => 'F.W.',
        geslachtsnaam       => 'Barend',
        geslachtsaanduiding => 'V',
        geboortedatum       => '19810403',
        straatnaam          => 'Teststraat',
        huisnummer          => '44',
        postcode            => '1042JL',
        woonplaats          => 'Amsterdam',

        a_nummer            => '6748772382',
        voorvoegsel         => 'de',
        huisnummertoevoeging => '1rec',
        huisletter          => 'B',
        landcode            => 6030,
        gemeente_code       => 1332,
    };

    $zs->zs_transaction_ok(sub {
        my $interface       = $self->_create_prs_interface;

        my $result;

        ok(
            ($result = $interface->process_trigger('search', { burgerservicenummer => '851345689'})),
            'Got results'
        );

        is($result->[0]->{onderzoek_persoon}, 1, 'Found onderzoek');

        ok(
            ($result = $interface->process_trigger('search', { burgerservicenummer => '852345689'})),
            'Got results'
        );

        is($result->[0]->{onderzoek_overlijden}, 1, 'Found onderzoek: Overlijden');

        ok(
            ($result = $interface->process_trigger('search', { burgerservicenummer => '854345689'})),
            'Got results'
        );

        is($result->[0]->{onderzoek_verblijfplaats}, 1, 'Found onderzoek: Adres');

        ok(
            ($result = $interface->process_trigger('search', { burgerservicenummer => '855345689'})),
            'Got results'
        );

        is($result->[0]->{onderzoek_huwelijk}, 1, 'Found onderzoek: Huwelijk');
    }, 'Validated getting person in "Onderzoek"');

    $zs->zs_transaction_ok(sub {
        my $interface       = $self->_create_prs_interface;

        my $result;
        ok(
            ($result = $interface->process_trigger('search', { burgerservicenummer => '832345689'})),
            'Got results'
        );

        is($result->[0]->{indicatie_geheim}, 1, 'Searched person: indicatie_geheim = 1');

    }, 'Validated getting person in "Geheim"');

    $zs->zs_transaction_ok(sub {
        my $interface       = $self->_create_prs_interface;

        my $result;
        ok(
            ($result = $interface->process_trigger('search', { burgerservicenummer => '842345689'})),
            'Got results'
        );

        is($result->[0]->{functie_adres}, 'B', 'Searched person: functie_adres = B');

        ### Do we have all address details
        ok($result->[0]->{$_}, 'Found value for: ' . $_) for qw/
            straatnaam
            woonplaats
            huisnummer
            postcode
        /;

    }, 'Validated getting person in "Briefadres"');

    $zs->zs_transaction_ok(sub {
        my $interface       = $self->_create_prs_interface;

        my $result;
        ok(
            ($result = $interface->process_trigger('search', { geboortedatum => '19830609'})),
            'Got results'
        );

        is($result->[0]->{geboortedatum}, '19830609', 'Found geboortedatum: 19830609');
    }, 'Validated searching of person through geboortedatum: ZS-3136');

    $zs->zs_transaction_ok(sub {
        my $interface       = $self->_create_prs_interface;

        my $result;
        ok(
            ($result = $interface->process_trigger('search', { postcode => '1051JL', huisnummer => 7})),
            'Got results'
        );

        is($result->[0]->{postcode}, '1051JL', 'Found postcode: 1051JL');
        is($result->[0]->{huisnummer}, '7', 'Found huisnummer: 7');

        my ($logline) = $schema->resultset('Logging')->search(
            {
                component   => 'transaction',
            },
            {
                order_by    => { -desc => 'id' }
            }
        );

        like($logline->onderwerp, qr/Via StUF.*gezocht.*\d+/, 'Found log entry for search');
    }, 'Validated searching of person through postcode/huisnummer: ZS-3137');

    $zs->zs_transaction_ok(sub {
        my $interface       = $self->_create_prs_interface;

        my $result;
        ok(
            ($result = $interface->process_trigger('search', $EVERYTHING)),
            'Got results'
        );

        for my $entry (keys %{ $EVERYTHING }) {
            is($result->[0]->{$entry}, $EVERYTHING->{$entry}, 'Found value for: ' . $entry);
        }
    }, 'Validated searching of person through everything ;)');

}

=head2 stuf_prs_import_natuurlijk_persoon

Check the importing of a person

=cut

sub stuf_prs_import_natuurlijk_persoon : Tests {
    my $self            = shift;

    $zs->zs_transaction_ok(sub {
        my $interface       = $self->_create_prs_interface;

        my $search_result;
        ok(
            ($search_result = $interface->process_trigger('search', { burgerservicenummer => '887654321'})),
            'Got results'
        );

        ### Import first transaction
        my $first = $search_result->[0];

        my $result;
        ok(
            ($result = $interface->process_trigger('import', $first)),
            'Got entry'
        );

        ### Check last transactions
        my ($kennisgeving, $zs_import) = $schema->resultset('Transaction')->search(
            {},
            {
                order_by    => { '-desc' => 'me.id' },
                rows        => 1,
            }
        );

        is($kennisgeving->success_count, 1, 'SOAP Call Kennisgeving: No errors');
        is($kennisgeving->processed, 1, 'SOAP Call Kennisgeving: Transaction sucessfully processed');


        my $kennisgeving_record = $kennisgeving->transaction_records->first;
        like($kennisgeving_record->input, qr/indicatorOvername>V/, 'Request: Found indicatorOvername: V');
        like($kennisgeving_record->output, qr/indicatorOvername>V/, 'Response: Found indicatorOvername: V');
        is($kennisgeving_record->preview_string, $first->{voornamen} . ' ' . $first->{voorvoegsel} . ' ' . $first->{geslachtsnaam}, 'Found preview_string: ' . $first->{voornamen} . ' ' . $first->{geslachtsnaam});

        is($kennisgeving->direction, 'outgoing', 'Transaction type is outgoing for StUF message to SOAP SBUS');

        ### Check object subscription
        my ($object_subscription)   = $schema->resultset('ObjectSubscription')->search(
            {}, { order_by => {'-desc' => 'me.id' }}
        );

        is($object_subscription->local_table, 'NatuurlijkPersoon', 'ObjectSubscription: found correct local_table');
        like($object_subscription->external_id, qr/^\d+$/, 'ObjectSubscription: found ID for external_id');
        like($object_subscription->local_id, qr/^\d+$/, 'ObjectSubscription: found ID for local_id');

    }, 'Validated import of person');

    $zs->zs_transaction_ok(sub {
        my $interface       = $self->_create_prs_interface;

        my $search_result;
        ok(
            (
                $search_result = $interface->process_trigger(
                    'search', {
                        burgerservicenummer => '987654321',
                        via_gbav            => 1,
                    }
                )
            ),
            'Got results'
        );

        my ($logline) = $schema->resultset('Logging')->search(
            {
                component   => 'transaction',
            },
            {
                order_by    => { -desc => 'id' }
            }
        );

        like($logline->onderwerp, qr/Via StUF.*gezocht.*987654321/, 'Found log entry for search');;

        ### Import first transaction
        my $first = $search_result->[0];

        my $result;
        ok(
            ($result = $interface->process_trigger('import', $first)),
            'Got entry from GBA-V'
        );

        ($logline) = $schema->resultset('Logging')->search(
            {
                component   => 'transaction',
            },
            {
                order_by    => { -desc => 'id' }
            }
        );

        like($logline->onderwerp, qr/Via StUF geimporteerd.*987654321/, 'Found log entry for import');;

        ### Check object subscription
        my ($object_subscription)   = $schema->resultset('ObjectSubscription')->search(
            {}, { order_by => {'-desc' => 'me.id' }}
        );

        is($object_subscription->local_table, 'NatuurlijkPersoon', 'ObjectSubscription: found correct local_table');
        like($object_subscription->external_id,  qr/^423\d+$/, 'ObjectSubscription: found ID for external_id');
        like($object_subscription->local_id, qr/^\d+$/, 'ObjectSubscription: found ID for local_id');

    }, 'Validated import of person via GBA-V');
}

=head2 stuf_prs_complete_check_xmls

Does some checks on XML which get pomped around, to see if the necessary variables are
found

=cut

sub stuf_prs_complete_check_xmls : Tests {
    my $self        = shift;

    $zs->zs_transaction_ok(sub {
        my $interface       = $self->_create_prs_interface;

        ### Find last transaction
        my $higher_than     = 0;
        my $transactions    = $schema->resultset('Transaction')->search(
            {},
            {
                order_by    => {'-desc' => 'id'}
            }
        );

        if ($transactions->count) {
            $higher_than    = $transactions->first->id;
        }

        my $search_result   = $interface->process_trigger('search', { burgerservicenummer => '887654321'});
        my $result          = $interface->process_trigger('import', $search_result->[0]),

        ### Check last transactions
        my (@transactions)  = $schema->resultset('Transaction')->search(
            {
                id  => { '>' => $higher_than }
            },
        );

        is($transactions->count, 2, 'Found 2 transactions for simple "vraagbericht" and "kennisgevingsbericht"');

        my $external_id;
        for my $transaction (@transactions) {
            my @records     = $transaction->transaction_records->search;
            my $record  = shift @records;

            if ($transaction->processor_params->{processor} eq '_process_search_natuurlijkpersoon') {
                ($external_id)  = $record->output =~ /sleutelGegevensbeheer="(\d+)"/g;
            } elsif ($transaction->processor_params->{processor} eq '_process_import_natuurlijkpersoon') {
                my ($import_id)   = $record->input =~ /sleutelGegevensbeheer="(\d+)"/g;

                ok($external_id, 'Question, found external ID in search');
                ok($import_id, 'Question, found external ID in import');
                is($external_id, $import_id, 'When importing, use same "key" as we got from the search');
                unlike($record->input, qr/bsn-nummer/, 'Import message is simple form');
                like($record->input, qr/sleutelVerzendend="\d+"/, 'Import message has sleutelVerzendend');
                like($record->input, qr/indicatorOvername>V/, 'Indicator Overname gezet');

                like($record->input, qr/applicatie>CMODIS/, 'Found CMODIS application');
            }
        }
    }, 'XML tests for search and import: binnengemeentelijk');

    $zs->zs_transaction_ok(sub {
        my $interface       = $self->_create_prs_interface;

        ### Find last transaction
        my $higher_than     = 0;
        my $transactions    = $schema->resultset('Transaction')->search(
            {},
            {
                order_by    => {'-desc' => 'id'}
            }
        );

        if ($transactions->count) {
            $higher_than    = $transactions->first->id;
        }

        my $search_result   = $interface->process_trigger('search', { via_gbav => 1,burgerservicenummer => '987654321'});
        my $result          = $interface->process_trigger('import', $search_result->[0]),

        ### Check last transactions
        my (@transactions)  = $schema->resultset('Transaction')->search(
            {
                id  => { '>' => $higher_than }
            },
        );

        is($transactions->count, 4, 'Found 2 transactions for simple "vraagbericht" and "kennisgevingsbericht"');

        my $external_id;
        for my $transaction (@transactions) {
            my @records     = $transaction->transaction_records->search;
            my $record  = shift @records;

            if ($transaction->processor_params && $transaction->processor_params->{processor} eq '_process_search_natuurlijkpersoon') {
                ($external_id)  = $record->output =~ /sleutelGegevensbeheer="(\d+)"/g;
            } elsif ($transaction->processor_params && $transaction->processor_params->{processor} eq '_process_import_natuurlijkpersoon') {
                my ($import_id)   = $record->input =~ /sleutelGegevensbeheer="(\d+)"/g;

                ok(!$external_id, 'GBA-V question, no external ID available in search');
                ok(!$import_id, 'GBA-V question, no external ID available in import');
                like($record->input, qr/bsn-nummer/, 'Import message is simple form');
                like($record->input, qr/sleutelVerzendend="\d+"/, 'Import message has sleutelVerzendend');
                like($record->input, qr/indicatorOvername>V/, 'Indicator Overname gezet');

                like($record->input, qr/applicatie>CMODIS/, 'Found CMODIS application');
            }
        }
    }, 'XML tests for search and import: buitengemeentelijk');
}

=head2 stuf_prs_search_for_single_result

Search for a single result, by using BSN or a combination of postcode, huisnummer, geboortedatum

=cut

sub stuf_prs_search_for_single_result : Tests {
    my $self        = shift;

    $zs->zs_transaction_ok(sub {
        my $interface       = $self->_create_prs_interface();

        throws_ok(
            sub {
                $interface->process_trigger('search_for_single_result', { external_id => '34254321'});
            },
            qr/Validation of profile failed/,
            'Throws correctly on missing combintation of bsn or gebdatum,postcode,huisnummer'
        );

        my $search_result   = $interface->process_trigger('search_for_single_result', { burgerservicenummer => '887654321'});
        ok(($search_result && ref $search_result eq 'ARRAY' && @$search_result == 1), 'Got a single result');

        is($search_result->[0]->{burgerservicenummer}, '887654321', 'Found entry with correct BSN');
    }, 'Trigger: search_for_single_result');
}

=head2 stuf_prs_disable_subscription

Disables a subscription for a person

=cut

sub stuf_prs_disable_subscription : Tests {
    my $self            = shift;

    $zs->zs_transaction_ok(sub {
        my $interface       = $self->_create_prs_interface();
        my $search_result   = $interface->process_trigger('search_complete_via_bsn', { burgerservicenummer => '887894321'});
        $interface->process_trigger('import', $search_result->[0]);

        my $np              = $schema->resultset('NatuurlijkPersoon')->search(
            {burgerservicenummer => '887894321'}
        )->first;

        my $subscription    = $np->subscription_id;
        ok($subscription, 'Found subscription for user');

        ok(!$np->deleted_on, 'disable_subscription: before: found person, active.');
        ok(!$np->adres_id->deleted_on, 'disable_subscription: before: found person, address active.');
        ok(!$subscription->date_deleted, 'disable_subscription: before: found subscription, active.');

        $interface->process_trigger('disable_subscription', { subscription_id => $subscription});

        $np                 = $schema->resultset('NatuurlijkPersoon')->search(
            {burgerservicenummer => '887894321'}
        )->first;
        $subscription       = $np->subscription_id;

        ok($np->deleted_on, 'disable_subscription: before: found person, deleted.');
        ok($np->adres_id->deleted_on, 'disable_subscription: before: found person, address deleted.');
        ok($subscription->date_deleted, 'disable_subscription: before: found subscription, deleted.');

    }, 'Disable subscription');
}

sub stuf_prs_error_handling : Tests {
    my $self            = shift;

    $zs->zs_transaction_ok(sub {
        my $interface       = $self->_create_prs_interface();;

        my $transaction = $interface->process({
            input_data              => $zs->get_file_contents_as_string(
                                        catfile(XML_PATH, '121-prs-update-tinus.xml')
                                    ),
        });

        like($transaction->records->first->output, qr/no_entry_found/, 'Missing subscription');
    }, 'Tested: PRS Update Mutation, missing object subscription');


}

=head1 CHAIN TESTS

The following tests are so called "ketentests", which test a complete flow of messages on our StUF interface
proving different situations.

These tests will use XML messages almost identical to the real world, to prove it can process these
XML messages and process them in zaaksysteem.

=cut

=head2 stuf_prs_chain_create_new_person

Creates a new person from XML, and checks the data

B<Situations>

=over 4

=item create a new person

=item create a new person in other country

=back

=cut

sub stuf_prs_chain_create_new_person : Tests {
    my $self            = shift;

    my $VALIDATION_MAP = {
        'NatuurlijkPersoon'     => {
            'a_nummer'                  => '1234567890',
            'burgerservicenummer'       => '987654321',
            'voornamen'                 => 'Tinus',
            'voorletters'               => 'T',
            'geslachtsnaam'             => 'Testpersoon',
            'geslachtsaanduiding'       => 'M',
        },
        'Adres'                 => {
            'postcode'                  => '1015JL',
            'woonplaats'                => 'Amsterdam',
            'straatnaam'                => 'Donker Curtiusstraat',
            'huisnummer'                => '7',
            'huisnummertoevoeging'      => '521',
        }
    };

    $zs->zs_transaction_ok(sub {
        my $interface   = $self->_create_prs_interface;

        ### Last created object_subscription
        my $transaction = $interface->process({
            input_data => $zs->get_file_contents_as_string(
                catfile(XML_PATH, '101-prs-create-tinus.xml')
            ),
        });

        ok($transaction, 'PRS Transaction completed');

        ### Success?
        ok(!$transaction->error_count, 'PRS Transaction: no errors');
        is($transaction->success_count, 1, 'PRS Transaction: 1 success');
        is($transaction->external_transaction_id, 'MK0000229154', 'PRS Transaction: external transaction id');

        ### Retry values
        is($transaction->automated_retry_count, undef, 'PRS Transaction: automated retry count');
        ok($transaction->date_last_retry, 'PRS Transaction: date last retry');
        ok(!$transaction->date_next_retry, 'PRS Transaction: no date next_retry');

        ### Dates
        ok($transaction->date_created, 'PRS Transaction: date created');
        ok(!$transaction->date_deleted, 'PRS Transaction: no date deleted');


        is(scalar(@{ $transaction->preview_data }), 1, "Found preview string");
        is($transaction->preview_data->[0]->{preview_string}, 'Tinus Testpersoon', "Found preview string");
        ok($transaction->preview_data->[0]->{ $_ }, "Found $_ in preview_data") for qw/id preview_string transaction_id/;
    }, '--- Tested basic transaction data');


    $zs->zs_transaction_ok(sub {
        my $interface   = $self->_create_prs_interface;

        ### Last created object_subscription
        my $transaction = $interface->process({
            input_data => $zs->get_file_contents_as_string(
                catfile(XML_PATH, '101-prs-create-tinus.xml')
            ),
        });

        ### Check record
        is($transaction->records->count, 1, 'Got single transaction record');
        my $record      = $transaction->records->first;
        is($record->transaction_record_to_objects, 2, 'Got two mutations');
    }, '--- Tested transaction->record data');

    $zs->zs_transaction_ok(sub {
        my $interface   = $self->_create_prs_interface;

        ### Last created object_subscription
        my $transaction = $interface->process({
            input_data => $zs->get_file_contents_as_string(
                catfile(XML_PATH, '101-prs-create-tinus.xml')
            ),
        });

        my $record      = $transaction->records->first;


        ### Check person
        my $npms        = $record->transaction_record_to_objects->search(
            {
                'local_table'           => 'NatuurlijkPersoon',
                'transaction_record_id' => $record->id,
            }
        );
        is($npms->count, 1, 'Got single NP mutation record');

        my $npm         = $npms->first;

        my $np          = $schema->resultset('NatuurlijkPersoon')->find(
            $npm->local_id
        );

        ok($np->authenticated, 'Record is authenticated');
        is($np->authenticatedby, 'gba', 'Record is authenticated by GBA');

        is(
            $schema->resultset('ObjectSubscription')->search(
                {
                    'local_table'   => 'NatuurlijkPersoon',
                    'local_id'      => $np->id,
                }
            )->count,
            1,
            'Got single subscription'
        );

        for my $key (keys %{ $VALIDATION_MAP->{NatuurlijkPersoon} }) {
            my $givenvalue = $np->$key;
            my $wantedvalue = $VALIDATION_MAP->{NatuurlijkPersoon}->{ $key };

            is($givenvalue, $wantedvalue, 'Correct value for: ' . $key);
        }

        for my $key (keys %{ $VALIDATION_MAP->{Adres} }) {
            my $givenvalue = $np->adres_id->$key;
            my $wantedvalue = $VALIDATION_MAP->{Adres}->{ $key };

            is($givenvalue, $wantedvalue, 'Correct value for: ' . $key);
        }

        ok($np->in_gemeente, 'in_gemeente: true - binnen gemeentelijk persoon');
        is($np->adres_id->landcode, 6030, 'Got correct landcode for person');

    }, '--- Tested created Natuurlijk Persoon');

    $zs->zs_transaction_ok(sub {
        my $interface   = $self->_create_prs_interface;

        ### Last created object_subscription
        my $transaction = $interface->process({
            input_data              => $zs->get_file_contents_as_string(
                                        catfile(XML_PATH, '102-prs-create-huwelijk.xml')
                                    ),
        });

        ok($transaction, 'PRS Transaction completed');
        ok(!$transaction->error_count, 'PRS Transaction: no errors');
        is($transaction->success_count, 1, 'PRS Transaction: 1 success');

        my $record      = $transaction->records->first;

        my $npms        = $record->transaction_record_to_objects->search(
            {
                'local_table'           => 'NatuurlijkPersoon',
                'transaction_record_id' => $record->id,
            }
        );

        is($npms->count, 1, 'Got single NP mutation record');

        my $npm         = $npms->first;

        my $np          = $schema->resultset('NatuurlijkPersoon')->find(
            $npm->local_id
        );

        is($np->partner_geslachtsnaam, 'TestpartnernaamGOOD', 'Found correct partner');
        is($np->aanduiding_naamgebruik, 'P', 'Found correct naamgebruik');
    }, '--- Tested created Natuurlijk Persoon with a partner');

    $zs->zs_transaction_ok(sub {
        my $interface   = $self->_create_prs_interface;

        ### Last created object_subscription
        my $transaction = $interface->process({
            input_data => $zs->get_file_contents_as_string(
                catfile(XML_PATH, '105-prs-create-foreigner.xml')
            ),
        });

        my $record      = $transaction->records->first;


        ### Check person
        my $npms        = $record->transaction_record_to_objects->search(
            {
                'local_table'           => 'NatuurlijkPersoon',
                'transaction_record_id' => $record->id,
            }
        );
        is($npms->count, 1, 'Got single NP mutation record');

        my $npm         = $npms->first;

        my $np          = $schema->resultset('NatuurlijkPersoon')->find(
            $npm->local_id
        );

        ok($np->authenticated, 'Record is authenticated');
        is($np->authenticatedby, 'gba', 'Record is authenticated by GBA');

        ok(!$np->in_gemeente, 'in_gemeente: false - buiten gemeentelijk persoon');

        is($np->adres_id->landcode, 5010, 'Got correct landcode for foreigner');
        is($np->adres_id->adres_buitenland1, '2e Carabinierslaan 19', 'Found adres_buitenland1');
        is($np->adres_id->adres_buitenland2, 'Foreigncity', 'Found adres_buitenland2');
        is($np->adres_id->adres_buitenland3, 'Foreigntown', 'Found adres_buitenland3');

    }, '--- Tested created Natuurlijk Persoon: buitenland');
}

=head2 stuf_prs_chain_update_person

Create a new person, and mutates it in different situations

B<Situations>

=over 4

=item create a new person, and mutate it

=back

=cut

sub stuf_prs_chain_update_person : Tests {
    my $self            = shift;

    $zs->zs_transaction_ok(sub {
        my $interface   = $self->_create_prs_interface;

        my $VALIDATION_MAP = {
            'NatuurlijkPersoon'     => {
                'a_nummer'                  => '1234567890',
                'burgerservicenummer'       => '987654321',
                'voornamen'                 => 'Minus',
                'voorletters'               => 'M',
                'geslachtsnaam'             => 'Mestpersoon',
                'geslachtsaanduiding'       => 'M',
            },
            'Adres'                 => {
                'postcode'                  => '1015JL',
                'woonplaats'            => 'Amsterdam',
                'straatnaam'                => 'Donker Curtiusstraat',
                'huisnummer'                => '70',
                'huisnummertoevoeging'      => undef,
            }
        };

        ###
        ### CREATE FIRST ENTRY
        ###
        my $transaction = $interface->process({
            input_data              => $zs->get_file_contents_as_string(
                                        catfile(XML_PATH, '101-prs-create-tinus.xml')
                                    ),
        });


        ###
        ### MUTATE CREATED ENTRY
        ###

        my $second_transaction = $interface->process({
            input_data              => $zs->get_file_contents_as_string(
                                        catfile(XML_PATH, '121-prs-update-tinus.xml')
                                    ),
        });


        my $record     = $second_transaction->records->first;

        my $mutation   = $record->transaction_record_to_objects->search(
            {
                'local_table' => 'NatuurlijkPersoon',
                'transaction_record_id' => $record->id,
            }
        )->first;

        ok($mutation, 'Found NatuurlijkPersoon mutation');

        my $np          = $schema->resultset('NatuurlijkPersoon')->find(
            $mutation->local_id
        );

        ok(!$second_transaction->error_count, 'Succesfully mutated PRS entry');

        for my $key (keys %{ $VALIDATION_MAP->{NatuurlijkPersoon} }) {
            my $givenvalue = $np->$key;
            my $wantedvalue = $VALIDATION_MAP->{NatuurlijkPersoon}->{ $key };

            is($givenvalue, $wantedvalue, 'Correct value for: ' . $key);
        }

        for my $key (keys %{ $VALIDATION_MAP->{Adres} }) {
            my $givenvalue = $np->adres_id->$key;
            my $wantedvalue = $VALIDATION_MAP->{Adres}->{ $key };

            is($givenvalue, $wantedvalue, 'Correct value for: ' . $key);
        }

    }, '--- Tested changing of Tinus Testpersoon to Minus Mestpersoon');

    $zs->zs_transaction_ok(sub {
        my $interface   = $self->_create_prs_interface;

        ### Last created object_subscription
        my $first_transaction = $interface->process({
            input_data  => $zs->get_file_contents_as_string(
                catfile(XML_PATH, '101-prs-create-tinus.xml')
            ),
        });

        ok(!$first_transaction->error_count, 'PRS Transaction: no errors');
        is($first_transaction->success_count, 1, 'PRS Transaction: 1 success');
        is($first_transaction->records->count, 1, 'Got single transaction record');

        my $local        = $first_transaction->records->first->transaction_record_to_objects->search(
            {
                'local_table'           => 'NatuurlijkPersoon',
                'transaction_record_id' => $first_transaction->records->first->id,
            }
        )->first;
        my $local_np          = $schema->resultset('NatuurlijkPersoon')->find(
            $local->local_id
        );

        ok($local_np->in_gemeente, 'Tinus lives in gemeente');
        is($local_np->adres_id->landcode, 6030, 'Tinus lives in this country');

        my $transaction = $interface->process({
            input_data              => $zs->get_file_contents_as_string(
                                        catfile(XML_PATH, '125-prs-update-tinus_to_foreigner.xml')
                                    ),
        });

        my $record      = $transaction->records->first;
        is($record->transaction_record_to_objects, 2, 'Got two mutations');

        my $npm        = $record->transaction_record_to_objects->search(
            {
                'local_table'           => 'NatuurlijkPersoon',
                'transaction_record_id' => $record->id,
            }
        )->first;
        my $np          = $schema->resultset('NatuurlijkPersoon')->find(
            $npm->local_id
        );

        ok($np->authenticated, 'Record is authenticated');
        is($np->authenticatedby, 'gba', 'Record is authenticated by GBA');

        is(
            $schema->resultset('ObjectSubscription')->search(
                {
                    'local_table'   => 'NatuurlijkPersoon',
                    'local_id'      => $np->id,
                }
            )->count,
            1,
            'Got single subscription'
        );

        is($np->adres_id->landcode, 5010, 'Got correct landcode for foreigner');

        ok($local_np->in_gemeente, 'Tinus moved outside gemeente');
        is($local_np->adres_id->landcode, 6030, 'Tinus lives in this country');

        is($np->adres_id->adres_buitenland1, '2e Carabinierslaan 19', 'Found adres_buitenland1');
        is($np->adres_id->adres_buitenland2, 'Foreigncity', 'Found adres_buitenland2');
        is($np->adres_id->adres_buitenland3, 'Foreigntown', 'Found adres_buitenland3');

    }, '--- Tested PRS Move tinus to foreigner (emigrate) mutation');

    ### TODO, FIX THIS ONE
    $zs->zs_transaction_ok(sub {
        my $interface   = $self->_create_prs_interface;

        ###
        ### CREATE FIRST ENTRY
        ###
        my $transaction = $interface->process({
            input_data              => $zs->get_file_contents_as_string(
                                        catfile(XML_PATH, '101-prs-create-tinus.xml')
                                    ),
        });


        ###
        ### MOVE CREATED ENTRY
        ###

        my $second_transaction = $interface->process({
            input_data              => $zs->get_file_contents_as_string(
                                        catfile(XML_PATH, '142-prs-verhuis-tinus.xml')
                                    ),
        });

        my $record     = $second_transaction->records->first;

        my $mutation   = $record->transaction_record_to_objects->search(
            {
                'local_table' => 'NatuurlijkPersoon',
                'transaction_record_id' => $record->id,
            }
        )->first;

        ok($mutation, 'Found NatuurlijkPersoon mutation');

        my $np          = $schema->resultset('NatuurlijkPersoon')->find(
            $mutation->local_id
        );

        ok(!$np->in_gemeente, 'in_gemeente: false - buitengemeentelijk persoon');

        #ok($np->datum_overlijden, 'Got overlijdensdate: ' . $np->datum_overlijden);


        ### Process last transaction
        my $subscription_delete = $schema->resultset('Transaction')->search(
            {},{order_by => { '-desc' => 'id' } }
        )->first;

        #$subscription_delete->process;
        #note(explain({ $subscription_delete->get_columns }));

    }, '--- Tested Move (verhuis) person');
}

=head2 stuf_prs_chain_delete_person

Delete a PRS entry, in different situations

B<Situations>

=over 4

=item create a new person, and decease it

=back

=cut

sub stuf_prs_chain_delete_person : Tests {
    my $self            = shift;

    $zs->zs_transaction_ok(sub {
        my $interface   = $self->_create_prs_interface;

        ###
        ### CREATE FIRST ENTRY
        ###
        my $transaction = $interface->process({
            input_data              => $zs->get_file_contents_as_string(
                                        catfile(XML_PATH, '101-prs-create-tinus.xml')
                                    ),
        });

        my $record     = $transaction->records->first;

        is(
            $schema->resultset('ObjectSubscription')->search_active->count,
            1,
            'Single object subscription'
        );

        my $mutation   = $record->transaction_record_to_objects->search(
            {
                'local_table' => 'NatuurlijkPersoon',
                'transaction_record_id' => $record->id,
            }
        )->first;

        ok($mutation, 'Found NatuurlijkPersoon mutation');

        ###
        ### DELETE CREATED ENTRY
        ###
        my $second_transaction = $interface->process({
            input_data              => $zs->get_file_contents_as_string(
                                        catfile(XML_PATH, '131-prs-delete-tinus.xml')
                                    ),
        });

        my $np          = $schema->resultset('NatuurlijkPersoon')->find(
            $mutation->local_id
        );

        ok(!$second_transaction->error_count, 'Succesfully deleted PRS entry');
        ok($np->deleted_on, 'NatuurlijkPersoon is deleted');
        ok($np->adres_id->deleted_on, 'NatuurlijkPersoon->Adres is deleted');

        ok(
            !$schema->resultset('ObjectSubscription')->search_active->count,
            'Object subscription removed'
        );
    }, '--- Tested delete PRS');

    

    $zs->zs_transaction_ok(sub {
        my $interface   = $self->_create_prs_interface;

        ###
        ### CREATE FIRST ENTRY
        ###
        my $transaction = $interface->process({
            input_data              => $zs->get_file_contents_as_string(
                                        catfile(XML_PATH, '101-prs-create-tinus.xml')
                                    ),
        });

        my $record     = $transaction->records->first;

        is(
            $schema->resultset('ObjectSubscription')->search_active->count,
            1,
            'Single object subscription'
        );

        my $mutation   = $record->transaction_record_to_objects->search(
            {
                'local_table' => 'NatuurlijkPersoon',
                'transaction_record_id' => $record->id,
            }
        )->first;

        ok($mutation, 'Found NatuurlijkPersoon mutation');

        ###
        ### DELETE CREATED ENTRY
        ###
        my $second_transaction = $interface->process({
            input_data              => $zs->get_file_contents_as_string(
                                        catfile(XML_PATH, '131-prs-delete-tinus.xml')
                                    ),
        });

        my $np          = $schema->resultset('NatuurlijkPersoon')->find(
            $mutation->local_id
        );

        ok(!$second_transaction->error_count, 'Succesfully deleted PRS entry');
        ok($np->deleted_on, 'NatuurlijkPersoon is deleted');
        ok($np->adres_id->deleted_on, 'NatuurlijkPersoon->Adres is deleted');

        ok(
            !$schema->resultset('ObjectSubscription')->search_active->count,
            'Object subscription removed'
        );

        ### Let's recreate

        $transaction = $interface->process({
            input_data              => $zs->get_file_contents_as_string(
                                        catfile(XML_PATH, '101-prs-create-tinus.xml')
                                    ),
        });

        $record     = $transaction->records->first;


        $mutation   = $record->transaction_record_to_objects->search(
            {
                'local_table' => 'NatuurlijkPersoon',
                'transaction_record_id' => $record->id,
            }
        )->first;

        $np          = $schema->resultset('NatuurlijkPersoon')->find(
            $mutation->local_id
        );

        ok(!$np->deleted_on, 'NatuurlijkPersoon is undeleted');
        ok(!$np->adres_id->deleted_on, 'NatuurlijkPersoon->Adres is undeleted');
        ok(!$np->subscription_id->date_deleted, 'Object subscription is active');


    }, 'Tested: PRS Delete and recreate');

    $zs->zs_transaction_ok(sub {
        my $interface   = $self->_create_prs_interface;

        ###
        ### CREATE FIRST ENTRY
        ###
        my $transaction = $interface->process({
            input_data              => $zs->get_file_contents_as_string(
                                        catfile(XML_PATH, '101-prs-create-tinus.xml')
                                    ),
        });


        ###
        ### DECEASE CREATED ENTRY
        ###

        my $second_transaction = $interface->process({
            input_data              => $zs->get_file_contents_as_string(
                                        catfile(XML_PATH, '141-prs-decease-tinus.xml')
                                    ),
        });

        my $record     = $transaction->records->first;

        my $mutation   = $record->transaction_record_to_objects->search(
            {
                'local_table' => 'NatuurlijkPersoon',
                'transaction_record_id' => $record->id,
            }
        )->first;

        ok($mutation, 'Found NatuurlijkPersoon mutation');

        my $np          = $schema->resultset('NatuurlijkPersoon')->find(
            $mutation->local_id
        );

        ok($np->deleted_on, 'Person deleted');
        ok($np->datum_overlijden, 'Got overlijdensdate: ' . $np->datum_overlijden);


        ### Process last transaction
        my $subscription_delete = $schema->resultset('Transaction')->search(
            {},{order_by => { '-desc' => 'id' } }
        )->first;

        #$subscription_delete->process;
        #note(explain({ $subscription_delete->get_columns }));

    }, '--- Tested Decease person');
}


=head2 stuf_prs_chain_disable_subscription

Delete a PRS entry, in different situations

B<Situations>

=over 4

=item create a new person, and decease it

=back

=cut

sub stuf_prs_chain_disable_subscription : Tests {
    my $self            = shift;

    $zs->zs_transaction_ok(sub {
        my $interface   = $self->_create_prs_interface;

        my $inserted_transaction = $interface->process({
            input_data              => $zs->get_file_contents_as_string(
                                        catfile(XML_PATH, '101-prs-create-tinus.xml')
                                    ),
        });

        my $transaction = $interface->process_trigger(
            'disable_subscription',
            {
                subscription_id => $interface->object_subscriptions->first->id,
            }
        );

        my $now         = DateTime->now();

        ok(!$transaction->error_count, 'No errors in disabling subscription');

        my ($record) = $transaction->transaction_records->search();

        like($record->input, qr/mutatiesoort>V/, 'Found mutatiesoort V (removal) in message');
        like($record->input, qr/sleutelGegevensbeheer="/, 'Found sleutelgegevensbeheer in message');
        my $sleutel = $interface->object_subscriptions->first->external_id;
        like($record->input, qr/sleutelGegevensbeheer="$sleutel"/, 'Found correct sleutel in message');
    }, 'Tested: PRS remove afnemerindicatie');
}

=head1 REGRESSION PROTECTION

Bugreports need to be fixed in code, and idealiter, if possible, need to be provided with a regression test.
Making sure this bug does not happen again in the future.

=head2 stuf_prs_bugs_search

=over 4

=item ZS-4961: 8-digit BSNs need to work

=back

=cut

sub stuf_prs_bugs_search : Tests {
    my $self            = shift;

    $zs->zs_transaction_ok(sub {
        my $interface       = $self->_create_prs_interface();

        my %search_opts     = (
            geslachtsnaam   => 'Jansen',
            geboortedatum   => '19550404',
            postcode        => '1234AA',
            huisnummer      => '44'
        );

        ### USAGE:
        ### Make sure you use at least a BSN or a combination of postcode, huisnummer, geboortedatum
        my $result          = $interface->process_trigger('search_for_single_result', { burgerservicenummer => 12345678 });

        ### END USAGE;

        ok(($result && ref $result eq 'ARRAY' && @$result == 1), 'Got a single result');
        is($result->[0]->{burgerservicenummer}, 12345678, 'ZS-4961 Found 8 digit BSN');

    }, '--- Tested ZS-4961: 8-digit BSNs need to work');
}



=head1 TODO

Move below to TestFor::STUFCONFIG

=head2 stuf_prs_get_natuurlijkpersoon_interface

=cut

# sub stuf_prs_get_natuurlijkpersoon_interface : Tests {
#     my $self            = shift;

#     $zs->zs_transaction_ok(sub {
#         my $stufconfig          = $self->_create_config_interface(
#             {
#                 stuf_supplier       => 'pink',
#             }
#         );
#         my $interface           = $zs->create_named_interface_ok(
#             {
#                 module              => 'stufprs',
#                 name                => 'STUF PRS Parsing',
#                 active              => 1,
#             },
#         );

#         ok($stufconfig, 'Found config interface');

#         my $stufconfig_module = $stufconfig->module_object;

#         ok($stufconfig_module, 'found stufconfig module');

#         throws_ok(
#             sub {
#                 $stufconfig_module->get_natuurlijkpersoon_interface($interface)
#             },
#             qr/Please supply/,
#             'Exception: invalid config interface'
#         );
#     }, 'Got valid interface for Pink');

#     $zs->zs_transaction_ok(sub {
#         my $stufconfig          = $self->_create_config_interface(
#             {
#                 stuf_supplier       => 'pink',
#             }
#         );
#         my $interface           = $zs->create_named_interface_ok(
#             {
#                 module              => 'stufprs',
#                 name                => 'STUF PRS Parsing',
#                 active              => 1,
#             },
#         );

#         ok($stufconfig, 'Found config interface');

#         my $stufconfig_module = $stufconfig->module_object;

#         ok($stufconfig_module, 'found stufconfig module');

#         my ($prs_or_nps) = $stufconfig_module->get_natuurlijkpersoon_interface($stufconfig);
#         ok($prs_or_nps, 'Got PRS or NPS module');

#         is($prs_or_nps->module, 'stufprs', 'Found PRS module');
#     }, 'Got valid interface for Pink');

#     $zs->zs_transaction_ok(sub {
#         my $stufconfig          = $self->_create_config_interface(
#             {
#                 stuf_supplier       => 'vicrea',
#             }
#         );
#         my $interface           = $zs->create_named_interface_ok(
#             {
#                 module              => 'stufprs',
#                 name                => 'STUF PRS Parsing',
#                 active              => 1,
#             },
#         );

#         ok($stufconfig, 'Found config interface');

#         my $stufconfig_module = $stufconfig->module_object;

#         ok($stufconfig_module, 'found stufconfig module');

#         my ($prs_or_nps) = $stufconfig_module->get_natuurlijkpersoon_interface($stufconfig);
#         ok($prs_or_nps, 'Got PRS or NPS module');

#         is($prs_or_nps->module, 'stufprs', 'Found PRS module');
#     }, 'Got valid interface for Vicrea');
# }

=head2 stuf_prs_can_search_sbus

=cut

# sub stuf_prs_can_search_sbus : Tests {
#     my $self            = shift;

#     $zs->zs_transaction_ok(sub {
#         $zs->_create_prs_interface(
#             {
#                 module              => 'stufprs',
#                 name                => 'STUF PRS Parsing',
#                 active              => 0,
#             },
#         );

#         my $stufconfig_module = $stufconfig->module_object;

#         ok(!$stufconfig_module->can_search_sbus($stufconfig), 'Search sbus: inactive interface: false');
#     }, 'Testing exception: inactive interface');

#     $zs->zs_transaction_ok(sub {
#         my $stufconfig          = $self->_create_config_interface(
#             {
#                 stuf_supplier       => 'vicrea',
#             }
#         );
#         my $interface           = $zs->create_named_interface_ok(
#             {
#                 module              => 'stufprs',
#                 name                => 'STUF PRS Parsing',
#                 active              => 1,
#                 interface_config    => {
#                     bidirectional       => 0,
#                     sbus_search         => 1,
#                 }
#             },
#         );

#         my $stufconfig_module = $stufconfig->module_object;

#         ok(!$stufconfig_module->can_search_sbus($stufconfig), 'Search sbus: no bidirectional set: false');
#     }, 'Testing exception: bidirectional turned off');

#     $zs->zs_transaction_ok(sub {
#         my $stufconfig          = $self->_create_config_interface(
#             {
#                 stuf_supplier       => 'vicrea',
#                 bidirectional       => 1,
#                 sbus_search         => 0,
#             }
#         );
#         my $interface           = $zs->create_named_interface_ok(
#             {
#                 module              => 'stufprs',
#                 name                => 'STUF PRS Parsing',
#                 active              => 1,
#             },
#         );

#         my $stufconfig_module = $stufconfig->module_object;

#         ok(!$stufconfig_module->can_search_sbus($stufconfig), 'Search sbus: no sbus_search set: false');
#     }, 'Testing exception: sbus_search turned off');

#     $zs->zs_transaction_ok(sub {
#         my $stufconfig          = $self->_create_config_interface(
#             {
#                 stuf_supplier       => 'vicrea',
#                 bidirectional       => 1,
#                 sbus_search         => 1,
#             }
#         );
#         my $interface           = $zs->create_named_interface_ok(
#             {
#                 module              => 'stufprs',
#                 name                => 'STUF PRS Parsing',
#                 active              => 1,
#             },
#         );

#         my $stufconfig_module = $stufconfig->module_object;

#         my $prsnps;
#         ok(($prsnps = $stufconfig_module->can_search_sbus($stufconfig)), 'Search sbus: all set: true');

#         is($prsnps->module, 'stufprs', 'Retrieve module is stufprs module');
#     }, 'Got correct answer');
# }

=head2 stuf_prs_can_search_gbav

=cut

# sub stuf_prs_can_search_gbav : Tests {
#     my $self            = shift;

#     $zs->zs_transaction_ok(sub {
#         $self->_create_prs_interface(
#             {
#                 config => {stuf_supplier       => 'vicrea'},
#             }
#         );

#         my $stufconfig_module = $stufconfig->module_object;

#         ok(!$stufconfig_module->can_search_gbav($stufconfig), 'Search sbus: inactive interface: false');
#     }, 'Testing exception: inactive interface');

#     $zs->zs_transaction_ok(sub {
#         my $stufconfig          = $self->_create_config_interface(
#             {
#                 stuf_supplier       => 'vicrea',
#             }
#         );
#         my $interface           = $zs->create_named_interface_ok(
#             {
#                 module              => 'stufprs',
#                 name                => 'STUF PRS Parsing',
#                 active              => 1,
#                 interface_config    => {
#                     bidirectional       => 0,
#                     gbav_search         => 1,
#                 }
#             },
#         );

#         my $stufconfig_module = $stufconfig->module_object;

#         ok(!$stufconfig_module->can_search_gbav($stufconfig), 'Search sbus: no bidirectional set: false');
#     }, 'Testing exception: bidirectional turned off');

#     $zs->zs_transaction_ok(sub {
#         my $stufconfig          = $self->_create_config_interface(
#             {
#                 stuf_supplier       => 'vicrea',
#                 bidirectional       => 1,
#                 gbav_search         => 0,
#             }
#         );
#         my $interface           = $zs->create_named_interface_ok(
#             {
#                 module              => 'stufprs',
#                 name                => 'STUF PRS Parsing',
#                 active              => 1,
#             },
#         );

#         my $stufconfig_module = $stufconfig->module_object;

#         ok(!$stufconfig_module->can_search_gbav($stufconfig), 'Search sbus: no gbav_search set: false');
#     }, 'Testing exception: gbav_search turned off');

#     $zs->zs_transaction_ok(sub {
#         my $stufconfig          = $self->_create_config_interface(
#             {
#                 stuf_supplier       => 'vicrea',
#                 bidirectional       => 1,
#                 gbav_search         => 1,
#             }
#         );
#         my $interface           = $zs->create_named_interface_ok(
#             {
#                 module              => 'stufprs',
#                 name                => 'STUF PRS Parsing',
#                 active              => 1,
#             },
#         );

#         my $stufconfig_module = $stufconfig->module_object;

#         my $prsnps;
#         ok(($prsnps = $stufconfig_module->can_search_gbav($stufconfig)), 'Search sbus: gbav_search set: true');

#         is($prsnps->module, 'stufprs', 'Retrieve module is stufprs module');
#     }, 'Got correct answer');
# }


=head1 HELPER METHODS

=head2 _create_prs_interface

Creates a prs interface to work with

=cut

sub _create_prs_interface {
    my $self            = shift;
    my $params          = shift || {};

    return $zs->create_stuf_interfaces_ok($params);
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

