package TestFor::General::Backend::Sysin::Modules::ControlPanel;
use base qw(Test::Class);
use TestSetup;
use Zaaksysteem::Backend::Sysin::Modules::ControlPanel;

=head1 NAME

TestFor::General::Backend::Sysin::Modules::ControlPanel - A control panel tester

=head1 SYNOPSIS

    TEST_METHOD='zs_control_panel.*' ./zs_prove -v t/testclass

=head1 TEST_METHODS

=head2 zs_control_panel

Unit test the Control Panel interface.

=cut


sub zs_control_panel : Tests {
    $zs->zs_transaction_ok(
        sub {
            my $interface = $zs->create_interface_ok(
                module           => 'controlpanel',
                name             => 'Controlpanel',
                interface_config => {
                    "medewerker" => {
                        "object_type" => "medewerker",
                        "id"          => 1,
                        "naam"        => "A. Admin",
                        "username"    => "admin"
                    },
                    "api_key" => "meuk"
                },
            );

            my %tests = (
                releases => {
                    master    => "Productie",
                    accept    => "Acceptatie",
                    hotfix    => "Patch release",
                    quarterly => "Testing",
                    sprint    => "Development",
                },
                clouds => {
                    gov        => 'Overheidscloud',
                    'gov-acc'  => 'Overheidscloud - accept',
                    commercial => "Commerciele cloud",
                    training   => "Trainigscloud",
                    ontwikkel  => "Ontwikkelingscloud",
                    dev        => "Mintlab development cloud",
                },
                loadbalancers => {
                    gov        => 'Overheidscloud',
                    'gov-acc'  => 'Overheidscloud - accept',
                    commercial => "Commerciele cloud",
                    training   => "Trainigscloud",
                    ontwikkel  => "Ontwikkelingscloud",
                    dev        => "Mintlab development cloud",
                },
                templates => {
                    training   => 'Training template',
                    production => 'A copy of your production template',
                    accept     => 'A copy of your accept template',
                    initial =>
                        'An empty Zaaksysteem with predefined case types',
                    clean => 'An empty Zaaksysteem, no case types',
                },
            );

            my $res;
            foreach (sort keys %tests) {
                $res = $interface->process_trigger($_);
                is_deeply($res, $tests{$_}, "process_trigger for $_ works");
            }
        },
        "Control panel interface test"
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
