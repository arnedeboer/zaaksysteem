package TestFor::General::Backend::Sysin::Modules::Email;
use base qw(Test::Class);

use TestSetup;

use Zaaksysteem::Backend::Email;
use Zaaksysteem::Backend::Mailer;
use Zaaksysteem::Zaken::DelayedTouch;

use Zaaksysteem::Backend::Sysin::Modules::Email;
use Zaaksysteem::Backend::Mailer;


sub create_interface {
    return $zs->create_named_interface_ok({
        module => 'email',
        name   => 'email',
    });
}


sub get_email_module {
    my $callback = shift;

    my $email_sender = Test::MockObject->new();
    $email_sender->mock('send', sub {
        my ($self, $message) = @_;
        return $callback->($message) if $callback;
    });

    my $module = Zaaksysteem::Backend::Sysin::Modules::Email->new;
    $module->email_sender($email_sender);
    return $module;
}

sub setup : Test(startup) {
    my $self = shift;

    $zs->schema->default_resultset_attributes->{delayed_touch} = Zaaksysteem::Zaken::DelayedTouch->new;
}


sub sysin_email_base_case : Tests {
    $zs->zs_transaction_ok(sub {
        my $recipient = 'test@mintlab.nl';

        my $sent_email;
        my $module = get_email_module(sub {
            my ($message) = @_;
            $sent_email = $message;
            my $headers = $message->{header}->{headers};
            my %hashed = @$headers;
            is $hashed{To}, $recipient, "Recipient set correctly";
            return 1;
        });

        my $record = $zs->create_transaction_record_ok;
        my $case = $zs->create_case_ok;
        my $notificatie = $zs->create_notificatie_ok;

        throws_ok(sub{
            $module->_process_mail($record, {
                body => 'test body',
                notification => {
                    id                          => $notificatie->id,
                    recipient_type              => 'overig',
                    behandelaar                 => 'betrokkene-medewerker-1',
                    email                       => 'whatever',
                    case_id                     => $case->id,
                },
                case_id                     => $case->id,
            });
        }, qr|sysin/modules/email/mailer_problem|, 'Fails when no recipient is available');

        $case->aanvrager_object->email($recipient);

        {
            my $opts;
            no warnings qw(redefine once);
            local *Zaaksysteem::Backend::Email::send_from_case = sub {
                my $self = shift;
                $opts = shift;
            };
            use warnings;

            my $message = $module->_process_mail($record, {
                notification => {
                    id                          => $notificatie->id,
                    recipient_type              => 'aanvrager',
                    behandelaar                 => 'betrokkene-medewerker-1',
                    email                       => $recipient,
                    case_id                     => $case->id,
                },
                case_id                     => $case->id,
            });

            ok $message =~ m|^Zaak ID|, "Beginning of message looks good";
            is_deeply(
                $opts,
                {
                    log_error      => 1,
                    recipient      => $recipient,
                    subject        => $notificatie->subject,
                    body           => $notificatie->message,
                    attachments    => undef,
                    sender_address => undef,
                    sender         => undef,
                    cc             => undef,
                    bcc            => undef,
                },
                "send_from_case is called via send_case_notification"
            );
        }

    }, 'Unit test for sysin/modules/email');
}

sub sysin_email_attachments : Tests {
    $zs->zs_transaction_ok(sub {

        my $module = get_email_module;
        my $casetype = $zs->create_zaaktype_ok;
        my $zt_notificatie = $zs->create_zaaktype_notificatie_ok(
            node => $casetype->zaaktype_node_id
        );


        # phase 1 - empty config, no params
        is_deeply $module->get_attachment_ids, [],
            'Returns empty list if not given a notification id';


        # phase 2 - empty config, params given
        my $result = $module->get_attachment_ids($zs->schema, $zt_notificatie->id);
        is_deeply $result, [],
            'Returns empty list if notification is not bound to an attribute';


        # phase 3 - add config attachment
        my $status = $casetype->zaaktype_node_id->zaaktype_statuses->search->first;
        my $bibliotheek_kenmerk = $zs->create_bibliotheek_kenmerk_ok(value_type => 'file');
        my $zt_kenmerk = $zs->create_zaaktype_kenmerk_ok(
            bibliotheek_kenmerk => $bibliotheek_kenmerk,
            status => $status
        );

        $schema->resultset('BibliotheekNotificatieKenmerk')->create({
            bibliotheek_kenmerken_id   => $bibliotheek_kenmerk->id,
            bibliotheek_notificatie_id => $zt_notificatie->get_column('bibliotheek_notificaties_id'),
        });

        $result = $module->get_attachment_ids($zs->schema, $zt_notificatie->id);

        is_deeply $result, [$zt_kenmerk->id],
            'Returns list if notification is bound to an attribute';


        # phase 4 - integration, put an actual file in the attribute
        my $record = $zs->create_transaction_record_ok;
        my $case = $zs->create_case_ok;

        # this sub does horrible *things* that i can not speak of, but that
        # are necessary to accomplish this labour
        # (~ file_create is not completely covered by the testsuite)
        my $filename = 'my filename.pdf';
        my $file = $schema->resultset('File')->file_create({
            db_params => {
                created_by        => $zs->get_subject_ok,
                case_id           => $case->id,
                accepted          => 1,
            },
            case_document_ids => [$zt_kenmerk->id],
            file_path         => $zs->config->{filestore_test_file_path},
            name              => $filename,
        });

        $module = get_email_module(sub {
            my ($message) = @_;

            my $parts = $message->{parts};
            is ref $parts, 'ARRAY', 'Parts is a list';
            is scalar @$parts, 2, 'with two items';

            my ($main, $attachment) = @$parts;
            is $main->{body_raw}, $zt_notificatie->bibliotheek_notificaties_id->message . "\n", 'Body good';
            is $attachment->{ct}{attributes}{name}, $filename, "Filename correct";

            return 1;
        });

        $module->_process_mail($record, {
            notification => {
                id                          => $zt_notificatie->get_column('bibliotheek_notificaties_id'),
                recipient_type              => 'aanvrager',
                behandelaar                 => 'betrokkene-medewerker-1',
                email                       => 'test@test.nl',
                case_id                     => $case->id,
            },
            zaaktype_notificatie_id     => $zt_notificatie->id,
            case_id                     => $case->id,
        });

    });
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

