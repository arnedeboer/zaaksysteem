package TestFor::General::Backend::Sysin::Modules::EmailIntake;
use base qw(Test::Class);

use TestSetup;

use Zaaksysteem::Backend::Sysin::Modules::EmailIntake;
use Zaaksysteem::XML::Compile;
use Mail::Track;
use File::Spec::Functions qw(catfile);

require Zaaksysteem::Controller::API::Mail;

sub setup : Test(startup) {
    my $self = shift;
    $zs->stuf_0312;
    $self->{module} = Zaaksysteem::Backend::Sysin::Modules::Omgevingsloket->new;
}

sub zs_emailintake_documents : Tests {
    $zs->zs_transaction_ok(
        sub {
            my $interface = $zs->create_interface_ok(
                module           => 'emailintake',
                name             => 'Documentintake',
                interface_config => {
                    api_user => 'intake@meuk.nl',
                    type     => Zaaksysteem::Backend::Sysin::Modules::EmailIntake::DOCUMENT_INTAKE,
                },
            );

            my $msg = _smaf('marco.mime');

            my $pt = $interface->process_trigger(
                'process_mail', {
                    api_user => 'intake@meuk.nl',
                    message  => $msg,
            });
            isa_ok($pt, 'Zaaksysteem::Model::DB::Transaction');

            my $tr = $zs->get_from_zaaksysteem(
                resultset => 'TransactionRecord',
                search    => {transaction_id => $pt->id},
                options   => {rows => 1},
            )->first;

            like(
                $tr->preview_string,
                qr/Email van '.+' naar '.+' met onderwerp '.+' is verwerkt/,
               "Preview string matches"
            );

            my $fs = $zs->schema->resultset('File')->search_rs({});
            is($fs->count, 3, "Three files found");
        },
        "Documentintake via mail",
    );

    $zs->zs_transaction_ok(
        sub {
            my $interface = $zs->create_interface_ok(
                module           => 'emailintake',
                name             => 'Documentintake',
                interface_config => {
                    api_user => 'intake@meuk.nl',
                    type     => Zaaksysteem::Backend::Sysin::Modules::EmailIntake::DOCUMENT_INTAKE,
                },
            );

            my $mt = Mail::Track->new();
            my %params = (
                to      => 'intake@meuk.nl',
                from    => 'foobar@example.org',
                subject => 'Foobar',
            );

            my $employee = $zs->create_medewerker_ok();
            my $username = $employee->subject_id->username;

            my $msg = $mt->prepare({ %params, Body => 'This is some content', });
            $msg->add_body(
                {
                    content      => "Some strange characters like Gar\xc3\xa7on",
                    content_type => 'text/plain',
                }
            );

            # TODO: Fix bug in Mail::Track with attachment naming.
            use File::Copy qw(cp);
            my $dst = catfile(qw(t inc tmp), "$username-iets.pdf");
            cp(catfile(qw(t inc Documents openoffice_document.pdf)), $dst);
            $msg->add_attachment(
                path     => $dst,
                filename => "$username-iets.pdf",
            );

            my $message;
            my $opts;
            {
                no warnings qw(redefine once);
                local *Email::Sender::Simple::send = sub {
                    my $self = shift;
                    my $entity = shift;
                    $opts  = shift;
                    $message = $entity->stringify();
                };
                $msg->send;
            }

            my $file = Zaaksysteem::Controller::API::Mail::_save_message_as_file(
                $schema->resultset('File'), $message);

            my $pt = $interface->process_trigger(
                'process_mail', {
                    api_user => 'intake@meuk.nl',
                    message  => $file,
            });
            isa_ok($pt, 'Zaaksysteem::Model::DB::Transaction');

            my $frs = $schema->resultset('File')->search_rs({intake_owner => "betrokkene-medewerker-" . $employee->subject_id->id });
            is($frs->count, 1, "One document found");
            my $f = $frs->first;
            is_deeply(
                {
                    filename => $f->filename,
                    extension => $f->extension,
                },
                {
                    filename => 'iets.pdf',
                    extension => '.pdf'
                },
                "File properties are correct"
            );

        },
        "Documentintake via mail with username-document.pdf",
    );
}

sub zs_emailintake_generic : Tests {
    $zs->zs_transaction_ok(
        sub {
            my $zt = $zs->create_zaaktype_predefined_ok();

            my %kenmerken = (
                subject => 'onderwerp',
                body    => 'inhoud',
                from    => 'aanvrager'
            );

            my %kenmerken_mapping;

            foreach (values %kenmerken) {
                my $id = $zs->create_zaaktype_kenmerk_ok(
                    bibliotheek_kenmerk => $zs->create_bibliotheek_kenmerk_ok(
                        naam         => $_,
                        magic_string => $_
                    ),
                    node => $zt->zaaktype_node_id
                )->id;
                $kenmerken_mapping{$id} = $_;
            }

            $zt->zaaktype_node_id->zaaktype_definitie_id->update({ preset_client => "betrokkene-medewerker-" . $zs->set_current_user->id });

            my $interface = $zs->create_interface_ok(
                module           => 'emailintake',
                name             => 'Generiek',
                zaaktype         => $zt,
                interface_config => {
                    api_user => 'generiek@meuk.nl',
                    type     => Zaaksysteem::Backend::Sysin::Modules::EmailIntake::GENERIC,
                },
            );

            my @attributes = map {{
                attribute_type => 'magic_string',
                case_type_id => $zt->id,
                checked => 1,
                external_name => $_,
                internal_name => {
                    searchable_object_id => $kenmerken{$_},
                    searchable_object_label => $kenmerken{$_},
                    searchable_object_type => 'magicstring'
                },
                optional => 0
            }} keys %kenmerken;

            $interface->set_attribute_mapping(
                {attributes => \@attributes }
            );

            my $msg = _smaf('marco.mime');

            my $pt = $interface->process_trigger(
                'process_mail', {
                    api_user => 'generiek@meuk.nl',
                    message  => $msg,
            });
            isa_ok($pt, 'Zaaksysteem::Model::DB::Transaction');

            my $tr = $zs->get_from_zaaksysteem(
                resultset => 'TransactionRecord',
                search    => {transaction_id => $pt->id},
                options   => {rows => 1},
            )->first;

            like(
                $tr->preview_string,
                qr/Email van '.+' naar '.+' met onderwerp '.+' is verwerkt/,
                "Preview string matches"
            );

            my $cases = $zs->get_from_zaaksysteem(resultset => 'Zaak');
            my $case = $cases->next();
            is($cases->next, undef, "One case created");

            is($case->onderwerp, "Banana", "Subject is correct");

            my @zk = sort { $a cmp $b } map { $_->bibliotheek_kenmerken_id->naam } $case->zaak_kenmerken;
            my @bk = sort { $a cmp $b } values %kenmerken;
            is_deeply(\@zk, \@bk, "All kenmerken found in the case as well");

            my $f = $zs->get_from_zaaksysteem(resultset => 'File', search => { case_id => $case->id });
            is($f->count, 5, "Five files found, we also have the HTML");
            is($zs->get_from_zaaksysteem(resultset => 'File', search => { case_id => $case->id, accepted => 1})->count, 0, "Mails are not accepted as files");


        },
        "Documentintake via mail",
    );
}

sub zs_emailintake_olo : Tests {
    $zs->zs_transaction_ok(
        sub {
            my $interface = $zs->create_interface_ok(
                module           => 'emailintake',
                name             => 'OLO',
                interface_config => {
                    api_user => 'olo@meuk.nl',
                    type     => Zaaksysteem::Backend::Sysin::Modules::EmailIntake::OLO,
                },
            );

            my $olo = $zs->create_interface_omgevingsloket_ok();

            $zs->set_fallback_requestors($olo);

            my $msg = _smaf('olo_mail.mime');

            my $pt = $interface->process_trigger(
                'process_mail', {
                    api_user => 'olo@meuk.nl',
                    message  => $msg,
            });

            my $cases = $zs->get_from_zaaksysteem(resultset => 'Zaak');
            my $case = $cases->next();
            is($cases->next, undef, "One case created");

            is($case->onderwerp, "extra info ", "Subject is correct");

            my $f =  $zs->get_from_zaaksysteem(resultset => 'File', search => { case_id => $case->id, accepted => 1});
            is($zs->get_from_zaaksysteem(resultset => 'File', search => { case_id => $case->id, accepted => 1})->count, 1, "One accepted mail");


        },
        "Online omgevingsloket via mail",
    );
}

sub _smaf {
    return Zaaksysteem::Controller::API::Mail::_save_message_as_file($schema->resultset('File'), $zs->open_testsuite_mail(shift));
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
