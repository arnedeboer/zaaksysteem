package TestFor::General::ObjectAcl;
use base qw(Test::Class);

use TestSetup;



use Zaaksysteem::Object::Model;

sub acl_crud : Tests {
    $zs->zs_transaction_ok(sub {
        my $acls = $schema->resultset('ObjectAclEntry');

        my $count = $acls->count;

        my $auth = $zs->create_zaaktype_authorisation_ok;

        is $acls->count, $count + 1, 'Implicitly created ACL entry through zaaktype_authorisation creation';
    }, 'object/acl creating zaaktype creates acl entries');

    # ZS-2157 - ACLs must be deleted with an object deletion
    $zs->zs_transaction_ok(sub {
        my $auth = $zs->create_zaaktype_authorisation_ok;

        my $acls = $schema->resultset('ObjectAclEntry');

        my $total_count = $acls->count;

        my $objects = Zaaksysteem::Object::Model->new( schema => $schema );

        my $object = $objects->rs->find({ object_class => 'casetype', object_id => $auth->get_column('zaaktype_id') });

        isa_ok $object, 'Zaaksysteem::Backend::Object::Data::Component', 'object found';

        lives_ok sub { $object->delete }, 'object/acl object with acls deletes ok';

        is $acls->count, ($total_count - 1), 'object/acl acls implicitly deleted through cascade';
    });
}

# TODO: Extend tests when create_case_ok doesn't fail

sub without_user : Tests {
    # The following two tests 'prove' the most important aspects of ObjectAcl
    # interactions. The proof lies with the subquery being properly generated.
    # The 'properness' of the subquery is of course my interpretation.
    $zs->zs_transaction_ok(sub {
        lives_ok(
            sub { Zaaksysteem::Object::Model->new( schema => $schema )->rs },
            'object/acl object returns resultset'
        );
    }, 'object/acl returns resultset without user');
}

sub with_user : Tests {
    $zs->zs_transaction_ok(sub {
        my $user = $zs->create_subject_ok;
        my $ldap = $zs->connect_ldap_ok;

        my $user_ou_id = $ldap->user_ou($user)->get_value('l');
        my ($user_role_id) = map { $_->get_value('gidNumber') } $ldap->get_roles($user);

        my $auth = $zs->create_zaaktype_authorisation_ok(
            ou_id => $user_ou_id,
            role_id => $user_role_id
        );

        my $zaaktype = $auth->zaaktype_id;

        my $test_object = $zs->create_zaaktype_authorisation_ok->zaaktype_id->_object;

        $test_object->object_class('case');
        $test_object->class_uuid($zaaktype->_object->uuid);
        $test_object->update;

        # First we remove all ACLs so we can verify ACL-injected resultsets
        # really hide the content
        my $object = Zaaksysteem::Object::Model->new( schema => $schema );

        $test_object->object_acl_entries->delete_all;
        $object->rs->find($test_object->get_column('class_uuid'))->object_acl_entries->delete_all;

        ok $object->rs->find($test_object->uuid),
            'object/acl/casetype acl entries deleted, but object remains';

        is $object->rs->find($test_object->get_column('class_uuid'))->object_acl_entries->count, 0,
            'object/acl/casetype acls removed';

        my $object_user = Zaaksysteem::Object::Model->new(
            schema => $schema,
            user => $user,
            ldap => $ldap
        );

        is $object_user->rs->search({ object_class => 'case' })->count, 0, 'object/acl/casetype user cant find object';

        # diag(explain({ $test_object->class_uuid->get_columns }, { $object_user->rs->search({ object_class => 'case' })->first->get_columns }));

        my $updated_zaaktype_object = $zaaktype->_sync_object;

        ok $updated_zaaktype_object->object_acl_entries->count > 0,
            'object/acl/casetype sync_objects adds acls';

        $object_user = Zaaksysteem::Object::Model->new(
            schema => $schema,
            user => $user,
            ldap => $ldap
        );

        is $object_user->rs->search({ object_class => 'case' })->count, 1, 'object/acl/casetype user can find object after sync';
    }, 'object/acl ACLs actually hide objects in resultset');

    # TODO: Extend tests here with a zs_transaction_ok that checks multi-object
    # permissions
    # This is not yet possible because of limitations in LDAP mocking (every
    # user has every role, so you can't contrast one user's result to another)
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
