package TestFor::General::Email;
use base qw(Test::Class);

use TestSetup;

use Zaaksysteem::Email;
use File::Spec::Functions qw(catfile);

sub zs_email_documents : Tests {
    $zs->txn_ok(
        sub {
            my $mt  = Mail::Track->new();
            my $msg = $mt->parse($zs->open_testsuite_mail('marco.mime'));

            my $mailer = Zaaksysteem::Email->new(
                message => $msg,
                schema  => $zs->schema,
            );

            $mailer->add_attachments();

            my $rs
                = $schema->resultset('File')->search({ date_deleted => undef });
            is($rs->count, 3, "Three files found");
        }
    );
}

sub zs_email_case : Tests {
    $zs->txn_ok(
        sub {
            my $mt  = Mail::Track->new();
            my $msg = $mt->parse($zs->open_testsuite_mail('marco.mime'));

            my $mailer = Zaaksysteem::Email->new(
                message => $msg,
                schema  => $zs->schema,
            );

            my $case = $zs->create_case_ok();

            $mailer->add_to_case($case);

            my $rs = $schema->resultset('File')->search({ date_deleted => undef });
            is($rs->count, 5, "Five files found");
        }
    );
}

sub zs_email_olo : Tests {
    $zs->txn_ok(
        sub {

            $zs->stuf_0312;

            my $mt  = Mail::Track->new();
            my $msg = $mt->parse($zs->open_testsuite_mail('olo_mail.mime'));

            my $mailer = Zaaksysteem::Email->new(
                message => $msg,
                schema  => $zs->schema,
            );

            my $olo = $zs->create_interface_omgevingsloket_ok();
            $zs->set_fallback_requestors($olo);

            $mailer->add_to_omgevingsloket();

            my $rs = $schema->resultset('File')->search({ date_deleted => undef, accepted => 1 });
            is($rs->count, 1, "XML file is accepted");

            $rs = $schema->resultset('File')->search({ date_deleted => undef, accepted => 0 });
            is($rs->count, 3, "Attachments are downloaded, not accepted");

            my $case = $rs->first->case_id;
            # Aanvulling
            {
                my $msg = $mt->parse($zs->open_testsuite_mail('olo_aanvulling.mime'));
                my $mailer = Zaaksysteem::Email->new(
                    message => $msg,
                    schema  => $zs->schema,
                );

                $mailer->add_to_omgevingsloket();

                my $rs = $schema->resultset('File')->search(
                    {
                        date_deleted => undef,
                        accepted     => 1,
                        case_id      => $case->id
                    }
                );
                is($rs->count, 2, "All XML files are saved");

                $rs = $schema->resultset('File')->search({ date_deleted => undef, accepted => 0 });
                is($rs->count, 4, "Attachments are downloaded, not accepted");
            }


        }
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
