package TestFor::Catalyst::CaseTypes;
use base qw(Test::Class);

use TestSetup;

zs_configfile_ok;

# use Zaaksysteem::TestMechanize;
use constant DEFAULT_URI => "http://testsuite";

sub casetype : Tests {
    my $self    = shift;
    my $mech = $zs->mech;
    $mech->zs_login();

    my $casetype_id;
    my $json;

    $mech->get_ok(DEFAULT_URI . '/casetype');
    $json = parse_zapi_response_ok($mech->response);
    is(
        $json->{result}[0]{object_type}, 'zaaktype',
        "Got zaaktype from result"
    );
    is($json->{rows}, 1, "Got one active zaaktype from result");
    $casetype_id = $json->{result}[0]{id};
    ok($casetype_id, "Case type ID found");

    $mech->post_ok(
        DEFAULT_URI .  "/beheer/zaaktypen/$casetype_id/offline", {
            confirmed      => 1,
            commit_message => 'bla',
        },
    );
    $mech->get_ok(DEFAULT_URI . '/casetype');
    $json = parse_zapi_response_ok($mech->response);
    is($json->{rows}, 0, "Casetype $casetype_id is inactive");

    $mech->get_ok(
        DEFAULT_URI . '/casetype?inactive=1',
        "Requesting all zaaktypes, including inactive"
    );
    $json = parse_zapi_response_ok($mech->response);
    is(
        $json->{result}[0]{object_type}, 'zaaktype',
        "Got zaaktype from result"
    );
    is($json->{rows}, 1, "Got one inactive zaaktype from result");
    is(
        $json->{result}[0]{id}, $casetype_id,
        "Got correct inactive zaaktype from result"
    );

    $mech->post_ok(
        DEFAULT_URI . "/beheer/zaaktypen/$casetype_id/online", {
            confirmed      => 1,
            commit_message => 'bla',
        },
    );

    $mech->get_ok(DEFAULT_URI . '/casetype');
    $json = parse_zapi_response_ok($mech->response);
    is($json->{rows}, 1, "Got one active zaaktypes from result");
    is(
        $json->{result}[0]{id}, $casetype_id,
        "Got correct zaaktype from result"
    );
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

