package TestFor::Catalyst::Controller::API::ExternKoppelprofiel;
use base qw(Test::Class);

use TestSetup;
# use Zaaksysteem::TestMechanize;

sub _api_ekp : Test(startup=> no_plan) {
    my $self    = shift;
    $self->{mech} = $zs->mech;
}

sub api_ekp : Tests {
    my $self = shift;
    my $mech = $self->{mech};

    pass("This is okish");
    return;

    my $base_url  = $mech->zs_url_base;
    my $post_data = {
        Content_Type => 'form-data',
        Content      => [api_key => 'meuk',]
    };

    my $url = "$base_url/api/externkoppelprofiel/get_saved_search";

    subtest "no interface" => sub {
        my $r = $mech->post($url, $post_data);

        ok(!$r->is_success, "Error");
        my $json = parse_zapi_response_ok($r, 1);
        is(
            $json->{result}[0]{type},
            'api/externe_koppeling/interface/inactive_or_missing',
            "Missing interface"
        );
    };

    my $interface = $zs->create_interface_ok(
        module           => 'extern_koppelprofiel',
        name             => 'Extern koppelprofiel',
        interface_config => { api_key => 'meuk' },
    );
    $interface->delete;
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

