package TestFor::Catalyst::Controller::API::Object;
use base qw(Test::Class);

use TestSetup;
zs_configfile_ok;

use JSON;
# use Zaaksysteem::TestMechanize;
use Zaaksysteem::Object::Model;

sub setup : Test(startup => no_plan) {
    my $self = shift;

    my $agent = $zs->mech;

    $agent->zs_login;

    $self->{ agent } = $agent;
    $self->{ model } = Zaaksysteem::Object::Model->new(schema => $zs->schema);
}

sub test_object_api_create : Tests {
    my $mech = shift->{ agent };

    # /api/object/save
    $mech->post_ok(
        $mech->zs_url_base . "/api/object/save",
        {
            object => '{ "object_class": "object" }',
        }
    );

    my $object_data = decode_json($mech->content);
    my $initial_object = $object_data->{result}[0];

    is_deeply($initial_object->{actions}, [qw(read write manage)], 'Actions on a base object');
    is_deeply($initial_object->{values}, {}, 'No values on a base object');
    is($initial_object->{type}, 'object', 'Correct object type created');
    like(
        $initial_object->{id},
        qr/^[0-9a-f]{8}(?:-[0-9a-f]{4}){3}-[0-9a-f]{12}$/,
        "Resulting object has a UUID",
    );

    # /api/object/UUID
    $mech->get_ok(
        $mech->zs_url_base . "/api/object/" . $initial_object->{id},
    );
    my $retrieved_object_data = decode_json($mech->content);
    my $retrieved_object = $retrieved_object_data->{result}[0];

    is_deeply(
        $retrieved_object,
        $initial_object,
        "Object retrieved using /api/object/UUID is identical to object returned by api/object/save"
    );

    # /api/object/UUID/delete?id=FOO
    $mech->post_ok(
        $mech->zs_url_base . "/api/object/$initial_object->{id}/delete",
    );

    $mech->get(
        $mech->zs_url_base . "/api/object/" . $initial_object->{id},
    );
    ok(!$mech->success, "Retrieving a deleted object fails");
}

use constant CREATE_OBJECT_TYPE_JSON => <<JSON;
{
    "object_class": "type",
    "category_id": "%s",
    "name": "Plantenbak",
    "casetype_name": "Melding woon of leefomgeving",

    "instance_authorizations": [],
    "related_casetypes": [],
    "attributes": [],
    "modified_sections": [],
    "modification_rationale": "Because I felt like it"
}
JSON

sub test_object_api_type_create : Tests {
    my $t = shift;

    my $agent = $t->{ agent };
    my $model = $t->{ model };

    my $category = $zs->create_bibliotheek_categorie_ok;

    my $object_json = sprintf(
        CREATE_OBJECT_TYPE_JSON,
        $category->id
    );

    $agent->post_ok(
        sprintf('%s/api/object/save', $agent->zs_url_base),
        { object => $object_json }
    );

    my $response = decode_json($agent->content)->{ result }[0];

    my $od = $zs->schema->resultset('ObjectData')->find($response->{ id });

    isa_ok $od, 'Zaaksysteem::Backend::Object::Data::Component',
        'Got back a valid ObjectData row';

    my $object = $model->inflate(row => $od);

    isa_ok $object, 'Zaaksysteem::Object',
        'Got back a valid Object instance from model';

    isa_ok $object, 'Zaaksysteem::Object::Types::Type',
        'Got back a valid Object::Types::Type instance from model';

    is $object->category_id, $category->id, 'category_id matches created category';
    is $object->name, 'Plantenbak', 'name matches';
    is $object->prefix, 'plantenbak', 'prefix automatically deduced from name correctly lower-cased';
    #is $object->casetype_name, 'Melding woon of leefomgeving', 'casetype_name matches';
    is $object->modification_rationale, 'Because I felt like it', 'modification_rationale matches';

    # Attempt to retrieve via API as well for complete picture

    $agent->get_ok(
        sprintf('%s/api/object/%s', $agent->zs_url_base, $object->id)
    );

    is_deeply $response, decode_json($agent->content)->{ result }[0],
        'object returned by save call is same as via normal get';
}

use constant CREATE_OBJECT_TYPE_WITH_ATTRS_JSON => <<JSON;
{
    "object_class": "type",
    "category_id": "%s",
    "name": "Plantenbak",
    "casetype_name": "Plantenbak-related casetype",

    "instance_authorizations": [],
    "related_casetypes": [],
    "modified_sections": [],
    "modification_rationale": "<Some rationale>",

    "attributes": [
        { "attribute_id": 12, "attribute_label": "Test A", "attribute_type": "text", "index": false, "required": true }
    ]
}
JSON

sub test_object_api_type_create_with_attributes : Tests {
    my $t = shift;

    my $agent = $t->{ agent };
    my $model = $t->{ model };

    my $category = $zs->create_bibliotheek_categorie_ok;

    my $object_json = sprintf(
        CREATE_OBJECT_TYPE_WITH_ATTRS_JSON,
        $category->id
    );

    $agent->post_ok(
        sprintf('%s/api/object/save', $agent->zs_url_base),
        { object => $object_json }
    );

    my $response = decode_json($agent->content)->{ result }[0];

    ok exists $response->{ values } && exists $response->{ values }{ attributes },
        'Re-flattened object type response from save has attributes';

    my $attributes = $response->{ values }{ attributes };

    ok ref $attributes eq 'ARRAY', 'Returned attributes are wrapped in an array';
    ok ref $attributes->[0] eq 'HASH', 'Returned attribute is a hash';

    for my $key (qw[attribute_id attribute_label attribute_type index required]) {
        ok exists $attributes->[0]{ $key }, "$key exists in attribute hash";
    }

    is $attributes->[0]{ attribute_type }, 'text', 'attribute_type is correct';
    is $attributes->[0]{ attribute_label }, 'Test A', 'attribute_type is correct';
    is $attributes->[0]{ attribute_id }, 12, 'attribute_type is correct';
    ok !$attributes->[0]{ index }, 'index is false';
    ok $attributes->[0]{ required }, 'required is true';
}

use constant CREATE_OBJECT_TYPE_WITH_RELATED_CASETYPES_JSON => <<JSON;
{
    "object_class": "type",
    "category_id": "%s",
    "name": "Plantenbak",
    "casetype_name": "Melding woon of leefomgeving",

    "instance_authorizations": [],
    "attributes": [],
    "modified_sections": [],
    "modification_rationale": "Because I felt like it",

    "related_casetypes": [
        { "id": %d, "title": "%s" }
    ]
}
JSON

sub test_object_api_type_create_with_related_casetypes : Tests {
    my $t = shift;

    my $agent = $t->{ agent };
    my $model = $t->{ model };

    my $category = $zs->create_bibliotheek_categorie_ok;
    my $casetype = $zs->create_zaaktype_ok;

    my $object_json = sprintf(
        CREATE_OBJECT_TYPE_WITH_RELATED_CASETYPES_JSON,
        $category->id,
        $casetype->id,
        $casetype->title,
    );

    $agent->post_ok(
        sprintf('%s/api/object/save', $agent->zs_url_base),
        { object => $object_json }
    );

    my $response = decode_json($agent->content)->{ result }[0];

    ok exists $response->{ values } && exists $response->{ values }{ related_casetypes },
        'Re-flattened object type response from save has attributes';

    my $casetypes = $response->{ values }{ related_casetypes };

    is_deeply $casetypes, [
        {
            related_object_id    => $casetype->id,
            related_object_title => $casetype->title,
            related_object_type  => 'casetype',
            related_object       => undef,
        }
    ], 'casetypes correctly (re)deflated';
}

use constant CREATE_OBJECT_SAVED_SEARCH => <<JSON;
{
    "object_class": "saved_search",

    "zql": "SELECT {} FROM product",
    "query": "",
    "title": "my saved search"
}
JSON

sub test_object_api_security : Tests {
    my $t = shift;

    my $agent = $t->{ agent };
    my $model = $t->{ model };

    $agent->post_ok(
        sprintf('%s/api/object/save', $agent->zs_url_base),
        { object => CREATE_OBJECT_SAVED_SEARCH }
    );

    my $response = decode_json($agent->content)->{ result }[0];

    isa_ok $response, 'HASH', 'Saving object returns deflated instance';

    my $object_url_base = sprintf(
        '%s/api/object/%s',
        $agent->zs_url_base,
        $response->{ id }
    );

    my $subject = $zs->create_subject_ok();

    my $rule_hash = {
        capability  => 'read',
        entity_type => 'user',
        entity_id   => $subject->username
    };

    with_stopped_clock {
        $agent->post_ok(sprintf('%s/permit', $object_url_base), $rule_hash);

        # Fixup rule info for is_deeply checks later on
        $rule_hash->{ verdict } = 'permit';

        $response = decode_json($agent->content)->{ result }[0];

        my $rules = $response->{ security_rules };

        is ref $rules, 'ARRAY', 'security rules hydrated as ARRAY';

        my ($rule) = grep { $_->{ entity_id } eq $subject->username } @{ $rules };

        is_deeply $rule, $rule_hash;

        $agent->get_ok($object_url_base);

        is_deeply decode_json($agent->content)->{ result }[0], $response,
            'chain-getting fresh object hydration same as ratify return value';

        $agent->post_ok(sprintf('%s/revoke', $object_url_base), $rule_hash);

        $response->{ security_rules } = [
            grep { $_->{ entity_id } ne $subject->username } @{ $rules }
        ];

        is_deeply decode_json($agent->content)->{ result }[0], $response,
            'after rule revocation, returned hydration equals previous hydration sans revoked rule';

        $agent->get_ok($object_url_base);

        is_deeply decode_json($agent->content)->{ result }[0], $response,
            'chain-getting fresh object hydration save as revoke return value';
    };
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

