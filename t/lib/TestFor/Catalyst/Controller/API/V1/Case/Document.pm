package TestFor::Catalyst::Controller::API::V1::Case::Document;
use base qw(ZSTest::Catalyst TestFor::Catalyst::Controller::API::V1::Roles::TestPreperation);

use Moose;
use TestSetup;

use File::Copy qw(cp);
use File::Temp qw(tempfile);


=head1 NAME

TestFor::Catalyst::Controller:API::V1::Case::Document - Proves the boundaries of our API: Case/Document

=head1 SYNOPSIS

    See USAGE tests

    Quick test of this single file from within vagrant
    ZS_DISABLE_STUF_PRELOAD=1 ./zs_prove -v t/lib/TestFor/Catalyst/Controller/API/V1/Case/Document.pm

=head1 DESCRIPTION

These tests prove the interactions between the outside servicebus and our zaaksysteem. This test
uses the version 1 API of zaaksysteem, and proves the C<api/v1/case/UUID/document> namespace.

=head1 USAGE 

Usage tests, use these if you would like to know how to interact with the API, it's also useful
for extended documentation of the StUF API.

=head2 Retrieval

=head3 Download an already uploaded document

    # curl --anyauth -k --digest -u "zaaksysteem:abcdefghijklmnop123qrs" https://localhost/api/v1/case/32da49c7-d6d9-449e-9de7-18203049f181/document/5aead1cc-68d0-41c9-a30f-b4d762ad13a8/download

B<Response>

A file download

=cut

sub cat_api_v1_case_download_document : Tests {
    my $self    = shift;

    ### Upload of a single file
    $zs->zs_transaction_ok(sub {
        my $mech = $zs->mech;

        $self->_create_api_interface;
        my $casetype_data = $self->api_test_zaaktype;
        my $zt            = $casetype_data->{casetype};
        my $person  = $zs->create_natuurlijk_persoon_ok;

        $mech->zs_login;

        ### Upload a file
        $mech->post_file(
            $mech->zs_url_base . '/api/v1/case/prepare_file',
            $zs->config->{filestore_test_file_path}
        );

        my $json      = $mech->content;
        my $perl      = $self->_get_json_as_perl($json);

        my ($first_uuid)  = %{ $perl->{result}->{instance}->{references} };

        $mech->post_json(
            $mech->zs_url_base . '/api/v1/case/create',
            {
                casetype_id => $zt->_object->uuid,
                source      => 'webformulier',
                requestor   => {
                    type    => 'person',
                    id      => $person->burgerservicenummer,
                },
                values => {
                    test_comment  => ['This is telling you: welcome to our api'],
                    test_paspoort => [$first_uuid],
                }
            }
        );

        $json         = $mech->content;

        my $c_json    = $self->get_case_structure($json);

        $mech->get_ok($mech->zs_url_base . '/api/v1/case/' . $c_json->{id} . '/document/' . $first_uuid . '/download', 'Succesfully downloaded file');
    });
}

=head3 Get metadata of a bunch of documents

    # curl --anyauth -k --digest -u "zaaksysteem:abcdefghijklmnop123qrs" https://localhost/api/v1/case/32da49c7-d6d9-449e-9de7-18203049f181/document

Update a case

B<Response>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-4d3835-939326",
   "result" : {
      "instance" : {
         "rows" : [
            {
               "instance" : {
                  "checksum" : "md5:b771ff6f06666e4eb88aab9f2131a68f",
                  "created" : "2015-03-04T10:59:02Z",
                  "id" : "1180d4f8-d07a-467d-a4b6-213905749b12",
                  "last_modified" : "2015-03-04T10:59:05Z",
                  "metadata" : {},
                  "mimetype" : "text/plain",
                  "name" : "FilestoreTest.txt",
                  "size" : 2387,
                  "title" : "FilestoreTest"
               },
               "reference" : "1180d4f8-d07a-467d-a4b6-213905749b12",
               "type" : "file"
            },
            {
               "instance" : {
                  "checksum" : "md5:b771ff6f06666e4eb88aab9f2131a68f",
                  "created" : "2015-03-04T10:59:02Z",
                  "id" : "6df8d923-1425-47b3-b966-0d7a6506e068",
                  "last_modified" : "2015-03-04T10:59:06Z",
                  "metadata" : {},
                  "mimetype" : "text/plain",
                  "name" : "wAsE3ldPML.txt",
                  "size" : 2387,
                  "title" : "wAsE3ldPML"
               },
               "reference" : "6df8d923-1425-47b3-b966-0d7a6506e068",
               "type" : "file"
            }
         ]
      },
      "type" : "set"
   },
   "status_code" : 200
}

=end javascript

=cut

sub cat_api_v1_case_document_list : Tests {
    my $self    = shift;

    ### Upload of a single file
    $zs->zs_transaction_ok(sub {
        my $mech = $zs->mech;

        $self->_create_api_interface;
        my $casetype_data = $self->api_test_zaaktype;
        my $zt            = $casetype_data->{casetype};
        my $person  = $zs->create_natuurlijk_persoon_ok;

        $mech->zs_login;

        ### Upload a file
        $mech->post_file(
            $mech->zs_url_base . '/api/v1/case/prepare_file',
            $zs->config->{filestore_test_file_path}
        );


        my $json      = $mech->content;
        my $perl      = $self->_get_json_as_perl($json);

        my ($first_uuid)  = %{ $perl->{result}->{instance}->{references} };

        my ($fh, $filename) = tempfile(TMPDIR => 1, UNLINK => 1, SUFFIX => '.txt');

        cp($zs->config->{filestore_test_file_path}, $filename);

        ### Upload a second file
        $mech->post_file(
            $mech->zs_url_base . '/api/v1/case/prepare_file',
            $filename
        );

        $json      = $mech->content;
        $perl      = $self->_get_json_as_perl($json);

        my ($second_uuid)  = %{ $perl->{result}->{instance}->{references} };

        $mech->post_json(
            $mech->zs_url_base . '/api/v1/case/create',
            {
                casetype_id => $zt->_object->uuid,
                source      => 'webformulier',
                requestor   => {
                    type    => 'person',
                    id      => $person->burgerservicenummer,
                },
                values => {
                    test_comment  => ['This is telling you: welcome to our api'],
                    test_paspoort => [$first_uuid, $second_uuid],
                }
            }
        );

        $json         = $mech->content;

        my $c_json    = $self->get_case_structure($json);

        $mech->get_ok($mech->zs_url_base . '/api/v1/case/' . $c_json->{id} . '/document', 'Succesfully got metadata');

        $perl      = $self->_get_json_as_perl($mech->content);

        is($perl->{result}->{type}, 'set', 'Got an set of files');
        is(@{ $perl->{result}->{instance}->{rows} }, 2, 'Got two files');

        my $instance = pop(@{ $perl->{result}->{instance}->{rows} });
        $instance  = $instance->{instance};

        is($instance->{id}, $second_uuid, 'Instance ID matches requested file id');
        ok($instance->{$_}, "Found data for '$_'") for qw/checksum created id last_modified metadata mimetype name size title/;
        like($instance->{last_modified}, qr/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}Z$/, 'Found "last_modified", like: 2015-01-19T13:59:02Z');
        like($instance->{created}, qr/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}Z$/, 'Found "created", like: 2015-01-19T13:59:02Z');
        like($instance->{mimetype}, qr|\w+/\w+|, 'Found valid mimetype');
        like($instance->{size}, qr|^\d+$|, 'Got an number of bytes');
        like($instance->{checksum}, qr|^md5:[a-f0-9]{32}$|, 'Got a valid md5 string');
    });
}

=head3 Get metadata of a document

    # curl --anyauth -k --digest -u "zaaksysteem:abcdefghijklmnop123qrs" https://localhost/api/v1/case/32da49c7-d6d9-449e-9de7-18203049f181/document/5aead1cc-68d0-41c9-a30f-b4d762ad13a8

Update a case

B<Response>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-cf3848-16f6ff",
   "result" : {
      "instance" : {
         "checksum" : "md5:b771ff6f06666e4eb88aab9f2131a68f",
         "created" : "2015-03-04T11:01:05Z",
         "id" : "8f03c9a6-2d21-4186-9aee-25a13ba1bcd8",
         "last_modified" : "2015-03-04T11:01:08Z",
         "metadata" : {},
         "mimetype" : "text/plain",
         "name" : "FilestoreTest.txt",
         "size" : 2387,
         "title" : "FilestoreTest"
      },
      "reference" : "8f03c9a6-2d21-4186-9aee-25a13ba1bcd8",
      "type" : "file"
   },
   "status_code" : 200
}

=end javascript

=cut

sub cat_api_v1_case_document_get : Tests {
    my $self    = shift;

    ### Upload of a single file
    $zs->zs_transaction_ok(sub {
        my $mech = $zs->mech;

        $self->_create_api_interface;
        my $casetype_data = $self->api_test_zaaktype;
        my $zt            = $casetype_data->{casetype};
        my $person  = $zs->create_natuurlijk_persoon_ok;

        $mech->zs_login;

        ### Upload a file
        $mech->post_file(
            $mech->zs_url_base . '/api/v1/case/prepare_file',
            $zs->config->{filestore_test_file_path}
        );

        my $json      = $mech->content;
        my $perl      = $self->_get_json_as_perl($json);

        my ($first_uuid)  = %{ $perl->{result}->{instance}->{references} };

        $mech->post_json(
            $mech->zs_url_base . '/api/v1/case/create',
            {
                casetype_id => $zt->_object->uuid,
                source      => 'webformulier',
                requestor   => {
                    type    => 'person',
                    id      => $person->burgerservicenummer,
                },
                values => {
                    test_comment  => ['This is telling you: welcome to our api'],
                    test_paspoort => [$first_uuid],
                }
            }
        );

        $json         = $mech->content;

        my $c_json    = $self->get_case_structure($json);

        $mech->get_ok($mech->zs_url_base . '/api/v1/case/' . $c_json->{id} . '/document/' . $first_uuid, 'Succesfully got metadata');

        $perl      = $self->_get_json_as_perl($mech->content);

        is($perl->{result}->{type}, 'file', 'Got an instance of type "file"');
        my $instance  = $perl->{result}->{instance};

        is($instance->{id}, $first_uuid, 'Instance ID matches requested file id');
        ok($instance->{$_}, "Found data for '$_'") for qw/checksum created id last_modified metadata mimetype name size title/;
        like($instance->{last_modified}, qr/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}Z$/, 'Found "last_modified", like: 2015-01-19T13:59:02Z');
        like($instance->{created}, qr/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}Z$/, 'Found "created", like: 2015-01-19T13:59:02Z');
        like($instance->{mimetype}, qr|\w+/\w+|, 'Found valid mimetype');
        like($instance->{size}, qr|^\d+$|, 'Got an number of bytes');
        like($instance->{checksum}, qr|^md5:[a-f0-9]{32}$|, 'Got a valid md5 string');

    });
}

=head2 get_case_structure

Make sure we always retrieve a single case object

=cut


sub get_case_structure {
    my $self    = shift;
    my $json    = shift;
    my $opts    = shift || {};

    my $perl    = $self->_get_json_as_perl($json);
    $self->validate_zsapi_structure($perl);


    my $case    = $perl->{result}->{instance};
    if ($opts->{set}) {
        $case   = $perl->{result}->{instance}->{rows}->[0]->{instance};
    }

    ### Validate given attributes
    return $case;
}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
