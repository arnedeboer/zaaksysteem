package TestFor::Catalyst::Controller::API::V1::Case;
use base qw(ZSTest::Catalyst TestFor::Catalyst::Controller::API::V1::Roles::TestPreperation);

use Moose;
use TestSetup;

use File::Copy qw(cp);
use File::Temp qw(tempfile);


=head1 NAME

TestFor::Catalyst::Controller:API::V1::Case - Proves the boundaries of our API: Case

=head1 SYNOPSIS

    See USAGE tests

    Quick test of this single file from within vagrant
    ZS_DISABLE_STUF_PRELOAD=1 ./zs_prove -v t/lib/TestFor/Catalyst/Controller/API/V1/Case.pm

=head1 DESCRIPTION

These tests prove the interactions between the outside servicebus and our zaaksysteem. This test
uses the version 1 API of zaaksysteem, and proves the C<api/v1/case> namespace.

=head1 USAGE 

Usage tests, use these if you would like to know how to interact with the API, it's also useful
for extended documentation of the StUF API.

=head2 Retrieval

=head3 List cases

    # curl --anyauth -k -H "Content-Type: application/json" --digest -u "zaaksysteem:abcdefghijklmnop123qrs" https://localhost/api/v1/case

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-d9db3d-199919",
   "result" : {
      "instance" : {
         "pager" : {
            "next" : null,
            "page" : 1,
            "pages" : 1,
            "prev" : null,
            "rows" : 2,
            "total_rows" : 2
         },
         "rows" : [
            {
               "instance" : {
                  "attributes" : {
                     "checkbox_agreed" : [
                        null
                     ],
                     "test_comment" : [
                        null
                     ],
                     "test_conclusion" : [
                        null
                     ],
                     "test_decision" : [
                        null
                     ],
                     "test_paspoort" : [
                        null
                     ]
                  },
                  "casetype" : {
                     "reference" : "5767c5e1-e666-4acc-908e-a61693ef96f8",
                     "type" : "casetype"
                  },
                  "date_of_registration" : "2015-03-04T11:49:03Z",
                  "date_target" : "2015-03-09T11:49:03Z",
                  "id" : "17276cbd-505d-405b-b89b-c37c72d47ed3",
                  "number" : 577,
                  "phase" : "afhandelfase",
                  "result" : null,
                  "status" : "new",
                  "subject_external" : null
               },
               "reference" : "17276cbd-505d-405b-b89b-c37c72d47ed3",
               "type" : "case"
            },
            {
               "instance" : {
                  "attributes" : {
                     "checkbox_agreed" : [
                        null
                     ],
                     "test_comment" : [
                        null
                     ],
                     "test_conclusion" : [
                        null
                     ],
                     "test_decision" : [
                        null
                     ],
                     "test_paspoort" : [
                        null
                     ]
                  },
                  "casetype" : {
                     "reference" : "5767c5e1-e666-4acc-908e-a61693ef96f8",
                     "type" : "casetype"
                  },
                  "date_of_registration" : "2015-03-04T11:49:03Z",
                  "date_target" : "2015-03-09T11:49:03Z",
                  "id" : "db5a29b6-7bc0-4149-b646-77a2e5e39861",
                  "number" : 578,
                  "phase" : "afhandelfase",
                  "result" : null,
                  "status" : "new",
                  "subject_external" : null
               },
               "reference" : "db5a29b6-7bc0-4149-b646-77a2e5e39861",
               "type" : "case"
            }
         ]
      },
      "reference" : null,
      "type" : "set"
   },
   "status_code" : 200
}

=end javascript

=cut

sub cat_api_v1_case_list : Tests {
    my $self = shift;

    $zs->zs_transaction_ok(sub {
        my $mech = $zs->mech;
        my $casetype_data = $self->api_test_zaaktype;
        my $casetype      = $casetype_data->{casetype};

        $self->_create_api_interface;
        # $zs->create_case_ok;

        $zs->create_case_ok(zaaktype => $casetype) for (1..2);
        $mech->zs_login;

        my $r       = $mech->get('/api/v1/case');
        my $json    = $r->content;

        $self->validate_case_structure($json, { set => 1 });
    });
}

=head3 Get a single case

    # curl --anyauth -k -H "Content-Type: application/json" --digest -u "zaaksysteem:abcdefghijklmnop123qrs" https://localhost/api/v1/case/d7c73f04-5fcc-4c03-b089-b5fc760a08d5

Retrieve a single case

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-6e212f-20ef20",
   "result" : {
      "instance" : {
         "attributes" : {
            "checkbox_agreed" : [
               null
            ],
            "test_comment" : [
               "This is a comment"
            ],
            "test_conclusion" : [
               null
            ],
            "test_decision" : [
               null
            ],
            "test_paspoort" : [
               null
            ]
         },
         "casetype" : {
            "reference" : "4a8904ef-0920-40a8-a160-4a814273890d",
            "type" : "casetype"
         },
         "date_of_registration" : "2015-03-04T11:49:53Z",
         "date_target" : "2015-03-09T11:49:53Z",
         "id" : "1f972123-a6d3-4d7b-85cd-1526e87a6525",
         "number" : 579,
         "phase" : "afhandelfase",
         "result" : null,
         "status" : "new",
         "subject_external" : null
      },
      "reference" : "1f972123-a6d3-4d7b-85cd-1526e87a6525",
      "type" : "case"
   },
   "status_code" : 200
}

=end javascript

=cut

sub cat_api_v1_case_get : Tests {
    my $self    = shift;

    $zs->zs_transaction_ok(sub {
        my $mech = $zs->mech;
        my $casetype_data = $self->api_test_zaaktype;
        my $casetype      = $casetype_data->{casetype};

        $self->_create_api_interface;
        my $case = $zs->create_case_ok(
          zaaktype => $casetype,
          kenmerken => [
              { $casetype_data->{kenmerken}->[1]->bibliotheek_kenmerken_id->id => 'This is a comment' },
          ]
        );

        $mech->zs_login;

        ok($case->object_data->uuid, 'Got case uuid');

        my $r       = $mech->get('/api/v1/case/' . $case->object_data->uuid);
        my $json    = $r->content;

        my $c_json  = $self->validate_case_structure($json);

        is($c_json->{id}, $case->object_data->uuid, 'UUID of get matches with retrieved object: ' . $c_json->{id});
    });
}

=head3 Create a single case

    # curl --anyauth -k -H "Content-Type: application/json" --data @file_with_request_json --digest -u "zaaksysteem:abcdefghijklmnop123qrs" https://localhost/api/v1/case/create

Create a case

B<Request>

=begin javascript

{
   "casetype_id" : "a8466f23-1bd3-4dd7-943c-38b23d648fb0",
   "requestor" : {
      "id" : "12345678",
      "type" : "person"
   },
   "source" : "webformulier",
   "values" : {
      "test_comment" : [
         "This is telling you: welcome to our api"
      ]
   }
}

=end javascript

B<Response>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-f0a833-ea87dc",
   "result" : {
      "instance" : {
         "attributes" : {
            "checkbox_agreed" : [
               null
            ],
            "test_comment" : [
               "This is telling you: welcome to our api"
            ],
            "test_conclusion" : [
               null
            ],
            "test_decision" : [
               null
            ],
            "test_paspoort" : [
               null
            ]
         },
         "casetype" : {
            "reference" : "a8466f23-1bd3-4dd7-943c-38b23d648fb0",
            "type" : "casetype"
         },
         "date_of_registration" : "2015-03-04T11:52:24Z",
         "date_target" : "2015-03-09T11:52:24Z",
         "id" : "2773480d-bc45-4dca-b351-490f5660b469",
         "number" : 580,
         "phase" : "afhandelfase",
         "result" : null,
         "status" : "new",
         "subject_external" : null
      },
      "reference" : "2773480d-bc45-4dca-b351-490f5660b469",
      "type" : "case"
   },
   "status_code" : 200
}

=end javascript

=cut

sub cat_api_v1_case_create : Tests {
    my $self    = shift;

    $zs->zs_transaction_ok(sub {
        my $mech = $zs->mech;

        $self->_create_api_interface;
        my $casetype_data = $self->api_test_zaaktype;
        my $zt            = $casetype_data->{casetype};
        my $person  = $zs->create_natuurlijk_persoon_ok;

        $mech->zs_login;

        ### Previous transaction
        my $last_transaction = $schema->resultset('Transaction')->search({}, {order_by => { '-desc' => 'id' }, rows => 1})->first;

        $mech->post_json(
            $mech->zs_url_base . '/api/v1/case/create',
            {
                casetype_id => $zt->_object->uuid,
                source      => 'webformulier',
                requestor   => {
                    type    => 'person',
                    id      => $person->burgerservicenummer,
                },
                values => {
                    test_comment => ['This is telling you: welcome to our api']
                }
            }
        );

        my $json    = $mech->content;

        my $c_json    = $self->validate_case_structure($json);


        ### Check attributes
        my $attributes = $c_json->{attributes};
        isa_ok($attributes, 'HASH', 'Got hashref of attributes');
        is_deeply($attributes, { test_decision => [undef], test_paspoort => [undef], test_conclusion => [undef], checkbox_agreed => [undef], test_comment => ['This is telling you: welcome to our api'] }, 'Found given attributes in created case');

        ### Post call, lets find a transaction;
        my $transaction = $schema->resultset('Transaction')->search($last_transaction ? {id => { '>' => $last_transaction->id }} : {})->first;

        ok($transaction, 'Found a transaction in the transactionlog');
        like($transaction->input_data, qr/Client IP: \d+\.\d+\.\d+\.\d+/m, 'Got IP address in transaction log');
        like($transaction->input_data, qr/Request method: [postget]+/m, 'Got request method in transaction log');
        like($transaction->input_data, qr/Request API call: api\/v1/m, 'Got request call in transaction log');
        like($transaction->input_data, qr/Request body:\n{/m, 'Got request body in transaction log');

    }, 'api/v1/case/create: create a simple case');

    $zs->zs_transaction_ok(sub {
        my $mech = $zs->mech;

        $self->_create_api_interface;
        my $casetype_data = $self->api_test_zaaktype();
        my $zt            = $casetype_data->{casetype};
        my $company       = $zs->create_bedrijf_ok;

        $mech->zs_login;

        $mech->post_json(
            $mech->zs_url_base . '/api/v1/case/create',
            {
                casetype_id => $zt->_object->uuid,
                source      => 'webformulier',
                requestor   => {
                    type          => 'company',
                    id            => {
                        kvk_number    => $company->dossiernummer,
                        branch_number => $company->vestigingsnummer,
                    }
                },
                values => {
                    test_comment => ['This is telling you: welcome to our api']
                }
            }
        );

        my $json    = $mech->content;

        my $c_json    = $self->validate_case_structure($json);
    }, 'api/v1/case/create: create a case with company');

    $zs->zs_transaction_ok(sub {
        my $mech = $zs->mech;

        $self->_create_api_interface;
        my $casetype_data = $self->api_test_zaaktype({preset_client => 1});
        my $zt            = $casetype_data->{casetype};

        $mech->zs_login;

        $mech->post_json(
            $mech->zs_url_base . '/api/v1/case/create',
            {
                casetype_id => $zt->_object->uuid,
                source      => 'webformulier',
                values => {
                    test_comment => ['This is telling you: welcome to our api']
                }
            }
        );

        my $json    = $mech->content;

        my $c_json    = $self->validate_case_structure($json);
    }, 'api/v1/case/create: create a case without given person/company');

    $zs->zs_transaction_ok(sub {
        my $mech = $zs->mech;

        $self->_create_api_interface;
        my $casetype_data = $self->api_test_zaaktype;
        my $zt            = $casetype_data->{casetype};
        my $person  = $zs->create_natuurlijk_persoon_ok;

        $mech->zs_login;

        ### Upload a file
        $mech->post_file(
            $mech->zs_url_base . '/api/v1/case/prepare_file',
            $zs->config->{filestore_test_file_path}
        );

        my $json      = $mech->content;
        my $perl      = $self->_get_json_as_perl($json);

        my ($first_uuid)  = %{ $perl->{result}->{instance}->{references} };

        $mech->post_json(
            $mech->zs_url_base . '/api/v1/case/create',
            {
                casetype_id => $zt->_object->uuid,
                source      => 'webformulier',
                requestor   => {
                    type    => 'person',
                    id      => $person->burgerservicenummer,
                },
                values => {
                    test_comment  => ['This is telling you: welcome to our api'],
                    test_paspoort => [$first_uuid],
                }
            }
        );

        $json         = $mech->content;

        my $c_json    = $self->validate_case_structure($json);

        ### Check attributes
        my $attributes = $c_json->{attributes};
        isa_ok($attributes, 'HASH', 'Got hashref of attributes');
        is_deeply($attributes, { test_decision => [undef], test_paspoort => [[$first_uuid]], test_conclusion => [undef], checkbox_agreed => [undef], test_comment => ['This is telling you: welcome to our api'] }, 'Found given attributes in created case');
    }, 'api/v1/case/create: create a case with attached document');
}

=head3 Update a single case

    # curl --anyauth -k -H "Content-Type: application/json" --data @file_with_request_json --digest -u "zaaksysteem:abcdefghijklmnop123qrs" https://localhost/api/v1/case/9e7ab012-ffee-45e2-a6ae-974c91312967/update

Update a case

B<Request>

=begin javascript

{
   "values" : {
      "test_conclusion" : [
         "This is a conclusion"
      ]
   }
}

=end javascript

B<Response>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-62ff11-10ed03",
   "result" : {
      "instance" : {
         "attributes" : {
            "checkbox_agreed" : [
               null
            ],
            "test_comment" : [
               "This is telling you: welcome to our api"
            ],
            "test_conclusion" : [
               "This is a conclusion"
            ],
            "test_decision" : [
               null
            ],
            "test_paspoort" : [
               null
            ]
         },
         "casetype" : {
            "reference" : "92ccea09-62b0-4a36-9af7-8fbbf807918f",
            "type" : "casetype"
         },
         "date_of_registration" : "2015-03-04T10:53:43Z",
         "date_target" : "2015-03-09T10:53:43Z",
         "id" : "c0209d5e-0918-4336-b533-0a2b8a0207d2",
         "number" : 584,
         "phase" : "afhandelfase",
         "result" : null,
         "status" : "new",
         "subject_external" : null
      },
      "reference" : "c0209d5e-0918-4336-b533-0a2b8a0207d2",
      "type" : "case"
   },
   "status_code" : 200
}

=end javascript

=cut

sub cat_api_v1_case_update : Tests {
    my $self    = shift;

    $zs->zs_transaction_ok(sub {
        my $mech = $zs->mech;

        $self->_create_api_interface;
        my $casetype_data = $self->api_test_zaaktype;
        my $zt            = $casetype_data->{casetype};
        my $person  = $zs->create_natuurlijk_persoon_ok;

        $mech->zs_login;

        $mech->post_json(
            $mech->zs_url_base . '/api/v1/case/create',
            {
                casetype_id => $zt->_object->uuid,
                source      => 'webformulier',
                requestor   => {
                    type    => 'person',
                    id      => $person->burgerservicenummer,
                },
                values => {
                    test_comment => ['This is telling you: welcome to our api']
                }
            }
        );

        my $json      = $mech->content;
        my $perl      = $self->_get_json_as_perl($json);


        ### TODO:
        ### 1) Case where we try to change an attribute from a previous phase

        ### Now update se case
        $mech->post_json(
            $mech->zs_url_base . '/api/v1/case/' . $perl->{result}->{instance}->{id} . '/update',
            {
                values => {
                    test_conclusion => ['This is a conclusion']
                }
            }
        );

        $json      = $mech->content;
        my $c_json    = $self->validate_case_structure($json);


        ### Check attributes
        my $attributes = $c_json->{attributes};
        isa_ok($attributes, 'HASH', 'Got hashref of attributes');

        is_deeply(
            $attributes,
            {
                test_decision   => [undef],
                test_paspoort   => [undef],
                test_conclusion => ['This is a conclusion'],
                test_comment => ['This is telling you: welcome to our api'],
                checkbox_agreed => [undef],
            },
            'Succesfully updated an attribute'
        );
    }, '/api/v1/case/update: update simple');

    $zs->zs_transaction_ok(sub {
        my $mech = $zs->mech;

        $self->_create_api_interface;
        my $casetype_data = $self->api_test_zaaktype;
        my $zt            = $casetype_data->{casetype};
        my $person  = $zs->create_natuurlijk_persoon_ok;

        $mech->zs_login;

        $mech->post_json(
            $mech->zs_url_base . '/api/v1/case/create',
            {
                casetype_id => $zt->_object->uuid,
                source      => 'webformulier',
                requestor   => {
                    type    => 'person',
                    id      => $person->burgerservicenummer,
                },
                values => {
                    test_comment => ['This is telling you: welcome to our api']
                }
            }
        );

        my $json      = $mech->content;
        my $perl      = $self->_get_json_as_perl($json);


        ### TODO:
        ### 1) Case where we try to change an attribute from a previous phase

        ### Upload a file
        $mech->post_file(
            $mech->zs_url_base . '/api/v1/case/prepare_file',
            $zs->config->{filestore_test_file_path}
        );

        $json      = $mech->content;
        my $postperl      = $self->_get_json_as_perl($json);

        my ($first_uuid)  = %{ $postperl->{result}->{instance}->{references} };

        ### Now update se case
        $mech->post_json(
            $mech->zs_url_base . '/api/v1/case/' . $perl->{result}->{instance}->{id} . '/update',
            {
                values => {
                    test_decision => [$first_uuid],
                    test_conclusion => ['wow this works']
                }
            }
        );

        $json      = $mech->content;
        my $c_json    = $self->validate_case_structure($json);


        ### Check attributes
        my $attributes = $c_json->{attributes};
        isa_ok($attributes, 'HASH', 'Got hashref of attributes');

        is_deeply(
            $attributes,
            {
                test_decision   => [[$first_uuid]],
                test_paspoort   => [undef],
                test_conclusion => ['wow this works'],
                test_comment => ['This is telling you: welcome to our api'],
                checkbox_agreed => [undef],
            },
            'Succesfully updated an attribute'
        );
    }, '/api/v1/case/update: update with file');
}

=head3 Upload a document for later processing

    # curl --form upload=@localfilename --anyauth -k --digest -u "zaaksysteem:abcdefghijklmnop123qrs" https://localhost/api/v1/case/prepare_file

Update a case

B<Request>

Make sure you post a file 

=begin javascript

=end javascript

B<Response>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-21bf98-e068c3",
   "result" : {
      "instance" : {
         "references" : {
            "86a9485f-40cf-4b26-88f8-db2875cb9628" : "FilestoreTest.txt"
         }
      },
      "reference" : null,
      "type" : "filebag"
   },
   "status_code" : 200
}

=end javascript

=cut

sub cat_api_v1_case_prepare_file : Tests {
    my $self    = shift;

    ### Upload of a single file
    $zs->zs_transaction_ok(sub {
        my $mech = $zs->mech;

        ### Test Preperations
        $self->_create_api_interface;
        my $casetype_data = $self->api_test_zaaktype;
        my $zt            = $casetype_data->{casetype};
        my $person  = $zs->create_natuurlijk_persoon_ok;

        $mech->zs_login;

        ### Upload a file
        $mech->post_file(
            $mech->zs_url_base . '/api/v1/case/prepare_file',
            $zs->config->{filestore_test_file_path}
        );

        my $json      = $mech->content;
        my $perl      = $self->_get_json_as_perl($json);

        my $file      = $perl->{result}->{instance};

        is($perl->{result}->{type}, 'filebag', 'Result type is "filebag"');
        isa_ok($file->{references}, 'HASH', 'Got a hash of filestore uuids');

        ### Get first file reference
        my ($first_uuid)  = keys %{ $file->{references} };

        like($first_uuid, qr/^\w+\-\w+\-/, 'Got a valid UUID for the uploaded file');
        is($file->{references}->{$first_uuid}, $zs->config->{filestore_test_file_name}, 'Got a UUID for uploaded file: ' . $zs->config->{filestore_test_file_name});
    });
}

=head3 Moving a single case to a next phase

    # curl --anyauth -k -H "Content-Type: application/json" --data @file_with_request_json --digest -u "zaaksysteem:abcdefghijklmnop123qrs" https://localhost/api/v1/case/9e7ab012-ffee-45e2-a6ae-974c91312967/transition

Update a case

B<Request>

=begin javascript

{
   "result_id" : 1299
}

=end javascript

B<Response>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-9af744-ddec6e",
   "result" : {
      "instance" : {
         "attributes" : {
            "checkbox_agreed" : [
               null
            ],
            "test_comment" : [
               "This is telling you: welcome to our api"
            ],
            "test_conclusion" : [
               null
            ],
            "test_decision" : [
               null
            ],
            "test_paspoort" : [
               null
            ]
         },
         "casetype" : {
            "reference" : "af086984-ae66-49c3-b241-0d041dc8932f",
            "type" : "casetype"
         },
         "date_of_registration" : "2015-03-04T10:55:12Z",
         "date_target" : "2015-03-09T10:55:12Z",
         "id" : "5a977270-1372-46e2-9598-24398c3d131f",
         "number" : 586,
         "phase" : null,
         "result" : "afgebroken",
         "status" : "resolved",
         "subject_external" : null
      },
      "reference" : "5a977270-1372-46e2-9598-24398c3d131f",
      "type" : "case"
   },
   "status_code" : 200
}

=end javascript

=cut

sub cat_api_v1_case_transition : Tests {
    my $self    = shift;

    $zs->zs_transaction_ok(sub {
        my $mech = $zs->mech;

        $self->_create_api_interface;
        my $casetype_data = $self->api_test_zaaktype;
        my $zt            = $casetype_data->{casetype};
        my $person  = $zs->create_natuurlijk_persoon_ok;

        $mech->zs_login;

        ### First create a case
        $mech->post_json(
            $mech->zs_url_base . '/api/v1/case/create',
            {
                casetype_id => $zt->_object->uuid,
                source      => 'webformulier',
                requestor   => {
                    type    => 'person',
                    id      => $person->burgerservicenummer,
                },
                values => {
                    test_comment => ['This is telling you: welcome to our api']
                }
            }
        );

        my $json        = $mech->content;
        my $perl        = $self->_get_json_as_perl($json);

        ### Retrieve info about casetype, so we know the result ids
        $mech->get('/api/v1/casetype/' . $perl->{result}->{instance}->{casetype}->{reference});
        $json           = $mech->content;
        my $ztperl      = $self->_get_json_as_perl($json);

        ### Get first resultID:
        my $first_id    = $ztperl->{result}->{instance}->{results}->[0]->{id};

        ### Now transition this case
        $mech->post_json(
            $mech->zs_url_base . '/api/v1/case/' . $perl->{result}->{instance}->{id} . '/transition',
            {
                result_id => $first_id,
            }
        );

        $json      = $mech->content;
        my $c_json    = $self->validate_case_structure($json);

        is($c_json->{result}, $ztperl->{result}->{instance}->{results}->[0]->{type}, 'Case result: Got correct result');
        is($c_json->{phase}, undef, 'Case phase: undef (resolved)');
        is($c_json->{status}, 'resolved', 'Case status: resolved');

    });
}


=head2 validate_case_structure

Make sure we always retrieve a valid response

B<Testing the following information>

=begin javascript

{
  "instance" : {
     "attributes" : {
        "checkbox_agreed" : [
           null
        ],
        "test_comment" : [
           "This is a comment"
        ],
        "test_conclusion" : [
           null
        ],
        "test_decision" : [
           null
        ],
        "test_paspoort" : [
           null
        ]
     },
     "casetype" : {
        "reference" : "4a8904ef-0920-40a8-a160-4a814273890d",
        "type" : "casetype"
     },
     "date_of_registration" : "2015-03-04T11:49:53Z",
     "date_target" : "2015-03-09T11:49:53Z",
     "id" : "1f972123-a6d3-4d7b-85cd-1526e87a6525",
     "number" : 579,
     "phase" : "afhandelfase",
     "result" : null,
     "status" : "new",
     "subject_external" : null
  },
  "reference" : "1f972123-a6d3-4d7b-85cd-1526e87a6525",
  "type" : "case"
},

=end javascript

=cut

my @MINIMAL_CASE_KEYS = qw/
    attributes
    casetype_id
    date_of_registration
    date_target
    number
    phase
    result
    status
    subject_external
/;

sub validate_case_structure {
    my $self    = shift;
    my $json    = shift;
    my $opts    = shift || {};

    my $perl    = $self->_get_json_as_perl($json);
    $self->validate_zsapi_structure($perl);


    my $case    = $perl->{result}->{instance};
    if ($opts->{set}) {
        is($perl->{result}->{type}, 'set', 'Got a set of data, multiple results');
        ok($perl->{result}->{instance}->{$_}, 'Got a set of data, column: ' . $_) for qw/rows pager/;
        ok($perl->{result}->{instance}->{pager}, 'Got a set of pager, column: ' . $_) for qw/next page pages prev rows total_rows/;
        isa_ok($perl->{result}->{instance}->{rows}, 'ARRAY', 'Got a set of data, got an array of rows');

        $case   = $perl->{result}->{instance}->{rows}->[0]->{instance};

        like($perl->{result}->{instance}->{rows}->[0]->{reference}, qr/^\w+\-\w+\-/, 'Got a "reference", like "f87e9f29-5720-4743-9cc5-47887b342709"');
    } else {
      like($perl->{result}->{reference}, qr/^\w+\-\w+\-/, 'Got a "reference", like "f87e9f29-5720-4743-9cc5-47887b342709"');
    }
   

    like($case->{id}, qr/^\w+\-\w+\-/, 'Got a "id", like "f87e9f29-5720-4743-9cc5-47887b342709"');

    ok(exists $case->{$_}, "Found the required key: $_") for @MINIMAL_CASE_KEYS;

    ### Required values:
    ok(ref $case->{attributes} eq 'HASH', 'Found a hash of custom_field attributes');
    like($case->{casetype}->{reference},qr/[\w]+\-[\w]+/, 'Found "casetype_id", like: 41460056-d747-4f07-8f24-98e02b31fb09');
    like($case->{date_of_registration}, qr/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}Z$/, 'Found "date_of_registration", like: 2015-01-19T13:59:02Z');
    like($case->{date_target}, qr/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}Z$/, 'Found "date_target", like: 2015-01-19T13:59:02Z');
    like($case->{number}, qr/^\d+$/, 'Found "number", like: 4');
    ok(exists $case->{phase}, 'Found "phase"');
    ok(exists $case->{result}, 'Got a "result"');
    like($case->{status}, qr/^(?:new|open|resolved|deleted|overdragen)$/, 'Got a "status", like "new","open","resolved","deleted","overdragen"');
    ok(exists $case->{subject_external}, 'Got a "subject_external"');

    ### Validate given attributes
    return $case;
}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
