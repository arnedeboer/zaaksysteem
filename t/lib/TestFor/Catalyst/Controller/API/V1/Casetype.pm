package TestFor::Catalyst::Controller::API::V1::Casetype;
use base qw(ZSTest::Catalyst TestFor::Catalyst::Controller::API::V1::Roles::TestPreperation);

use Moose;
use TestSetup;

# use Zaaksysteem::TestMechanize;


=head1 NAME

TestFor::Catalyst::Controller:API::V1::Casetype - Proves the boundaries of our API: Casetype

=head1 SYNOPSIS

    See USAGE tests

=head1 DESCRIPTION

These tests prove the interactions between the outside servicebus and our zaaksysteem. This test
uses the version 1 API of zaaksysteem, and proves the C<api/v1/casetype> namespace.

=head1 USAGE 

Usage tests, use these if you would like to know how to interact with the API, it's also useful
for extended documentation of the StUF API.

=head2 Retrieval

=head3 List casetypes

    # curl --anyauth -k -H "Content-Type: application/json" --digest -u "zaaksysteem:abcdefghijklmnop123qrs" https://localhost/api/v1/casetype

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-c74da2-399342",
   "result" : {
      "instance" : {
         "pager" : {
            "next" : null,
            "page" : 1,
            "pages" : 1,
            "prev" : null,
            "rows" : 4,
            "total_rows" : 4
         },
         "rows" : [
            {
               "instance" : {
                  "id" : "a2a198b8-442e-48db-b793-cd617acf2d64",
                  "phases" : [
                     {
                        "fields" : [
                           {
                              "id" : "checkbox_agreed",
                              "label" : "IFke9NDfe3",
                              "multiple_values" : true,
                              "required" : false,
                              "type" : "checkbox",
                              "values" : [
                                 {
                                    "active" : 1,
                                    "id" : 707,
                                    "sort_order" : 707,
                                    "value" : "agreed"
                                 }
                              ]
                           },
                           {
                              "id" : "test_comment",
                              "label" : "DPdj7MJnr8",
                              "multiple_values" : false,
                              "required" : false,
                              "type" : "text",
                              "values" : []
                           },
                           {
                              "id" : "test_paspoort",
                              "label" : "GTpr4XPtj7",
                              "multiple_values" : false,
                              "required" : false,
                              "type" : "file",
                              "values" : []
                           }
                        ],
                        "name" : "registratiefase",
                        "seq" : 1
                     },
                     {
                        "fields" : [
                           {
                              "id" : "test_conclusion",
                              "label" : "HXju6TZry2",
                              "multiple_values" : false,
                              "required" : false,
                              "type" : "text",
                              "values" : []
                           },
                           {
                              "id" : "test_decision",
                              "label" : "IOmc0ZHdh3",
                              "multiple_values" : false,
                              "required" : false,
                              "type" : "file",
                              "values" : []
                           }
                        ],
                        "name" : "afhandelfase",
                        "seq" : 2
                     }
                  ],
                  "results" : [
                     {
                        "id" : 1309,
                        "label" : "FBgd7LBxy8",
                        "type" : "afgebroken"
                     },
                     {
                        "id" : 1308,
                        "label" : "TLit2TCxu9",
                        "type" : "verwerkt"
                     }
                  ],
                  "sources" : [
                     "behandelaar",
                     "balie",
                     "telefoon",
                     "post",
                     "email",
                     "webformulier"
                  ],
                  "title" : "Paspoort aanvraag"
               },
               "reference" : "a2a198b8-442e-48db-b793-cd617acf2d64",
               "type" : "casetype"
            },
            {
               "instance" : {
                  "id" : "e6f9055a-54c5-43a3-8a77-bf02fbba6d43",
                  "phases" : [ ...],
                  "results" : [ ... ],
                  "sources" : [ ... ],
                  "title" : "Proef zaaktype"
               },
               "reference" : "e6f9055a-54c5-43a3-8a77-bf02fbba6d43",
               "type" : "casetype"
            },
            [...],
         ]
      }
      "reference" : null,
      "type" : "set"
   },
   "status_code" : 200
}


=end javascript

=cut

sub cat_api_v1_casetype_list : Tests {
    my $self = shift;

    $zs->zs_transaction_ok(sub {
        my $mech        = $zs->mech;
        $self->_create_api_interface;

        ### Build 3 casetypes
        $self->api_test_zaaktype for (1..3);

        ### Build another to work with
        my $casetype_data = $self->api_test_zaaktype;
        my $casetype      = $casetype_data->{casetype};

        $mech->zs_login;

        my $r       = $mech->get('/api/v1/casetype');
        my $json    = $r->content;

        ### Verify the first casetype
        $self->validate_casetype_structure($json, { set => 1 });

        my $perl    = $self->_get_json_as_perl($json);
    });
}

=head3 Get a single casetype

    # curl --anyauth -k -H "Content-Type: application/json" --digest -u "zaaksysteem:abcdefghijklmnop123qrs" https://localhost/api/v1/casetype/d7c73f04-5fcc-4c03-b089-b5fc760a08d5

Retrieve a single casetype

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-fa32cc-c91de4",
   "result" : {
      "instance" : {
         "id" : "5c09dee2-d782-45ed-8263-ffcceab37ce8",
         "phases" : [
            {
               "fields" : [
                  {
                     "id" : "checkbox_agreed",
                     "label" : "GKje6UGrb2",
                     "multiple_values" : true,
                     "required" : false,
                     "type" : "checkbox",
                     "values" : [
                        {
                           "active" : 1,
                           "id" : 711,
                           "sort_order" : 711,
                           "value" : "agreed"
                        }
                     ]
                  },
                  {
                     "id" : "test_comment",
                     "label" : "NCvz2TJkr0",
                     "multiple_values" : false,
                     "required" : false,
                     "type" : "text",
                     "values" : []
                  },
                  {
                     "id" : "test_paspoort",
                     "label" : "NLoi6XDsr4",
                     "multiple_values" : false,
                     "required" : false,
                     "type" : "file",
                     "values" : []
                  }
               ],
               "name" : "registratiefase",
               "seq" : 1
            },
            {
               "fields" : [
                  {
                     "id" : "test_conclusion",
                     "label" : "MOfq7PWba8",
                     "multiple_values" : false,
                     "required" : false,
                     "type" : "text",
                     "values" : []
                  },
                  {
                     "id" : "test_decision",
                     "label" : "DPil0VCbo9",
                     "multiple_values" : false,
                     "required" : false,
                     "type" : "file",
                     "values" : []
                  }
               ],
               "name" : "afhandelfase",
               "seq" : 2
            }
         ],
         "results" : [
            {
               "id" : 1317,
               "label" : "UHsa6FDxm1",
               "type" : "afgebroken"
            },
            {
               "id" : 1316,
               "label" : "KFat9GTdi1",
               "type" : "verwerkt"
            }
         ],
         "sources" : [
            "behandelaar",
            "balie",
            "telefoon",
            "post",
            "email",
            "webformulier"
         ],
         "title" : "Proefzaaktype"
      },
      "reference" : "5c09dee2-d782-45ed-8263-ffcceab37ce8",
      "type" : "casetype"
   },
   "status_code" : 200
}

=end javascript

=cut

sub cat_api_v1_casetype_get : Tests {
    my $self    = shift;

    $zs->zs_transaction_ok(sub {
        my $mech = $zs->mech;
        $self->_create_api_interface;

        my $casetype_data = $self->api_test_zaaktype;
        my $casetype      = $casetype_data->{casetype};

        $mech->zs_login;

        ok($casetype->_object->uuid, 'Got casetype uuid');

        my $r       = $mech->get('/api/v1/casetype/' . $casetype->_object->uuid);
        my $json    = $r->content;

        my $c_json  = $self->validate_casetype_structure($json);

        is($c_json->{id}, $casetype->_object->uuid, 'UUID of get matches with retrieved object: ' . $c_json->{id});
    });
}


=head2 validate_casetype_structure

Make sure we always retrieve a valid response

B<Testing the following information>

=begin javascript

{
  "instance" : {
     "id" : "5c09dee2-d782-45ed-8263-ffcceab37ce8",
     "phases" : [
        {
           "fields" : [
              {
                 "id" : "checkbox_agreed",
                 "label" : "GKje6UGrb2",
                 "multiple_values" : true,
                 "required" : false,
                 "type" : "checkbox",
                 "values" : [
                    {
                       "active" : 1,
                       "id" : 711,
                       "sort_order" : 711,
                       "value" : "agreed"
                    }
                 ]
              },
              {
                 "id" : "test_comment",
                 "label" : "NCvz2TJkr0",
                 "multiple_values" : false,
                 "required" : false,
                 "type" : "text",
                 "values" : []
              },
              {
                 "id" : "test_paspoort",
                 "label" : "NLoi6XDsr4",
                 "multiple_values" : false,
                 "required" : false,
                 "type" : "file",
                 "values" : []
              }
           ],
           "name" : "registratiefase",
           "seq" : 1
        },
        {
           "fields" : [
              {
                 "id" : "test_conclusion",
                 "label" : "MOfq7PWba8",
                 "multiple_values" : false,
                 "required" : false,
                 "type" : "text",
                 "values" : []
              },
              {
                 "id" : "test_decision",
                 "label" : "DPil0VCbo9",
                 "multiple_values" : false,
                 "required" : false,
                 "type" : "file",
                 "values" : []
              }
           ],
           "name" : "afhandelfase",
           "seq" : 2
        }
     ],
     "results" : [
        {
           "id" : 1317,
           "label" : "UHsa6FDxm1",
           "type" : "afgebroken"
        },
        {
           "id" : 1316,
           "label" : "KFat9GTdi1",
           "type" : "verwerkt"
        }
     ],
     "sources" : [
        "behandelaar",
        "balie",
        "telefoon",
        "post",
        "email",
        "webformulier"
     ],
     "title" : "Proefzaaktype"
  },
  "reference" : "5c09dee2-d782-45ed-8263-ffcceab37ce8",
  "type" : "casetype"
},


=end javascript

=cut

my @MINIMAL_CASETYPE_KEYS = qw/
    phases
    results
    sources
    title
/;

sub validate_casetype_structure {
    my $self    = shift;
    my $json    = shift;
    my $opts    = shift || {};

    my $perl    = $self->_get_json_as_perl($json);

    $self->validate_zsapi_structure($perl);

    ### First case:
    my $casetype    = $perl->{result}->{instance};
    if ($opts->{set}) {
        is($perl->{result}->{type}, 'set', 'Got a set of data, multiple results');
        ok($perl->{result}->{instance}->{$_}, 'Got a set of data, column: ' . $_) for qw/rows pager/;
        ok($perl->{result}->{instance}->{pager}, 'Got a set of pager, column: ' . $_) for qw/next page pages prev rows total_rows/;
        isa_ok($perl->{result}->{instance}->{rows}, 'ARRAY', 'Got a set of data, got an array of rows');

        $casetype = $perl->{result}->{instance}->{rows}->[0]->{instance};
    }

    ok(exists $casetype->{$_}, "Found the required key: $_") for @MINIMAL_CASETYPE_KEYS;

    like($casetype->{id}, qr/^\w+\-\w+\-/, 'Got a "id", like "f87e9f29-5720-4743-9cc5-47887b342709"');


    isa_ok($casetype->{phases}, 'ARRAY', 'Got a list of phases');
    isa_ok($casetype->{sources}, 'ARRAY', 'Got a list of sources');
    isa_ok($casetype->{results}, 'ARRAY', 'Got a list of sources');
    ok(@{ $casetype->{phases} } >= 2, 'Got a minimum of two phases');
    ok(@{ $casetype->{sources} } >= 1, 'Got a minimum of one source, like "webformulier" or "behandelaar"');
    ok(@{ $casetype->{results} } >= 1, 'Got a minimum of one result');

    ### Check phases
    for my $phase ( @{ $casetype->{phases} }) {
        ok($phase->{name}, 'Got a "name" for this phase, called: ' . $phase->{name});
        ok($phase->{seq}, 'Got a sort order "seq" for this phase');
        isa_ok($phase->{fields}, 'ARRAY', 'Got a list of fields for phase: ' . $phase->{name});

        for my $field (@{ $phase->{fields} }) {
            ok($field->{id}, 'Got an ID as attribute identifier for this field: ' . $field->{id});
            ok($field->{label}, 'Got a label for this field: ' . $field->{label} . ', field: ' . $field->{id});
            ok($field->{type}, 'Got a "type" for this field: ' . $field->{type} . ', field: ' . $field->{id});
            ok(exists $field->{required}, 'Got a "required" for this field: ' . ($field->{required} ? 'true' : 'false') . ', field: ' . $field->{id})
        }
    }

    ### Validate given attributes
    return $casetype;
}



1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
