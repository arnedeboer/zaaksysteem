package TestFor::Catalyst::Controller::API::V1::Roles::TestPreperation;

use TestSetup;
use Zaaksysteem::Object::Types::SavedSearch;



=head1 NAME

TestFor::Catalyst::Controller::API::V1::Roles::TestPreperation - Helper functions for API::V1 Testsuite

=head1 SYNOPSIS

    Please see:
        TestFor::Catalyst::Controller::API::V1::Case
        TestFor::Catalyst::Controller::API::V1::Casetype

=head1 DESCRIPTION

Helper functions for the testsuite of the V1 API Prove.

=head2 api_test_zaaktype

  $self->api_test_zaaktype

Will create a zaaktype to test our V1 API against.

B<Phases>

Two phases, C<registratiefase> and <afhandelfase>

=head3 registratiefase

B<Kenmerken>:

=over 4

=item checkbox_agreed

A checkbox where you can agree to something

=item test_comment

A textbox where you can place a comment

=back

=cut

sub api_test_zaaktype {
    my $self            = shift;
    my $opts            = shift || {};

    my $rv              = {};

    my $zaaktype_node   = $zs->create_zaaktype_node_ok;
    my $casetype        = $zs->create_zaaktype_ok(node => $zaaktype_node);

    ### Get gebruiker
    my $gebruiker       = $schema->resultset('Subject')->search({username => 'gebruiker'})->first;

    ### Add api_can_transition to node
    $zaaktype_node->properties->{api_can_transition} = 1;
    $zaaktype_node->properties->{preset_owner_identifier} = 'betrokkene-medewerker-' . $gebruiker->id;
    $zaaktype_node->properties($zaaktype_node->properties);
    $zaaktype_node->update;

    $rv->{phases}           = [
        (my $zaaktype_status = $zs->create_zaaktype_status_ok(
            status => 1,
            fase   => 'registratiefase',
            naam   => 'Geregistreerd',
            node   => $zaaktype_node
        )),
        (my $zaaktype_afhandelen = $zs->create_zaaktype_status_ok(
            status => 2,
            fase   => 'afhandelfase',
            naam   => 'Afgehandeld',
            node   => $zaaktype_node
        ))
    ];

    $zs->create_zaaktype_resultaat_ok(
        status    => $zaaktype_afhandelen,
        resultaat => 'verwerkt'
    );

    $zs->create_zaaktype_resultaat_ok(
        status    => $zaaktype_afhandelen,
        resultaat => 'afgebroken'
    );

    ### Create preset_client
    if ($opts->{preset_client}) {
        my $company = $zs->create_bedrijf_ok(vestigingsnummer => '125658798532', dossiernummer => '85645321');

        $zaaktype_node->zaaktype_definitie_id->preset_client('betrokkene-bedrijf-' . $company->id);
        $zaaktype_node->zaaktype_definitie_id->update;
    }


    ### Generate zaaktype_kenmerken, checkboxes
    my $zt_kenmerk = $zs->create_zaaktype_kenmerk_ok(
        status              => $zaaktype_status,
        publish_public      => 1, ### Important, else it won't show this kenmerk
        bibliotheek_kenmerk => $zs->create_bibliotheek_kenmerk_ok(
            naam            => 'checkbox_agreed',
            magic_string    => 'checkbox_agreed',
            value_type      => 'checkbox',
            values          => [
                {
                    value   => 'agreed',
                    active  => 1,
                }
            ]
        )
    );

    my $zt_kenmerk_test = $zs->create_zaaktype_kenmerk_ok(
        status              => $zaaktype_status,
        publish_public      => 1, ### Important, else it won't show this kenmerk
        bibliotheek_kenmerk => $zs->create_bibliotheek_kenmerk_ok(
            naam            => 'comment',
            label           => 'A comment to create this case with',
            magic_string    => 'test_comment',
            value_type      => 'text',
        )
    );

    my $zt_kenmerk_conclusion = $zs->create_zaaktype_kenmerk_ok(
        status              => $zaaktype_afhandelen,
        publish_public      => 1, ### Important, else it won't show this kenmerk
        bibliotheek_kenmerk => $zs->create_bibliotheek_kenmerk_ok(
            naam            => 'conclusion',
            label           => 'A conclusion to this casetype',
            magic_string    => 'test_conclusion',
            value_type      => 'text',
        )
    );

    my $zt_kenmerk_file = $zs->create_zaaktype_kenmerk_ok(
        status              => $zaaktype_status,
        publish_public      => 1, ### Important, else it won't show this kenmerk
        bibliotheek_kenmerk => $zs->create_bibliotheek_kenmerk_ok(
            naam            => 'paspoort',
            label           => 'Please attach a copy of your passport',
            magic_string    => 'test_paspoort',
            value_type      => 'file',
        )
    );

    my $zt_kenmerk_file2 = $zs->create_zaaktype_kenmerk_ok(
        status              => $zaaktype_afhandelen,
        publish_public      => 1, ### Important, else it won't show this kenmerk
        bibliotheek_kenmerk => $zs->create_bibliotheek_kenmerk_ok(
            naam            => 'besluit',
            label           => 'Please attach a copy the decision',
            magic_string    => 'test_decision',
            value_type      => 'file',
        )
    );

    $rv->{casetype}         = $casetype;
    $rv->{casetype_node_id} = $zaaktype_node;
    $rv->{kenmerken}        = [
        $zt_kenmerk,
        $zt_kenmerk_test,
        $zt_kenmerk_conclusion,
        $zt_kenmerk_file,
        $zt_kenmerk_file2
    ];


    return $rv;
}

sub _get_json_as_perl {
    my $self    = shift;
    my $json    = shift;

    my $jo      = JSON->new->utf8->pretty->canonical;

    return $jo->decode($json);
}


=head2 validate_zsapi_structure

Make sure we always return a valid base structure:

B<Testing the following information>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-4bae96-34a06d",
   "result" : {},
   "status_code" : 200
}


=end javascript

=cut

sub validate_zsapi_structure {
    my $self    = shift;
    my $perl    = shift;

    ok(exists $perl->{$_}, "Found root attribute:  $_") for qw/api_version status_code result request_id/;
}

=head2 _create_api_interface

Create an interface in our "koppelingen" module.

Attributes:

=over 4

=item version: 1

=item api_url: api/v1

=item medewerker: admin

=item access: rw

=item query: API

SELECT test_comment, checkbox_agreed FROM case

=back

=cut

sub _create_api_interface {
    my $self    = shift;


    my $jo      = JSON->new->utf8->pretty->canonical;

    my $search  = Zaaksysteem::Object::Types::SavedSearch->new(
        title     => 'API',
        query     => $jo->encode({
            zql => 'SELECT attribute.test_comment, case.documents, attribute.test_paspoort, attribute.test_decision, attribute.checkbox_agreed, attribute.test_conclusion FROM case'
        })
    );

    my $saved = $zs->object_model->save_object(object => $search);

    my $interface = $zs->create_interface_ok(
        module           => 'api',
        name             => 'Zaaksysteem API',
        interface_config => {
            api_key         => 'abcdefghijklmnop123qrs',
            access          => 'rw',
            api_url         => 'https://testsuite/api/v1',
            api_version     => 'v1',
            medewerker      => {
                username    => 'admin'
            },
            query_constraint => {
                id  => $saved->id,
            }
        },
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
