#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
use JSON;

### Test header end

my $now = DateTime->now;


$zs->zs_transaction_ok(sub {
    my $file = $zs->create_file_ok;

    throws_ok(sub {
        $file->update_properties({
            subject  => $zs->get_subject_ok,
            destroyed => 1});
    }, qr/Gebruiker heeft geen rechten om de prullenbak te legen/,
        'Fails when destroying file without explicit permissions');

    lives_ok(sub {
        $file->update_properties({
            subject  => $zs->get_subject_ok,
            user_has_zaak_beheer_permissions => 1,
            destroyed => 1});
        }
    ), "Is cool with destroying a file when given proper permissions";

}, 'Destroy a file');


$zs->zs_transaction_ok(sub {
    my $file = $zs->create_file_ok;
    my $case = $zs->create_case_ok;
    ok $file->update({case_id => undef}), 'Case ID unset';

    my $update = $file->update_properties({
        subject  => $zs->get_subject_ok,
        case_id  => $case->id,
    });
    ok $update, 'Updated file';
    is $update->case->id, $case->id, 'Has case set';

    my $logging = $schema->resultset('Logging')->search({
        zaak_id => $case->id,
        event_type => 'case/document/assign',
        component => 'document',
        component_id => $update->id,
    });

    is $logging->count, 1, "Found assign logging entry";

    my $log = $logging->first;
    ok $log->onderwerp =~ m/toegevoegd aan wachtrij/, "Logline formatting";

}, 'update_properties assign case check logging');


$zs->zs_transaction_ok(sub {
    my $file = $zs->create_file_ok(db_params => { case => $zs->create_case_ok() });
    ok $file->case_id, 'Has case ID set';

    my $reason = "BECAUSE IT'S WRONG";
    my $result = $file->update_properties({
        subject   => $zs->get_subject_ok,
        accepted  => 0,
        rejection_reason => "BECAUSE IT'S WRONG",
    });
    ok $result, 'Updated file';
    is $result->rejection_reason, $reason, 'Rejection reason set';
    ok $result->case_id, 'Case ID not removed';
    ok $result->date_deleted, 'Deleted set';
    is $result->accepted, 0, 'Accepted set to false';

    my $logging = $schema->resultset('Logging')->search({
        zaak_id => $result->case_id->id,
        event_type => 'case/document/reject',
        component => 'document',
        component_id => $result->id,
    });

    ok $logging->count == 1, "one logging line added";
    my $log = $logging->first;

    my $event_data = from_json($log->event_data);
    ok ref $event_data eq 'HASH', "Event data is hash";
    ok $event_data->{rejection_reason} eq $reason, "Reason has been stored correctly";

    my $expected_onderwerp = $result->reject_to_queue ?
        'Document "'. $result->filename .'" geweigerd en teruggestuurd naar de documentintake.' :
        'Document "' . $result->filename . '" geweigerd en permanent verwijderd."';

    is $log->onderwerp, $expected_onderwerp . ' Reden: ' . $reason, "Log onderwerp well formatted";
}, 'update_properties reject file');


$zs->zs_transaction_ok(sub {
    my $file = $zs->create_file_ok(db_params => { case => $zs->create_case_ok() });
    my $name      = '!@&#-%^$_';
    my $subject   = $zs->get_subject_ok;

    my ($result) = $file->update_properties({
        name     => $name,
        accepted => 1,
        subject  => $subject,
    });

    ok $result, "Updated $result";
    is $result->name, '!@_#-%^$_', 'Name as expected';
    is $result->accepted, 1, 'Accepted is true';
    is $result->modified_by, $subject, 'Correct modified subject';
    ok($result->date_modified >= $now, 'Correct modified date');

}, 'update_properties');


$zs->zs_transaction_ok(sub {
    my $file = $zs->create_file_ok;

    throws_ok sub {
        $file->update_properties({
            subject  => $zs->get_subject_ok,
            accepted => 'DIT MAG NIET',
            case_id  => undef,
        });
    }, qr/Invalid options given: accepted/, 'invalid options fail';
}, 'update_properties with invalid options');


$zs->zs_transaction_ok(sub {
    my $file = $zs->create_file_ok;

    ok $file->update({date_deleted => $now}), 'Set deleted on file';

    throws_ok sub {
        $file->update_properties({
            subject  => $zs->get_subject_ok,
            case_id => undef,
        });
    }, qr/Cannot alter a deleted/, 'updating deleted fails';
}, 'update_properties on deleted file');


$zs->zs_transaction_ok(sub {
    my $case      = $zs->create_case_ok();
    my $file      = $zs->create_file_ok(db_params => { case => $case });
    my $directory = $zs->create_directory_ok(case => $case);

    my $case_document = $zs->create_case_document_ok(case => $case);

    $file->update({directory => $directory});
    $file->update_properties({
        case_document_ids => [$case_document->id],
        subject           => $zs->get_subject_ok,
    });

    ok $file->directory, 'File has directory set';
    is $file->case_documents->count, 1, 'case_document is set';

    my $subject = $zs->get_subject_ok;
    my $result  = $file->update_properties({
            subject  => $subject,
            deleted  => 1,
    });
    ok  $result, 'Updated file';
    ok($result->date_deleted >= $now, 'Date deleted set');
    is  $result->deleted_by, $subject, 'Correct subject';
    ok !$result->directory_id, 'Directory unset';
    ok !$result->case_documents->count, 'case_document unset';
}, 'update_properties delete file');


$zs->zs_transaction_ok(sub {
    my $file    = $zs->create_file_ok;
    my $subject = $zs->get_subject_ok;
    my $ctd     = $zs->create_case_document_ok;

    ok $ctd->update({
        pip => 0,
        publish_public => 1,
    }), 'PIP publish false, public publish true';

    ok $file->update({
        publish_pip => 1,
        publish_website => 0,
    }), 'Made sure PIP and website are opposite of case document default';

    $file->update_properties({
        subject           => $subject,
        case_document_ids => [$ctd->id],
    });
    my ($file_case_document) = $file->case_documents;
    is $file_case_document->case_document->id, $ctd->id, 'Case type document set';
    ok $file->publish_website, 'Publish website true';
    ok !$file->publish_pip, 'Publish PIP false';
}, 'update_properties set case_document');


$zs->zs_transaction_ok(sub {
    my $file    = $zs->create_file_ok;
    my $subject = $zs->get_subject_ok;
    my $ctd     = $zs->create_case_document_ok;

    ok $ctd->update({
        pip => 0,
        publish_public => 1,
    }), 'PIP publish false, public publish true';

    ok $file->update({
        publish_pip => 1,
        publish_website => 0,
    }), 'Made sure PIP and website are opposite of case document default';

    $file->update_properties({
        subject     => $subject,
        publish_pip => 1,
        case_document_ids => [$ctd->id]
    });

    ok $file->publish_website, 'Publish website true';
    ok $file->publish_pip, 'Publish PIP true';
}, 'update_properties set case_document - respect overrides for defaults');


$zs->zs_transaction_ok(sub {
    my $file    = $zs->create_file_ok;
    my $subject = $zs->get_subject_ok;
    my $ctd     = $zs->create_case_document_ok;

    $file->update_properties({
        subject           => $subject,
        case_document_ids => [$ctd->id],
    });
    my ($file_case_document) = $file->case_documents;
    is $file_case_document->case_document->id, $ctd->id, 'Case type document set';

    my $second_file = $zs->create_file_ok;

    $second_file->update_properties({
        subject           => $subject,
        case_document_ids => [$ctd->id],
    });

    my ($second_case_document) = $second_file->case_documents;

    is $second_case_document->case_document->id,
       $file_case_document->case_document->id,
       'Assiging same case document to other file works';

}, 'update_properties set case_document on existing');


$zs->zs_transaction_ok(sub {
    my $file    = $zs->create_file_ok;
    my $subject = $zs->get_subject_ok;
    my $ctd     = $zs->create_case_document_ok;

    $file->update_properties({
        subject           => $subject,
        case_document_ids => [$ctd->id],
    });
    my ($file_case_document) = $file->case_documents;
    is $file_case_document->case_document->id, $ctd->id, 'Case type document set';

    my $second_file = $zs->create_file_ok;

    $second_file->move_case_document({
        subject => $subject,
        from    => $file->id,
        case_document_id => $ctd->id,
    });

    ok !$file->case_documents->count, 'No case document on first file';
    is $second_file->case_documents->count, 1, 'Second file has case document set';
}, 'move case document to other file');


$zs->zs_transaction_ok(sub {
    my $file = $zs->create_file_ok;

    $file->update_properties({
        case_document_ids => [$zs->create_case_document_ok->id],
        subject           => $zs->get_subject_ok,
    });
    ok $file->case_documents->count, 'case_document_id is set';

    my $subject = $zs->get_subject_ok;
    my $result  = $file->update_properties({
        subject           => $subject,
        case_document_ids => [],
    });
    ok  $result, 'Updated file';
    ok !$result->case_documents->count, 'Case document unset';
}, 'update_properties unset case_document');


$zs->zs_transaction_ok(sub {
    my $file      = $zs->create_file_ok;
    my $directory = $zs->create_directory_ok;

    $file->update({directory => $directory});
    ok $file->directory, 'File has directory set';

    my $subject = $zs->get_subject_ok;
    my $delete  = $file->update_properties({
            subject  => $subject,
            deleted  => 1,
    });
    ok  $delete, 'Updated file to deleted';

    # Let a different subject restore
    my $second_subject = $zs->get_subject_ok;
    my $undelete  = $file->update_properties({
            subject  => $second_subject,
            deleted  => 0,
    });
    ok  $undelete, 'Updated file to not deleted';
    is  $undelete->date_deleted, undef, 'Deleted date removed';
    is  $undelete->deleted_by, undef, 'Deleted by undefined';
    ok !$undelete->directory_id, 'Directory still unset';
    is  $undelete->modified_by, $second_subject, 'Correct subject in modified_by';
}, 'update_properties restore file');


$zs->zs_transaction_ok(sub {
    my $file      = $zs->create_file_ok;
    my $directory = $zs->create_directory_ok;

    my $result = $file->update_properties({
        subject      => $zs->get_subject_ok,
        directory_id => $directory->id,
    });
    ok $result, 'Updated file';
    is $result->directory->id, $directory->id, 'Set proper directory';
}, 'update_properties add directory');


$zs->zs_transaction_ok(sub {
    my $file      = $zs->create_file_ok;
    my $directory = $zs->create_directory_ok;

    $file->update({directory_id => $directory});

    ok $file->directory, 'File has directory set';
    my $result = $file->update_properties({
        subject      => $zs->get_subject_ok,
        directory_id => undef,
    });
    ok $result, 'Updated file';
    is $result->directory_id, undef, 'No directory defined';
}, 'update_properties remove directory');


$zs->zs_transaction_ok(sub {
    my $file   = $zs->create_file_ok();
    my $result = $file->update_properties({
        case_id   => $zs->create_case_ok->id,
        subject   => $zs->get_subject_ok,
        accepted  => 1,
    });
    ok $result, 'Updated file';
    is $result->accepted, 1, 'Accepted file';

    # after accepting a file, a logging line must show up on the timeline.
    # the logging is filtered by Event::Context, so we're gonna invoke
    # that and see if it lets the accept-event pass.
    my $logging = $schema->resultset('Logging')->search({
        zaak_id => $result->case_id->id,
        component => 'document',
        component_id => $result->id,
        event_type => 'case/document/accept'
    })->search_events_by_context('case');

    ok $logging->count == 1, "Accept logging event found";

}, 'update_properties accept file');

$zs->zs_transaction_ok(sub {
    my $file   = $zs->create_file_ok;
    ok $file->update({publish_pip => 1}), 'Set publish_pip to true';
    my $result = $file->update_properties({
        case_id   => $zs->create_case_ok->id,
        subject   => $zs->get_subject_ok,
        accepted  => 1,
    });
    ok $result, 'Updated file';
    is $result->accepted, 1, 'Accepted file';
}, 'update_properties accept file from PIP');


$zs->zs_transaction_ok(sub {
    my $file   = $zs->create_file_ok(db_params => { case => $zs->create_case_ok });
    my $reason = 'PIP reject';
    my $prep = $file->update_properties({
        publish_pip       => 1,
        case_document_ids => [$zs->create_case_document_ok->id],
        directory_id      => $zs->create_directory_ok,
        subject           => $zs->get_subject_ok,
    });
    ok $prep->publish_pip, 'publish_pip is true';
    ok $prep->case_documents->count, 'Case type document is set';
    ok $prep->directory, 'Directory is set';

    my $result = $file->update_properties({
        subject   => $zs->get_subject_ok,
        accepted  => 0,
        rejection_reason => $reason,
    });
    ok $result, 'Updated file';
    is $result->rejection_reason, $reason, 'Reason matches';
    is $result->case_id->id, $file->case->id, 'Case ID not removed';
    ok($result->date_deleted >= $now, 'File has deleted set');
    ok($result->date_deleted >= $now, 'File has deleted set');
    ok !$result->case_documents->count, 'Case type document removed';
    ok !$result->directory, 'Directory removed';
}, 'update_properties reject file from PIP');


$zs->zs_transaction_ok(sub {
    my $file   = $zs->create_file_ok;

    # By default a file doesn't get rejected to queue
    ok $file->update({reject_to_queue => 1}), 'Set reject_to_queue true';
    throws_ok sub {
        $file->discard_changes->update_properties({
            subject  => $zs->get_subject_ok,
            accepted => 0,
        })
    }, qr/Rejection reason required/, 'Declined file without reason';
}, 'update_properties reject file without reason');


$zs->zs_transaction_ok(sub {
    my $file = $zs->create_file_ok;
    my $case = $zs->create_case_ok;
    ok $file->update({case_id => undef}), 'Case ID unset';

    my $update = $file->update_properties({
        subject  => $zs->get_subject_ok,
        case_id  => $case->id,
    });
    ok $update, 'Updated file';
    is $update->case->id, $case->id, 'Has case set';
}, 'update_properties assign case');


$zs->zs_transaction_ok(sub {
    my $file = $zs->create_file_ok;
    my $case = $zs->create_case_ok;

    ok !$file->metadata, 'File does not have metadata yet';

    my $update = $file->update_properties({
        subject  => $zs->get_subject_ok,
        metadata => {
            description => 'Arrrr',
            document_category => 'Spluf',
        },
    });
    ok $update, 'Updated file';
    is $update->metadata->description, 'Arrrr', 'Description matches';
    is $update->metadata->document_category, 'Spluf', 'Category matches';

    # Update with existing metadata entry
    my $existingupdate = $file->update_properties({
        subject  => $zs->get_subject_ok,
        metadata => {
            description => 'Barrrr',
        },
    });
    is $update->metadata->description, 'Barrrr', 'Metadata got updated';
}, 'update_properties set metadata');


$zs->zs_transaction_ok(sub {
    my $file = $zs->create_file_ok(db_params => { case => $zs->create_case_ok() });
    my $case = $zs->create_case_ok;

    ok $file->case_id, 'Has case ID set';

    throws_ok sub {
        $file->update_properties({
            subject  => $zs->get_subject_ok,
            case_id  => $case->id,
        })
    }, qr/Cannot assign a new case/, 'Unable to assign new case';
}, 'update_properties assign case with existing case');


$zs->zs_transaction_ok(sub {
    my $file = $zs->create_file_ok;
    throws_ok sub {
        $file->update_properties({case_id => $zs->create_case_ok->id})
    }, qr/Missing options: subject/, 'Got missing subject error';
}, 'update_properties without subject parameter');

zs_done_testing();
