package Net::FTPSSL;
use Moose;
use Exporter 'import';

# ignore parameters, bypass issue with uneven sized list
around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig;
};

sub login { 1 }

sub get {
    my ($self, $remote_file, $local_file) = @_;

    # simulate missing argument and file that doesn't exist
    return unless $remote_file && $remote_file ne 'not_existing.txt';

    open my $file, ">", $local_file or die $!;
    print $file "Dit is testcontent";
    close $file;
}

sub last_message { 'Zaaksysteem Testsuite mockup Net::FTPSSL last message' }

sub quit {}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

