package MockModel;
use Moose;

has zs => (
    is       => 'rw',
    required => 1,
);

has model => (
    is      => 'rw',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return $self->zs->object_model;
    },
);

sub uri_for {
    my $self = shift;
    return $self->new(zs => $self->zs);
}

sub as_string {
    my $self = shift;
    return "uri/foo";
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 NAME

MockModel - A mocking model

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
