package Zaaksysteem::TestMechanize;
use Moose;

extends 'Test::WWW::Mechanize::Catalyst';

=head1 NAME

Zaaksysteem::TestMechanize - Test::WWW::Mechanize::Catalyst subclass with some Zaaksysteem sugar

=head1 DESCRIPTION

This class adds a few methods to L<Test::WWW::Mechanize::Catalyst>, for doing
common Zaaksysteem actions like logging in.

=head1 SYNOPSIS

    use Zaaksysteem::TestMechanize;

    my $mech = Zaaksysteem::TestMechanize->new();

    $mech->zs_login();
    $mech->get_ok($mech->zs_url_base . "/my_controller");

=head1 ATTRIBUTES

=head2 logged_in_as

=cut

has 'logged_in_as' => (is => 'rw');

=head1 METHODS

=head2 import

This class wraps "import", so the "Zaaksysteem" application is always selected.

=cut

around 'import' => sub {
    my $orig = shift;
    my $self = shift;
    
    return $self->$orig("Zaaksysteem");
};

=head2 $mech->zs_url_base

Returns the base URL that should be prepended to requested URLs.

=cut

sub zs_url_base {
    return "https://testsuite";
}

=head2 $mech->zs_login(%args)

Logs in on the Zaaksysteem instance.

Accepts C<username> and C<password> arguments, which both default to "admin".

=cut

sub zs_login {
    my $self = shift;
    my %args = @_;

    my $username    = $args{username} // 'admin';

    if ($self->logged_in_as) {
        if ($self->logged_in_as eq $username) {
            return $username;
        }

        $self->get_ok(
            $self->zs_url_base . '/auth/logout',
            'Already logged in as: ' . $self->logged_in_as
            . ': logging out'
        );
    }

    no warnings qw(redefine once);
    $self->get_ok($self->zs_url_base . '/auth/page', "GET / works");
    my $r = $self->submit_form_ok({
            with_fields => {
                username => $username,
                password => $args{password} // 'admin',
            },
        },
        'Sent login parameters for username: ' . $username,
    );

    return $self->logged_in_as($username);
    # print STDERR "Current forms: " . Data::Dumper::Dumper($self->content);
}

sub post_json {
    my $self        = shift;
    my $url         = shift;
    my $perldata    = shift;
    my $desc        = shift;

    my $jsono       = JSON->new->utf8->pretty->canonical;

    my $json        = $jsono->encode($perldata);


    $self->post($url, Content => $json, Content_Type => 'application/json');

    ### Keep here for easy copy-paste-manual purposes
    # print STDERR "Request:\n" . $json . "\n";
    # print STDERR "Response:\n" . $self->content . "\n";

    my $ok = $self->success;
    $ok = $self->_maybe_lint( $ok, $desc );
}

sub post_file {
    my $self        = shift;
    my $url         = shift;
    my $file        = shift;
    my $desc        = shift;


    $self->post(
        $url,
        Content => [ upload => [$file] ],
        Content_Type => 'form-data'
    );

    ### Keep here for easy copy-paste-manual purposes
    # print STDERR "Response:\n" . $self->content . "\n";

    my $ok = $self->success;
    $ok = $self->_maybe_lint( $ok, $desc );



}

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


1;
