use Log::Log4perl qw(:easy);
use Test::Class::Moose::Load 't/lib';
use Test::Class::Moose::Runner;

if (-e 't/etc/log4perl.conf') {
    Log::Log4perl->init('t/etc/log4perl.conf');
}
else {
    Log::Log4perl->easy_init($OFF);
}

Test::Class::Moose::Runner->new(
    test_classes => \@ARGV
)->runtests;
