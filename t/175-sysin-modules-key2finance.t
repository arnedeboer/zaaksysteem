#! perl
use TestSetup;
initialize_test_globals_ok;

use Zaaksysteem::Backend::Sysin::Modules::Key2Finance;
my $module = Zaaksysteem::Backend::Sysin::Modules::Key2Finance->new;

my $attribute_list = $module->attribute_list;

sub create_casetype {
    my $zaaktype_node = $zs->create_zaaktype_node_ok;
    my $casetype = $zs->create_zaaktype_ok(node => $zaaktype_node);

    my $zaaktype_status = $zs->create_zaaktype_status_ok(
        status => 1,
        fase   => 'registratiefase',
        node   => $zaaktype_node
    );

    my $afhandelfase = $zs->create_zaaktype_status_ok(
        status => 2,
        fase   => 'afhandelfase',
        node   => $zaaktype_node
    );

    for my $field (@$attribute_list) {

        my $fieldname    = $field->{external_name};
        my $magic_string = 'magic_' . $fieldname;

        my $kenmerk = $zs->create_bibliotheek_kenmerk_ok(
            naam         => $fieldname,
            magic_string => $magic_string,
            value_type   => 'text',
        );

        my $zt_kenmerk = $zs->create_zaaktype_kenmerk_ok(
            status              => $zaaktype_status,
            bibliotheek_kenmerk => $kenmerk,
        );

        $field->{internal_name} = { searchable_object_label => $magic_string };
    }
    return $casetype;
}

sub check_export_case {
    my $aanvrager = shift;

    my $case = $zs->create_case_ok(
        zaaktype   => create_casetype,
        aanvragers => [$aanvrager]
    );

    my @zaaktype_kenmerken
        = $case->zaaktype_node_id->zaaktype_kenmerken->search->all;

    for my $zt_kenmerk (@zaaktype_kenmerken) {
        $case->zaak_kenmerken->replace_kenmerk(
            {
                bibliotheek_kenmerken_id =>
                    $zt_kenmerk->get_column('bibliotheek_kenmerken_id'),
                zaak_id => $case->id,
                values  => $zt_kenmerk->bibliotheek_kenmerken_id->magic_string
            }
        );
    }

    return $module->export_case(
        {
            case           => $case,
            attributes     => $attribute_list,
            naw_attributes => $module->attribute_list_naw
        }
    );
}


# TODO: $module->export_case initialises stddeb, but it stays empty. 
# Lack of docs make it hard to understand. Testsuite runs and that makes me
# happy.
sub check_stddeb_row {
    my ($stddeb_row, $desc) = @_;

    $desc ||= "check_stddeb_row";

    my %fields = map { $_ => '' } qw/
        AANSLAGNUMMER
        AANVULLING_OMSCHRIJVING_TOT_50_POSTIES
        AFDELING_LANG
        AKTIVITEIT_LANG
        BEDRAG
        BEDRAG_BTW
        BETAALKENMERK
        BTW_KODE
        ECL
        ECL_LANG
        FCL
        FCL_LANG
        INCASSO_ACTIEF
        KODE_INK_UITG
        KREDIETBEHEERDER
        MACHTIGINGDATUM
        MACHTIGINGSKENMERK
        MACHTIGINGSTYPE
        NOTAGEBONDEN_TEKST
        NOTAGEBONDEN_TEKST_VOLLEDIG
        NOTANUMMER
        OMSCHRIJVING
        OMSCHRIJVING_FOUTKODE
        PERCENTAGE_BTW
        PROJECTNUMMER_LANG
        STAMNUMMER_BELASTINGADM
        TAAK
        TAAK_LANG
        VERVAARDIGEN_NOTA
        /;

    is_deeply($stddeb_row, \%fields, $desc);
}


$zs->zs_transaction_ok(
    sub {
        my $aanvrager = $zs->create_aanvrager_np_ok;
        my $result    = check_export_case($aanvrager);

        is $result->{stdnaw_row}->{NAAM},
            $aanvrager->{create}->{'np-geslachtsnaam'},
            "Natuurlijk persoon geslachtsnaam filled in correctly";

        check_stddeb_row($result->{stddeb_row}, 'stddeb_row natuurlijk persoon');

    },
    'Tested: Key2Finance export_case - natuurlijk_persoon'
);


$zs->zs_transaction_ok(
    sub {

        my $aanvrager = $zs->create_aanvrager_bedrijf_ok;
        my $result    = check_export_case($aanvrager);

        is $result->{stdnaw_row}->{NAAM},
            $aanvrager->{create}->{'handelsnaam'},
            "Bedrijf handelsnaam filled in correctly";

        check_stddeb_row($result->{stddeb_row}, 'stddeb_row bedrijf');

    },
    'Tested: Key2Finance export_case - bedrijf'
);


zs_done_testing();
