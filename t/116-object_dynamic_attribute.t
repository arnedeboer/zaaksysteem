#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
### Test header end

use DateTime;
use Test::DummyObjectData;
use Zaaksysteem::Object::Attribute;

test_daysleft();
test_dayspercentage();
test_daysleft_db();
test_case_destructable();
test_case_destruction_blocked();

sub test_daysleft {
    my $now = DateTime->now();
    my $yesterday = $now->clone->add(days => -1);
    my $tomorrow  = $now->clone->add(days => 1);
    my $last_year = $now->clone->add(days => -365);
    my $next_year = $now->clone->add(days => 365);

    my $parent_object = Test::DummyObjectData->new();
    my $target_attr = Zaaksysteem::Object::Attribute->new(
        name           => 'case.date_target',
        attribute_type => 'timestamp_or_text',
        value          => $now,
    );
    my $completion_attr = Zaaksysteem::Object::Attribute->new(
        name           => 'case.date_of_completion',
        attribute_type => 'timestamp',
        value          => undef,
    );
    $parent_object->add_object_attributes($target_attr, $completion_attr);

    my $daysleft_attr = Zaaksysteem::Object::Attribute->new(
        dynamic_class  => 'DaysLeft',
        name           => 'case.days_left',
        attribute_type => 'integer',
        parent_object  => $parent_object,
    );
    $parent_object->add_object_attributes($daysleft_attr);

    ok(
        $daysleft_attr->DOES('Zaaksysteem::Object::AttributeRole::DaysLeft'),
        'Dynamic attribute role successfully added'
    );

    is($daysleft_attr->value, 0, "target = today, completion = undef -> 0 days left");

    $completion_attr->value($tomorrow);
    is($daysleft_attr->value, -1, "target = today, completion = tomorrow -> -1 day left");

    $completion_attr->value($yesterday);
    is($daysleft_attr->value, 1, "target = today, completion = yesterday -> 1 day left");

    $completion_attr->value($next_year);
    is($daysleft_attr->value, -365, "target = today, completion = 365 days from now -> -365 days left");

    $completion_attr->value($last_year);
    is($daysleft_attr->value, 365, "target = today, completion = 365 days before now -> 365 days left");

    $target_attr->value('Opgeschort');
    is($daysleft_attr->value, undef, "target = Opgeschort -> days_left is undefined");

    throws_ok(
        sub { $daysleft_attr->value('something') },
        'Zaaksysteem::Exception::Base',
        'Dynamic attribute is unsettable'
    );

    {
        delete local $parent_object->attributes->{'case.date_target'};
        throws_ok(
            sub { $daysleft_attr->value() },
            'Zaaksysteem::Exception::Base',
            'Exception thrown when case.date_target does not exist in parent',
        );
    }

    {
        delete local $parent_object->attributes->{'case.date_target'};
        throws_ok(
            sub { $daysleft_attr->value() },
            'Zaaksysteem::Exception::Base',
            'Exception thrown when case.date_target does not exist in parent',
        );
    }

    {
        $daysleft_attr->parent_object(undef);
        throws_ok(
            sub { $daysleft_attr->value() },
            'Zaaksysteem::Exception::Base',
            'Exception thrown when no parent object is present',
        );
    }
}

sub test_dayspercentage {
    my $now = DateTime->now();
    my $a_while_ago = $now->clone->add(days => -100);
    my $soonish     = $now->clone->add(days => 50);
    my $soon        = $now->clone->add(days => 100);

    my $parent_object = Test::DummyObjectData->new();
    my $target_attr = Zaaksysteem::Object::Attribute->new(
        name           => 'case.date_target',
        attribute_type => 'timestamp_or_text',
        value          => $soon,
    );
    my $completion_attr = Zaaksysteem::Object::Attribute->new(
        name           => 'case.date_of_completion',
        attribute_type => 'timestamp',
        value          => undef,
    );
    my $registration_attr = Zaaksysteem::Object::Attribute->new(
        name           => 'case.date_of_registration',
        attribute_type => 'timestamp',
        value          => $now,
    );

    $parent_object->add_object_attributes($target_attr, $completion_attr, $registration_attr);

    my $daysperc_attr = Zaaksysteem::Object::Attribute->new(
        dynamic_class  => 'DaysPercentage',
        name           => 'case.days_perc',
        attribute_type => 'integer',
        parent_object  => $parent_object,
    );
    $parent_object->add_object_attributes($daysperc_attr);

    ok(
        $daysperc_attr->DOES('Zaaksysteem::Object::AttributeRole::DaysPercentage'),
        'Dynamic attribute role successfully added'
    );

    is($daysperc_attr->value, 0, "target = +100d, completion = undef, registration = now -> 0%");

    $registration_attr->value($a_while_ago);
    $target_attr->value($now);
    is($daysperc_attr->value, 100, "target = now, completion = undef, registration = -100d -> 100%");

    $registration_attr->value($a_while_ago);
    $target_attr->value($soon);
    is($daysperc_attr->value, 50, "target = +100d, completion = undef, registration = -100d -> 50%");

    $registration_attr->value($a_while_ago);
    $completion_attr->value($now);
    $target_attr->value($soon);
    is($daysperc_attr->value, 50, "target = +100d, completion = now, registration = -100d -> 50%");

    $registration_attr->value($now);
    $completion_attr->value($soonish);
    $target_attr->value($soon);
    is($daysperc_attr->value, 50, "target = +100d, completion = +50d, registration = now -> 50%");
}

sub test_daysleft_db {
    $zs->zs_transaction_ok(sub {
        my $now = DateTime->now();
        my $yesterday = $now->clone->add(days => -1);

        my $obj = $zs->schema->resultset('ObjectData')->create(
            { object_class => 'flup' }
        );
        my $target_attr = Zaaksysteem::Object::Attribute->new(
            name           => 'case.date_target',
            attribute_type => 'timestamp_or_text',
            value          => $now,
        );
        my $completion_attr = Zaaksysteem::Object::Attribute->new(
            name           => 'case.date_of_completion',
            attribute_type => 'timestamp',
            value          => $yesterday,
        );
        my $daysleft_attr = Zaaksysteem::Object::Attribute->new(
            dynamic_class  => 'DaysLeft',
            name           => 'case.days_left',
            attribute_type => 'integer',
            parent_object  => $obj,
        );
        $obj->add_object_attributes($target_attr, $completion_attr, $daysleft_attr);
        $obj->update();
        $obj->discard_changes();

        my $object_attrs = $obj->object_attributes;
        my ($retrieved_daysleft_oa) = grep { $_->name eq 'case.days_left' } @{ $object_attrs };
        is(
            $retrieved_daysleft_oa->value,
            1,
            "Dynamic attribute when retrieved from database (using ->object_attributes)"
        );

        my $retrieved_daysleft_attr = $obj->get_object_attribute('case.days_left');
        is($retrieved_daysleft_attr->value, 1, "Dynamic attribute when retrieved from database");

        is_deeply(
            $daysleft_attr->TO_JSON,
            {
                name           => 'case.days_left',
                attribute_type => 'integer',
                dynamic_class  => 'DaysLeft',
                human_label    => undef,
            },
            'JSON representation of dynamic attribute is correct'
        );

        is_deeply(
            $obj->TO_JSON,
            {
                id => $obj->uuid,
                object_id => $obj->object_id,
                object_type => 'flup',
                values => {
                    'case.date_of_completion' => $yesterday,
                    'case.date_target'        => $now,
                    'case.days_left'          => 1,
                },

            },
            'JSON representation of object with dynamic attribute is correct'
        );
    }, 'DaysLeft attribute stored in and retrieved from database');
}

sub test_case_destructable {
    my $now         = DateTime->now();
    my $a_while_ago = $now->clone->add(days => -100);
    my $soon        = $now->clone->add(days => 100);

    my $parent_object = Test::DummyObjectData->new();

    my $date_destruction_attr = Zaaksysteem::Object::Attribute->new(
        name           => 'case.date_destruction',
        attribute_type => 'timestamp',
        value          => $a_while_ago,
    );
    $parent_object->add_object_attributes($date_destruction_attr);

    my $destructable_attr = Zaaksysteem::Object::Attribute->new(
        dynamic_class  => 'CaseDestructable',
        name           => 'case.destructable',
        attribute_type => 'integer',
        parent_object  => $parent_object,
    );
    $parent_object->add_object_attributes($destructable_attr);

    is($destructable_attr->value, 1, "If date_destruction is in the past, destructable = true");

    $date_destruction_attr->value($soon);
    is($destructable_attr->value, 0, "If date_destruction is in the future, destructable = false");

    $date_destruction_attr->value(undef);
    is($destructable_attr->value, 0, "If date_destruction is not set, destructable = false");
}

sub test_case_destruction_blocked {
    my $mock_case = Test::MockObject->new();
    my $parent_object = Test::DummyObjectData->new(source_object => $mock_case);

    my $can_delete = 0;
    $mock_case->mock('can_delete', sub { return $can_delete; });

    my $blocked_attr = Zaaksysteem::Object::Attribute->new(
        dynamic_class  => 'CaseDestructionBlocked',
        name           => 'case.destruction_blocked',
        attribute_type => 'integer',
        parent_object  => $parent_object,
    );
    $parent_object->add_object_attributes($blocked_attr);

    $can_delete = 0;
    is($blocked_attr->value, 1, "If case->can_delete() is false, blocked = true.");

    $can_delete = 1;
    is($blocked_attr->value, 0, "If case->can_delete() is true, blocked is false.");
}

zs_done_testing();

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

