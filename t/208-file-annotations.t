#! perl
use TestSetup;
initialize_test_globals_ok;

use Data::UUID;

sub create_file {
    my ($rs) = @_;

    return $rs->file_create({
        db_params => {
            created_by => $zs->get_subject_ok,
        },
        file_path => $zs->config->{filestore_test_file_path},
        name      => 'piet.txt',
    });
}


sub create_annotation {
    my ($rs_annotations, $file_id, $uidnumber) = @_;

    my $generator = new Data::UUID;

    return $rs_annotations->create({
        id => $generator->to_string($generator->create),
        file_id => $file_id,
        subject => 'betrokkene-medewerker-' . $uidnumber,
        properties => {boter => 1, kaas => 2, worst => 3}
    });
}

$zs->zs_transaction_ok(sub {
    my $dummy_user = Test::MockObject->new();
    $dummy_user->set_always(uidnumber => 1);

    my $rs = $schema->resultset('File');

    my $file = create_file($rs);
    $file->result_source->schema->default_resultset_attributes->{ current_user } = $dummy_user;

    my $rs_annotations = $schema->resultset('FileAnnotation');

    my $annotation = create_annotation($rs_annotations, $file->id, $dummy_user->uidnumber);
    is ($file->annotation_count, 1, "one annotation present after addition");
    is ($file->TO_JSON->{annotation_count}, 1, "one annotation present after addition, correct in TO_JSON output");

    is ($annotation->properties->{worst}, 3, "stored properties retrieved properly");

    my $other = create_annotation($rs_annotations, $file->id, 1337);
    is ($file->annotation_count, 1, "one annotation present after addition for other user");
    is ($file->annotation_count, 1, "one annotation present after addition for other user, in TO_JSON output");

    $schema->resultset('Config')->create({
        parameter => 'pdf_annotations_public',
        value => 1
    });

    is ($file->annotation_count, 2, "two annotations present after declaring annotations public");

    $other->delete;
    $annotation->delete;

    is ($file->annotation_count, 0, "no annotation present after delete own");

}, 'check file annotations');


zs_done_testing();
