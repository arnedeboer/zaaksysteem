#! perl

use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
BEGIN { use_ok('Zaaksysteem::Geo') };

use constant GEO_CODE_TEST_TERM => 'Amsterdam, Donker Curtiusstraat 7';

my $geo = Zaaksysteem::Geo->new();

note 'Test exceptions';
{
    throws_ok(
        sub {
            $geo->geocode();
        },
        qr/cannot code unless query is given/,
        'Invalid query evaluation'
    );
}


note 'Default query';
{
    $geo->query(GEO_CODE_TEST_TERM);

    ok($geo->geocode(), 'Got Google Response');

    my $results  = $geo->results;

    ok(($results && UNIVERSAL::isa($results, 'ARRAY')), 'Found results');

    my $result  = shift(@$results);

    isa_ok($result, 'Zaaksysteem::Geo::Location', 'Got correct result');

    can_ok($result, qw/address street province city country coordinates
        zipcode identification/,
    );

    ok($result->$_, 'Found filled entry for: ' . $_ . ': ' . $result->$_) for qw/
        address street province city country coordinates
        zipcode identification/;

    ok(($_ =~ /\d+.\d+/), 'Got numeric coordinate') for values %{ $result->coordinates };
}


zs_done_testing();
