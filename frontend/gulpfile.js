/*global require*/
var gulp = require('gulp'),
	watch = require('gulp-watch'),
	_ = require('lodash'),
	del = require('del'),
	concat = require('gulp-concat'),
	rename = require('gulp-rename'),
	bowerFiles = require('main-bower-files'),
	runSequence = require('run-sequence'),
	changed = require('gulp-changed'),
	livereload = require('gulp-livereload'),
	/* javascript */ 
	uglify = require('gulp-uglify'),
	sourcemaps = require('gulp-sourcemaps'),
	karma = require('gulp-karma'),
	protractor = require('gulp-protractor'),
	/* sass */
	sass = require('gulp-sass'),
	/* html */
	swig = require('gulp-swig'),
	replace = require('gulp-replace'),
	config = null,
	constants = {
		SRC: './*/src',
		TEST: './*/test',
		DEST: './../root',
		APP_NAME: 'zaaksysteem',
		TASKS: [ 'concat', 'minify', 'comp', 'sass', 'swig' ]
	};
	
	config = {
		js: {
			src: [ constants.SRC + '/js/**/_*.js', constants.SRC + '/js/**/*.js' ],
			dest: constants.DEST + '/js',
			concat: {
				name: constants.APP_NAME + '.js',
			},
			components: {
				name: 'components.js'
			},
			uglify: {
				name: constants.APP_NAME + '.min.js',
				sourcemaps: {
					name: constants.APP_NAME + '.min.js.map',
					dest: '.'
				}
			},
			karma: {
				files : [
					constants.DEST + '/js/components.js',
					'./components/angular-mocks/angular-mocks.js',
					constants.DEST + '/js/' + constants.APP_NAME + '.min.js',
					constants.TEST + '/unit/**/*.js'
				],
				autoWatch : false,
				frameworks: ['jasmine'],
				browsers : [ 'PhantomJS' ],
				plugins : [
						'karma-chrome-launcher',
						'karma-phantomjs-launcher',
						'karma-jasmine'
						],
				junitReporter : {
					outputFile: 'test/output/unit.xml',
					suite: 'unit'
				}
			},
			protractor: {
				files: constants.TEST + '/e2e/**/*.js'
			}
		},
		css: {
			src: [ constants.SRC + '/css/**/*.scss', '!' + constants.SRC + '/css/**/*_.scss' ],
			dest: constants.DEST + '/css'
		},
		html: {
			src: [ constants.SRC + '/html/**/*.swig' ],
			dest: constants.DEST + '/html'
		}
	};
	
gulp.task('clean', function ( callback ) {
	del([ config.js.dest, config.css.dest, config.html.dest ], { force: true }, callback);
});
	
gulp.task('concat', function ( ) {
	return gulp.src(config.js.src)
		.pipe(concat(config.js.concat.name))
		.pipe(gulp.dest(config.js.dest));
});

gulp.task('comp', function ( ) {
	return gulp.src(bowerFiles({ env: 'production' }))
		.pipe(concat(config.js.components.name))
		.pipe(replace(/\/\/\# sourceMappingURL=(.*?)map/g, ''))
		.pipe(gulp.dest(config.js.dest));
});

gulp.task('minify', function ( ) {
	return gulp.src(config.js.src)
		.pipe(sourcemaps.init())
			.pipe(concat(config.js.uglify.name))
			.pipe(uglify())
		.pipe(sourcemaps.write(config.js.uglify.sourcemaps.dest))
		.pipe(gulp.dest(config.js.dest));
});

gulp.task('karma', function ( ) {
	return gulp.src(config.js.karma.files)
		.pipe(karma(_.assign({}, config.js.karma, { action: 'run' })));
});

gulp.task('protractor', function ( ) {
	return gulp.src(config.js.protractor.files)
		.pipe(protractor.protractor({
			configFile: './zaaksysteem/test/protractor.config.js'
		}));
});

gulp.task('sass', function ( ) {
	return gulp.src(config.css.src)
		.pipe(sass({errLogToConsole: true}))
		.pipe(rename(function ( path ) {
			path.dirname = path.dirname.replace(/(.*?)[\\\/]src[\\\/]css[\\\/]?/, '');
		}))
		.pipe(changed(config.css.dest, { hasChanged: changed.compareSha1Digest }))
		.pipe(gulp.dest(config.css.dest));
});

gulp.task('swig', function ( ) {
	return gulp.src(config.html.src)
		.pipe(replace(/%%(.*?)%%/g, '$1'))
		.pipe(swig({
			defaults: {
				cache: false,
				varControls: ['[[', ']]'],
				tagControls: ['[%', '%]'],
				cmtControls: ['{#', '#}']
			}
		}))
		.pipe(rename(function ( path ) {
			path.dirname = path.dirname.replace(/(.*?)[\\\/]src[\\\/]html/, 'nl');
		}))
		.pipe(gulp.dest(config.html.dest));	
});

gulp.task('compile', constants.TASKS );

gulp.task('build', function ( ) {
	runSequence('clean', 'compile');
});

gulp.task('unit', function ( ) {
	runSequence(constants.TASKS, 'karma');
});

gulp.task('e2e', function ( ) {
	runSequence(constants.TASKS, 'protractor');
});

gulp.task('default', function ( ) {
	['concat', 'sass', 'swig'].forEach(function ( task ) {
		if(constants.TASKS.indexOf(task) !== -1) {
			var key = { 'concat': 'js', 'sass': 'css', 'swig': 'html' }[task];
			gulp.watch(config[key].src, [ task ]);
		}
	});
	
	// livereload.listen();
	// watch(config.css.dest + '/**/*.css', livereload.changed);
	
	return gulp.src(config.css.dest + '/**/*.css')
		.pipe(watch(config.css.dest + '/**/*.css'))
		.pipe(livereload());
	
});
