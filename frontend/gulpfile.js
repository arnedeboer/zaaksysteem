/*global require*/
var gulp = require('gulp'),
	gutil = require('gulp-util'),
	watch = require('gulp-watch'),
	del = require('del'),
	concat = require('gulp-concat'),
	rename = require('gulp-rename'),
	bowerFiles = require('main-bower-files'),
	runSequence = require('run-sequence'),
	mergeStream = require('merge-stream'),
	changed = require('gulp-changed'),
	livereload = require('gulp-livereload'),
	/* javascript */
	uglify = require('gulp-uglify'),
	sourcemaps = require('gulp-sourcemaps'),
	protractor = require('gulp-protractor'),
	/* sass */
	sass = require('gulp-sass'),
	/* html */
	swig = require('gulp-swig'),
	replace = require('gulp-replace'),
	cleanCss = require('gulp-clean-css'),
	path = require('path');

var config = null,
	constants = {
		SRC: './*/src',
		TEST: './*/test',
		DEST: './../root',
		APP_NAME: 'zaaksysteem',
		TASKS: [ 'concat', 'minify', 'comp', 'sass', 'sass-build', 'swig' ]
	};
	
	config = {
		js: {
			src: [ constants.SRC + '/js/**/_*.js', constants.SRC + '/js/**/*.js' ].concat(
				[
					'jquery-1.7.1.min.js',
					'jquery-ui-1.8.24.custom.min.js',
					'OpenLayers-2.12/OpenLayers.js',
					'jquery.cookie.js',
					'jquery.layout-latest.min.js',
					'jquery.layout.resizePaneAccordions-latest.js',
					'jquery.treeTable.js',
					'zaaksysteem.js',
					'jquery.ui.selectmenu.js',
					'jquery.hoverIntent.js',
					'easing.js',
					'multiselect/plugins/localisation/jquery.localisation-min.js',
					'ui.multiselect.js',
					'highcharts.js',
					'exporting.js',
					'jquery.ui.datepicker-nl.js',
					'regel_editor.js',
					'jquery.defaultvalue.js',
					'zaaksysteem/WebformTextfieldUpdateRegistry.js',
					'webform.js',
					'validate.js',
					'jquery.fileUploader.js',
					'jquery.qtip.min.js',
					'modernizr.js',
					'mintloader.js',
					'zaaktype_import.js',
					'zaaksysteem/ezra_objectsearch.js',
					'ezra_listactions.js',
					'maps.js',
					'zaaksysteem/wgxpath.install.js',
					'zaaksysteem/ezra_dialog.js',
					'zaaksysteem/ezra_simpletable.js',
					'zaaksysteem/ezra_kennisbank.js',
					'zaaksysteem/ezra_search_box.js',
					'zaaksysteem/ezra_maps.js',
					'zaaksysteem/ezra_import.js',
					'zaaksysteem/ezra_object_import.js',
					'zaaksysteem/import_inavigator.js',
					'zaaksysteem/ezra_search_chart.js',
					'ICanHaz.min.js',
					'zaaksysteem/ezra_woz_photo.js',
					'zaaksysteem/ezra_ztb_notificaties.js',
					'zaaksysteem/ezra_attribute_ie_upload.js',
					'iban.js',
					'quill/quill.min.js'
				]
					.map(function ( filename ) {
						return '../root/tpl/zaak_v1/nl_NL/js/' + filename;
					})
			),
			dest: constants.DEST + '/js',
			concat: {
				name: constants.APP_NAME + '.js'
			},
			components: {
				name: 'components.js'
			},
			uglify: {
				name: constants.APP_NAME + '.min.js',
				sourcemaps: {
					name: constants.APP_NAME + '.min.js.map',
					dest: '.'
				}
			},
			protractor: {
				files: constants.TEST + '/e2e/**/*.js'
			}
		},
		css: {
			src: [ constants.SRC + '/css/**/*.scss', '!' + constants.SRC + '/css/**/*_.scss' ],
			dest: constants.DEST + '/css',
			concat: {
				name: constants.APP_NAME + '.css'
			}
		},
		html: {
			src: [ constants.SRC + '/html/**/*.swig' ],
			dest: constants.DEST + '/html'
		}
	};
	
gulp.task('clean', function ( callback ) {
	del([ config.js.dest, config.css.dest, config.html.dest ], { force: true, read: false })
		.then(function ( ) { callback(); }, function ( ) { callback(); });
});
	
gulp.task('concat', function ( ) {
	return gulp.src(config.js.src)
		.pipe(concat(config.js.concat.name))
		.pipe(gulp.dest(config.js.dest));
});

gulp.task('comp', function ( ) {
	return mergeStream(
		gulp.src(bowerFiles({ env: 'production' }))
				.pipe(concat(config.js.components.name))
				.pipe(replace(/\/\/\# sourceMappingURL=(.*?)map/g, ''))
				.pipe(gulp.dest(config.js.dest)),
		gulp.src('**/*', { cwd: './components/webodf' })
			.pipe(gulp.dest('../root/webodf')),
		gulp.src('**/*', { cwd: './components/pdf.js-with-viewer' })
			.pipe(gulp.dest('../root/pdf.js-with-viewer'))
	);
});

gulp.task('minify', function ( ) {
	return gulp.src(config.js.src)
		.pipe(sourcemaps.init())
			.pipe(concat(config.js.uglify.name))
			.pipe(uglify())
		.pipe(sourcemaps.write(config.js.uglify.sourcemaps.dest))
		.pipe(gulp.dest(config.js.dest));
});

gulp.task('protractor', function ( ) {
	return gulp.src(config.js.protractor.files)
		.pipe(protractor.protractor({
			configFile: './zaaksysteem/test/protractor.config.js'
		}));
});

gulp.task('sass', function ( ) {
	return gulp.src(config.css.src)
		.pipe(sass({errLogToConsole: true}))
		.pipe(rename(function ( p ) {
			p.dirname = p.dirname.replace(/(.*?)[\\\/]src[\\\/]css[\\\/]?/, '');
		}))
		.pipe(changed(config.css.dest, { hasChanged: changed.compareSha1Digest }))
		.pipe(gulp.dest(config.css.dest));
});

gulp.task('sass-build', function ( ) {

	return gulp.src(config.css.src)
		.pipe(sass({errLogToConsole: true}))
		.pipe(rename(function ( p ) {
			p.dirname = p.dirname.replace(/(.*?)[\\\/]src[\\\/]css[\\\/]?/, '');
			p.basename += '.min';
		}))
		.pipe(cleanCss({
			keepSpecialComments: false
		}))
		.pipe(gulp.dest(config.css.dest));
});

gulp.task('swig', function ( ) {
	return gulp.src(config.html.src)
		.pipe(replace(/%%(.*?)%%/g, '$1'))
		.pipe(replace(/\\%/g, '%'))
		.pipe(swig({
			defaults: {
				cache: false,
				varControls: ['[[', ']]'],
				tagControls: ['[%', '%]'],
				cmtControls: ['{#', '#}']
			}
		}))
		.pipe(rename(function ( p ) {
			p.dirname = p.dirname.replace(/(.*?)[\\\/]src[\\\/]html/, 'nl');
		}))
		.pipe(gulp.dest(config.html.dest));
});

gulp.task('compile', constants.TASKS );

gulp.task('build', function ( ) {
	runSequence('clean', 'compile');
});

gulp.task('e2e', function ( ) {
	runSequence(constants.TASKS, 'protractor');
});

gulp.task('default', function ( ) {
	['concat', 'sass', 'swig'].forEach(function ( task ) {
		if(constants.TASKS.indexOf(task) !== -1) {
			var key = { 'concat': 'js', 'sass': 'css', 'swig': 'html' }[task];
			gulp.watch(config[key].src, [ task ]);
		}
	});
	
	livereload.listen();
	// watch(config.css.dest + '/**/*.css', livereload.changed);
	
	return gulp.src(config.css.dest + '/**/*.css')
		.pipe(watch(config.css.dest + '/**/*.css'))
		.pipe(livereload());
	
});
