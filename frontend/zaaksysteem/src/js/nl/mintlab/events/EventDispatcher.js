/*global define, fetch*/
(function ( ) {
	
	define('nl.mintlab.events.EventDispatcher', function ( ) {
		
		var indexOf = fetch('nl.mintlab.utils.shims.indexOf');
		
		function toArray ( array ) {
			return Array.prototype.slice.call(array, 0);
		}

		
		function EventDispatcher ( ) {
			
		}
	
		EventDispatcher.prototype.getListenerCollection = function ( event ) {
			// lazy initialization to enable mixins
			if(!this._listeners) {
				this._listeners = [];
			}
			var listeners = this._listeners;
			if(!listeners[event]) {
				listeners[event] = [];
			}
			return listeners[event];
		};

		EventDispatcher.prototype.publish = function ( ) {
			var event = arguments[0],
				collection = this.getListenerCollection(event),
				i,
				l;
				
			for(i = 0, l = collection.length; i < l; ++i) {
				collection[i].apply(null, toArray(arguments));
			}
		};

		EventDispatcher.prototype.subscribe = function ( event, callback ) {
			this.getListenerCollection(event).push(callback);
		};

		EventDispatcher.prototype.unsubscribe = function ( event, callback ) {
			var collection = this.getListenerCollection(event);
			var index = indexOf(collection, callback);
			if(index !== -1) {
				collection.splice(index, 1);
			}
		};

		EventDispatcher.prototype.isSubscribed = function ( event, callback ) {
			return indexOf(this.getListenerCollection(event), callback) !== -1;
		};
		
		return EventDispatcher;
		
	});
	
})();