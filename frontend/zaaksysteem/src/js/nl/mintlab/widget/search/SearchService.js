/*global angular,_,EventEmitter2*/
(function ( ) {
	
	angular.module('Zaaksysteem.widget.search')
		.service('searchService', [ '$q', '$http', 'formService', 'userService', 'objectService', 'translationService', function ( $q, $http, formService, userService, objectService, translationService ) {
			
			var searchService = {},
				configsById = {};
			
			function addDefaultSecurityRules ( filter ) {
				var username = userService.getCurrentUsername();
				
				if(!userService.isAdmin()) {
					_.each([ 'read', 'write', 'delete', 'manage' ], function ( capability ) {
						filter.security_rules.push(objectService.createSecurityRule(username, 'user', capability));
					});
				}
			}
			
			function getDefaultObjectType ( ) {
				return {
					object_type: 'case',
					label: 'Zaak',
					id: 1
				};
			}
			
			function setConfigDefault ( config ) {

				var columnDefault = config.columns['default'],
					templates = config.columns.templates;
				
				_.each(config.attributes, function ( attr ) {
					
					var field,
						formAttrType = attr.type,
						template;

					if(attr.type === 'date') {
						formAttrType = 'date-range';
					}

					field = angular.extend({
						name: attr.name,
						label: ''
					}, formService.getFormFieldForAttributeType(formAttrType));
					
					if('options' in attr.data && attr.data.options !== null) {
						field.data = {
							options: _.map(_.sortBy(attr.data.options, 'sort_order'), function ( val ) {
								return {
									value: val.value,
									label: val.value,
									always_visible: val.active
								};
							})
						};
						
					}

					columnDefault.splice(columnDefault.length - 1, 0, attr.name);

					template = angular.extend(templates[attr.name] || {}, searchService.getTypeColumn(attr.name, attr.public_label || attr.label, attr.type));
					
					if(attr.is_multiple) {
						field.data = angular.extend(field.data || {}, { operator: 'CONTAINS' });
					}
					
					templates[attr.name] = template;
					
					config.filters.fieldsets.push({
						'name': attr.name,
						'title': attr.label,
						'description': '',
						'fields': [ field ]
					});
					
				});

				config.columns['default'] = _.uniq(config.columns['default']);
				
			}
						
			function createSearch ( params ) {
				var search = new EventEmitter2(),
					filters = null,
					filterPromise,
					defaults = {
						label: undefined,
						predefined: false,
						values: {},
						options: {},
						zql: undefined,
						columns: [],
						public: false,
						objectType: getDefaultObjectType()
					};
					
				if(!params) {
					params = {};
				}
				
				search.setDefaults = function ( def ) {
					_.merge(defaults, angular.copy(def));
				};
				
				search.createFilter = function ( ) {
					var filter = objectService.createObject('saved_search');
					
					_.merge(filter.values, defaults);
					
					filter.owner = userService.getCurrentUuid();
					
					addDefaultSecurityRules(filter);
										
					return filter;
				};
				
				search.addFilter = function ( filter ) {
					filters.push(filter);
				};
				
				search.saveFilter = function ( filter ) {
					
					var copy = angular.copy(filter);
					
					copy.values = {
						query: JSON.stringify(angular.copy(filter.values)),
						title: filter.values.label,
						public: filter.values.public
					};
					
					return objectService.saveObject(copy)
						.then(function ( data ) {
							_.merge(filter, data);
							
							filter.values = JSON.parse(data.values.query);
							
						});
				};
				
				search.deleteFilter = function ( filter ) {
					
					var index = _.indexOf(filters, filter);
					
					_.pull(filters, filter);
					
					return objectService.deleteObject(filter)
						.then(
							null,
							function ( /*response*/ ) {
								filters.splice(index, 0, filter);
							}
						);
				};
				
				search.getSavedFilters = function ( options ) {
					var deferred,
						
					options = _.defaults(
						options,
						{ baseUrl: '/api/object/search' }
					);
						
					if(!filterPromise) {
						deferred = $q.defer();
						
						filterPromise = deferred.promise;
						
						$http({
							method: 'GET',
							url: options.baseUrl,
							params: {
								'zql': 'SELECT {} from saved_search',
								'zapi_no_pager': 1
							}
						})
							.success(function ( response ) {
								filters = response.result;
								
								_.each(filters, function ( filter ) {
									var vals = filter.values;
									filter.values = JSON.parse(vals.query);
									
									if(filter.values.objectType === undefined) {
										filter.values.objectType = getDefaultObjectType();
									}
								});
								
								deferred.resolve(filters);
							})
							.error(function ( response) {
								deferred.reject(response.result[0]);
							});
					}
					
					return filterPromise;
				};
				
				search.getCopy = function ( from ) {
					var copy = search.createFilter();
					
					copy.values.label = from.values.label + ' ' + translationService.get('(kopie)');
					copy.values.zql = from.zql;
					copy.values.values = angular.copy(from.values.values);
					copy.values.options = angular.copy(from.values.options);
					copy.values.columns = angular.copy(from.values.columns);
					copy.values.objectType = angular.copy(from.values.objectType);
					
					delete copy.values.predefined;
					
					return copy;
				};
				
				return search;
			}
			
			searchService.create = function ( params ) {
				return createSearch( params);
			};
			
			searchService.isEditable =  function ( filter ) {
				return !filter.values.predefined && objectService.isUserCapable(filter, 'write');
			};
			
			searchService.isSharable = function ( filter ) {
				return !filter.values.predefined && objectService.isUserCapable(filter, 'manage');
			};
			
			searchService.isRemovable = function ( filter ) {
				return !filter.values.predefined && objectService.isUserCapable(filter, 'manage');	
			};
			
			searchService.isChanged = function ( filterA, filterB ) {
				var isChanged = false;
				
				isChanged = filterA.values.zql !== filterB.values.zql;
				
				if(!isChanged) {
					isChanged = !angular.equals(filterA.values.options, filterB.values.options) || !angular.equals(filterA.values.objectType, filterB.values.objectType);
				}
				
				return isChanged;
			};
			
			searchService.isLabs = function ( ) {
				var setting = userService.getSetting('labs.object_search');
				return setting;
			};
			
			searchService.getTypeColumn = function ( name, label, type ) {
				var column = {
						id: name,
						label: label,
						resolve: 'values["' + name + '"]',
						attributeType: type
					};
					
				switch(type) {
					case 'valuta':
					case 'valutain':
					case 'valutaex':
					case 'valutain6':
					case 'valutain21':
					case 'valutaex21':
					column.template = '<span><[item.values["' + name + '"]!==null?(item.values["' + name + '"]|currency):""]></span>';
					break;
					
					case 'date':
					column.filter = 'date:"dd-MM-yyyy"';
					break;
					
					case 'timestamp':
					column.filter = 'date:"dd-MM-yyyy HH:mm:ss"';
					break;
					
					case 'timestamp_or_text':
					column.filter = 'dateOrText:"dd-MM-yyyy HH:mm:ss"';
					break;
					
					case 'numeric':
					column.filter = 'number';
					break;
					
					case 'option':
					case 'checkbox':
					column.templateUrl = '/html/widget/search/column-type-list.html';
					break;
					
					case 'bag_straat_adres':
					case 'bag_adres':
					case 'bag_openbareruimte':
					column.resolve = 'values["' + name + '"].human_identifier';
					break;
					
					case 'bag_straat_adressen':
					case 'bag_adressen':
					case 'bag_openbareruimtes':
					column.templateUrl = '/html/widget/search/column-type-addr-mult.html';
					break;
					
					case 'file':
					column.templateUrl = '/html/widget/search/column-type-file.html';
					break;

					case 'calendar':
					column.templateUrl = '/html/widget/search/column-type-calendar.html';
					break;

					case 'calendar_supersaas':
					column.templateUrl = '/html/widget/search/column-type-calendar-supersaas.html';
					break;
				}
				
				return column;
			};
			
			searchService.toggleLabs = function ( ) {
				var deferred = $q.defer(),
					isLabs = searchService.isLabs();
					
				userService.setSetting('labs.object_search', !isLabs);
					
				$http({
					url: '/api/user/labs/' + (isLabs ? 'disable' : 'enable'),
					method: 'POST',
					data: {
						id: 'object_search'
					}
				})
					.success(function ( /*response*/ ) {
						deferred.resolve();
					})
					.error(function ( response ) {
						userService.setSetting('labs.object_search', isLabs);
						deferred.reject(response);
					});
				
				return deferred.promise;
			};
			
			searchService.getConfig = function ( objectType, options ) {
				var deferred = configsById[objectType];

				options = _.defaults(
					options || {},
					{ baseUrl: '/api/search/config' }
				);
				
				if(!deferred) {
					
					deferred = configsById[objectType] = $q.defer();
					
					if(!objectType) {
						deferred.resolve({
							actions: [],
							filters: {},
							columns: {},
							resolve: null,
							map: {},
							sort: {},
							style: {},
							grouping: []
						});
					} else {
						$http({
							method: 'GET',
							url: options.baseUrl + '/' + objectType
						})
							.success(function ( response ) {
								var config = response.result[0];
								
								if(config.attributes !== undefined) {
									setConfigDefault(config);
								}
								
								deferred.resolve(config);
							})
							.error(function ( response ) {
								deferred.reject(response.result[0]);
							});
					}
					
				}
				
				return deferred.promise;
			};
			
			searchService.getMaxResults = function ( ) {
				return 2500;	
			};
			
			return searchService;
			
		}]);
	
})();
