/*global angular,fetch,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.widget.search')
		.controller('nl.mintlab.widget.search.SearchWidgetResultController', [ '$scope', '$parse', 'userService', 'searchService', 'formService', function ( $scope, $parse, userService, searchService, formService ) {
			
			var safeApply = fetch('nl.mintlab.utils.safeApply'),
				crud = null,
				numRows = 20,
				crudItems = [],
				count = 0,
				maxCount = searchService.getMaxResults();
				
			$scope.displayMode = 'list';
			
			numRows = userService.getSetting('search.numPerPage') || numRows;
			
			$scope.$on('zs.pagination.numrows.change', function ( event, numRows ) {
				userService.setSetting('search.numPerPage', numRows);
			});
			
			function rebuildCrud ( ) {
				var filters,
					form = formService.get('searchWidgetForm'),
					columns = $scope.getActiveColumns();
					
				if(!form || !$scope.activeSearch) {
					return null;
				}
				
				filters = form.scope.getValues(true);
				
				crud = {
					"url": $scope.getCrudUrl(),
					"options": {
						"select": "all",
						"link": $scope.getItemLink(),
						"resolve": $scope.getItemResolve(),
						"sort": $scope.getActiveSort()
					},
					"actions": $scope.getActions() || [],
					"columns": columns,
					"style": $scope.getItemStyle(),
					"numrows": numRows
				};
			}
			
			function hasOpenGroup ( ) {
				return !!_.find($scope.groups, function ( group ) {
					return group.value === $scope.openGroup;
				});
			}
			
			$scope.getCrud = function ( group ) {
				if(group.value !== $scope.openGroup) {
					return null;
				}
				return crud;
			};
			
			$scope.onGroupClick = function ( group ) {
				if($scope.openGroup === group.value) {
					$scope.setOpenGroup(null);
				} else {
					$scope.setOpenGroup(group.value);
				}
			};
			
			$scope.setDisplayMode = function ( mode ) {
				$scope.displayMode = mode;
			};
			
			$scope.getLocations = function ( ) {
				var locations = [],
					mapConfig = $scope.map || {},
					locationResolve = mapConfig.resolve,
					parser = $parse(locationResolve),
					location;
					
				if(!locationResolve) {
					return locations;
				}
				
				_.each(crudItems, function ( crudItem ) {
					var lat,
						lng,
						expl,
						data = {},
						mapping = mapConfig.mapping || {};
											
					location = parser(crudItem);
					
					if(location) {
						expl = location.substr(1, location.length-2).split(',');
						lat = parseFloat(expl[0]);
						lng = parseFloat(expl[1]);
						
						for(var key in mapping) {
							data[key] = $parse(mapping[key])(crudItem);
						}
						
						locations.push({
							item: crudItem,
							marker: {
								latitude: lat,
								longitude: lng,
								popup_data: data,
								no_popup: 1
							}
						});
					}
				});
				
				return locations;
			};
			
			$scope.handleCrudDataChange = function ( $response ) {
				count = $response.num_rows;
			};
			
			$scope.getNumResults = function ( ) {
				var countLabel = count;
				
				if(count > maxCount) {
					countLabel = maxCount + '+';
				}
				
				return countLabel;
			};
			
			$scope.hasResults = function ( ) {
				return !$scope.isLoading() && !isNaN(count) && hasOpenGroup();
			};
			
			this.exceedsLimit = function ( ) {
				return searchService.getMaxResults() < count;
			};
			
			$scope.$on('activeSearch.change', function ( ) {
				$scope.displayMode = 'list';
			});
			
			$scope.$watch('groups', function onGroupChange ( ) {
				
				if(!hasOpenGroup()) {
					$scope.setOpenGroup($scope.groups.length === 1 ? $scope.groups[0].value : null);
				}
				
			}, true);
			
			$scope.$watch(function onChange ( ) {
				rebuildCrud();
			});
			
			$scope.$on('zs.pagination.numrows.change', function onNumRowsChange ( event, nr ) {
				safeApply($scope, function ( ) {
					numRows = nr;
				});
			});
			
			$scope.$on('zs.crud.item.change', function ( event, items ) {
				count = NaN;
				crudItems = items;
			});
			
		}]);
	
})();
