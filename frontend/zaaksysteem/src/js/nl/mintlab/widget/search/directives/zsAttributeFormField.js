/*global angular,fetch,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.widget.search')
		.directive('zsAttributeFormField', [ 'formService', 'searchService', 'searchOperatorService', 'translationService', function ( formService, searchService, searchOperatorService, translationService ) {
			
			var generateUid = fetch('nl.mintlab.utils.generateUid');
			
			return {
				require: 'ngModel',
				scope: true,
				controller: [ '$scope', function ( $scope ) {
					
					function saveList ( ) {
						$scope.saveList();
					}
					
					return {
						saveList: saveList
					};
				}],
				link: function ( scope, element, attrs, ngModel ) {
					
					var safeApply = fetch('nl.mintlab.utils.safeApply'),
						attrForm = null,
						attrConfigForm = null;
					
					function saveList ( ) {
						
						var form = formService.get('attributeForm'),
							val;
						
						if(scope.selectedAttribute) {
							saveAttribute();
						}
						
						val = {
							attributes: form.getValue('attributes'),
							grouping: form.getValue('grouping')
						};
						
						ngModel.$setViewValue(val);
					}
					
					function setOptions ( field, attribute ) {
						var options = [],
							values = attribute.object.values || [];
							
						values = _.sortBy(values, 'sort_order');
						_.each(values, function ( val ) {
							options.push({
								value: val.value,
								label: val.value,
								always_visible: val.active
							});
							
						});
						
						field.data = _.merge(field.data || {}, {
							options: options
						});
						
					}
					
					function saveAttribute ( ) {
						var attr = scope.selectedAttribute,
							resolvedValues = formService.get('attributeConfigForm').getValues(true),
							values = formService.get('attributeConfigForm').getValues(false);
							
						attr._value = resolvedValues.value;
						attr._operator = resolvedValues.operator;
						attr._unresolved = values.value;
					}
					
					function parseAttrForm ( ) {
						var config,
							value = ngModel.$modelValue;
							
						if(!value) {
							value = {
								'attributes': [],
								'grouping': 'and'
							};
						}
						
						config = {
							"name": "attributeForm",
							"fieldsets": [
								{
									"fields": [
										{
											"name": "grouping",
											"type": "select",
											"data": {
												"options": [
													{
														"label": "En",
														"value": "and"
													},
													{
														"label": "En/of",
														"value": "or"
													}
												]
											},
											"value": value.grouping
										},
										{
											"name": "attributes",
											"type": "spot-enlighter",
											"template": "/html/widget/search/attribute-spot-enlighter.html",
											"data": {
												"restrict": "attributes",
												"label": "label",
												"multi": scope.field.data.multi
											},
											"value": value.attributes
										}
									]
								}
							]
						};
						
						attrForm = config;
					}
					
					function parseAttrConfigForm ( ) {
						var attribute = scope.selectedAttribute,
							attrType,
							config,
							operators,
							field,
							fields = [],
							defaultOp;
						
						if(attribute) {
							attrType = attribute.object.value_type;
							operators = searchOperatorService.getOperatorsForAttributeType(attrType);
							if(operators && operators.length) {
								defaultOp = operators[0];
								if(attribute._operator === undefined) {
									attribute._operator = defaultOp.value;
								}
								fields.push({
									"name": "operator",
									"type": "select",
									"data": {
										"options": operators
									},
									"value": attribute._operator,
									"default": defaultOp.value
								});
							}
							
							field = formService.getFormFieldForAttributeType(attrType);
							if(field.type === 'radio' || field.type === 'select') {
								field.type = 'checkbox-list';
							}
							
							if(field.type === 'textarea') {
								field.type = 'text';
							}
							
							if(field.type === 'checkbox-list') {
								setOptions(field, attribute);
								field.data.maxVisible = 0;
								field.data.labels = {
									"more": translationService.get('Toon inactieve waarden'),
									"less": translationService.get('Verberg inactieve waarden')
								};
							}
							
							field = _.merge(field, {
								"name": "value",
								"value": attribute._unresolved
							});
							fields.push(field);
							
							config = {
								"name": "attributeConfigForm",
								"options": {
									"autosave": true
								},
								"fieldsets": [
									{
										"fields": fields
									}
								]
							};
						}
						
						attrConfigForm = config;
					}
					
					scope.setSelectedAttribute = function ( attribute ) {
						scope.selectedAttribute = attribute;
						parseAttrConfigForm();
					};
					
					scope.getSelectedAttribute = function ( ) {
						return scope.selectedAttribute;
					};
					
					scope.getAttributeForm = function ( ) {
						return attrForm;
					};
					
					scope.getAttributeConfigForm = function ( ) {
						return attrConfigForm;
					};
					
					scope.saveList = function ( ) {
						saveList();	
					};
					
					scope.handleRemoveAttrClick = function ( obj ) {
						if(scope.selectedAttribute === obj) {
							scope.setSelectedAttribute(null);
						}
						scope.saveList();
					};
					
					parseAttrForm();
					
					scope.$on('zs.spotenlighter.object.select', function ( event, object ) {
						safeApply(scope, function ( ) {
							// FIXME
							if(object && object.object !== undefined && object.object.value_type !== undefined) {
								scope.setSelectedAttribute(object);
								object._id = generateUid();
							}
						});
					});
					
					ngModel.$formatters.push(function ( val ) {
						var form = formService.get('attributeForm');
						if(_.isObject(val) && form) {
							form.setValue('attributes', val.attributes);
							form.setValue('grouping', val.grouping);
						} else {
							parseAttrForm();
						}
						return val;
					});
				}
			};
			
		}]);
	
})();