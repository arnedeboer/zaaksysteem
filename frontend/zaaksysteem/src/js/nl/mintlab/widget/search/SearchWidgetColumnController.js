/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.widget.search')
		.controller('nl.mintlab.widget.search.SearchWidgetColumnController', [ '$scope', function ( $scope ) {
			
			var q;
			
			$scope.column = {
				query: '',
				loading: false
			};
			
			$scope.columnResults = [];
			
			function search ( ) {
				
				if($scope.column.query !== q) {
					q = $scope.column.query;
					
					$scope.getSearchColumns(q)
						.then(function ( columns ) {
							$scope.columnResults = columns;
						})
						['finally'](function ( ) {
							$scope.column.loading = false;
						});
				}
				
			}
			
			$scope.isSelected = function ( column ) {
				return _.find($scope.getActiveColumns(), { 'id': column.id });
			};
			
			$scope.toggleColumn = function ( column ) {
				var selected = $scope.isSelected(column);
				
				if(selected) {
					$scope.$emit('crud.column.remove', column);
				} else {
					$scope.$emit('crud.column.add', column);
				}
			};
			
			$scope.handleQueryChange = function ( ) {
				$scope.column.loading = true;
			};
			
			search();
			
			$scope.$on('form.change.committed', function ( ) {
				search();
			});
			
		}]);
	
})();