/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.widget.search')
		.controller('nl.mintlab.widget.search.FilterSettingsController', [ '$scope', 'translationService', 'userService', function ( $scope, translationService, userService ) {
			
			$scope.positions = [];
			
			$scope.getPositions = function ( ) {
				var filtered,
					uniques,
					positions = [];
					
				filtered = _.filter($scope.filter.security_rules, { 'entity_type': 'position' });
					
				uniques = _.uniq(filtered, function ( rule ) {
					return rule.entity_id + '/' + rule.entity_type;
				});
				
				_.each(uniques, function ( entity ) {
					if(!(entity.entity_id === userService.getCurrentUuid() && entity.entity_type === 'user')) {
						positions.push({
							entity_type: entity.entity_type,
							entity_id: entity.entity_id,
							tracking_id: entity.entity_id + '/' + entity.entity_type
						});
					}
				});
				
				
				return positions;
			};
			
			$scope.handlePublicClick = function ( ) {
				
				$scope.filter.values.public = !$scope.filter.values.public;
				
				$scope.search.saveFilter($scope.filter)
					.then(
						null,
						function ( /*filter*/ ) {
							$scope.$emit('systemMessage', {
								type: 'error',
								content: translationService.get('Er ging iets fout bij het opslaan van de wijzigingen. Probeer het later opnieuw')
							});
						}
					);
			};
			
			// $watchCollection fails and is not usable for ng-repeat
			$scope.$watch('getPositions()', function ( nw/*, old, scope*/ ) {
				$scope.positions = nw;
			}, true);
			
		}]);
		
})();