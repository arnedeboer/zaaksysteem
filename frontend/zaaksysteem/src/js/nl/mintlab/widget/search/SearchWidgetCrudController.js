/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.widget.search')
		.controller('nl.mintlab.widget.search.SearchWidgetCrudController', [ '$scope', '$parse', 'zqlEscapeFilter', 'translationService', function ( $scope, $parse, zqlEscapeFilter, translationService ) {
			
			function getObjectTypeName ( ) {
				var objType = $scope.getObjectType(),
					name;
					
				if(objType) {
					name = objType.object_type;
				}
					
				return name;
			}
			
			function getItemIdKey ( ) {
				var key;
				switch(getObjectTypeName()) {
					default:
					key = 'id';
					break;
					
					case 'case':
					key = 'values[\'case.number\']';
					break;
				}
				return key;
			}
			
			function getItemIdName ( ) {
				var name;
				switch(getObjectTypeName()) {
					default:
					name = 'object.uuid';
					break;
					
					case 'case':
					name = 'case.number';
					break;
				}
				return name;
			}
			
			function getItemId ( item ) {
				return $parse(getItemIdKey())(item);
			}
			
			$scope.getSelectionZql = function ( params ) {
				var selection = $scope.zsCrud.getSelection(),
					selectionType = $scope.zsCrud.getSelectionType(),
					objType = $scope.getObjectType(),
					zqlWhere = $scope.getZqlWhere(),
					zqlMatching = $scope.getZqlMatching(),
					where = '',
					matching = '',
					ids;
					
				if(selectionType === 'subset') {
					where = getItemIdName() + ' IN ';
					ids = _.map(selection, function ( item ) {
						return zqlEscapeFilter(getItemId(item));
					});
					where += '(' + ids.join(',') + ')';
				} else {
					where = zqlWhere;
					matching = zqlMatching;
				}
				
				return '/api/object/search/?zapi_no_pager=1&zql=SELECT ' + $scope.getZqlColumns(_.pick(params, 'without')) + ' FROM ' + objType.object_type + (where ? (' WHERE ' + where) : '') + (matching ? ' MATCHING ' + matching : '');
			};
			
			$scope.getEzraDialogSelectionParameters = function ( /*action*/ ) {
				var selection = $scope.zsCrud.getSelection(),
					selectionIds,
					selectionType;
					
				selectionIds = _.map(selection, getItemId);
				
				if($scope.zsCrud.getSelectionType() === 'all') {
					selectionType = 'zql';
				} else {
					selectionType = 'selected_cases';
				}
				
				return {
					'selected_case_ids': selectionIds.join(','),
					// TODO: move this somewhere else
					'selection': selectionType,
					'zql': $scope.getZql(),
					'no_redirect': 1
				};
			};
			
			$scope.getExportZql = function ( ) {
				
				var zql = $scope.getSelectionZql({
						without: [ 'case.num_unaccepted_updates', 'case.num_unaccepted_files', 'case.destructable' ]
					});
				
				return zql;
			};
			
			$scope.$on('zs.search.bulk.attempt', function ( ) {
				$scope.$parent.loading.bulk = true;
			});
			
			$scope.$on('zs.search.bulk.success', function ( event, messages ) {
				$scope.$parent.loading.bulk = false;
				$scope.zsCrud.reloadData();
				
				if(!messages.length) {
					messages.push({
						message: 'Actie succesvol uitgevoerd op de geselecteerde zaken'
					});
				}
				
				_.each(messages, function ( msg ) {
					$scope.$emit('systemMessage', {
						type: 'info',
						content: msg.message
					});
				});
			});
			
			$scope.$on('zs.search.bulk.error', function ( event, messages) {
				$scope.$parent.loading.bulk = false;
				$scope.$emit('systemMessage', {
					type: 'error',
					content: messages ? messages[0] : translationService.get('Er ging iets fout bij het uitvoeren van de acties. Probeer het opnieuw')
				});
			});
			
			$scope.$watch('loading', function ( ) {
				$scope.setCrudLoading($scope.loading);
			});
			
			$scope.$on('zs.crud.item.change', function ( event, items ) {
				_.each(items, function ( item ) {
					item._permissions = {};
					_.each(item.actions, function ( actionId ) {
						item._permissions[actionId] = true;
					});
				});
			});
			
			$scope.$on('search.reload', function ( ) {
				if($scope.zsCrud) {
					$scope.zsCrud.reloadData();
				}
			});
			
		}]);
	
})();
