/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.kcc')
		.controller('nl.mintlab.kcc.PhoneCallController', [ '$scope', 'callService', 'translationService', function ( $scope, callService, translationService ) {
			
			$scope.acceptCall = function ( ) {
				callService.acceptCall($scope.activeTabData.id)
					.error(function ( ) {
						$scope.$emit('systemMessage', {
							type: 'error',
							content: translationService.get('Er ging iets fout bij het accepteren van dit telefoontje. Probeer het opnieuw.')
						});
					});
			};
			
			$scope.rejectCall = function ( ) {
				callService.rejectCall($scope.activeTabData.id)
					.error(function ( ) {
						$scope.$emit('systemMessage', {
							type: 'error',
							content: translationService.get('Er ging iets fout bij het weigeren van dit telefoontje. Probeer het opnieuw.')
						});
					});
			};
			
		}]);
})();