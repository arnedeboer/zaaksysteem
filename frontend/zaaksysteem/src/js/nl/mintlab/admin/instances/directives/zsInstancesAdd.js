/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.admin.instances')
		.directive('zsInstancesAdd', [ 'instancesService', 'systemMessageService', function ( instancesService, systemMessageService ) {
			
			return {
				require: [ 'zsInstancesAdd', '^zsInstancesView' ],
				controller: [ function ( ) {
					
					var ctrl = {},
						loading = false,
						config = null,
						zsInstancesView;
					
					function getCustomerId ( ) {
						return zsInstancesView ? zsInstancesView.getCustomerId() : null;
					}
					
					ctrl.link = function ( controllers ) {
						zsInstancesView = controllers[0];
					};
					
					ctrl.getFormConfig = function ( ) {
						return config;
					};
					
					ctrl.isLoading = function ( ) {
						return loading;
					};
					
					ctrl.createInstance = function ( values ) {
						
						var customerId = getCustomerId(),
							vals = _.pick(values, [ 'title', 'label', 'release', 'origin', 'ipv4', 'ipv6', 'loadbalancer', 'cloud', 'database', 'fallback', 'aliases' ]);
						
						loading = true;
						
						vals.fqdn = values.host + '.' + zsInstancesView.getCustomerConfig().values.domain;
						vals.disabled = !values.active;
						vals.owner = customerId;
						
						_.each(vals, function ( value, key ) {
							if(value === null) {
								delete vals[key];
							}
						});
						
						zsInstancesView.createInstance(vals)
							['catch'](function ( ) {
								systemMessageService.emitSaveError('deze omgeving');
							})
							['finally'](function ( ) {
								loading = false;
							});
					};
						
					instancesService.loadReleaseOptions()
						.then(function ( ) {
							config = angular.copy(instancesService.getCustomerDForm());
						});
					
					return ctrl;
				}],
				controllerAs: 'instancesAdd',
				link: function ( scope, element, attrs, controllers ) {
					
					controllers[0].link(controllers.splice(1));
					
				}
			};
			
		}]);
	
})();
