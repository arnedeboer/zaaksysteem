/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.admin')
		.directive('zsInstancesEdit', [ '$q', '$interpolate', '$parse', 'instancesService', 'systemMessageService', function ( $q, $interpolate, $parse, instancesService, systemMessageService ) {
			
			return {
				controller: [ '$scope', '$attrs', function ( $scope, $attrs ) {
					
					var ctrl = {},
						config,
						instance = $scope.$eval($attrs.instance),
						zsInstancesView;
						
					$q.all([
						instancesService.loadReleaseOptions()
					])
						.then(function ( ) {
							
							var customerConfig = ctrl.getCustomerConfig();
							
							config = angular.copy(instancesService.getCustomerDForm());
							
							config.fields = _.reject(config.fields, function ( field ) {
								return _.indexOf([ 'release', 'origin', 'active' ], field.name) !== -1;
							});
							
							config.actions = [ 
								{
									id: 'submit',
									type: 'submit',
									label: 'Opslaan'
								}
							];
							
							_.each(config.fields, function ( field ) {
								var value;
								switch(field.name) {
									case 'host':
									value = instance.values.fqdn.replace('.' + customerConfig.values.domain, '');
									break;
									
									default:
									value = instance.values[field.name];
									break;
								}
								field.value = value;
							});
							
						});
					
					ctrl.getCustomerConfig = function ( ) {
						return $scope.$eval($attrs.customerConfig);
					};
					
					ctrl.getFormConfig = function ( ) {
						return config;
					};
					
					ctrl.updateInstance = function ( $values ) {
						var values = angular.copy($values);
					
						values.fqdn = values.host + '.' + ctrl.getCustomerConfig().values.domain;
						
						delete values.host;
						
						_.each(values, function ( value, key ) {
							if(value === null) {
								delete values[key];
							}
						});
							
						instancesService.updateInstance(instance, values)
							.then(function ( ) {
								systemMessageService.emitSave();
								$parse($attrs.zsInstancesEditSuccess)($scope, { $instance: instance });
							})
							['catch'](function ( error ) {
								systemMessageService.emitSaveError();
								$parse($attrs.zsInstancesEditSuccess)($scope, { $instance: instance, $error: error });
							});
					};
					
					return ctrl;
					
				}],
				controllerAs: 'instancesEdit'
			};
			
		}]);
	
})();