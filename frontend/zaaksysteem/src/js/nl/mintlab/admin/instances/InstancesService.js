/*global angular,EventEmitter2,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.admin.instances')
		.factory('instancesService', [ '$q', '$http', 'objectService', 'zqlService', 'zqlEscapeFilter', function ( $q, $http, objectService, zqlService, zqlEscapeFilter ) {
			
			var instancesService = new EventEmitter2({
					wildcard: true
				}),
				templateOptions = {
					accept: "A copy of your accept template",
					production: "A copy of your production template",
					initial: "An empty Zaaksysteem with predefined case types",
					clean: "An empty Zaaksysteem, no case types",
					training: "Training template"
				},
				releaseOptions = {
					integration: "quarterly",
					accept: "accept",
					production: "master",
					development: "sprint",
					hotfix: "hotfix"
				},
				originTypes = [
					{
						type: 'production',
						label: 'Productie'
					},
					{
						type: 'accept',
						label: 'Acceptatie'
					},
					{
						type: 'from_template',
						label: 'Template'
					}
				],
				configs = {},
				customerDForm,
				templatePromise,
				releasePromise;
			
			function getOriginOptions ( ) {
				return _.map(instancesService.getOriginTypes(), function ( type ) {
					return {
						value: type.type,
						label: type.label
					};
				});
			}
			
			instancesService.loadCustomerConfig = function ( customerId ) {
				var deferred = configs[customerId];
				
				if(deferred) {
					return deferred.promise;
				}
				
				deferred = configs[customerId] = $q.defer();
				
				zqlService.select('customer_config', [
					{
						name: 'customer_id',
						value: customerId
					}
				])
					.success(function ( response ) {
						deferred.resolve(response.result[0]);
					})
					.error(function ( response ) {
						deferred.reject(response.result[0]);
					});
					
				return deferred.promise;
			};
			
			instancesService.loadInstances = function ( customerId ) {
				var promise = $q.defer(),
					zql = 'SELECT {} FROM customer_d';
					
				if(customerId) {
					zql += ' WHERE owner = ' + zqlEscapeFilter(customerId);
				}
				
				zqlService.query(zql)
					.success(function ( response ) {
						promise.resolve(response.result);
					})
					.error(function ( response ) {
						promise.reject(response);
					});
				
				return promise.promise;
			};
			
			instancesService.createInstance = function ( values ) {
				
				var instance = objectService.createObject('customer_d');
				instance.values = values;
				
				return objectService.saveObject(instance)
					.then(function ( ) {
						instancesService.emit('instance.add', instance);
					});
			};
			
			instancesService.deleteInstance = function ( instance ) {
				return instancesService.updateInstance(instance, {
					deleted: true
				});
			};
			
			instancesService.recoverInstance = function ( instance ) {
				return instancesService.updateInstance(instance, {
					deleted: false
				});
			};
			
			instancesService.deactivateInstance = function ( instance ) {
				return instancesService.updateInstance(instance, {
					disabled: true
				});
			};
			
			instancesService.activateInstance = function ( instance ) {
				return instancesService.updateInstance(instance, {
					disabled: false
				});
			};
			
			instancesService.updateInstance = function ( instance, params ) {
				return objectService.updateObject(instance, params);
			};
			
			instancesService.getOriginTypes = function ( ) {
				return originTypes;	
			};
			
			instancesService.getCustomerDForm = function ( ) {
				return customerDForm;	
			};
			
			instancesService.loadTemplateOptions = function ( ) {
				var deferred,
					options;
				
				if(!templatePromise) {
					deferred = $q.defer();
					
					templatePromise = deferred.promise;
					
					options = _.map(templateOptions, function ( value, key ) {
						return {
							value: key,
							label: value
						};
					});
					
					deferred.resolve(options);
				}
				
				return templatePromise;
			};
			
			instancesService.loadReleaseOptions = function ( ) {
				var deferred,
					options,
					field;
				
				if(!releasePromise) {
					deferred = $q.defer();
					
					releasePromise = deferred.promise;
					
					options = _.map(releaseOptions, function ( value, key ) {
						return {
							value: key,
							label: value
						};
					});
					
					field = _.find(customerDForm.fields, { name: 'release' });
					
					field.data.options = options;
					field['default'] = options[0].value;
					
					deferred.resolve(options);
				}
				
				return releasePromise;
			};
			
			customerDForm = {
				name: 'instance-form',
				fields: [
					{
						name: 'label',
						label: 'Titel',
						type: 'text',
						required: true
					},
					{
						name: 'host',
						label: 'Host',
						type: 'text',
						required: true,
						data: {
							pattern: '/^[-A-Za-z0-9.]{3,255}$/'
						},
						template: '/html/admin/instances/instances.html#host'
					},
					{
						name:  'release',
						label: 'Type omgeving',
						type: 'select',
						data: {

							options: []
						}
					},
					{
						name: 'origin',
						label: 'Oorsprong',
						type: 'select',
						data: {
							options: getOriginOptions()
						},
						'default': getOriginOptions()[0].value
					},
					{
						name: 'active',
						label: 'Activeren',
						type: 'checkbox',
						data: {
							checkboxlabel: 'Direct'
						},
						'default': true
					},
					{
						name: 'ipv4',
						label: 'IPv4',
						type: 'text',
						data: {
							validators: [ 'ipv4' ]
						},
						required: true
					},
					{
						name: 'ipv6',
						label: 'IPv6',
						type: 'text',
						data: {
							validators: [ 'ipv6' ]
						},
						required: false
					},
					{
						name: 'certificate',
						label: 'SSL certificaat',
						type: 'textarea',
						required: false
					},
					{
						name: 'key',
						label: 'SSL key',
						type: 'textarea',
						required: false
					},
					{
						name: 'loadbalancer',
						type: 'select',
						label: 'Loadbalancer',
						data: {
							options: [
								{
									value: 'commercial',
									label: 'Commerciele cloud'
								},
								{
									value: 'dev',
									label: 'Mintlab development cloud'
								},
								{
									value: 'gov',
									label: 'Overheidscloud'
								},
								{
									value: 'gov-acc',
									label: 'Overheidscloud - accept'
								},
								{
									value: 'ontwikkel',
									label: 'Ontwikkelingscloud'
								},
								{
									value: 'training',
									label: 'Trainingscloud'
								}
							]
						},
						'default': 'commercial',
						required: true
					},
					{
						name: 'cloud',
						type: 'select',
						label: 'Cloud',
						data: {
							options: [
								{
									value: 'commercial',
									label: 'Commerciele cloud'
								},
								{
									value: 'dev',
									label: 'Mintlab development cloud'
								},
								{
									value: 'gov',
									label: 'Overheidscloud'
								},
								{
									value: 'gov-acc',
									label: 'Overheidscloud - accept'
								},
								{
									value: 'ontwikkel',
									label: 'Ontwikkelingscloud'
								},
								{
									value: 'training',
									label: 'Trainingscloud'
								}
							]
						},
						'default': 'commercial',
						required: true
					},
					{
						name: 'fallback',
						type: 'url',
						label: 'Fallback',
						required: true
					},
					{
						name: 'database',
						type: 'text',
						label: 'Database',
						required: false
					},
					{
						name: 'aliases',
						type: 'list',
						label: 'Aliases',
						required: false
					}
				],
				actions: [
					{
						id: 'submit',
						type: 'submit',
						label: 'Aanmaken'
					}
				]
			};
			
			return instancesService;
			
		}]);
	
})();
