/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.admin.instances')
		.directive('zsInstancesCrud', [ '$q', 'instancesService', 'systemMessageService', 'translationService', 'dateFilter', function ( $q, instancesService, systemMessageService, translationService, dateFilter ) {
			
			var config = {
				columns: [
					{
						id: 'label',
						label: 'Titel',
						resolve: 'values.label',
						locked: true,
						dynamic: true
					},
					{
						id: 'fqdn',
						label: 'url',
						resolve: 'values.fqdn',
						locked: true,
						dynamic: true
					},
					{
						id: 'status',
						label: 'Status',
						templateUrl: '/html/admin/instances/instances.html#status',
						locked: true,
						dynamic: true
					},
					{
						id: 'actions',
						label: 'Acties',
						templateUrl: '/html/admin/instances/instances.html#crud-menu',
						locked: true,
						dynamic: true,
						sort: false
					}
				],
				options: {
					select: 'single'
				},
				actions: [
					{
						id: 'edit',
						type: 'popup',
						label: 'Bewerken',
						data: {
							url: '/html/admin/instances/instances.html#edit'
						},
						when: 'selectedItems.length&&instancesCrud.canEdit()&&!instancesCrud.isDeleted(selectedItems[0])'
					},
					{
						id: 'access',
						type: 'popup',
						label: 'Toegangsbeheer',
						data: {
							url: '/html/admin/instances/instances.html#access'
						},
						when: 'selectedItems.length&&!instancesCrud.isDeleted(selectedItems[0])'
					},
					{
						id: 'activate',
						type: 'click',
						label: 'Activeren',
						data: {
							click: 'instancesCrud.activate($selection[0])'
						},
						when: 'selectedItems.length&&!instancesCrud.isDeleted(selectedItems[0])&&!instancesCrud.isActive(selectedItems[0])'
					},
					{
						id: 'deactivate',
						type: 'confirm',
						label: 'Deactiveren',
						data: {
							click: 'instancesCrud.deactivate($selection[0])',
							label: 'Weet u zeker dat u deze omgeving wil deactiveren?',
							verb: 'Deactiveer'
						},
						when: 'selectedItems.length&&!instancesCrud.isDeleted(selectedItems[0])&&instancesCrud.isActive(selectedItems[0])'
					},
					{
						id: 'delete',
						type: 'confirm',
						label: 'Verwijderen',
						data: {
							click: 'instancesCrud.deleteInstance($selection[0])',
							label: 'Weet u zeker dat u deze omgeving wil verwijderen?',
							verb: 'Verwijder'
						},
						when: 'selectedItems.length&&!instancesCrud.isDeleted(selectedItems[0])'
					},
					{
						id: 'recover',
						type: 'click',
						label: 'Herstellen',
						data: {
							click: 'instancesCrud.recover($selection[0])'
						},
						when: 'selectedItems.length&&instancesCrud.isDeleted(selectedItems[0])'
					}
				],
				style: {
					classes: {
						'instance-crud-item-deleted': 'instancesCrud.isDeleted(item)',
						'instance-crud-item-active': 'instancesCrud.isActive(item)',
						'instance-crud-item-pending': 'instancesCrud.hasPendingChanges(item)',
						'instance-crud-item-inactive': '!instancesCrud.isActive(item)&&!instancesCrud.isDeleted(item)' 
					}
				},
				url: undefined
			};
			
			return {
				require: [ 'zsInstancesCrud', '^zsInstancesView' ],
				controller: [ function ( ) {
					
					var ctrl = {},
						dataProvider,
						zsInstancesView;
						
					ctrl.options = {
						showDeleted: false
					};
					
					ctrl.link = function ( controllers ) {
						zsInstancesView = controllers[0];
					};
					
					ctrl.getInstances = function ( $sortBy, $sortReversed ) {
						var instances = zsInstancesView ? zsInstancesView.getInstances() : null;
						
						if($sortBy) {
							instances = _.sortBy(instances, $sortBy);
							if($sortReversed) {
								instances.reverse();
							}
						}
						
						return instances;
					};
					
					ctrl.getCrudConfig = function ( ) {
						return config;	
					};
					
					ctrl.activate = function ( instance ) {
						instancesService.activateInstance(instance);
					};
					
					ctrl.deactivate = function ( instance ) {
						instancesService.deactivateInstance(instance);
					};
					
					ctrl.deleteInstance = function ( instance ) {
						instancesService.deleteInstance(instance);
					};
					
					ctrl.recover = function ( instance ) {
						instancesService.recoverInstance(instance);	
					};
					
					ctrl.isActive = function ( instance ) {
						return !ctrl.isDeleted(instance) && !instance.values.disabled;
					};
					
					ctrl.isDeleted = function ( instance ) {
						return !!instance.values.deleted;	
					};
					
					ctrl.hasPendingChanges = function ( instance ) {
						return instance.values.provisioned_on === undefined || (new Date(instance.values.provisioned_on).getTime() >= new Date(instance.date_modified));
					};
					
					ctrl.getStatus = function ( instance ) {
						
						var status;
						
						if(ctrl.hasPendingChanges(instance)) {
							status = 'pending';
						} else if(ctrl.isDeleted(instance)) {
							status = 'deleted';
						} else if(!ctrl.isActive(instance)) {
							status = 'inactive';
						} else {
							status = 'active';
						}
						
						return status;
						
					};
					
					ctrl.getStatusLabel = function ( instance ) {
						
						var status = ctrl.getStatus(instance),
							label;
							
						switch(status) {
							
							case 'pending':
							label = translationService.get('Bezig');
							break;
							
							case 'active':
							label = translationService.get('Actief');
							break;
							
							case 'inactive':
							label = translationService.get('Inactief');
							break;
							
							case 'deleted':
							label = translationService.get('Wordt verwijderd');
							if(instance.values.delete_on !== undefined) {
								label += 'op ' + dateFilter(instance.values.delete_on, 'mediumDate');
							}
							break;
						}
						
						return label;
						
					};
					
					ctrl.setDataProvider = function ( provider ) {
						dataProvider = provider;
					};
					
					ctrl.getCustomerConfig = function ( ) {
						return zsInstancesView ? zsInstancesView.getCustomerConfig() : null;	
					};
					
					ctrl.canEdit = function ( ) {
						return true;
					};
					
					return ctrl;
				}],
				controllerAs: 'instancesCrud',
				templateUrl: '/html/admin/instances/instances.html',
				link: function ( scope, element, attrs, controllers ) {
					
					controllers[0].link(controllers.splice(1));
					
				}
			};
			
		}]);
	
})();