/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.admin.instances')
		.directive('zsInstancesView', [ '$q', '$http', 'instancesService', 'objectService', 'systemMessageService', function ( $q, $http, instancesService, objectService, systemMessageService ) {
			
			return {
				require: [ 'zsInstancesView', '^?zsSubjectView' ],
				controller: [ '$scope', '$element', '$attrs', function ( $scope, $element, $attrs ) {
					
					var ctrl = this,
						zsSubjectView,
						customerConfig,
						instances = [],
						loaded = false,
						creating = false,
						instancePromise;
						
					function reloadInstances ( ) {
						instancePromise = 
							instancesService.loadInstances(ctrl.getControlPanelId())
								.then(function ( inst/*, pager*/ ) {
									instances = inst;
									return inst;
								});
					 	return instancePromise;
					}
					
					$scope.domain = '';
					
					ctrl.link = function ( controllers ) {
						zsSubjectView = controllers[0];
						
						instancesService.loadCustomerConfig(ctrl.getCustomerId())
							.then(function ( config ) {

								customerConfig = config;
								
								$scope.domain = ctrl.getDomain();
								
								if(customerConfig) {
									return reloadInstances();
								}
							})
							['catch'](function ( ) {
								throw systemMessageService.emitLoadError();
							})
							['finally'](function ( ) {
								loaded = true;
							});
					};
					
					ctrl.getCustomerId = function ( ) {
						return zsSubjectView ? zsSubjectView.getSubjectId() : $attrs.customerId;
					};
					
					ctrl.hasCustomerConfig = function ( ) {
						return customerConfig;	
					};
					
					ctrl.getCustomerConfig = function ( ) {
						return customerConfig;	
					};
					
					ctrl.getInstances = function ( ) {
						return instancePromise;
					};
					
					ctrl.isLoaded = function ( ) {
						return loaded;	
					};
					
					ctrl.createCustomer = function ( type ) {
						
						creating = true;
						
						return instancesService.createControlPanel(ctrl.getCustomerId(), type)
							.then(function ( ) {
								customerConfig = arguments[0];
								
								$scope.domain = ctrl.getDomain();
								
								systemMessageService.emit('info', 'Initialisatie geslaagd. U kunt nu omgevingen aanmaken.');
							})
							['catch'](function ( ) {
								systemMessageService.emitError('Er ging iets bij het initialiseren van de omgevingen. Probeer het later opnieuw.');
							})
							['finally'](function ( ) {
								creating = false;
							});
					};
					
					ctrl.isCreating = function ( ) {
						return creating;	
					};
					
					ctrl.createInstance = function ( values ) {
						return instancesService.createInstance(ctrl.getControlPanelId(), values);
					};
					
					ctrl.isPip = function ( ) {
						return $scope.$eval($attrs.pip);	
					};
					
					ctrl.isPipWritable = function ( ) {
						var isPipWritable = ctrl.isPip() && $scope.$eval($attrs.pipWrite),
							isReadOnly = false;

						if(ctrl.getCustomerConfig()) {
							isReadOnly = !!ctrl.getCustomerConfig().instance.read_only;
						}

						return isPipWritable && !isReadOnly;
					};
					
					ctrl.getDomain = function ( ) {
						return customerConfig ? customerConfig.instance.domain : '';
					};
					
					ctrl.getControlPanelId = function ( ) {
						var customerConfig = ctrl.getCustomerConfig(),
							id;
							
						if(customerConfig) {
							id = customerConfig.reference;
						}
						
						return id;
					};
					
					ctrl.onHostUpdate = function ( /*hosts*/ ) {
						reloadInstances();	
					};
					
					ctrl.getActiveInstancesLabel = function ( ) {
						var numTotal = instances.length,
							label;

						var customerConfig = ctrl.getCustomerConfig();

						if(!numTotal) {
							label = '-';
						} else {
							label = numTotal + ' van ' + customerConfig.instance.allowed_instances;
						}
								
						return label;
					};
					
					ctrl.getDiskspaceLabel = function ( ) {
						var label = '-',
							allowedDiskspace;

						if(customerConfig) {
							allowedDiskspace = customerConfig.instance.allowed_diskspace;

							label = Math.round(customerConfig.instance.stat_diskspace) + '/' + (allowedDiskspace || allowedDiskspace === 0 ? allowedDiskspace : '∞') + 'GB';
						}

						return label;
					};
					
					ctrl.getUptimeLabel = function ( ) {
						return instances.length ? '99,989%' : '-';
					};
					
					function onInstanceAdd ( instance) {
						instances.push(instance);
					}
					
					instancesService.on('instance.add', onInstanceAdd);
					
					$scope.$on('$destroy', function ( ) {
						instancesService.off('instance.add', onInstanceAdd);
					});
					
					return ctrl;
					
				}],
				controllerAs: 'instancesView',
				link: function ( scope, element, attrs, controllers ) {
					
					controllers[0].link(controllers.splice(1));
					
				}
			};
			
		}]);
	
	
})();
