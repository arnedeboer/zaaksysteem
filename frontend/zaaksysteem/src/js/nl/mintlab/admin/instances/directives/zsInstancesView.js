/*global angular,_,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem.admin.instances')
		.directive('zsInstancesView', [ '$q', '$http', 'instancesService', 'objectService', 'systemMessageService', function ( $q, $http, instancesService, objectService, systemMessageService ) {
			
			var trim = fetch('nl.mintlab.utils.shims.trim');
			
			return {
				require: [ 'zsInstancesView', '^?zsSubjectView' ],
				controller: [ '$scope', '$element', '$attrs', function ( $scope, $element, $attrs ) {
					
					var ctrl = {},
						zsSubjectView,
						customerConfig,
						instances = [],
						loaded = false,
						promises = [];
					
					ctrl.link = function ( controllers ) {
						zsSubjectView = controllers[0];
						
						if(ctrl.isPip()) {
							promises = [
								$http({
									method: 'GET',
									url: '/pip/get_instances'
								})
									.success(function ( response ) {
										var result = response.result[0];
										
										customerConfig = result.customer_config[0];
										instances = result.customer_d;
										
									})
							];
						} else {
							promises = [
								instancesService.loadCustomerConfig(ctrl.getCustomerId())
									.then(function ( config ) {
										customerConfig = config;
									}),
								instancesService.loadInstances(ctrl.getCustomerId())
									.then(function ( inst ) {
										instances = inst;
									})
							];
						}
						
						$q.all(promises)
							['catch'](function ( ) {
								systemMessageService.emitLoadError();
							})
							['finally'](function ( ) {
								loaded = true;
							});
					};
					
					ctrl.getCustomerId = function ( ) {
						return zsSubjectView ? zsSubjectView.getSubjectId() : $attrs.customerId;
					};
					
					ctrl.hasCustomerConfig = function ( ) {
						return customerConfig;	
					};
					
					ctrl.getCustomerConfig = function ( ) {
						return customerConfig;	
					};
					
					ctrl.getInstances = function ( ) {
						return instances;	
					};
					
					ctrl.isLoaded = function ( ) {
						return loaded;	
					};
					
					ctrl.handleFormSubmit = function ( $values ) {
						var config = objectService.createObject('customer_config'),
							values = {};
							
						_.each($values, function ( value, key) {
							if(value) {
								values[key] = trim(value);
							}
						});
												
						angular.extend(config.values, values);
						config.values.customer_id = ctrl.getCustomerId();
						
						objectService.saveObject(config)
							.then(function ( object ) {
								
								customerConfig = object;
								
							})
							['catch'](function ( ) {
								systemMessageService.emitSaveError('de klantconfiguratie');
							});
					};
					
					ctrl.createInstance = function ( values ) {
						var instance = objectService.createObject('customer_d');
						
						angular.extend(instance.values, values);
						
						instances.push(instance);
						
						return objectService.saveObject(instance)
							['catch'](function ( ) {
								
								_.pull(instances, instance);
								
								throw systemMessageService.emitSaveError('de omgeving');
								
							});
						
					};
					
					ctrl.isPip = function ( ) {
						return $scope.$eval($attrs.pip);	
					};
					
					return ctrl;
					
				}],
				controllerAs: 'instancesView',
				link: function ( scope, element, attrs, controllers ) {
					
					controllers[0].link(controllers.splice(1));
					
				}
			};
			
		}]);
	
	
})();
