/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.admin.instances')
		.directive('zsInstancesConfigForm', [ 'instancesService', 'translationService', function ( instancesService, translationService ) {
			
			return {
				controller: [ function ( ) {
					
					var ctrl = {},
						config = {
							name: 'instancesConfig',
							fields: [
								{
									name: 'template',
									type: 'text',
									label: translationService.get('Standaard template'),
									data: {
										pattern: '/^[A-Za-z0-9_]+$/'
									},
									required: true
								},
								{
									name: 'domain',
									type: 'text',
									label: translationService.get('Domein'),
									required: true
								},
								{
									name: 'naam_lang',
									type: 'text',
									label: translationService.get('Officiële naam'),
									required: true
								},
								{
									name: 'naam_kort',
									type: 'text',
									label: translationService.get('Korte naam'),
									required: true
								},
								{
									name: 'woonplaats',
									type: 'text',
									label: translationService.get('Woonplaats'),
									required: true
								},
								{
									name: 'straatnaam',
									type: 'text',
									label: translationService.get('Straatnaam'),
									required: true
								},
								{
									name: 'huisnummer',
									type: 'text',
									label: translationService.get('Huisnummer'),
									required: true
								},
								{
									name: 'postcode',
									type: 'text',
									label: translationService.get('Postcode'),
									required: true
								},
								{
									name: 'postbus',
									type: 'text',
									label: translationService.get('Postbus'),
									required: false
								},
								{
									name: 'postbus_postcode',
									type: 'text',
									label: translationService.get('Postbus postcode'),
									required: false
								},
								{
									name: 'website',
									type: 'url',
									label: translationService.get('Website'),
									required: true
								},
								{
									name: 'email',
									type: 'email',
									label: translationService.get('Email'),
									required: true
								},
								{
									name: 'telefoonnummer',
									type: 'text',
									label: translationService.get('Telefoonnummer'),
									required: true
								},
								{
									name: 'faxnummer',
									type: 'text',
									label: translationService.get('Faxnummer'),
									required: false
								},
								{
									name: 'latitude',
									type: 'number',
									label: translationService.get('Latitude'),
									required: true
								},
								{
									name: 'longitude',
									type: 'number',
									label: translationService.get('Longitude'),
									required: true
								},
								{
									name: 'gemeente_id_url',
									type: 'url',
									label: translationService.get('Gemeente ID URL'),
									required: true
								},
								{
									name: 'gemeente_portal',
									type: 'url',
									label: translationService.get('Gemeente portal'),
									required: true
								},
								{
									name: 'ad_key',
									type: 'text',
									label: translationService.get('Active Directory key'),
									required: true
								},
								{
									name: 'production_url',
									type: 'url',
									label: translationService.get('Production URL'),
									required: true
								},
								{
									name: 'production_ip',
									type: 'text',
									label: translationService.get('Production IP'),
									required: true
								},
								{
									name: 'accept_url',
									type: 'url',
									label: translationService.get('Accept URL'),
									required: true
								},
								{
									name: 'accept_ip',
									type: 'text',
									label: translationService.get('Accept IP'),
									required: true
								}
							],
							actions: [
								{
									name: 'submit',
									type: 'submit',
									label: 'Opslaan'
								}
							]
						};
					
					ctrl.getFormConfig = function ( ) {
						return config;	
					};
					
					return ctrl;
					
				}],
				controllerAs: 'instancesConfigForm'
			};
			
		}]);
	
})();
