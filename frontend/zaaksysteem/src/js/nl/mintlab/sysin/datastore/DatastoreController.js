/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.sysin.datastore')
		.controller('nl.mintlab.sysin.datastore.DatastoreController', [ '$scope', '$http', 'translationService', '$window', function ( $scope, $http, translationService, $window ) {
			
			$scope.options = [];
			
			$scope.downloadTable = function ( ) {
				$window.open('/datastore/csv/' + $scope.cl);
			};
			
			$http({
				method: 'GET',
				url: '/datastore/classes'
			})
				.success(function ( response ) {
					
					$scope.options = response.result;
					$scope.cl = $scope.options[0];
					
				})
				.error(function ( /*response*/ ) {
					$scope.$emit('systemMessage', {
						type: 'error',
						content: translationService.get('Er ging iets fout bij het ophalen van de gegevens. Probeer het later opnieuw.')
					});
				});
			
		}]);
	
})();