/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.object.view')
		.directive('zsObjectView', [ '$q', 'objectService', 'zqlService', 'systemMessageService', 'translationService', function ( $q, objectService, zqlService, systemMessageService, translationService ) {
			
			return {
				controller: [ '$scope', '$element', '$attrs', function ( $scope, $element, $attrs ) {
					
					var ctrl = {},
						object,
						activeTab,
						loadingDeferred,
						objectType,
						loading;
					
					function getObjectTypePrefix ( ) {
						return object ? object.type : undefined;
					}
					
					function setTabs ( ) {
						var type = getObjectTypePrefix(),
							tabs;
							
						switch(type) {
							default:
							tabs = [
								{
									id: 'fields',
									label: translationService.get('Gegevens'),
									templateUrl: '/html/object/view/fields.html'
								},
								{
									id: 'timeline',
									label: translationService.get('Timeline'),
									templateUrl: '/html/object/view/timeline.html'
								}
							];
							break;
						}
						
						ctrl.tabs = tabs;
						activeTab = ctrl.tabs[0];
					}
					
					ctrl.getObjectTitle = function ( ) {
						return object ? object.label : undefined;
					};
					
					ctrl.getObjectTypeLabel = function ( ) {
						return objectType ? objectType.label : undefined;
					};
					
					ctrl.isActiveTab = function ( tab ) {
						return activeTab === tab;
					};
					
					ctrl.selectTab = function ( tab ) {
						activeTab = tab;
					};

					ctrl.getActiveTab = function ( ) {
						return activeTab;
					};
					
					ctrl.getActiveTabPartial = function ( ) {
						return activeTab ? activeTab.templateUrl : undefined;
					};
					
					ctrl.getObject = function ( ) {
						return object;	
					};
					
					ctrl.getObjectType = function ( ) {
						return objectType;
					};
					
					ctrl.loadObject = function ( ) {
						return loadingDeferred.promise;	
					};
					
					ctrl.getInclude = function ( ) {
						var url;
						
						if(!loading) {
							switch(getObjectTypePrefix()) {
								default:
								url = '/html/object/view/generic.html';
								break;
							}
						}
						
						return url;
					};
					
					ctrl.isLoading = function ( ) {
						return loading;	
					};
					
					ctrl.onUpdateListeners = [];
					
					ctrl.tabs = [];
					
					loading = true;
					
					loadingDeferred = $q.defer();
					
					objectService.getObject($attrs.objectId, { deep_relations: true })
						.then(function ( obj ) {
							object = obj;
							
							zqlService.query('SELECT {} FROM type WHERE prefix = ' + zqlService.escape(getObjectTypePrefix()), { deep_relations: true })
								.success(function ( response ) {
									
									objectType = response.result[0];
									
									if(!objectType && _.indexOf([ 'faq', 'product' ], getObjectTypePrefix()) === -1) {
										loadingDeferred.reject();
									} else {
										loadingDeferred.resolve();
									}
									
								})
								.error(function ( response ) {
									loadingDeferred.reject(response);
								});
							
							
						})
						['catch'](function ( obj ) {
							loadingDeferred.reject(obj);
						});
							
					loadingDeferred.promise.then(function ( ) {
						setTabs();
							_.each(ctrl.onUpdateListeners, function ( listener) { 
								listener();
							});
					})
						['catch'](function ( ) {
							systemMessageService.emitLoadError('het object');
						})
						['finally'](function ( ) {
							loading = false;
						});
					
					return ctrl;
					
				}],
				controllerAs: 'objectView'
			};
			
		}]);
	
})();