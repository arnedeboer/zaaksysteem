/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.timeline', [ 'Zaaksysteem.events', 'Zaaksysteem.net', 'Zaaksysteem.locale', 'Zaaksysteem.filters', 'Zaaksysteem.directives', 'Zaaksysteem.dom' ] );
	
})();
