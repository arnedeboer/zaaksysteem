/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.timeline', [ 'Zaaksysteem.events' ] );
	
})();