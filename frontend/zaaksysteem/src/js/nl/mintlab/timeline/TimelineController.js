/*global angular,fetch,_*/
(function ( ) {
	
	var MAX_CONTENT_LENGTH = 400;
	
	angular.module('Zaaksysteem.timeline')
		.controller('nl.mintlab.timeline.TimelineController', [ '$scope', '$window', '$sce', 'smartHttp', function ( $scope, $window, $sce, smartHttp ) {
			
			var TimelineEvent = fetch('nl.mintlab.timeline.TimelineEvent'),
				indexOf = fetch('nl.mintlab.utils.shims.indexOf'),
				safeApply = fetch('nl.mintlab.utils.safeApply');
			
			var _lastResult,
				_itemsById = {},
				_contactChannels = [
					{
						value: 'behandelaar',
						label: 'Behandelaar'
					},
					{
						value: 'balie',
						label: 'Balie'
					},
					{
						value: 'telefoon',
						label: 'Telefoon'
					},
					{
						value: 'post',
						label: 'Post'
					},
					{
						value: 'email',
						label: 'Email'
					},
					{
						value: 'webformulier',
						label: 'Webformulier'
					}
				],
				_filtersById = {};

			$scope.availableFilters = [
				{
					id: 'case',
					label: 'Zaken'
				},
				{
					id: 'contactmoment',
					label: 'Contactmomenten'
				},
				{
					id: 'document',
					label: 'Documenten'
				},
				{
					id: 'note',
					label: 'Notities'
				},
				{
					id: 'question',
					label: 'Vragen'
				},
				{
					id: 'product',
					label: 'Producten'
				}
			];
			
			$scope.activeFilters = [];
			$scope.items = [];
			$scope.loading = false;
			
			function processFilters ( ) {
				angular.forEach($scope.availableFilters, function ( filter ) {
					_filtersById[filter.id] = filter;
				});
			}

			processFilters();
						
			function loadData ( ) {
				var url,
					params = {};

				if(!$scope.loading && (!_lastResult || _lastResult.next)) {
					if(_lastResult) {
						url = _lastResult.next;
						params = {};
					} else if($scope.category === 'object') {
						url = '/api/object/' + $scope.id + '/action/log';
					} else {
						url = 'event/list';
						params = {
							'for': $scope.id,
							'category': $scope.category
						};
					}
					
					$scope.loading = true;
					smartHttp.connect({
						method: 'GET',
						url: url,
						params: params
					})
						.success(handleSuccess)
						.error(handleError);
				} else {
					$scope.loading = false;
				}
			}
			
			function handleSuccess ( data ) {
				$scope.loading = false;
				var items = data && data.result ? data.result : [],
					event;
				
				_lastResult = data;
				
				angular.forEach(items, function ( item ) {
					event = getItemById(item.id);
					for(var key in item) {
						event[key] = item[key];
					}
				});
				invalidateScroll();
			}
			
			function handleError ( ) {
				$scope.loading = false;
			}
			
			function getItemById ( id ) {
				if(_itemsById[id]) {
					return _itemsById[id];
				}
				var event = new TimelineEvent();
				_itemsById[id] = event;
				$scope.items.push(event);
				return event;
			}
			
			function invalidateScroll ( ) {
				$scope.$broadcast('scrollinvalidate');
			}
			
			$scope.toggleFilter = function ( filter ) {
				if(indexOf($scope.activeFilters, filter) === -1) {
					$scope.activateFilter(filter);
				} else {
					$scope.deactivateFilter(filter);
				}
			};
			
			$scope.activateFilter = function ( filter ) {
				if(indexOf($scope.activeFilters, filter) === -1) {
					$scope.activeFilters.push(filter);
				}
				invalidateScroll();
			};
			
			$scope.deactivateFilter = function ( filter ) {
				var index = indexOf($scope.activeFilters, filter);
				if(index !== -1) {
					$scope.activeFilters.splice(index, 1);
				}
				invalidateScroll();
			};
			
			$scope.isFiltered = function ( filterId ) {
				var filter = _filtersById[filterId];
				return $scope.activeFilters.length === 0 || indexOf($scope.activeFilters, filter) !== -1;
			};
			
			$scope.addNote = function ( content ) {
				var note,
					url;
				
				if(!content) {
					return;
				}
				
				note = new TimelineEvent();
				note.description = 'Notitie aangemaakt';
				note.content = content;
				note.id = -1;
				note.timestamp = new Date().toISOString();
				note.event_category = 'note';
				$scope.items.push(note);
				
				if($scope.category === 'subject') {
					url = 'betrokkene/' + $scope.id + '/notes/create';
				} else if($scope.category === 'case') {
					url = 'zaak/' + $scope.id + '/notes/create';
				} else if($scope.category === 'object') {
					url = '/api/object/' + $scope.id + '/action/create_note';
				}
				
				smartHttp.connect( {
					method: 'POST',
					url: url,
					data: {
						content: content
					}
				})
					.success(function ( data ) {
						var noteData = data.result[0];
						for(var key in noteData) {
							note[key] = noteData[key];
						}
					})
					.error(function ( ) {
						var index = indexOf($scope.items, note);
						if(index !== -1) {
							$scope.items.splice(index, 1);
						}
					});
			};
			
			$scope.addContactmoment = function ( content, contactchannel, rel ) {
				
				
				var contactmoment,
					url,
					data,
					caseId,
					betrokkeneId;
					
				if($scope.category === 'case') {
					caseId = $scope.id;
					betrokkeneId = 'subject_identifier' in rel ? rel.subject_identifier : 'betrokkene-' + rel.object_type + '-' + rel.id;
				} else {
					caseId = rel ? rel.id : undefined;
					betrokkeneId = $scope.id; 
				}
					
				contactmoment = new TimelineEvent();
				contactmoment.description = 'Contactmoment toegevoegd';
				contactmoment.content = content;
				contactmoment.contact_channel = contactchannel;
				contactmoment.case_id = caseId;
				contactmoment.id = -1;
				contactmoment.timestamp = new Date().toISOString();
				contactmoment.event_category = 'contactmoment';
				
				data = {
					contactkanaal: contactchannel,
					content: content,
					case_id: caseId
				};
				
				url = 'betrokkene/' + betrokkeneId + '/contactmoment/create';
				
				$scope.items.push(contactmoment);
				smartHttp.connect( {
					method: 'POST',
					url: url,
					data: data
				})
					.success(function ( data ) {
						var contactMomentData = data.result[0];
						for(var key in contactMomentData) {
							contactmoment[key] = contactMomentData[key];
						}
						$scope.contactmomentContent = '';
						$scope.caseId = null;
					})
					.error(function ( ) {
						var index = indexOf($scope.items, contactmoment);
						if(index !== -1) {
							$scope.items.splice(index, 1);
						}
					});
					
			};
			
			$scope.getItemVisibility = function ( item ) {
				var isFiltered = $scope.isFiltered(item.event_category);
				
				if(!isFiltered) {
					isFiltered = $scope.activeFilters.length && _.some($scope.activeFilters, function ( filter ) {
						return filter.filter !== undefined ? _.find([ item ], filter.filter) : false;
					});
				}
				
				return isFiltered;
			};
			
			$scope.getContactChannels = function ( ) {
				return _contactChannels;
			};
			
			$scope.initialize = function ( category, id ) {
				$scope.category = category;
				$scope.id = id;

				if(category === 'case') {
					$scope.availableFilters = [
						{ id: 'contactmoment', label: 'Contactmomenten' },
						{ id: 'case-mutation', label: 'Wijzigingen' },
						{ id: 'document', label: 'Documenten' },
						{ id: 'note', label: 'Notities' }
					];

					processFilters();
				} else if(category === 'object') {
					$scope.availableFilters = [
						{ id: 'created', label: 'Aangemaakt', filter: { event_type: 'case/object/create' } },
						{ id: 'related', label: 'Gerelateerd', filter: { event_type: 'case/object/relate' } },
						{ id: 'updated', label: 'Gewijzigd', filter: { event_type: 'case/object/update' } },
						{ id: 'deleted', label: 'Verwijderd', filter: { event_type: 'case/object/delete' } },
						{ id: 'note', label: 'Notities', filter: { event_type: 'object/note' } }
					];
				}
				
				loadData();
			};
			
			$scope.loadMoreData = function ( ) {
				if(!$scope.loading) {
					loadData();
				}
			};
			
			$scope.getTruncatedText = function ( item ) {
				var content = item.content,
					match;
				if(!content){
					return '';
				}
				
				if(content.length > MAX_CONTENT_LENGTH) {
					content = content.substr(0, MAX_CONTENT_LENGTH);
					match = content.match(/^([\s\S]*)\s+/m);
					if(match) {
						content = match[0];
					}
					content += ' ...';
				}
				
				return content;
			};
			
			$scope.isTruncated = function ( item ) {
				return item.content && (!$scope.isEmail(item) || (item.content.length > MAX_CONTENT_LENGTH));
			};
			
			$scope.showItemContent = function ( item, expanded ) {
				return !$scope.isEmail(item)&&!(item.expanded === false && !expanded);
			};
			
			$scope.getSpotEnlighterLabel = function ( rel ) {
				var label;
					
				if($scope.category === 'case') {
					if(rel && rel.object_type === 'bedrijf') {
						label = 'handelsnaam';
					} else if(rel && 'voorletters' in rel) {
						label ='voorletters + \' \' + geslachtsnaam';
					} else {
						label = 'naam';
					}
				} else {
					label = 'id + \': \' + zaaktype_node_id.titel';
				}
				
				return label;
			};
			
			$scope.downloadLog = function ( ) {
				$window.open('/event/download?for=' + $scope.id + '&category=' + $scope.category + '&context=none');
			};
			
			$scope.isEmail = function ( item ) {
				return item.event_type && item.event_type.indexOf('email') === 0;
			};
			
			$scope.getItemDescription = function ( item ) {
				return $sce.trustAsHtml(item.description);	
			};
			
			$scope.handleAttachmentClick = function ( item, attachment ) {
				$window.open('/zaak/' + item.case_id + '/document/' + attachment.file_id + '/download');
			};
			
			$scope.hasNoteSupport = function ( ) {
				return $scope.category === 'subject' || $scope.category === 'case' || $scope.category === 'object';
			};
			
			$scope.$on('scrollend', function ( event/*, from, upto*/ ) {
				safeApply($scope, function ( ) {
					loadData();
					event.stopPropagation();
				});
			});
			
		}]);
})();
