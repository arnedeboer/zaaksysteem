/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.auth')
		.service('organisationalUnitService', [ '$q', 'translationService', 'smartHttp', function ( $q, translationService, smartHttp ) {
			
			var organisationalUnitService = {},
				loading,
				deferred,
				defaultOrgUnitId = null,
				defaultRoleIdByOrgUnitId = {},
				orgUnits,
				orgUnitsById = {};
			
			organisationalUnitService.getUnits = function ( ) {
				if(deferred) {
					return deferred.promise;
				}
				
				deferred = $q.defer();
				
				loading = true;
				
				smartHttp.connect({
					method: 'GET',
					url: '/api/authorization/org_unit',
					params: {
						zapi_no_pager: 1
					}
				})
					.success(function ( response ) {
						
						orgUnits = response.result;
						
						_.each(orgUnits, function ( orgUnit ) {
							orgUnitsById[orgUnit.org_unit_id] = orgUnit;
							if(orgUnit.depth === 0) {
								orgUnit.name = translationService.get('Alle afdelingen');
							}
						});
							
						if(orgUnits.length) {
							defaultOrgUnitId = orgUnits[0].org_unit_id;
						}
						
						deferred.resolve(orgUnits);
						
					})
					.error(function ( /*response*/ ) {
						deferred.reject();
					})
					['finally'](function ( ) {
						loading = false;
					});
				
				return deferred.promise;
			};
			
			organisationalUnitService.ready = function ( ) {
				return organisationalUnitService.getUnits();
			};
			
			organisationalUnitService.getDefaultOrgUnitId = function ( ) {
				return defaultOrgUnitId;	
			};
			
			organisationalUnitService.getDefaultRoleIdForOrgUnitId = function ( orgUnitId ) {
				var roleId = defaultRoleIdByOrgUnitId[orgUnitId],
					role,
					orgUnit;
					
				if(orgUnitId === undefined) {
					return null;
				}
					
				if(roleId !== undefined) {
					return roleId;
				}
				
				orgUnit = _.find(orgUnits, { 'org_unit_id': orgUnitId });
				
				role = _.find(orgUnit.roles, { 'name': 'Behandelaar' }) || orgUnit.roles[0];
				
				roleId = role ? role.role_id : null;
				
				defaultRoleIdByOrgUnitId[orgUnitId] = roleId;
				
				return roleId;
			};
			
			organisationalUnitService.getDefaults = function ( ) {
				return {
					org_unit_id: defaultOrgUnitId,
					role_id: organisationalUnitService.getDefaultRoleIdForOrgUnitId(defaultOrgUnitId)
				};
			};
			
			organisationalUnitService.getOrgUnitById = function ( orgUnitId ) {
				return orgUnitsById[orgUnitId];	
			};
			
			return organisationalUnitService;
			
		}]);
	
})();