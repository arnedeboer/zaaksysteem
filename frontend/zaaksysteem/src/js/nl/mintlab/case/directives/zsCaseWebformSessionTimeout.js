/*global angular,_,fetch*/
(function ( ) {
	
	var RENEW_EVERY = 30000,
		CHECK_EVERY = 120000,
		WARN_AT = CHECK_EVERY;
	
	angular.module('Zaaksysteem.case')
		.directive('zsCaseWebformSessionTimeout', [ '$http', '$interval', 'systemMessageService', function ( $http, $interval, systemMessageService ) {
			
			var safeApply = fetch('nl.mintlab.utils.safeApply');
			
			return {
				require: [ 'zsCaseWebformSessionTimeout', '?zsCaseWebform' ],
				controller: [ '$scope', '$element', function ( $scope, $element ) {
					
					var ctrl = {},
						zsCaseWebform,
						onInvalidate,
						intervalP,
						expirationIntervalP,
						expires = NaN,
						pushedWarning = false;
						
					function pushWarning ( ) {
						var ms = expires - new Date().getTime();
						
						pushedWarning = true;
						
						systemMessageService.emit('info', 'Uw sessie dreigt te verlopen.', {
							timeout: ms,
							actionLabel: 'Verleng sessie',
							action: function ( ) {
								renewSession()
									.then(function ( ) {
										systemMessageService.emit('info', 'Uw sessie is verlengd.');
									});
							},
							onEnd: function ( ) {
								getSessionExpiration();
							}
						});
					}
					
					function pushError ( ) {
						pushedWarning = true;
						systemMessageService.emitError('Uw sessie is verlopen. Ververs de pagina');
						onInvalidate = angular.noop;
						$interval.cancel(intervalP);
					}
					
					function setSession ( session ) {

						expires = !session.ttl ? Date.now() : (Date.now() + session.ttl);

						checkExpiry();
					}

					function checkExpiry ( ) {

						var now,
							expiresIn,
							warnAt;

						if(isNaN(expires)) {
							return;
						}

						now = Date.now();
						expiresIn = expires - now;
						warnAt = expires - WARN_AT;

						if(now >= warnAt && !pushedWarning) {
							pushWarning();
						} else if(expiresIn <= 0) {
							pushError();
						}
					}
					
					function renewSession ( ) {
						
						pushedWarning = false;
						
						return $http({
							method: 'POST',
							url: '/api/users/session/extend'
						})
							.success(function ( response ) {
								
								setSession(response.result[0].session);
								
							});
						
					}
					
					function getSessionExpiration ( ) {
												
						$http({
							method: 'GET',
							url: '/api/users/session'
						})
							.success(function ( response ) {
								setSession(response.result[0].session);
							});
					}
					
					ctrl.link = function ( controllers ) {
						zsCaseWebform = controllers[0];
						
						if(zsCaseWebform){
							zsCaseWebform.onInvalidate.push(function ( ) {
								onInvalidate();
							});
						} else {
							$element.bind('keyup input paste change', onInvalidate);
						}
						
					};
					
					onInvalidate = _.throttle(function ( ) {
						
						safeApply($scope, renewSession);
						
					}, RENEW_EVERY, { leading: false, trailing: true });
					
					intervalP = $interval(function ( ) {
						getSessionExpiration();
					}, CHECK_EVERY);

					expirationIntervalP = $interval(function ( ) {
						checkExpiry();
					}, 1000);
					
					$scope.$on('$destroy', function ( ) {
						onInvalidate = angular.noop;
						$interval.cancel(intervalP);
						$interval.cancel(expirationIntervalP);
					});

					return ctrl;
					
				}],
				link: function ( scope, element, attrs, controllers ) {
					
					controllers[0].link(controllers.slice(1));
					
				}
			};
			
		}]);
	
})();
