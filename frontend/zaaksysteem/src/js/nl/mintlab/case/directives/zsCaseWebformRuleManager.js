/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.case')
		.directive('zsCaseWebformRuleManager', [ 'objectService', function ( objectService ) {
			
			return {
				require: [ 'zsCaseWebformRuleManager', '^?zsCaseView' ],
				controller: [ '$scope', function ( $scope ) {
					
					var ctrl = {},
						zsCaseView,
						visibility = {},
						disabled = {},
						controls = [],
						values = {},
						paused = {},
						fixedValues = {},
						revalidateAttributes = [];
						
					function isValid ( rule, values ) {
						var relation = rule.conditions.type,
							filter = relation === 'and' ? _.every : _.some;
						
						return !rule.conditions.conditions.length || filter(rule.conditions.conditions, function ( condition ) {
							return matchCondition(condition, values);
						});
					}
					
					function isFile ( key ) {
						var value = ctrl.getValue(key),
							file = _.isArray(value) ? value[0] : value,
							isF;
							
						if(file) {
							isF = file.filename !== undefined;
						}
						
						return isF;
					}
					
					function matchCondition ( condition, values ) {
						var attributeName = condition.attribute_name,
							attrValue = values[attributeName] !== undefined ? values[attributeName] : ctrl.getValue(attributeName),
							matches,
							isArray = _.isArray(attrValue);
						
						if(attrValue && attrValue.original !== undefined) {
							attrValue = attrValue.original;
						}
							
						if(condition.validation_type === 'revalidate') {
							revalidateAttributes.push(attributeName);
						}

						if(condition.validates_true !== undefined) {
							matches = !!condition.validates_true;
						} else {
							if(visibility[attributeName] === false) {
								attrValue = null;
							}
							
							matches = _.some(condition.values, function ( val ) {
								var m;
								if(isArray) {
									m = _.indexOf(attrValue, val) !== -1;
								} else {
									m = objectService.isEqualValue(attrValue, val);
								}
								return m;
							});
						}
						
						return matches;
					}
					
					function getValue ( key ) {
						var value;
						
						if(zsCaseView) {
							value = zsCaseView.getCaseValue(key);
						} else {
							value = values[key];
						}
						
						return value;
					}
					
					function unsetExec ( actions ) {
						_.each(actions, function ( action ) {
							delete action.executed;
						});
					}
					
					function applyRules ( ) {
						
						var vals = {},
							invalidated;
						
						visibility = {};
						disabled = {};
						paused = {};
						fixedValues = {};
						
						revalidateAttributes = [];
						
						_.each(controls, function ( child ) {
							
							_.each(child.getRules(), function ( rule ) {
								
								var valid = isValid(rule, vals),
									actions;
									
								rule.valid = valid;
								
								if(valid) {
									actions = rule.then;
									unsetExec(rule['else']);
								} else {
									actions = rule['else'];
									unsetExec(rule.then);
								}
								
								_.each(actions, function ( action ) {
									switch(action.type) {
										default:
										break;
										
										case 'hide_attribute':
										case 'hide_group':
										visibility[action.data.attribute_name] = false;
										break;
										
										case 'show_attribute':
										case 'show_group':
										delete visibility[action.data.attribute_name];
										break;
										
										case 'set_value':
										if(!action.data.can_change || !action.executed) {
											vals[action.data.attribute_name] = action.data.value;	
										}
										if(!action.data.can_change) {
											fixedValues[action.data.attribute_name] = action.data.value;
										}
										break;
										
										case 'pause_application':
										paused[child.getPhaseId()] = rule;
										break;
									}
									action.executed = true;
								});
								
							});
						});

						_.each(visibility, function ( value, key ) {
							if(!value && !isFile(key)) {
								vals[key] = null;
							}
						});
						
						_.each(vals, function ( value, key ) {
							disabled[key] = true;
							if(!objectService.isEqualValue(value, getValue(key))) {
								ctrl.setValue(key, value);
								invalidated = true;
							}
						});
						
						if(invalidated) {
							_.each(controls, function ( child ) {
								child.invalidateForm();
							});
						}
					}
					
					function onUpdate ( ) {
						ctrl.invalidateRules();
					}
					
					function reloadRulesOf ( attributeName, value ) {
						_.each(controls, function ( child ) {
							if (_.indexOf(child.getAttributeNames(), attributeName) !== -1) {
								child.reloadRules(attributeName, value);
							}
						});
					}
					
					ctrl.link = function ( controllers ) {
						zsCaseView = controllers[0];
						if(zsCaseView) {
							zsCaseView.updateListeners.push(onUpdate);
							$scope.$on('$destroy', function ( ) {
								_.pull(zsCaseView.updateListeners, onUpdate);
							});
						}
					};
					
					ctrl.addControl = function ( child ) {
						controls.push(child);
					};
					
					ctrl.removeControl = function ( child ) {
						_.pull(controls, child);
					};
					
					ctrl.setValue = function ( key, value, resolved ) {
						if(zsCaseView) {
							zsCaseView.setCaseValue(key, value, resolved);
						} else {
							values[key] = value;
						}
						
						if(_.indexOf(revalidateAttributes, key) !== -1) {
							reloadRulesOf(key, resolved);
						}
					};
					
					ctrl.getValue = function ( key ) {
						var value;
							
						value = getValue(key);
						
						return value;
					};
					
					ctrl.getValues = function ( ) {
						var vals = angular.copy(values),
							caseObj = zsCaseView ? zsCaseView.getCase() : null;
							
						if(caseObj) {
							angular.extend(vals, angular.copy(caseObj.values));
						}
						
						return vals;
					};
					
					ctrl.isFieldVisible = function ( attributeName ) {
						return visibility[attributeName] !== false;
					};
					
					ctrl.isFixedValue = function ( attributeName ) {
						return fixedValues[attributeName] !== undefined;
					};
					
					ctrl.getFixedValue = function ( attributeName ) {
						return fixedValues[attributeName];
					};
					
					ctrl.invalidateRules = function ( ) {
						applyRules();
					};
					
					ctrl.getApplicationPauseRule = function ( phaseId ) {
						return paused[phaseId];
					};
					
					ctrl.matchCondition = function ( condition, values ) {
						return matchCondition(condition, values);
					};
					
					ctrl.getAttributeNames = function ( ) {
						return _.unique(_.flatten(_.map(controls, function ( child ) {
							return child.getAttributeNames();
						})));
					};
					
					return ctrl;
					
				}],
				controllerAs: 'caseWebformRuleManager',
				link: function ( scope, element, attrs, controllers ) {
					
					var zsCaseWebformRuleManager = controllers[0];
					zsCaseWebformRuleManager.link(controllers.slice(1));
					
				}
			};
			
		}]);
	
})();
