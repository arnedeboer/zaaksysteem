/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.case.relation')
		.directive('zsPlannedCaseEdit', [ '$parse', 'translationService', 'organisationalUnitService', function ( $parse, translationService, organisationalUnitService ) {
			
			return {
				scope: true,
				templateUrl: '/html/case/relations/relations.html#planned-case-edit',
				controller: [ '$scope', '$element', '$attrs', function ( $scope, $element, $attrs ) {
					
					var ctrl = {},
						obj = $scope.$eval($attrs.plannedCase),
						config;
						
					function isRepeated ( ) {
						var intervalPeriod = $parse('values.interval_period')(obj),
							repeated;
							
						if(intervalPeriod) {
							repeated = intervalPeriod !== 'once';
						}
						
						return repeated;
					}
						
					function setConfig ( ) {
						var intervalOptions = [
								{
									value: 'days',
									label: translationService.get('Dagen')
								},
								{
									value: 'weeks',
									label: translationService.get('Weken')
								},
								{
									value: 'months',
									label: translationService.get('Maanden')
								},
								{
									value: 'years',
									label: translationService.get('Jaren')
								}
							],
							caseOptions = _.map($scope.$eval($attrs.cases), function ( caseObj ) {
								return {
									value: caseObj.id,
									label: caseObj.label
								};
							});
						
						config = {
							name: 'planned-case-edit',
							options: {
								autosave: false
							},
							actions: [
								{
									id: 'cancel',
									label: translationService.get('Annuleren'),
									type: 'click',
									importance: 'secondary',
									click: 'plannedCaseEdit.handleCancel()'
								},
								{
									id: 'submit',
									label: translationService.get('Opslaan'),
									type: 'submit',
									importance: 'primary'
								}
							],
							fields: [
								{
									name: 'case',
									label: translationService.get('Zaak'),
									type: 'select',
									data: {
										options: caseOptions
									},
									'default': caseOptions.length ? caseOptions[0].value : null,
									when: Boolean(caseOptions.length).toString()
								},
								{
									name: 'casetype',
									label: translationService.get('Zaaktype'),
									type: 'spot-enlighter',
									data: {
										params: {
											object_type: 'zaaktype'
										},
										transform: 'plannedCaseEdit.transform($object)',
										label: 'zs_object.values["casetype.name"]'
									},
									required: true,
									value: $scope.$eval($attrs.casetype),
									when: 'plannedCaseEdit.isCreate()'
								},
								{
									name: 'copy_relations',
									label: translationService.get('Objectrelaties kopiëren'),
									type: 'checkbox',
									data: {
										'checkboxlabel': translationService.get('Ja')
									},
									'default': true,
									when: 'plannedCaseEdit.isCreate()'
								},
								{
									name: 'planning',
									label: translationService.get('Planning'),
									type: 'form',
									data: {
										fields: [
											{
												name: 'next_run',
												label: translationService.get('Vanaf'),
												type: 'date',
												required: true,
												value: $parse('values.next_run')(obj)
											},
											{
												name: 'repeat',
												label: translationService.get('Terugkeerpatroon'),
												type: 'checkbox',
												data: {
													checkboxlabel: translationService.get('Ja')
												},
												'default': false,
												value: isRepeated()
											},
											{
												name: 'pattern',
												label: 'Elke',
												type: 'form',
												data: {
													fields: [
														{
															name: 'interval_value',
															label: '',
															type: 'number',
															required: true,
															'default': 1,
															value: $parse('values.interval_value')(obj)
														},
														{
															name: 'interval_period',
															label: '',
															type: 'select',
															data: {
																options: intervalOptions
															},
															'default': intervalOptions[0].value,
															required: true,
															value: $parse('values.interval_period')(obj)
														}
													]
												},
												required: true,
												when: '!!repeat'
											},
											{
												name: 'runs_left',
												label: translationService.get('Aantal herhalingen'),
												type: 'number',
												when: '!!repeat',
												required: true,
												'default': 1,
												value: $parse('values.runs_left')(obj)
											}
										],
										promises: [
											{
												'watch': 'repeat',
												'when': '!!repeat',
												'then': 'pattern.interval_period="days"'
											}
										]
									},
									required: true
								}
							]
						};
					}
					
					organisationalUnitService.ready()
						.then(function ( ) {
							setConfig();
						});
						
					ctrl.transform = function ( $object ) {
						if($object && $object.zs_object !== undefined) {
							$object = $object.zs_object;
						}
						
						return $object;
					};
					
					ctrl.getFormConfig = function ( ) {
						return config;
					};
					
					ctrl.handleCancel = function ( ) {
						$scope.closePopup();
					};
					
					ctrl.isCreate = function ( ) {
						return $scope.$eval($attrs.isCreate);
					};
					
					ctrl.handleFormSubmit = function ( $values ) {
						
						var repeat = $values.planning.repeat,
							pattern = $values.planning.pattern || {},
							intervalPeriod = 'once',
							intervalValue;
							
						if(repeat) {
							intervalPeriod = pattern.interval_period;
							intervalValue = pattern.interval_value;
						}
						
						$parse($attrs.submit)($scope, {
							$values: {
								'case': $values['case'],
								casetype: $values.casetype,
								next_run: new Date($values.planning.next_run.from).toISOString(),
								interval_period: intervalPeriod,
								interval_value: intervalValue,
								runs_left: !repeat ? 1 : parseInt($values.planning.runs_left, 10),
								copy_relations: $values.copy_relations
							}
						});
						
						$scope.closePopup();
					};
					
					return ctrl;
					
				}],
				controllerAs: 'plannedCaseEdit'
			};
					
		}]);
	
})();
