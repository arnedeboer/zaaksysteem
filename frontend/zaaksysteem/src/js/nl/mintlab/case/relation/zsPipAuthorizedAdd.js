/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.case.relation')
		.directive('zsPipAuthorizedAdd', [ '$http', 'pipAuthorizationService', 'systemMessageService', function ( $http, pipAuthorizationService, systemMessageService ) {
			
			return {
				require: [ 'zsPipAuthorizedAdd', '^?zsCaseView' ],
				controller: [ function ( ) {
					
					var ctrl = this,
						zsCaseView;
					
					ctrl.link = function ( controllers ) {
						zsCaseView = controllers[0];
					};
					
					ctrl.addAuthorization = function ( $authorization ) {
						
						var caseId = zsCaseView.getCaseId(),
							params;
						
						params = pipAuthorizationService.getApiParams($authorization);
						
						return $http({
							method: 'POST',
							url: '/api/case/' + caseId + '/subjects/create',
							data: params
						})
							.success(function ( response ) {
								var authorization = response.result[0];
								zsCaseView.relations.authorizations.push(authorization);
								systemMessageService.emit('Gemachtigde toegevoegd');
							})
							.error(function ( ) {
								throw systemMessageService.emitError('Er ging iets fout aolbij het toevoegen van de machtiging. Probeer het later opnieuw.');
							});
					};
					
					return ctrl;
				}],
				controllerAs: 'pipAuthorizedAdd',
				link: function ( scope, element, attrs, controllers ) {
					
					controllers[0].link(controllers.slice(1));
					
				}
			};
			
		}]);
	
})();
