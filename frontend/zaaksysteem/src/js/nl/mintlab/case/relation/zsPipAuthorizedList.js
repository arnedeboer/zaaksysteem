/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.case.relation')
		.directive('zsPipAuthorizedList', [ '$http', 'pipAuthorizationService', 'systemMessageService', 'capitalizeFilter', function ( $http, pipAuthorizationService, systemMessageService, capitalizeFilter ) {
			
			var roles = [];
			
			pipAuthorizationService.getRoles()
				.then(function ( r ) {
					roles = r;
				});
			
			function getCrudConfig ( ) {
				
				var config = {
						columns: [
							{
								id: 'type',
								label: 'Type',
								resolve: [ '$item', function ( $item ) {
									return capitalizeFilter($item.betrokkene_type.replace(/_/g, ' '));
								}]
							},
							{
								id: 'name',
								label: 'Naam',
								resolve: 'name'
							},
							{
								id: 'role',
								label: 'Rol',
								resolve: 'role'
							},
							{
								id: 'pip_authorized',
								label: 'Gemachtigd',
								template: '<i class="icon pip-authorized-icon" data-ng-class="{ \'icon-checked\': item.pip_authorized }"></i>'
							},
							{
								id: 'actions',
								label: 'Acties',
								locked: true,
								templateUrl: '/html/core/crud/crud-item-action-menu.html',
								dynamic: true,
								sort: false
							}
						],
						actions: [
							{
								id: 'edit',
								type: 'popup',
								label: 'Bewerken',
								when: 'selectedItems.length',
								data: {
									url: '/html/case/relations/relations.html#authorization-edit-dialog'
								}
							},
							{
								id: 'remove',
								type: 'confirm',
								label: 'Verwijderen',
								when: 'selectedItems.length',
								data: {
									label: 'Weet u zeker dat u de relatie met de betrokkene wilt verwijderen?',
									verb: 'Verwijder',
									click: 'pipAuthorizedList.handleDeleteClick(selectedItems[0])'
								}
							}
						], 
						options: {
							select: 'single',
							resolve: 'id'
						}
					};
					
				_.each(config.columns, function ( col ) {
					col.locked = true;
					col.sort = false;
				});
				
				return config;
			}
			
			return {
				require: [ 'zsPipAuthorizedList', '^zsCaseView' ],
				scope: true,
				controller: [ function ( ) {
					
					var ctrl = {},
						config,
						authorizations,
						zsCaseView;
						
					config = getCrudConfig();
						
					ctrl.link = function ( controllers ) {
						zsCaseView = controllers[0];
						
						authorizations = zsCaseView.relations.authorizations;
						
						$http({
							method: 'GET',
							url: '/api/case/' + zsCaseView.getCaseId() + '/subjects',
							params: {
								zapi_no_pager: 1
							}
						})
							.success(function ( response ) {
								authorizations = zsCaseView.relations.authorizations = response.result;
							})
							.error(function ( /*response*/ ) {
								systemMessageService.emitLoadError('machtigingen');
							});
					};
					
					ctrl.getConfig = function ( ) {
						return config;
					};
					
					ctrl.getAuthorizations = function ( ) {
						return authorizations;	
					};
					
					ctrl.editAuthorization = function ( authorization, $values ) {
						
						var caseId = zsCaseView.getCaseId(),
							authorizationId = authorization.id,
							params = pipAuthorizationService.getApiParams($values),
							old = angular.copy(authorization);
							
						_.merge(authorization, params);
						
						return $http({
							method: 'POST',
							url: '/api/case/' + caseId + '/subjects/' + authorizationId + '/update',
							data: params
						})
							.success(function ( /*response*/ ) {
								systemMessageService.emitSave();
							})
							.error(function ( /*response*/ ) {
								
								var index = _.findIndex(authorizations, { id: authorizationId });
								authorizations.splice(index, 1, old);
								
								throw systemMessageService.emitSaveError('uw wijzigingen');
							});
						
					};
					
					ctrl.handleDeleteClick = function ( item ) {
						
						var caseId = zsCaseView.getCaseId(),
							authorizationId = item.id,
							copy = authorizations.concat();
						
						_.remove(authorizations, { id: authorizationId });
							
						return $http({
							method: 'POST',
							url: '/api/case/' + caseId + '/subjects/' + authorizationId + '/delete'
						})
							.success(function ( /*response*/ ) {
								systemMessageService.emit('De machtiging is verwijderd.');
							})
							.error(function ( /*response*/ ) {
								authorizations = copy;
								throw systemMessageService.emitError('Er ging iets fout bij het verwijderen van de machtiging. Probeer het later opnieuw.');
							});
						
					};
					
					ctrl.canAdd = function ( ) {
						return true;
					};
					
					return ctrl;
					
				}],
				controllerAs: 'pipAuthorizedList',
				link: function ( scope, element, attrs, controllers  ) {
					controllers[0].link(controllers.slice(1));
				}
			};
			
		}]);
	
})();
