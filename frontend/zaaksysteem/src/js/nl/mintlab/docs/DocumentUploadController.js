/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem.docs')
		.controller('nl.mintlab.docs.DocumentUploadController', [ '$scope', 'fileUploader', function ( $scope, fileUploader ) {
			
			var indexOf = fetch('nl.mintlab.utils.shims.indexOf');
			
			$scope.uploadQueue = [];
			$scope.toGo = [];
			
			$scope.completed = [];
			$scope.failed = [];
			
			$scope.totalBytes = 0;
			$scope.loadedBytes = 0;
			$scope.progress = 0;
			
			$scope.clearQueue = function ( ) {
				
				angular.forEach($scope.uploadQueue.concat(), function ( upload ) {
					if(upload.completed || upload.error) {
						$scope.uploadQueue.splice(indexOf($scope.uploadQueue, upload), 1);
					}
				});
				
				updateTotalProgress();
				
				$scope.completed.length = 0;
				$scope.failed.length = 0;
			};
			
			$scope.abortUpload = function ( ) {
				angular.forEach($scope.uploadQueue.concat(), function ( upload ) {
					if(!upload.completed && !upload.error) {
						upload.abort();
					}
				});
				
				$scope.clearQueue();
			};
			
			$scope.getProgressBarWidth = function ( ) {
				return $scope.toGo.length ? Math.round(($scope.progress||0)*100) : 0;
			};
			
			function updateTotalProgress ( ) {
				var totalBytes = 0,
					loadedBytes = 0,
					allFilesInit = true;
					
				angular.forEach($scope.uploadQueue, function ( upload ) {
					if(!isNaN(upload.totalBytes)) {
						totalBytes += upload.totalBytes;
						if(!upload.error) {
							loadedBytes += upload.loadedBytes;
						} else {
							loadedBytes += upload.totalBytes;
						}
					} else {
						allFilesInit = false;
					}
				});
				$scope.loadedBytes = loadedBytes;
				$scope.totalBytes = totalBytes;
				$scope.progress = allFilesInit ? loadedBytes/totalBytes : 0;
			}
			
			function onFileAdd ( event, upload ) {
				
				$scope.clearQueue();
				
				
				
				upload.subscribe('init', onFileInit);
				upload.subscribe('progress', onFileProgress);
				upload.subscribe('complete', onFileComplete);
				upload.subscribe('end', onFileEnd);
				upload.subscribe('error', onFileError);
				
				$scope.uploadQueue.push(upload);
				$scope.toGo.push(upload);
				$scope.$apply();
			}
			
			function onFileInit ( /*event*/ ) {
				updateTotalProgress();
				$scope.$apply();
			}
			
			function onFileProgress ( /*event, upload, progress*/ ) {
				updateTotalProgress();
				$scope.$apply();
			}
			
			function onFileComplete ( event, upload ) {
				var data;
				
				updateTotalProgress();
				if(!upload.error) {
					$scope.completed.push(upload);
				} else {
					data = upload.getData();
					if(data.error_code && (data.error_code === '/filestore/assert_allowed_filetype/extension_not_allowed' || data.error_code === 500)) {
						// TODO(dario): use a pseudo-class
						$scope.$emit('systemMessage', {
							content: data.messages[0],
							type: 'error',
							code: data.error_code
						});
					}
				}
				$scope.$apply();
			}
			
			function onFileEnd ( event, upload ) {
				var index = indexOf($scope.toGo, upload);
				$scope.toGo.splice(index, 1);
			}
			
			function onFileError ( event, upload ) {
				updateTotalProgress();
				$scope.failed.push(upload);
			}
			
			fileUploader.subscribe('fileadd', onFileAdd);
			
			
		}]);
	
})();