/*global angular,fetch,_*/
(function ( ) {
	angular.module('Zaaksysteem.docs')
		.controller('nl.mintlab.docs.DocumentListController', [ '$scope', '$q', 'smartHttp', 'translationService', '$window', 'systemMessageService', function ( $scope, $q, smartHttp, translationService, $window, systemMessageService ) {
			
			// TODO(dario): refactor to use different controllers for
			// list,intake,trash, using js inheritance
			
			var File = fetch('nl.mintlab.docs.File'),
				Folder = fetch('nl.mintlab.docs.Folder'),
				indexOf = _.indexOf,
				words,
				cutFiles = [],
				_url = 'file/';
			
			$scope.root = null;
			$scope.viewType = 'listView';
			$scope.selectedFiles = [];
			
			$scope.maxDepth = 1;
			$scope.filterQuery = '';
			$scope.dragging = false;
			
			function clearData ( ) {
				$scope.deselectAll();
			}
			
			$scope.onToggleClick = function ( event, entity ) {
				if($scope.isSelected(entity)) {
					$scope.deselectEntity(entity);
				} else {
					$scope.selectEntity(entity);
				}
				
				if(event) {
					event.stopPropagation();
				}
			};
			
			$scope.onEntityClick = function ( event, entity ) {
				if($scope.isSelected(entity) && $scope.selectedFiles.length === 1) {
					$scope.deselectEntity(entity);
				} else {
					$scope.deselectAll();
					$scope.selectEntity(entity);
				}
			};
			
			$scope.onSelectAllClick = function ( /*event*/ ) {
				if($scope.isSelected($scope.root)) {
					$scope.deselectAll();
				} else {
					$scope.selectAll();
				}
			};
			
			$scope.selectEntity = function ( entity, recursive ) {
				if(!entity.getSelected()) {
					entity.setSelected(true);
					if(entity !== $scope.root) {
						$scope.selectedFiles.push(entity);
					}
				}
				
				if(recursive === true && entity instanceof Folder) {
					angular.forEach(entity.getFolders(), function ( value ) {
						$scope.selectEntity(value, true);
					});
					angular.forEach(entity.getFiles(), function ( value ) {
						$scope.selectEntity(value);
					});
				}
				
				setRootState();
			};
			
			$scope.deselectEntity = function ( entity, recursive ) {
				var selectedFiles = $scope.selectedFiles,
					index = indexOf(selectedFiles, entity);
				
				if(entity.getSelected()) {
					entity.setSelected(false);
					if(entity !== $scope.root) {
						selectedFiles.splice(index, 1);
					}
				}
				
				if(recursive === true && entity instanceof Folder) {
					angular.forEach(entity.getFolders(), function ( value ) {
						$scope.deselectEntity(value, true);
					});
					angular.forEach(entity.getFiles(), function ( value ) {
						$scope.deselectEntity(value);
					});
				}
					
				
				$scope.root.setSelected(false);
			};
			
			$scope.selectAll = function ( ) {
				$scope.selectEntity($scope.root, true);
			};
			
			$scope.deselectAll = function ( ) {
				$scope.deselectEntity($scope.root, true);
				while($scope.selectedFiles.length) {
					$scope.deselectEntity($scope.selectedFiles[0]);
				}
			};
			
			$scope.isSelected = function ( entity ) {
				if(!entity) {
					return null;
				}
				return entity.getSelected();
			};
			
			$scope.moveEntity = function ( entities, target ) {
				angular.forEach(entities, function ( entity ) {
					
					var currentParent = entity.getParent(),
						directoryId = target.getDepth() > 0 ? target.id : null;
					
					if(validateEntity(entity, target)) {
						target.add(entity);
					}
					
					entity.updating = true;
					
					smartHttp.connect({
						url: 'file/update/',
						method: 'POST',
						data: {
							file_id: entity.id,
							directory_id: directoryId
						},
						blocking: false
					})
						.success(function ( ) {
							entity.directory_id = target.id;
							entity.updating = false;
						})
						.error(function ( ) {
							currentParent.add(entity);
							entity.updating = false;
						});
				});
			};
			
			$scope.removeEntity = function ( entities ) {
				var toDelete = entities.concat(),
					folders = _.filter(entities, function ( entity ) {
						return entity instanceof Folder;
					}),
					files;
				
				_.each(folders, function ( folder ) {
					var children = folder.getFiles();
					toDelete = toDelete.concat(children);
				});
				
				files = _.filter(toDelete, function ( entity ) {
					return entity instanceof File;
				});
				
				_.each(toDelete, function ( entity ) {
					if($scope.isSelected(entity)) {
						$scope.deselectEntity(entity);
					}
				});
				
				_.each(files, function ( file ) {
					var parent = file.getParent();
					
					if($scope.caseId) {
						$scope.trash.add(file);
					} else {
						parent.remove(file);
					}
					
				});
					
				return smartHttp.connect({
				    	url: '/api/bulk/file/update',
				    	method: 'POST',
				    	data: {
				    		files: _(files)
				    			.map(function ( file ) {
				    				return {
				    					id: file.id,
				    					action: 'update_properties',
				    					data: {
				    						deleted: true
				    					}
				    				};
				    			})
				    			.indexBy('id')
				    			.omit('id')
				    			.value()
				    	},
				    	blocking: false
				    })
				    	.success(function ( response ) {
				    		
				    		_(response.result)
				    			.filter({ result: 'success' })
				    			.each(function ( r ) {
				    				var file = _.find(files, { id: r.file_id });
				    				file.updateWith(r.data);
				    			});
				    			
			    			if(_.countBy(response.result, { result: 'error' }) > 0) {
			    				throw new Error();
			    			}
				    		
				    	})
			    	
			    	['finally'](function ( ) {
			    		return $q.all(folders.map(function ( folder ) {
    			    		return smartHttp.connect({
    							url: 'directory/delete/',
    							method: 'POST',
    							data: {
    								directory_id: folder.id
    							},
    							blocking: false
    						});
    			    	}));
			    	})
					['catch'](function ( ) {
						throw systemMessageService.emitError('Niet alle bestanden konden worden verwijderd. Probeer het later opnieuw.');
					})
					['finally'](function ( ) {
						$scope.reloadData();
					});
				
			};
			
			$scope.restoreEntity = function ( entities ) {
				entities = entities.concat();
				angular.forEach(entities, function ( entity ) {
					$scope.list.add(entity);
					if($scope.isSelected(entity)) {
						$scope.deselectEntity(entity);
					}
					
					smartHttp.connect({
						url: _url + 'update/',
						method: 'POST',
						data: {
							file_id: entity.id,
							deleted: false
						},
						blocking: false
					})
						.success(function ( ) {
							entity.deleted = false;
						})
						.error(function ( ) {
							$scope.trash.add(entity);
						});
				});
			};
			
			$scope.destroyEntity = function ( entities ) {
				
				entities = entities.concat();
				angular.forEach(entities, function ( entity ) {
					
					entity.getParent().remove(entity);
					
					if($scope.isSelected(entity)) {
						$scope.deselectEntity(entity);
					}
					
					smartHttp.connect( {
						url: _url + 'update/',
						method: 'POST',
						data: {
							file_id: entity.id,
							destroyed: true
						},
						blocking: false
					})
						.success(function ( ) {
							
						})
						.error(function (response) {
							$scope.$emit('systemMessage', {
								type: 'error',
								content: translationService.get(response.messages.join(""))
							});

							$scope.trash.add(entity);
						});
				});
				
			};
			
			$scope.acceptFile = function ( files ) {
				
				var fileList = getFlatFileList(),
					toAccept = _.filter(files, function ( file ) {
						return Number(file.is_revision) !== 1;
					}),
					toUpdate = _.difference(files, toAccept),
					actions;
					
				files = files.concat();
				
				if(_.uniq(toUpdate, 'is_duplicate_of').length < toUpdate.length) {
					systemMessageService.emitError('U kunt niet meerdere versies van hetzelfde bestand tegelijkertijd accepteren.');
					return;
				}
					
				_.each(files, function ( file ) {
					
					file.updating = true;
					
					if($scope.isSelected(file)) {
						$scope.deselectEntity(file);
					}
				});
				
				_.each(toUpdate, function ( file ) {
					var fileToUpdate = _.find(fileList, function ( f ) {
						return f.id === file.is_duplicate_of || f.root_file_id === file.is_duplicate_of;
					});
					
					file.getParent().remove(file);
					
					// if it doesn't exist, it's deleted
					if(fileToUpdate) {
						fileToUpdate.updating = true;
					} else {
						$scope.list.add(file);
					}
					
				});
				
				_.each(toAccept, function ( file ) {
					$scope.list.add(file);
				});
				
				// rebuild file list after
				fileList = getFlatFileList();
				
				actions = _.map(toUpdate, function ( file ) {
					return {
						id: file.is_duplicate_of,
						action: 'update_file',
						data: {
							existing_file_id: file.id
						}
					};
					
				}).concat(_.map(toAccept, function ( file ) {
					return {
						id: file.id,
						action: 'update_properties',
						data: {
							accepted: true
						}
					};
               	}));
               	
               	actions = _(actions)
               				.indexBy('id')
               				.mapValues(function ( action ) {
               					return _.omit(action, 'id');
               				})
               				.value();
				
				smartHttp.connect({
					url: '/api/bulk/file/update',
					method: 'POST',
					data: {
						files: actions
					},
					blocking: false
				})
					.success(function ( response ) {
						
						var statuses = response.result;
							
						_.each(statuses, function ( status ) {
							
							var fileId = Number(status.file_id),
								result = status.result,
								file,
								data = status.data;
								
							if(result === 'error') {
								systemMessageService.emitSaveError('uw wijzigingen');
							} else {
								file = _.find(fileList, status.action === 'update_file' ? { root_file_id: data.root_file_id } : { id: fileId });
								
								// if it doesn't exist, it's deleted
								if(file) {
									file.updating = false;
									file.updateWith(data);
									if(file.destroyed) {
										file.getParent().remove(file);
									}
								}
							}
							
						});
						
						$scope.$emit('file.accept');
						
					})
					.error(function ( ) {
						
						systemMessageService.emitSaveError('uw wijzigingen');
						
					})
					['finally'](function ( ) {
						
						$scope.reloadData();
						
					});
			};
			
			$scope.rejectFile = function ( files, rejectionReason ) {
				rejectionReason = rejectionReason || 'foo';
				files = files.concat();
				angular.forEach(files, function ( file ) {
					if($scope.isSelected(file)) {
						$scope.deselectEntity(file);
					}

					$scope.intake.remove(file);
					smartHttp.connect({
						url: _url + 'update/',
						method: 'POST',
						data: {
							file_id: file.id,
							accepted: false,
							rejection_reason: rejectionReason
						},
						blocking: false
					})
						.success(function ( ) {
							$scope.$emit('file.reject', file);
						})
						.error(function ( ) {
							$scope.intake.add(file);
						});
				});
			};
			
			$scope.assignCaseDoc = function ( files, caseDoc ) {
				angular.forEach(files, function ( file ) {
					
					var data = {
							file_id: file.id
						},
						ids = _.map(file.case_documents, function ( cd ) {
							return cd.id;
						});
						
					if(indexOf(ids, caseDoc.id) !== -1) {
						return;
					}
					
					ids.push(caseDoc.id);
					file.case_documents.push(caseDoc);
					
					data.case_document_ids = ids;
					
					smartHttp.connect({
						url: 'file/update',
						method: 'POST',
						data: data,
						blocking: false
					})
						.success(function() {
							$scope.reloadData();
						})
						.error(function ( ) {
							$scope.$emit('systemMessage', {
								type: 'error',
								content: translationService.get('Er ging iets fout bij het toewijzen van het kenmerk')
							});
							file.case_documents = _.without(file.case_documents, caseDoc);
						});
				});
			};
			
			$scope.moveCaseDoc = function ( from, to, caseDoc ) {
				var data = {
						to: to.id,
						from: from.id,
						case_document_id: caseDoc.id
					},
					ids;

				// Return if the ID already exists in the target file
				ids = _.map(to.case_documents, function ( cd ) {
					return cd.id;
				});
				if(indexOf(ids, caseDoc.id) !== -1) {
					return;	
				}
				
				// Process the update in the UI
				from.case_documents = _.filter(from.case_documents, function ( cd ) {
					return cd.id !== caseDoc.id;
				});
				
				to.case_documents.push(caseDoc);

				// Process remote
				smartHttp.connect({
					url: 'file/move_case_document',
					method: 'POST',
					data: data,
					blocking: false
				})
					.success(function() {
						$scope.reloadData();
					})
					.error(function ( ) {
						$scope.$emit('systemMessage', {
							type: 'error',
							content: translationService.get('Er ging iets fout bij het toewijzen van het kenmerk')
						});
						from.case_documents.push(caseDoc);
						to.case_documents = _.without(to.case_documents, caseDoc);
					});
			};
			
			$scope.removeCaseDoc = function ( files, caseDoc ) {
				angular.forEach(files, function ( file ) {
					var data = {
							file_id: file.id
						},
						ids = _.map(file.case_documents, function ( cd ) {
							return cd.id;
						}),
						index = indexOf(ids, caseDoc.id);
						
					if(index === -1) {
						return;
					}
						
					ids.splice(index, 1);
					file.case_documents = _.filter(file.case_documents, function ( cd ) {
						return cd.id !== caseDoc.id;
					});
					
					data.case_document_ids = ids;
					
					smartHttp.connect({
						url: 'file/update',
						method: 'POST',
						data: data,
						blocking: false
					})
						.success(function() {
							$scope.reloadData();
						})
						.error(function ( ) {
							$scope.$emit('systemMessage', {
								type: 'error',
								content: translationService.get('Er ging iets fout bij het verwijderen van het kenmerk')
							});
							file.case_documents.push(caseDoc);
						});
				});
				
			};
			
			$scope.clearCaseDocs = function ( files ) {
				angular.forEach(files, function ( file ) {
					
					var caseDocs = file.case_documents;
					
					file.case_documents = [];
					
					smartHttp.connect({
						url: 'file/update',
						method: 'POST',
						data: {
							file_id: file.id,
							case_document_ids: []
						},
						blocking: false
					})
						.success(function() {
							$scope.reloadData();
						})
						.error(function ( ) {
							file.case_documents = caseDocs;
							
							$scope.$emit('systemMessage', {
								type: 'error',
								content: translationService.get('Er ging iets fout bij het verwijderen van het kenmerk')
							});
						});
				});
			};
			
			$scope.handleCaseDocClick = function ( caseDoc/*, $event*/ ) {
				var files = $scope.selectedFiles.concat();
					
				if(!caseDoc) {
					$scope.clearCaseDocs(files);
				} else {
					if(!$scope.hasCaseDoc(files, caseDoc)) {
						$scope.assignCaseDoc(files, caseDoc);
					} else {
						$scope.removeCaseDoc(files, caseDoc);
					}
				}
			};
			
			$scope.hasCaseDoc = function ( files, caseDoc ) {
				var i,
					l;
				
				function has ( file ) {
					if(!caseDoc) {
						return file.case_documents && file.case_documents.length === 0;
					}
					return _.filter(file.case_documents, function ( cd ) {
						return caseDoc.id === cd.id;
					}).length > 0;
				}
				
				for(i = 0, l = files.length; i < l; ++i) {
					if(!has(files[i])) {
						return false;
					}
				}
				
				return true;
			};
			
			$scope.getCaseDocAssignment = function ( caseDoc ) {
				
				var id = caseDoc ? caseDoc.id : null,
					flatFileList = getFlatFileList(),
					i,
					l,
					file;
					
					
				for(i = 0, l = flatFileList.length; i < l; ++i) {
					file = flatFileList[i];
					if(file.case_type_document_id && file.case_type_document_id.id === id) {
						return file;
					}
				}
				
				return null;
				
				
			};
			
			$scope.filterEntity = function ( entity ) {
				var name = entity.name,
					desc = entity.desc,
					type = entity.type ? entity.type.name : '',
					i,
					l,
					j,
					m,
					toTest = [],
					word,
					match;
				
				if($scope.filterQuery === '') {
					return true;
				}
					
				if(name) {
					toTest.push(name.toLowerCase());
				}
				if(desc) {
					toTest.push(desc.toLowerCase());
				}
				if(type) {
					toTest.push(type.toLowerCase());
				}
				
				for(i = 0, l = words.length, m = toTest.length; i < l; ++i) {
					word = words[i].toLowerCase();
					match = false;
					for(j = 0; j < m; ++j) {
						if(toTest[j].indexOf(word) !== -1) {
							match = true;
							break;
						}
					}
					if(!match) {
						return false;
					}
				}
				
				return true;
			};
			
			$scope.getFilteredChildren = function ( source ) {
				
				var filtered = [];
				
				function filterChildren ( parent, filterRoot ) {
					var i,
						l,
						children,
						entity;
						
						
					if(filterRoot && $scope.filterEntity(parent)) {
						filtered.push(parent);
					}
					
					children = parent.getFolders();
						
					for(i = 0, l = children.length; i < l; ++i) {
						entity = children[i];
						if($scope.filterEntity(entity)) {
							filtered.push(entity);
						}
						filterChildren(entity, true);
					}
					
					children = parent.getFiles();
					for(i = 0, l = children.length; i < l; ++i) {
						entity = children[i];
						if($scope.filterEntity(entity)) {
							filtered.push(entity);
						}
					}
				}
				
				if(!source) {
					source = $scope.root;
				}
			
				filterChildren(source, false);
				
				return filtered;
				
			};
			
			$scope.getAttachments = function ( ) {
				var attachments = [],
					selection = $scope.selectedFiles.concat(),
					file;
					
				for(var i = 0, l = selection.length; i < l; ++i) {
					file = selection[i];
					if(file.getEntityType() === 'file') {
						attachments.push(file);
					}
				}
				return attachments;
			};
			
			$scope.clearCutSelection = function ( ) {
				cutFiles.length = 0;
			};
			
			$scope.cutFiles = function ( files ) {
				var i,
					l,
					file;
					
				$scope.clearCutSelection();
				
				for(i = 0, l = files.length; i < l; ++i) {
					file = files[i];
					if(file.getEntityType() === 'file') {
						cutFiles.push(file);
					}
				}
			};
			
			$scope.pasteFiles = function ( ) {
				var target,
					files = $scope.selectedFiles;
				
				if(files.length && files[0].getEntityType() === 'folder') {
					target = files[0];
				} else if(files.length) {
					target = files[0].getParent();
				}
				
				$scope.moveEntity(cutFiles, target);
				$scope.clearCutSelection();
				
			};
			
			$scope.isCut = function ( file ) {
				return indexOf(cutFiles, file) !== -1;
			};
			
			$scope.hasCutFiles = function ( ) {
				return cutFiles && cutFiles.length;
			};
			
			$scope.isCuttable = function ( ) {
				var files = $scope.selectedFiles,
					i,
					l,
					file;
					
				for(i = 0, l = files.length; i < l; ++i) {
					file = files[i];
					if(file.getEntityType() === 'file') {
						return true;
					}
				}
				
				return false;
			};
			
			$scope.isPastable = function ( ) {
				return cutFiles.length;
			};
			
			$scope.hasFilesInSubdirectories = function ( ) {
				var selection = $scope.selectedFiles,
					file,
					root = $scope.root;
					
				for(var i = 0, l = selection.length; i < l; ++i) {
					file = selection[i];
					if(file.getParent() !== root) {
						return true;
					}
				}
				
				return false;
			};
			
			$scope.downloadFile = function ( fileId, as, $event ) {
				var isIntake = $scope.isIntake,
					url = isIntake ? ('/zaak/intake/' + fileId + '/download') : ('/zaak/' + $scope.caseId + '/document/' + fileId + '/download');

				if(as) {
					url += '/' + as;
				}
					
                if($scope.pip) {
                    url = '/pip' + url;
                }

				$window.open(url);
				if($event) {
					$event.stopPropagation();
				}
			};
			
			$scope.downloadZip = function ( files, $event ) {
				
				var toDownload = _.filter(files, function ( entity ) {
						return entity.getEntityType() === 'file';
					}),
					url = '/api/case/' + $scope.caseId + '/file/download/?format=zip&';
					
				url += _.map(toDownload, function ( file ) {
					return 'file=' + file.id;
				}).join('&');
				
				$window.open(url);
				if($event) {
					$event.stopPropagation();
				}
			};
			
			$scope.downloadAllFiles = function ( ) {
				$window.open('/api/case/' + $scope.caseId + '/file/download/?format=zip');
			};
			
			function validateEntity ( entity/*, target*/ ) {
				if(entity instanceof Folder) {
					return false;
				}
				
				return true;
			}
			
			function moveEntities ( targetFolder, data ) {
				var dropData = data,
					selectedFiles = $scope.selectedFiles;
					
				if(indexOf(selectedFiles, dropData) === -1) {
					$scope.moveEntity( [ dropData ], targetFolder);
				} else {
					$scope.moveEntity(selectedFiles, targetFolder);
				}
			}
			
			function setRootState ( ) {
				var files = $scope.root.getFiles(),
					folders = $scope.root.getFolders(),
					isSelected;
					
				if(files.length || folders.length) {
					isSelected = checkSelected($scope.root);
				} else {
					isSelected = $scope.root.getSelected();
				}
				
				$scope.root.setSelected(isSelected);
				
			}
			
			function checkSelected ( parent ) {
				var files = parent.getFiles(),
					folders = parent.getFolders(),
					i,
					l;
					
				for(i = 0, l = folders.length; i < l; ++i) {
					if(!folders[i].getSelected() || !checkSelected(folders[i])) {
						return false;
					}
				}
				
				for(i = 0, l = files.length; i < l; ++i) {
					if(!files[i].getSelected()) {
						return false;
					}
				}
				return true;
			}
			
			function getFlatFileList ( ) {
				var files = [];
					
				function getChildrenOf ( parent ) {
					var children = parent.getFolders(),
						i,
						l;
						
					files = files.concat(parent.getFiles());
					
					for(i = 0, l = children.length; i < l; ++i) {
						getChildrenOf(children[i]);
					}
				}
				
				getChildrenOf($scope.root);
				getChildrenOf($scope.list);
				
				return files;
			}
			
			$scope.$on('drop', function ( event, data, mimeType ) {
				var targetFolder = event.targetScope.entity;
				if(mimeType !== 'Files') {
					moveEntities(targetFolder, data);
				}
				$scope.dragging = false;
				$scope.$apply();
			});
			
			$scope.$on('reload', function ( ) {
				clearData();
			});
			
			$scope.$watch('filterQuery', function ( ) {
				words = $scope.filterQuery.split(' ');
			});
			
			$scope.$on('fileselect', function ( event, files ) {
				$scope.uploadFile(files);
			});
			
			$scope.$on('startdrag', function ( event, element, mimeType ) {
				
				var entity = event.targetScope.entity;
				if(entity && !$scope.isSelected(entity)) {
					$scope.deselectAll();
					$scope.selectEntity(entity);
				}
				
				$scope.dragging = mimeType;
				$scope.$apply();
			});
			
			$scope.$on('stopdrag', function ( /*event, element, mimeType*/ ) {
				$scope.dragging = '';
				$scope.$apply();
			});
			
			$scope.$on('contextmenuopen', function ( event ) {
				var entity = event.targetScope.entity;
				event.stopPropagation();
				if(!$scope.isSelected(entity)) {
					$scope.deselectAll();
					$scope.selectEntity(entity);
				}
			});
			
	}]);
})();
