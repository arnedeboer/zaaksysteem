/*global angular,fetch,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.docs')
		.controller('nl.mintlab.docs.TemplateController', [ '$scope', 'smartHttp', 'translationService', '$timeout', 'caseDocumentService', function ( $scope, smartHttp, translationService, $timeout, caseDocumentService ) {
			
			var File = fetch('nl.mintlab.docs.File'),
				defaultTemplateId;
				
			$scope.template = null;
			
			$scope.isTemplateUploading = function ( ) {
				return $scope.loadState.template;
			};
			
			$scope.createFileFromTemplate = function ( name, template, caseDocId, targetFormat ) {
				
				var file = new File(),
					params,
					templateId,
					mimetype;
					
				if(typeof template === 'string' || typeof template === 'number') {
					templateId = template;
				} else {
					templateId = template.bibliotheek_sjablonen_id.id;
				}
					
				file.name = name;
				file.extension = '.' + targetFormat;
				file.extension_dotless = targetFormat;
				
				if(targetFormat === 'odt') {
					mimetype = 'application/vnd.oasis.opendocument.text';
				} else {
					mimetype = 'application/pdf';
				}
				
				file.filestore_id = {
					mimetype: mimetype
				};
				
				file.updating = true;
				
				params = {
					case_id: $scope.caseId,
					name: name,
					zaaktype_sjabloon_id: template.id,
					target_format: targetFormat
				};
				
				if(caseDocId) {
					params.case_document_ids = caseDocId;
				}
				
				function onComplete ( ) {
					$timeout(function ( ) {
						file.updating = false;
					});
				}
				
				$scope.loadState.template = true;
				
				return smartHttp.connect( {
					url: 'file/file_create_sjabloon',
					method: 'POST',
					data: params,
					blocking: false
				})
					.success(function ( data ) {
						
						var fileData = data.result[0];
				
						if(fileData.accepted) {
							$scope.list.add(file);
						} else {
							$scope.intake.add(file);
						}
						
						file.updateWith(fileData);
						
						file.case_documents = _.map(fileData.case_documents, function ( caseDocId ) {
							return caseDocumentService.getCaseDocById(caseDocId);
						});
						
						onComplete();
					})
					.error(function ( /*data*/ ) {
						$scope.$emit('systemMessage', {
							type: 'error',
							content: translationService.get('Sjabloon "%s" kon niet worden toegevoegd', name + '.' + targetFormat)
						});
						onComplete();
					})
					['finally'](function ( ) {
						$scope.loadState.template = false;
					});
			};
			
			$scope.setTemplateId = function ( tplId ) {
				var i,
					l,
					tpl,
					templates = $scope.templates;
				
				for(i = 0, l = templates.length; i < l; ++i) {
					tpl = templates[i];
					if(tpl.id === tplId) {
						$scope.template = tpl;
						return;
					}
				}
				
				defaultTemplateId = tplId;
				
			};
			
			$scope.$watch('templates', function ( nw/*, old*/ ) {
				if(nw && nw.length) {
					if(!defaultTemplateId) {
						$scope.template = $scope.templates[0];
					} else {
						$scope.setTemplateId(defaultTemplateId);
					}
				}
			});
			
			$scope.$watch('template', function ( nw, old ) {
				var template = $scope.template,
					name = $scope.name;
					
				if(!name || old && old.bibliotheek_sjablonen_id.naam === name) {
					if(!template) {
						name = '';
					} else {
						name = template.bibliotheek_sjablonen_id.naam;
					}
					$scope.name = name;	
				}
				
				$scope.targetFormat = $scope.template && $scope.template.target_format ? $scope.template.target_format : 'odt';
				
			});
			
			
		}]);
	
})();
