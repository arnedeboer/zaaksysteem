/*global angular,fetch,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.docs')
		.controller('nl.mintlab.docs.DocumentController', [ '$scope', 'smartHttp', '$filter', '$routeParams', 'fileUploader', '$timeout', '$q', 'caseDocumentService', function ( $scope, smartHttp, $filter, $routeParams, fileUploader, $timeout, $q, caseDocumentService ) {
				
			var safeApply = fetch('nl.mintlab.utils.safeApply'),
				Folder = fetch('nl.mintlab.docs.Folder'),
				File = fetch('nl.mintlab.docs.File');
			
			$scope.trash = new Folder( { name: 'Prullenbak' });
			$scope.list = new Folder( { name: 'Map' });
			$scope.intake = new Folder( { name: 'Intake' });
			$scope.caseDocs = [];
			$scope.docCats = [];
			$scope.initialized = false;
			$scope.readOnly = false;
			$scope.loading = false;
			$scope.loadState = {};
			
			$scope.view = 'list';
			
			$scope.reloadData = function ( ) {
				var promises = [],
					fileData,
					folderData,
					openFolders = [];
					
				if($scope.list) {
					_.each($scope.list.getFolders(), function ( folder ) {
						if(!folder.getCollapsedInScope()) {
							openFolders.push(folder.id);
						}
					});
				}

				$scope.loading = true;
				
				promises.push(
					smartHttp.connect({
						url: ($scope.pip ? '/pip/zaak/' + $scope.caseId + '/file/search' :
							'/api/case/' + $scope.caseId + '/file/search/'),
						method: 'GET'
					})
						.success(function ( data ) {
							fileData = data;
						})
				);
					
				if(!$scope.pip) {
					promises.push(
						smartHttp.connect( {
							url: 'directory/search/case_id/' + $scope.caseId,
							method: 'GET'
						})
							.success(function ( data ) {
								folderData = data;
							})
					);
				}
				
				if(!$scope.pip) {
					promises.push(
						caseDocumentService.getCaseDocumentsByCaseId($scope.caseId)
							.then(processCaseDocData)
					);
				}
					
				if(!$scope.pip) {
					promises.push(
						smartHttp.connect( {
							url: 'file/document_categories',
							method: 'GET'
						})
							.success(processDocCatData)
					);
				}
				
				$q.all(promises).then(function ( ) {
					
					var files = [],
						caseDocsById = {};
						
					$scope.trash.empty();
					$scope.list.empty();
					$scope.intake.empty();
					
					processFileData(fileData);
					processFolderData(folderData, openFolders);
						
					_.each($scope.caseDocs, function ( caseDoc ) {
						caseDocsById[caseDoc.id] = caseDoc;
					});
					
					files = getFlatFileList();
					
					_.each(files, function ( file ) {
						file.case_documents = _.map(file.case_documents, function ( caseDocId ) {
							return caseDocsById[caseDocId];
						});
					});
					
					$scope.loading = false;
					
					$scope.$broadcast('reload');
				});
			};
			
			$scope.setView = function ( view ) {
				$scope.view = view;
			};
			
			$scope.addFolder = function ( folder, folderName ) {
				var child = new Folder( { name: folderName });
				folder.add(child);
			};
			
			$scope.uploadFile = function ( files ) {
				
				var needReload = false, 
					uploadPromises = [],
					url = $scope.pip ? 'pip/file/create' : 'file/create';
				
				angular.forEach(files, function ( value ) {
					var promise = fileUploader.upload(value, smartHttp.getUrl(url), {
						'case_id': $scope.caseId
					}).promise.then(function ( upload ) {

						var file = new File(),
							result = upload.getData().result,
							reloadMimetypes = [
								'application/x-ole-storage', 
								'application/vnd.ms-outlook', 
								'message/rfc822'
							],
							data = result ? result[0] : null;

						file.updating = true;
						
						file.case_documents = data.case_documents;
						file.updateWith(data);
						
						$timeout(function ( ) {
							file.updating = false;
						});

						// for certain mimetypes, the server will unwrap and 
						// but the file contents in a folder. for these
						// cases, reload the document screen after
						// all files have been uploaded
						// in the case of reload, we don't need to bother rendering -
						// we'll do that next time.
						if (_.contains(reloadMimetypes, data.filestore_id.mimetype)) {
							needReload = true;
						} else {
							if(!file.accepted) {
								$scope.intake.add(file);
							} else {
								$scope.list.add(file);
							}
							countIntake();
						}

					}, function ( /*upload*/ ) {
						
					});
					uploadPromises.push(promise);
				});

				$q.all(uploadPromises).then(function () {
					if (needReload) {
						$scope.reloadData();
					}
				});
			};
			
			$scope.replaceFile = function ( file, replace ) {
				var replacement = replace[0];
				
				file.updating = true;
				
				if(replacement) {
					fileUploader.upload(replacement, smartHttp.getUrl('file/update_file'), {
						'file_id': file.id
					}).promise.then(function ( upload ) {
						var result = upload.getData().result,
							data = result ? result[0] : null;
						file.updating = false;
						file.updateWith(data);
					}, function ( /*upload*/ ) {
						file.updating = false;
					});
				}
			};
			
			function processFileData ( data ) {
				var files = data.result || [],
					parsedFiles = [];
					
				angular.forEach(files, function ( file ) {
					parsedFiles.push(parseFileData(file));
				});
				
				countIntake();
				
				$scope.initialized = true;
			}
			
			function processFolderData ( data, openFolders ) {
				var folders = data.result || [];
				
				angular.forEach(folders, function ( folder ) {
					var collapsed = true;
					
					
					if(_.indexOf(openFolders, folder.id) !== -1) {
						collapsed = false;
					}
					
					parseFolderData(folder, collapsed);
				});
			}
			
			function processCaseDocData ( data ) {
				$scope.caseDocs = data || [];
			}
			
			function processDocCatData ( data ) {
				var docCats = data.result || [],
					result = [];
				
				for(var i = 0, l = docCats.length; i < l; ++i) {
					result.push({
						index: i,
						label: docCats[i],
						value: docCats[i]
					});
				}
				
				$scope.docCats = result;
			}
			
			function parseFileData ( fileData ) {
				var file = new File(),
					parent;
					
				file.updateWith(fileData);
				file.case_documents = fileData.case_documents;
				
				if(file.accepted && !file.date_deleted) {
					if(file.directory_id) {
						parent = $scope.list.getEntityByPath(file.directory_id.id) || parseFolderData(file.directory_id);
					}
					
					if(!parent) {
						parent = $scope.list;
					}
					
				} else if(!file.accepted) {
					parent = $scope.intake;
				} else {
					parent = $scope.trash;
				}
				parent.add(file);
			}
			
			function parseFolderData ( folderData, collapsed ) {
				var folder = $scope.list.getEntityByPath( [ 'folder_' + folderData.id ]);
					
				if(!folder){
					folder = new Folder( { id: folderData.id, uuid: folderData.id } );
					
					$scope.list.add(folder);
				}
				if(collapsed !== undefined) {
					folder.setCollapsed(collapsed);	
				}
					
				folderData.uuid = folderData.id;
					
				folder.updateWith(folderData);
				
				return folder;
			}
			
			function countIntake ( ) {
				$scope.setNotificationCount('documents', $scope.intake.getFiles().length);
			}
			
			function getFlatFileList ( ) {
				var files = [];
					
				function getChildrenOf ( parent ) {
					var children = parent.getFolders(),
						i,
						l;
						
					files = files.concat(parent.getFiles());
					
					for(i = 0, l = children.length; i < l; ++i) {
						getChildrenOf(children[i]);
					}
				}
				
				getChildrenOf($scope.intake);
				getChildrenOf($scope.list);
				
				return files;
			}
			
			$scope.getCaseDocLabel = function ( caseDoc ) {
				var label = '';
				
				if(caseDoc) {
					label = caseDoc.label || (caseDoc.bibliotheek_kenmerken_id ? caseDoc.bibliotheek_kenmerken_id.naam : '');
				}
				return label;
			};
			
			$scope.isSameCaseDoc = function ( caseDocA, caseDocB ) {
				if(caseDocA === caseDocB) {
					return true;
				}
				
				if(!caseDocA || !caseDocB) {
					return false;
				}
				
				return caseDocA.id === caseDocB.id;
			};
			
			$scope.isNotReferentialCaseDoc = function ( caseDoc ) {
				return !caseDoc || !caseDoc.referential;
			};
			
			$scope.init = function ( ) {
				$scope.reloadData();
			};
			
			$scope.isLoading = function ( ) {
				return $scope.loading;
			};
		
			$scope.$on('drop', function ( event, data, mimeType ) {
				if(mimeType === 'Files') {
					$scope.uploadFile(data);
				}
			});
			
			$scope.$on('file.accept', function ( event, file ) {
				safeApply($scope, function ( ) {
					countIntake();
				});
			});
			
			$scope.$on('file.reject', function ( event, file ) {
				safeApply($scope, function ( ) {
					countIntake();
				});
			});
			
		}]);
})();
