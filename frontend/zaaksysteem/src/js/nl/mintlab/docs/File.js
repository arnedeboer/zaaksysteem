/*global define, fetch*/
(function ( ) {
	
	define('nl.mintlab.docs.File', function ( ) {
		
		var inherit = fetch('nl.mintlab.utils.object.inherit'),
			StoredEntity = fetch('nl.mintlab.docs.StoredEntity');
		
		function File ( ) {
			this._entityType = 'file';
			File.uber.constructor.apply(this, arguments);
		}
		
		inherit(File, StoredEntity);
		
		File.prototype.setAsRevision = function ( asRevision ) {
			this._asRevision = asRevision;
		};
		
		File.prototype.getAsRevision = function ( ) {
			return this._asRevision;
		};
		
		return File;
		
	});
})();