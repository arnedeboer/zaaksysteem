/*global $,angular,fetch*/
(function ( ) {

    angular.module('Zaaksysteem.docs')
        .controller('nl.mintlab.docs.FileAttributeController', [ '$scope', '$window', 'smartHttp', 'flexpaperService', function ( $scope, $window, smartHttp, flexpaperService ) {

            $scope.onClickDoc = function ( event ) {

                if (flexpaperService.canDisplay()) {
                    if (flexpaperService.canDisplayInIFrame()) {
                        $scope.openPopup();
                    } else {
                        $window.open($scope.flexpaperUrl);
                    }
                } else {
                    $window.open(smartHttp.getUrl($scope.downloadUrl));
                    event.stopPropagation();
                }
            };

        }]);
}());
