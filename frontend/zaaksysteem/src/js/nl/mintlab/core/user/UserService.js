/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.core.user')
		.factory('userService', [ '$parse', '$http', 'dataStore', function ( $parse, $http, dataStore ) {
			
			var user = dataStore.read('user'),
				userService = {};
				
			userService.getCurrentUser = function ( ) {
				return user;	
			};
			
			userService.getCurrentUuid = function ( ) {
				return user ? user.betrokkene_identifier : undefined;
			};
			
			userService.getCurrentUsername = function ( ) {
				return user ? user.username : undefined;	
			};
			
			userService.isAdmin = function ( ) {
				return user && user.is_admin;	
			};
			
			userService.getSetting = function ( name ) {
				if(!user) {
					return undefined;
				}
				
				return $parse(name)(user.settings);
			};
			
			userService.setSetting = function ( name, value ) {
				
				var currentValue = userService.getSetting(name),
					data;
				
				if(!user || currentValue === value) {
					return;
				}
				
				data = {};
				
				$parse(name).assign(user.settings, value);
				$parse(name).assign(data, value);
				
				$http({
					method: 'POST',
					url: '/api/user/settings',
					data: data
				});
			};
			
			return userService;
			
		}]);
	
})();
