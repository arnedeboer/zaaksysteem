/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsCrudDataProvider', [ '$parse', function ( $parse ) {
			
			return {
				require: [ 'zsCrudDataProvider', 'zsCrudTemplateParser' ],
				controller: [ '$scope', '$element', '$attrs', function ( $scope, $element, $attrs ) {
					
					var ctrl = {},
						zsCrudTemplateParser,
						getter = $parse($attrs.zsCrudDataProvider);
							
					function getCollection ( ) {
						var items = getter($scope, {
							'$currentPage': zsCrudTemplateParser.getCurrentPage(),
							'$numPerPage': zsCrudTemplateParser.getNumPerPage(),
							'$sortBy': zsCrudTemplateParser.getSortBy(),
							'$sortReversed': zsCrudTemplateParser.isSortReversed()
						});
						return items;
					}
					
					ctrl.link = function ( controllers ) {
						zsCrudTemplateParser = controllers[0];
						
						zsCrudTemplateParser.setDataProvider(ctrl);
						
						$scope.$watchCollection(getCollection, function ( newItems/*, oldItems*/ ) {
							zsCrudTemplateParser.setItems(newItems);
						});
					};
					
					return ctrl;
					
				}],
				link: function ( scope, element, attrs, controllers ) {
					controllers[0].link(controllers.splice(1));
				}
			};
			
		}]);
	
})();
