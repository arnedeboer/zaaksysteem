/*global angular,fetch,_*/
(function ( ) {
	
	var TEMPLATE_URL = '/html/core/crud/crud-interface.html';
	
	angular.module('Zaaksysteem')
		.directive('zsCrudTemplateParser', [ '$compile', '$parse', '$q', '$timeout', '$interpolate', 'smartHttp', 'templateCompiler', 'formService', 'translationService', function ( $compile, $parse, $q, $timeout, $interpolate, smartHttp, templateCompiler, formService, translationService ) {
			
			return {
				scope: true,
				require: 'zsCrudTemplateParser',
				controller: [ '$scope', function ( $scope ) {
					
					var ctrl = {},
						hasColumnManagement = false,
						selection = [],
						selectionType = 'subset',
						resolve = 'id',
						dataProvider,
						filters = [],
						filteredItems = [];
						
					ctrl.enableColumnManagement = function ( ) {
						hasColumnManagement = true;
					};
					
					ctrl.disableColumnManagement = function ( ) {
						hasColumnManagement = false;
					};
					
					ctrl.hasColumnManagement = function ( ) {
						return hasColumnManagement;
					};
					
					ctrl.setSelection = function ( items) {
						selection = items;
					};
					
					ctrl.getSelection = function ( ) {
						return selection;
					};
					
					ctrl.setSelectionType = function ( type ) {
						selectionType = type;
					};
					
					ctrl.getSelectionType = function ( ) {
						return selectionType;
					};
					
					ctrl.getResolve = function ( ) {
						return resolve;
					};
					
					ctrl.setResolve = function ( res ) {
						resolve = res;	
					};
					
					ctrl.resolveItemId = function ( item ) {
						return $parse(resolve)(item);
					};
					
					ctrl.reloadData = function ( ) {
						$scope.reloadData();
					};
					
					ctrl.getTrackingId = function ( item, $index ) {
						var id = resolve ? ctrl.resolveItemId(item) : $index;
						return id;
					};
					
					ctrl.setItems = function ( items ) {
						
						if(!items) {
							items = [];
						}
						
						$scope.items = items;
						$scope.$emit('zs.crud.item.change', items);
						
						_.each(ctrl.itemChangeListeners, function ( fn ) {
							fn();
						});	
					};
					
					ctrl.getSortBy = function ( ) {
						return $scope.getSortBy();	
					};
					
					ctrl.isSortReversed = function ( ) {
						return $scope.isSortReversed();
					};
					
					ctrl.getCurrentPage = function ( ) {
						return $scope.currentPage;
					};
					
					ctrl.getNumPerPage = function ( ) {
						return $scope.perPage;
					};
					
					ctrl.setDataProvider = function ( provider ) {
						dataProvider = provider;
					};
					
					ctrl.getDataProvider = function ( ) {
						return dataProvider;	
					};
					
					ctrl.hasDataProvider = function ( ) {
						return !!dataProvider;
					};
					
					ctrl.addFilter = function ( filter ) {
						filters.push(filter);
					};
					
					ctrl.removeFilter = function ( filter ) {
						_.pull(filters, filter);	
					};
					
					ctrl.filterItems = function ( items ) {
						var filtered = items;
						
						// angular runs into a infinite loop when returning a new array,
						// so we just modify the contents instead of creating a new one
						filteredItems.length = 0;
						
						_.each(filters, function ( filter ) {
							filtered = _.filter(filtered, filter);
						});
						
						filteredItems.push.apply(filteredItems, filtered);
						
						return filteredItems;
					};
					
					ctrl.itemChangeListeners = [];
					
					return ctrl;
					
				}],
				controllerAs: 'zsCrud',
				compile: function ( ) {
					
					return function link ( scope, element, attrs, controller ) {
						
						var templateUrl = scope.$eval(attrs.zsCrudTemplateUrl) || TEMPLATE_URL,
							baseUrl,
							templateElement,
							config,
							sortBy = null,
							sortOrder = false,
							getPromise,
							hasLoaded = false;
							
						scope.loading = false;
						scope.currentPage = 1;
						scope.perPage = 20;
						scope.items = [];
						
							
						function attemptInit ( ) {
							if(!config || !templateElement) {
								return;
							}
							
							compileInterface();
						}
							
						function reloadConfig ( ) {
							
							if(!attrs.zsCrudTemplateParser && !attrs.config) {
								setConfig(null);
								return;
							}
							
							var data;
							try {
								data = attrs.config ? scope.$eval(attrs.config) : JSON.parse(attrs.zsCrudTemplateParser);
								setConfig(data);
								attemptInit();
							} catch ( error ) {
								var url = attrs.zsCrudTemplateParser;
								smartHttp.connect({
									url: url,
									method: 'GET',
									params: {
										zapi_crud: 1
									}
								})
									.success(function ( data ) {
										setConfig(data.result[0]);
										attemptInit();
									});
							}
						}
							
						function compileInterface ( ) {
							
							if(element.children().length) {
								return;
							}
								
							templateCompiler.getCompiler(templateUrl).then(function ( compiler ) {
								compiler(scope, function ( clonedElement ) {
									for(var i = 0, l = clonedElement.length; i < l; ++i) {
										element[0].appendChild(clonedElement[0]);
									}
								});
							});
							
						}
						
						function setUrl ( url ) {
							scope.baseUrl = baseUrl = url;
						}
						
						function setTemplateElement ( el ) {
							templateElement = el;
						}
						
						function setItems ( items ) {
							controller.setItems(items);
						}
						
						function setConfig ( cnfg ) {
							
							var baseUrl,
								i,
								l;
								
							config = cnfg;
							
							cnfg = cnfg || {};
							
							scope.name = attrs.zsCrudName || cnfg.name || scope.$id;
							scope.actions = cnfg.actions;
							scope.columns = cnfg.columns;
							scope.options = cnfg.options || {};
							
							if(!scope.options.select) {
								scope.options.select = 'all';
							}
							
							scope.filters = cnfg.filters;
							scope.style = cnfg.style;
							
							controller.setSelectionType('subset');
							
							if(scope.options.resolve !== undefined) {
								controller.setResolve(scope.options.resolve);
							}
							
							setSort();
							
							if(cnfg.numrows) {
								scope.perPage = parseInt(cnfg.numrows, 10);
							}
							
							scope.numPages = 1;
							scope.numRows = 0;
							
							for(i = 0, l = scope.filters ? scope.filters.length : 0; i < l; ++i) {
								scope.filters[i].name = scope.filters[i].name.replace(/\./g, '$dot$');
							}
							
							baseUrl = config ? (attrs.zsCrudBaseUrl ? attrs.zsCrudBaseUrl : $interpolate(cnfg.url || '')(scope)) : '';
							
							setUrl(baseUrl);
							
							scope.$emit('crud.config.change');
						}
						
						function setSort ( ) {
							var options = scope.options || {};
							
							if(options.sort) {
								sortBy = options.sort.by;
								sortOrder = options.sort.order === 'desc' ? 'desc' : 'asc';
							}
							
						}
						
						scope.sort = function ( id, reversed ) {
							sortBy = id;
							sortOrder = reversed ? 'desc' : 'asc';
							scope.$emit('crud.sort.change', sortBy, sortOrder);
							scope.reloadData();
						};
						
						scope.setSelectionType = function ( selectionType ) {
							controller.setSelectionType(selectionType);
						};
						
						scope.getFilterValues = function ( ) {
							var vals = {},
								form = scope.getForm(),
								filters = scope.filters,
								i,
								l,
								val,
								filter;
								
							if(!form || !filters) {
								return vals;
							}
								
							for(i = 0, l = filters.length; i < l; ++i) {
								filter = filters[i];
								val = form.getValue(filter.name);
								if(val === undefined) {
									continue;
								}
								switch(filter.type) {
									case 'checkbox':
									val = !!val ? 1 : 0;
									break;
								}
								vals[filter.name.replace(/\$dot\$/g, '.')] = val;
							}
							
							return vals;
						};
						
						scope.reloadData = function ( ) {
							var params = {
									zapi_page: scope.currentPage,
									zapi_num_rows: scope.perPage
								},
								filters = scope.filters,
								form = scope.getForm(),
								vals = scope.getFilterValues();
								
							if(getPromise) {
								getPromise.resolve();
								getPromise = null;
							}
							
							if(!form && (filters && filters.length)) {
								return;
							}
							
							for(var key in vals) {
								params[key] = vals[key];
							}
							
							if(sortBy) {
								params.zapi_order_by = sortBy.indexOf('.') !== -1 ? sortBy : ('me.' + sortBy);
								params.zapi_order_by_direction = sortOrder;
							}
							
							if(baseUrl) {
							
								scope.loading = true;
								
								getPromise = $q.defer();
								
								smartHttp.connect({
									url: baseUrl,
									method: 'GET',
									params: params,
									timeout: getPromise.promise
								})
									.success(function ( data ) {
										
										getPromise = null;
										hasLoaded = true;
										scope.loading = false;
										
										data.num_rows = Number(data.num_rows);
										
										setItems(data.result);
										scope.numPages = Math.ceil(data.num_rows/scope.perPage) || 1;
										scope.numRows = data.num_rows;
										if(scope.currentPage > scope.numPages) {
											scope.currentPage = scope.numPages || 1;
										}
										
										if(attrs.zsCrudDataChange) {
											$parse(attrs.zsCrudDataChange)(scope, { '$response': data });
										}
										
										scope.$emit('crud.data.load');
									})
									.error(function ( /*data*/ ) {
										
										if(arguments[0] === null) {
											// arguments[0] === null = request was canceled
											return;
										}
										
										getPromise = null;
										hasLoaded = true;
										scope.loading = false;
										
										scope.$emit('systemMessage', {
											type: 'error',
											content: translationService.get('Er ging iets fout bij het ophalen van de data')
										});
										setItems([]);
										scope.$emit('crud.data.error');
									});
							} else {
								if(!controller.hasDataProvider()) {
									setItems([]);
								}
							}
						};
						
						scope.getForm = function ( ) {
							var formName = 'crud-filters-' + scope.name;
							return formService.get(formName);	
						};
						
						scope.getFormConfig = function ( ) {
							var filters = scope.filters || [],
								form;
								
							if(filters.length) {
								form = {
									name: 'crud-filters-' + scope.name,
									fieldsets: [
										{
											fields: filters
										}
									]
								};
							}
							
							return form;
						};
						
						scope.hasColumnManagement = function ( ) {
							return controller.hasColumnManagement();
						};
						
						scope.getSortBy = function ( ) {
							return sortBy;
						};
						
						scope.isSortReversed = function ( ) {
							return sortOrder === 'desc';	
						};
						
						scope.isEmptyResultSet = function ( ) {
							var items = scope.getItems(),
								isEmpty = items && items.length === 0;
								
							return isEmpty;
						};
						
						scope.isLoading = function ( ) {
							var dataProvider = controller.getDataProvider();
							return dataProvider ? (dataProvider.isLoading === undefined ? false : dataProvider.isLoading()) : scope.loading;
						};
						
						scope.getItems = function ( ) {
							return controller.filterItems(scope.items);	
						};
					
						scope.hasLoaded = function ( ) {
							return hasLoaded;
						};
						
						scope.showPagination = function ( ) {
							return scope.options.pagination === undefined || scope.options.pagination === true;	
						};
						
						scope.$watch('baseUrl', function ( ) {
							scope.reloadData();
						});
						
						scope.$on('form.ready', function ( event ) {
							if(event.targetScope.getFormName() === ('crud-filters-' + scope.name)) {
								scope.reloadData();
								scope.$on('form.change', function ( /*event, field*/ ) {
									scope.reloadData();
								});
							}
						});
						
						scope.$on('zs.pagination.page.change', function ( event, page ) {
							scope.currentPage = page;
							scope.reloadData();
						});
						
						scope.$on('zs.pagination.numrows.change', function ( event, numRows ) {
							var at = scope.perPage * (scope.currentPage-1),
								page = Math.floor(at/numRows) + 1;
							
							scope.currentPage = page;
							scope.perPage = numRows;
							scope.reloadData();
						});
						
						attrs.$observe('zsCrudTemplateParser', function ( ) {
							reloadConfig();
						});
						
						templateCompiler.getElement(templateUrl).then(function ( element ) {
							setTemplateElement(element);
							attemptInit();
						});
					};
				}
				
			};
			
		}]);
	
})();
