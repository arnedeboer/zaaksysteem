/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.controller('nl.mintlab.core.crud.CrudItemActionMenuController', [ '$scope', function ( $scope ) {
			
			$scope.behaviour = 'hover';
			
			$scope.handleOpenMenuClick = function ( /*$event*/ ) {
				$scope.setSelection([]);
			};
			
			$scope.$on('zs.ezra.dialog.open', function ( ) {
				$scope.closePopupMenu();
			});
			
		}]);

})();