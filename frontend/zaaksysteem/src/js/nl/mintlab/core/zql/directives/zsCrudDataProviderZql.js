/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.core.zql')
		.directive('zsCrudDataProviderZql', [ '$http', '$q', 'systemMessageService', function ( $http, $q, systemMessageService ) {
			
			return {
				require: [ 'zsCrudDataProviderZql', 'zsCrudTemplateParser' ],
				controller: [ '$scope', '$element', '$attrs', function ( $scope, $element, $attrs ) {
					
					var ctrl = {},
						zsCrud,
						promise;
					
					function cancelRunningRequest ( ) {
						if(promise) {
							promise.resolve();
							promise = null;
						}
					}
					
					function getNewData ( ) {
						
						var params = getParams();
						
						cancelRunningRequest();
						
						if(!params.zql) {
							return;
						}
						
						promise = $q.defer();
						
						$http({
							method: 'GET',
							url: '/api/object/search/',
							params: getParams(),
							timeout: promise.promise
						})
							.success(function ( response ) {							
								var items = response.result;
								zsCrud.setItems(items);
							})
							.error(function ( /*response*/ ) {
								systemMessageService.emitLoadError();
							})
							['finally'](function ( ) {
								promise = null;
							});
					}
					
					ctrl.reload = function ( ) {
						getNewData();	
					};
					
					ctrl.setControls = function ( ) {
						zsCrud = arguments[0];
						zsCrud.setDataProvider(ctrl);
					};
					
					ctrl.isLoading = function ( ) {
						return !!promise;	
					};
					
					function getParams ( ) {
						var params = {},
							zql = $attrs.zsCrudDataProviderZql,
							sortBy = zsCrud.getSortBy(),
							isSortReversed = zsCrud.isSortReversed(),
							sortType = $scope.$eval($attrs.zsCrudDataProviderZqlSortType),
							pageIndex = zsCrud.getCurrentPage(),
							numPerPage = zsCrud.getNumPerPage();
						
						if(sortBy) {
							zql += 'ORDER BY ' + sortBy;
							if(sortType) {
								zql += sortType;
							}
							zql += (isSortReversed ? ' DESC' : ' ASC');
						}
						
						params.zql = zql;
						params.zapi_page = pageIndex;
						params.zapi_num_rows = numPerPage;
						
						return params;
					}
						
					$scope.$watch(getParams, function ( /*newZql, oldZql*/ ) {
						getNewData();
					}, true);
					
					return ctrl;	
				}],
				link: function ( scope, element, attrs, controllers ) {
					
					controllers[0].setControls.apply(controllers[0], controllers.slice(1));
					
				}
			};
			
		}]);
	
})();