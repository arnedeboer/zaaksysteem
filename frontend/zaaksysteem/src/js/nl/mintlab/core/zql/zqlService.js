/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.core.zql')
		.service('zqlService', [ '$http', '$q', 'zqlEscapeFilter', function ( $http, $q, zqlEscapeFilter ) {
			
			var zqlService = {},
				interpreters;
				
			// todo: move this to controller
				
			function getArchiefStatus ( prop ) {
				var part,
					val = prop.value,
					parts;
				
				if(!val || !val.length) {
					return part;
				}
				
				parts = [];
				_.each(val, function ( v ) {
					switch(v) {
						case 'te_vernietigen':
						parts.push('(case.date_destruction < ' + new Date().toISOString() + ')');
						break;
					}
				});
				
				part = parts.join(' AND ');
				
				return part;
			}
			
			function isEmpty ( val ) {
				return val === '' || val === null || val === undefined || (_.isArray(val) && val.length === 0);
			}
			
			function getDateRangePart ( prop ) {
				var part = '',
					from,
					to;
					
				part += prop.name + ' BETWEEN ';
				from = parseInt(prop.value.from, 10);
				to = parseInt(prop.value.to, 10);
				
				part += new Date(from).toISOString() + ' AND ' + new Date(to).toISOString();
				return '(' + part + ')';
			}
			
			function getAttrPart ( prop ) {
				var part = '',
					parts = [];
					
				parts = _.map(prop.value.attributes, function ( attr ) {
					var name = attr.object.column_name,
						val = attr._value,
						operator = attr._operator || '=',
						part = '';
						
					if(!isEmpty(val)) {
						if(attr.object.value_type === 'date') {
							part = getPart(name, new Date(val.from).toISOString(), operator, false);
						} else {
							if(attr.is_multiple && !(operator && operator.indexOf('~') !== -1)) {
								operator = 'CONTAINS';
							}
							part = getPart(name, val, operator);
						}
					}
					
					return part;
				});
				
				parts = _.filter(parts, function ( pt ) {
					return !!pt;
				});
				
				if(parts.length) {
					if(prop.value.grouping === 'and') {
						part = parts.join(' AND ');
					} else {
						part = parts.join(' OR ');
					}
					part = '(' + part + ')';
				}
				
				return part;
			}
			
			function getPart ( name, value, operator, escape ) {
				var part = '',
					valuePart = '',
					like,
					not;
					
				if(escape === undefined) {
					escape = true;
				}
				
				if(operator === undefined) {
					operator = '=';
				}
				
				if(operator.indexOf('~') !== -1) {
					like = true;
				}
				
				if(operator.indexOf('!') === 0) {
					not = true;
					operator = operator[1];
				}
				
				if(_.isArray(value) && value.length > 1) {
					if(operator !== 'CONTAINS') {
						operator = 'IN';
					}
					valuePart += '(';
					valuePart += _.map(value, function ( val ) {
						return escape ? zqlEscapeFilter(val) : val;
					}).join(',');
					valuePart += ')';
				} else {
					if(_.isArray(value)) {
						value = value[0];
					}
					if(like) {
						value = '%' + value + '%';
					}
					valuePart = escape ? zqlEscapeFilter(value) : value;
				}
				
				part = name + ' ' + operator + ' ' + valuePart;
				if(not) {
					part = 'NOT(' + part + ')';
				}
				
				return '(' + part + ')';
			}
			
			function handleProp ( prop ) {
				var part = '';
				
				if(interpreters[prop.name]) {
					part = interpreters[prop.name](prop);
				} else {
					switch(prop.type) {
						case 'date-range':
						part = getDateRangePart(prop);
						break;
						
						case 'attributes':
						part = getAttrPart(prop);
						break;
						
						case 'property-group':
						part = '((' + _.map(prop.value, function ( val ) {
							return handleProp(val);
						}).join(') ' + prop.relation + ' (') + '))';
						break;
						
						default:
						part = getPart(prop.name, prop.value, prop.operator);
						break;
					}
				}
				
				return part;
			}
			
			function getConditionZql ( conditions, relation ) {
				var statements = [];
				
				relation = relation || ' AND ';
					
				statements = _.map(conditions, function ( condition ) { 
					var statement = '';
					
					if(condition.type === 'group') {
						statement = getConditionZql(condition.children, condition.relation);
					} else {
						statement = getPart(condition.name, condition.value, condition.operator);
					}
				
					return statement;
				});
				
				return '(' + statements.join(') ' + relation + ' (') + ')';
			}
			
			zqlService.getConditions = function ( properties ) {
				var conditions;
				
				conditions = _.map(properties, handleProp);
				
				conditions = _.filter(conditions, function ( cond ) {
					return !!cond;
				});
				
				return conditions;
			};
			
			zqlService.select = function ( table, conditions, columns, options ) {
				var zql = 'SELECT ';
				
				if(columns && columns.length) {
					zql += _.map(columns, zqlEscapeFilter).join(',');
				} else {
					zql += '{}';
				}
				
				zql += ' FROM ' + table;
				
				if(conditions && conditions.length) {
					zql += ' WHERE ' + getConditionZql(conditions);
				}
				
				return zqlService.query(zql, options);
			};
			
			zqlService.escape = function ( value ) {
				return zqlEscapeFilter(value);
			};
			
			zqlService.query = function ( zql, options ) {
				return $http({
					url: '/api/object/search',
					method: 'GET',
					params: _.merge({ zql: zql }, options)
				});
			};
			
			zqlService.escape = function ( value ) {
				return zqlEscapeFilter(value);
			};
			
			interpreters = {
				'archiefstatus': getArchiefStatus
			};
			
			return zqlService;
			
		}]);
	
})();
