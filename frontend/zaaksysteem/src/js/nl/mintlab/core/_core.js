/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.core', [ 
		'Zaaksysteem.core.bag', 
		'Zaaksysteem.core.data', 
		'Zaaksysteem.core.detail', 
		'Zaaksysteem.core.error',
		'Zaaksysteem.core.user',
		'Zaaksysteem.core.zql'
	]);
	
})();