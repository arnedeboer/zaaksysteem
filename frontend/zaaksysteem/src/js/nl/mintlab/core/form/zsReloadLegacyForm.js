/*global angular,$,loadWebform,console*/
(function ( ) {
	
	angular.module('Zaaksysteem.form')
		.directive('zsReloadLegacyForm', [ function ( ) {
			
			return {
				require: [ 'zsReloadLegacyForm', '^zsCaseWebform' ],
				scope: false,
				controller: [ '$scope', '$element', '$attrs', function ( $scope, $element, $attrs ) {
					
					var ctrl = {},
						deregister,
						zsCaseWebform;
					
					function onEvent ( ) {
						ctrl.reloadLegacyForm();
					}
					
					ctrl.reloadLegacyForm = function ( ) {
						var params = null;
							
						if($attrs.zsReloadLegacyFormParams) {
							try {
								params = $scope.$eval($attrs.zsReloadLegacyFormParams);
							} catch ( error ) {
								console.log('Error parsing parameters: ' + $attrs.zsReloadLegacyForm);
								params = {};
							}
						} else {
							params = {};
						}
						
						zsCaseWebform.reloadHtml(params);
					};
					
					ctrl.setControls = function ( ) {
						zsCaseWebform = arguments[0];
					};
					
					$attrs.$observe('zsReloadLegacyFormOn', function ( ) {
						if(deregister) {
							deregister();
							deregister = null;
						}
						if($attrs.zsReloadLegacyFormOn) {
							deregister = $scope.$on($attrs.zsReloadLegacyFormOn, onEvent);
						}
					});
					
					return ctrl;
					
				}],
				controllerAs: 'reloadLegacyForm',
				link: function ( scope, element, attrs, controllers ) {
					
					controllers[0].setControls.apply(controllers[0], controllers.slice(1));
					
				}
			};
			
		}]);
	
})();