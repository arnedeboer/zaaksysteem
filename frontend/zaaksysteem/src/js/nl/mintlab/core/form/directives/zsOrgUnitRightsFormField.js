/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.form')
		.directive('zsOrgUnitRightsFormField', [ 'organisationalUnitService', function ( organisationalUnitService ) {
			
			var rightTypes = [
				{
					id: 'zaak_beheer',
					label: 'Mag zaken beheren'
				},
				{
					id: 'zaak_edit',
					label: 'Mag zaken behandelen'
				},
				{
					id: 'zaak_read',
					label: 'Mag zaken raadplegen'
				}
			];
			
			return {
				require: [ 'zsOrgUnitRightsFormField', 'zsArrayModel' ],
				controller: [ function ( ) {
					
					var ctrl = {},
						arrayModel;
					
					ctrl.getRightTypes = function ( ) {
						return rightTypes;
					};
					
					ctrl.addRight = function ( ) {
						organisationalUnitService.getUnits().then(function ( ) {
							var right = {
									zaak_beheer: false,
									zaak_read: false,
									zaak_edit: false
								},
								orgUnitId = organisationalUnitService.getDefaultOrgUnitId(),
								roleId = organisationalUnitService.getDefaultRoleIdForOrgUnitId(orgUnitId);
								
							right.org_unit_id = orgUnitId;
							right.role_id = roleId;
								
							arrayModel.addObject(right);	
						});
					};
					
					ctrl.removeRight = function ( right ) {
						arrayModel.removeObject(right);
					};
					
					ctrl.setArrayModel = function ( model ) {
						arrayModel = model;
					};
					
					ctrl.handlePositionChange = function ( right, $orgUnitId, $roleId ) {
						right.org_unit_id = $orgUnitId;
						right.role_id = $roleId;
					};
					
					ctrl.getRights = function ( ) {
						return arrayModel.getList();
					};
					
					return ctrl;
					
				}],
				controllerAs: 'orgUnitRightsFormField',
				link: function ( scope, element, attrs, controllers ) {
					
					var zsOrgUnitRightsFormField = controllers[0],
						arrayModel = controllers[1];
						
					zsOrgUnitRightsFormField.setArrayModel(arrayModel);
					
				}
			};
			
		}]);
	
})();