/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.form')
		.controller('nl.mintlab.core.form.FormController', [ '$scope', '$parse', '$interpolate', '$timeout', '$q', 'smartHttp', 'formService', function ( $scope, $parse, $interpolate, $timeout, $q, smartHttp, formService ) {
			
			var forEach = _.forEach,
				difference = _.difference,
				promises = [],
				form;
				
			$scope.submitting = false;
			$scope.lastSaved = NaN;
				
			function init ( ) {
				var i,
					l,
					fields = $scope.zsForm.fields || [],
					field;
					
				setFieldData();
				
				$scope.$emit('form.prepare');
				
				function getFieldWatcher ( field ) {
					return function ( nwVal, oldVal/*, scope*/ ) {
						if(angular.isArray(nwVal) && difference(oldVal, nwVal).length === 0) {
							return;
						}
						$scope.$emit('form.change', field, nwVal, oldVal);
					};
				}
				
				for(i = 0, l = fields.length; i < l; ++i) {
					field = fields[i];
					$scope.$watch(field.name, getFieldWatcher(field));
				}
				
				$timeout(function ( ) {
					$scope.$emit('form.ready');
				});
				
			}
				
			function watch ( promise ) {
				var obj = {
					promise: promise
				};
				
				obj.unwatch = $scope.$watch(promise.watch, function ( /*nw, old*/ ) {
					if(!promise.when || $scope.$eval(promise.when)) {
						$parse(promise.then)($scope);
					}
				});
				
				promises.push(obj);
				
			}
			
			function interpret ( val ) {
				var match = val.toString().match(/^<\[(.*?)\]>$/);
				if(match) {
					return $parse(match[1])($scope);
				}
				
				return val.toString().replace(/<\[(.*?)\]>/, function ( match, expr ) {
					var parsed = $parse(expr)($scope);
					return parsed;
				});
			}
			
			function setFieldData ( ) {
				var fields = $scope.zsForm.fields || [],
					field,
					i,
					l,
					val,
					parsed;
					
				for(i = 0, l = fields.length; i < l; ++i) {
					field = fields[i];
					val = undefined;
					if(field.value !== undefined) {
						val = field.type === 'checkbox' ? !!field.value : field.value;
					} else {
						val = _.cloneDeep(field['default']);
					}
					if(val !== undefined) {
						parsed = !angular.isObject(val) && val !== null && typeof val !== 'boolean' && typeof val !== 'number' ? interpret(val) : val;
						form.setValue(field.name, parsed);
					} else {
						delete $scope[field.name];
					}
				}
			}
			
			function resolve ( field ) {
				var value = $scope[field.name];
				if(value !== null && value !== undefined && field.data && field.data.resolve) {
					switch(field.type) {
						default:
						value = resolveVal(value, field.data.resolve);
						break;
						
						case 'checkbox':
						value = typeof value === 'string' ? value === '1' : resolveVal(value, resolve);
						break;
						
						case 'file':
						case 'checkbox-list':
						value = resolveArray(value, field.data.resolve);
						break;
						
						case 'spot-enlighter':
						if(field.data.multi) {
							value = resolveArray(value, field.data.resolve);
						} else {
							value = resolveVal(value, field.data.resolve);
						}
						break;
					}
					
				}
				return value;
			}
			
			function resolveVal ( val, resolve ) {
				return resolve ? $parse(resolve)(val) : val;
			}
			
			function resolveArray ( val, resolve ) {
				var i,
					l,
					arr = [];
					
				if(!val || !angular.isArray(val)) {
					val = [];
				}
				
				for(i = 0, l = val.length; i < l; ++i) {
					arr.push(resolveVal(val[i], resolve));
				}
				return arr;
			}
			
			function getForm ( ) {
				return $scope[$scope.getName()];
			}
			
			function resetForm ( ) {
				var fields = $scope.zsForm.fields || [],
					field,
					i,
					l;
				
				for(i = 0, l = fields.length; i < l; ++i) {
					field = fields[i];
					if(field['default'] !== undefined) {
						$scope[field.name] = angular.copy(field['default']);
					} else {
						$scope[field.name] = undefined;
					}
				}
			}
			
			function submitForm ( action ) {
				var url = action.data && action.data.url ? $interpolate(action.data.url)($scope) : null,
					data = {},
					fields = $scope.zsForm.fields || [],
					method = ($scope.zsForm.options ? $scope.zsForm.options.method : null ) || 'POST',
					field,
					val,
					i,
					l,
					promise = null,
					deferred = null;
					
				$scope.submitting = true;
				
				for(i = 0, l = fields.length; i < l; ++i) {
					field = fields[i];
					if($scope.showField(field)) {
						val = resolve(field);
						if(val === undefined) {
							val = null;
						}
						data[field.name] = val;
					}
				}
				
				$scope.$emit('form.submit.attempt', data);
				
				if(url) {
					promise = smartHttp.connect( {
						url: url,
						method: method,
						data: data
					})
						.success(function onSuccess ( data ) {
							$scope.submitting = false;
							$scope.$emit('form.submit.success', action.name, data);
							
							if(action.data.success) {
								$parse(action.data.success)($scope);
							}
							
						})
						.error(function onError ( data ) {
							$scope.submitting = false;
							$scope.$emit('form.submit.error', action.name, data);
							validate(data);
							
							if(action.data.error) {
								$parse(action.data.error)($scope);
							}
							
						});
				} else {
					deferred = $q.defer();
					promise = deferred.promise;
					deferred.resolve();
				}
				
				return promise;
			}
			
			function validate ( data ) {
				var form = getForm(),
					statuses = data && data.result && data.result[0] ? data.result[0].data || [] : [],
					status,
					field,
					control,
					messages = {},
					validityType,
					i,
					l;
					
				if(!form) {
					return;
				}
					
				for(i = 0, l = statuses.length; i < l; ++i) {
					status = statuses[i];
					switch(status.result) {
						default:
						validityType = '';
						break;
						
						case 'missing':
						validityType = 'required';
						break;
						
						case 'invalid':
						field = $scope.zsForm.fields[i];
						if(field && field.data && field.data.pattern) {
							validityType = 'pattern';
						} else {
							validityType = 'required';
						}
						break;
					}
					control = form[status.parameter];
					if(control) {
						control.$setValidity(validityType, false);
					}
					messages[status.parameter] = status.message;
				}
				
				for(i = 0, l = $scope.zsForm.fields.length; i < l; ++i) {
					field = $scope.zsForm.fields[i];
					field.statusMessage = messages[field.name];
				}
			}
			
			function isEqual ( a, b ) {
				
				if(a && !b || !a && b){
					return false;
				}
				
				if(!_.isObject(a) || !_.isObject(b)) {
					return a === b;
				}
				
				for(var key in a) {
					if(key !== '$$hashKey' && !isEqual(a[key], b[key])) {
						return false;
					}
				}
				return true;
			}
			
			$scope.handleActionClick = function ( action, event ) {
				switch(action.type) {
					case 'submit':
					submitForm(action);
					break;
					
					case 'reset':
					resetForm();
					break;
				}
				
				if(action.click) {
					$parse(action.click)($scope, {
						$event: event,
						$values: $scope.getValues(),
						$unresolved: $scope.getValues(false)
					});
				}
			};
			
			$scope.isActionDisabled = function ( action ) {
				var form = getForm(),
					isDisabled,
					isDefined = (action.disabled !== undefined && action.disabled !== null);
					
				if(!form) {
					return;
				}
				
				if(isDefined) {
					isDisabled = $parse(action.disabled)($scope);
				} else if(action.type === 'submit') {
					isDisabled = !form.$valid;
				} else {
					isDisabled = false;
				}
				
				return isDisabled;
			};
			
			$scope.getFields = function ( fieldset ) {
				return _.filter(fieldset.fields, $scope.showField);
			};
			
			$scope.showField = function ( field ) {
				var when = field.when;
				return (when === undefined || when === null) || $parse(when)($scope);
			};
			
			$scope.revertField = function ( field ) {
				$scope[field.name] = _.cloneDeep(field['default']);
			};
			
			$scope.isDefaultValue = function ( field ) {
				var isDefaultValue = false,
					def = null,
					val = $scope[field.name];
					
				if(field['default']) {
					def = field['default'];
				}
				
				switch(field.type) {
					
					default:
					if(def === undefined) {
						def = null;
					}
					if(val === undefined) {
						val = null;
					}
					break;
					
					case 'checkbox':
					val = !!val;
					def = !!def;
					break;
					
					case 'spot-enlighter': 
					case 'checkbox-list':
					case 'file':
					case 'org-unit':
					if(!val) {
						val = [];
					}
					if(!def) {
						def = [];
					}
					break;
					
					case 'text':
					if(!val) {
						val = '';
					}
					if(!def) {
						def = '';
					}
					break;
				}
				
				if(_.isArray(val)) {
					isDefaultValue = _.isEqual(def, val, function ( a, b ) {
						return isEqual(a, b) && isEqual(b, a);
					});
				} else {
					isDefaultValue = _.isEqual(def, val);
				}
				
				
				return isDefaultValue;
			};
			
			$scope.isFieldEmpty = function ( field ) {
				var val = $scope[field.name],
					isEmpty;
					
				isEmpty = val !== 0 && (!val || (typeof val !== 'number' && _.isEmpty(val)));
				
				return isEmpty;
			};
			
			$scope.getRequired = function ( field ) {
				var required = field.required,
					isRequired;
					
				if(typeof required === 'boolean' || typeof required === 'number') {
					isRequired = !!required;
				} else {
					isRequired = $parse(required)($scope);
				}
				return isRequired;
			};
			
			$scope.getFieldId = function ( field ) {
				return field.name;
			};
			
			$scope.getPlaceholder = function ( field ) {
				var placeholder = field.data ? field.data.placeholder : undefined;
				return placeholder !== undefined ? $parse(placeholder)($scope) : '';
			};
			
			$scope.getName = function ( ) {
				return $scope.zsForm.name;
			};
			
			$scope.getOptions = (function ( ) {
				var prev;
					
				function getResult ( field, allowEmpty ) {
					var options = angular.isArray(field.data.options) ? field.data.options.concat() : field.data.options,
						interpolated;
						
					allowEmpty = !!allowEmpty;
						
					if(typeof options === 'string') {
						try {
							options = interpret(options);
							interpolated = true;
						} catch ( error ) {
							options = [];
						}
					}
					
					if(!allowEmpty && !interpolated) {
						// causes a recursive loop if interpolated (??)
						field._empty = _.find(options, function ( option ) {
							return (option.value === '' || option.value === undefined || option.value === null);
						});
						if(field._empty) {
							options.splice(_.indexOf(options, field._empty), 1);
						}
					} else {
						field._empty = null;
					}
					
					return options;
					
				}
				
				return function ( field, allowEmpty ) {
					var result = getResult(field, allowEmpty);
					if(!angular.equals(prev, result)) {
						prev = result;
					}
					return prev;
				};
			})();
			
			$scope.getRestrict = function ( field ) {
				var restrict = field.data ? field.data.restrict : '';
				if(restrict) {
					restrict = $interpolate(restrict)($scope);
				}
				
				return restrict;
			};
			
			$scope.isFormValid = function ( ) {
				return getForm() ? getForm().$valid : false;
			};
			
			$scope.isValid = function ( field ) {
				var form = getForm(),
					control = form ? form[field.name] : null;
				
				return control && control.$valid;
			};
			
			$scope.isEmpty = function ( field ) {
				var form = getForm(),
					control = form ? form[field.name] : null;
					
				return control && control.$isEmpty($scope[field.name]);
			};
			
			$scope.isPristine = function ( field ) {
				var form = getForm(),
					control = form ? form[field.name] : null;
				
				return control && control.$pristine;	
			};
			
			$scope.getValue = function ( name ) {
				return form.getValue(name);
			};
			
			$scope.setValue = function ( name, value ) {
				form.setValue(name, value);
			};
			
			$scope.getValues = function ( resolveValues ) {
				var fields = $scope.zsForm.fields || [],
					field,
					val,
					vals = {},
					i,
					l;
					
				resolveValues = resolveValues === undefined ? true : !!resolveValues;
					
				for(i = 0, l = fields.length; i < l; ++i) {
					field = fields[i];
					if($scope.showField(field)) {
						val = resolveValues ? resolve(field) : $scope[field.name];
						if(val === undefined) {
							val = null;
						}
						vals[field.name] = val;
					}
				}
				
				return vals;
			};
			
			$scope.getTrueValue = function ( field ) {
				return field.data && field.data.trueValue !== undefined ? interpret(field.data.trueValue) : true;
			};
			
			$scope.getFalseValue = function ( field ) {
				return field.data && field.data.falseValue !== undefined ? interpret(field.data.falseValue) : false;
			};
			
			$scope.isVisible = function ( action ) {
				var when = action.when;
				return (when === null || when === undefined) || $parse(when)($scope);
			};
			
			$scope.hasVisibleActions = function ( actions ) {
				var i,
					l;
					
				for(i = 0, l = actions ? actions.length : 0; i < l; ++i) {
					if($scope.isVisible(actions[i])) {
						return true;
					}
				}
				return false;
			};
			
			$scope.getForm = function ( ) {
				return getForm();
			};
			
			$scope.resetForm = function ( ) {
				resetForm();
			};
			
			$scope.submitForm = function ( ) {
				var actions = $scope.zsForm.actions || [],
					action,
					i,
					l;
					
				for(i = 0, l = actions.length; i < l; ++i) {
					action = actions[i];
					if(action.type === 'submit') {
						submitForm(action);
					}
				}
			};
			
			form = {
				getValue: function ( name ) {
					return $scope[name];
				},
				setValue: function ( name, value) {
					$scope[name] = value;
				},
				getName: function ( ) {
					return $scope.getName();
				},
				getValues: function ( resolveValues ) {
					return $scope.getValues(resolveValues);
				},
				getFields: function ( ) {
					return $scope.zsForm.fields;
				},
				isDefaultValue: function ( field ) {
					return $scope.isDefaultValue(field);	
				},
				reset: function ( ) {
					$scope.resetForm();
				},
				isFormValid: function ( ) {
					return $scope.isFormValid();
				},
				scope: $scope
			};
			
			formService.register(form);
			
			// we can't use a dynamic ngModel value because
			// it's not interpolated (just parsed), so we
			// expose the scope object
			// see: https://github.com/angular/angular.js/issues/1404
			$scope.scope = $scope;
			
			$scope.$watch('zsForm.promises', function ( nw/*, old*/ ) {
				if(nw && nw.length) {
					forEach(nw, function ( value ) {
						watch(value);
					});
				}
				
			});
			
			$scope.$on('$destroy', function ( ) {
				if(form) {
					formService.unregister(form);
				}
			});
			
			$scope.$on('form.change.committed', function ( ) {
				var autosave = $scope.zsForm.options ? $scope.zsForm.options.autosave : false,
					actions = $scope.zsForm.actions || [],
					action,
					i;
					
				function onSubmitSuccess ( /*response*/ ) {
					$scope.lastSaved = new Date().getTime();
				}
				
				if(autosave && (autosave !== 'valid' || getForm().$valid)) {
					for(i = actions.length-1; i >= 0; --i) {
						action = actions[i];
						if(action.type === 'submit') {
							submitForm(action).then(onSubmitSuccess);
						}
					}
				}
			});
			
			init();
			
		}]);
	
})();
