/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.form')
		.directive('zsCheckboxList', [ '$parse', '$timeout', 'translationService', function ( $parse, $timeout, translationService ) {
			
			return {
				scope: true,
				require: 'ngModel',
				link: function ( scope, element, attrs, ngModel ) {
					
					scope.zsExpanded = false;
					scope.zsNumVisibleOptions = 0;
					
					var setOptions = [];
					
					function setChildValues ( val ) {
						var options = $parse(attrs.zsCheckboxListOptions)(scope) || [],
							i,
							l,
							option,
							toRemove = setOptions.concat(),
							index,
							name;
							
						if(val === undefined) {
							val = ngModel.$viewValue || [];
						}
						
						setOptions.length = 0;
						
						for(i = 0, l = options.length; i < l; ++i) {
							option = options[i];
							name = option.value;
							index = _.indexOf(toRemove, name);
							if(index !== -1) {
								toRemove.splice(index, 1);
							}
							setOptions.push(name);
							scope[name] = _.indexOf(val, name) !== -1;
						}
						
						while(toRemove.length) {
							name = toRemove.shift();
							delete scope[name];
						}
					}
					
					function getMaxVisible ( ) {
						return attrs.zsCheckboxListMaxVisible ? parseInt(attrs.zsCheckboxListMaxVisible, 10) : 5;
					}
					
					scope.getVisibleOptions = function ( options ) {
						var visible = [],
							limit = getMaxVisible(),
							isVisible,
							option,
							expanded = scope.zsExpanded,
							i,
							l;
							
						if(expanded) {
							return options;
						}
						
						for(i = 0, l = options ? options.length : 0; i < l; ++i) {
							option = options[i];
							isVisible = option.always_visible || scope[option.value];
							if(isVisible) {
								visible.push(option);
							}
						}
						
						for(i = 0, l = options ? options.length : 0; i < l; ++i) {
							option = options[i];
							if(visible.length < limit && _.indexOf(visible, option) === -1) {
								visible.push(option);
							}
						}
						
						scope.zsNumVisibleOptions = visible.length;
						
						return _.filter(options, function ( option ) {
							return _.indexOf(visible, option) !== -1;
						});
					};
					
					scope.toggleExpanded = function ( ) {
						scope.zsExpanded = !scope.zsExpanded;
					};
					
					scope.toggleOption = function ( name ) {
						var val = ngModel.$viewValue ? ngModel.$viewValue.concat() : [],
							index = _.indexOf(val, name);
							
						if(index === -1) {
							val.push(name);
							scope[name] = true;
						} else {
							val.splice(index, 1);
							scope[name] = false;
						}
						
						ngModel.$setViewValue(val);
					};
					
					scope.hasInvisibleOptions = function ( options ) {
						var minVisibleOptions;
							
						minVisibleOptions = _.reduce(options, function ( sum, option ) {
							return sum += option.always_visible || scope[option.value];
						}, 0);
						
						return minVisibleOptions < options.length && options.length > getMaxVisible();
					};
					
					scope.getLessOptionsLabel = function ( ) {
						return attrs.zsCheckboxListLessLabel || translationService.get('Minder opties');
					};
					
					scope.getMoreOptionsLabel = function ( ) {
						return attrs.zsCheckboxListMoreLabel || translationService.get('Meer opties');
					};
					
					scope.scope = scope;
					
					ngModel.$viewChangeListeners.push(function ( ) {
						setChildValues();
					});
					
					ngModel.$formatters.push(function ( val ) {
						setChildValues(val);
						return val;
					});
					
					ngModel.$isEmpty = function ( val ) {
						return !val || val.length === 0;	
					};
					
					$timeout(function ( ) {
						setChildValues();
					});
				}
			};
			
		}]);
	
})();