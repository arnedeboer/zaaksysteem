/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.form')
		.directive('name', [ '$interpolate', function ( $interpolate ) {
			
			return {
				restrict: 'A',
				priority: 1000,
				controller: [ '$scope', '$element', '$attrs', function ( $scope, $element, $attrs ) {
					
					if($element[0].tagName && $element[0].tagName.toLowerCase() !== 'comment') {
						$attrs.name = $interpolate($attrs.name)($scope);
						$element.attr('name', $attrs.name);	
					}
					
				}]
			};
			
		}]);
	
})();