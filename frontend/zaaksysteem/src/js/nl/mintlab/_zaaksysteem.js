/*global angular,window,$,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem', [
		'ngRoute',
		'ngAnimate',
		'LocalStorageModule',
		'MintJS',
		'Zaaksysteem.admin',
		'Zaaksysteem.auth',
		'Zaaksysteem.case',
		'Zaaksysteem.core',
		'Zaaksysteem.docs',
		'Zaaksysteem.form',
		'Zaaksysteem.kcc',
		'Zaaksysteem.menu',
		'Zaaksysteem.message',
		'Zaaksysteem.net',
		'Zaaksysteem.notification',
		'Zaaksysteem.object',
		'Zaaksysteem.pip',
		'Zaaksysteem.subject',
		'Zaaksysteem.sysin',
		'Zaaksysteem.timeline',
		'Zaaksysteem.user',
		'Zaaksysteem.widget'
	])
		.config([ '$httpProvider', '$interpolateProvider', 'smartHttpProvider', '$anchorScrollProvider', 'localStorageServiceProvider', function ( $httpProvider, $interpolateProvider, smartHttpProvider, $anchorScrollProvider, localStorageServiceProvider ) {
					
			$interpolateProvider.startSymbol('<[');
			$interpolateProvider.endSymbol(']>');
			
			$httpProvider.defaults.useXDomain = true;
			$httpProvider.defaults.withCredentials = true;
			
			$anchorScrollProvider.disableAutoScrolling();
			
			var prefix = '/',
				loc = window.location.href;
				
			if(loc.match(/^http(s):\/\/localhost/)) {
				prefix = 'http://sprint.zaaksysteem.nl/';
			}
			
			smartHttpProvider.defaults.prefix = prefix;
			
			localStorageServiceProvider.setPrefix('zaaksysteem');
			
			$httpProvider.interceptors.push(function ( ) {
				
				return {
					'request': function ( config ) {
						
						var key,
							params,
							val;
						
						if(config.method && config.method.toUpperCase() === 'GET' && config.params) {
							params = config.params;
							for(key in params) {
								val = params[key];
								if(val === true || val === false) {
									params[key] = val ? 1 : 0;
								}
							}
						}
						
						return config;
					}
				};
				
			});
			
		}])
		.run([ '$rootScope', '$compile', '$cookies', '$document', '$window', 'systemMessageService', function ( $rootScope, $compile, $cookies, $document, $window, systemMessageService ) {
			
			var safeApply = fetch('nl.mintlab.utils.safeApply');
			
			$rootScope.$on('legacyDomLoad', function ( event, jq ) {
				safeApply($rootScope, function ( ) {
					var i,
						l,
						el,
						scope;
					
					for(i = 0, l = jq.length; i < l; ++i) {
						el = jq[i];
						scope = angular.element(el).scope();
						$compile(el)(scope);
					}	
				});
				
			});
			
			// FIXME(dario): this is very unclean, perhaps a define/fetch() method?
			window.getXSRFToken = function ( ) {
				return $cookies['XSRF-TOKEN'];
			};

			if($cookies['XSRF-TOKEN']) {
				$.ajaxSetup({
					headers: { 'X-XSRF-TOKEN': $cookies['XSRF-TOKEN'] }
				});
			}
			
			$($document[0]).ajaxSend(function ( event, xhr/*, options*/ ) {
				var accessToken = $cookies['XSRF-TOKEN'];
				xhr.setRequestHeader('X-XSRF-TOKEN', accessToken);
			});
			
			// show popup for legacy browsers
			// edited from http://stackoverflow.com/questions/10964966/detect-ie-version-in-javascript
			var version = (function ( ) {
				
				var myNav = $window.navigator.userAgent.toLowerCase(),
					isIE = (myNav.indexOf('msie') !== -1),
					version = isIE ? parseInt(myNav.split('msie')[1]) : NaN;
					
				return version;
			})();
		   
			if(version && version < 9) {
				// systemMessageService.emit('info', 'U maakt gebruik van een verouderde versie van uw browser en loopt een beveiligingsrisico. Wij adviseren u uw browser bij te werken of een recente versie te downloaden van Firefox of Chrome.');
			}			
			
			
		}]);
})();
