/*global angular,fetch,_*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsSelectableList', [ '$document', '$parse', function ( $document, $parse ) {
			
			var indexOf = fetch('nl.mintlab.utils.shims.indexOf'),
				cancelEvent = fetch('nl.mintlab.utils.events.cancelEvent'),
				difference = _.difference,
				forEach = _.forEach,
				doc = $document[0];
			
			return {
				compile: function ( /*tElement, tAttrs, transclude*/ ) {
					
					return function link ( scope, element, attrs ) {
						
						var selection = [],
							selectables = [],
							firstSelected,
							mode = attrs.zsSelectableListMode || 'all';
							
						if(scope.$eval(attrs.zsSelectableList) === false || (mode === 'none')) {
							return;
						}
						
						function addSelectable ( el ) {
							selectables.push(el[0]);
							el.attr('tabindex', '0');
							el.bind('click', onClick);
							el.bind('contextmenu', onClick);
						}
						
						function removeSelectable ( el ) {
							selectables.splice(indexOf(selectables, el[0]), 1);
							el.removeAttr('tabindex');
							el.unbind('click', onClick);
							el.unbind('contextmenu', onClick);
						}
						
						function onKeyUp ( event ) {
							var keyCode = event.keyCode,
								shiftKey = event.shiftKey,
								ctrlKey = event.ctrlKey,
								list = element[0].querySelectorAll('[data-zs-selectable]'),
								index = indexOf(list, doc.activeElement);
								
							if(index === -1) {
								return;
							}
							
							switch(keyCode) {
								case 38:
								if(shiftKey) {
									index = Math.max(0, index-1);
									if(list[index]) {
										selectUpto(list[index]);
									}
								} else {
									index--;
									if(index < 0) {
										index = list.length-1;
									}
									if(ctrlKey) {
										list[index].focus();
									} else {
										selectSingle(list[index]);
									}
								}
								break;
								
								case 40:
								if(shiftKey) {
									index = Math.min(list.length-1, index + 1);
									if(list[index]) {
										selectUpto(list[index]);
									}
								} else {
									index++;
									if(index === list.length) {
										index = 0;
									}
									if(ctrlKey) {
										list[index].focus();
									} else {
										selectSingle(list[index]);
									}
								}
								break;
								
								case 32:
								toggle(doc.activeElement);
								break;
								
								case 27:
								selectNone();
								break;
							}
							
							
						}
						
						function onKeyDown ( event ) {
							var keyCode = event.keyCode,
								index = indexOf(selectables, doc.activeElement);
								
							if(index !== -1 && (keyCode === 38 || keyCode === 40 || keyCode === 32)) {
								return cancelEvent(event, true);
							}
						}
						
						function onClick ( event ) {
							var el = event.target,
								shiftKey = event.shiftKey,
								ctrlKey = event.ctrlKey,
								tagName = el.tagName.toLowerCase(),
								currentTarget = event.currentTarget,
								p;
								
							if(tagName === 'a') {
								return;
							}
							
							if(!currentTarget) { // IE8
								p = angular.element(el);
								while(p.length) {
									if(p.attr('data-zs-selectable') !== undefined) {
										currentTarget = p[0];
										break;
									}
									p = p.parent();
								}
							}
								
							if(event.type === 'contextmenu') {
								if(!isSelected(currentTarget)) {
									selectSingle(currentTarget);
								}
							} else if(!shiftKey || mode === 'single') {
								if(ctrlKey || 
									(tagName === 'input' && el.getAttribute('type') === 'checkbox') ||
									(selection.length === 1 && selection[0] === getChildData(currentTarget))
								) {
									toggle(currentTarget);
								} else {
									selectSingle(currentTarget);
								}
							} else {
								selectUpto(currentTarget);
							}
						}
						
						function selectSingle ( selectable ) {
							firstSelected = selectable;
							selectable.focus();
							emitSelect([ getChildData(selectable) ]);
						}
						
						function selectUpto ( selectable ) {
							var list = element[0].querySelectorAll('[data-zs-selectable]'),
								indexFrom,
								indexTo,
								from,
								to,
								toEmit = [];
								
							if(!firstSelected || firstSelected === selectable) {
								selectSingle(selectable);
							} else {
								indexFrom = indexOf(list, firstSelected);
								indexTo = indexOf(list, selectable);
								
								if(indexFrom > indexTo) {
									from = indexTo;
									to = indexFrom;
								} else {
									from = indexFrom;
									to = indexTo;
								}
								
								selectable.focus();
								forEach(Array.prototype.slice.call(list, from,to+1), function ( value/*, key*/ ) {
									var data = getChildData(value);
									toEmit.push(data);
								});
								
								emitSelect(toEmit);
							}
							
						}
						
						function toggle ( selectable ) {
							var data = getChildData(selectable),
								index = indexOf(selection, data),
								toEmit = selection.concat();
							
							if(index === -1) {
								toEmit.push(data);
							} else {
								toEmit.splice(index, 1);
							}
							emitSelect(toEmit);
						}
						
						function selectNone ( ) {
							firstSelected = null;
							emitSelect([]);
						}
						
						function emitSelect ( list ) {
							var toAdd,
								toRemove;
							
							toAdd = difference(list, selection);
							toRemove = difference(selection, list);
							
							selection.length = 0;
							selection.push.apply(selection, list);
							
							scope.$emit('zsSelectableList:change', selection, toAdd, toRemove);
						}
						
						function getChildData ( child ) {
							var data;
							
							child = angular.element(child);
							data = $parse(child.attr('data-zs-select-data'))(child.scope());
							
							return data;
						}
						
						function findSelectables ( ) {
							var children = element[0].querySelectorAll('[data-zs-selectable]'),
								el,
								i,
								l,
								toRemove = [],
								toAdd = [];
								
							for(i = 0, l = selectables.length; i < l; i++) {
								el = selectables[i];
								if(indexOf(selectables, children[i]) === -1) {
									toRemove.push(el);
								} 
							}
							
							for(i = 0, l = children.length; i < l; ++i) {
								el = children[i];
								if(el.nodeType === 1 && _.indexOf(selectables, el) === -1) {
									toAdd.push(el);
								}
							}
							
							forEach(toRemove, function ( value/*, key*/ ) {
								removeSelectable(angular.element(value));
							});
							
							forEach(toAdd, function ( value/*, key*/ ) {
								addSelectable(angular.element(value));
							});
							
						}
						
						function isSelected ( selectable ) {
							var data = getChildData(selectable),
								index = indexOf(selection, data);
								
							return index !== -1;
						}
						
						element.bind('keyup', onKeyUp);
						element.bind('keydown', onKeyDown);
						
						scope.$watch(function ( ) {
							findSelectables();
						});
						
					};
				}
			};
			
		}]);
	
})();