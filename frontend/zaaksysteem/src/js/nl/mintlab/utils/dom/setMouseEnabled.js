/*global define,fetch,window,document*/
(function ( ) {
	
	define('nl.mintlab.utils.dom.setMouseEnabled', function ( ) {
		
		// from https://github.com/ausi/Feature-detection-technique-for-pointer-events/
		var	doc = document,
			body = doc.body,
			element = doc.createElement('x'),
			documentElement = doc.documentElement,
			getComputedStyle = window.getComputedStyle,
			supports;
			
		if(!('pointerEvents' in element.style)){
			supports = false;
		} else {
			element.style.pointerEvents = 'auto';
			element.style.pointerEvents = 'x';
			documentElement.appendChild(element);
			supports = getComputedStyle && getComputedStyle(element, '').pointerEvents === 'auto';
			documentElement.removeChild(element);
		}
		
		if(supports) {
			return function ( target, isEnabled ) {
				target.style['pointer-events'] = isEnabled ? 'auto' : 'none';
			};
		} else {
			
			return (function ( ) {
				
				var indexOf = fetch('nl.mintlab.utils.shims.indexOf'),
					addEventListener = fetch('nl.mintlab.utils.events.addEventListener'),
					removeEventListener = fetch('nl.mintlab.utils.events.removeEventListener'),
					dispatchEvent = fetch('nl.mintlab.utils.events.dispatchEvent'),
					fromGlobalToLocal = fetch('nl.mintlab.utils.dom.fromGlobalToLocal'),
					getMousePosition = fetch('nl.mintlab.utils.dom.getMousePosition'),
					getElementFromPoint = document.elementFromPoint,
					listeningTo = [];
				
				function listen ( target ) {
					var index = indexOf(listeningTo, target);
					if(index === -1) {
						listeningTo.push(target);
						addEventListener(target, 'click', onTargetMouseEvent);
						addEventListener(target, 'mouseover', onTargetMouseEvent);
						addEventListener(target, 'mouseout', onTargetMouseEvent);
					}
				}
				
				function unlisten ( target ) {
					var index = indexOf(listeningTo, target);
					if(index !== -1) {
						listeningTo.splice(index, 1);
						removeEventListener(target, 'click', onTargetMouseEvent);
						removeEventListener(target, 'mouseover', onTargetMouseEvent);
						removeEventListener(target, 'mouseout', onTargetMouseEvent);
					}
				}
				
				function onTargetMouseEvent ( event ) {
					
					var target = event.currentTarget,
						pos = fromGlobalToLocal(body, getMousePosition(event)),
						clonedEvent = doc.createEvent('MouseEvents'),
						hidden = [],
						el;
					
					/*TODO(dario): properly init mouse event*/
					clonedEvent.initMouseEvent(event.type);
					
					for(var key in event) {
						clonedEvent[key] = event[key];
					}
					
					while(indexOf(listeningTo, target) !== -1) {
						hidden.push( { element: target, display: target.style.display });
						target.style.display = 'none';
						target = getElementFromPoint(pos);
					}
					
					if(target) {
						dispatchEvent(target, clonedEvent);
					}
					
					for(var i = 0, l = hidden.length; i < l; ++i) {
						el = hidden[i].element;
						el.style.display = hidden[i].display;
					}
					
				}
				
				return function ( target, isEnabled ) {
					if(isEnabled) {
						listen(target);
					} else {
						unlisten(target);
					}
				};
				
			})();
		}
	});
	
})();