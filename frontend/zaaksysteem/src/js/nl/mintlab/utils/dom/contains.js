/*global define*/
(function ( ) {
	
	define('nl.mintlab.utils.dom.contains', function ( ) {
		
		return function ( parent, element ) {
			while(element) {
				element = element.parentNode;
				if(element === parent) {
					return true;
				}
			}
			return false;
		};
	});
	
})();