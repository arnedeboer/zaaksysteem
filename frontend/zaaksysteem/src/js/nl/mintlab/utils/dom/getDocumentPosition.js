/*global define,fetch,window,document*/
(function ( ) {
	
	window.zsDefine('nl.mintlab.utils.dom.getDocumentPosition', function ( ) {
		
		var win = window,
			doc = document,
			docEl = document.documentElement,
			body = document.body,
			scrollX,
			scrollY;
		
		if("pageXOffset" in win) {
			scrollX = function ( ) {
				return win.pageXOffset;
			};
			scrollY = function ( ) {
				return win.pageYOffset;
			};
		} else if(doc.doctype ) {
			scrollX = function ( ) {
				return body.scrollLeft;
			};
			scrollY = function ( ) {
				return body.scrollTop;
			};
		} else {
			scrollX = function ( ) {
				return docEl.scrollLeft;
			};
			scrollY = function ( ) {
				return docEl.scrollTop;
			};
		}
		
		var getViewportPosition = window.zsFetch('nl.mintlab.utils.dom.getViewportPosition');
		
		return function ( element ) {
			var offset = getViewportPosition(element);
			return { x: offset.x, y: offset.y };
		};
	});
})();
