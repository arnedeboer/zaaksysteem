/*global define,document,fetch*/
(function ( ) {
	
	window.zsDefine('nl.mintlab.utils.dom.getMousePosition', function ( ) {
		
		var body = document.body,
			fromLocalToGlobal = window.zsFetch('nl.mintlab.utils.dom.fromLocalToGlobal');
		
		return function ( event ) {
			var x,
				y,
				pos;
			
			if(event.pageX !== undefined) {
				pos = fromLocalToGlobal(body, { x: event.pageX, y: event.pageY } );
				x = pos.x;
				y = pos.y;
			} else if(event.clientX) {
				x = event.clientX;
				y = event.clientY;
			}
			
			return { x: x, y: y };
		};
	});
	
})();
