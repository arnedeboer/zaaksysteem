/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.filters')
		.filter('capitalize', function ( ) {
			return function ( from ) {
				return from ? from [0].toUpperCase() + from.substr(1) : '';
			};
		});
})();
