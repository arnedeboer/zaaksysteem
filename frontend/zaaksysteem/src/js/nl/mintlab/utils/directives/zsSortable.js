/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsSortable', [ '$parse', function ( $parse ) {
			
			return {
				require: [ '^zsSort', 'ngDraggable' ],
				link: function ( scope, element, attrs, controllers ) {
					
					var data = $parse(attrs.zsSortable)(scope),	
						zsSort = controllers[0],
						ngDraggable = controllers[1],
						isEnabled;
						
					scope.$on('$destroy', function ( ) {
						if(zsSort.getTrackedElement() === element) {
							zsSort.untrack(element);
						}
						if(isEnabled) {
							zsSort.unregister(element, data);
						}
					});
					
					scope.$watch(function ( ) {
						var shouldBeEnabled = ngDraggable.isEnabled();
						if(shouldBeEnabled !== isEnabled) {
							isEnabled = shouldBeEnabled;
							if(isEnabled) {
								zsSort.register(element, data);
							} else {
								zsSort.unregister(element, data);
							}
						}
					});
					
					scope.$on('startdrag', function ( ) {
						zsSort.track(element);
					});
					
					scope.$on('stopdrag', function ( ) {
						zsSort.untrack(element);
					});
				}
			};
			
		}]);
	
})();