/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsAutogrow', [ function ( ) {
			
			return {
				link: function ( scope, element, attrs ) {
					
					var minRows = attrs.zsAutogrow || 1;
					
					element.attr('rows', minRows);
					element.css('height', 'auto');
					
					element.bind('change keypress paste focus textInput input', function ( ) {
						setSize();
					});
					
					function setSize ( ) {
						
						var el = element[0],
							rows = minRows;
						
						element.attr('rows', rows);
						
						while(el.scrollHeight > el.clientHeight) {
							element.attr('rows', rows++);
						}
						
					}
					
				}
			};
		}]);
	
})();