/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('ngDroppable', [ '$document', 'dropManager', function ( $document, dropManager ) {
			
			var element = $document[0].createElement('div'),
				hasPartialDragNDropSupport,
				hasFullDragNDropSupport,
				defaultMimetype = 'text/json',
				indexOf = fetch('nl.mintlab.utils.shims.indexOf'),
				link;
				
			hasFullDragNDropSupport = 'draggable' in element;
			hasPartialDragNDropSupport = !hasFullDragNDropSupport && !!element.dragDrop;
			
			if(hasFullDragNDropSupport) {
				link = function ( scope, element, attrs ) {
					
					var _enabled = false;
											
					function evaluateDirective ( ) {
						var shouldBeEnabled = attrs.ngDroppable !== '' ? scope.$eval(attrs.ngDroppable) : true;
						
						if(shouldBeEnabled === undefined) {
							shouldBeEnabled = true;
						}
						
						if(shouldBeEnabled !== _enabled) {
							if(shouldBeEnabled) {
								enable();
							} else {
								disable();
							}
						}
					}
					
					function enable ( ) {
						_enabled = true;
						element.bind('dragenter', onDragEnter);
						element.bind('dragover', onDragOver);
						element.bind('dragleave', onDragLeave);
						element.bind('drop', onDrop);
					}
					
					function disable ( ) {
						_enabled = false;
						element.unbind('dragenter', onDragEnter);
						element.unbind('dragover', onDragOver);
						element.unbind('dragleave', onDragLeave);
						element.unbind('drop', onDrop);
					}
					
					function onDragEnter ( event ) {
						if(isValidDrag(event)) {
							enterDragMode(event);
							return false;
						}
					}
					
					function onDragOver ( event ) {
						if(isValidDrag(event)) {
							enterDragMode(event);
							return false;
						}
					}
					
					function onDragLeave ( event ) {
						exitDragMode(event);
					}
					
					function enterDragMode ( event ) {
						
						event.preventDefault();
						event.stopPropagation();
						
						element.addClass('drag-over');
					}
					
					function exitDragMode ( /*event*/ ) {
						element.removeClass('drag-over');
					}
					
					function isValidDrag ( event ) {
						
						var mimetype = attrs.ngDropMimetype || defaultMimetype,
							dataTransfer = event.dataTransfer,
							isValidMimetype = false,
							drop = dropManager.getCurrentDrop();
						
						if(drop) {
							isValidMimetype = drop.mimetype === mimetype;
						} else {
							if(hasFullDragNDropSupport) {
								if(angular.isArray(dataTransfer.types)) {
									isValidMimetype = indexOf(dataTransfer.types, mimetype) !== -1;
								} else if('contains' in dataTransfer.types) {
									isValidMimetype = dataTransfer.types.contains(mimetype);
								}
							}	
						}
						
						return isValidMimetype;
					}
					
					function onDrop ( event ) {
						var mimetype = attrs.ngDropMimetype || defaultMimetype,
							data;
							
						if(!isValidDrag(event)) {
							return;
						}
														
						if(mimetype === 'Files') {
							data = event.dataTransfer.files;
						} else {
							data = dropManager.getCurrentDrop().data;
						}
						
						scope.$emit('drop', data, mimetype);
						
						exitDragMode();
						
						event.preventDefault();
						event.stopPropagation();
					}
					
					scope.$watch('ngDroppable', function ( ) {
						evaluateDirective();
					});
				};
			} else {
				link = function ( scope, element, attrs ) {
					
					var _enabled;
					
					function evaluateDirective ( ) {
						var shouldBeEnabled = attrs.ngDroppable !== '' ? scope.$eval(attrs.ngDroppable) : true;
						if(shouldBeEnabled === undefined) {
							shouldBeEnabled = true;
						}
						
						if(shouldBeEnabled !== _enabled) {
							if(shouldBeEnabled) {
								enable();
							} else {
								disable();
							}
						}
					}
					
					function enable ( ) {
						_enabled = true;
						dropManager.register(element);
					}
					
					function disable ( ) {
						_enabled = false;
						dropManager.unregister(element);
					}
					
					scope.performDrop = function ( data, mimetype ) {
						scope.$emit('drop', data, mimetype);
					};
					
					scope.$watch('ngDroppable', function ( ) {
						evaluateDirective();
					});
					
					scope.$on('$destroy', function ( ) {
						disable();
					});
				};
			}
			
			return link;
			
			
		}]);
	
})();