/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsDateRangePicker', [ 'dateFilter', function ( dateFilter ) {
			
			return {
				scope: {
					'from': '@zsDateRangePickerFrom',
					'to': '@zsDateRangePickerTo',
					'min': '@zsDateRangePickerMin',
					'max': '@zsDateRangePickerMax'
				},
				templateUrl: '/html/directives/date/date-picker.html',
				link: function ( scope/*, element, attrs*/ ) {
					
					var firstClick = null,
						availableYears = [],
						start = 1970,
						end = new Date().getFullYear() + 15;
					
					function setNow ( now ) {
						if(isNaN(now)) {
							now = new Date().getTime();
						}
						scope.now = now;
						scope.year = new Date(now).getFullYear();
					}
						
					availableYears = _.range(start, end);
					
					scope.min = Number.MIN_VALUE;
					scope.max = Number.MAX_VALUE;
					
					scope.from = NaN;
					scope.to = NaN;
					
					setNow(new Date().getTime());
					
					scope.prevMonth = function ( ) {
						var date = new Date(scope.now),
							prev = new Date(date.getFullYear(), date.getMonth()-1, 1);
							
						setNow(prev.getTime());
					};
					
					scope.nextMonth = function ( ) {
						var date = new Date(scope.now),
							next = new Date(date.getFullYear(), date.getMonth()+1, 1);
							
						setNow(next.getTime());	
					};
					
					scope.hasPrevMonth = function ( ) {
						var date = new Date(scope.now),
							prev = new Date(date.getFullYear(), date.getMonth(), 1);
							
						prev = new Date(prev.getTime()-1);
						
						return prev.getTime() >= scope.min;
					};
					
					scope.hasNextMonth = function ( ) {
						var date = new Date(scope.now),
							next = new Date(date.getFullYear(), date.getMonth()+1, 1);
							
						return next.getTime() <= scope.max;
					};
					
					scope.getMonthLabel = function ( ) {
						return dateFilter(scope.now, 'MMMM');
					};
					
					scope.getAvailableYears = function ( ) {
						return availableYears;
					};
					
					scope.handleYearChange = function ( ) {
						var now = new Date(scope.now),
							date = new Date(scope.year, now.getMonth(), now.getDate());
							
						date.setFullYear(scope.year);
						
						if(scope.year) {
							setNow(date.getTime());
						}
					};
					
					scope.isSelectedYear = function ( year ) {
						var now = new Date(scope.now),
							isSelected = now.getFullYear() === year;
							
						return isSelected;
					};
					
					scope.getWeeks = function ( ) {
						var date = new Date(scope.now),
							start = new Date(date.getFullYear(), date.getMonth(), 1),
							end = new Date(date.getFullYear(), date.getMonth()+1, 1),
							startDay = start.getDay() - 1,
							endDate = new Date(end.getTime()-1).getDate(),
							numWeeks = Math.ceil((startDay + endDate)/7),
							weeks = [],
							i,
							l;
							
						if(startDay < 1) {
							weeks.push(-1);
						}
							
						for(i = 0, l = numWeeks; i < l; ++i) {
							weeks.push(i);
						}
							
						return weeks;
					};
					
					scope.getWeekdayLabel = function ( day ) {
						var date = new Date(),
							str;

						date = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + day + 1);
						str = dateFilter(date.getTime(), 'EEE');
						
						return str;
					};
					
					scope.getDateObj = function ( week, day ) {
						var cur = new Date(scope.now),
							start = new Date(cur.getFullYear(), cur.getMonth(), 1),
							startDay = start.getDay() - 1,
							date = week * 7 + (day+1) - startDay;
							
						return new Date(start.getFullYear(), start.getMonth(), date);
					};
					
					scope.getDate = function ( week, day ) {
						return scope.getDateObj(week, day).getDate();	
					};
					
					scope.isInRange = function ( week, day ) {
						var dateObj = scope.getDateObj(week, day),
							startOfDay = dateObj.getTime(),
							endOfDay = new Date(dateObj.getFullYear(), dateObj.getMonth(), dateObj.getDate() + 1).getTime();
						
						return endOfDay > scope.min && startOfDay <= scope.max;
					};
					
					scope.selectDay = function ( week, day ) {
						var dateObj = scope.getDateObj(week, day),
							tmp,
							from,
							to;
							
						if(firstClick) {
							if(firstClick.getTime() > dateObj.getTime()) {
								tmp = dateObj;
								dateObj = firstClick;
								firstClick = tmp;
							} 
							from = firstClick.getTime();
							firstClick = null;
						} else {
							from = dateObj.getTime();
							firstClick = dateObj;
						}
						
						to = new Date(dateObj.getFullYear(), dateObj.getMonth(), dateObj.getDate() + 1).getTime()-1;
						
						scope.select(from, to);
					};
					
					scope.select = function ( from, to ) {
						scope.from = from;
						scope.to = to;
						scope.$emit('date.range.select', from, to);
					};
					
					scope.isSelected = function ( week, day ) {
						var dateObj = scope.getDateObj(week, day),
							start = dateObj.getTime(),
							end = new Date(dateObj.getFullYear(), dateObj.getMonth(), dateObj.getDate()+1).getTime();
							
						return end > scope.from && start <= scope.to;
					};
					
					scope.isInMonth = function ( week, day ) {
						var now = new Date(scope.now),
							start = new Date(now.getFullYear(), now.getMonth(), 1),
							end = new Date(now.getFullYear(), now.getMonth() + 1, 1),
							date = scope.getDateObj(week, day);
							
						return date.getTime() >= start && date.getTime() < end;
					};
					
					scope.$watch('min', function ( ) {
						if(isNaN(new Date(scope.min).getTime())) {
							scope.min = Number.NEGATIVE_INFINITY;
						}
					});
					
					scope.$watch('max', function ( ) {
						if(isNaN(new Date(scope.max).getTime())) {
							scope.max = Number.POSITIVE_INFINITY;
						}
					});
					
				}
			};
		}]);
	
})();
