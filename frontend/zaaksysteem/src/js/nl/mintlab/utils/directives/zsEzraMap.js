/*global angular,$,load_ezra_map,_*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsEzraMap', [ function ( ) {
			
			return {
				controller: [ '$element', function ( $element ) {
					var ctrl = {},
						markers = [],
						mapEl = $($element[0].querySelector('.ezra_map'));
						
						
					function mapFunc ( ) {
						return mapEl.ezra_map.apply(mapEl, arguments);
					}
					
					function clearMarkers ( ) {
						mapFunc('clearMarkers');
					}
					
					function addMarkers ( ) {
						_.each(markers, function ( marker ) {
							mapFunc('setMarker', marker);
						});
					}
					
					ctrl.addMarker = function ( marker ) {
						clearMarkers();
						markers.push(marker);
						addMarkers();
					};
					
					ctrl.removeMarker = function ( marker ) {
						clearMarkers();
						_.pull(markers, marker);
						addMarkers();
					};
					
					ctrl.setMarkers = function ( newMarkers ) {
						clearMarkers();
						markers = newMarkers.concat();
						addMarkers();
					};
					
					ctrl.fromLatLngToGps = function ( lat, lng ) {
						return mapFunc('_get_epsg_points', lat, lng);
					};
					
					return ctrl;
				}],
				link: function ( /*scope, element, attrs*/ ) {
					
					load_ezra_map();
					
				}
			};
			
		}]);
	
})();