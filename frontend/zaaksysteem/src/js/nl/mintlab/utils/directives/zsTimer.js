/*global angular,window,fetch*/
(function ( ) {
	
	var DEFAULT_TIMEOUT = 10000,
		INTERVAL = 50;
	
	angular.module('Zaaksysteem')
		.directive('zsTimer', [ '$timeout', function ( $timeout ) {
			
			var setInterval = window.setInterval,
				clearInterval = window.clearInterval,
				safeApply = fetch('nl.mintlab.utils.safeApply'),
				getTime;
				
			if(typeof Date.now !== 'undefined') {
				getTime = function ( ){
					return Date.now();
				};
			} else {
				getTime = function ( ) {
					return +(new Date());
				};
			}
				
			
			return {
				scope: true,
				compile: function ( /*tElement, tAttrs, transclude*/ ) {
					
					return function link ( scope, element, attrs ) {
						
						var timeout = scope.$eval(attrs.zsTimer),
							timeoutCancel,
							intervalId,
							started,
							timeLeft;
							
						if(timeout === undefined) {
							timeout = DEFAULT_TIMEOUT;
						}
						
						timeout = parseInt(timeout, 10);
							
						if(timeout === 0) {
							return;
						}
						
						function clearOut ( ) {
							if(timeoutCancel) {
								$timeout.cancel(timeoutCancel);
								timeoutCancel = null;
							}
							if(intervalId) {
								clearInterval(intervalId);
								intervalId = null;
							}
						}
						
						function setTimeLeft ( ) {
							safeApply(scope, function ( ) {
								timeLeft = Math.max(0, timeout - (getTime() - started));
							});
						}
						
						scope.zsTimerLeft = function ( ) {
							return timeLeft;
						};
						
						scope.zsTimerStop = function ( ) {
							clearOut();
							timeLeft = 0;
						};
						
						intervalId = setInterval(function onInterval ( ) {
							setTimeLeft();
						}, INTERVAL);
						
						started = getTime();
						setTimeLeft();
						
						timeoutCancel = $timeout(function onTimeout ( ) {
							scope.$emit('zsTimerComplete');
							clearOut();
							timeLeft = 0;
						}, timeout, false);
						
						scope.$on('$destroy', function onDestroy ( ) {
							clearOut();
						});
						
					};
					
				}
			};
			
		}]);
	
})();