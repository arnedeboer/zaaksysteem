/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('script', [ '$rootScope', '$parse', function ( $rootScope, $parse ) {
			
			var trim = fetch('nl.mintlab.utils.shims.trim');
			
			return {
				restrict: 'E',
				scope: true,
				terminal: 'true',
				compile: function ( tElement, tAttrs/*, transclude*/ ) {
					
					if(tAttrs.type !== 'text/zs-scope-data') {
						return angular.noop;
					}
					
					var parsedData = null,
						error,
						text = trim(tElement[0].text);
						
					try {
						parsedData = text ? JSON.parse(text) : null;
					} catch ( e ) {
						error = e;
					}
					
					return function link ( scope, element, attrs ) {
						
						var parentScope;
						
						if(attrs.zsScopeDataRoot !== undefined) {
							parentScope = $rootScope;
						} else {
							// restrict: 'E' always creates new scope
							parentScope = scope.$parent;
						}
						
						if(!error) {
							for(var key in parsedData) {
								parentScope[key] = parsedData[key];
							}
							if(attrs.zsScopeDataLoad !== undefined) {
								$parse(attrs.zsScopeDataLoad)(scope, {
									'$data': parsedData
								});
							}
							scope.$emit('zs.scope.data.apply', parentScope, parsedData);
						} else {
							scope.$emit('zs.scope.data.error', parentScope, error);
						}
						
					};
					
				}
			};
			
		}]);
	
})();