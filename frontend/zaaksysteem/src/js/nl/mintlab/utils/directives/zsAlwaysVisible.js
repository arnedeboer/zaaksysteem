/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsAlwaysVisible', [ '$window', '$document', function ( $window, $document ) {
			
			var addEventListener = window.zsFetch('nl.mintlab.utils.events.addEventListener'),
				removeEventListener = window.zsFetch('nl.mintlab.utils.events.removeEventListener'),
				getViewportPosition = window.zsFetch('nl.mintlab.utils.dom.getViewportPosition'),
				PREFIX = 'zs-always-visible';
			
			return {
				scope: false,
				require: '^?zsAlwaysVisibleParent',
				link: function ( scope, element, attrs, parent ) {
					
					function checkPos ( ) {
						var bounds = getViewportPosition(element[0]),
							parentEl = parent ? parent.getElement() : null,
							parentBounds = parentEl ? getViewportPosition(parentEl[0]) : { bottom: 0 },
							isInView = bounds.y > parentBounds.bottom;
						
						if(isInView) {
							element.addClass(PREFIX + '-visible');
							element.removeClass(PREFIX + '-hidden');
							element.css('top', '');
						} else {
							element.removeClass(PREFIX + '-visible');
							element.addClass(PREFIX + '-hidden');
							if(parentEl) {
								element.css('top', parentBounds.bottom + 'px');
							}
						}
					}
					
					scope.$watch(checkPos);
					addEventListener($window, 'resize', checkPos);
					$document.bind('scroll', checkPos);
					
					scope.$on('$destroy', function ( ) {
						removeEventListener($window, 'resize', checkPos);
						$document.unbind('scroll', checkPos);
					});
					
					scope.$on('zs.always.visible.reference.update', checkPos);
					
					element.addClass(PREFIX);
					
					checkPos();
					
				}
			};
			
		}]);
	
})();
