/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsModal', [ '$document', function ( $document ) {
			
			var addEventListener = fetch('nl.mintlab.utils.events.addEventListener'),
				removeEventListener = fetch('nl.mintlab.utils.events.removeEventListener'),
				doc = $document[0];
			
			return {
				scope: true,
				transclude: true,
				restrict: 'A',
				templateUrl: '/partials/directives/popup/modal.html',
				compile: function ( /*tElement, tAttrs, transclude*/ ) {
					
					return function ( scope, element, attrs ) {
						
						var closeUnbind;
						
						function onKeyUp ( event ) {
							if(event.keyCode === 27) {
								clean();
								scope.closePopup();
							}
						}
						
						function clean ( ) {
							removeEventListener(doc, 'keyup', onKeyUp);
							if(closeUnbind) {
								closeUnbind();
							}
							if(destroyUnbind) {
								destroyUnbind();
							}
						}
					
						addEventListener(doc, 'keyup', onKeyUp);
						
						closeUnbind = scope.$on('popupclose', clean);
						
						var destroyUnbind = scope.$on('destroy', function ( ) {
							clean();
							scope.closePopup();
						});
						
						attrs.$observe('zsModalTitle', function ( ) {
							// fallback to scope.title to support legacy syntax
							scope.title = attrs.zsModalTitle || scope.title;
						});
						
						
					};
				}
			};
			
		}]);
	
})();
