/*global angular,fetch,_*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsPopupMenu', [ '$window', '$document', function ( $window, $document ) {
			
			var contains = fetch('nl.mintlab.utils.dom.contains'),
				addEventListener = fetch('nl.mintlab.utils.events.addEventListener'),
				removeEventListener = fetch('nl.mintlab.utils.events.removeEventListener'),
				getViewportPosition = fetch('nl.mintlab.utils.dom.getViewportPosition'),
				getViewportSize = fetch('nl.mintlab.utils.dom.getViewportSize'),
				fromLocalToGlobal = fetch('nl.mintlab.utils.dom.fromLocalToGlobal'),
				fromGlobalToLocal = fetch('nl.mintlab.utils.dom.fromGlobalToLocal');
			
			return {
				scope: false,
				require: 'zsPopupMenu',
				controller: [ '$attrs', function ( $attrs ) {
					var ctrl = {},
						list,
						reference;
						
					function positionToReference ( ) {
						var bounds = getViewportPosition(list[0]),
							viewportSize = getViewportSize(),
							refBounds,
							point,
							x,
							y;
							
						refBounds = getViewportPosition(reference[0]);
						
						if(refBounds.bottom + bounds.height > viewportSize.height && refBounds.top - bounds.height > 0) {
							y = -bounds.height;
							list.addClass('zs-popup-menu-positioned-top');
						} else {
							y = refBounds.height;
							list.addClass('zs-popup-menu-positioned-bottom');
						}
						
						if(refBounds.left + bounds.width > viewportSize.width && refBounds.left - bounds.width > 0) {
							x = refBounds.width - bounds.width;
							list.addClass('zs-popup-menu-positioned-right');
						} else {
							x = 0;
							list.addClass('zs-popup-menu-positioned-left');
						}
						
						point = fromGlobalToLocal(list[0].offsetParent, fromLocalToGlobal(reference[0], { x: x, y: y }));
						
						list.css('top', point.y + 'px');
						list.css('left', point.x + 'px');
					}
					
					function positionToViewport ( ) {
						var bounds,
							viewportSize;
						
						bounds = getViewportPosition(list[0]);
						viewportSize = getViewportSize();
						
						if(bounds.bottom > viewportSize.height) {
							list.addClass('zs-popup-menu-positioned-top');
						} else {
							list.addClass('zs-popup-menu-positioned-bottom');
						}
						
						if(bounds.right > viewportSize.width) {
							list.addClass('zs-popup-menu-positioned-right');
						} else {
							list.addClass('zs-popup-menu-positioned-left');
						}
					}
						
					ctrl.setList = function ( el ) {
						if(list) {
							list.removeClass('zs-popup-menu-list');
							list.removeClass('zs-popup-menu-open');
							list.removeClass('zs-popup-menu-closed');
						}
						
						list = el && el.length ? el : null;
						
						if(list) {
							list.addClass('zs-popup-menu-list');
							ctrl.positionMenu();
						}
					};
					
					ctrl.setReference = function ( ref ) {
						reference = ref;
						if(reference) {
							ctrl.positionMenu();
						}
					};
					
					ctrl.getList = function ( ) {
						return list;
					};
					
					ctrl.positionMenu = function ( ) {
							
						if(!list || $attrs.zsPopupMenuPositioning !== 'smart') {
							return;
						}
						
						_.each([ 'top', 'bottom', 'left', 'right' ], function ( val ) {
							list.removeClass('zs-popup-menu-positioned-' + val);
						});
						
						list.css('right', '');
						list.css('bottom', '');
						
						if(reference) {
							positionToReference();
						} else {
							positionToViewport();
						}
					};
					
					return ctrl;
				}],
				compile: function ( ) {
					
					return function link ( scope, element, attrs, zsPopupMenu ) {
						
						var isOpen = null,
							button = element.find('button'),
							behaviour = 'click',
							positionUnwatch;
						
						function onDocumentClick ( event ) {
							scope.$apply(function ( ) {
								var clickTarget = event.target;
								
								if(!(contains(element[0], clickTarget))) {
									closeMenu();
								}
							});
						}
						
						function onElementMouseOut ( /*event*/ ) {
							scope.$apply(function ( ) {
								closeMenu();
							});
						}
						
						function openMenu ( ) {
							
							if(isOpen) {
								return;
							}
							
							isOpen = true;							
							
							element.addClass('zs-popup-menu-open');
							element.removeClass('zs-popup-menu-closed');
							
							if(attrs.zsPopupMenuPositioning === 'smart') {
								addPositionListeners();
							}
						
							if(behaviour === 'click') {
								$document.bind('click', onDocumentClick);
							} else {
								element.bind('mouseleave', onElementMouseOut);
							}
							
							zsPopupMenu.positionMenu();
							
							scope.$emit('zs-popup-menu.open');
						}
						
						function closeMenu ( ) {
							
							if(!isOpen) {
								return;
							}
							
							isOpen = false;
							
							element.addClass('zs-popup-menu-closed');
							element.removeClass('zs-popup-menu-open');
							
							if(attrs.zsPopupMenuPositioning === 'smart') {
								removePositionListeners();
							}
							
							if(behaviour === 'click') {
								$document.unbind('click', onDocumentClick);
							} else {
								element.unbind('mouseleave', onElementMouseOut);
							}
							
							scope.$emit('zs-popup-menu.close');
						}
						
						scope.openPopupMenu = function ( ) {
							openMenu();
						};
						
						scope.closePopupMenu = function ( ) {
							closeMenu();	
						};
						
						scope.isPopupMenuOpen = function ( ) {
							return isOpen;
						};
						
						function onTrigger ( /*evt*/ ) {
							scope.$apply(function ( ) {
								if(isOpen) {
									closeMenu();
								} else {
									openMenu();
								}
							});
						}
						
						function onScroll ( ) {
							zsPopupMenu.positionMenu();
						}
						
						function onResize ( ) {
							zsPopupMenu.positionMenu();
						}
						
						function unbind ( ) {
							var type = behaviour === 'hover' ? 'mouseenter' : 'click';
							button.unbind(type, onTrigger);
						}
						
						function bind ( ) {
							var type = behaviour === 'hover' ? 'mouseenter' : 'click';
							button.bind(type, onTrigger);
						}
						
						function addPositionListeners ( ) {
							
							positionUnwatch = scope.$watch(function ( ) {
								zsPopupMenu.positionMenu();
							});
							
							$document.bind('scroll', onScroll);
							addEventListener($window, 'resize', onResize);
						}
						
						function removePositionListeners ( ) {
							
							if(positionUnwatch) {
								positionUnwatch();
								positionUnwatch = null;
							}
							
							$document.unbind('scroll', onScroll);
							removeEventListener($window, 'resize', onResize);
						}
						
						button.addClass('zs-popup-menu-button');
						
						element.find('ul').eq(0).addClass('zs-popup-menu-list');
						element.addClass('zs-popup-menu');
						element.addClass('zs-popup-menu-closed');
						
						closeMenu();
						
						if(attrs.zsPopupMenuIf) {
							scope.$watch(function ( ) {
								var shouldBeOpen = scope.$eval(attrs.zsPopupMenuIf);
								if(shouldBeOpen) {
									openMenu();
								} else {
									closeMenu();
								}
							});
						} else {
							attrs.$observe('zsPopupMenuBehaviour', function ( ) {
								unbind();
								behaviour = attrs.zsPopupMenuBehaviour === 'hover' ? 'hover' : 'click';
								bind();
							});
						}
						
						if(!zsPopupMenu.getList()) {
							zsPopupMenu.setList(element.find('ul').eq(0));
						}
							
						scope.$on('$destroy', function ( ) {
							$document.unbind('scroll', onScroll);
							removeEventListener($window, 'resize', onResize);
						});
					};
					
				}
			};
		}]);
	
})();