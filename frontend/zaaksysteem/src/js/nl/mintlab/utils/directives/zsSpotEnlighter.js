/*global angular,fetch,_*/
(function ( ) {
	
	var TEMPLATES = {
		'default': '/html/directives/spot-enlighter/default.html',
		'contact': '/html/directives/spot-enlighter/contact.html',
		'contact/medewerker': '/html/directives/spot-enlighter/medewerker.html',
		'contact/bedrijf': '/html/directives/spot-enlighter/bedrijf.html',
		'contact/natuurlijk_persoon': '/html/directives/spot-enlighter/natuurlijk_persoon.html',
		'bag': '/html/directives/spot-enlighter/address.html',
		'bag-street': '/html/directives/spot-enlighter/street.html',
		'zaak': '/html/directives/spot-enlighter/case.html'
	};
	
	var LABELS = {
		'default': undefined,
		'contact': 'voorletters + \' \' + geslachtsnaam',
		'contact/medewerker': 'naam'
	};
	
	angular.module('Zaaksysteem')
		.directive('zsSpotEnlighter', [ '$window', '$document', '$timeout', '$parse', 'templateCompiler', 'smartHttp', function ( $window, $document, $timeout, $parse, templateCompiler, smartHttp) {
			
			var addEventListener = fetch('nl.mintlab.utils.events.addEventListener'),
				removeEventListener = fetch('nl.mintlab.utils.events.removeEventListener'),
				cancelEvent = fetch('nl.mintlab.utils.events.cancelEvent'),
				fromLocalToGlobal = fetch('nl.mintlab.utils.dom.fromLocalToGlobal'),
				getViewportSize = fetch('nl.mintlab.utils.dom.getViewportSize'),
				trim = fetch('nl.mintlab.utils.shims.trim'),
				isObject = angular.isObject,
				body = $document.find('body'),
				parent;
				
			function createMenu ( ) {
				
				parent = angular.element('<div></div>');
				parent.addClass('spot-enlighter-container');
				
				body.append(parent);
				
			}
			
			function moveToFront ( ) {
				body.append(parent);
			}
			
			createMenu();
			
			return {
				scope: false,
				require: 'ngModel',
				priority: 1000,
				compile: function ( /*tElement, tAttrs, transclude*/) {
					
					return function link ( scope, element, attrs, ngModel ) {
						
						var menu,
							query,
							highlighted,
							loading,
							parent,
							loadingEl,
							timeoutCancel,
							tagParent,
							tagEl,
							clearButton,
							interval = 125,
							isOpen = false;
							
						element.attr('autocomplete', 'off');
							
							
						if(!element.parent().hasClass('spot-enlighter-wrapper')) {
							parent = angular.element('<div class="spot-enlighter-wrapper"></div>');
							element.after(parent);
							parent.append(element);
						} else {
							parent = element.parent();
						}
						
						loadingEl = angular.element('<div class="spot-enlighter-loading"></div>');
						element.after(loadingEl);
							
						tagParent = angular.element('<div class="spot-enlighter-tag-wrapper"></div>');
						tagParent.css('visibility', 'hidden');
						element.after(tagParent);
						
						tagEl = angular.element('<div class="spot-enlighter-tag" tabindex="0"></div>');
						tagParent.append(tagEl);
						
						clearButton = angular.element('<button type="button" class="spot-enlighter-clear">x</button>');
						tagParent.append(clearButton);
						
						function openMenu ( ) {
							
							if(isOpen) {
								return;
							}
							
							isOpen = true;
							
							if(!scope.$$phase && !scope.$root.$$phase) {
								scope.$apply(getTemplate);
							} else {
								getTemplate();
							}
							
						}
						
						function hideMenu ( ) {
							
							if(!isOpen) {
								return;
							}
							
							isOpen = false;
							
							unhighlight();
							setData([]);
							
							parent.removeClass('spot-enlighter-visible');
							if(menu) {
								menu.unbind('mousedown', onMenuMouseDown);
								menu.unbind('click', onMenuClick);
								menu.remove();
								menu = undefined;
							}
						}
						
						function getTemplate ( ) {
							
							var url = getTemplateUrl();
							
							templateCompiler.getCompiler(url).then(function ( compiler ) {
								
								if(element[0] !== $document[0].activeElement) {
									menu = undefined;
									isOpen = false;
									return;
								}
								
								moveToFront();
								
								compiler(scope, function ( clonedElement/*, scope*/ ) {
									menu = clonedElement;
									menu.bind('mousedown', onMenuMouseDown);
									menu.bind('click', onMenuClick);
									
									parent.append(menu);
									parent.addClass('spot-enlighter-visible');
									
									if(query) {
										getData();	
									}
									
									positionMenu();
								});
							});
						}
						
						function getTemplateUrl ( ) {
							var url;
							if(attrs.zsSpotEnlighterTemplate) {
								url = scope.$eval(attrs.zsSpotEnlighterTemplate);
							} else {
								url = TEMPLATES[scope.$eval(attrs.zsSpotEnlighterRestrict)] || TEMPLATES['default'];
							}
							return url;
						}
						
						function getObjectType ( ) {
							return scope.$eval(attrs.zsSpotEnlighterRestrict);
						}
						
						function getLabel ( obj ) {
							var toParse = attrs.zsSpotEnlighterLabel || LABELS[getObjectType()] || LABELS['default'],
								label = $parse(toParse)(obj);
							
							return label;
						}
						
						function positionMenu ( ) {
							var pos,
								viewportSize,
								elWidth,
								elHeight,
								menuWidth,
								menuHeight,
								vOrient,
								hOrient,
								x,
								y;
								
							if(!menu) {
								return;
							}
							
							viewportSize = getViewportSize();
							elWidth = element[0].clientWidth;
							elHeight = element[0].clientHeight;
							
							pos = fromLocalToGlobal(element[0], { x: 0, y: 0 });
							
							x = pos.x;
							
							menu.css('width', elWidth + 'px');
							
							menuWidth = menu[0].clientWidth;
							menuHeight = menu[0].clientHeight;
							
							if(pos.y + elHeight + menuHeight > viewportSize.height) {
								vOrient = 'top';
								y = pos.y - menuHeight;
							} else {
								vOrient = 'bottom';
								y = pos.y + elHeight;
							}
							
							if(pos.x + menuWidth > viewportSize.width) {
								x = Math.min(viewportSize.width, pos.x + elWidth) - menuWidth;
								hOrient = 'left';
							} else {
								x = pos.x;
								hOrient = 'right';
							}
							
							menu.css('top', y + 'px');
							menu.css('left', x + 'px');
							menu.attr('data-zs-spot-enlighter-horizontal-orientation', hOrient);
							menu.attr('data-zs-spot-enlighter-vertical-orientation', vOrient);
							
							scope.reversed = vOrient === 'top';
							
						}
						
						function invalidate ( ) {
							if(!loading) {
								if(timeoutCancel) {
									$timeout.cancel(timeoutCancel);
								}
								timeoutCancel = $timeout(function ( ) {
									timeoutCancel = null;
									getData();
								}, interval);
							}
						}
						
						function getData ( ) {
							var loadedQuery,
								params,
								objType = getObjectType(),
								postfix = '',
								url,
								zsSpotEnlighterParams = scope.$eval(attrs.zsSpotEnlighterParams) || {};
							
							if(!query) {				
								return;
							}
							
							loadedQuery = query;
							loading = true;
							
							params = _.clone(zsSpotEnlighterParams);
							params.zapi_num_rows = 20;
							params.query = query;
							
							if(objType) {
								if(objType === 'contact' || objType === 'zaak' || objType === 'natuurlijk_persoon') {
									params.object_type = objType;
								} else {
									postfix = objType;
								}
							}
							
							// TODO: no urls should start with objectsearch
							if(postfix.indexOf('casetype') === 0) {
								url = postfix;
							} else {
								url = 'objectsearch/' + postfix;
							}
							
							setLoader();
							
							smartHttp.connect( {
								url: url,
								method: 'GET',
								params: params,
								// backend needs this to "know" it's a JSON request
								headers: {
									'X-Requested-With': 'XMLHttpRequest'
								}
							})
								.success(handleData)
								.error(handleError)
								.then(function ( ) {
									handleEnd(loadedQuery);
								}, function ( ) {
									handleEnd(loadedQuery);
								});
								
						}
						
						function handleData ( data, status, headers, config ) {
							var entries = data.result !== undefined ? data.result : data.json.entries,
								currentQuery = query,
								performedQuery = config.params.query;
								
							if(currentQuery === performedQuery) {
								setData(entries);
							}
						}
						
						function handleError ( ) {
							
						}
						
						function handleEnd ( loadedQuery ) {
							loading = false;
							if(query !== loadedQuery) {
								invalidate();
							}
							setLoader();
						}
						
						function setData ( entries ) {
							function updateEntries ( ) {
								scope.entries = entries;
								$timeout(function ( ) {
									positionMenu();
								});
							}
							
							if(!scope.$$phase && !scope.$root.$$phase) {
								scope.$apply(updateEntries);
							} else {
								updateEntries();
							}
						}
									
						function setQuery ( q ) {
							q = trim(q);
							if(query !== q) {
								if(!menu) {
									openMenu();
								}
								query = q;
								if(!query) {
									setData([]);
								}
								invalidate();
							}
						}
						
						function selectCurrent ( ) {
							var data;
							if(highlighted) {
								data = highlighted.scope().entry;
							}
							unhighlight();
							selectEntry(data);
							hideMenu();
						}
						
						function highlightNext ( ) {
							var next,
								nextSibling;
							
							if(highlighted) {
								nextSibling = angular.element(highlighted[0].nextSibling);
								while(nextSibling.length && (!nextSibling.scope() || nextSibling.scope().entry === undefined)) {
									nextSibling = angular.element(nextSibling[0].nextSibling);
								}
								next = nextSibling;
							}
							
							if(!next || !next.length) {
								next = menu.find('li').eq(0);
							}
							
							highlight(next);
						}
					
						function highlightPrev ( ) {
							var prev,
								prevSibling,
								list;
							
							if(highlighted) {
								prevSibling = angular.element(highlighted[0].previousSibling);
								while(prevSibling.length && (!prevSibling.scope() || prevSibling.scope().entry === undefined)) {
									prevSibling = angular.element(prevSibling[0].previousSibling);
								}
								prev = prevSibling;
							}
							
							if(!prev || !prev.length) {
								list = menu.find('li');
								prev = list.eq(list.length-1);
							}
							
							highlight(prev);
						}
						
						function highlight ( el ) {
				
							unhighlight();
							
							if(!el.length) {
								return;
							}
							
							el.addClass('spot-enlighter-entry-highlight');
							highlighted = el;
						}
						
						function unhighlight ( ) {
							if(highlighted) {
								highlighted.removeClass('spot-enlighter-entry-highlight');
								highlighted = null;
							}
						}
						
						function onChange ( event ) {
							
							var val;
							
							if(event.keyCode === 13 || event.keyCode === 9) {
								return cancelEvent(event, true);
							}
							
							val = element.val();
							
							setQuery(val);
							
							if(event.keyCode) {
								switch(event.keyCode) {
									
									case 27:
									if(menu) {
										hideMenu();
										return cancelEvent(event, true);
									}
									break;
									
									case 38:
									highlightPrev();
									break;
									
									case 40:
									highlightNext();
									break;
								}
							}
						}
						
						function onKeyDown ( event ) {
							switch(event.keyCode) {
								case 13: case 9:
								if(highlighted) {
									selectCurrent();
									tagEl[0].focus();
									return cancelEvent(event, true);
								}
								break;
								
								case 8:
								var getter = $parse(attrs.ngModel),
									val = getter(scope);
									
								if(val && (typeof val !== 'string' && typeof val !== 'number')) {
									selectEntry(null);
								}
								break;
							}
						}
						
						function onMenuMouseDown ( /*event*/ ) {
							element.unbind('blur', onBlur);
							menu.bind('mouseup', function onMenuMouseUp ( /*event*/ ) {
								menu.unbind('mouseup', onMenuMouseUp);
								// hideMenu();
								element.bind('blur', onBlur);
							});
						}
						
						function onMenuClick ( event ) {
							var el = angular.element(event.target),
								elScope = el.scope();
								
							if(elScope && elScope.entry) {
								selectEntry(elScope.entry);
								hideMenu();
							}
						}
						
						function onFocus ( /*event*/ ) {
							element.val('');
							selectEntry(null);
							openMenu();
						}
						
						function onBlur ( /*event*/ ) {
							hideMenu();
						}
						
						function fromUser ( val ) {
							// make sure user changes never result in model changes
							return val && angular.isObject(val) ? val : undefined;
						}
						
						function toUser ( obj ) {
							var output;
							
							setValidity();
							element.val(obj ? getLabel(obj) : '');
							
							output = obj ? getLabel(obj) : '';
							
							return output;
						}
						
						function selectEntry ( entry ) {
							
							// deal with different object structures
							var obj = entry ? (entry.object || entry) : null;
							
							if(obj && getObjectType() === 'attributes') {
								obj = entry;
							}
							
							scope.$apply(function ( ) {
								ngModel.$setViewValue(obj);
								element.val(obj ? ' ' : '');
								setValidity();
								ngModel.$render();
								
								scope.$emit('zs.spotenlighter.object.select', obj);
								
								if(obj && attrs.zsSpotEnlighterSelect !== undefined) {
									$parse(attrs.zsSpotEnlighterSelect)(scope, {
										'$object': entry
									});
								}
							});
						}
						
						function getModelData ( ) {
							return $parse(attrs.ngModel)(scope);
						}
						
						function setValidity ( ) {
							var data = getModelData(),
								isRequired = attrs.zsObjectRequired && scope.$eval(attrs.zsObjectRequired);
							
							ngModel.$setValidity('zs-object', (data && isObject(data)) || !isRequired);
						}
						
						function onTagKeyDown ( event ) {
							if(!scope.isDisabled() && event.keyCode === 8 || event.keyCode === 46) {
								selectEntry(null);
								element[0].focus();
								return cancelEvent(event, true);
							}
						}
						
						function onClearClick ( /*event*/ ) {
							selectEntry(null);
						}
						
						function setLoader ( ) {
							loadingEl.css('visibility', loading ? 'visible' : 'hidden');
						}
						
						function onScroll ( ) {
							if(isOpen) {
								positionMenu();
							}
						}
						
						scope.getEntryLabel = function ( entry ) {
							return getLabel(entry.object) || entry.label || entry.searchable_object_label;
						};
						
						setLoader();
						
						addEventListener($window, 'scroll', onScroll);
						
						scope.$on('$destroy', function ( ) {
							hideMenu();
							removeEventListener($window, 'scroll', onScroll);
						});
						
						element.bind('focus', onFocus);
						element.bind('blur', onBlur);
						element.bind('change paste keyup input', onChange);
						element.bind('keydown', onKeyDown);
						tagEl.bind('keydown', onTagKeyDown);
						clearButton.bind('click', onClearClick);
						
						ngModel.$formatters.push(toUser);
						ngModel.$parsers.push(fromUser);
						
						ngModel.$render = function ( ) {
							var data = getModelData(),
								val = data ? getLabel(data)  : '';
								
							tagEl.text(val);
							tagParent.css('visibility', val ? 'visible' : 'hidden');
							
							if(val) {
								element.attr('disabled', 'disabled');
							} else {
								element.removeAttr('disabled');
							}
						};
						
						scope.isDisabled = function ( ) {
							return !!scope.$eval(attrs.ngDisabled);
						};
						
						scope.$watch(scope.isDisabled, function ( disabled ) {
							if(disabled) {
								clearButton.attr('disabled', 'disabled');
								tagEl.removeAttr('tabindex');
							} else {
								clearButton.removeAttr('disabled');
								tagEl.attr('tabindex', 0);
							}
						});
						
						attrs.$observe('zsObjectRequired', function ( ) {
							setValidity();
						});
						
						attrs.$observe('zsSpotEnlighterLabel', function ( ) {
							ngModel.$render();
						});
												
					};
					
				}
			};
			
		}]);

})();
