/*global define,window*/
(function ( ) {
	
	var win = window;
	
	window.zsDefine('nl.mintlab.utils.events.removeEventListener', function ( ) {
		
		if(win.removeEventListener) {
			return function ( dispatcher, type, listener, useCapture ) {
				dispatcher.removeEventListener(type, listener, useCapture);
			};
		} else if(win.attachEvent) {
			return function ( dispatcher, type, listener, useCapture ) {
				dispatcher.detachEvent(type, listener);
			};
		}
		
		throw new Error('Events not supported in this browser');
	});
})();
