/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.menu')
		.controller('nl.mintlab.menu.SideMenuController', [ '$scope', 'localDataService', function ( $scope, localDataService ) {
			
			var ctrl = {};
			
			ctrl.collapsed = false;
			
			ctrl.toggleCollapse = function ( ) {
				ctrl.collapsed = !ctrl.collapsed;
				localDataService.set('zs.side-menu.collapsed', ctrl.collapsed);
			};
			
			ctrl.collapsed = localDataService.get('zs.side-menu.collapsed');
			
			return ctrl;
			
		}]);
	
})();