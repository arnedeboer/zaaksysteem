(function ( ) {
	
	angular.module('MintJS')
		.directive('mlFormField', [ function ( ) {
			
			return {
				require: [ '^mlForm', 'mlFormField' ],
				controller: [ '$scope', '$element', '$attrs', function ( $scope, $element, $attrs ) {
					
					var ctrl = {};
					
					ctrl.name = $scope.$eval($attrs.mlFormFieldName);
					
					return ctrl;
					
				}],
				controllerAs: 'mlFormField',
				link: function ( scope, element, attrs, controllers ) {
					
					var mlForm = controllers[0],
						mlFormField = controllers[1];
					
					mlForm.addControl(mlFormField);
					
					scope.$on('$destroy', function ( ) {
						mlForm.removeControl(mlFormField);
					});
					
				}
			};
			
		}]);
	
})();