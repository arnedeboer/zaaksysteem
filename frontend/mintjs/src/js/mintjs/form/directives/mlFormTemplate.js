/*global angular*/
(function ( ) {
	
	angular.module('MintJS.form')
		.directive('mlFormTemplate', [ function ( ) {
			
			return {
				templateUrl: '/html/form/ml-form.html'
			};
			
		}]);
	
})();
