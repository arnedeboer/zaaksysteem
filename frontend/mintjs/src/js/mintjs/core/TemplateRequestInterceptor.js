/*global angular,_*/
(function ( ) {
	
	var ERR_NOT_FOUND = 'Targeted partial was not found in parent ',
		app = angular.module('MintJS');
	
	app.factory('templateRequestInterceptor', [ '$rootScope', '$injector', '$templateCache', '$cacheFactory', '$q', '$document', function ( $rootScope, $injector, $templateCache, $cacheFactory, $q, $document ) {
			
			var $http,
				cache = $cacheFactory('partials');
			
			return {
				request: function ( config ) {
					var match,
						base,
						deferred,
						tplResult,
						httpPromise;
					
					// check if URL is a targeted partial
					match = config.url.match(/((.*?)\.html)\#(.*)$/);
					if(!match) {
						return config;
					}
					
					// check if partial is cached
					tplResult = $templateCache.get(config.url);
					
					if(tplResult) {
						return config;
					}
					
					deferred = $q.defer();
					
					base = match[1];
					
					if(!$http) {
						$http = $injector.get('$http');
					}
					
					function resolveConfig ( ) {
						var transformer = function ( ) {
							var data = $templateCache.get(config.url);
							
							_.pull(config.transformResponse, transformer);
							
							if(!data || data.then !== undefined) {
								data = undefined;
								throw new Error(config.url + ': ' + ERR_NOT_FOUND + base);
							}
							
							return data;
						};
						config.transformResponse.push(transformer);
						deferred.resolve(config);
					}
					
					// check if base url is being loaded
					tplResult = $templateCache.get(base);
					
					if(tplResult) {
						if(tplResult.then === undefined) {
							// base url is loaded but partial was not found
							throw new Error(config.url + ': ' + ERR_NOT_FOUND + base);
						} else {
							// wait until base url is loaded
							tplResult.then(resolveConfig);
						}
					} else {
						// load url
						httpPromise = $http({
								method: 'GET',
								url: base,
								cache: cache
							})
							.success(function ( response ) {
								
								var div,
									templates;
								
								if($templateCache.get(base)) {
									// resolve with current config as partial was already loaded
									resolveConfig();
									return;
								}
								
								$templateCache.put(base, response);
								
								div = $document[0].createElement('div');
								div.innerHTML = response;
								
								templates = div.querySelectorAll('script[type="text/ng-template"]');
								
								_.each(templates, function ( tpl ) {
									$templateCache.put(tpl.getAttribute('id'), angular.element(tpl).text());
								});
								
								resolveConfig();
								
								
							});
					}
					
					return deferred.promise;
				}
			};
			
		}])
		.config([ '$httpProvider', function ( $httpProvider ) {
			$httpProvider.interceptors.push('templateRequestInterceptor');
		}]);
	
})();
