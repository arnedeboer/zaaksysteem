#!/bin/bash

if [ $# -lt "1" ]; then
    echo "USAGE: $0 databasename [database.sql]"
    echo
    echo "Imports db/test-template.sql (or optionally database.sql) to databasename"
    exit;
fi

if [ ! -d db ] || [ ! -d lib ]; then
    echo "Please run this script from your source tree root"
    exit;
fi

DATABASE=$1;
shift;
TEMPLATE=$1;
shift;
CLEAR_LOADED=$1
shift

if [ -z "$TEMPLATE" ]; then TEMPLATE="db/test-template.sql"; fi

DBFILLED=$(dev-bin/is_database_loaded.sh $DATABASE);

if [ -n "$DBFILLED" ]; then
    if [ -n "$CLEAR_LOADED" ] && [ $CLEAR_LOADED == 1 ] ; then
        cat <<OEF | psql template1
SELECT pg_terminate_backend(procpid) FROM pg_stat_activity WHERE procpid <> pg_backend_pid() AND datname = '$DATABASE';
DROP DATABASE $DATABASE;
CREATE DATABASE $DATABASE;
OEF
    else
        echo $DBFILLED;
        exit 0;
    fi
fi

psql $DATABASE < $TEMPLATE;
