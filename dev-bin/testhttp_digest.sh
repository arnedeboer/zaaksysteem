#!/usr/bin/env bash

USER=$1
PASS=$2
URL=$3

# Example without post
curl -k --digest -u "$USER:$PASS" "$URL" --verbose

# Example with post
#curl --anyauth -k -H "Content-Type: application/json" -d '{"uuid":"56716082-55e2-43f7-9cf9-4836b603f6f9"}' --digest -u "$USER:$PASS" "$URL" --verbose
