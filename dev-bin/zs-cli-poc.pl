#!/usr/bin/perl
use strict;
use warnings;

use Cwd 'realpath';
use FindBin;
use lib "$FindBin::Bin/../lib";

use Zaaksysteem::CLI::PoC;

my $cli = Zaaksysteem::CLI::PoC->new(
    config   => "/vagrant/etc/zaaksysteem.conf",
    hostname => "vagrant",
    debug    => 1,
    dry      => 1, # Set to 0 for no dry runs
);
# Or
# my $cli = Zaaksysteem::CLI::PoC->init();

# Run the script.
$cli->run();

1;

__END__

=head1 NAME

zs-cli-poc.pl - A Proof of Concept CLI script

=head1 DESCRIPTION

Showing you what you need and how to implement it.

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
