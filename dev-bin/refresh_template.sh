#!/bin/bash

if [ $# -lt "1" ]; then
    echo "USAGE: $0 [LIST OF TEST FILES]"
    echo
    echo "Loads update scripts on test template, and dumps is back"
    exit;
fi

if [ ! -d db ] || [ ! -d lib ]; then
    echo "Please run this script from your source tree root"
    exit;
fi

TEMPLATE="db/test-template.sql";
EMPTYTEMPLATE="db/template.sql";


PROCID=$$;
DATABASE="zs_test_${USER}_${PROCID}";
DATABASEFILE="/tmp/${DATABASE}.sql";

dbexists=`psql -l |grep $DATABASE`;
if [ "$dbexists" != "" ]; then
    echo "DB $DATABASE EXISTS, STOPPING. PLEASE TRY AGAIN";
    exit;
fi

echo "BEGIN;" > $DATABASEFILE;
for arg in $*; do
    cat $arg | sed 's/BEGIN;//i' | sed 's/COMMIT;//i' >> $DATABASEFILE;
done;
echo "COMMIT;" >> $DATABASEFILE;

echo "CREATING TEST DATABAE $DATABASE";

echo "CREATE DATABASE $DATABASE ENCODING 'UTF-8';"| psql template1 && \
    psql $DATABASE < $TEMPLATE &&
    psql $DATABASE < $DATABASEFILE;

echo "DUMPING DATABASE $DATABASE BACK TO $TEMPLATE"

pg_dump -O $DATABASE > $TEMPLATE

echo "DUMPING DATABASE $DATABASE BACK TO $EMPTYTEMPLATE"
pg_dump -O --schema-only $DATABASE > $EMPTYTEMPLATE

echo "DROP DATABASE $DATABASE" | psql template1
