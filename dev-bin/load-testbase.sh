#! /bin/bash

set -e

DUMP_PATH="${1:-$HOME}/testbase"
DATABASE="${DUMP_PATH}/db.sql.gz"
FILESTORE="${DUMP_PATH}/filestore.tar.xz"

if [ ! -e "${DATABASE}" -o ! -e "${FILESTORE}" ]; then
    echo "No dump found. Please run copy-testbase.sh first."
    exit 1
fi

echo "Dropping old database"
dropdb testbase || true

echo "Creating new database"
createdb testbase

echo "Filling new database"
zcat "${DATABASE}" | psql -q testbase

if [ -f after_custom_load.sql ]
then
    psql -f after_custom_load.sql testbase
fi

if [ -e /var/tmp/testbase.zaaksysteem.nl ]; then
    echo "Removing old filestore"
    rm -r /var/tmp/testbase.zaaksysteem.nl
fi

echo "Unpacking filestore dump"
tar -x -a -C /var/tmp -f "${FILESTORE}"
