#!/usr/bin/perl
#
use strict;
use warnings;

use Cwd 'realpath';
use FindBin;
use lib "$FindBin::Bin/../lib";

use Try::Tiny;
use Time::HiRes qw(gettimeofday tv_interval);
use Zaaksysteem::CLI;

my $cli = try {
    Zaaksysteem::CLI->init;
}
catch {
    die $_;
};

my %opt = %{ $cli->options };

if ($opt{case} && $opt{offset}) {
    die "Conflicting options found: case and offset";
}

my $args = {};
if (exists $opt{case}) {
    if ($opt{case} =~ /^\d+/) {
        $args = { 'me.id' => $opt{case} };
    }
    elsif ($opt{case} eq 'empty') {
        my $rs = $cli->schema->resultset('ObjectData')->search({
            object_class => 'case',
            properties   => '{}',
        });
        $args = { 'me.id' => { '-in' => $rs->get_column('object_id')->as_query }};
    }
}

my $rs = $cli->schema->resultset('Zaak')->search_extended(
    $args,
    {
        order_by => { -desc => 'me.id' },
        $opt{offset} ? (offset => $opt{offset}) : (),
        $opt{limit}  ? (rows   => $opt{limit}) : (),
    }
);

my $count = $rs->count();
if (!$count) {
    print "No cases found\n";
    exit 0;
}

my $cases_done = 0;

my $starttime  = [gettimeofday];
while (my $zaak = $rs->next) {
    $cli->do_transaction(
        sub {
            my $self   = shift;
            my $schema = shift;

            $cases_done++;
            if ($zaak->is_deleted) {
                $zaak->object_data;
                printf(
                    "Skipping hstore update for zaak @{[$zaak->id]} (%d of %d, %.3f/second) because it's deleted\n",
                    $cases_done, $count,
                    $cases_done / tv_interval($starttime, [gettimeofday]),
                );
                return;
            }

            printf(
                "Updating hstore for zaak @{[$zaak->id]} (%d of %d, %.3f/second)\n",
                $cases_done, $count, $cases_done / tv_interval($starttime, [gettimeofday]),
            );

            if ($opt{casetype}) {
                $zaak->object_data->object_relation_object_ids->search({
                    object_type => 'casetype'
                })->delete;
            }

            $zaak->touch();
        }
    );
}

1;

__END__

=head1 NAME

touch_case.pl - A case toucher

=head1 SYNOPSIS

touch_case.pl OPTIONS [ -o case=1234 ] || [ -o case=empty ] || [ -o offset=2 ]

=head1 OPTIONS

=over

=item * config

The Zaaksysteem configuration defaults to /etc/zaaksysteem/zaaksysteem.conf

=item * customer_d

The customer_d dir, defaults to the relative directory where zaaksysteem.conf is found.

=item * hostname

The hostname you want to touch cases for

=item * o

You can use this specify an option.

=back

=over 8

=item * case

You can use this to update an specific case, not supplying this will touch all cases. If you supply 'emtpy' you will touch empty cases.

=item * offset

In case you want to touch all cases but want to start with a certain offset. This is handy in case the process get's killed and you want to continue the run.
This option conflicts with the case option.

=item * limit

Limits the number of cases touched. Can be combined with C<offset> to touch all cases in
batches.

=item * casetype

Include the casetype relation update for a case.

=back

=over

=item * no-dry

Run it and commit the changes. The default is NOT to commit any changes.

=back

=head1 COPYRIGHT and LICENSE

Copyright (c) 2014-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
