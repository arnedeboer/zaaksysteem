#!/usr/bin/perl
#
use strict;
use warnings;

use Cwd 'realpath';
use FindBin;
use lib "$FindBin::Bin/../lib";

use Try::Tiny;
use Time::HiRes qw(gettimeofday tv_interval);
use Zaaksysteem::CLI;

my $cli = try {
    Zaaksysteem::CLI->init;
}
catch {
    die $_;
};

my %opt = %{ $cli->options };

if ($opt{case} && $opt{offset}) {
    die "Conflicting options found: case and offset";
}

my $args = {};
if (exists $opt{case}) {
    $args = { 'me.id' => $opt{case} };
}

my $rs = $cli->schema->resultset('Zaak')->search_extended(
    $args,
    {
        order_by => { -desc => 'me.id' },
        $opt{offset} ? (offset => $opt{offset}) : (),
    }
);

my $count = $rs->count();
if (!$count) {
    print "No cases found\n";
    return;
}

my $cases_done = 0;

my $starttime  = [gettimeofday];
while (my $zaak = $rs->next) {
    $cli->do_transaction(
        sub {
            my $self   = shift;
            my $schema = shift;

            $cases_done++;
            if ($zaak->is_deleted) {
                $zaak->object_data;
                printf(
                    "Skipping hstore update for zaak @{[$zaak->id]} (%d of %d, %.3f/second) because it's deleted\n",
                    $cases_done, $count,
                    $cases_done / tv_interval($starttime, [gettimeofday]),
                );
                return;
            }

            printf(
                "Updating hstore for zaak @{[$zaak->id]} (%d of %d, %.3f/second)\n",
                $cases_done, $count, $cases_done / tv_interval($starttime, [gettimeofday]),
            );

            $zaak->touch();
        }
    );
}

1;

__END__

=head1 NAME

touch_case.pl - A case toucher

=head1 SYNOPSIS

touch_case.pl OPTIONS [ -o case=1234 ] [ -o offset=2 ]

=head1 OPTIONS

=over

=item * config

The Zaaksysteem configuration defaults to /etc/zaaksysteem/zaaksysteem.conf

=item * customer_d

The customer_d dir, defaults to the relative directory where zaaksysteem.conf is found.

=item * hostname

The hostname you want to touch cases for

=item * o

You can use this specify multiple sub options

=back

=over 8

=item * case

You can use this to update specific cases, not supplying this will touch all cases

=item * offset

In case you want to touch all cases but want to start with a certain offset. This is handy in case the process get's killed and you want to continue the run.
This option conflicts with the case option.

=back

=over

=item * n

Dry run, run it, but don't

=back

=head1 COPYRIGHT and LICENSE

Copyright (c) 2014-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
