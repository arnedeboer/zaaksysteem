#!/bin/sh

if [ "$3" = "" ]; then
    echo "USAGE $0 xmlfile.xml ApIKeY hostname";
    exit
fi
curl -kvv --data-urlencode "component=sync_users" --data-urlencode "api_key=AD-$2" --data-urlencode "message@$1" https://$3/api/users
