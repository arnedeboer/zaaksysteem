#!/usr/bin/env bash

rel=$1
if [ -z "$rel" ]
then
    echo 'No release supplied' 1>&2
    exit 1
fi

_basedir=db/upgrade

dir=$_basedir/$rel
if [ ! -d "$dir" ]
then
    echo "Release '$rel' has no upgrade directory";
    exit 1
fi

roleup=$_basedir/$rel.sql

echo "BEGIN;" > $roleup
for i in $dir/*.sql
do
        sql=$(cat $i | egrep -iv "(BEGIN|COMMIT);")
        echo "    /* $i */"
        echo -e "$sql"
        echo ""

done >> $roleup
echo "COMMIT;" >> $roleup;

invalid_sql=$(find $dir ! -iname '*\.sql' -type f -print0);
if [ -n "$invalid_sql" ]
then
    echo "Upgrade scripts with incorrect filenames found!" 1>&2
    echo -e "$invalid_sql"; 1>&2
    exit 1;
fi

exit 0;
