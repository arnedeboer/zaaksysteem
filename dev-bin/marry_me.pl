#!/usr/bin/perl
use strict;
use warnings;

use Cwd 'realpath';
use FindBin;
use lib "$FindBin::Bin/../lib";

use Zaaksysteem::CLI::NatuurlijkPersoon::Marry;
my $cli = Zaaksysteem::CLI::NatuurlijkPersoon::Marry->init;
$cli->run;

1;

__END__

=head1 NAME

marry_me.pl - Marry a natural person for testing purposes

=head1 SYNOPSIS

marry_me.pl OPTIONS

=head1 OPTIONS

=over

=item * config

The Zaaksysteem configuration defaults to /etc/zaaksysteem/zaaksysteem.conf

=item * customer_d

The customer_d dir, defaults to the relative directory where zaaksysteem.conf is found.

=item * hostname

The hostname you want to touch cases for

=item * no-dry

Run it and commit the changes. The default is NOT to commit any changes.

=item * o

You can use this specify an option.

=over

=item * bsn

The BSN of the natural person you want to marry

=item * partner_bsn

The BSN of the partner

=item * partner_a_nummer

The a-number of the partner

=item * partner_voorvoegsel

The voorvoegsel of the partner

=item * partner_geslachtsnaam

The geslachtsnaam of the partner

=item * naamsgebruik

The use of the name.

=item * datum

The date of the marriage

=item * naamsgebruik

The use of the name.

=back

=back

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
