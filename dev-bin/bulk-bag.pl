#!/usr/bin/perl
use warnings;
use strict;

use FindBin qw/$Bin/;
use lib "$Bin/../lib";

our $VERSION = 1_0;


use autodie;
use File::Spec::Functions qw(catfile);
use File::Basename;
use Pod::Usage;
use Zaaksysteem::CLI;
use Getopt::Long qw{ :config no_ignore_case };

my %opt = (
    help       => 0,
    config     => '/etc/zaaksysteem/zaaksysteem.conf',
    customer_d => '/etc/zaaksysteem/customer.d',
    n          => 0,
    directory  => undef,
    hostname   => '10.44.0.11',
);


{
    local $SIG{__WARN__};
    my $ok = eval {
        GetOptions(
            \%opt, qw(
                help
                n
                config=s
                customer_d=s
                hostname=s
                file=s
                )
        );
    };
    if (!$ok) {
        pod2usage(1);
    }
}

pod2usage(0) if ($opt{help});

foreach (qw(config hostname file)) {
    if (!defined $opt{$_}) {
        warn "Missing option: $_";
        pod2usage(1);
    }
}

pod2usage(0) if $opt{help};
pod2usage(1) if !-f $opt{file};

my $cli = Zaaksysteem::CLI->new(
    customer_d => $opt{customer_d},
    config     => $opt{config},
    hostname   => $opt{hostname},
    n          => $opt{n},
);

my $interface = $cli->schema->resultset('Interface')
    ->search_active({ module => 'bagcsv' })->first;
die "No BAG CSV interface found" if !$interface;

$cli->do_transaction(
    sub {
        print "Processing $opt{file}\n";
        my $file = basename($opt{file});
        my $fs   = $cli->schema->resultset('Filestore')->filestore_create(
            {
                file_path     => $opt{file},
                original_name => $file,
            }
        );
        my $transaction = $interface->process(
            {
                input_filestore_uuid    => $fs->uuid,
                external_transaction_id => "ZS::CLI: " . DateTime->now->iso8601,
            }
        );
        printf("'%s' inserted %d BAG records\n",
            $opt{file}, $transaction->transaction_records->count);
    }
);

1;

__END__

=head1 NAME

bulk-bag.pl - A bulk bag importer

=head1 SYNOPSIS

bulk-ztb.pl OPTIONS

=head1 OPTIONS

=over

=item * directory

=back

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
