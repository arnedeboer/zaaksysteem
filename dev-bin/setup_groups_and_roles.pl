#!/usr/bin/perl

use strict;
use warnings;

use Cwd 'realpath';
use FindBin;
use lib "$FindBin::Bin/../lib";

use Getopt::Long;
use Zaaksysteem::Config;
use Pod::Usage;
use File::Spec::Functions;
use File::Basename;
use Digest::SHA;
use MIME::Base64;

use Time::HiRes qw(gettimeofday tv_interval);

my %opt = (
    help       => 0,
    config     => '/etc/zaaksysteem/zaaksysteem.conf',
    n          => 0,
);


{
    local $SIG{__WARN__};
    my $ok = eval {
        GetOptions(
            \%opt, qw(
                help
                n
                config=s
                customer_d=s
                hostname=s
                organization=s
                case=s@
                password=s
                )
        );
    };
    if (!$ok) {
        pod2usage(1) ;
    }
}

pod2usage(0) if ($opt{help});

foreach (qw(config hostname organization)) {
    if (!defined $opt{$_}) {
        warn "Missing option: $_";
        pod2usage(1) ;
    }
}

$opt{password} = 'zaaksysteem' unless $opt{password};

if (!defined $opt{customer_d}) {
    $opt{customer_d} = catdir(dirname($opt{config}), 'customer.d');
}

my $ZS = Zaaksysteem::Config->new(
    zs_customer_d => $opt{customer_d},
    zs_conf       => $opt{config},
);

my $schema = $ZS->get_customer_schema($opt{hostname}, 1, { skip_ldap => 1 });

setup_roles_and_groups($schema);

sub setup_roles_and_groups {
    my $dbic = shift;
    my @roles;

    die("Groups already created, abort\n") if $dbic->resultset('Groups')->count;
    die("Roles already created, abort\n") if $dbic->resultset('Roles')->count;

    do_transaction(
        $dbic,
        sub {
            my $parent      = $dbic->resultset('Groups')->create_group(
                {
                    name        => ucfirst($opt{organization}),
                    description => ucfirst($opt{organization}) . ' root',
                }
            );

            warn('Created top group: ' . $parent->name . "\n");

            my $backoffice      = $dbic->resultset('Groups')->create_group(
                {
                    name            => "Backoffice",
                    description     => "Default backoffice",
                    parent_group_id => $parent->id,
                }
            );

            warn(' |_> created systeemgroep: ' . $backoffice->name . "\n");

            my $frontoffice      = $dbic->resultset('Groups')->create_group(
                {
                    name            => "Frontoffice",
                    description     => "Default frontoffice",
                    parent_group_id => $parent->id,
                }
            );

            warn(' |_> created systeemgroep: ' . $frontoffice->name . "\n");

            push(@roles,
                $dbic->resultset('Roles')->create_role(
                    {
                        name                => $_,
                        parent_group_id     => $parent->id,
                        description         => "Systeemgroep: " . $_,
                        system_role         => 1,
                    }
                )
            ) for qw/
                Administrator
                Zaaksysteembeheerder
                Zaaktypebeheerder
                Zaakbeheerder
                Contactbeheerder
                Basisregistratiebeheerder
                Wethouder
                Directielid
                Afdelingshoofd
                Kcc-medewerker
                Zaakverdeler
                Behandelaar
                Gebruikersbeheerder
            /;

            my $interface   = $dbic->resultset('Interface')->find_by_module_name('authldap');

            create_subject($dbic, 'admin', $interface, $backoffice, [grep { $_->name =~ /Administrator|Behandelaar/} @roles]);
            create_subject($dbic, 'gebruiker', $interface, $frontoffice, [grep { $_->name =~ /Behandelaar/} @roles]);
            create_subject($dbic, 'beheerder', $interface, $backoffice, [grep { $_->name =~ /Administrator|Behandelaar/} @roles]);
        }
    );

    warn("Created roles: \n - " . join("\n - ", map({ $_->name } @roles)) . "\n");

}

sub create_subject {
    my $dbic        = shift;
    my $username    = shift;
    my $interface   = shift;
    my $group       = shift;
    my $roles       = shift;

    my $ldap_data = {
        cn              => $username,
        sn              => $username,
        displayname     => $username,
        givenname       => $username,
        initials        => $username,
        mail            => 'devnull@zaaksysteem.nl',
        telephonenumber => '0612345678',
    };


    my $subject     = $dbic->resultset('Subject')->create(
        {
            subject_type    => 'employee',
            properties      => $ldap_data,
            username        => $username,
        }
    );

    $dbic->resultset('UserEntity')->create(
        {
            source_interface_id         => $interface->id,
            source_identifier           => $username,
            active                      => 1,
            subject_id                  => $subject->id,
            password                    => password_ssha($opt{password})
        }
    );

    $subject->group_ids([$group->id]);
    $subject->role_ids([ map { $_->id } @$roles]);

    $subject->update;

    warn("Created user: " . $username . "\n");
    warn("  Group: " . $group->name . "\n");
    warn("  Roles:\n    - " . join("\n    - ", map { $_->name } @$roles) . "\n");
}

sub do_transaction {
    my ($dbic, $sub) = @_;

    $dbic->txn_do(
        sub {
            $sub->();
            if ($opt{n}) {
                $dbic->txn_rollback;
            }
        }
    );
}

sub password_ssha {
    my $pass    = shift;

    my ($hashedPasswd,$salt);

    $salt           = get_salt8();
    my $ctx         = Digest::SHA->new;

    $ctx->add($pass);
    $ctx->add($salt);

    $hashedPasswd   = '{SSHA}' . encode_base64($ctx->digest . $salt,'');

    return($hashedPasswd);
}

sub get_salt8 {
    my $salt = join '', ('a'..'z')[rand 26,rand 26,rand 26,rand 26,rand 26,rand 26,rand 26,rand 26];
    return($salt);
}

1;

__END__

=head1 NAME

touch_case.pl - A case toucher

=head1 SYNOPSIS

touch_case.pl OPTIONS [ [ --case 1234 ] [--case 12345 ] ]

=head1 OPTIONS

=over

=item * config

The Zaaksysteem configuration defaults to /etc/zaaksysteem/zaaksysteem.conf

=item * customer_d

The customer_d dir, defaults to the relative directory where zaaksysteem.conf is found.

=item * hostname

The hostname you want to touch cases for

=item * organization

Organization name, to use as primary root of the Organization Tree

=item * n

Dry run, run it, but don't

=back

=head1 COPYRIGHT and LICENSE

Copyright (c) 2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
