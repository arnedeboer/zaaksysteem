#!/usr/bin/perl
use strict;
use warnings;

use Cwd 'realpath';
use FindBin;
use lib "$FindBin::Bin/../lib";

use Try::Tiny;
use Zaaksysteem::CLI;
use Term::ReadPassword::Win32 qw(read_password);

my $cli = try {
    Zaaksysteem::CLI->init;
}
catch {
    die $_;
};

foreach (qw(username)) {
    if (!defined $cli->options->{$_}) {
        warn "Missing option: $_";
        pod2usage(1) ;
    }
}

my $password = read_password("Please supply the password: ");
my $check    = read_password("Reenter the password: ");

while ($password ne $check) {
    $password = read_password("Please supply the password: ");
    $check    = read_password("Reenter the password: ");
}

$cli->do_transaction(
    sub {
        my $schema = $cli->schema;

        my $auth_ldap = $schema->resultset('Interface')->search_active({module => 'authldap'})->first;
        die "Unable to change password, no authentication interface available" if !$auth_ldap;

        my $rs = $schema->resultset('UserEntity')->search_rs(
            {
                source_identifier   => $cli->options->{username},
                source_interface_id => $auth_ldap->id
            }
        );

        if ($rs->count != 1) {
            die "Unable to change username, multiple entries found: " . $rs->count;
        }
        my $user = $rs->first;
        $user->subject_id->update_password($user, {password => $password});
    }
);


__END__

=head1 NAME

zs-passwd.pl - Change passwords of users from the command line

=head1 SYNOPSIS

zs-passwd.pl OPTIONS --username <username>

=head1 OPTIONS

=over

=item * config

The Zaaksysteem configuration defaults to /etc/zaaksysteem/zaaksysteem.conf

=item * customer_d

The customer_d dir, defaults to the relative directory where zaaksysteem.conf is found.

=item * hostname

The hostname you want to touch cases for

=item * username

The username for which you want to change the password

=item * password

The password you want the user to have

=item * n

Dry run, run it, but don't

=back

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
