#!/usr/bin/env bash

APIKEY=$1
HOST=$2
DIR=$3

log_error() {
    echo "$@" 1>&2
    exit 1
}

if [ -z "$APIKEY" ] || [ -z "$HOST" ] || [ -z "$DIR" ]
then
    log_error "Usage: $(basename $0) <apikey> <https:://mijn.omgeving.nl> <directory>"
fi

URLPATH=/api/scanstraat/upload_document

if [ ! -d "$DIR" ]
then
    log_error "Directory '$DIR' does not exist!"
fi

for i in $DIR/*
do
    curl -k --form "api_key=$APIKEY" --form "filename=@$i" "${HOST}${URLPATH}"
done
