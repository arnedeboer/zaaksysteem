#!/usr/bin/perl
use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../lib";

use autodie;
use DateTime;
use IO::Handle;
use Log::Log4perl qw(:easy);
use Text::CSV_XS;
use Try::Tiny;
use Zaaksysteem::CLI;
use Zaaksysteem::Zaken::DelayedTouch;

my $cli = try {
    Zaaksysteem::CLI->init;
}
catch {
    die $_;
};

my %opt = %{ $cli->options };

if (not exists $opt{output}) {
    $opt{output} = "output-" . DateTime->now->ymd . ".csv";
}
if (not exists $opt{errors}) {
    $opt{errors} = "errors-" . DateTime->now->ymd . ".csv";
}

open my $outputlog, ">", $opt{output}; $outputlog->autoflush(1);
open my $errorlog,  ">", $opt{errors}; $errorlog->autoflush(1);

my $csv = Text::CSV_XS->new({ binary => 1, eol => "\015\012" });

$csv->print(
    $outputlog,
    [
        "ID Origineel",
        "BSN Origineel",
        "Voornaam Origineel",
        "Achternaam Origineel",
        "ID Nieuw",
        "BSN Nieuw",
        "Voornaam Nieuw",
        "Achternaam Nieuw",
        "Zaaknummer"
    ]
);

$csv->print(
    $errorlog,
    [
        "ID Origineel",
        "BSN Origineel",
        "Voornaam Origineel",
        "Achternaam Origineel",
        "Foutmelding"
    ]
);

my $delayed = Zaaksysteem::Zaken::DelayedTouch->new;
$cli->schema->default_resultset_attributes->{delayed_touch} = $delayed;

my $total_count = 0;
$cli->do_transaction(sub {
    my $self = shift;
    my $schema = shift;

    my $rs = $schema->resultset('NatuurlijkPersoon')->search(
        { authenticated => \'IS NOT TRUE' },
        { order         => ['id'] }
    );

    while(my $contact = $rs->next) {
        INFO("Got contact " . $contact->id);

        my $authorized_contact = eval {
            get_authorized_contact($contact);
        };
        if (my $e = $@) {
            chomp($e);
            skip($contact, $e);
            next;
        }

        my $betrokkenen_rs = $schema->resultset('ZaakBetrokkenen')->search_rs(
            {
                'me.betrokkene_type'      => 'natuurlijk_persoon',
                'me.gegevens_magazijn_id' => $contact->get_column('id'),
                'me.deleted'              => undef,
            }
        );
        my $cases = $schema->resultset('Zaak')->search_extended(
            { aanvrager => { -in => $betrokkenen_rs->get_column('id')->as_query } }
        );

        while (my $case = $cases->next) {
            INFO("Updating case " . $case->get_column('id'));
            $case->set_aanvrager(sprintf("betrokkene-natuurlijk_persoon-%d", $authorized_contact->id));
            success($contact, $authorized_contact, $case);
        }

        $contact->update({ deleted_on => DateTime->now() });
    }

    # Will die (and roll back transaction) on disk-full/other disk exceptions.
    close $outputlog;
    close $errorlog;
});

INFO("Don't forget to do a touch_case.pl run!");

sub skip {
    my ($contact, $reason) = @_;
    DEBUG("Skipped contact " . $contact->id . " for reason '$reason'");

    $csv->print(
        $errorlog,
        [
            $contact->id,
            int($contact->burgerservicenummer),
            $contact->voornamen,
            $contact->achternaam,
            $reason,
        ]
    );
}

sub success {
    my ($contact, $authcontact, $case) = @_;
    DEBUG("Successfully updated " . $case->id . " for contact " . $contact->id);

    $csv->print(
        $outputlog,
        [
            $contact->id,
            int($contact->burgerservicenummer),
            $contact->voornamen,
            $contact->achternaam,
            $authcontact->id,
            int($authcontact->burgerservicenummer),
            $authcontact->voornamen,
            $authcontact->achternaam,
            $case->id
        ]
    );
}

sub get_authorized_contact {
    my $contact = shift;

    my $authorized_contact_rs = $cli->schema->resultset('NatuurlijkPersoon')->search(
        {
            authenticated       => 1,
            deleted_on          => undef,
            'NULLIF(me.burgerservicenummer,\'\')::integer' => int($contact->burgerservicenummer),
        },
    );
    my $count = $authorized_contact_rs->count;
    if ($count < 1) {
        die "No authenticated contact found\n";
    }
    elsif ($count > 1) {
        die "More than one authenticated contact found\n";
    }

    return $authorized_contact_rs->single;
}

1;

__END__

=head1 NAME

deduplicate_contacts.pl - Move all cases to "authenticated" versions of the requestor.

=head1 SYNOPSIS

deduplicate_contacts.pl OPTIONS [ -o errors=errorlog_filename.csv ] ] [ -o output=output_filename.csv ]

=head1 OPTIONS

=over

=item * config

The Zaaksysteem configuration defaults to /etc/zaaksysteem/zaaksysteem.conf

=item * customer_d

The customer_d dir, defaults to the relative directory where zaaksysteem.conf is found.

=item * hostname

The hostname you want to touch cases for

=item * o

You can use this specify an option.

=back

=over 8

=item * errors

Write errors to this (CSV) log. Defaults to C<errors-yyyy-mm-dd.csv>.

This will list all contacts that don't have authenticated equivalents.

=item * output

Write output to this (CSV) log. Defaults to C<output-yyyy-mm-dd.csv>.

=back

=over

=item * no-dry

Run it and commit the changes. The default is NOT to commit any changes.

=back

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
