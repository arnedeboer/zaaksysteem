#!/usr/bin/perl

use warnings;
use strict;
use FindBin qw/$Bin/;

use lib "$Bin/../lib";

use Zaaksysteem::Constants;
use Zaaksysteem::Schema;

use autodie;
use LWP::UserAgent;
use File::Spec::Functions qw(catfile);
use Zaaksysteem::Exception;

use Getopt::Long qw{ :config no_ignore_case };
my %opt = (
    directory      => '',
    dbi            => '',
    db_username    => undef,
    db_password    => undef,
    create_subject => 'betrokkene-medewerker-20000',
    base_url       => 'https://10.44.0.11',
    api_key        => '',
);

GetOptions \%opt => qw{
  directory=s
  dbi=s
  db_username=s
  db_password=s
  create_subject=s
  base_url=s
  api_key=s
  help
  } or exit usage();

if ($opt{help}) {
    usage();
    exit 0;
}

my $schema;
my @args;

if (!$opt{api_key}) {
    @args = ($opt{dbi}, $opt{db_username}, $opt{db_password});
    $schema = Zaaksysteem::Schema->connect(
        @args, {AutoCommit => 1},
    );
    import_directory($opt{directory});
}
else {
    upload_to_web($opt{directory});
}

sub upload_to_web {
    my $dir   = shift;
    my @files = _get_files($dir);

    if (!@files) {
        print "No files ready for processing in dropdir '$dir'\n";
        return;
    }

    my $ua = LWP::UserAgent->new(
        agent      => 'dropdir/client v1',
        timeout    => 30,

        # Self signed certs
        ssl_opts              => {verify_hostname => 0},
        requests_redirectable => ["POST"],
    );

    my @errors;

    my $url = "$opt{base_url}/api/scanstraat/upload_document";
    for my $file (@files) {
        my $r = $ua->post(
            $url,
            Content_Type => 'form-data',
            Content      => [
                api_key  => $opt{api_key},
                filename => [$file],
        ]);

        if ($r->is_success) {
            unlink $file;
        }
        else {
            push(@errors, sprintf("File '%s' could not be uploaded: %s", $file, $r->status_line));
        }
    }

    if (@errors) {
        print "Errors occurred processing files in dropdir:\n";
        print join("\n", @errors);
        print "\n";
    }
}

sub _get_files {
    my $dir = shift;

    my $d;
    opendir $d, $dir;
    my $now = time();

    my @files;
    for my $file (readdir($d)) {
        my $filepath = catfile($dir, $file);
        next if (!-f $filepath);

        # TODO: perhaps set blocking read/write lock?
        my $stat = (stat($filepath))[9];
        next if ($now - $stat < 120);
        push @files, $filepath;
    }
    closedir($d);
    return @files;
}

sub import_directory {
    my $directory = shift;

    opendir(DIR, $directory);

    my @errors;
    for my $file (readdir(DIR)) {

        # Skip directories
        next if (!-f "$directory/$file");

        # Files younger than 60 seconds are considered to possible
        # be incomplete.
        my @stat = stat("$directory/$file");
        next if (time - $stat[9] < 60);

        eval {
            $schema->resultset('File')->file_create({
                    file_path => "$directory/$file",
                    name      => $file,
                    db_params => {
                        reject_to_queue => 1,
                        created_by      => $opt{create_subject},
                    },
                });
        };
        if ($@) {
            push @errors, "Error processing $file: $@";
        }
        else {
            unlink "$directory/$file";
        }
    }

    closedir(DIR);

    if (@errors) {
        print "Errors occurred processing files in dropdir:\n\n";
        print join("\n", @errors) if @errors;
        print "\n";
    }
}

sub usage {

    print
      qq[Imports files from a certain directory in zaaksysteem documentintake.

--directory        Directory to read files from
--dbi              DBI DSN string
--db_username      DBI username
--db_password      DBI password
--create_subject   Subject to create files with (ex. betrokkene-medewerker-20000)
--base_url         Use the web API, no need for a direct DB connection.
--api_key          The API key to use. If you set this, we will not inject files directly into the DB, we will use the API instead.

];
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

