#!/usr/bin/perl

use warnings;
use strict;

use v5.10;

use Config::Any;
use Data::Dumper;
use File::Spec::Functions qw(catfile);
use JSON::XS;
use LockFile::Simple;
use LWP::UserAgent;
use HTTP::Request;

### Configuration root
my $config_root = '/etc/zaaksysteem/customer.d/';
my $lockfile    = '/tmp/schedule_run.pl.lock';
my $debug       = 1;

# Fetch configuration files
opendir CONFIG, $config_root or die "Error reading config root: $!";
my @confs = grep {/\.conf$/} readdir CONFIG;

# Prepare some LWP/HTTP::Request stuff
my $agent = LWP::UserAgent->new(env_proxy => 1,keep_alive => 1, timeout => 30);

debug("Found configuration files: @confs");

# Lockfile check/create
my $lock = LockFile::Simple->make(-stale => 1, -warn => 0, -ext => '');
$lockfile = catfile($lockfile);
unless ($lock->trylock($lockfile)) {
    die "$0: process locked - already running";
}
debug("Process isn't locked");

for my $f (@confs) {
    my $config = _process_config("$config_root/$f");
    my @customers = $config;

    debug("Number of hosts in $f: ".map {keys %$_} @customers);

    for my $customer (@customers) {
        for my $host (keys %$customer) {
            _run_jobs_for_host($host);
        }
    }
}

sub _process_config {
    my ($config) = @_;
    return Config::Any->load_files({
        files       => [$config],
        use_ext     => 1,
        driver_args => {}
    })->[0]->{$config};
}

sub _run_jobs_for_host {
    my ($host) = @_;

    my $url = "http://$host/schedule/run";
    debug("\nCalling GET for: $url");

    my $header   = HTTP::Request->new(GET => $url);
    my $request  = HTTP::Request->new('GET', $url, $header);
    my $response = $agent->request($request);

    # Fatal error during communication with server
    if ($response->is_error) {
        say sprintf "Fatal error processing run_scheduled for %s: %s", ($url, $response->content);
        return;
    }

    # Parse the JSON response
    my $json = decode_json($response->content);

    if (!exists $json->{result} || $json->{result}[0]{result} ne 'success') {
        say sprintf "Caught error processing run_scheduled for %s: %s", ($url, Dumper $json);
    }
    debug ("Successful run: $host");

    return;
}

sub debug {
    if ($debug) {
        say @_;
    }
}

$lock->unlock($lockfile);
debug("Removed lockfile");



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

