#
# Cookbook Name:: zaaksysteem
# Recipe:: database
#
# Copyright 2013, Example Com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

include_recipe "zaaksysteem::ubuntu"
include_recipe "zaaksysteem::clamav"

apt_package "curl" do
    action :install
end

execute "add_my_apt_key" do
        command "curl http://packages.public.zaaksysteem.nl/mintlab-pubkey.txt | /usr/bin/apt-key add -"
        user "root"
        notifies :run, resources(:execute => "apt-get-update"), :immediately
end

apt_repository "mintlab" do
    uri "http://packages.public.zaaksysteem.nl/ubuntu-precise/"
    arch "amd64"
    components ["precise","main"]
end

File.open("/vagrant/MANIFEST.Debian", "r") do |file_handle|
    file_handle.each_line do |server|
        if server !~ /^\s*$/ && server !~ /^\s*#/
            apt_package server.chomp do
                not_if { node['zaaksysteem']['package'] }
                action :upgrade
            end
        end
    end
end

apt_package "libzaaksysteem-perl" do
    only_if { node['zaaksysteem']['package'] }
    action :install
end

cookbook_file "/etc/init/zaaksysteem.conf" do
    source "zaaksysteem.conf"
    mode 00700
    owner "root"
    group "root"
end

### Default config copy
bash "default_zaaksysteem_conf" do
    user "vagrant"
    cwd "/vagrant"
    code "cp /vagrant/etc/zaaksysteem.conf.dist /vagrant/etc/zaaksysteem.conf"
    not_if do ::File.exists?('/vagrant/etc/zaaksysteem.conf') end;
end

bash "default_zaaksysteem_test_conf" do
    user "vagrant"
    cwd "/vagrant"
    code "cp /vagrant/etc/zaaksysteem-test.conf.dist /vagrant/etc/zaaksysteem-test.conf"
    not_if do ::File.exists?('/vagrant/etc/zaaksysteem-test.conf') end;
end

bash  "default_vagrant_test_conf" do
    user "vagrant"
    cwd "/vagrant"
    code "cp /vagrant/etc/vagrant_default.conf.dist /vagrant/etc/customer.d/vagrant.conf"
    not_if do ::File.exists?('/vagrant/etc/customer.d/vagrant.conf') end;
end

directory "/usr/share/ca-certificates/extra" do
    mode 00755
    owner "root"
end

execute "extra-root-certificates" do
    command <<-EOF
    cp /vagrant/etc/caroots/*.crt /usr/share/ca-certificates/extra;
    ls /usr/share/ca-certificates/extra | xargs -I {} -n1 echo extra/{} >> /etc/ca-certificates.conf;
    update-ca-certificates
    EOF
    user "root"
end

basedir = "/var/tmp/zs"

directory basedir do
    mode 00750
    owner "vagrant"
    recursive true
end

%w{storage tmp}.each do |d|
    d = File.join(basedir, d)
    directory d do
        mode 00750
        owner "vagrant"
        recursive true
    end
end


include_recipe "zaaksysteem::webserver";
include_recipe "zaaksysteem::database";
include_recipe "zaaksysteem::openldap";
include_recipe "zaaksysteem::soffice";
include_recipe "zaaksysteem::development";
include_recipe "zaaksysteem::postfix";

