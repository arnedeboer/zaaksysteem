#
# Cookbook Name:: zaaksysteem
# Recipe:: webserver
#
# Copyright 2013, Example Com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

service "nginx" do
        supports :status => true, :restart => true, :reload => true
        action [ :enable, :start ]
end

directory '/etc/nginx/ssl' do
    owner "vagrant"
    group "vagrant"
    mode 0500
    action :create
end

bash "generate_ssl_certificate" do
    user "root"
    cwd "/tmp"
    code <<-EOS
        openssl genrsa -out ca.key 2048
        openssl req -new -x509 -days 365 -key ca.key -out ca.crt -subj "/C=NL/O=Devlab/OU=CA/CN=vagrant.zaaksysteem.nl"
        openssl genrsa -out server.key 2048
        openssl req -new -subj "/C=NL/O=Devlab/OU=Dev/CN=vagrant.zaaksysteem.nl" -key server.key -out server.csr
        openssl x509 -req -days 365 -in server.csr -CA ca.crt -CAkey ca.key -set_serial $(shuf -i 2000-65000 -n1) -out server.crt

        mv {server,ca}.{crt,key} /etc/nginx/ssl
    EOS
    not_if { ::File.exists?('/etc/nginx/ssl/server.crt') and ::File.exists?('/etc/nginx/ssl/server.key') }
end

template '/etc/nginx/sites-available/zaaksysteem.conf' do
    source 'nginx/zaaksysteem-virtualhost.conf'
    notifies  :restart, 'service[nginx]'
end

template '/etc/nginx/fastcgi_params' do
    source 'nginx/fastcgi_params'
    notifies  :restart, 'service[nginx]'
end

template '/etc/nginx/zaaksysteem.conf' do
    source 'nginx/zaaksysteem.conf'
    notifies  :restart, 'service[nginx]'
end

execute "enable_site_zaaksysteem" do
    command "/usr/sbin/nxensite zaaksysteem.conf"
    user "root"
    notifies  :reload, 'service[nginx]'
    not_if 'ls /etc/nginx/sites-enabled/zaaksysteem.conf'
end

execute "disable_site_default" do
    command "/usr/sbin/nxdissite default"
    user "root"
    notifies  :reload, 'service[nginx]'
    only_if 'ls /etc/nginx/sites-enabled/zaaksysteem.conf'
end

template '/etc/nginx/conf.d/zaaksysteem.conf' do
    source 'nginx/zaaksysteem-conf.conf'
    notifies  :restart, 'service[nginx]'
end
