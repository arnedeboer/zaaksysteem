%w{
    postfix
    postfix-pcre
    mailutils
    mutt
}.each do |p|
    apt_package p do
        action :install
    end
end

service "postfix" do
    supports :restart => true
end

cookbook_file "/etc/postfix/main.cf" do
    source "postfix_main.cf"
    mode 0644
    notifies :restart, "service[postfix]"
end

file "/etc/postfix/mydestinations" do
    content "/.*/ ACCEPT"
    mode 0644
    notifies :restart, "service[postfix]"
end
