#
# Cookbook Name:: zaaksysteem
# Recipe:: default
#
# Copyright 2013, Example Com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

include_recipe "postgresql::server";
include_recipe "postgresql::ruby";

apt_package 'postgresql-contrib'

cookbook_file "/etc/postgresql/9.1/main/pg_ident.conf" do
    source "pg_ident.conf"
    mode 00600
    owner "postgres"
    group "postgres"
    notifies :reload, 'service[postgresql]', :delayed
end

cookbook_file "/etc/postgresql/9.1/main/pg_hba.conf" do
    source "pg_hba.conf"
    mode 00600
    owner "postgres"
    group "postgres"
    notifies :reload, 'service[postgresql]', :delayed
end

### Database creation
postgresql_connection_info = {
    :host       => "127.0.0.1",
    :port       => node['zaaksysteem']['database']['config']['port'],
    :username   => node['zaaksysteem']['database']['username']['postgres'],
    :password   => node['zaaksysteem']['database']['password']['postgres']
}

postgresql_database_user 'vagrant' do
    connection postgresql_connection_info
    password node['zaaksysteem']['database']['password']['postgres']
    action :create
end

postgresql_database_user 'root' do
    connection postgresql_connection_info
    password node['zaaksysteem']['database']['password']['postgres']
    action :create
end

postgresql_database 'zaaksysteem' do
  connection postgresql_connection_info
  action :create
end

postgresql_database 'zs_test' do
  connection postgresql_connection_info
  action :create
end

postgresql_database_user 'vagrant' do
    connection postgresql_connection_info
    database_name "zaaksysteem"
    privileges [:all]
    action :grant
end

execute "vagrant_superuser" do
    user "postgres"
    command "echo 'ALTER USER vagrant SUPERUSER' | psql template1"
end

execute "vagrant_password" do
    user "postgres"
    command "echo 'ALTER USER vagrant PASSWORD \"#{node['zaaksysteem']['database']['password']['postgres']}\"'"
end

cmd = '/bin/bash dev-bin/load_database.sh zs_test db/template.sql';
if node.key?('databases') && node['databases']['zs_test']['refresh'] == true
    execute "database_test_reload" do
        cwd "/vagrant"
        user "vagrant"
        command cmd + " 1"
        notifies :run, "bash[set_zs_test_db]", :immediately
    end
else
    execute "database_test_initial_load" do
        cwd "/vagrant"
        user "vagrant"
        command cmd
        only_if '/bin/bash /vagrant/dev-bin/is_database_loaded.sh zs_test', :user => 'postgres'
        notifies :run, "bash[set_zs_test_db]", :immediately
    end
end

cmd = '/bin/bash dev-bin/load_database.sh zaaksysteem db/test-template.sql';
if node.key?('databases') && node['databases']['zaaksysteem']['refresh'] == true
    execute "database_zaaksysteem_reload" do
        cwd "/vagrant"
        user "vagrant"
        command cmd + " 1"
        notifies :run, "bash[set_zaaksysteem_db]", :immediately
    end
else
    execute "database_zaaksysteem_initial_load" do
        cwd "/vagrant"
        user "vagrant"
        command cmd
        only_if '/bin/bash /vagrant/dev-bin/is_database_loaded.sh zaaksysteem', :user => 'postgres'
        notifies :run, "bash[set_zaaksysteem_db]", :immediately
    end
end

execute "zaaksysteem_load_users" do
    cwd "/vagrant"
    user "vagrant"
    command "/usr/bin/perl dev-bin/setup_groups_and_roles.pl --config /vagrant/etc/zaaksysteem.conf --customer_d /vagrant/etc/customer.d --hostname 10.44.0.11 -o organisation=Mintlab -o password=admin -o init=1"
    only_if '/bin/bash /vagrant/dev-bin/are_users_loaded.sh zaaksysteem', :user => 'vagrant'
    subscribes :run, "bash[database_zaaksysteem]", :immediately
end

bash "set_zaaksysteem_db" do
    user "vagrant"
    action :nothing
    code <<-EOH
        echo "DELETE FROM config WHERE parameter IN ('filestore_location','tmp_location');" | psql zaaksysteem
    EOH
    subscribes :run, "bash[database_zaaksysteem]", :immediately
end

bash "set_zs_test_db" do
    action :nothing
    user "vagrant"
    code <<-EOH
        echo "DELETE FROM config WHERE parameter IN ('filestore_location','tmp_location');" | psql zs_test
        echo "DELETE FROM interface WHERE module IN ('authldap');" | psql zs_test
        echo "INSERT INTO interface (name, active, max_retries, interface_config, multiple, module) VALUES ( 'LDAP authenticatie', true, 10, '{}', true, 'authldap');" | psql zs_test
    EOH
    subscribes :run, "bash[database_test]", :immediately
end

