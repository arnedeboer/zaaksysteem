'use strict';

let http = require('http'),
	https = require('https'),
	json2csv = require('json2csv'),
	fs = require('fs'),
	minimist = require('minimist'),
	assign = require('lodash/object/assign'),
	argv = minimist(process.argv.slice(2)),
//	docs = fs.readFileSync('README.md', 'utf8'),
	port = argv.port ? argv.port : 1030;

if (argv.help) {
    console.log("Please see the README.md file for more information");
    process.exit(0);
}

console.log('Starting json2csv...');

let processData = ( obj ) => {

	let output;

	json2csv({ data: obj, flatten: true }, (err, csv) => {

		if (err) {
			console.log(`Error with parsing: ${err}`);
			return err;
		}

		output = csv;

	});

	return output;
};

let queryAPI = ( response, requestOptions ) => {

	https.get(requestOptions, (res) => {

		let data = '';

		res.on('data', (chunk) => {
			data += chunk;
		});

		res.on('end', ( ) => {

			let apiResponse,
				output;

			console.log('Attempting to parse result.instance.rows from this retrieved API response:', data);

			try {
				apiResponse = JSON.parse(data);

				if (apiResponse.result.instance.rows.length) {
					output = processData(apiResponse.result.instance.rows);
				} else {
					output = 'Geen resultaten verkregen op ingestelde zoekopdracht in combinatie met de ingestelde gebruiker in het koppelprofiel. Controleer de instellingen in het koppelprofiel en probeer het opnieuw.';
				}

			} catch (err) {
				response.write('Unable to parse response as JSON: ', err);
			}

			response.write(`${output}`);

			response.end();

		});

	}).on('error', (err) => {

		let errorString = `Got error: ${err.message}`;

		console.error(errorString);

		response.write(errorString);

	});
};


let server = http.createServer( ( req, res ) => {

    let currentHost = req.headers.host,
        requestOptions = {
            host: argv.server ? argv.server : currentHost,
            path: `${argv.path ? `${argv.path}` : `/api/v1/case`}${req.url.substring(1).replace('app/api2csv', '') }`,
            method: 'GET',
            rejectUnauthorized: false,
            headers: assign({},
                argv.interfaceid ? { 'Api-Interface-Id': argv.interfaceid } : null,
                argv.apikey ? { 'Api-Key': argv.apikey } : null,
                argv.user ? { 'ZS-User-Name': argv.user } : null,
                argv.password ? { 'ZS-User-Password': argv.password } : null
            )
        };

    if (req.headers['api-key']) {
        requestOptions.headers['Api-Key'] = req.headers['api-key'];
    }

    if (req.headers['api-interface-id']) {
        requestOptions.headers['Api-Interface-Id'] = req.headers['api-interface-id'];
    }

    if (req) {
        queryAPI(res, requestOptions);
    }

});

process.on('SIGINT', () => {
    console.log('Received SIGINT. Exiting.');
    server.close( ( ) => {
        process.exit(0);
    });
});

process.on('SIGTERM', () => {
    console.log('Received SIGTERM. Exiting.');
    server.close( ( ) => {
        process.exit(0);
    });
});

server.listen(port);

console.log(`Awaiting requests on port ${port}.`);

