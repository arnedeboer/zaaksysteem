'use strict';

const TTL = 60 * 15;

let https = require('https'),
	zlib = require('zlib'),
	digest = require('digest-header'),
	Memcached = require('memcached'),
	url = require('url'),
	series = require('promise-map-series'),
	uniq = require('lodash/array/uniq'),
	without = require('lodash/array/without');

let fetcher = {},
	cache,
	cachedRequests = [];

let getDigestHeaders = ( req, res ) => {

	let user = req.headers['zs-user-name'],
		pass = req.headers['zs-user-password'],
		auth = digest(req.method, req.url, res.headers['www-authenticate'], `${user}:${pass}`);

	return { Authorization: auth };
};

let request = ( req, options, respond ) => {

	let domain = req.headers.host.split(':')[0],
		target = `https://${domain}`;

	let opts = Object.assign(
			url.parse(req.url),
			{
				host: target.split('://')[1],
				secure: false,
				rejectUnauthorized: false,
				headers: Object.assign({}, req.headers, options.headers)
			}
		);

	let end = ( response, data ) => {

		respond(response, data);

	};

	https.request( opts, ( response ) => {

		let chunks = [];

		response.on('data', ( chunk ) => {
			chunks.push(chunk);
		});

		response.on('end', ( ) => {

			let statusCode = response.statusCode,
				data;

			try {
				let buffer = Buffer.concat(chunks);

				if (response.headers['content-encoding']) {
					data = zlib.unzipSync(buffer).toString();
				} else {
					data = buffer;
				}

			} catch ( error ) {
				statusCode = 500;
				console.error('Decode error', error);
			}

			if (statusCode >= 200 && statusCode < 300) {

				if (response.headers['content-type'].indexOf('application/json') !== -1) {

					cache.set(req.url, data, TTL, ( err ) => {
						if (err) {
							console.error('Error storing data', req.url, err);
						}
					});

				}

				end(response, data);
			} else if (statusCode === 401 && !(options.headers && 'Authorization' in options.headers.Authorization)) {
				request(req, Object.assign({}, options, { headers: getDigestHeaders(req, response) }), respond);
			} else {
				console.error('Error', req.url, data);
				end(response, data);
			}

		});

		response.on('error', ( err ) => {
			console.error('Response error', req.url, err);
			end(response);
		});

	})
		.on('error', ( err ) => {
			console.error('Request error', req.url, err);
			end();
		})
		.end();

};

fetcher.proxy = ( req, res ) => {

	cachedRequests = uniq(cachedRequests.concat(req), ( cachedReq ) => cachedReq.url);

	cache.get(req.url, ( err, cacheData ) => {

		if (cacheData) {
			res.writeHead(200, { 'content-type': 'application/json', 'content-encoding': 'gzip' });
			res.end(zlib.gzipSync(cacheData));
		} else {
			request(req, {}, ( response, data ) => {

				let gzip = response.headers['content-type'].indexOf('application/json') !== -1,
					headers = Object.assign({}, response.headers);

				if (gzip) {
					headers = Object.assign(headers, { 'content-encoding': 'gzip' });
				}

				if (response) {
					res.writeHead(
						response.statusCode,
						headers
					);
				} else {
					res.writeHead(500);
				}
				res.end(gzip ? zlib.gzipSync(data) : data);
			});
		}

	});

};

fetcher.flush = ( ) => {

	return Promise.all(
		cachedRequests.map(
			( cachedRequest ) => {

				return new Promise( ( resolve, reject ) => {

					cache.del(cachedRequest.url, ( err ) => {

						if (err) {
							reject(err);
						} else {
							resolve();
						}

					});

				})
					.then(( ) => {
						cachedRequests = without(cachedRequests, cachedRequest);
					});
			}
		)
	);

};

fetcher.retry = ( ) => {

	return series(cachedRequests, ( cachedRequest ) => {

		return new Promise( ( resolve/*, reject*/ ) => {
			request(cachedRequest, {}, ( ) => {
				resolve();
			});

		});
	});

};

module.exports = ( opts ) => {

	cache = new Memcached(opts.memcachedAddress && opts.memcachedAddress.length ? opts.memcachedAddress : '127.0.0.1:11211');

	setInterval(fetcher.retry, TTL * 1000);

	return fetcher;

};
