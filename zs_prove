#! /usr/bin/perl
use warnings;
use strict;

use File::Spec::Functions qw/ :DEFAULT rel2abs abs2rel /;
use File::Basename;
use List::Util qw(shuffle);

my $bindir;
BEGIN { $bindir = rel2abs dirname $0 }

chdir $bindir or die "Cannot chdir($bindir): $!";
use lib catdir $bindir, 'lib';

my $test_dir    = catdir $bindir, 't';
my $test_blocks = get_test_blocks($test_dir);
my $test_rules  = make_test_rules($test_blocks);

{
    my @args = @ARGV;

    my $prove = App::Prove::Rules->new;
    $prove->process_args( @args );
    $prove->test_rules( $test_rules );

    exit ( $prove->run ? 0 : 1 );
}

sub get_test_blocks {
    my ($dir) = @_;

    # Can be used to run tests sequentially in a later stage.
    my $skip = { map +( $_ => undef ), (
       # map abs2rel( $_ ), glob( catfile $dir, '1234*.t' ),
    )};

    # no_shuffle makes sure they are not randomized.
    my $no_shuffle = 1;
    return {
        pre  => _filter_tests(catfile($dir, '0*.t'), $skip, $no_shuffle),  # testsuite
        par1 => _filter_tests(catfile($dir, '{1,2,3,4,5,6,7}*.t'), $skip), # regular
        post => _filter_tests(catfile($dir, '8*.t' ), $skip, $no_shuffle), # catalyst
        end  => _filter_tests(catfile($dir, '9*.t' ), $skip, $no_shuffle), # cleanup
    };
}

sub make_test_rules {
    my ($blocks) = @_;

    return {
        seq => [
            { seq => $blocks->{pre} },
            { par => $blocks->{par1} },
            { par => $blocks->{par2} },
            { seq => $blocks->{late} },
            { seq => $blocks->{post} },
            { seq => $blocks->{end} },
        ]
    };
}

sub _filter_tests {
    my ($mask, $filter, $no_shuffle) = @_;

    my @tests = grep {
        not exists $filter->{ $_ }
    } map abs2rel( $_ ), glob $mask;

    return $no_shuffle ? \@tests : [ shuffle @tests ];
}

package App::Prove::Rules;
use warnings;
use strict;
use base 'App::Prove';

use Sys::CPU qw/cpu_count/;

=head1 NAME

App::Prove::Rules - A subclass of App::Prove for better test-rules

=head1 SYNOPSIS

    use App::Prove::Rules;

    my $test_rules = {
        seq => [
            { seq => [ glob 't/0*.t' ] },
            { par => [ glob 't/1*.t' ] },
            { par => [ glob 't/{2,3,4,5,6,7}*.t' ] },
            { seq => [ glob 't/{8,9}*.t' ] },
        ]
    };
    my $prove = App::Prove::Rules->new;

    $prove->process_args( @args );
    $prove->test_rules( $test_rules );

    exit ( $prove->run ? 0 : 1 );

=head1 DESCRIPTION

A new baseblock to help fine-tune parallel testing.

=cut

our $VERSION = 0.01;

=head2 $prove->process_args( @args )

This method overrides the baseclass method, in order to set some defaults:

=over 4

=item --jobs=5

=item --lib

=item -w

=back

=cut

sub process_args {
    my ($self, @args) = @_;

    $ENV{ZS_TEST} = 1;
    # Make sure we do not override the commandline
    my $has_jobs = grep /^-j(?:=\d+)?$/ || /^--?jobs(?:=\d+)?$/ => @args;
    if ( not $has_jobs ) {
        my $ncpu = 4;
        eval { require Sys::CPU };
        if (!$@) { $ncpu = Sys::CPU::cpu_count(); }
        unshift @args, ( '-j', $ncpu + 1 );
    }
    my $has_blib_or_lib = grep /^--?b?lib$/ || /^-\w*?[lb]/ => @args;
    if ( not $has_blib_or_lib ) {
        unshift @args, (
            '-l',
            '-It/inc',
            '-It/lib',
        );
    }
    my $has_warnings =  grep /^-\w*?[wW]/ => @args;
    if ( not $has_warnings ) {
        unshift @args, '-w';
    }

    if ($ENV{'TEST_VERBOSE'}) {
        unshift @args, '-v';
    }

    if (grep /^-v$/, @args) {
        delete $ENV{ZS_TEST};
    }

    $self->SUPER::process_args( @args );
}

=head2 $prove->test_rules( [\%rules] )

Getter/Setter method for the (temporary) B<test_rules> attribute.

=cut

sub test_rules {
    my ($self) = shift;

    if ( @_ ) { $self->{test_rules} = shift }
    return $self->{test_rules};
}

# =begin private

# =head2 $self->_get_args

# This is a dirty hack to set the rules field to the hash we passed earlier.

# =end private

# =cut

sub _get_args {
    my $self = shift;

    my ($args, $class) = $self->SUPER::_get_args( @_ );

    if ( $self->test_rules ) {
        $args->{rules} = delete $self->{test_rules};
    }

    return ( $args, $class );
}

1;
__END__

